﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstTeaSnakcs.aspx.cs" Inherits="MstTeaSnakcs" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Master</a></li>
                    <li class="active">Tea/Sancks</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Tea/Snacks </h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Tea/Snacks</h4>
                            </div>
                            <div class="panel-body">
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Canteen Name</label>
                                            <asp:DropDownList ID="ddlCanteenName" runat="server" class="form-control  BorderStyle select2" Style="width: 100%"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ControlToValidate="ddlCanteenName" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Shift</label>
                                            <asp:DropDownList ID="ddlShift" runat="server" class="form-control  BorderStyle select2" Style="width: 100%"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ControlToValidate="ddlShift" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker" Style="text-transform: uppercase">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Time</label>
                                            <asp:TextBox runat="server" ID="txttime" class="form-control" Style="text-transform: uppercase" MaxLength="8">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txttime" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txttime" ValidChars="0123456789: APMapm">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="form-group col-md-1">
                                        <br />
                                        <asp:Button ID="btnView" Width="50" Height="30" class="btn-success" runat="server" Text="View" ValidationGroup="Item_Validate_Field" OnClick="btnView_Click" />
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <!-- begin col-4 -->

                                    <!-- end col-4 -->
                                    <div class="col-md-4"></div>
                                </div>
                                <!-- end row -->

                                <!-- table start -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <asp:CheckBox ID="chkAll" runat="server" Text="Select / UnSelect" Visible="false"
                                                OnCheckedChanged="chkAll_CheckedChanged" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
                                            <asp:GridView ID="GVModule" runat="server" AutoGenerateColumns="false"
                                                ClientIDMode="Static" class="gvv display table">
                                                <Columns>
                                                    <%--  <asp:TemplateField  HeaderText="FormID" Visible="false">
				                            <ItemTemplate>
				                                <asp:Label id="ItemID" runat="server" Text='<%# Eval("ItemID") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="ItemName" HeaderText="Item Name" />
                                                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                                    <asp:TemplateField HeaderText="Add">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <div class="col-md-4">
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="row">
                                    </div>
                                    <div class="row">
                                        <div align="center">
                                            <br />
                                            <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                ValidationGroup="Validate_Field" Visible="false" OnClick="btnSave_Click" />
                                            <asp:Button runat="server" ID="btnClear" Visible="false" Text="Clear" class="btn btn-danger"
                                                OnClick="btnClear_Click" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Canteen Name</th>
                                                                <th>Shift</th>
                                                                <th>Date</th>
                                                                <th>Time</th>
                                                                <th>Item Name</th>
                                                                <th>Amount</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("CanteenName")%></td>
                                                        <td><%# Eval("Shift")%></td>
                                                        <td><%# Eval("Dte")%></td>
                                                        <td><%# Eval("Time")%></td>
                                                        <td><%# Eval("ItemName")%></td>
                                                        <td><%# Eval("Amount")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Canteen Name?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                                <!-- table End -->

                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->

            <script src="assets/js/master_list_jquery.min.js"></script>
            <script src="assets/js/master_list_jquery-ui.min.js"></script>
            <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#example').dataTable();
                    $('.select2').select2();
                });
            </script>

            <script type="text/javascript">
                //On UpdatePanel Refresh
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                if (prm != null) {
                    prm.add_endRequest(function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            $('.select2').select2();
                            $('#example').dataTable();
                        }
                    });
                };
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
