﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="AttendanceUpload.aspx.cs" Inherits="AttendanceUpload" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
     $(document).ready(function () {
         App.init();
         FormWizard.init();
         //FormWizardValidation.init();
         $('.select2').select2();
         $('.datepicker').datepicker({
             format: "dd/mm/yyyy",
             autoclose: true
         });

     });
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>


<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Attendance Upload</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Attendance Upload</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>
                  <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Attendance Upload</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlfinance" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlfinance_SelectedIndexChanged">
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlMonths_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								<br />
								 <asp:FileUpload ID="FileUpload" runat="server" />
								</div>
								</div>
								 <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                          <!-- begin col-4 -->
                              <div id="Div1" class="col-md-4" runat="server" >
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" style="width:100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">Staff</asp:ListItem>
								 <asp:ListItem Value="2">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div id="Div2" class="col-md-4" runat="server" >
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                                 <div id="Div3" class="col-md-4" runat="server" visible="false">
									<div class="form-group">
										<label>No Of Working Days</label>
										<asp:TextBox runat="server" ID="txtdays" class="form-control" Text="0"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                                 <div id="Div4" class="col-md-4" runat="server" visible="false">
									<div class="form-group">
										<label>NFH Days</label>
										<asp:TextBox runat="server" ID="txtNfh" class="form-control" Text="0"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                        <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFrom" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtTo" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                             
                         </div>
                       <!-- end row -->
                      
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnUpload" Text="Upload" OnClick="btnUpload_Click" OnClientClick="ProgressBarShow();" class="btn btn-success" />
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                        </div>
                          <div id="Download_loader" style="display:none"/></div>
                    </div>
                    <!-- end panel -->
                </div>
                  </ContentTemplate>
                  <Triggers>
                     <asp:PostBackTrigger ControlID="btnUpload" />
                  </Triggers>
                </asp:UpdatePanel>
			    
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->





</asp:Content>

