﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstLabourAllot : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string SessionIsAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionIsAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Master";
            Load_Department();
            Load_Designation();
            ManDayCost_Details();
            Load_Location();
        }
        Load_Allotment_Details();
        Load_New_Data();
    }
    private void Load_Location()
    {
        SSQL = "";
        SSQL = "Select LocCode from Location_Mst ";
        DataTable Dt_Loc = new DataTable();
        Dt_Loc = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlUnit.DataSource = Dt_Loc;
        ddlUnit.DataTextField = "LocCode";
        ddlUnit.DataValueField = "LocCode";
        ddlUnit.DataBind();
        ddlUnit.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
    private void Load_Designation()
    {
        DataTable dt = new DataTable();
        if (SessionIsAdmin == "1")
        {
            SSQL = "Select DE.DesignNo,DE.DesignName from Designation_Mst DE inner join Department_Mst DM";
            SSQL = SSQL + " on DE.DeptCode=DM.DeptCode";
            SSQL = SSQL + " where DE.CompCode='" + SessionCcode + "' And DE.LocCode='" + ddlUnit.SelectedValue + "'";
            SSQL = SSQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + ddlUnit.SelectedValue + "'";
           // SSQL = "SELECT DesignNo, DesignName FROM Designation_Mst where CompCode = '" + SessionCcode + "' And LocCode = '" + ddlUnit.SelectedValue + "'";
        }
        else
        {
            SSQL = "Select DE.DesignNo,DE.DesignName from Designation_Mst DE inner join Department_Mst DM";
            SSQL = SSQL + " on DE.DeptCode=DM.DeptCode";
            SSQL = SSQL + " where DE.CompCode='" + SessionCcode + "' And DE.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And DM.CompCode='" + SessionCcode + "' And DM.LocCode='" + SessionLcode + "'";
            //SSQL = "SELECT DesignNo, DesignName FROM Designation_Mst where CompCode = '" + SessionCcode + "' And LocCode = '" + SessionLcode + "'";
        }

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDesignation.DataSource = dt;
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignNo";
        ddlDesignation.DataBind();
        ddlDesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }

    private void Load_New_Data()
    {
        DataTable dt = new DataTable();
        if (SessionIsAdmin == "1")
        {
            SSQL = "Select * from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode + "'";
        }
        else
        {
            SSQL = "Select * from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode + "'";
        }
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptName";
        ddlDepartment.DataBind();
    }

    private void ManDayCost_Details()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        query = "Select *from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; ;
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        if (dtdsupp.Rows.Count != 0)
        {
            txtDayCost.Text = dtdsupp.Rows[0]["ManDayCost"].ToString();
            txtStaffAllot.Text = dtdsupp.Rows[0]["StaffAllot"].ToString();
        }
        else
        {
            txtDayCost.Text = "0";
            txtStaffAllot.Text = "0";
        }

    }

    private void Load_Allotment_Details()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And DeptName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And DeptName='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Allotment Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Allotment Not Found');", true);
        }

        Load_Allotment_Details();
    }

    protected void btnCostSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (txtDayCost.Text == "" || txtDayCost.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Day cost');", true);
        }
        if (!ErrFlag)
        {
            if (SessionIsAdmin == "1")
            {
                query = "Select *from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedValue + "'";
            }
            else
            {
                query = "Select *from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            }
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                if (SessionIsAdmin == "1")
                {
                    query = "Delete from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedValue + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    query = "Delete from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }

            }
            SaveMode = "Insert";
            query = "Insert Into ManDayCost(CompCode,LocCode,ManDayCost,StaffAllot)";
            if (SessionIsAdmin == "1")
            {
                query = query + " Values ('" + SessionCcode + "','" + ddlUnit.SelectedValue + "','" + txtDayCost.Text + "','" + txtStaffAllot.Text + "')";
            }
            else
            {
                query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtDayCost.Text + "','" + txtStaffAllot.Text + "')";
            }
            objdata.RptEmployeeMultipleDetails(query);
        }
        if (!ErrFlag)
        {
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ManDayCost Saved Successfully');", true);
            }
        }

        ManDayCost_Details();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (txtLabourAllot.Text == "" || txtLabourAllot.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Labour Allotment');", true);
        }
        if (!ErrFlag)
        {
            if (SessionIsAdmin == "1")
            {
                query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedValue + "'";
                query = query + " And DeptName='" + ddlDepartment.SelectedValue + "'";
            }
            else
            {
                query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                query = query + " And DeptName='" + ddlDepartment.SelectedValue + "'";
            }
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                if (SessionIsAdmin == "1")
                {
                    query = "Delete from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedValue + "'";
                    query = query + " And DeptName='" + ddlDepartment.SelectedValue + "'";
                }
                else
                {
                    query = "Delete from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query = query + " And DeptName='" + ddlDepartment.SelectedValue + "'";
                }
                objdata.RptEmployeeMultipleDetails(query);

            }
            SaveMode = "Insert";
            query = "Insert Into Employee_Allotment_Mst_New(CompCode,LocCode,EmpType,Emp_Count,DeptName)";
            if (SessionIsAdmin == "1")
            {
                query = query + " Values ('" + SessionCcode + "','" + ddlUnit.SelectedValue + "','LABOUR','" + txtLabourAllot.Text + "','" + ddlDepartment.SelectedValue + "')";
            }
            else
            {
                query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','LABOUR','" + txtLabourAllot.Text + "','" + ddlDepartment.SelectedValue + "')";
            }
            objdata.RptEmployeeMultipleDetails(query);
        }
        if (!ErrFlag)
        {
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Allotment Saved Successfully');", true);
            }
        }

        Load_Allotment_Details();
        Clear_All_Field();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtLabourAllot.Text = "";
        ddlDepartment.SelectedValue = "-Select-";
        btnSave.Text = "Save";
    }

    protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string message = "";
        string SQL = "select * from Designation_Mst where DesignNo='" + ddlDesignation.SelectedValue + "'";
        if (SessionIsAdmin == "1")
        {
            SQL = SQL + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + ddlUnit.SelectedValue + "'";
        }
        else
        {
            SQL = SQL + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode + "'";
        }

        dt = objdata.RptEmployeeMultipleDetails(SQL);

        if (dt.Rows.Count != 0)
        {
            lblWorkCode.Text = dt.Rows[0]["WorkerCode"].ToString();
        }
        else
        {
            lblWorkCode.Text = "0";
        }
    }

    protected void txtGeneral_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }
    protected void txtGeneralCost_TextChanged(object sender, EventArgs e)
    {
        SumCost();
    }
    private void Sum()
    {
        txtTotal.Text = (Convert.ToDecimal(txtGeneral.Text) + Convert.ToDecimal(txtShift1.Text) + Convert.ToDecimal(txtShift2.Text) + Convert.ToDecimal(txtShift3.Text) + Convert.ToDecimal(txtShift4.Text) + Convert.ToDecimal(txtShift5.Text)).ToString();
    }
    private void SumCost()
    {
        txtCostTotal.Text = (Convert.ToDecimal(txtGeneralCost.Text) + Convert.ToDecimal(txtShift1Cost.Text) + Convert.ToDecimal(txtShift2Cost.Text) + Convert.ToDecimal(txtShift3Cost.Text) + Convert.ToDecimal(txtShift4Cost.Text) + Convert.ToDecimal(txtShift5Cost.Text)).ToString();
    }

    protected void txtShift1_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }
    protected void txtShift1Cost_TextChanged(object sender, EventArgs e)
    {
        SumCost();
    }

    protected void txtShift2_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }
    protected void txtShift2Cost_TextChanged(object sender, EventArgs e)
    {
        SumCost();
    }
    protected void txtShift3_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }
    protected void txtShift3Cost_TextChanged(object sender, EventArgs e)
    {
        SumCost();
    }
    protected void txtShift4_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }
    protected void txtShift5_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }
    protected void txtShift4Cost_TextChanged(object sender, EventArgs e)
    {
        SumCost();
    }
    protected void txtShift5Cost_TextChanged(object sender, EventArgs e)
    {
        SumCost();
    }
    protected void txtRequired_TextChanged(object sender, EventArgs e)
    {
        Sum();
    }

    protected void btnSave1_Click(object sender, EventArgs e)
    {
        string query = "";
        string message = "Success";
        DataTable Design_check = new DataTable();
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string DeptCode = "";
        string Designation = "";

        if (ddlDesignation.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Designation')", true);
            return;
        }
        if (!ErrFlag)
        {
            query = "select * from Designation_Mst where DesignNo='" + ddlDesignation.SelectedValue + "'";
            if (SessionIsAdmin == "1")
            {
                query = query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + ddlUnit.SelectedValue + "'";
            }
            else
            {
                query = query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode + "'";
            }
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                Designation = DT.Rows[0]["DesignName"].ToString();
                DeptCode = DT.Rows[0]["DeptCode"].ToString();
            }
            query = "select * from LabourAllotment_Mst where DesignNo='" + ddlDesignation.SelectedValue + "'";
            if (SessionIsAdmin == "1")
            {
                query = query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + ddlUnit.SelectedValue + "'";
            }
            else
            {
                query = query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode + "'";
            }
            Design_check = objdata.RptEmployeeMultipleDetails(query);

            if (Design_check.Rows.Count != 0)
            {
                query = "Delete from LabourAllotment_Mst where DesignNo='" + ddlDesignation.SelectedValue + "'";
                if (SessionIsAdmin == "1")
                {
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedValue + "'";
                }
                else
                {
                    query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                }
                objdata.RptEmployeeMultipleDetails(query);

                message = "Update";
            }

            //Inert into INWard_Sub
            query = "insert into LabourAllotment_Mst(DesignNo,DesignName,WorkerCode,GENERAL,";
            query = query + "SHIFT1,Shift1Cost,Shift2Cost,Shift3Cost,SHIFT2,SHIFT3,Total,CompCode,LocCode,DeptCode,Required,Cost,ESI,PF,Canteen,TeaSnacks,LrComm,LrNo,ESINo,PFNo,Weeknos,weekCost,traineenos,traineeCost,SHIFT4,SHIFT4COST,SHIFT5,SHIFT5COST)values";
            query = query + "('" + ddlDesignation.SelectedValue + "','" + ddlDesignation.SelectedItem.Text + "','" + lblWorkCode.Text + "','" + txtGeneral.Text + "',";
            query = query + "'" + txtShift1.Text + "','" + txtShift1Cost.Text + "','" + txtShift2Cost.Text + "','" + txtShift3Cost.Text + "','" + txtShift2.Text + "','" + txtShift3.Text + "','" + txtTotal.Text + "',";
            if (SessionIsAdmin == "1")
            {
                query = query + "'" + SessionCcode.ToString() + "',";
            }
            else
            {
                query = query + "'" + SessionCcode.ToString() + "',";
            }
            query = query + "'" + ddlUnit.SelectedValue + "','" + DeptCode + "','" + txtRequired.Text + "','" + txtGeneralCost.Text + "','" + txtEsi.Text + "','" + txtPF.Text + "','" + txtCanteen.Text + "','" + txtTeaSnacks.Text + "','" + txtLeaderComm.Text + "','" + txtLdrno.Text + "','" + txtESINo.Text + "','" + txtPFNo.Text + "','" + txtweeknos.Text + "','" + txtweekcost.Text + "','" + txttraineenos.Text + "','" + txttraineecost.Text + "','" + txtShift4.Text + "','" + txtShift4Cost.Text + "','" + txtShift5.Text + "','" + txtShift5Cost.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);
            btnClear1_Click(sender, e);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully');", true);
        }
    }

    protected void btnClear1_Click(object sender, EventArgs e)
    {
        ddlDesignation.ClearSelection();
        txtGeneral.Text = "0";
        txtShift1.Text = "0";
        txtShift2.Text = "0";
        txtShift3.Text = "0";
        lblWorkCode.Text = "0";
        txtTotal.Text = "0";
        txtRequired.Text = "0";
        btnSave.Text = "Save";
        txtGeneralCost.Text = "0";
        txtEsi.Text = "0";
        txtPF.Text = "0";
        txtCanteen.Text = "0";
        txtTeaSnacks.Text = "0";
        txtLeaderComm.Text = "0";
        txtLdrno.Text = "0";
        txtESINo.Text = "0";
        txtPFNo.Text = "0";
        txtShift1Cost.Text = "0";
        txtShift2Cost.Text = "0";
        txtShift3Cost.Text = "0";
        txtweekcost.Text = "0";
        txtweeknos.Text = "0";
        txttraineenos.Text = "0";
        txttraineecost.Text = "0";
        txtCostTotal.Text = "0";
        ddlUnit.ClearSelection();
        Load_New_Data();
    }
    protected void btnSaveWeek_Click(object sender, EventArgs e)
    {
        //string query = "";
        //string message = "Success";
        //DataTable Design_check = new DataTable();
        //bool ErrFlag = false;
        //DataTable DT = new DataTable();
        //string DeptCode = "";
        //string Designation = "";

       
        //if (!ErrFlag)
        //{
        //    //Inert into Weekly
        //    query = "insert into MstWeekly(Weekly,WeeklyCost,";
        //    query = query + " CompCode,LocCode)values";
        //    query = query + "('" + txtweek.Text + "','" + txtweekCost.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
        //   //// query = query + "'" + txtShift1.Text + "','" + txtShift2.Text + "','" + txtShift3.Text + "','" + txtTotal.Text + "',";
        //   // if (SessionIsAdmin == "1")
        //   // {
        //   //     query = query + "'" + SessionCcode.ToString() + "',";
        //   // }
        //   // else
        //   // {
        //   //     query = query + "'" + SessionCcode.ToString() + "',";
        //   // }
        //   // query = query + "'" + ddlUnit.SelectedValue + "','" + DeptCode + "','" + txtRequired.Text + "','" + txtGeneralCost.Text + "','" + txtEsi.Text + "','" + txtPF.Text + "','" + txtCanteen.Text + "','" + txtTeaSnacks.Text + "','" + txtLeaderComm.Text + "','" + txtLdrno.Text + "','" + txtESINo.Text + "','" + txtPFNo.Text + "')";
        //    objdata.RptEmployeeMultipleDetails(query);
        //    btnClearWeek_Click(sender, e);
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully');", true);
        //}
    }
    protected void btnClearWeek_Click(object sender, EventArgs e)
    {
        //txtweek.Text = "";
        //txtweekCost.Text = "";
    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from LabourAllotment_Mst where Auto_ID='" + e.CommandName.ToString() + "'";
        //SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedValue + "' ";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_New_Data();
    }

    protected void btnEdit_Command(object sender, CommandEventArgs e)
    {
        //var Dept = db_Entity.Department_Mst.ToList();
        DataTable dt = new DataTable();

        string SQL = "Select * from LabourAllotment_Mst where Auto_ID='" + e.CommandName.ToString() + "'";
        //  SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

        dt = objdata.RptEmployeeMultipleDetails(SQL);



        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlUnit.SelectedValue = dt.Rows[i]["LocCode"].ToString();
            ddlUnit_SelectedIndexChanged(sender, e);
            ddlDesignation.SelectedValue = dt.Rows[i]["DesignNo"].ToString();
            lblWorkCode.Text = dt.Rows[i]["WorkerCode"].ToString();
            txtGeneral.Text = dt.Rows[i]["GENERAL"].ToString();
            txtShift1.Text = dt.Rows[i]["SHIFT1"].ToString();
            txtShift2.Text = dt.Rows[i]["SHIFT2"].ToString();
            txtShift3.Text = dt.Rows[i]["SHIFT3"].ToString();
            txtTotal.Text = dt.Rows[i]["Total"].ToString();
            txtRequired.Text = dt.Rows[i]["Required"].ToString();
            txtGeneralCost.Text = dt.Rows[i]["Cost"].ToString();
            txtEsi.Text = dt.Rows[i]["ESI"].ToString();
            txtPF.Text = dt.Rows[i]["PF"].ToString();
            txtCanteen.Text = dt.Rows[i]["Canteen"].ToString();
            txtTeaSnacks.Text = dt.Rows[i]["TeaSnacks"].ToString();
            txtLeaderComm.Text = dt.Rows[i]["LrComm"].ToString();
            txtLdrno.Text = dt.Rows[i]["LrNo"].ToString();
            txtESINo.Text = dt.Rows[i]["ESINo"].ToString();
            txtPFNo.Text = dt.Rows[i]["PFNo"].ToString();
            txtShift4.Text = dt.Rows[i]["SHIFT4"].ToString();
            txtShift5.Text = dt.Rows[i]["SHIFT5"].ToString();
            txtShift1Cost.Text = dt.Rows[i]["SHIFT1COST"].ToString();
            txtShift2Cost.Text = dt.Rows[i]["SHIFT2COST"].ToString();
            txtShift3Cost.Text = dt.Rows[i]["SHIFT3COST"].ToString();
            txtShift4Cost.Text = dt.Rows[i]["SHIFT4COST"].ToString();
            txtShift5Cost.Text = dt.Rows[i]["SHIFT5COST"].ToString();
            txtweeknos.Text = dt.Rows[i]["WeekNos"].ToString();
            txtweekcost.Text = dt.Rows[i]["WeekCost"].ToString();
            txttraineenos.Text = dt.Rows[i]["TraineeNos"].ToString();
            txttraineecost.Text = dt.Rows[i]["TraineeCost"].ToString();
            txtCostTotal.Text = dt.Rows[i]["Total"].ToString();
            btnSave.Text = "Update";
            Load_New_Data();
        }
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Department();
        Load_Designation();
        Load_New_Data();
    }
}
