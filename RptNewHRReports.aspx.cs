﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;


public partial class RptNewHRReports : System.Web.UI.Page
{
    System.Web.UI.WebControls.DataGrid grid =
      new System.Web.UI.WebControls.DataGrid();
    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    int intK;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;
    string Agent = "";
    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable(); // mLocalDS
    DataTable DataCell = new DataTable();
    DateTime date1;
    DateTime date2;
    string FromDate = "";
    string ToDate = "";
    DateTime Date2 = new DateTime();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Report";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionDivision = Request.QueryString["Division"].ToString();
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED - DEPT GROUP WISE")
            {
                if (SessionAdmin == "2")
                {
                    NonAdminGetHRConsolidatesDeptGroupWise_Changes();
                }
                else
                {
                    GetHRConsolidatesDeptGroupWise();
                }
            }
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED - DEPT WISE")
            {
                if (SessionAdmin == "2")
                {
                    GetHRConsolidatesDeptWise();
                }
                else
                {
                    GetHRConsolidatesDeptWise();
                }
            }
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED - GROUP WISE")
            {
                if (SessionAdmin == "2")
                {
                    GetHRConsolidatesGroupWise();
                }
                else
                {
                    GetHRConsolidatesGroupWise();
                }
            }
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED REPORT")
            {
                FromDate = Request.QueryString["FromDate"];
                hrreportConsolidate(FromDate);
            }
            if (Request.QueryString["RptName"].ToString() == "NEW_REL")
            {
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                hrreportNewReliveConsolidate(FromDate, ToDate);
            }
            if (Request.QueryString["RptName"].ToString() == "SOURCE GRADE DEPT WISE")
            {
                GetHRConsolidatesDeptWiseSource();
            }
            if (Request.QueryString["RptName"].ToString() == "LEADER COMMISSION")
            {
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                Agent = Request.QueryString["Agent"].ToString();
                GetLeaderCommission(Agent, FromDate, ToDate);
            }
            if (Request.QueryString["RptName"].ToString() == "HR TRACER ALL")
            {
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                GetHRTracer_All(FromDate, ToDate, DateTime.Now.ToString("dd/MM/yyyy"));
            }
            if (Request.QueryString["RptName"].ToString() == "LEADER GRADE")
            {
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                Wages_Type = Request.QueryString["Wages"].ToString();
                LeaderGradeDeptReportBTDates(Wages_Type, FromDate, ToDate);
            }
            if (Request.QueryString["RptName"].ToString() == "ON OFF DUTY REPORT")
            {
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                Wages_Type = Request.QueryString["Wages"].ToString();
                All_LeaderONOFFReport(Wages_Type, FromDate, ToDate);
            }
        }
    }
    public void GetHRConsolidatesDeptWiseSource()
    {
        if (SessionUserType == "2")
        {
            NonAdminGetHRConsolidatesDeptWise_Changes();
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable AgentMst = new DataTable();
            DataTable NonActive = new DataTable();
            DataTable GradeMst = new DataTable();

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("Allot");
            AutoDTable.Columns.Add("T");
            AutoDTable.Columns.Add("T+");
            AutoDTable.Columns.Add("A");
            AutoDTable.Columns.Add("A+");
            AutoDTable.Columns.Add("A++");
            AutoDTable.Columns.Add("Total");
            AutoDTable.Columns.Add("AgentID");

            SSQL = " Select AgentID,AgentName from AgentMst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            AgentMst = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int j = 0; j < AgentMst.Rows.Count; j++)
            {
                SSQL = "select DeptName,DeptCode,'0' as Allot_Total,SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,";
                SSQL = SSQL + " (SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total from (select DM.DeptName,DM.DeptCode,CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
                SSQL = SSQL + " CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
                SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five";
                SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
                SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " and GM.CompCode='" + SessionCcode.ToString() + "' And GM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " and DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'";
                SSQL = SSQL + " and EM.AgentName='" + AgentMst.Rows[j]["AgentName"].ToString() + "'";
                SSQL = SSQL + " group by DM.DeptName,GM.GradeName,DM.DeptCode ) as PPV group by DeptName,DeptCode Order by DeptCode";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["DeptName"].ToString();

                    SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "'";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Allot"] = dt1.Rows[0]["Allot"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt.Rows[i]["One"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt.Rows[i]["Two"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt.Rows[i]["Three"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt.Rows[i]["Four"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt.Rows[i]["Five"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = dt.Rows[i]["Final_Total"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AgentID"] = AgentMst.Rows[j]["AgentID"].ToString();

                }
            }

            int TotCount = AutoDTable.Rows.Count;

            double Sum = 0;
            double Sum1 = 0;
            double Sum2 = 0;
            double Sum3 = 0;
            double Sum4 = 0;
            double Sum5 = 0;
            double Sum6 = 0;
            double Sum7 = 0;

            for (int m = 0; m < AutoDTable.Rows.Count; m++)
            {
                Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
                Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
                Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
                Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
                Sum4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
                Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
                Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
            }
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[TotCount]["DEPT"] = "Total";
            AutoDTable.Rows[TotCount]["Allot"] = Sum;
            AutoDTable.Rows[TotCount]["T"] = Sum1;
            AutoDTable.Rows[TotCount]["T+"] = Sum2;
            AutoDTable.Rows[TotCount]["A"] = Sum3;
            AutoDTable.Rows[TotCount]["A+"] = Sum4;
            AutoDTable.Rows[TotCount]["A++"] = Sum5;
            AutoDTable.Rows[TotCount]["Total"] = Sum6;
            if (AutoDTable.Rows.Count > 0)
            {

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=HR CONSOLIDATED - SOURCE GRADE DEPT WISE.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='16'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='16'>");
                Response.Write("<a style=\"font-weight:bold\"> HR CONSOLIDATED - DEPT WISE REPORT &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
                Response.Write("</td>");

                //Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">Allot</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">T</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">T+</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A+</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A++</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2' >");
                Response.Write("<a style=\"font-weight:bold\">Total</a>");

                Response.Write("</td>");
                Response.Write("</tr>");

                for (int M = 0; M < AgentMst.Rows.Count; M++)
                {
                    string DD = AgentMst.Rows[M]["AgentID"].ToString();

                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='16'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AgentMst.Rows[M]["AgentName"].ToString() + " </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");

                    for (int k = 0; k < AutoDTable.Rows.Count; k++)
                    {
                        string DD1 = AutoDTable.Rows[k]["AgentID"].ToString();

                        if (DD == DD1)
                        {

                            Response.Write("<tr Font-Bold='true' align='center'>");
                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
                            Response.Write("</td>");

                            //Response.Write("<tr Font-Bold='true' align='center'>");
                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Allot"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++"].ToString() + " </a>");
                            Response.Write("</td>");


                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
                            Response.Write("</td>");
                            Response.Write("</tr>");


                        }
                    }


                }
                Response.Write("</table>");

                // Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!');", true);
            }


        }

    }
    public void GetLeaderCommission(string BrokerName, string FromDate, string ToDate)
    {
        if (SessionUserType == "2")
        {
            NonAdminGetLeaderCommission_Changes(BrokerName, FromDate, ToDate);
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();


            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("Grade");
            AutoDTable.Columns.Add("Group");
            AutoDTable.Columns.Add("Days");
            AutoDTable.Columns.Add("Amount");
            AutoDTable.Columns.Add("AgentID");
            AutoDTable.Columns.Add("AgentName");


            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                //AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            SSQL = "Select AM.AgentID,AM.AgentName,EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(MC.CateName,'') as WageCategoty,EM.AgentName,MG.GradeName from Employee_Mst EM";
            SSQL = SSQL + " inner join AgentMst AM on EM.AgentID = AM.AgentID and EM.LocCode =AM.LocCode";
            SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID and EM.LocCode =MC.LocCode inner join MstGrade MG on EM.Grade = MG.GradeID and EM.LocCode =MG.LocCode";
            SSQL = SSQL + " where (EM.IsActive='Yes' or CONVERT(DATETIME,EM.RelieveDate, 103)>=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)) And (EM.AgentID !='0' or EM.AgentID!='-Select-') And EM.WageCategoty !='0'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And MC.CompCode='" + SessionCcode.ToString() + "' And MC.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And MG.CompCode='" + SessionCcode.ToString() + "' And MG.LocCode='" + SessionLcode.ToString() + "'";

            if (BrokerName != "0")
            {
                SSQL = SSQL + " And EM.AgentID= '" + BrokerName + "'";
            }

            SSQL = SSQL + " And EM.EligibleLeader='1'";
            SSQL = SSQL + "Group By AM.AgentID,AM.AgentName,EM.EmpNo,EM.FirstName,MC.CateName,EM.AgentName,MG.GradeName order by AM.AgentName";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            int TotCount;

            int IntK = 0;
            int SNo = 1;
            double Total;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                string MachineID = dt.Rows[i]["EmpNo"].ToString();

                AutoDTable.Rows[intK]["SNo"] = SNo;
                AutoDTable.Rows[intK]["EmpNo"] = dt.Rows[i]["EmpNo"].ToString();
                AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[intK]["Grade"] = dt.Rows[i]["GradeName"].ToString();
                AutoDTable.Rows[intK]["Group"] = dt.Rows[i]["WageCategoty"].ToString();
                AutoDTable.Rows[intK]["AgentID"] = dt.Rows[i]["AgentID"].ToString();
                AutoDTable.Rows[intK]["AgentName"] = dt.Rows[i]["AgentName"].ToString();


                int count = 4;
                double Total_Time_get = 0;
                double Total_Hours = 0;
                double Total_Days = 0;
                double TwelveHours = 0;
                double EightHours = 0;
                double FinalTotal = 0;

                string Temp_Total_Time = "";
                string[] Temp_Slipt;


                for (int j = 0; j < daysAdded; j++)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                    string Date1 = dayy.ToString("yyyy/MM/dd");

                    SSQL = "select Total_Hrs1 from LogTime_Days where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date,103) = CONVERT(datetime,'" + Convert.ToDateTime(dayy).ToString("dd/MM/yyyy") + "',103)";

                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt1.Rows.Count > 0)
                    {
                        //Total_Time_get = Convert.ToDouble(dt1.Rows[0]["Total_Hrs1"].ToString());

                        Temp_Total_Time = dt1.Rows[0]["Total_Hrs1"].ToString();
                        Temp_Slipt = Temp_Total_Time.Split(':');

                        if (Convert.ToDecimal(Temp_Slipt[1]) >= Convert.ToDecimal(45))
                        {
                            Total_Time_get = Convert.ToDouble(1) + Convert.ToDouble(Temp_Slipt[0]);
                        }
                        else
                        {
                            Total_Time_get = Convert.ToDouble(Temp_Slipt[0]);
                        }


                        Total_Hours = Total_Hours + Total_Time_get;

                        if (Total_Time_get >= 12)
                        {
                            SSQL = "Select Shift12 from AgentMst where AgentID='" + dt.Rows[i]["AgentID"].ToString() + "' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

                            TwelveHours = TwelveHours + Convert.ToDouble(dt2.Rows[0]["Shift12"].ToString());

                            Total_Days = Total_Days + 1;
                        }
                        else if (Total_Time_get >= 8 & Total_Time_get < 12)
                        {
                            SSQL = "Select Shift8 from AgentMst where AgentID='" + dt.Rows[i]["AgentID"].ToString() + "' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

                            EightHours = EightHours + Convert.ToDouble(dt3.Rows[0]["Shift8"].ToString());

                            Total_Days = Total_Days + 1;
                        }
                        else
                        {

                        }


                    }
                    else
                    {
                        Total_Hours = Total_Hours + Total_Time_get;

                    }


                    Total_Time_get = 0;
                }

                FinalTotal = EightHours + TwelveHours;

                AutoDTable.Rows[intK]["Days"] = Total_Days;
                AutoDTable.Rows[intK]["Amount"] = FinalTotal;

                intK = intK + 1;

                SNo = SNo + 1;

                Total_Days = 0;
                EightHours = 0;
                TwelveHours = 0;
            }


            SSQL = " Select distinct AM.AgentID,AM.AgentName from Employee_Mst EM inner join AgentMst AM on EM.AgentID=AM.AgentID ";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And EM.IsActive='Yes' And EM.EligibleLeader='1'";

            if (BrokerName != "0")
            {
                SSQL = SSQL + " And AM.AgentID ='" + BrokerName + "' ";
            }

            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);



            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=LEADER COMMISSION REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\">LEADER COMMISSION REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\"> From Date:" + FromDate + "- To Date:" + ToDate + "</a>");
            Response.Write("</td>");

            Response.Write("</tr>");



            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">SNo</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">EmpNo</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">EmpName</a>");
            Response.Write("</td>");

            //Grade

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Grade</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Group</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Days</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Amount</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            //Response.Write("<tr Font-Bold='true' align='center'>");

            if (dt1.Rows.Count > 0)
            {

                for (int l = 0; l < dt1.Rows.Count; l++)
                {
                    string DD = dt1.Rows[l]["AgentID"].ToString();

                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='14'>");
                    Response.Write("<a style=\"font-weight:bold\">" + dt1.Rows[l]["AgentName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");
                    decimal Sum = 0;
                    decimal Sum1 = 0;

                    for (int k = 0; k < AutoDTable.Rows.Count; k++)
                    {
                        string DD1 = AutoDTable.Rows[k]["AgentID"].ToString();

                        if (DD == DD1)
                        {

                            Response.Write("<tr Font-Bold='true' align='center'>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["SNo"].ToString() + "</a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["EmpNo"].ToString() + " </a>");
                            Response.Write("</td>");


                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Name"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Grade"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Group"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Days"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("<td colspan='2'>");
                            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Amount"].ToString() + " </a>");
                            Response.Write("</td>");

                            Response.Write("</tr>");

                            Sum = Sum + Convert.ToDecimal(AutoDTable.Rows[k]["Days"].ToString());
                            Sum1 = Sum1 + Convert.ToDecimal(AutoDTable.Rows[k]["Amount"].ToString());

                        }
                    }

                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\"></a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\"> </a>");
                    Response.Write("</td>");


                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\"></a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\"></a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">Grand Total</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + Sum + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\"> " + Sum1 + "</a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");
                    Sum = 0;
                    Sum1 = 0;
                }
            }
            else
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='14'>");
                Response.Write("<a style=\"font-weight:bold\">No Data Found</a>");
                Response.Write("</td>");

                Response.Write("</tr>");

            }



            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }

    }


    public void GetHRConsolidatesDeptWise()
    {
        if (SessionUserType == "2")
        {
            NonAdminGetHRConsolidatesDeptWise_Changes();
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable NonActive = new DataTable();
            DataTable GradeMst = new DataTable();

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("Allot");
            AutoDTable.Columns.Add("T");
            AutoDTable.Columns.Add("T+");
            AutoDTable.Columns.Add("A");
            AutoDTable.Columns.Add("A+");
            AutoDTable.Columns.Add("A++");
            AutoDTable.Columns.Add("A+*");
            AutoDTable.Columns.Add("Total");

            SSQL = "select DeptName,DeptCode,'0' as Allot_Total,SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,SUM(Six) as Six,";
            SSQL = SSQL + " (SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)+SUM(Six)) as Final_Total from (select DM.DeptName,DM.DeptCode,DM.ShortCode,CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
            SSQL = SSQL + " CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
            SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five,";
            SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+*' THEN count(EM.EmpNo) else 0 END AS Six";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " and GM.CompCode='" + SessionCcode.ToString() + "' And GM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " and DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'";
            SSQL = SSQL + " group by DM.DeptName,GM.GradeName,DM.DeptCode,DM.ShortCode ) as PPV group by DeptName,DeptCode,ShortCode Order by ShortCode Asc";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["DeptName"].ToString();

                    SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "'";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Allot"] = dt1.Rows[0]["Allot"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt.Rows[i]["One"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt.Rows[i]["Two"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt.Rows[i]["Three"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt.Rows[i]["Four"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt.Rows[i]["Five"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+*"] = dt.Rows[i]["Six"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = dt.Rows[i]["Final_Total"].ToString();

                }

                int TotCount = AutoDTable.Rows.Count;

                double Sum = 0;
                double Sum1 = 0;
                double Sum2 = 0;
                double Sum3 = 0;
                double Sum4 = 0;
                double Sum5 = 0;
                double Sum6 = 0;
                double Sum7 = 0;

                for (int m = 0; m < AutoDTable.Rows.Count; m++)
                {
                    Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
                    Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
                    Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
                    Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
                    Sum4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
                    Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
                    Sum7 += Convert.ToDouble(AutoDTable.Rows[m]["A+*"].ToString());
                    Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
                }
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[TotCount]["DEPT"] = "Total";
                AutoDTable.Rows[TotCount]["Allot"] = Sum;
                AutoDTable.Rows[TotCount]["T"] = Sum1;
                AutoDTable.Rows[TotCount]["T+"] = Sum2;
                AutoDTable.Rows[TotCount]["A"] = Sum3;
                AutoDTable.Rows[TotCount]["A+"] = Sum4;
                AutoDTable.Rows[TotCount]["A++"] = Sum5;
                AutoDTable.Rows[TotCount]["A+*"] = Sum7;
                AutoDTable.Rows[TotCount]["Total"] = Sum6;

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=HR CONSOLIDATED - DEPT WISE REPORT.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='18'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='18'>");
                Response.Write("<a style=\"font-weight:bold\"> HR CONSOLIDATED - DEPT WISE REPORT &nbsp;&nbsp;&nbsp;</a>");

                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
                Response.Write("</td>");

                //Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">Allot</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">T</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">T+</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A+</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A++</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">A+*</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2' >");
                Response.Write("<a style=\"font-weight:bold\">Total</a>");

                Response.Write("</td>");
                Response.Write("</tr>");

                for (int k = 0; k < AutoDTable.Rows.Count; k++)
                {

                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
                    Response.Write("</td>");

                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Allot"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+*"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");


                }

                Response.Write("</table>");

                // Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!')", true);
            }

        }

    }
    public void GetHRConsolidatesDeptGroupWise()
    {
        if (SessionUserType == "2")
        {
            NonAdminGetHRConsolidatesDeptGroupWise_Changes();
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable LabourAllot = new DataTable();
            DataTable GradeMst = new DataTable();

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("Allot");

            SSQL = "Select AM.AgentID,AM.AgentName from Employee_Mst EM inner join AgentMst AM on EM.AgentName = AM.AgentName  And AM.LocCode = EM.LocCode  ";
            SSQL = SSQL + " where EM.IsActive='Yes' And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And EM.AgentName !='' Group By  AM.AgentID,AM.AgentName Order by AM.AgentID Asc";

            GradeMst = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i1 = 0; i1 < GradeMst.Rows.Count; i1++)
            {
                AutoDTable.Columns.Add(GradeMst.Rows[i1]["AgentName"].ToString());

            }

            AutoDTable.Columns.Add("Total");

            SSQL = "Select DM.DeptCode,DM.DeptName from Employee_Mst EM inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignName";
            SSQL = SSQL + " Inner Join Department_Mst DM on EM.DeptCode = DM.DeptCode And EM.LocCode = DM.LocCode And LM.LocCode = EM.LocCode";
            SSQL = SSQL + " Where EM.IsActive='Yes' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And LM.CompCode='" + SessionCcode.ToString() + "' And LM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " Group By  DM.DeptCode,DM.DeptName,DM.ShortCode Order by DM.ShortCode Asc ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            int TotCount;

            int IntK = 0;
            double Total;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    AutoDTable.Rows[IntK]["DEPT"] = dt.Rows[i]["DeptName"].ToString();

                    SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "'";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    AutoDTable.Rows[IntK]["Allot"] = dt1.Rows[0]["Allot"].ToString();
                    Total = 0;

                    double Total1 = 0;

                    int Count = 2;

                    for (int i1 = 0; i1 < GradeMst.Rows.Count; i1++)
                    {
                        SSQL = "select COUNT(EM.EmpNo) As EmpNo from Employee_Mst EM inner join  MstGrade MG  on EM.Grade= MG.GradeID And EM.CompCode = MG.CompCode And MG.LocCode = EM.LocCode";
                        SSQL = SSQL + " Where  EM.DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "' And EM.AgentName='" + GradeMst.Rows[i1]["AgentName"].ToString() + "'";
                        SSQL = SSQL + " And EM.CompCode ='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And MG.CompCode ='" + SessionCcode.ToString() + "' And MG.LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And EM.IsActive='Yes'";
                        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt1.Rows.Count >= 0)
                        {
                            Total1 = Convert.ToDouble(dt1.Rows[0]["EmpNo"].ToString());

                            if (Total1 != 0)
                            {
                                AutoDTable.Rows[IntK][Count] = dt1.Rows[0]["EmpNo"].ToString();
                            }
                            else
                            {
                                AutoDTable.Rows[IntK][Count] = "0";

                            }
                        }
                        else
                        {
                            AutoDTable.Rows[IntK][Count] = "0";

                        }

                        Total = Total + Total1;
                        Count = Count + 1;

                    }

                    AutoDTable.Rows[IntK]["Total"] = Total;
                    IntK = IntK + 1;



                }

                double Sum = 0;
                double Sum1 = 0;
                double Sum2 = 0;

                TotCount = AutoDTable.Rows.Count;
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[TotCount]["DEPT"] = "Total";

                for (int Col = 0; Col < GradeMst.Rows.Count; Col++)
                {
                    string SS = GradeMst.Rows[Col]["AgentName"].ToString();
                    for (int j = 0; j < AutoDTable.Rows.Count - 1; j++)
                    {
                        Sum += Convert.ToDouble(AutoDTable.Rows[j][SS].ToString());
                        Sum1 += Convert.ToDouble(AutoDTable.Rows[j]["Total"].ToString());
                        Sum2 += Convert.ToDouble(AutoDTable.Rows[j]["Allot"].ToString());
                    }

                    AutoDTable.Rows[TotCount]["Allot"] = Sum2;
                    AutoDTable.Rows[TotCount][GradeMst.Rows[Col]["AgentName"].ToString()] = Sum;
                    AutoDTable.Rows[TotCount]["Total"] = Sum1;
                    Sum = 0;
                    Sum1 = 0;
                    Sum2 = 0;
                }


                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=HRConsolidates_Dept_Group_Wise.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
                Response.Write(" &nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">HR CONSOLIDATED - DEPT GROUP WISE</a>");
                Response.Write(" &nbsp;&nbsp;&nbsp; ");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!')", true);
            }

        }

    }
    public void GetHRConsolidatesGroupWise()
    {
        if (SessionUserType == "2")
        {
            NonAdminGetHRConsolidatesGroupWise_Changes();
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable NonActive = new DataTable();
            DataTable GradeMst = new DataTable();

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("Allot");
            AutoDTable.Columns.Add("T");
            AutoDTable.Columns.Add("T+");
            AutoDTable.Columns.Add("A");
            AutoDTable.Columns.Add("A+");
            AutoDTable.Columns.Add("A++");
            AutoDTable.Columns.Add("A+*");
            AutoDTable.Columns.Add("Total");
            try
            {
                SSQL = "select AgentName,AgentID,SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,SUM(Six) as Six,";
                SSQL = SSQL + " (SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)+SUM(Six)) as Final_Total from (select DM.AgentName,DM.AgentID,CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
                SSQL = SSQL + " CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
                SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five,";
                SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+*' THEN count(EM.EmpNo) else 0 END AS Six";
                SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode inner join AgentMst DM on DM.AgentName=EM.AgentName and DM.LocCode=EM.LocCode";
                SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " and GM.CompCode='" + SessionCcode.ToString() + "' And GM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " and DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'";
                SSQL = SSQL + " group by DM.AgentName,GM.GradeName,DM.AgentID ) as PPV group by AgentName,AgentID Order by AgentID";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["AgentName"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt.Rows[i]["One"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt.Rows[i]["Two"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt.Rows[i]["Three"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt.Rows[i]["Four"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt.Rows[i]["Five"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+*"] = dt.Rows[i]["Six"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = dt.Rows[i]["Final_Total"].ToString();

                    }

                    int TotCount = AutoDTable.Rows.Count;

                    double Sum = 0;
                    double Sum1 = 0;
                    double Sum2 = 0;
                    double Sum3 = 0;
                    double Sum4 = 0;
                    double Sum5 = 0;
                    double Sum6 = 0;
                    double Sum7 = 0;

                    for (int m = 0; m < AutoDTable.Rows.Count; m++)
                    {
                        //Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
                        Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
                        Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
                        Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
                        Sum4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
                        Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
                        Sum7 += Convert.ToDouble(AutoDTable.Rows[m]["A+*"].ToString());
                        Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
                    }
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[TotCount]["DEPT"] = "Total";
                    //AutoDTable.Rows[TotCount]["Allot"] = Sum;
                    AutoDTable.Rows[TotCount]["T"] = Sum1;
                    AutoDTable.Rows[TotCount]["T+"] = Sum2;
                    AutoDTable.Rows[TotCount]["A"] = Sum3;
                    AutoDTable.Rows[TotCount]["A+"] = Sum4;
                    AutoDTable.Rows[TotCount]["A++"] = Sum5;
                    AutoDTable.Rows[TotCount]["A+*"] = Sum7;
                    AutoDTable.Rows[TotCount]["Total"] = Sum6;



                    grid.DataSource = AutoDTable;
                    grid.DataBind();
                    string attachment = "attachment;filename=HR CONSOLIDATED - GROUP WISE REPORT.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    grid.RenderControl(htextw);
                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='16'>");
                    Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
                    Response.Write("--");
                    Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='16'>");
                    Response.Write("<a style=\"font-weight:bold\"> HR CONSOLIDATED - GROUP WISE REPORT &nbsp;&nbsp;&nbsp;</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");

                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">T</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">T+</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">A</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">A+</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">A++</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2'>");
                    Response.Write("<a style=\"font-weight:bold\">A+*</a>");
                    Response.Write("</td>");

                    Response.Write("<td colspan='2' >");
                    Response.Write("<a style=\"font-weight:bold\">Total</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");

                    for (int k = 0; k < AutoDTable.Rows.Count; k++)
                    {

                        Response.Write("<tr Font-Bold='true' align='center'>");
                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+*"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
                        Response.Write("</td>");
                        Response.Write("</tr>");


                    }

                    Response.Write("</table>");

                    // Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!')", true);
                }
            }catch(Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Error try Again')", true);
            }

        }

    }
    public void hrreportConsolidate(string FromDate)
    {

        if (SessionUserType == "2")
        {
            NonAdminhrreportConsolidate(FromDate);
        }
        else
        {
            DataTable dt_Dept = new DataTable();
            DataTable dt_TempOpen = new DataTable();
            DataTable dt_TempJoin = new DataTable();
            DataTable dt_Relive = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            string count = "0";
            string add = "0";
            string sub = "0";
            string finaltotal = "0";
            string DeptCode = "";
            string LabCount = "0";


            string TempOpen = "", TempJoin = "", TempRelive = "";
            string TempFinalTotal = "";

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("ALLOT");
            AutoDTable.Columns.Add("OPG");
            AutoDTable.Columns.Add("REL");
            AutoDTable.Columns.Add("ADD");
            AutoDTable.Columns.Add("TOTAL");

            DateTime myDate = DateTime.Parse(FromDate);
            DateTime dateForButton = myDate.AddDays(-1);
            string TempDate = dateForButton.ToString("dd/MM/yyyy");

            SSQL = "";
            SSQL = SSQL + "select * from Department_Mst where LocCode='" + SessionLcode.ToString() + "' order by DeptCode asc";
            dt_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < dt_Dept.Rows.Count; i++)
            {
                count = "0";
                add = "0";
                sub = "0";
                finaltotal = "0";

                SSQL = "";
                SSQL = SSQL + "select COUNT(*) as OpenTol from Employee_Mst where LocCode='" + SessionLcode.ToString() + "' and CONVERT(datetime,DOJ,103)<=CONVERT(datetime,'" + TempDate + "',103)  and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                dt_TempOpen = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_TempOpen.Rows.Count > 0) { TempOpen = dt_TempOpen.Rows[0]["OpenTol"].ToString(); } else { TempOpen = "0"; }

                //SSQL = "";
                //SSQL = SSQL + "select COUNT(*) as JoinTol from Employee_Mst where LocCode='" + SessionLcode.ToString() + "'  and IsActive='Yes' and CONVERT(datetime,DOJ,103)=CONVERT(datetime,'" + TempDate + "',103) and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                //dt_TempJoin = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_TempJoin.Rows.Count > 0) { TempJoin = dt_TempJoin.Rows[0]["JoinTol"].ToString(); } else { TempJoin = "0"; }

                SSQL = "";
                SSQL = SSQL + "select COUNT(*) as ReliveOpen from Employee_Mst where LocCode='" + SessionLcode.ToString() + "'  and IsActive='No' and CONVERT(datetime,DOR,103)<=CONVERT(datetime,'" + TempDate + "',103) and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                dt_Relive = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Relive.Rows.Count > 0) { TempRelive = dt_Relive.Rows[0]["ReliveOpen"].ToString(); } else { TempRelive = "0"; }

                SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt1.Rows.Count > 0) { LabCount = dt1.Rows[0]["Allot"].ToString(); }
                else { LabCount = "0"; }

                SSQL = "";
                SSQL = SSQL + "Select COUNT(*) as New_Join from Employee_Mst where IsActive='Yes' and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'and CompCode='" + SessionCcode.ToString() + "' ";
                SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,DOJ, 103)=CONVERT(Datetime,'" + FromDate + "',103) ";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt2.Rows.Count > 0) { add = dt2.Rows[0]["New_Join"].ToString(); }
                else { add = "0"; }

                SSQL = "Select count(RelieveDate)as RelieveDate  from Employee_Mst   where IsActive='No' and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'and CompCode='" + SessionCcode.ToString() + "' ";
                SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,RelieveDate, 103)=CONVERT(Datetime,'" + FromDate + "',103) ";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0) { sub = dt3.Rows[0]["RelieveDate"].ToString(); }
                else { sub = "0"; }


                TempFinalTotal = (Convert.ToDecimal(TempOpen) - Convert.ToDecimal(TempRelive)).ToString();
                finaltotal = ((Convert.ToDecimal(TempFinalTotal) + Convert.ToDecimal(add)) - Convert.ToDecimal(sub)).ToString();

                //finaltotal = count + add - sub;

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt_Dept.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = TempFinalTotal;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = LabCount;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = add;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = sub;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TOTAL"] = finaltotal;



            }


            // SSQL = "";
            // SSQL = SSQL + "select DeptName,DeptCode,'0' as Allot_Total,'0' as Additional,'0' as Relive,";
            // SSQL = SSQL + "(SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total ";
            // SSQL = SSQL + "from ( select DM.DeptName,DM.DeptCode,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five ";
            // SSQL = SSQL + "from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            // SSQL = SSQL + "inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode ";
            // SSQL = SSQL + "where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'  and ";
            // SSQL = SSQL + "CONVERT(datetime,DOJ,103)<CONVERT(datetime,'" + FromDate + "',103) ";
            // SSQL = SSQL + "group by DM.DeptName,GM.GradeName,DM.DeptCode ";
            // SSQL = SSQL + ") as PPV group by DeptName,DeptCode ";
            //// SSQL = " Select DeptCode,DeptName from Department_Mst  where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' order by DeptCode ";
            // dt = objdata.RptEmployeeMultipleDetails(SSQL);

            // if (dt.Rows.Count != 0)
            // {
            //     for (int k = 0; k < dt.Rows.Count; k++)
            //     {






            //     }
            // }

            double Sum = 0;
            double Opg = 0;
            double Allot = 0;
            double Add = 0;
            double Rel = 0;

            int TotCount = 0;

            TotCount = AutoDTable.Rows.Count;
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[TotCount]["DEPT"] = "Total";

            for (int j = 0; j < AutoDTable.Rows.Count - 1; j++)
            {
                //AutoDTable.Columns.Add("A1 No's");
                //AutoDTable.Columns.Add("A1 Days");
                //AutoDTable.Columns.Add("A1 Amt");

                Sum += Convert.ToDouble(AutoDTable.Rows[j]["TOTAL"].ToString());
                Opg += Convert.ToDouble(AutoDTable.Rows[j]["OPG"].ToString());
                Allot += Convert.ToDouble(AutoDTable.Rows[j]["ALLOT"].ToString());
                Add += Convert.ToDouble(AutoDTable.Rows[j]["ADD"].ToString());
                Rel += Convert.ToDouble(AutoDTable.Rows[j]["REL"].ToString());

                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = TempFinalTotal;
                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = LabCount;
                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = add;
                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = sub;


            }

            AutoDTable.Rows[TotCount]["TOTAL"] = Sum;
            AutoDTable.Rows[TotCount]["OPG"] = Opg;
            AutoDTable.Rows[TotCount]["ALLOT"] = Allot;
            AutoDTable.Rows[TotCount]["ADD"] = Add;
            AutoDTable.Rows[TotCount]["REL"] = Rel;

            Sum = 0;
            Opg = 0;
            Allot = 0;
            Add = 0;
            Rel = 0;
            DataCell.Columns.Add("S.NO");
            DataCell.Columns.Add("BrokerName");
            DataCell.Columns.Add("DeptName");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("GradeName");
            DataCell.Columns.Add("DOJ");
            DataCell.Columns.Add("RejoinDate");
            DataCell.Columns.Add("RelieveDate");
            DataCell.Columns.Add("LeaveFrom1");
            DataCell.Columns.Add("LeaveTo1");
            DataCell.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),EM.RejoinDate,103) as RejoinDate,CONVERT(varchar(10),EM.RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),EM.LeaveFrom,103) as LeaveFrom1,CONVERT(varchar(10),EM.LeaveTo,103) as LeaveTo1,EM.Reason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='Yes' ";
            SSQL = SSQL + " and CONVERT(datetime,DOJ,103)=CONVERT(datetime,'" + FromDate + "',103) order by RelieveDate Asc";

            dt4 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt4.Rows.Count > 0)
            {
                int srno = 1;
                for (int d = 0; d < dt4.Rows.Count; d++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[d]["S.NO"] = srno;
                    DataCell.Rows[d]["BrokerName"] = dt4.Rows[d]["AgentName"].ToString();
                    DataCell.Rows[d]["DeptName"] = dt4.Rows[d]["DeptName"].ToString();
                    DataCell.Rows[d]["MachineID"] = dt4.Rows[d]["EmpNo"].ToString();
                    DataCell.Rows[d]["Name"] = dt4.Rows[d]["FirstName"].ToString();
                    DataCell.Rows[d]["GradeName"] = dt4.Rows[d]["GradeName"].ToString();
                    DataCell.Rows[d]["DOJ"] = dt4.Rows[d]["DOJ"].ToString();
                    DataCell.Rows[d]["RejoinDate"] = dt4.Rows[d]["RejoinDate"].ToString();
                    DataCell.Rows[d]["RelieveDate"] = dt4.Rows[d]["RelieveDate"].ToString();
                    DataCell.Rows[d]["LeaveFrom1"] = dt4.Rows[d]["LeaveFrom1"].ToString();
                    DataCell.Rows[d]["LeaveTo1"] = dt4.Rows[d]["LeaveTo1"].ToString();
                    DataCell.Rows[d]["ReliveReason"] = dt4.Rows[d]["Reason"].ToString();
                    srno = srno + 1;

                }
            }

            DataCells.Columns.Add("S.NO");
            DataCells.Columns.Add("BrokerName");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("GradeName");
            DataCells.Columns.Add("DOJ");
            DataCells.Columns.Add("RejoinDate");
            DataCells.Columns.Add("RelieveDate");
            DataCells.Columns.Add("LeaveFrom1");
            DataCells.Columns.Add("LeaveTo1");
            DataCells.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo,103) as LeaveTo1,EM.Reason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='No' ";
            SSQL = SSQL + " and CONVERT(datetime,RelieveDate,103)=CONVERT(datetime,'" + FromDate + "',103) order by RelieveDate Asc";

            dt5 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt5.Rows.Count > 0)
            {
                int srnoo = 1;
                for (int d = 0; d < dt5.Rows.Count; d++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[d]["S.NO"] = srnoo;
                    DataCells.Rows[d]["BrokerName"] = dt5.Rows[d]["AgentName"].ToString();
                    DataCells.Rows[d]["DeptName"] = dt5.Rows[d]["DeptName"].ToString();
                    DataCells.Rows[d]["MachineID"] = dt5.Rows[d]["EmpNo"].ToString();
                    DataCells.Rows[d]["Name"] = dt5.Rows[d]["FirstName"].ToString();
                    DataCells.Rows[d]["GradeName"] = dt5.Rows[d]["GradeName"].ToString();
                    DataCells.Rows[d]["DOJ"] = dt5.Rows[d]["DOJ"].ToString();
                    DataCells.Rows[d]["RejoinDate"] = dt5.Rows[d]["RejoinDate"].ToString();
                    DataCells.Rows[d]["RelieveDate"] = dt5.Rows[d]["RelieveDate"].ToString();
                    DataCells.Rows[d]["LeaveFrom1"] = dt5.Rows[d]["LeaveFrom1"].ToString();
                    DataCells.Rows[d]["LeaveTo1"] = dt5.Rows[d]["LeaveTo1"].ToString();
                    DataCells.Rows[d]["ReliveReason"] = dt5.Rows[d]["ReliveReason"].ToString();
                    srnoo = srnoo + 1;

                }
            }
            //   grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HR CONSOLIDATED REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">HR CONSOLIDATED REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");



            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td rowspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Rept.No:4</a>");

            //Response.Write("</td>");
            //Response.Write("<td rowspan='2'>");
            //Response.Write("<a style=\"font-weight:bold\">Details</a>");

            Response.Write("</td>");


            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");


            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">HR CONSOLDT RPT</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\">RPTG TIME : :</a>");
            Response.Write("</td>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DEPT</a>");

            Response.Write("</td>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");

            Response.Write("</td>");
            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">OPG</a>");

            Response.Write("</td>");


            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">REL</a>");

            Response.Write("</td>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">ADD</a>");

            Response.Write("</td>");
            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");

            Response.Write("</td>");

            Response.Write("</tr>");
            for (int k = 0; k < AutoDTable.Rows.Count; k++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DEPT"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ALLOT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["OPG"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["REL"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["ADD"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TOTAL"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }

            //Joining Process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">NEW N EXST JOINERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            Response.Write("</tr>");

            for (int l = 0; l < DataCell.Rows.Count; l++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");




                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");




                Response.Write("</tr>");

            }

            //Reliving process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">RELIVERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");




            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");
            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");
            Response.Write("</tr>");

            for (int n = 0; n < DataCells.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }
    }
    public void hrreportNewReliveConsolidate(string FromDate, string ToDate)
    {



        if (SessionUserType == "2")
        {
            NonAdminhrreportNewReliveConsolidate(FromDate);
        }
        else
        {
            DataTable dt_Dept = new DataTable();
            DataTable dt_TempOpen = new DataTable();
            DataTable dt_TempJoin = new DataTable();
            DataTable dt_Relive = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            string count = "0";
            string add = "0";
            string sub = "0";
            string finaltotal = "0";
            string DeptCode = "";
            string LabCount = "0";


            string TempOpen = "", TempJoin = "", TempRelive = "";
            string TempFinalTotal = "";

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("ALLOT");
            AutoDTable.Columns.Add("OPG");
            AutoDTable.Columns.Add("REL");
            AutoDTable.Columns.Add("ADD");
            AutoDTable.Columns.Add("TOTAL");

            DateTime myDate = DateTime.Parse(FromDate);
            DateTime dateForButton = myDate.AddDays(-1);
            string TempDate = dateForButton.ToString("dd/MM/yyyy");

            SSQL = "";
            SSQL = SSQL + "select * from Department_Mst where LocCode='" + SessionLcode.ToString() + "' order by ShortCode asc";
            dt_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < dt_Dept.Rows.Count; i++)
            {
                count = "0";
                add = "0";
                sub = "0";
                finaltotal = "0";

                SSQL = "";
                SSQL = SSQL + "select COUNT(*) as OpenTol from Employee_Mst where LocCode='" + SessionLcode.ToString() + "' and CONVERT(datetime,DOJ,103)<=CONVERT(datetime,'" + TempDate + "',103)  and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                dt_TempOpen = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_TempOpen.Rows.Count > 0) { TempOpen = dt_TempOpen.Rows[0]["OpenTol"].ToString(); } else { TempOpen = "0"; }

                //SSQL = "";
                //SSQL = SSQL + "select COUNT(*) as JoinTol from Employee_Mst where LocCode='" + SessionLcode.ToString() + "'  and IsActive='Yes' and CONVERT(datetime,DOJ,103)=CONVERT(datetime,'" + TempDate + "',103) and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                //dt_TempJoin = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_TempJoin.Rows.Count > 0) { TempJoin = dt_TempJoin.Rows[0]["JoinTol"].ToString(); } else { TempJoin = "0"; }

                SSQL = "";
                SSQL = SSQL + "select COUNT(*) as ReliveOpen from Employee_Mst where LocCode='" + SessionLcode.ToString() + "'  and IsActive='No' and CONVERT(datetime,RelieveDate,103)<=CONVERT(datetime,'" + TempDate + "',103) and DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                dt_Relive = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Relive.Rows.Count > 0) { TempRelive = dt_Relive.Rows[0]["ReliveOpen"].ToString(); } else { TempRelive = "0"; }

                SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And DeptCode='" + dt_Dept.Rows[i]["DeptCode"].ToString() + "'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt1.Rows.Count > 0) { LabCount = dt1.Rows[0]["Allot"].ToString(); }
                else { LabCount = "0"; }

                SSQL = "";
                SSQL = SSQL + "Select COUNT(*) as New_Join from Employee_Mst where IsActive='Yes' and CompCode='" + SessionCcode.ToString() + "' ";
                SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,DOJ, 103)>=CONVERT(Datetime,'" + FromDate + "',103) and CONVERT(DATETIME,DOJ, 103)<=CONVERT(Datetime,'" + ToDate + "',103) ";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt2.Rows.Count > 0) { add = dt2.Rows[0]["New_Join"].ToString(); }
                else { add = "0"; }

                SSQL = "Select count(RelieveDate)as RelieveDate  from Employee_Mst   where IsActive='No' and CompCode='" + SessionCcode.ToString() + "' ";
                SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,RelieveDate, 103)>=CONVERT(Datetime,'" + FromDate + "',103)  and CONVERT(DATETIME,RelieveDate, 103)<=CONVERT(Datetime,'" + ToDate + "',103)";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0) { sub = dt3.Rows[0]["RelieveDate"].ToString(); }
                else { sub = "0"; }


                TempFinalTotal = (Convert.ToDecimal(TempOpen) - Convert.ToDecimal(TempRelive)).ToString();
                finaltotal = ((Convert.ToDecimal(TempFinalTotal) + Convert.ToDecimal(add)) - Convert.ToDecimal(sub)).ToString();

                //finaltotal = count + add - sub;

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt_Dept.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = TempFinalTotal;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = LabCount;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = add;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = sub;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TOTAL"] = finaltotal;



            }


            // SSQL = "";
            // SSQL = SSQL + "select DeptName,DeptCode,'0' as Allot_Total,'0' as Additional,'0' as Relive,";
            // SSQL = SSQL + "(SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total ";
            // SSQL = SSQL + "from ( select DM.DeptName,DM.DeptCode,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,";
            // SSQL = SSQL + "CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five ";
            // SSQL = SSQL + "from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            // SSQL = SSQL + "inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode ";
            // SSQL = SSQL + "where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'  and ";
            // SSQL = SSQL + "CONVERT(datetime,DOJ,103)<CONVERT(datetime,'" + FromDate + "',103) ";
            // SSQL = SSQL + "group by DM.DeptName,GM.GradeName,DM.DeptCode ";
            // SSQL = SSQL + ") as PPV group by DeptName,DeptCode ";
            //// SSQL = " Select DeptCode,DeptName from Department_Mst  where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' order by DeptCode ";
            // dt = objdata.RptEmployeeMultipleDetails(SSQL);

            // if (dt.Rows.Count != 0)
            // {
            //     for (int k = 0; k < dt.Rows.Count; k++)
            //     {






            //     }
            // }

            double Sum = 0;

            int TotCount = 0;

            TotCount = AutoDTable.Rows.Count;
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[TotCount]["REL"] = "GTotal";

            for (int j = 0; j < AutoDTable.Rows.Count - 1; j++)
            {
                //AutoDTable.Columns.Add("A1 No's");
                //AutoDTable.Columns.Add("A1 Days");
                //AutoDTable.Columns.Add("A1 Amt");

                Sum += Convert.ToDouble(AutoDTable.Rows[j]["TOTAL"].ToString());


            }

            AutoDTable.Rows[TotCount]["TOTAL"] = Sum;

            Sum = 0;
            DataCell.Columns.Add("S.NO");
            DataCell.Columns.Add("BrokerName");
            DataCell.Columns.Add("DeptName");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("GradeName");
            DataCell.Columns.Add("DOJ");
            DataCell.Columns.Add("RejoinDate");
            DataCell.Columns.Add("RelieveDate");
            DataCell.Columns.Add("LeaveFrom1");
            DataCell.Columns.Add("LeaveTo1");
            DataCell.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo,103) as LeaveTo1,EM.Reason as ReliveReason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='Yes' ";
            SSQL = SSQL + " and CONVERT(datetime,DOJ,103)>=CONVERT(datetime,'" + FromDate + "',103) and CONVERT(datetime,DOJ,103)<=CONVERT(datetime,'" + ToDate + "',103) order by EmpNo Asc";

            dt4 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt4.Rows.Count > 0)
            {
                int srno = 1;
                for (int d = 0; d < dt4.Rows.Count; d++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[d]["S.NO"] = srno;
                    DataCell.Rows[d]["BrokerName"] = dt4.Rows[d]["AgentName"].ToString();
                    DataCell.Rows[d]["DeptName"] = dt4.Rows[d]["DeptName"].ToString();
                    DataCell.Rows[d]["MachineID"] = dt4.Rows[d]["EmpNo"].ToString();
                    DataCell.Rows[d]["Name"] = dt4.Rows[d]["FirstName"].ToString();
                    DataCell.Rows[d]["GradeName"] = dt4.Rows[d]["GradeName"].ToString();
                    DataCell.Rows[d]["DOJ"] = dt4.Rows[d]["DOJ"].ToString();
                    DataCell.Rows[d]["RejoinDate"] = dt4.Rows[d]["RejoinDate"].ToString();
                    DataCell.Rows[d]["RelieveDate"] = dt4.Rows[d]["RelieveDate"].ToString();
                    DataCell.Rows[d]["LeaveFrom1"] = dt4.Rows[d]["LeaveFrom1"].ToString();
                    DataCell.Rows[d]["LeaveTo1"] = dt4.Rows[d]["LeaveTo1"].ToString();
                    DataCell.Rows[d]["ReliveReason"] = dt4.Rows[d]["ReliveReason"].ToString();
                    srno = srno + 1;

                }
            }

            DataCells.Columns.Add("S.NO");
            DataCells.Columns.Add("BrokerName");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("GradeName");
            DataCells.Columns.Add("DOJ");
            DataCells.Columns.Add("RejoinDate");
            DataCells.Columns.Add("RelieveDate");
            DataCells.Columns.Add("LeaveFrom1");
            DataCells.Columns.Add("LeaveTo1");
            DataCells.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo,103) as LeaveTo1,EM.Reason as ReliveReason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='No' ";
            SSQL = SSQL + " and CONVERT(datetime,RelieveDate,103)>=CONVERT(datetime,'" + FromDate + "',103) and CONVERT(datetime,RelieveDate,103)<=CONVERT(datetime,'" + ToDate + "',103) order by EmpNo Asc";

            dt5 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt5.Rows.Count > 0)
            {
                int srnoo = 1;
                for (int d = 0; d < dt5.Rows.Count; d++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[d]["S.NO"] = srnoo;
                    DataCells.Rows[d]["BrokerName"] = dt5.Rows[d]["AgentName"].ToString();
                    DataCells.Rows[d]["DeptName"] = dt5.Rows[d]["DeptName"].ToString();
                    DataCells.Rows[d]["MachineID"] = dt5.Rows[d]["EmpNo"].ToString();
                    DataCells.Rows[d]["Name"] = dt5.Rows[d]["FirstName"].ToString();
                    DataCells.Rows[d]["GradeName"] = dt5.Rows[d]["GradeName"].ToString();
                    DataCells.Rows[d]["DOJ"] = dt5.Rows[d]["DOJ"].ToString();
                    DataCells.Rows[d]["RejoinDate"] = dt5.Rows[d]["RejoinDate"].ToString();
                    DataCells.Rows[d]["RelieveDate"] = dt5.Rows[d]["RelieveDate"].ToString();
                    DataCells.Rows[d]["LeaveFrom1"] = dt5.Rows[d]["LeaveFrom1"].ToString();
                    DataCells.Rows[d]["LeaveTo1"] = dt5.Rows[d]["LeaveTo1"].ToString();
                    DataCells.Rows[d]["ReliveReason"] = dt5.Rows[d]["ReliveReason"].ToString();
                    srnoo = srnoo + 1;

                }
            }
            //   grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HR NEW RELIVE REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">HR NEW RELIVE REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> FromDate -" + FromDate + " ToDate - " + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");



            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td rowspan='2'>");
            //Response.Write("<a style=\"font-weight:bold\">Rept.No:4</a>");

            ////Response.Write("</td>");
            ////Response.Write("<td rowspan='2'>");
            ////Response.Write("<a style=\"font-weight:bold\">Details</a>");

            //Response.Write("</td>");


            //Response.Write("<td colspan='3' >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td colspan='2' >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");

            //Response.Write("<tr Font-Bold='true' align='center'>");


            //Response.Write("<td colspan='3' >");
            //Response.Write("<a style=\"font-weight:bold\">HR CONSOLDT RPT</a>");
            //Response.Write("</td>");

            //Response.Write("<td colspan='2' >");
            //Response.Write("<a style=\"font-weight:bold\">RPTG TIME : :</a>");
            //Response.Write("</td>");

            //Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\">DEPT</a>");

            //Response.Write("</td>");
            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");

            //Response.Write("</td>");
            //Response.Write("<td  >");
            //Response.Write("<a style=\"font-weight:bold\">OPG</a>");

            //Response.Write("</td>");


            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\">REL</a>");

            //Response.Write("</td>");
            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\">ADD</a>");

            //Response.Write("</td>");
            //Response.Write("<td  >");
            //Response.Write("<a style=\"font-weight:bold\">Total</a>");

            //Response.Write("</td>");

            //Response.Write("</tr>");
            //for (int k = 0; k < AutoDTable.Rows.Count; k++)
            //{


            //    Response.Write("<tr Font-Bold='true' align='center'>");

            //    Response.Write("<td >");
            //    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DEPT"].ToString() + "</a>");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ALLOT"].ToString() + " </a>");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("<a >" + AutoDTable.Rows[k]["OPG"].ToString() + " </a>");
            //    Response.Write("</td>");


            //    Response.Write("<td >");
            //    Response.Write("<a >" + AutoDTable.Rows[k]["REL"].ToString() + " </a>");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("<a >" + AutoDTable.Rows[k]["ADD"].ToString() + " </a>");
            //    Response.Write("</td>");


            //    Response.Write("<td >");
            //    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TOTAL"].ToString() + " </a>");
            //    Response.Write("</td>");


            //    Response.Write("</tr>");

            //}

            //Joining Process
            //Response.Write("<tr>");

            //Response.Write("</tr>");

            //Response.Write("<tr>");

            //Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">NEW N EXST JOINERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            Response.Write("</tr>");

            for (int l = 0; l < DataCell.Rows.Count; l++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");




                Response.Write("</tr>");

            }

            //Reliving process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">RELIVERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");




            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");


            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");
            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");
            Response.Write("</tr>");

            for (int n = 0; n < DataCells.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");



                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }

    }
    public void GetHRTracer_All(string FromDate, string ToDate, string CurrentDate)
    {

        if (SessionUserType == "2")
        {
            // NonAdminGetHRTracer_Changes(DeptName, FromDate, ToDate, CurrentDate);
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable IsActive = new DataTable();
            DataTable NonActive = new DataTable();
            DataTable CurrentEmp = new DataTable();

            AutoDTable.Columns.Add("S.No");
            AutoDTable.Columns.Add("DesignName");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("GRD");
            AutoDTable.Columns.Add("AgentName");
            AutoDTable.Columns.Add("FD",typeof(decimal));
            AutoDTable.Columns.Add("ON", typeof(decimal));
            AutoDTable.Columns.Add("AS ON", typeof(decimal));
            AutoDTable.Columns.Add("OFF", typeof(decimal));
            AutoDTable.Columns.Add("Total", typeof(decimal));
            AutoDTable.Columns.Add("Status");

            int dayCount = (int)((Convert.ToDateTime(ToDate) - Convert.ToDateTime(FromDate)).TotalDays);
            dayCount = dayCount + 1;
            int SNo = 1;
            int intK = 0;

            double Total = 0;
            string Count_Present = "";

            string[] sp_Date = FromDate.Split('/');
            string Fd = sp_Date[0];

            DataTable da_Val = new DataTable();







            SSQL = " select distinct  EM.DOJ,EM.MachineID,EM.FirstName,MG.GradeName,DM.DeptName,DS.DesignName,AM.AgentName ";
            SSQL = SSQL + "  from Employee_Mst EM inner join MstGrade MG on  EM.LocCode=MG.LocCode and EM.Grade=MG.GradeID  ";
            SSQL = SSQL + " inner join AgentMst AM on AM.LocCode=EM.LocCode and AM.AgentID=EM.AgentID   ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptCode  inner join Designation_Mst DS on  DS.DesignName=EM.Designation  where";
            // SSQL = SSQL + " CONVERT(DATETIME,LD.Attn_Date,103) >= CONVERT(DATETIME,'" + FromDate + "' ,103) ";
            // SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date,103) <= CONVERT(DATETIME,'" + ToDate + "',103)";
            SSQL = SSQL + "  EM.IsActive='Yes'  and  EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " group by EM.DOJ,EM.MachineID,EM.FirstName,MG.GradeName,DM.DeptName,DS.DesignName,AM.AgentName";

            da_Val = objdata.RptEmployeeMultipleDetails(SSQL);

            string Tot_ON = "0";
            string Tot_ASON = "0";
            string Tot_OFF = "0";
            string Tot_Total = "0";

            for (int i = 0; i < da_Val.Rows.Count; i++)
            {

                string MID = da_Val.Rows[i]["MachineID"].ToString();
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[intK]["S.No"] = SNo.ToString();
                AutoDTable.Rows[intK]["DesignName"] = da_Val.Rows[i]["DesignName"].ToString();
                AutoDTable.Rows[intK]["DeptName"] = da_Val.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[intK]["MachineID"] = da_Val.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[intK]["Name"] = da_Val.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[intK]["GRD"] = da_Val.Rows[i]["GradeName"].ToString();
                AutoDTable.Rows[intK]["AgentName"] = da_Val.Rows[i]["AgentName"].ToString();
                AutoDTable.Rows[intK]["Total"] = dayCount;
                Tot_Total = (Convert.ToInt32(Tot_Total) + Convert.ToInt32(dayCount)).ToString();


                DataTable DT_Present = new DataTable();
                SSQL = "select isnull(Sum(Present),0) as Present from LogTime_Days where MachineID='" + MID + "'";  //and Present='1'";
                SSQL = SSQL + " And CONVERT(date,Attn_Date,103) >= CONVERT(date,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(date,Attn_Date,103) <= CONVERT(date,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                DT_Present = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT_Present.Rows.Count != 0)
                {
                    Count_Present = DT_Present.Rows[0]["Present"].ToString();
                    AutoDTable.Rows[intK]["AS ON"] = DT_Present.Rows[0]["Present"].ToString();

                }
                else
                {
                    Count_Present = "0";
                    AutoDTable.Rows[intK]["AS ON"] = "0";

                }

                double OFF = (Convert.ToDouble(dayCount) - Convert.ToDouble(Count_Present));

                AutoDTable.Rows[intK]["OFF"] = OFF;

                Tot_ASON = (Convert.ToDouble(Tot_ASON) + Convert.ToDouble(Count_Present.ToString())).ToString();
                Tot_OFF = (Convert.ToDouble(Tot_OFF) + Convert.ToDouble(OFF)).ToString();

                SSQL = "select convert(varchar(2),'" + FromDate + "',103) as FD,Present from LogTime_Days where MachineID='" + MID + "'";  //and Present='1'";
                SSQL = SSQL + " And CONVERT(date,Attn_Date,120) >= CONVERT(date,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',120)";
                SSQL = SSQL + " And CONVERT(date,Attn_Date,120) <= CONVERT(date,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',120)";
                CurrentEmp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (CurrentEmp.Rows.Count != 0)
                {
                    //AutoDTable.Rows[intK]["FD"] = Fd;
                    AutoDTable.Rows[intK]["ON"] = CurrentEmp.Rows[0]["Present"].ToString();

                    Tot_ON = (Convert.ToDouble(Tot_ON) + Convert.ToDouble(CurrentEmp.Rows[0]["Present"].ToString())).ToString();

                }
                else
                {
                    //AutoDTable.Rows[intK]["FD"] = Fd;
                    AutoDTable.Rows[intK]["ON"] = "0";

                    Tot_ON = (Convert.ToDouble(Tot_ON) + Convert.ToDouble(0)).ToString();

                }

                if (da_Val.Rows[i]["DOJ"].ToString() != "")
                {
                    if ((Convert.ToDateTime(da_Val.Rows[i]["DOJ"].ToString()) > Convert.ToDateTime(FromDate)) && (Convert.ToDateTime(da_Val.Rows[i]["DOJ"].ToString()) < Convert.ToDateTime(ToDate)))
                    {
                        string DOJ_FD = Convert.ToDateTime(da_Val.Rows[i]["DOJ"].ToString()).ToString("dd/MM/yyyy");
                        string[] FD_Split = DOJ_FD.Split('/');
                        AutoDTable.Rows[intK]["FD"] = FD_Split[0];
                        AutoDTable.Rows[intK]["Status"] = "N";
                    }
                    else
                    {
                        AutoDTable.Rows[intK]["FD"] = Fd;
                        AutoDTable.Rows[intK]["Status"] = "Y";
                    }
                }
                else
                {
                    AutoDTable.Rows[intK]["FD"] = Fd;
                    AutoDTable.Rows[intK]["Status"] = "Y";
                }

                SNo = SNo + 1;
                intK = intK + 1;

            }

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["FD"] = "Total";
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ON"] = Tot_ON;
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AS ON"] = Tot_ASON;
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFF"] = Tot_OFF;
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Tot_Total;

            DataTable DT_Comp = new DataTable();
            string CompName = "";
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode.ToString() + "'";
            DT_Comp = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_Comp.Rows.Count != 0)
            {
                CompName = DT_Comp.Rows[0]["CompName"].ToString();
            }


            //grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=TraceAll.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">" + CompName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">HR TRACER REPORT -- " + FromDate + " -- " + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">New Joining</a>");
            Response.Write("</td>");



            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">S.No</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">DesignName</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">GRD</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">Source Name</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">FD</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">ON</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">AS ON</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">OFF</a>");
            Response.Write("</td>");

            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");
            Response.Write("</td>");

            Response.Write("</tr>");
            for (int k = 0; k < AutoDTable.Rows.Count - 1; k++)
            {
                string day = "";
                day = AutoDTable.Rows[k]["Status"].ToString();
                if (day == "N")
                {

                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["S.No"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DesignName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DeptName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["MachineID"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Name"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["GRD"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["AgentName"].ToString() + "</a>");
                    Response.Write("</td>");



                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["FD"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ON"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["AS ON"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["OFF"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td style='background-color:lightgreen;font-weight: bold;'>");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Total"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");


                }

                else
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["S.No"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DesignName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DeptName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["MachineID"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Name"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["GRD"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["AgentName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["FD"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ON"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["AS ON"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["OFF"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Total"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");

                }


            }

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Total </a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">" + Tot_ON + "</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">" + Tot_ASON + "</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">" + Tot_OFF + "</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">" + Tot_Total + "</a>");
            Response.Write("</td>");


            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

            // Response.Write("<td>");
            // Response.Write("</td>");

            //SSQL = " Select DeptName from Department_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And DeptCode='" + DeptName + "' ";
            //dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            //    Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            //    Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            //    System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            //    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            //    writer.PageEvent = new HeaderFooter();
            //    document.Open();


            //    PdfPTable table = new PdfPTable(5);
            //    float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f };
            //    table.SetWidths(widths);
            //    table.WidthPercentage = 100;

            //    PdfPTable table1 = new PdfPTable(10);
            //    float[] widths1 = new float[] { 10f, 30f, 25f, 25f, 10f, 10f, 10f, 10f, 10f, 10f };
            //    table1.SetWidths(widths1);
            //    table1.WidthPercentage = 100;

            //    PdfPTable table2 = new PdfPTable(4);
            //    float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            //    table2.SetWidths(widths2);
            //    table2.WidthPercentage = 100;

            //    PdfPCell cell;

            //    if (AutoDTable.Rows.Count > 0)
            //    {
            //        cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);


            //            cell = PhraseCell(new Phrase("ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = 0;
            //            cell.Colspan = 5;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table.AddCell(cell);


            //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 2;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase("Current Date: " + CurrentDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 2;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //        table.AddCell(cell);

            //        //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        //cell.Border = 0;
            //        //cell.Colspan = 2;
            //        //cell.PaddingTop = 5f;
            //        //cell.PaddingBottom = 5f;
            //        //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        //table.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        //cell.Border = Rectangle.TOP_BORDER;
            //        //cell.Colspan = 8;
            //        //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        //table1.AddCell(cell); Allot

            //        cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase("Designation", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);


            //        cell = PhraseCell(new Phrase("DeptName", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);



            //        cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);


            //        cell = PhraseCell(new Phrase("GRD", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase("FD", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase("ON", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase("AS ON", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase("OFF", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase("Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        //cell.Border = Rectangle.BOTTOM_BORDER;
            //        //cell.Colspan = 8;
            //        //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        //table1.AddCell(cell);





            //        for (int i = 0; i < AutoDTable.Rows.Count; i++)
            //        {


            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["S.No"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);



            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DesignName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);



            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Name"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["GRD"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FD"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ON"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["AS ON"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["OFF"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //        }




            //    }
            //    else
            //    {
            //        cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase("OE VAJPAI - ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER;
            //        cell.Colspan = 8;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.BOTTOM_BORDER;
            //        cell.Colspan = 8;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //    }

            //    table.SpacingBefore = 10f;
            //    table.SpacingAfter = 10f;

            //    document.Add(table);

            //    //table1.SpacingBefore = 5f;
            //    //table1.SpacingAfter = 5f;

            //    document.Add(table1);


            //    document.Close();

            //    byte[] bytes = memoryStream.ToArray();
            //    memoryStream.Close();
            //    Response.Clear();
            //    Response.ContentType = "application/pdf";
            //    Response.AddHeader("Content-Disposition", "attachment; filename=HR_Trace.pdf");
            //    Response.ContentType = "application/pdf";
            //    Response.Buffer = true;
            //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //    Response.BinaryWrite(bytes);
            //    Response.End();
            //    Response.Close();



            //}
        }
    }
    public void LeaderGradeDeptReportBTDates(string WagesType, string FromDate, string ToDate)
    {
        if (SessionUserType == "2")
        {
            // NonAdminGetDayAttendanceBTDates_Changes(WagesType, FromDate, ToDate);
        }
        else
        {
            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("LeaderName");
            AutoDTable.Columns.Add("REQ");
            AutoDTable.Columns.Add("AVL");
            AutoDTable.Columns.Add("ON %");
            AutoDTable.Columns.Add("UP %");
            AutoDTable.Columns.Add("A++");
            AutoDTable.Columns.Add("A+");
            AutoDTable.Columns.Add("A");
            AutoDTable.Columns.Add("T+");
            AutoDTable.Columns.Add("T");

            SSQL = "select LAM.AgentName,LAM.Total AS REQ,COUNT(EM.EmpNo) as AVL from LeaderAllotment_Mst LAM inner join ";
            SSQL = SSQL + " Employee_Mst EM on LAM.AgentID=EM.AgentID ";
            SSQL = SSQL + " and EM.LocCode=LAM.LocCode  where EM.CompCode='" + SessionCcode + "' and  EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and LAM.CompCode='" + SessionCcode + "' and LAM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and IsActive='Yes' group by LAM.AgentName ,LAM.Total";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                int SNo = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNo"] = (i + 1);
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LeaderName"] = dt.Rows[i]["AgentName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REQ"] = dt.Rows[i]["REQ"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AVL"] = dt.Rows[i]["AVL"].ToString();

                    SSQL = "select SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,";
                    SSQL = SSQL + "(SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total,'0' as Allot_Total";
                    SSQL = SSQL + " from (select CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
                    SSQL = SSQL + "CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN ";
                    SSQL = SSQL + " GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three, CASE WHEN ";
                    SSQL = SSQL + " GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four, CASE WHEN ";
                    SSQL = SSQL + " GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five";
                    SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode COLLATE DATABASE_DEFAULT=GM.LocCode COLLATE DATABASE_DEFAULT";
                    //SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode ";
                    SSQL = SSQL + " inner join LeaderAllotment_Mst LAM on LAM.AgentID=EM.AgentID inner join AgentMst AM on AM.LocCode COLLATE DATABASE_DEFAULT=EM.LocCode COLLATE DATABASE_DEFAULT and LAM.AgentID=AM.AgentID where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'  ";
                    SSQL = SSQL + " and GM.CompCode='" + SessionCcode + "' and GM.LocCode='" + SessionLcode + "' and LAM.CompCode='" + SessionCcode + "' and LAM.LocCode='" + SessionLcode + "' and EM.IsActive='Yes' and  AM.AgentName='" + dt.Rows[i]["AgentName"].ToString() + "' ";
                    SSQL = SSQL + " group by GM.GradeName) as PPV";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt1.Rows.Count != 0)
                    {
                        double Up = (Convert.ToDouble(Convert.ToDouble(dt.Rows[i]["AVL"].ToString()) / Convert.ToDouble(dt.Rows[i]["REQ"].ToString())) * 100);
                        string basic = (Math.Round(Convert.ToDecimal(Up), 0, MidpointRounding.AwayFromZero)).ToString();

                        if (Convert.ToDecimal(basic) > 100)
                        {
                            basic = "100";
                        }

                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ON %"] = basic;
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["UP %"] = basic;
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt1.Rows[0]["Five"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt1.Rows[0]["Four"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt1.Rows[0]["Three"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt1.Rows[0]["Two"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt1.Rows[0]["One"].ToString();
                    }



                }
            }


            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=LEADER GRADE DEPARTMENT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">LEADER GRADE DEPARTMENT REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
    public void All_LeaderONOFFReport(string WagesType, string FromDate, string ToDate)
    {
        if (SessionUserType == "2")
        {
            // NonAdminGetDayAttendanceBTDates_Changes(WagesType, FromDate, ToDate);
        }
        else
        {
            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable Dep_da = new DataTable();
            DataTable Active_da = new DataTable();
            DataTable Present_da = new DataTable();
            DataTable On_Grade = new DataTable();
            DataTable Off_Grade = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("ALLOT");
            AutoDTable.Columns.Add("ON");
            AutoDTable.Columns.Add("OFF");
            AutoDTable.Columns.Add("TOTAL");
            AutoDTable.Columns.Add("A+*");
            AutoDTable.Columns.Add("A++");
            AutoDTable.Columns.Add("A+");
            AutoDTable.Columns.Add("A");
            AutoDTable.Columns.Add("T+");
            AutoDTable.Columns.Add("T");
            AutoDTable.Columns.Add("Total_ON");
            AutoDTable.Columns.Add("OFF_A+*");
            AutoDTable.Columns.Add("OFF_A++");
            AutoDTable.Columns.Add("OFF_A+");
            AutoDTable.Columns.Add("OFF_A");
            AutoDTable.Columns.Add("OFF_T+");
            AutoDTable.Columns.Add("OFF_T");
            AutoDTable.Columns.Add("OFF_Total");

            SSQL = " select * from Department_Mst where  LocCode='" + SessionLcode + "' and LocCode='" + SessionLcode + "' order by ShortCode Asc";
            Dep_da = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Dep_da.Rows.Count != 0)
            {
                int SNo = 1;
                for (int i = 0; i < Dep_da.Rows.Count; i++)
                {
                    string ALLOT = "0";
                    string ONS = "0";
                    string OFF = "0";

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[intK]["SNo"] = (i + 1);
                    AutoDTable.Rows[intK]["DeptName"] = Dep_da.Rows[i]["DeptName"].ToString();

                    // IsActive Count
                    // SSQL = "select COUNT(MachineID)  as  ALLOT from Employee_Mst where DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "' and IsActive='Yes' and LocCode='" + SessionLcode + "' and LocCode='" + SessionLcode + "'";
                    SSQL = "Select isnull(SUM(Total),0) as ALLOT from LabourAllotment_Mst LM inner join ";
                    SSQL = SSQL + "Designation_Mst DS on LM.LocCode COLLATE DATABASE_DEFAULT=DS.LocCode COLLATE DATABASE_DEFAULT and LM.DesignNo=Ds.DesignNo where DS.DeptCode='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and DS.CompCode='" + SessionCcode + "' and DS.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LM.CompCode='" + SessionCcode + "' and LM.LocCode='" + SessionLcode + "'";
                    Active_da = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Active_da.Rows.Count != 0)
                    {
                        AutoDTable.Rows[intK]["ALLOT"] = Active_da.Rows[0]["ALLOT"].ToString();
                        ALLOT = Active_da.Rows[0]["ALLOT"].ToString();
                    }
                    else
                    {
                        ALLOT = "0";
                    }


                    //Present And Absent and Total Count

                    //SSQL = "Select COUNT(LD.MachineID) as ONS from LogTime_Days LD inner join Employee_Mst EM ";
                    //SSQL = SSQL + "on LD.ExistingCode=EM.ExistingCode and LD.LocCode=EM.LocCode";
                    //SSQL = SSQL + " where LD.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and (LD.Present='1.0' or LD.Present='0.5')";
                    //SSQL = SSQL + "  and LD.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and EM.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,EM.DOJ, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    SSQL = "select COUNT(EM.ExistingCode) as ONS from Employee_Mst EM inner join LogTime_Days LT";
                    SSQL = SSQL + " on EM.ExistingCode=LT.ExistingCode";
                    SSQL = SSQL + " where EM.LocCode='" + SessionLcode + "' And EM.CompCode='" + SessionCcode + "'";
                    SSQL = SSQL + " and EM.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and LT.CompCode='" + SessionCcode + "' and LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LT.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and (LT.Present='1.0' or LT.Present='0.5')";
                    SSQL = SSQL + " and (EM.IsActive='Yes' or CONVERT(DATETIME,EM.RelieveDate, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
                    Present_da = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (Present_da.Rows.Count != 0)
                    {
                        AutoDTable.Rows[intK]["ON"] = Present_da.Rows[0]["ONS"].ToString();

                    }
                    else
                    {
                        AutoDTable.Rows[intK]["ON"] = "0";
                    }

                    DataTable OFF_da = new DataTable();
                    //SSQL = "Select COUNT(LD.MachineID) as OFFs from LogTime_Days LD inner join Employee_Mst EM ";
                    //SSQL = SSQL + "on LD.ExistingCode=EM.ExistingCode and LD.LocCode=EM.LocCode";
                    //SSQL = SSQL + " where LD.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and LD.Present='0.0'";
                    //SSQL = SSQL + "  and LD.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and EM.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,EM.DOJ, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    SSQL = "Select COUNT(EM.ExistingCode) as OFFs from Employee_Mst EM inner join LogTime_Days LT";
                    SSQL = SSQL + " on EM.ExistingCode=LT.ExistingCode";
                    SSQL = SSQL + " where EM.LocCode='" + SessionLcode + "' And EM.CompCode='" + SessionCcode + "'";
                    SSQL = SSQL + " and EM.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and LT.CompCode='" + SessionCcode + "' and LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LT.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and (LT.Present='0.0' or LT.Present='0.0')";
                    SSQL = SSQL + " and (EM.IsActive='Yes' or CONVERT(DATETIME,EM.RelieveDate, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";

                    OFF_da = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (OFF_da.Rows.Count != 0)
                    {
                        AutoDTable.Rows[intK]["OFF"] = OFF_da.Rows[0]["OFFs"].ToString();

                    }
                    else
                    {
                        AutoDTable.Rows[intK]["OFF"] = "0";
                    }

                    //DataTable da_Total = new DataTable();
                    //SSQL = "select COUNT(MachineID) as TOTAL from Employee_Mst where IsActive='Yes' and DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "' ";
                    //SSQL = SSQL + " And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    //da_Total = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (da_Total.Rows.Count != 0)
                    //{
                    //AutoDTable.Rows[intK]["TOTAL"] = da_Total.Rows[0]["TOTAL"].ToString();
                    AutoDTable.Rows[intK]["TOTAL"] = Convert.ToInt32(Convert.ToInt32(AutoDTable.Rows[intK]["ON"].ToString())) + Convert.ToInt32(AutoDTable.Rows[intK]["OFF"].ToString());
                    //}

                    //Grade Present 

                    SSQL = "select SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,SUM(Six) as Six,(SUM(One) +";
                    SSQL = SSQL + " SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)+SUM(Six)) as Final_Total ";
                    SSQL = SSQL + " from (select CASE WHEN GM.GradeName = 'T' THEN count(LD.ExistingCode) else 0 END AS One,CASE WHEN";
                    SSQL = SSQL + " GM.GradeName = 'T+' THEN count(LD.ExistingCode) else 0 END AS Two,CASE WHEN  GM.GradeName = 'A'";
                    SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Three, CASE WHEN  GM.GradeName = 'A+' ";
                    SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Four, CASE WHEN  GM.GradeName = 'A++' ";
                    SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Five,";
                    SSQL = SSQL + "CASE WHEN  GM.GradeName = 'A+*' THEN count(LD.ExistingCode) else 0 END AS Six";
                    SSQL = SSQL + " from Employee_Mst LD";
                    SSQL = SSQL + " inner join LogTime_Days LT on LD.ExistingCode=LT.ExistingCode";
                    SSQL = SSQL + " inner join MstGrade GM on LD.Grade=GM.GradeID and LD.LocCode COLLATE DATABASE_DEFAULT=GM.LocCode COLLATE DATABASE_DEFAULT ";
                    SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=LD.DeptCode and DM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT ";
                    SSQL = SSQL + " where DM.CompCode='" + SessionCcode + "' and DM.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LT.CompCode='" + SessionCcode + "' and LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and GM.CompCode='" + SessionCcode + "' and GM.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and (LT.Present='1.0' or LT.Present='0.5') and LT.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and LD.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and DM.DeptCode='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and (LD.IsActive='Yes' or CONVERT(DATETIME,LD.DOJ, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)) group by GM.GradeName) as PPV";
                    On_Grade = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (On_Grade.Rows.Count != 0)
                    {
                        AutoDTable.Rows[intK]["A+*"] = On_Grade.Rows[0]["Six"].ToString();
                        AutoDTable.Rows[intK]["A++"] = On_Grade.Rows[0]["Five"].ToString();
                        AutoDTable.Rows[intK]["A+"] = On_Grade.Rows[0]["Four"].ToString();
                        AutoDTable.Rows[intK]["A"] = On_Grade.Rows[0]["Three"].ToString();
                        AutoDTable.Rows[intK]["T+"] = On_Grade.Rows[0]["Two"].ToString();
                        AutoDTable.Rows[intK]["T"] = On_Grade.Rows[0]["One"].ToString();
                        AutoDTable.Rows[intK]["Total_ON"] = On_Grade.Rows[0]["Final_Total"].ToString();

                    }
                    else
                    {
                        AutoDTable.Rows[intK]["A+*"] = "0";
                        AutoDTable.Rows[intK]["A++"] = "0";
                        AutoDTable.Rows[intK]["A+"] = "0";
                        AutoDTable.Rows[intK]["A"] = "0";
                        AutoDTable.Rows[intK]["T+"] = "0";
                        AutoDTable.Rows[intK]["T"] = "0";
                        AutoDTable.Rows[intK]["Total_ON"] = "0";
                    }

                    //Grade Absent 
                    //SSQL = "select SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,(SUM(One) +";
                    //SSQL = SSQL + " SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total ";
                    //SSQL = SSQL + " from (select CASE WHEN GM.GradeName = 'T' THEN count(LD.ExistingCode) else 0 END AS One,CASE WHEN";
                    //SSQL = SSQL + " GM.GradeName = 'T+' THEN count(LD.ExistingCode) else 0 END AS Two,CASE WHEN  GM.GradeName = 'A'";
                    //SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Three, CASE WHEN  GM.GradeName = 'A+' ";
                    //SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Four, CASE WHEN  GM.GradeName = 'A++' ";
                    //SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Five from LogTime_Days LD";
                    //SSQL = SSQL + " inner join MstGrade GM on LD.Grade=GM.GradeID and LD.LocCode=GM.LocCode ";
                    //SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=LD.DeptName and DM.LocCode=LD.LocCode ";
                    //SSQL = SSQL + " where DM.CompCode='" + SessionCcode + "' and DM.LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and GM.CompCode='" + SessionCcode + "' and GM.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and LD.Present='0.0' and LD.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and LD.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    //SSQL = SSQL + " and DM.DeptCode='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,LD.DOJ, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) group by DM.DeptName,GM.GradeName) as PPV";

                    SSQL = "select SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,SUM(Six) as Six,(SUM(One) +";
                    SSQL = SSQL + " SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)+SUM(Six)) as Final_Total ";
                    SSQL = SSQL + " from (select CASE WHEN GM.GradeName = 'T' THEN count(LD.ExistingCode) else 0 END AS One,CASE WHEN";
                    SSQL = SSQL + " GM.GradeName = 'T+' THEN count(LD.ExistingCode) else 0 END AS Two,CASE WHEN  GM.GradeName = 'A'";
                    SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Three, CASE WHEN  GM.GradeName = 'A+' ";
                    SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Four, CASE WHEN  GM.GradeName = 'A++' ";
                    SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Five,";
                    SSQL = SSQL + "CASE WHEN  GM.GradeName = 'A+*' THEN count(LD.ExistingCode) else 0 END AS Six";
                    SSQL = SSQL + " from Employee_Mst LD";
                    SSQL = SSQL + " inner join LogTime_Days LT on LD.ExistingCode=LT.ExistingCode";
                    SSQL = SSQL + " inner join MstGrade GM on LD.Grade=GM.GradeID and LD.LocCode COLLATE DATABASE_DEFAULT=GM.LocCode COLLATE DATABASE_DEFAULT";
                    SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=LD.DeptCode and DM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode  COLLATE DATABASE_DEFAULT";
                    SSQL = SSQL + " where DM.CompCode='" + SessionCcode + "' and DM.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LT.CompCode='" + SessionCcode + "' and LT.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and GM.CompCode='" + SessionCcode + "' and GM.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LT.Present='0.0' and LT.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and LD.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and DM.DeptCode='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and (LD.IsActive='Yes' or CONVERT(DATETIME,LD.DOJ, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)) group by GM.GradeName) as PPV";
                    Off_Grade = objdata.RptEmployeeMultipleDetails(SSQL);

                    //SSQL = "select SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,(SUM(One) +";
                    //SSQL = SSQL + " SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total ";
                    //SSQL = SSQL + " from (select CASE WHEN GM.GradeName = 'T' THEN count(LD.ExistingCode) else 0 END AS One,CASE WHEN";
                    //SSQL = SSQL + " GM.GradeName = 'T+' THEN count(LD.ExistingCode) else 0 END AS Two,CASE WHEN  GM.GradeName = 'A'";
                    //SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Three, CASE WHEN  GM.GradeName = 'A+' ";
                    //SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Four, CASE WHEN  GM.GradeName = 'A++' ";
                    //SSQL = SSQL + " THEN count(LD.ExistingCode) else 0 END AS Five from LogTime_Days LD";
                    //SSQL = SSQL + " inner join MstGrade GM on LD.Grade=GM.GradeID and LD.LocCode=GM.LocCode ";
                    //SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=LD.DeptName and DM.LocCode=LD.LocCode ";
                    //SSQL = SSQL + " and DM.LocCode='" + SessionLcode + "' and GM.LocCode='" + SessionLcode + "' and LD.LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Present='0.0' and LD.Attn_Date_Str='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "' and LD.DeptName='" + Dep_da.Rows[i]["DeptCode"].ToString() + "'  group by DM.DeptName,GM.GradeName) as PPV";
                    //Off_Grade = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (Off_Grade.Rows.Count != 0)
                    {

                        AutoDTable.Rows[intK]["OFF_A+*"] = Off_Grade.Rows[0]["Six"].ToString();
                        AutoDTable.Rows[intK]["OFF_A++"] = Off_Grade.Rows[0]["Five"].ToString();
                        AutoDTable.Rows[intK]["OFF_A+"] = Off_Grade.Rows[0]["Four"].ToString();
                        AutoDTable.Rows[intK]["OFF_A"] = Off_Grade.Rows[0]["Three"].ToString();
                        AutoDTable.Rows[intK]["OFF_T+"] = Off_Grade.Rows[0]["Two"].ToString();
                        AutoDTable.Rows[intK]["OFF_T"] = Off_Grade.Rows[0]["One"].ToString();
                        AutoDTable.Rows[intK]["OFF_Total"] = Off_Grade.Rows[0]["Final_Total"].ToString();

                    }
                    else
                    {
                        AutoDTable.Rows[intK]["OFF_A+*"] = "0";
                        AutoDTable.Rows[intK]["OFF_A++"] = "0";
                        AutoDTable.Rows[intK]["OFF_A+"] = "0";
                        AutoDTable.Rows[intK]["OFF_A"] = "0";
                        AutoDTable.Rows[intK]["OFF_T+"] = "0";
                        AutoDTable.Rows[intK]["OFF_T"] = "0";
                        AutoDTable.Rows[intK]["OFF_Total"] = "0";
                    }

                    intK = intK + 1;    
                }
            }     
        }

        grid.DataSource = AutoDTable;
        grid.DataBind();

        string attachment = "attachment;filename=ON OFF DUTY REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">ON OFF DUTY REPORT &nbsp;&nbsp;&nbsp;</a>");

        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\"> DATE -" + FromDate + "</a>");
        Response.Write("&nbsp;&nbsp;&nbsp;");
        //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");

        Response.Write(stw.ToString());
        string Pay_RowCount = "0";
        //   Pay_RowCount = Convert.ToDecimal(grid.Rows.Count + 5).ToString();

        //Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
        //Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");


        //Response.Write("</tr>");
        //Response.Write("</table>");
        //Response.Write("<table>");
        Response.End();
        Response.Clear();
    }
    public void NonAdminhrreportNewReliveConsolidate(string FromDate)
    {

        if (SessionUserType == "2")
        {
            NonAdminhrreportConsolidate(FromDate);
        }
        else
        {

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            string count = "0";
            string add = "0";
            string sub = "0";
            string finaltotal = "0";
            string DeptCode = "";
            string LabCount = "0";

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("ALLOT");
            AutoDTable.Columns.Add("OPG");
            AutoDTable.Columns.Add("REL");
            AutoDTable.Columns.Add("ADD");
            AutoDTable.Columns.Add("TOTAL");
            SSQL = "";
            SSQL = SSQL + "select DeptName,DeptCode,'0' as Allot_Total,'0' as Additional,'0' as Relive,";
            SSQL = SSQL + "(SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total ";
            SSQL = SSQL + "from ( select DM.DeptName,DM.DeptCode,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five ";
            SSQL = SSQL + "from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + "inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode ";
            SSQL = SSQL + "where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'  and ";
            SSQL = SSQL + "CONVERT(datetime,DOJ,103)<=CONVERT(datetime,'" + FromDate + "',103) ";
            SSQL = SSQL + "group by DM.DeptName,GM.GradeName,DM.DeptCode ";
            SSQL = SSQL + ") as PPV group by DeptName,DeptCode ";
            // SSQL = " Select DeptCode,DeptName from Department_Mst  where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' order by DeptCode ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count != 0)
            {
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    count = "0";
                    add = "0";
                    sub = "0";
                    finaltotal = "0";

                    SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And DeptCode='" + dt.Rows[k]["DeptCode"].ToString() + "'";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt1.Rows.Count > 0) { LabCount = dt1.Rows[0]["Allot"].ToString(); }
                    else { LabCount = "0"; }

                    SSQL = "";
                    SSQL = SSQL + "Select COUNT(*) as New_Join from Employee_Mst where IsActive='Yes' and DeptName='" + dt.Rows[k]["DeptCode"].ToString() + "'and CompCode='" + SessionCcode.ToString() + "' ";
                    SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,DOJ, 103)=CONVERT(Datetime,'" + FromDate + "',103) ";
                    dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt2.Rows.Count > 0) { add = dt2.Rows[0]["New_Join"].ToString(); }
                    else { add = "0"; }

                    SSQL = "Select count(RelieveDate)as RelieveDate  from Employee_Mst   where IsActive='No' and DeptName='" + dt.Rows[k]["DeptCode"].ToString() + "'and CompCode='" + SessionCcode.ToString() + "' ";
                    SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,RelieveDate, 103)=CONVERT(Datetime,'" + FromDate + "',103) ";
                    dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt3.Rows.Count > 0) { sub = dt3.Rows[0]["RelieveDate"].ToString(); }
                    else { sub = "0"; }

                    finaltotal = ((Convert.ToDecimal(dt.Rows[k]["Final_Total"].ToString()) + Convert.ToDecimal(add)) - Convert.ToDecimal(sub)).ToString();
                    //finaltotal = count + add - sub;

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[k]["DeptName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = dt.Rows[k]["Final_Total"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = LabCount;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = add;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = sub;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TOTAL"] = finaltotal;

                }
            }



            double Sum = 0;

            int TotCount = 0;

            TotCount = AutoDTable.Rows.Count;
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[TotCount]["REL"] = "GTotal";


            for (int j = 0; j < AutoDTable.Rows.Count - 1; j++)
            {

                //AutoDTable.Columns.Add("A1 No's");
                //AutoDTable.Columns.Add("A1 Days");
                //AutoDTable.Columns.Add("A1 Amt");

                Sum += Convert.ToDouble(AutoDTable.Rows[j]["TOTAL"].ToString());


            }

            AutoDTable.Rows[TotCount]["TOTAL"] = Sum;

            Sum = 0;
            DataCell.Columns.Add("S.NO");
            DataCell.Columns.Add("BrokerName");
            DataCell.Columns.Add("DeptName");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("GradeName");
            DataCell.Columns.Add("DOJ");
            DataCell.Columns.Add("RejoinDate");
            DataCell.Columns.Add("RelieveDate");
            DataCell.Columns.Add("LeaveFrom1");
            DataCell.Columns.Add("LeaveTo1");
            DataCell.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom1,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo1,103) as LeaveTo1,EM.ReliveReason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='Yes' ";
            SSQL = SSQL + " and CONVERT(datetime,DOJ,103)=CONVERT(datetime,'" + FromDate + "',103) order by RelieveDate Asc";

            dt4 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt4.Rows.Count > 0)
            {
                int srno = 1;
                for (int d = 0; d < dt4.Rows.Count; d++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[d]["S.NO"] = srno;
                    DataCell.Rows[d]["BrokerName"] = dt4.Rows[d]["AgentName"].ToString();
                    DataCell.Rows[d]["DeptName"] = dt4.Rows[d]["DeptName"].ToString();
                    DataCell.Rows[d]["MachineID"] = dt4.Rows[d]["EmpNo"].ToString();
                    DataCell.Rows[d]["Name"] = dt4.Rows[d]["FirstName"].ToString();
                    DataCell.Rows[d]["GradeName"] = dt4.Rows[d]["GradeName"].ToString();
                    DataCell.Rows[d]["DOJ"] = dt4.Rows[d]["DOJ"].ToString();
                    DataCell.Rows[d]["RejoinDate"] = dt4.Rows[d]["RejoinDate"].ToString();
                    DataCell.Rows[d]["RelieveDate"] = dt4.Rows[d]["RelieveDate"].ToString();
                    DataCell.Rows[d]["LeaveFrom1"] = dt4.Rows[d]["LeaveFrom1"].ToString();
                    DataCell.Rows[d]["LeaveTo1"] = dt4.Rows[d]["LeaveTo1"].ToString();
                    DataCell.Rows[d]["ReliveReason"] = dt4.Rows[d]["ReliveReason"].ToString();
                    srno = srno + 1;

                }
            }

            DataCells.Columns.Add("S.NO");
            DataCells.Columns.Add("BrokerName");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("GradeName");
            DataCells.Columns.Add("DOJ");
            DataCells.Columns.Add("RejoinDate");
            DataCells.Columns.Add("RelieveDate");
            DataCells.Columns.Add("LeaveFrom1");
            DataCells.Columns.Add("LeaveTo1");
            DataCells.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom1,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo1,103) as LeaveTo1,EM.ReliveReason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='No' ";
            SSQL = SSQL + " and CONVERT(datetime,RelieveDate,103)=CONVERT(datetime,'" + FromDate + "',103) order by RelieveDate Asc";

            dt5 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt5.Rows.Count > 0)
            {
                int srnoo = 1;
                for (int d = 0; d < dt5.Rows.Count; d++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[d]["S.NO"] = srnoo;
                    DataCells.Rows[d]["BrokerName"] = dt5.Rows[d]["AgentName"].ToString();
                    DataCells.Rows[d]["DeptName"] = dt5.Rows[d]["DeptName"].ToString();
                    DataCells.Rows[d]["MachineID"] = dt5.Rows[d]["EmpNo"].ToString();
                    DataCells.Rows[d]["Name"] = dt5.Rows[d]["FirstName"].ToString();
                    DataCells.Rows[d]["GradeName"] = dt5.Rows[d]["GradeName"].ToString();
                    DataCells.Rows[d]["DOJ"] = dt5.Rows[d]["DOJ"].ToString();
                    DataCells.Rows[d]["RejoinDate"] = dt5.Rows[d]["RejoinDate"].ToString();
                    DataCells.Rows[d]["RelieveDate"] = dt5.Rows[d]["RelieveDate"].ToString();
                    DataCells.Rows[d]["LeaveFrom1"] = dt5.Rows[d]["LeaveFrom1"].ToString();
                    DataCells.Rows[d]["LeaveTo1"] = dt5.Rows[d]["LeaveTo1"].ToString();
                    DataCells.Rows[d]["ReliveReason"] = dt5.Rows[d]["ReliveReason"].ToString();
                    srnoo = srnoo + 1;

                }
            }
            //   grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HR CONSOLIDATED REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">HR CONSOLIDATED REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");



            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td rowspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Rept.No:4</a>");

            //Response.Write("</td>");
            //Response.Write("<td rowspan='2'>");
            //Response.Write("<a style=\"font-weight:bold\">Details</a>");

            Response.Write("</td>");


            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");


            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">HR CONSOLDT RPT</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\">RPTG TIME : :</a>");
            Response.Write("</td>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DEPT</a>");

            Response.Write("</td>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");

            Response.Write("</td>");
            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">OPG</a>");

            Response.Write("</td>");


            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">REL</a>");

            Response.Write("</td>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">ADD</a>");

            Response.Write("</td>");
            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");

            Response.Write("</td>");

            Response.Write("</tr>");
            for (int k = 0; k < AutoDTable.Rows.Count; k++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DEPT"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ALLOT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["OPG"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["REL"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["ADD"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TOTAL"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }

            //Joining Process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">NEW N EXST JOINERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            Response.Write("</tr>");

            for (int l = 0; l < DataCell.Rows.Count; l++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");




                Response.Write("</tr>");

            }

            //Reliving process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">RELIVERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");




            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");
            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");
            Response.Write("</tr>");

            for (int n = 0; n < DataCells.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }
    }
    public void NonAdminhrreportConsolidate(string FromDate)
    {

        if (SessionUserType == "2")
        {
            NonAdminhrreportConsolidate(FromDate);
        }
        else
        {

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            string count = "0";
            string add = "0";
            string sub = "0";
            string finaltotal = "0";
            string DeptCode = "";
            string LabCount = "0";

            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("ALLOT");
            AutoDTable.Columns.Add("OPG");
            AutoDTable.Columns.Add("REL");
            AutoDTable.Columns.Add("ADD");
            AutoDTable.Columns.Add("TOTAL");
            SSQL = "";
            SSQL = SSQL + "select DeptName,DeptCode,'0' as Allot_Total,'0' as Additional,'0' as Relive,";
            SSQL = SSQL + "(SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total ";
            SSQL = SSQL + "from ( select DM.DeptName,DM.DeptCode,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,";
            SSQL = SSQL + "CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five ";
            SSQL = SSQL + "from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + "inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode ";
            SSQL = SSQL + "where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'  and ";
            SSQL = SSQL + "CONVERT(datetime,DOJ,103)<=CONVERT(datetime,'" + FromDate + "',103) ";
            SSQL = SSQL + "group by DM.DeptName,GM.GradeName,DM.DeptCode ";
            SSQL = SSQL + ") as PPV group by DeptName,DeptCode ";
            // SSQL = " Select DeptCode,DeptName from Department_Mst  where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' order by DeptCode ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count != 0)
            {
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    count = "0";
                    add = "0";
                    sub = "0";
                    finaltotal = "0";

                    SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And DeptCode='" + dt.Rows[k]["DeptCode"].ToString() + "'";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt1.Rows.Count > 0) { LabCount = dt1.Rows[0]["Allot"].ToString(); }
                    else { LabCount = "0"; }

                    SSQL = "";
                    SSQL = SSQL + "Select COUNT(*) as New_Join from Employee_Mst where IsActive='Yes' and DeptName='" + dt.Rows[k]["DeptCode"].ToString() + "'and CompCode='" + SessionCcode.ToString() + "' ";
                    SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,DOJ, 103)=CONVERT(Datetime,'" + FromDate + "',103) ";
                    dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt2.Rows.Count > 0) { add = dt2.Rows[0]["New_Join"].ToString(); }
                    else { add = "0"; }

                    SSQL = "Select count(RelieveDate)as RelieveDate  from Employee_Mst   where IsActive='No' and DeptName='" + dt.Rows[k]["DeptCode"].ToString() + "'and CompCode='" + SessionCcode.ToString() + "' ";
                    SSQL = SSQL + " and LocCode='" + SessionLcode.ToString() + "' and CONVERT(DATETIME,RelieveDate, 103)=CONVERT(Datetime,'" + FromDate + "',103) ";
                    dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt3.Rows.Count > 0) { sub = dt3.Rows[0]["RelieveDate"].ToString(); }
                    else { sub = "0"; }

                    finaltotal = ((Convert.ToDecimal(dt.Rows[k]["Final_Total"].ToString()) + Convert.ToDecimal(add)) - Convert.ToDecimal(sub)).ToString();
                    //finaltotal = count + add - sub;

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[k]["DeptName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = dt.Rows[k]["Final_Total"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = LabCount;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = add;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = sub;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TOTAL"] = finaltotal;

                }
            }



            double Sum = 0;

            int TotCount = 0;

            TotCount = AutoDTable.Rows.Count;
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[TotCount]["DEPT"] = "GTotal";


            for (int j = 0; j < AutoDTable.Rows.Count - 1; j++)
            {

                //AutoDTable.Columns.Add("A1 No's");
                //AutoDTable.Columns.Add("A1 Days");
                //AutoDTable.Columns.Add("A1 Amt");

                Sum += Convert.ToDouble(AutoDTable.Rows[j]["TOTAL"].ToString());


            }

            AutoDTable.Rows[TotCount]["TOTAL"] = Sum;

            Sum = 0;
            DataCell.Columns.Add("S.NO");
            DataCell.Columns.Add("BrokerName");
            DataCell.Columns.Add("DeptName");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("GradeName");
            DataCell.Columns.Add("DOJ");
            DataCell.Columns.Add("RejoinDate");
            DataCell.Columns.Add("RelieveDate");
            DataCell.Columns.Add("LeaveFrom1");
            DataCell.Columns.Add("LeaveTo1");
            DataCell.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom1,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo1,103) as LeaveTo1,EM.ReliveReason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='Yes' ";
            SSQL = SSQL + " and CONVERT(datetime,DOJ,103)=CONVERT(datetime,'" + FromDate + "',103) order by RelieveDate Asc";

            dt4 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt4.Rows.Count > 0)
            {
                int srno = 1;
                for (int d = 0; d < dt4.Rows.Count; d++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[d]["S.NO"] = srno;
                    DataCell.Rows[d]["BrokerName"] = dt4.Rows[d]["AgentName"].ToString();
                    DataCell.Rows[d]["DeptName"] = dt4.Rows[d]["DeptName"].ToString();
                    DataCell.Rows[d]["MachineID"] = dt4.Rows[d]["EmpNo"].ToString();
                    DataCell.Rows[d]["Name"] = dt4.Rows[d]["FirstName"].ToString();
                    DataCell.Rows[d]["GradeName"] = dt4.Rows[d]["GradeName"].ToString();
                    DataCell.Rows[d]["DOJ"] = dt4.Rows[d]["DOJ"].ToString();
                    DataCell.Rows[d]["RejoinDate"] = dt4.Rows[d]["RejoinDate"].ToString();
                    DataCell.Rows[d]["RelieveDate"] = dt4.Rows[d]["RelieveDate"].ToString();
                    DataCell.Rows[d]["LeaveFrom1"] = dt4.Rows[d]["LeaveFrom1"].ToString();
                    DataCell.Rows[d]["LeaveTo1"] = dt4.Rows[d]["LeaveTo1"].ToString();
                    DataCell.Rows[d]["ReliveReason"] = dt4.Rows[d]["ReliveReason"].ToString();
                    srno = srno + 1;

                }
            }

            DataCells.Columns.Add("S.NO");
            DataCells.Columns.Add("BrokerName");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("GradeName");
            DataCells.Columns.Add("DOJ");
            DataCells.Columns.Add("RejoinDate");
            DataCells.Columns.Add("RelieveDate");
            DataCells.Columns.Add("LeaveFrom1");
            DataCells.Columns.Add("LeaveTo1");
            DataCells.Columns.Add("ReliveReason");

            SSQL = "";
            SSQL = SSQL + " Select DM.DeptName,AM.AgentName,EM.EmpNo,Em.FirstName,GM.GradeName,CONVERT(varchar(10),DOJ,103) as DOJ,  ";
            SSQL = SSQL + " CONVERT(varchar(10),RejoinDate,103) as RejoinDate,CONVERT(varchar(10),RelieveDate,103) as RelieveDate,";
            SSQL = SSQL + " CONVERT(varchar(10),LeaveFrom1,103) as LeaveFrom1,CONVERT(varchar(10),LeaveTo1,103) as LeaveTo1,EM.ReliveReason";
            SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
            SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.LocCode='" + SessionLcode.ToString() + "' and GM.LocCode='" + SessionLcode.ToString() + "' and DM.LocCode='" + SessionLcode.ToString() + "' and AM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='No' ";
            SSQL = SSQL + " and CONVERT(datetime,RelieveDate,103)=CONVERT(datetime,'" + FromDate + "',103) order by RelieveDate Asc";

            dt5 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt5.Rows.Count > 0)
            {
                int srnoo = 1;
                for (int d = 0; d < dt5.Rows.Count; d++)
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[d]["S.NO"] = srnoo;
                    DataCells.Rows[d]["BrokerName"] = dt5.Rows[d]["AgentName"].ToString();
                    DataCells.Rows[d]["DeptName"] = dt5.Rows[d]["DeptName"].ToString();
                    DataCells.Rows[d]["MachineID"] = dt5.Rows[d]["EmpNo"].ToString();
                    DataCells.Rows[d]["Name"] = dt5.Rows[d]["FirstName"].ToString();
                    DataCells.Rows[d]["GradeName"] = dt5.Rows[d]["GradeName"].ToString();
                    DataCells.Rows[d]["DOJ"] = dt5.Rows[d]["DOJ"].ToString();
                    DataCells.Rows[d]["RejoinDate"] = dt5.Rows[d]["RejoinDate"].ToString();
                    DataCells.Rows[d]["RelieveDate"] = dt5.Rows[d]["RelieveDate"].ToString();
                    DataCells.Rows[d]["LeaveFrom1"] = dt5.Rows[d]["LeaveFrom1"].ToString();
                    DataCells.Rows[d]["LeaveTo1"] = dt5.Rows[d]["LeaveTo1"].ToString();
                    DataCells.Rows[d]["ReliveReason"] = dt5.Rows[d]["ReliveReason"].ToString();
                    srnoo = srnoo + 1;

                }
            }
            //   grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HR CONSOLIDATED REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">HR CONSOLIDATED REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");



            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td rowspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Rept.No:4</a>");

            //Response.Write("</td>");
            //Response.Write("<td rowspan='2'>");
            //Response.Write("<a style=\"font-weight:bold\">Details</a>");

            Response.Write("</td>");


            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");


            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">HR CONSOLDT RPT</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\">RPTG TIME : :</a>");
            Response.Write("</td>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DEPT</a>");

            Response.Write("</td>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");

            Response.Write("</td>");
            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">OPG</a>");

            Response.Write("</td>");


            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">REL</a>");

            Response.Write("</td>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">ADD</a>");

            Response.Write("</td>");
            Response.Write("<td  >");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");

            Response.Write("</td>");

            Response.Write("</tr>");
            for (int k = 0; k < AutoDTable.Rows.Count; k++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DEPT"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ALLOT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["OPG"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["REL"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + AutoDTable.Rows[k]["ADD"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TOTAL"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }

            //Joining Process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">NEW N EXST JOINERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            Response.Write("</tr>");

            for (int l = 0; l < DataCell.Rows.Count; l++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCell.Rows[l]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");




                Response.Write("</tr>");

            }

            //Reliving process
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr>");

            Response.Write("</tr>");
            Response.Write("<tr>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "-" + SessionLcode.ToString() + "</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("<a style=\"font-weight:bold\">RELIVERS</a>");

            Response.Write("</td>");
            Response.Write("<td colspan='3' >");
            Response.Write("<a style=\"font-weight:bold\">DATE -" + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");




            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BrokerName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DeptName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">MachineID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">GradeName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">LR.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">R.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">N.JOI</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">F.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">T.DT</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");
            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");

            //Response.Write("<td >");
            //Response.Write("<a style=\"font-weight:bold\"></a>");
            //Response.Write("</td>");
            Response.Write("</tr>");

            for (int n = 0; n < DataCells.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["S.NO"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["BrokerName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DeptName"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["MachineID"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["Name"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["GradeName"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RejoinDate"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["RelieveDate"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveFrom1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["LeaveTo1"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + DataCells.Rows[n]["ReliveReason"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("</tr>");

            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }
    }
    public void NonAdminGetHRConsolidatesGroupWise_Changes()
    {

        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable GradeMst = new DataTable();

        AutoDTable.Columns.Add("DEPT");
        AutoDTable.Columns.Add("Allot");
        AutoDTable.Columns.Add("T");
        AutoDTable.Columns.Add("T+");
        AutoDTable.Columns.Add("A");
        AutoDTable.Columns.Add("A+");
        AutoDTable.Columns.Add("A++");
        AutoDTable.Columns.Add("Total");

        SSQL = "select AgentName,AgentID,SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,";
        SSQL = SSQL + " (SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total from (select DM.AgentName,DM.AgentID,CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
        SSQL = SSQL + " CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
        SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five";
        SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode inner join AgentMst DM on DM.AgentID=EM.AgentID and DM.LocCode=EM.LocCode";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " and GM.CompCode='" + SessionCcode.ToString() + "' And GM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " and DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='Yes' and EM.IsNonAdmin='1'";
        SSQL = SSQL + " group by DM.AgentName,GM.GradeName,DM.AgentID ) as PPV group by AgentName,AgentID Order by AgentID";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["AgentName"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt.Rows[i]["One"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt.Rows[i]["Two"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt.Rows[i]["Three"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt.Rows[i]["Four"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt.Rows[i]["Five"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = dt.Rows[i]["Final_Total"].ToString();

        }

        int TotCount = AutoDTable.Rows.Count;

        double Sum = 0;
        double Sum1 = 0;
        double Sum2 = 0;
        double Sum3 = 0;
        double Sum4 = 0;
        double Sum5 = 0;
        double Sum6 = 0;
        double Sum7 = 0;

        for (int m = 0; m < AutoDTable.Rows.Count; m++)
        {
            //Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
            Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
            Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
            Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
            Sum4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
            Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
            Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
        }
        AutoDTable.NewRow();
        AutoDTable.Rows.Add();
        AutoDTable.Rows[TotCount]["DEPT"] = "Total";
        //AutoDTable.Rows[TotCount]["Allot"] = Sum;
        AutoDTable.Rows[TotCount]["T"] = Sum1;
        AutoDTable.Rows[TotCount]["T+"] = Sum2;
        AutoDTable.Rows[TotCount]["A"] = Sum3;
        AutoDTable.Rows[TotCount]["A+"] = Sum4;
        AutoDTable.Rows[TotCount]["A++"] = Sum5;
        AutoDTable.Rows[TotCount]["Total"] = Sum6;



        grid.DataSource = AutoDTable;
        grid.DataBind();
        string attachment = "attachment;filename=HR CONSOLIDATED - GROUP WISE REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='14'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='14'>");
        Response.Write("<a style=\"font-weight:bold\"> HR CONSOLIDATED - GROUP WISE REPORT &nbsp;&nbsp;&nbsp;</a>");

        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">T</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">T+</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A+</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A++</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2' >");
        Response.Write("<a style=\"font-weight:bold\">Total</a>");

        Response.Write("</td>");
        Response.Write("</tr>");

        for (int k = 0; k < AutoDTable.Rows.Count; k++)
        {

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++"].ToString() + " </a>");
            Response.Write("</td>");


            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
            Response.Write("</td>");
            Response.Write("</tr>");


        }

        Response.Write("</table>");

        // Response.Write(stw.ToString());
        Response.End();
        Response.Clear();

    }
    public void NonAdminGetLeaderCommission_Changes(string BrokerName, string FromDate, string ToDate)
    {
        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();


        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("EmpNo");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("Grade");
        AutoDTable.Columns.Add("Group");
        AutoDTable.Columns.Add("Days");
        AutoDTable.Columns.Add("Amount");
        AutoDTable.Columns.Add("AgentID");
        AutoDTable.Columns.Add("AgentName");


        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            //AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            daycount -= 1;
            daysAdded += 1;
        }

        SSQL = "Select AM.AgentID,AM.AgentName,EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(MC.CateName,'') as WageCategoty,EM.AgentName,MG.GradeName from Employee_Mst EM";
        SSQL = SSQL + " inner join AgentMst AM on EM.AgentID = AM.AgentID";
        SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where EM.IsActive='Yes' And (EM.AgentID !='0' or EM.AgentID !='-Select-') And EM.WageCategoty !='0'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And MC.CompCode='" + SessionCcode.ToString() + "' And MC.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And MG.CompCode='" + SessionCcode.ToString() + "' And MG.LocCode='" + SessionLcode.ToString() + "'";

        if (BrokerName != "0")
        {
            SSQL = SSQL + " And EM.BrokerName= '" + BrokerName + "'";
        }

        SSQL = SSQL + " And EM.EligibleLeader='1' And EM.IsNonAdmin='1'";

        SSQL = SSQL + "Group By  AM.AgentID,AM.AgentName,EM.EmpNo,EM.FirstName,MC.CateName,EM.BrokerName,MG.GradeName order by AM.AgentName";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        int TotCount;

        int IntK = 0;
        int SNo = 1;
        double Total;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["EmpNo"].ToString();

            AutoDTable.Rows[intK]["SNo"] = SNo;
            AutoDTable.Rows[intK]["EmpNo"] = dt.Rows[i]["EmpNo"].ToString();
            AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
            AutoDTable.Rows[intK]["Grade"] = dt.Rows[i]["GradeName"].ToString();
            AutoDTable.Rows[intK]["Group"] = dt.Rows[i]["WageCategoty"].ToString();
            AutoDTable.Rows[intK]["AgentID"] = dt.Rows[i]["AgentID"].ToString();
            AutoDTable.Rows[intK]["AgentName"] = dt.Rows[i]["AgentName"].ToString();


            int count = 4;
            double Total_Time_get = 0;
            double Total_Hours = 0;
            double Total_Days = 0;
            double TwelveHours = 0;
            double EightHours = 0;
            double FinalTotal = 0;

            for (int j = 0; j < daysAdded; j++)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                string Date1 = dayy.ToString("yyyy/MM/dd");

                SSQL = "select Total_Hrs from LogTime_Days where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,120) = CONVERT(datetime,'" + Convert.ToDateTime(dayy).ToString("yyyy/MM/dd") + "',120)";

                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt1.Rows.Count > 0)
                {
                    Total_Time_get = Convert.ToDouble(dt1.Rows[0]["Total_Hrs"].ToString());

                    Total_Hours = Total_Hours + Total_Time_get;

                    if (Total_Time_get >= 12)
                    {
                        SSQL = "Select Shift12 from AgentMst where AgentID='" + dt.Rows[i]["AgentID"].ToString() + "' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

                        TwelveHours = TwelveHours + Convert.ToDouble(dt2.Rows[0]["Shift12"].ToString());

                        Total_Days = Total_Days + 1;
                    }
                    else if (Total_Time_get >= 8 & Total_Time_get < 12)
                    {
                        SSQL = "Select Shift8 from AgentMst where AgentID='" + dt.Rows[i]["AgentID"].ToString() + "' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

                        EightHours = EightHours + Convert.ToDouble(dt3.Rows[0]["Shift8"].ToString());

                        Total_Days = Total_Days + 1;
                    }
                    else
                    {

                    }


                }
                else
                {
                    Total_Hours = Total_Hours + Total_Time_get;

                }


                Total_Time_get = 0;
            }

            FinalTotal = EightHours + TwelveHours;

            AutoDTable.Rows[intK]["Days"] = Total_Days;
            AutoDTable.Rows[intK]["Amount"] = FinalTotal;

            intK = intK + 1;

            SNo = SNo + 1;

            Total_Days = 0;
            EightHours = 0;
            TwelveHours = 0;
        }


        SSQL = " Select distinct AM.AgentID,AM.AgentName from Employee_Mst EM inner join AgentMst AM on EM.BrokerName=AM.AgentID ";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And EM.IsActive='Yes' And EM.EligibleLeader='1'";

        if (BrokerName != "0")
        {
            SSQL = SSQL + " And AM.AgentID ='" + BrokerName + "' ";
        }

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);



        grid.DataSource = AutoDTable;
        grid.DataBind();
        string attachment = "attachment;filename=LEADER COMMISSION REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='14'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='14'>");
        Response.Write("<a style=\"font-weight:bold\">LEADER COMMISSION REPORT &nbsp;&nbsp;&nbsp;</a>");

        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='14'>");
        Response.Write("<a style=\"font-weight:bold\"> From Date:" + FromDate + "- To Date:" + ToDate + "</a>");
        Response.Write("</td>");

        Response.Write("</tr>");



        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">SNo</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">EmpNo</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">EmpName</a>");
        Response.Write("</td>");

        //Grade

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">Grade</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">Group</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">Days</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">Amount</a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        //Response.Write("<tr Font-Bold='true' align='center'>");

        if (dt1.Rows.Count > 0)
        {

            for (int l = 0; l < dt1.Rows.Count; l++)
            {
                string DD = dt1.Rows[l]["AgentID"].ToString();

                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='14'>");
                Response.Write("<a style=\"font-weight:bold\">" + dt1.Rows[l]["AgentName"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("</tr>");
                decimal Sum = 0;
                decimal Sum1 = 0;

                for (int k = 0; k < AutoDTable.Rows.Count; k++)
                {
                    string DD1 = AutoDTable.Rows[k]["AgentID"].ToString();

                    if (DD == DD1)
                    {

                        Response.Write("<tr Font-Bold='true' align='center'>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["SNo"].ToString() + "</a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["EmpNo"].ToString() + " </a>");
                        Response.Write("</td>");


                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Name"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Grade"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Group"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Days"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td colspan='2'>");
                        Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Amount"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("</tr>");

                        Sum = Sum + Convert.ToDecimal(AutoDTable.Rows[k]["Days"].ToString());
                        Sum1 = Sum1 + Convert.ToDecimal(AutoDTable.Rows[k]["Amount"].ToString());

                    }
                }

                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\"></a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\"> </a>");
                Response.Write("</td>");


                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\"></a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\"></a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">Grand Total</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + Sum + "</a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\"> " + Sum1 + "</a>");
                Response.Write("</td>");

                Response.Write("</tr>");
            }
        }
        else
        {
            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\">No Data Found</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

        }



        Response.Write("</table>");
        //Response.Write(stw.ToString());
        Response.End();
        Response.Clear();

    }
    public void NonAdminGetHRConsolidatesDeptWise_Changes()
    {
        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable GradeMst = new DataTable();

        AutoDTable.Columns.Add("DEPT");
        AutoDTable.Columns.Add("Allot");
        AutoDTable.Columns.Add("T");
        AutoDTable.Columns.Add("T+");
        AutoDTable.Columns.Add("A");
        AutoDTable.Columns.Add("A+");
        AutoDTable.Columns.Add("A++");
        AutoDTable.Columns.Add("Total");

        SSQL = "select DeptName,DeptCode,'0' as Allot_Total,SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,";
        SSQL = SSQL + " (SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)) as Final_Total from (select DM.DeptName,DM.DeptCode,CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
        SSQL = SSQL + " CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
        SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five";
        SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode inner join Department_Mst DM on DM.DeptCode=EM.DeptName and DM.LocCode=EM.LocCode";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " and GM.CompCode='" + SessionCcode.ToString() + "' And GM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " and DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "' and EM.IsActive='Yes' and  EM.IsNonAdmin='1'";
        SSQL = SSQL + " group by DM.DeptName,GM.GradeName,DM.DeptCode ) as PPV group by DeptName,DeptCode Order by DeptCode";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["DeptName"].ToString();

            SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Allot"] = dt1.Rows[0]["Allot"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt.Rows[i]["One"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt.Rows[i]["Two"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt.Rows[i]["Three"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt.Rows[i]["Four"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt.Rows[i]["Five"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = dt.Rows[i]["Final_Total"].ToString();

        }

        int TotCount = AutoDTable.Rows.Count;

        double Sum = 0;
        double Sum1 = 0;
        double Sum2 = 0;
        double Sum3 = 0;
        double Sum4 = 0;
        double Sum5 = 0;
        double Sum6 = 0;
        double Sum7 = 0;

        for (int m = 0; m < AutoDTable.Rows.Count; m++)
        {
            Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
            Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
            Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
            Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
            Sum4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
            Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
            Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
        }
        AutoDTable.NewRow();
        AutoDTable.Rows.Add();
        AutoDTable.Rows[TotCount]["DEPT"] = "Total";
        AutoDTable.Rows[TotCount]["Allot"] = Sum;
        AutoDTable.Rows[TotCount]["T"] = Sum1;
        AutoDTable.Rows[TotCount]["T+"] = Sum2;
        AutoDTable.Rows[TotCount]["A"] = Sum3;
        AutoDTable.Rows[TotCount]["A+"] = Sum4;
        AutoDTable.Rows[TotCount]["A++"] = Sum5;
        AutoDTable.Rows[TotCount]["Total"] = Sum6;

        grid.DataSource = AutoDTable;
        grid.DataBind();
        string attachment = "attachment;filename=HR CONSOLIDATED - DEPT WISE REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='16'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='16'>");
        Response.Write("<a style=\"font-weight:bold\"> HR CONSOLIDATED - DEPT WISE REPORT &nbsp;&nbsp;&nbsp;</a>");

        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
        Response.Write("</td>");

        //Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">Allot</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">T</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">T+</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A+</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A++</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2' >");
        Response.Write("<a style=\"font-weight:bold\">Total</a>");

        Response.Write("</td>");
        Response.Write("</tr>");

        for (int k = 0; k < AutoDTable.Rows.Count; k++)
        {

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
            Response.Write("</td>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Allot"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++"].ToString() + " </a>");
            Response.Write("</td>");


            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
            Response.Write("</td>");
            Response.Write("</tr>");


        }

        Response.Write("</table>");

        // Response.Write(stw.ToString());
        Response.End();
        Response.Clear();


    }
    public void NonAdminGetHRConsolidatesDeptGroupWise_Changes()
    {

        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable LabourAllot = new DataTable();
        DataTable GradeMst = new DataTable();

        AutoDTable.Columns.Add("DEPT");
        AutoDTable.Columns.Add("Allot");

        SSQL = "Select AM.AgentID,AM.AgentName from Employee_Mst EM inner join AgentMst AM on EM.BrokerName = AM.AgentID  And AM.LocCode = EM.LocCode  ";
        SSQL = SSQL + " where EM.IsActive='Yes' And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And BrokerName !='0' Group By  AM.AgentID,AM.AgentName Order by AM.AgentID Asc";

        GradeMst = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i1 = 0; i1 < GradeMst.Rows.Count; i1++)
        {
            AutoDTable.Columns.Add(GradeMst.Rows[i1]["AgentName"].ToString());

        }

        AutoDTable.Columns.Add("Total");

        SSQL = "Select DM.DeptCode,DM.DeptName from Employee_Mst EM inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignNo";
        SSQL = SSQL + " Inner Join Department_Mst DM on EM.DeptName = DM.DeptCode And EM.LocCode = DM.LocCode And LM.LocCode = EM.LocCode";
        SSQL = SSQL + " Where EM.IsActive='Yes' and EM.IsNonAdmin='1' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And LM.CompCode='" + SessionCcode.ToString() + "' And LM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " Group By  DM.DeptCode,DM.DeptName Order by DM.DeptCode Asc ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        int TotCount;

        int IntK = 0;
        double Total;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[IntK]["DEPT"] = dt.Rows[i]["DeptName"].ToString();

            SSQL = "select isnull(sum(Total),0) As Allot from LabourAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            AutoDTable.Rows[IntK]["Allot"] = dt1.Rows[0]["Allot"].ToString();
            Total = 0;

            double Total1 = 0;

            int Count = 2;

            for (int i1 = 0; i1 < GradeMst.Rows.Count; i1++)
            {
                SSQL = "select COUNT(EM.EmpNo) As EmpNo from Employee_Mst EM inner join  MstGrade MG  on EM.Grade= MG.GradeID And EM.CompCode = MG.CompCode And MG.LocCode = EM.LocCode";
                SSQL = SSQL + " Where  EM.DeptName='" + dt.Rows[i]["DeptCode"].ToString() + "' And EM.BrokerName='" + GradeMst.Rows[i1]["AgentID"].ToString() + "'";
                SSQL = SSQL + " And EM.CompCode ='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And MG.CompCode ='" + SessionCcode.ToString() + "' And MG.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And EM.IsActive='Yes'";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt1.Rows.Count >= 0)
                {
                    Total1 = Convert.ToDouble(dt1.Rows[0]["EmpNo"].ToString());

                    if (Total1 != 0)
                    {
                        AutoDTable.Rows[IntK][Count] = dt1.Rows[0]["EmpNo"].ToString();
                    }
                    else
                    {
                        AutoDTable.Rows[IntK][Count] = "0";

                    }
                }
                else
                {
                    AutoDTable.Rows[IntK][Count] = "0";

                }

                Total = Total + Total1;
                Count = Count + 1;

            }

            AutoDTable.Rows[IntK]["Total"] = Total;
            IntK = IntK + 1;



        }

        double Sum = 0;
        double Sum1 = 0;
        double Sum2 = 0;

        TotCount = AutoDTable.Rows.Count;
        AutoDTable.NewRow();
        AutoDTable.Rows.Add();

        AutoDTable.Rows[TotCount]["DEPT"] = "Total";

        for (int Col = 0; Col < GradeMst.Rows.Count; Col++)
        {
            string SS = GradeMst.Rows[Col]["AgentName"].ToString();
            for (int j = 0; j < AutoDTable.Rows.Count - 1; j++)
            {
                Sum += Convert.ToDouble(AutoDTable.Rows[j][SS].ToString());
                Sum1 += Convert.ToDouble(AutoDTable.Rows[j]["Total"].ToString());
                Sum2 += Convert.ToDouble(AutoDTable.Rows[j]["Allot"].ToString());
            }

            AutoDTable.Rows[TotCount]["Allot"] = Sum2;
            AutoDTable.Rows[TotCount][GradeMst.Rows[Col]["AgentName"].ToString()] = Sum;
            AutoDTable.Rows[TotCount]["Total"] = Sum1;
            Sum = 0;
            Sum1 = 0;
            Sum2 = 0;
        }


        grid.DataSource = AutoDTable;
        grid.DataBind();
        string attachment = "attachment;filename=HRConsolidates_Dept_Group_Wise.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write(" &nbsp;&nbsp;");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">HR CONSOLIDATED - DEPT GROUP WISE</a>");
        Response.Write(" &nbsp;&nbsp;&nbsp; ");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
    }
}