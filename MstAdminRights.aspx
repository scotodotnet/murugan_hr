<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstAdminRights.aspx.cs" Inherits="MstAdminRights" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Admin Rights</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Admin Rights </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Admin Rights</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>User Type</label>
								  <asp:DropDownList ID="txtUserType" runat="server" class="form-control select2" Width="100%">
								    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                    <asp:ListItem Value="2" Text="IF User"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Normal User"></asp:ListItem>
                                 </asp:DropDownList>
								 <asp:RequiredFieldValidator ControlToValidate="txtUserType" Display="Dynamic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-2" style="margin-top:29px;">
                                 <div class="form-group">
                                  <asp:CheckBox ID="chkSalaryConfirm" runat="server"  /> Salary Confirm
                                </div>
                              </div>
                              <div class="col-md-2" style="margin-top:29px;">
                                 <div class="form-group">
                                  <asp:CheckBox ID="chkManualAttnd" runat="server"  /> Manual Attendance 
                                </div>
                              </div>
                              <div class="col-md-2" style="margin-top:29px;">
                                 <div class="form-group">
                                  <asp:CheckBox ID="chkMedical" runat="server"  /> Medical
                                </div>
                              </div>
                               </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                            <div class="col-md-4"></div>
                               <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"   />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                            
                              </div>
                        <!-- end row -->
                    
                  
                       
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>

