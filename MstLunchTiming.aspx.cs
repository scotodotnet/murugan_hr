﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstLunchTiming : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Lunch Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");


           
            Load_Shift();
            Load_WagesType();
           
            Load_Department();
            Load_Designation();
        }
        Load_Data_Incentive();
       
    }



    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }

    private void Load_Designation()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDesignation.Items.Clear();
        query = "Select *from Designation_Mst where DeptName='" + ddlDepartment.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDesignation.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DesignName"] = "-Select-";
        dr["DesignName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {

       
       
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Designation();
    }
    
//CREATE TABLE [dbo].[LunchTiming_Mst](
//    [Ccode] [nvarchar](50) NULL,
//    [Lcode] [nvarchar](50) NULL,
//    [Wages] [nvarchar](50) NULL,
//    [DeptCode] [nvarchar](50) NULL,
//    [DeptName] [nvarchar](250) NULL,
//    [Designation] [nvarchar](250) NULL,
//    [LunchTiming] [decimal](18, 2) NULL
	
//) ON [PRIMARY]

//GO

    protected void btnLunchTimingSave_Click(object sender, EventArgs e)
    {
        SSQL = "Delete from LunchTiming_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + ddlDepartment.SelectedValue + "' And Designation='" + ddlDesignation.SelectedValue + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Insert into LunchTiming_Mst(Ccode,Lcode,Wages,DeptCode,DeptName,Designation,LunchTiming)values('" + SessionCcode + "','" + SessionLcode + "',";
        SSQL = SSQL + "'" + ddlWages.SelectedItem.Text + "','" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "','" + ddlDesignation.SelectedItem.Text + "',";
        SSQL = SSQL + "'" + txtLunchTiming.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully.!');", true);
        //Load_OTData();
        Load_Data_Incentive();
        Clear();
    }

    protected void btnLunchTimingClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        ddlWages.SelectedValue = "-Select-";
        txtLunchTiming.Text = "0";

        ddlDepartment.SelectedValue = "0";
        ddlDesignation.SelectedValue = "-Select-";
        //Load_OTData();
        btnSave.Text = "Save";
        Load_Data_Incentive();
    }

    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from LunchTiming_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }


    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        string[] DeptDetails;
        DeptDetails = e.CommandName.Split(',');


        query = "select * from LunchTiming_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + DeptDetails[0] + "' And Designation='" + DeptDetails[1] + "' And Wages='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            ddlWages.SelectedValue = DT.Rows[0]["Wages"].ToString();
            ddlDepartment.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            ddlDepartment_SelectedIndexChanged(sender, e);
            ddlDesignation.SelectedValue = DT.Rows[0]["Designation"].ToString();
            txtLunchTiming.Text = DT.Rows[0]["LunchTiming"].ToString();
           
            btnSave.Text = "Update";
        }


    }


    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string[] DeptDetails;
        DeptDetails = e.CommandName.Split(',');


        string query;
        DataTable DT = new DataTable();


        query = "select * from LunchTiming_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + DeptDetails[0] + "' And Designation='" + DeptDetails[1] + "' And Wages='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from LunchTiming_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "' And Designation='" + e.CommandArgument.ToString() + "' And Wages='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lunch Timing Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }




    private void Load_Data_LunchTime()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Shift,StartTime,EndTime from Mst_Lunch where CompCode='"+SessionCcode +"' and LocCode='"+SessionLcode +"'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlShift.Items.Clear();
        query = "Select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='"+SessionLcode +"'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftDesc"] = "-Select-";
        dr["ShiftDesc"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();

    }

    //protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    //{
    //    string query;
    //    DataTable DT = new DataTable();


    //    query = "select * from Mst_Lunch where Shift='" + e.CommandArgument.ToString() + "' and LocCode='"+SessionLcode +"' and CompCode='"+SessionCcode +"'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);

    //    if (DT.Rows.Count > 0)
    //    {
    //        query = "delete from Mst_Lunch where  Shift='" + e.CommandArgument.ToString() + "' and LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "' ";
    //        objdata.RptEmployeeMultipleDetails(query);

    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lunch Timing  Deleted Successfully...!');", true);
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
    //    }
    //    Load_Data_LunchTime();
    //}
    //protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    //{
    //    string query;
    //    DataTable DT = new DataTable();


    //    query = "select * from Mst_Lunch where  Shift='" + e.CommandArgument.ToString() + "'and LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);

    //    if (DT.Rows.Count > 0)
    //    {
    //         ddlShift.SelectedValue = DT.Rows[0]["Shift"].ToString();
    //        txtStartTime.Text = DT.Rows[0]["StartTime"].ToString();
    //        txtEndTime.Text = DT.Rows[0]["EndTime"].ToString();


    //        btnlunchSave.Text = "Update";
    //    }
    //    else
    //    {
    //        ddlShift.SelectedValue = "-Select-";
    //        txtStartTime.Text = "";
    //        txtEndTime.Text = "";

    //        btnlunchSave.Text = "Save";
    //    }
    //}
    protected void btnlunchSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        query = "select * from Mst_Lunch where Shift='" + ddlShift.SelectedItem.Text + "' And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            SaveMode = "Update";
            query = "delete from Mst_Lunch where Shift='" + ddlShift.SelectedItem.Text + "' And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }


        query = "Insert into Mst_Lunch(CompCode,LocCode,Shift,StartTime,EndTime)";
        query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + ddlShift.SelectedItem.Text + "','" + txtStartTime.Text + "',";
        query = query + " '" + txtEndTime.Text + "' ) ";
        objdata.RptEmployeeMultipleDetails(query);

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lunch Timing Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lunch Timing Saved Successfully...!');", true);
        }

        Load_Data_LunchTime();
      //  Clear_All_Field();
    }
    protected void btnlunchClear_Click(object sender, EventArgs e)
    {
        txtLateIN.Text = "";
        txtimproper.Text = "";
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        query = "select * from Mst_LunchLink where  CompCode='" + SessionCcode  + "' and LocCode='"+SessionLcode +"'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            SaveMode = "Update";
            query = "delete from Mst_LunchLink where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }


        query = "Insert into Mst_LunchLink(CompCode,LocCode,LateIN,Improper)";
        query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtLateIN.Text + "','" + txtimproper.Text + "') ";
        
        objdata.RptEmployeeMultipleDetails(query);

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lunch Timing Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lunch Timing Saved Successfully...!');", true);
        }

        Load_Data_LunchTime();
       // Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtStartTime.Text = "";
        txtEndTime.Text = "";
       
        ddlShift.SelectedValue = "-Select-";

        btnSave.Text = "Save";
    }

}
