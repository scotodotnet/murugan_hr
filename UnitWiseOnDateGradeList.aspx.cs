﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class UnitWiseOnDateGradeList : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Wages = "";
    string FDate = "";
    string TDate = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    string Grade = "";
    int SumTotal = 0;
    int TotEmp = 0;
    int TotPresent = 0;
    string GradePlusPlus = "";
    string GradePlus = "";
    string GradeA = "";
    string GradeTPlus = "";
    string GradeT = "";
    string GradeONPercentage = "";

    int UpTotEmp = 0;
    int UpTotPresent = 0;
    string UpGradePlusPlus = "";
    string UpGradePlus = "";
    string UpGradeA = "";
    string UpGradeTPlus = "";
    string UpGradeT = "";
    string UpGradeONPercentage = "";
    string Totalsum = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    //ONDATE
    DataTable ReqTable = new DataTable();
    DataTable RequiredTable = new DataTable();
    DataTable EmpGrade = new DataTable();
    DataTable AttendanceAllo = new DataTable();
    DataTable AttendancePres = new DataTable();
    DataTable LatePres = new DataTable();
    DataTable LateNo = new DataTable();
    DataTable MissPunchPres = new DataTable();
    DataTable MissPunchnos = new DataTable();

    //UPDATE
    DataTable UpReqTable = new DataTable();
    DataTable UpEmpGrade = new DataTable();
    DataTable UpAttendanceAllo = new DataTable();
    DataTable UpAttendancePres = new DataTable();
    DataTable UpLatePres = new DataTable();
    DataTable UpLateNo = new DataTable();
    DataTable UpMissPunchPres = new DataTable();
    DataTable UpMissPunchnos = new DataTable();
    // DataTable MissPunchnos = new DataTable();
    DataTable EmpGradePercentage = new DataTable();
    DataTable DataCell = new DataTable();

    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Unit Wise OnDate Grade List";

            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();


            FDate = Request.QueryString["FromDate"].ToString();

            // Division = Request.QueryString["Division"].ToString();

            if (SessionUserType == "2")
            {
                //NonAdminGetAttdDayWise_Change();
            }
            else
            {
                GetUnitWise_Change();
            }
        }
    }
    public void GetUnitWise_Change()
    {
        DataCell.Columns.Add("SNO");
        DataCell.Columns.Add("Unit");
        DataCell.Columns.Add("Req");
        DataCell.Columns.Add("Avail");
        DataCell.Columns.Add("ON%");
        DataCell.Columns.Add("UP%");
        DataCell.Columns.Add("A++");
        DataCell.Columns.Add("A+");
        DataCell.Columns.Add("A");
        DataCell.Columns.Add("T+");
        DataCell.Columns.Add("T");
        DataCell.Columns.Add("ONGRADE%");
        DataCell.Columns.Add("UPGRADE%");

        DataCell.Columns.Add("Allo");
        DataCell.Columns.Add("AttendancePresent");
        DataCell.Columns.Add("ONATTENDANCE%");
        DataCell.Columns.Add("UPATTENDANCE%");
        DataCell.Columns.Add("Present");
        DataCell.Columns.Add("LateNos");
        DataCell.Columns.Add("ONLATE%");
        DataCell.Columns.Add("UPLATE%");
        DataCell.Columns.Add("MissPunchPresent");
        DataCell.Columns.Add("MissPunchNos");
        DataCell.Columns.Add("ONMISSPUCH%");
        DataCell.Columns.Add("UPMISSPUNCH%");


        //OnDatePercentage
        SSQL = "";
        SSQL = "select lm.LocCode as UNIT,Required as REQ,count(*) as AVAIL from LabourAllotment_Mst lm join Employee_Mst em on lm.LocCode=em.LocCode where em.IsActive='YES' group by lm.LocCode,lm.Required";
        ReqTable = objdata.RptEmployeeMultipleDetails(SSQL);


        if (ReqTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < ReqTable.Rows.Count; i++)
            {
                //ONDATE

                SSQL = "select lm.LocCode as UNIT,Required as REQ,count(*) as AVAIL from LabourAllotment_Mst lm join Employee_Mst em on lm.LocCode=em.LocCode where em.IsActive='YES' and lm.LocCode='" + ReqTable.Rows[i][0].ToString() + "' group by lm.LocCode,lm.Required";
                RequiredTable = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select  cast(Total as int) as cnt from LabourAllotment_Mst where LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                AttendanceAllo = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "select count(*) as Attndce from LogTime_Days where Attn_Date_str='" + FDate + "' and Present_Absent='Present' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                AttendancePres = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select count(*) as Present from LogTime_Days where Attn_Date_str='" + FDate + "' and Present_Absent='Present' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                LatePres = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "select count(*) as LateNos from LogTime_Days ld inner join Shift_Mst sm on ld.LocCode=sm.LocCode where  ld.Present_Absent='Present' and sm.StartTime < ld.TimeIN and ld.LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                LateNo = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select count(*) as Present from LogTime_Days where Attn_Date_str='" + FDate + "' and Present_Absent='Present' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                MissPunchPres = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "select count(*) as MisspunchNos from LogTime_Days where Attn_Date_str='" + FDate + "' and Present_Absent='Present' and TypeName='Improper' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                MissPunchnos = objdata.RptEmployeeMultipleDetails(SSQL);

                //UPDATE
                SSQL = "select cast(Total as int) as cnt from LabourAllotment_Mst where LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                UpAttendanceAllo = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "select count(*) as Attndce from LogTime_Days where  Present_Absent='Present' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                UpAttendancePres = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "select count(*) as Present from LogTime_Days where Present_Absent='Present' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                UpLatePres = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "select count(*) as LateNos from LogTime_Days ld inner join Shift_Mst sm on ld.LocCode=sm.LocCode where  ld.Present_Absent='Present' and sm.StartTime < ld.TimeIN and ld.LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                UpLateNo = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "select count(*) as Present from LogTime_Days where Present_Absent='Present' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                UpMissPunchPres = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "select count(*) as MisspunchNos from LogTime_Days where Present_Absent='Present' and TypeName='Improper' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                UpMissPunchnos = objdata.RptEmployeeMultipleDetails(SSQL);

                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[DataCell.Rows.Count - 1]["SNo"] = sno;
                DataCell.Rows[DataCell.Rows.Count - 1]["Unit"] = RequiredTable.Rows[0]["UNIT"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["Req"] = RequiredTable.Rows[0]["REQ"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["Avail"] = RequiredTable.Rows[0]["AVAIL"].ToString();
                //EmpGrade
                SSQL = "select count(*) as Grade from LogTime_Days where Attn_Date_Str='" + FDate + "'  and Grade='A++' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGrade = objdata.RptEmployeeMultipleDetails(SSQL);
                DataCell.Rows[DataCell.Rows.Count - 1]["A++"] = EmpGrade.Rows[0]["Grade"].ToString();

                SSQL = "select count(*) as Grade from LogTime_Days where Attn_Date_Str='" + FDate + "' and Grade='A+' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGrade = objdata.RptEmployeeMultipleDetails(SSQL);
                DataCell.Rows[DataCell.Rows.Count - 1]["A+"] = EmpGrade.Rows[0]["Grade"].ToString();

                SSQL = "select count(*) as Grade from LogTime_Days where Attn_Date_Str='" + FDate + "' and Grade='A' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGrade = objdata.RptEmployeeMultipleDetails(SSQL);
                DataCell.Rows[DataCell.Rows.Count - 1]["A"] = EmpGrade.Rows[0]["Grade"].ToString();

                SSQL = "select count(*) as Grade from LogTime_Days where Attn_Date_Str= '" + FDate + "' and Grade='T+' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGrade = objdata.RptEmployeeMultipleDetails(SSQL);
                DataCell.Rows[DataCell.Rows.Count - 1]["T+"] = EmpGrade.Rows[0]["Grade"].ToString();

                SSQL = "select count(*) as Grade from LogTime_Days where Attn_Date_Str= '" + FDate + "' and Grade='T' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGrade = objdata.RptEmployeeMultipleDetails(SSQL);
                DataCell.Rows[DataCell.Rows.Count - 1]["T"] = EmpGrade.Rows[0]["Grade"].ToString();

                DataCell.Rows[DataCell.Rows.Count - 1]["Allo"] = AttendanceAllo.Rows[0]["cnt"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["AttendancePresent"] = AttendancePres.Rows[0]["Attndce"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["Present"] = LatePres.Rows[0]["Present"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["LateNos"] = LateNo.Rows[0]["LateNos"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["MissPunchPresent"] = MissPunchPres.Rows[0]["Present"].ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["MissPunchNos"] = MissPunchnos.Rows[0]["MisspunchNos"].ToString();
                int ava = Convert.ToInt32(ReqTable.Rows[0]["AVAIL"].ToString());
                int req = Convert.ToInt32(ReqTable.Rows[0]["REQ"].ToString());
                try
                {
                    Totalsum = Math.Round((Convert.ToDecimal(ava.ToString()) / Convert.ToDecimal(req.ToString()))).ToString();
                    // SumTotal = int.Parse(Totalsum);
                    DataCell.Rows[DataCell.Rows.Count - 1]["ON%"] = Totalsum.ToString();
                }
                catch (Exception ex)
                {
                    DataCell.Rows[DataCell.Rows.Count - 1]["ON%"] = "0";
                }

                int UPava = Convert.ToInt32(ReqTable.Rows[0]["AVAIL"].ToString());
                int UPreq = Convert.ToInt32(ReqTable.Rows[0]["REQ"].ToString());
                try
                {
                    string ReqUPDATETotal = Math.Round((Convert.ToDecimal(UPava.ToString()) / Convert.ToDecimal(UPreq.ToString()))).ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["UP%"] = ReqUPDATETotal;
                }
                catch (Exception ex)
                {
                    DataCell.Rows[DataCell.Rows.Count - 1]["UP%"] = "0";
                }
                int allow = Convert.ToInt32(AttendanceAllo.Rows[0]["cnt"].ToString());
                int Present = Convert.ToInt32(AttendancePres.Rows[0]["Attndce"].ToString());
                try
                {
                    string EmpGradeTotoal = Math.Round((Convert.ToDecimal(Present.ToString()) / Convert.ToDecimal(allow.ToString()))).ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ONATTENDANCE%"] = EmpGradeTotoal;
                }
                catch (Exception ex)
                {
                    DataCell.Rows[DataCell.Rows.Count - 1]["ONATTENDANCE%"] = "0";
                }
                int UPallow = Convert.ToInt32(UpAttendanceAllo.Rows[0]["cnt"].ToString());
                int UPPresent = Convert.ToInt32(UpAttendancePres.Rows[0]["Attndce"].ToString());
                try
                {
                    string AtteUPDATE = Math.Round((Convert.ToDecimal(UPPresent.ToString()) / Convert.ToDecimal(UPallow.ToString()))).ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["UPATTENDANCE%"] = AtteUPDATE;
                }
                catch (Exception ex)
                {
                    DataCell.Rows[DataCell.Rows.Count - 1]["UPATTENDANCE%"] = "0";
                }
                int present = Convert.ToInt32(LatePres.Rows[0]["Present"].ToString());
                int latenos = Convert.ToInt32(LateNo.Rows[0]["LateNos"].ToString());
                try
                {
                    string LateNosTotal = Math.Round((Convert.ToDecimal(latenos.ToString()) / Convert.ToDecimal(present.ToString()))).ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ONLATE%"] = LateNosTotal.ToString();
                }
                catch (Exception ex)
                {
                    DataCell.Rows[DataCell.Rows.Count - 1]["ONLATE%"] = "0";
                }
                //DataCell.Rows[DataCell.Rows.Count - 1]["ONLATE%"] = LateNosTotal;

                int UPpresent = Convert.ToInt32(UpLatePres.Rows[0]["Present"].ToString());
                int UPlatenos = Convert.ToInt32(UpLateNo.Rows[0]["LateNos"].ToString());
                string UPLateNosTotal = Math.Round((Convert.ToDecimal(UPlatenos.ToString()) / Convert.ToDecimal(UPPresent.ToString()))).ToString();
                DataCell.Rows[DataCell.Rows.Count - 1]["UPLATE%"] = UPLateNosTotal;


                int MissPunchpresent = Convert.ToInt32(MissPunchPres.Rows[0]["Present"].ToString());
                int MissPuchNos = Convert.ToInt32(MissPunchnos.Rows[0]["MisspunchNos"].ToString());
                if (MissPuchNos == 0)
                {
                    //DataCell.Rows[i]["ONMISSPUCH%"] = "0";
                }
                else
                {
                    string MissPunchTotal = Math.Round((Convert.ToDecimal(MissPuchNos.ToString()) / Convert.ToDecimal(MissPunchpresent.ToString()))).ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ONMISSPUCH%"] = MissPunchTotal;
                }

                int UPMissPunchpresent = Convert.ToInt32(UpMissPunchPres.Rows[0]["Present"].ToString());
                int UPMissPuchNos = Convert.ToInt32(UpMissPunchnos.Rows[0]["MisspunchNos"].ToString());

                if (UPMissPuchNos == 0)
                {
                    // DataCell.Rows[i]["UPMISSPUCH%"] = "0";
                }
                else
                {
                    string UPMissPunchTotal = Math.Round((Convert.ToDecimal(UPMissPuchNos.ToString()) / Convert.ToDecimal(UPMissPunchpresent.ToString()))).ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["UPMISSPUCH%"] = UPMissPuchNos;
                }


                //ONDATE GRADE Percentage & UPDATE GRADE Percentage
                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where  Attn_Date_str='" + FDate + "' and Present_Absent='Present' and Grade='A++' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                GradePlusPlus = Math.Round((Convert.ToDecimal(TotPresent.ToString()) / Convert.ToDecimal(TotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where Present_Absent='Present' and Grade='A++' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                UpGradePlusPlus = Math.Round((Convert.ToDecimal(UpTotPresent.ToString()) / Convert.ToDecimal(UpTotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where  Attn_Date_str='" + FDate + "' and Present_Absent='Present' and Grade='A+' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                GradePlus = Math.Round((Convert.ToDecimal(TotPresent.ToString()) / Convert.ToDecimal(TotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where Present_Absent='Present' and Grade='A+' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                UpGradePlus = Math.Round((Convert.ToDecimal(UpTotPresent.ToString()) / Convert.ToDecimal(UpTotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where  Attn_Date_str='" + FDate + "' and Present_Absent='Present' and Grade='A' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                GradeA = Math.Round((Convert.ToDecimal(TotPresent.ToString()) / Convert.ToDecimal(TotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where Present_Absent='Present' and Grade='A' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                UpGradeA = Math.Round((Convert.ToDecimal(UpTotPresent.ToString()) / Convert.ToDecimal(UpTotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where  Attn_Date_str='" + FDate + "' and Present_Absent='Present' and Grade='T+' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                GradeTPlus = Math.Round((Convert.ToDecimal(TotPresent.ToString()) / Convert.ToDecimal(TotEmp.ToString()) * 100), 1).ToString();


                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where Present_Absent='Present' and Grade='T+' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                UpGradeTPlus = Math.Round((Convert.ToDecimal(UpTotPresent.ToString()) / Convert.ToDecimal(UpTotEmp.ToString()) * 100), 1).ToString();

                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where  Attn_Date_str='" + FDate + "' and Present_Absent='Present' and Grade='T' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                TotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                GradeT = Math.Round((Convert.ToDecimal(TotPresent.ToString()) / Convert.ToDecimal(TotEmp.ToString()) * 100), 1).ToString();


                SSQL = "select count(*) as Present from Employee_Mst where IsActive='Yes'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotEmp = Convert.ToInt32(EmpGradePercentage.Rows[0]["Present"].ToString());
                SSQL = "select count(*) as TotalPresent from LogTime_Days where Present_Absent='Present' and Grade='T' and LocCode='" + ReqTable.Rows[i][0].ToString() + "'";
                EmpGradePercentage = objdata.RptEmployeeMultipleDetails(SSQL);
                UpTotPresent = Convert.ToInt32(EmpGradePercentage.Rows[0]["TotalPresent"].ToString());
                UpGradeT = Math.Round((Convert.ToDecimal(UpTotPresent.ToString()) / Convert.ToDecimal(UpTotEmp.ToString()) * 100), 1).ToString();

                // GradeONPercentage =(Convert.ToDecimal(GradePlusPlus.ToString()) + Convert.ToDecimal(GradePlus.ToString()) + Convert.ToDecimal(GradeA.ToString()) + Convert.ToDecimal(GradeTPlus.ToString()) + Convert.ToDecimal(GradeT.ToString()));
                GradeONPercentage = GradePlusPlus.ToString() + GradePlus.ToString() + GradeA.ToString() + GradeTPlus.ToString() + GradeT.ToString();
                // string totperc = Convert.ToDecimal(GradeONPercentage.ToString())/5;
                //  UpGradeONPercentage = UpGradePlusPlus + UpGradePlus + UpGradeA + UpGradeTPlus + UpGradeT) / 5;
                DataCell.Rows[DataCell.Rows.Count - 1]["ONGRADE%"] = "0";
                DataCell.Rows[DataCell.Rows.Count - 1]["UPGRADE%"] = "0";

                sno += 1;
            }

        }
        grid.DataSource = DataCell;
        grid.DataBind();
        string attachment = "attachment;filename=UNIT WISE ONDATE GRADE LIST.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table border='1'>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='48'>");
        Response.Write("<a style=\"font-weight:bold\">MURUGAN TEXTILES HR TEAM</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='48'>");
        Response.Write("<a style=\"font-weight:bold\">UNIT WISE ONDATE GRADE LIST</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">REQUIREMENT</a>");
        Response.Write("</td>");
        Response.Write("<td colspan='14'>");
        Response.Write("<a style=\"font-weight:bold\">EMPLOYEE GRADE</a>");
        Response.Write("</td>");
        Response.Write("<td colspan='8'>");
        Response.Write("<a style=\"font-weight:bold\">ATTENDANCE GRADE</a>");
        Response.Write("</td>");
        Response.Write("<td colspan='8'>");
        Response.Write("<a style=\"font-weight:bold\">LATE REPORT</a>");
        Response.Write("</td>");
        Response.Write("<td colspan='8'>");
        Response.Write("<a style=\"font-weight:bold\">MISS PUNCH</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">UNIT</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">REQ</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">AVAIL</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ON%</a>");
        Response.Write("</td>");


        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">OFF%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A++</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A+</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">A</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">T+</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">T</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ON%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">OFF%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ALLO</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">PRESE</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ON%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">OFF%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">PRES</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">LATE NOS</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ON%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">OFF%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">PRES</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">MISS NOS</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ON%</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">OFF%</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        for (int k = 0; k < DataCell.Rows.Count; k++)
        {

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["Unit"].ToString() + " </a>");
            Response.Write("</td>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["Req"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["Avail"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["ON%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["UP%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["A++"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["A+"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["A"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["T+"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["T"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["ONGRADE%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["UPGRADE%"].ToString() + " </a>");
            Response.Write("</td>");


            //UPDATE

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["Allo"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["AttendancePresent"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["ONATTENDANCE%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["UPATTENDANCE%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["Present"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["LateNos"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["ONLATE%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["UPLATE%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["MissPunchPresent"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["MissPunchNos"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["ONMISSPUCH%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:normal\">" + DataCell.Rows[k]["UPMISSPUNCH%"].ToString() + " </a>");
            Response.Write("</td>");

            Response.Write("</tr>");
        }
        Response.Write("</table>");
        //Response.Write(stw.ToString());
        Response.End();
        //Response.Clear();


    }
}