﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstTeaSnakcs : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Tea Snacks Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Shift();
            Load_Canteen();
            
        }

        //Load_Data();
        txtDate.Text = DateTime.Today.ToShortDateString();
        Load_Data();
        // txttime.Text = DateTime.Now.ToString("hh:mm tt");
    }

    private void Load_Data()
    {
        SSQL = "Select CanteenName,Shift,Convert(varchar,Dte,103) as Dte,CONVERT(varchar(15),time,22) as time,ItemName,Amount,Auto_ID from MstTeaSnacks where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_Canteen()
    {
        SSQL = "";
        SSQL = "Select * from MstCanteen where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlCanteenName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCanteenName.DataTextField = "CanteenName";
        ddlCanteenName.DataValueField = "CanID";
        ddlCanteenName.DataBind();
        ddlCanteenName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Shift()
    {
        SSQL = "";
        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from MstTeaSnacks where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "";
        bool ErrFlag = false;
        if (ddlShift.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Please Select the Shift Name)", true);
            return;
        }
        if (ddlCanteenName.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Please Select the Canteen Name)", true);
            return;
        }

        if (!ErrFlag)
        {
            //string SSQL = "";
            //SSQL = "Delete from MstTeaSnacks where CanteenID='" + ddlCanteenName.SelectedValue + "' and Shift='" + ddlShift.SelectedValue + "' and CCode='" + SessionCcode.ToString() + "' and LCode='" + SessionLcode.ToString() + "'";
            //objdata.RptEmployeeMultipleDetails(SSQL);
            //SSQL = "insert into MstTeaSnacks(CanteenName,CanteenID,CCode,LCode,TeaAmount,SnacksAmount,Shift)values";
            //SSQL = SSQL + "('" + ddlCanteenName.SelectedItem.Text + "','" + ddlCanteenName.SelectedValue + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','" + txtTeaAmount.Text + "','" + txtSancksAmount.Text + "','" + ddlShift.SelectedValue + "')";
            //objdata.RptEmployeeMultipleDetails(SSQL);
            //Load_Data();
            //btnClear_Click(sender, e);

            foreach (GridViewRow gvsal in GVModule.Rows)
            {
                SaveMode = "Insert";

                CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");
                string AddCheck = "0";
                string Item_Name = "";
                string Amount = "";
                string ViewCheck = "0";

                if (ChkSelect_chk.Checked == true) { AddCheck = "1"; }


                Item_Name = gvsal.Cells[0].Text.ToString();
                Amount = gvsal.Cells[1].Text.ToString();


                //if (ChkSelect_chk.Checked == true || ChkModify.Checked == true || ChkDelete.Checked == true || ChkView.Checked == true || ChkApprove.Checked == true || ChkPrint.Checked == true)
                if (ChkSelect_chk.Checked == true)
                {

                    //Insert Items
                    SSQL = "insert into MstTeaSnacks(CanteenName,CanteenID,CCode,LCode,ItemName,Amount,Dte,time,Shift)values";
                    SSQL = SSQL + "('" + ddlCanteenName.SelectedItem.Text + "','" + ddlCanteenName.SelectedValue + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','" + Item_Name + "','" + Amount + "',Convert(date,'" + txtDate.Text + "',103),'" + txttime.Text + "','" + ddlShift.SelectedValue + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    Load_Data();
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Not Saved Properly...');", true);
            }

        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlShift.ClearSelection();
        ddlCanteenName.ClearSelection();
        //txtSancksAmount.Text = "0.00";
        //txtTeaAmount.Text = "0.00";
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        SSQL = "select ItemName,Amount from mstItems where Compcode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
            chkAll.Visible = true;
            btnSave.Visible = true;
            btnClear.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
            chkAll.Visible = false;
            btnSave.Visible = false;
            btnClear.Visible = false;
        }
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;

            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;

            }
        }
    }
}