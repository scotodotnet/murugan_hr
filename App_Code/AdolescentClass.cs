﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AdolescentClass
/// </summary>
public class AdolescentClass
{
	public AdolescentClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    private string _Mode;

    public string Mode
    {
        get { return _Mode; }
        set { _Mode = value; }
    }


    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _CertificateNumber;

    public string CertificateNumber
    {
        get { return _CertificateNumber; }
        set { _CertificateNumber = value; }
    }

    private string _CertificateDate;

    public string CertificateDate
    {
        get { return _CertificateDate; }
        set { _CertificateDate = value; }
    }


    private string _NextNewDate;

    public string NextNewDate
    {
        get { return _NextNewDate; }
        set { _NextNewDate = value; }
    }

    private string _TypesOfCertificate;

    public string TypesOfCertificate
    {
        get { return _TypesOfCertificate; }
        set { _TypesOfCertificate = value; }
    }

    private string _Remarks;

    public string Remarks
    {
        get { return _Remarks; }
        set { _Remarks = value; }
    }


    private string _ISAdolescent;

    public string ISAdolescent
    {
        get { return _ISAdolescent; }
        set { _ISAdolescent = value; }
    }

    private string _BankName;

    public string BankName
    {
        get { return _BankName; }
        set { _BankName = value; }
    }

    private string _BranchCode;

    public string BranchCode
    {
        get { return _BranchCode; }
        set { _BranchCode = value; }
    }

    private string _AccountNo;

    public string AccountNo
    {
        get { return _AccountNo; }
        set { _AccountNo = value; }
    }


    private string _IFSCnumber;

    public string IFSCnumber
    {
        get { return _IFSCnumber; }
        set { _IFSCnumber = value; }
    }

}
