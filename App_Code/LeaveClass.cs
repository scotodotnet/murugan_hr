﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for LeaveClass
/// </summary>
public class LeaveClass
{

    private string _TotalWorkingDays;
    private string _LeaveDate;
    private string _LeaveType1;
    private string _LeaveType2;
    private string _LeaveType3;
    private string _NoofLeave1;
    private string _NoofLeave2;
    private string _NoofLeave3;
    private string _CasualLeave;
    private string _SickLeave;
    private string _EarnedLeave;
    private string _MaternalLeave;
    private string _NoofLeave;


	public LeaveClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string NoofLeave
    {
        get { return _NoofLeave; }
        set { _NoofLeave = value; }
    }
    public string CasualLeave
    {
        get { return _CasualLeave; }
        set { _CasualLeave = value; }
    }
    public string SickLeave
    {
        get { return _SickLeave ; }
        set { _SickLeave = value; }
    }
    public string EarnedLeave
    {
        get { return _EarnedLeave; }
        set { _EarnedLeave = value; }
    }
    public string MaternalLeave
    {
        get { return _MaternalLeave; }
        set { _MaternalLeave = value; }
    }
    public string TotalWorkingDays
    {
        get { return _TotalWorkingDays; }
        set { _TotalWorkingDays = value; }
    }
    public string LeaveDate
    {
        get { return _LeaveDate; }
        set { _LeaveDate = value; }
    }
    public string LeaveType1
    {
        get { return _LeaveType1; }
        set { _LeaveType1 = value; }
    }
    public string LeaveType2
    {
        get { return _LeaveType2; }
        set { _LeaveType2 = value; }
    }
    public string LeaveType3
    {
        get { return _LeaveType3; }
        set { _LeaveType3 = value; }
    }
    public string Noofleave1
    {
        get { return _NoofLeave1; }
        set { _NoofLeave1 = value; }
    }
    public string Noofleave2
    {
        get { return _NoofLeave2; }
        set { _NoofLeave2 = value; }
    }
    public string Noofleave3
    {
        get { return _NoofLeave3; }
        set { _NoofLeave3 = value; }
    }
       
}
