﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EmployeeDetailsEpayClass
/// </summary>
public class EmployeeDetailsEpayClass
{
	public EmployeeDetailsEpayClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }

    private string _ExistingNo;

    public string ExistingNo
    {
        get { return _ExistingNo; }
        set { _ExistingNo = value; }
    }
    private string _EmpName;

    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }

    private string _FatherName;

    public string FatherName
    {
        get { return _FatherName; }
        set { _FatherName = value; }
    }

    private string _Gender;

    public string Gender
    {
        get { return _Gender; }
        set { _Gender = value; }
    }
    private string _DOB;

    public string DOB
    {
        get { return _DOB; }
        set { _DOB = value; }
    }

    private string _PermanentAddr;

    public string PermanentAddr
    {
        get { return _PermanentAddr; }
        set { _PermanentAddr = value; }
    }

    private string _TempAddress;

    public string TempAddress
    {
        get { return _TempAddress; }
        set { _TempAddress = value; }
    }

    private string _PermanentTlk;

    public string PermanentTlk
    {
        get { return _PermanentTlk; }
        set { _PermanentTlk = value; }
    }

    private string _PermanentDst;

    public string PermanentDst
    {
        get { return _PermanentDst; }
        set { _PermanentDst = value; }
    }

    private string _EmpMobile;

    public string EmpMobile
    {
        get { return _EmpMobile; }
        set { _EmpMobile = value; }
    }
    
    private string _Qualification;

    public string Qualification
    {
        get { return _Qualification; }
        set { _Qualification = value; }
    }

    private string _Department;

    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }

    private string _Desgination;

    public string Desgination
    {
        get { return _Desgination; }
        set { _Desgination = value; }
    }

    private string _Category;

    public string Category
    {
        get { return _Category; }
        set { _Category = value; }
    }
    private string _MaritalStatus;

    public string MaritalStatus
    {
        get { return _MaritalStatus; }
        set { _MaritalStatus = value; }
    }

    private string _IsActive;

    public string IsActive
    {
        get { return _IsActive; }
        set { _IsActive = value; }
    }


    private string _CreatedBy;
    public string CreatedBy
    {
        get { return _CreatedBy; }
        set { _CreatedBy = value; }
    }


    private string _CreatedDate;
    public string CreatedDate
    {
        get { return _CreatedDate; }
        set { _CreatedDate = value; }
    }

    private string _EmployeeType;
    public string EmployeeType
    {
        get { return _EmployeeType; }
        set { _EmployeeType = value; }
    }

    private string _UnEducated;
    public string UnEducated
    {
        get { return _UnEducated; }
        set { _UnEducated = value; }
    }

    private string _NonPFGrade;
    public string NonPFGrade
    {
        get { return _NonPFGrade; }
        set { _NonPFGrade = value; }
    }

    private string _IsValid;
    public string IsValid
    {
        get { return _IsValid; }
        set { _IsValid = value; }
    }

  

    private string _IsLeave;

    public string IsLeave
    {
        get { return _IsLeave; }
        set { _IsLeave = value; }
    }

    private string _Status;

    public string Status
    {
        get { return _Status; }
        set { _Status = value; }
    }

    private string _RoleCode;

    public string RoleCode
    {
        get { return _RoleCode; }
        set { _RoleCode = value; }
    }

    private string _Initial;

    public string Initial
    {
        get { return _Initial; }
        set { _Initial = value; }
    }


}
