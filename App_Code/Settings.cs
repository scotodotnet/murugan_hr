﻿//Setting Class @0-FC6BFAAD
using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Collections.Specialized;
using Payroll.Data;

namespace Payroll.Configuration
{
    public sealed class Settings
    {
        private static string _serverUrl = ConfigurationManager.AppSettings["ServerUrl"];
        private static string _securedUrl = ConfigurationManager.AppSettings["SecuredUrl"];
        private static string _cultureId = ConfigurationManager.AppSettings["CultureId"];
        private static string _siteLanguage = ConfigurationManager.AppSettings["SiteLanguage"];
        private static string _AccesDeniedUrl = ServerURL + ConfigurationManager.AppSettings["AccessDeniedUrl"];
        private static string _AccesDeniedPage = ConfigurationManager.AppSettings["AccessDeniedUrl"];

        private Settings()
        { }

        public static string CultureId
        {
            get
            {
                if (_cultureId == null || _cultureId == "")
                    return CultureInfo.CurrentCulture.Name;
                else
                    return _cultureId;
            }
            set
            {
                _cultureId = value;
            }
        }

        public static string SiteLanguage
        {
            get
            {
                return _siteLanguage;
            }
            set
            {
                _siteLanguage = value;
            }
        }

        public static string DateFormat
        {
            get
            {
                return "dd/MM/yyyy HH\\:mm\\:ss";
            }
        }

        public static string BoolFormat
        {
            get
            {
                return "true;false";
            }
        }

        public static string AccessDeniedUrl
        {
            get
            {
                return _AccesDeniedUrl;
            }
            set
            {
                _AccesDeniedUrl = value;
            }
        }

        public static string AccessDeniedPage
        {
            get
            {
                return _AccesDeniedPage;
            }
            set
            {
                _AccesDeniedPage = value;
            }
        }

        public static string ServerURL
        {
            get
            {
                return _serverUrl;
            }
            set
            {
                _serverUrl = value;
            }
        }

        public static string SecuredURL
        {
            get
            {
                return _securedUrl;
            }
            set
            {
                _securedUrl = value;
            }
        }

        public static DataAccessObject GetConnection(string name)
     
        {
            switch (name)
            {
                case "scholar":
                    return scholarDataAccessObject;
            }
            return null;
        }

        public static ConnectionString scholarConnection
        {
            get
            {
                ConnectionString cs = new ConnectionString();

                cs.Connection = ConfigurationManager.AppSettings["ConnectionString"];
                return cs;
            }
        }

        public static DataAccessObject scholarDataAccessObject
        {
            get
            {
                ConnectionString Connection = scholarConnection;
                return new SqlDao(Connection);
            }
        }

        public static ConnectionString SampleConnection
        {
            get
            {
                ConnectionString cs = new ConnectionString();
                cs.Connection = ConfigurationManager.AppSettings["ConnectionString"];
                return cs;
            }
        }

        public static DataAccessObject sampleDataAccessObject
        {
            get
            {
                ConnectionString Connection = SampleConnection;
                return new SqlDao(Connection);
            }
        }

    }
}
//End Setting Class

