﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Overtimeclass
/// </summary>
public class Overtimeclass
{
    private string _Finance;
    private string _EmpNo;
    private string _EmpName;
    private string _department;
    private string _Daysalary;
    private string _hrsalary;
    private string _Nhrs;
    private string _Month;
    private string _chkManual;
    private string _netAmount;
    private string _Ccode;
    private string _Lcode;
	public Overtimeclass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string Finance
    {
        get { return _Finance; }
        set { _Finance = value; }
    }
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }
    public string department
    {
        get { return _department; }
        set { _department = value; }
    }
    public string dailysalary
    {
        get { return _Daysalary; }
        set { _Daysalary = value; }
    }
    public string hrsalary
    {
        get { return _hrsalary; }
        set { _hrsalary = value; }
    }
    public string NHrs
    {
        get { return _Nhrs; }
        set { _Nhrs = value; }
    }
    public string Months
    {
        get { return _Month; }
        set { _Month = value; }
    }
    public string chkmanual
    {
        get { return _chkManual; }
        set { _chkManual = value; }
    }
    public string Netamt
    {
        get { return _netAmount; }
        set { _netAmount = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { value = _Ccode; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { value = _Lcode; }
    }
}
