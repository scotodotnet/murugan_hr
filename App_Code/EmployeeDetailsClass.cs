﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EmployeeDetailsClass
/// </summary>
public class EmployeeDetailsClass
{
	public EmployeeDetailsClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;
    private string _EmpPrefix;
    private string _LastName;
    private string _PayPeriod_Desc;
    private string _CreateBy;
    private string _CreateDate;

    private string _Working_Hours;
    private string _Calculate_Work_Hours;
    private string _RecutersMob;
    private string _EmpLeft;
    private string _SalaryThro;
    private string _Mother;
    private string _SamepresentAddress;
    private string _RecuirtMob;
    private string _BrokerName;
    private string _Types;
    private string _Types_Name;
    private string _Bank_Branch;
    private string _States;
    private string _Community;
    private string _Caste;
    private string _PhysicalRemarks;

    private string _RejoinDate;
    private string _RelieveDate;

    public string RelieveDate
    {
        get { return _RelieveDate; }
        set { _RelieveDate = value; }
    }


    public string RejoinDate
    {
        get { return _RejoinDate; }
        set { _RejoinDate = value; }
    }


    public string PhysicalRemarks
    {
        get { return _PhysicalRemarks; }
        set { _PhysicalRemarks = value; }
    }


    public string Caste
    {
        get { return _Caste; }
        set { _Caste = value; }
    }


    public string Community
    {
        get { return _Community; }
        set { _Community = value; }
    }


    public string States
    {
        get { return _States; }
        set { _States = value; }
    }



    public string Bank_Branch
    {
        get { return _Bank_Branch; }
        set { _Bank_Branch = value; }
    }


    public string Types_Name
    {
        get { return _Types_Name; }
        set { _Types_Name = value; }
    }


    public string Types
    {
        get { return _Types; }
        set { _Types = value; }
    }


    public string BrokerName
    {
        get { return _BrokerName; }
        set { _BrokerName = value; }
    }


    public string RecuirtMob
    {
        get { return _RecuirtMob; }
        set { _RecuirtMob = value; }
    }


    public string SamepresentAddress
    {
        get { return _SamepresentAddress; }
        set { _SamepresentAddress = value; }
    }


    public string Mother
    {
        get { return _Mother; }
        set { _Mother = value; }
    }
    private string _Permanent_Taluk;

    public string Permanent_Taluk
    {
        get { return _Permanent_Taluk; }
        set { _Permanent_Taluk = value; }
    }
    private string _Present_Taluk;

    public string Present_Taluk
    {
        get { return _Present_Taluk; }
        set { _Present_Taluk = value; }
    }


    public string SalaryThro
    {
        get { return _SalaryThro; }
        set { _SalaryThro = value; }
    }


    public string EmpLeft
    {
        get { return _EmpLeft; }
        set { _EmpLeft = value; }
    }


    public string RecutersMob
    {
        get { return _RecutersMob; }
        set { _RecutersMob = value; }
    }


    public string Calculate_Work_Hours
    {
        get { return _Calculate_Work_Hours; }
        set { _Calculate_Work_Hours = value; }
    }


    public string Working_Hours
    {
        get { return _Working_Hours; }
        set { _Working_Hours = value; }
    }


    public string CreateDate
    {
        get { return _CreateDate; }
        set { _CreateDate = value; }
    }

    public string CreateBy
    {
        get { return _CreateBy; }
        set { _CreateBy = value; }
    }


    public string LastName
    {
        get { return _LastName; }
        set { _LastName = value; }
    }

    public string EmpPrefix
    {
        get { return _EmpPrefix; }
        set { _EmpPrefix = value; }
    }
    

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _ExistingNo;

    public string ExistingNo
    {
        get { return _ExistingNo; }
        set { _ExistingNo = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }


    private string _EmpName;

    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }

    private string _FirstName;

    public string FirstName
    {
        get { return _FirstName; }
        set { _FirstName = value; }
    }

    private string _Initial;

    public string Initial
    {
        get { return _Initial; }
        set { _Initial = value; }
    }

    private string _ShiftType;

    public string ShiftType
    {
        get { return _ShiftType; }
        set { _ShiftType = value; }
    }

    private string _Gender;

    public string Gender
    {
        get { return _Gender; }
        set { _Gender = value; }
    }


    private string _Age;

    public string Age
    {
        get { return _Age; }
        set { _Age = value; }
    }

    private string _DOB;

    public string DOB
    {
        get { return _DOB; }
        set { _DOB = value; }
    }

    private string _DOJ;

    public string DOJ
    {
        get { return _DOJ; }
        set { _DOJ = value; }
    }

    private string _Category;

    public string Category
    {
        get { return _Category; }
        set { _Category = value; }
    }

    private string _SubCategory;

    public string SubCategory
    {
        get { return _SubCategory; }
        set { _SubCategory = value; }
    }

    private string _MaritalStatus;

    public string MaritalStatus
    {
        get { return _MaritalStatus; }
        set { _MaritalStatus = value; }
    }

    private string _WagesType;

    public string WagesType
    {
        get { return _WagesType; }
        set { _WagesType = value; }
    }

    private string _WeekOff;

    public string WeekOff
    {
        get { return _WeekOff; }
        set { _WeekOff = value; }
    }

    private string _PFEligible;
        public string PFEligible
    {
        get { return _PFEligible; }
        set { _PFEligible = value; }
    }
    private string _IsActive;

    public string IsActive
    {
        get { return _IsActive; }
        set { _IsActive = value; }
    }

    private string _EmpStatus;

    public string EmpStatus
    {
        get { return _EmpStatus; }
        set { _EmpStatus = value; }
    }

    private string _EmpType;

    public string EmpType
    {
        get { return _EmpType; }
        set { _EmpType = value; }
    }

    private string _CompletedDate;

    public string CompletedDate
    {
        get { return _CompletedDate; }
        set { _CompletedDate = value; }
    }

    private string _AdminUser;

    public string AdminUser
    {
        get { return _AdminUser; }
        set { _AdminUser = value; }
    }


    private string _RecuritThr;

    public string RecuritThr
    {
        get { return _RecuritThr; }
        set { _RecuritThr = value; }
    }

    private string _IsNonAdmin;

    public string IsNonAdmin
    {
        get { return _IsNonAdmin; }
        set { _IsNonAdmin = value; }
    }
        private string _PermanentAddr;

        public string PermanentAddr
    {
        get { return _PermanentAddr; }
        set { _PermanentAddr = value; }
    }

      private string _ParentsMobile1;

        public string ParentsMobile1
    {
        get { return _ParentsMobile1; }
        set { _ParentsMobile1 = value; }
    }

    private string _ParentsMobile2;

        public string ParentsMobile2
    {
        get { return _ParentsMobile2; }
        set { _ParentsMobile2 = value; }
    }

   private string _PermanentDst;

   public string PermanentDst
    {
        get { return _PermanentDst; }
        set { _PermanentDst = value; }
    }

   private string _TempDst;

   public string TempDst
    {
        get { return _TempDst; }
        set { _TempDst = value; }
    }

   private string _IFSCCode;

   public string IFSCCode
    {
        get { return _IFSCCode; }
        set { _IFSCCode = value; }
    }

   private string _ESIEligible;

   public string ESIEligible
    {
        get { return _ESIEligible; }
        set { _ESIEligible = value; }
    }

   private string _HostelRmNo;

   public string HostelRmNo
    {
        get { return _HostelRmNo; }
        set { _HostelRmNo = value; }
    }

   private string _PermanentTlk;

   public string PermanentTlk
    {
        get { return _PermanentTlk; }
        set { _PermanentTlk = value; }
    }

   private string _TempTlk;

   public string TempTlk
    {
        get { return _TempTlk; }
        set { _TempTlk = value; }
    }
    private string _AdharNo;

    public string AdharNo
    {
        get { return _AdharNo; }
        set { _AdharNo = value; }
    }

    private string _VoterID;

    public string VoterID
    {
        get { return _VoterID; }
        set { _VoterID = value; }
    }

    private string _DrivingLicence;

    public string DrivingLicence
    {
        get { return _DrivingLicence; }
        set { _DrivingLicence = value; }
    }

    private string _PassportNo;

    public string PassportNo
    {
        get { return _PassportNo; }
        set { _PassportNo = value; }
    }

    private string _RationCardNo;

    public string RationCardNo
    {
        get { return _RationCardNo; }
        set { _RationCardNo = value; }
    }

    private string _panCardNo;

    public string panCardNo
    {
        get { return _panCardNo; }
        set { _panCardNo = value; }
    }

    private string _SmartCardNo;

    public string SmartCardNo
    {
        get { return _SmartCardNo; }
        set { _SmartCardNo = value; }
    }

    private string _Other;

    public string Other
    {
        get { return _Other; }
        set { _Other = value; }
    }

    private string _CertificateNumber;

    public string CertificateNumber
    {
        get { return _CertificateNumber; }
        set { _CertificateNumber = value; }
    }

    private string _CertificateDate;

    public string CertificateDate
    {
        get { return _CertificateDate; }
        set { _CertificateDate = value; }
    }


    private string _NextNewDate;

    public string NextNewDate
    {
        get { return _NextNewDate; }
        set { _NextNewDate = value; }
    }

    private string _TypesOfCertificate;

    public string TypesOfCertificate
    {
        get { return _TypesOfCertificate; }
        set { _TypesOfCertificate = value; }
    }

    private string _Remarks;

    public string Remarks
    {
        get { return _Remarks; }
        set { _Remarks = value; }
    }


    private string _ISAdolescent;

    public string ISAdolescent
    {
        get { return _ISAdolescent; }
        set { _ISAdolescent = value; }
    }

    private string _BankName;

    public string BankName
    {
        get { return _BankName; }
        set { _BankName = value; }
    }

    private string _BranchCode;

    public string BranchCode
    {
        get { return _BranchCode; }
        set { _BranchCode = value; }
    }

    private string _AccountNo;

    public string AccountNo
    {
        get { return _AccountNo; }
        set { _AccountNo = value; }
    }


    private string _IFSCnumber;

    public string IFSCnumber
    {
        get { return _IFSCnumber; }
        set { _IFSCnumber = value; }
    }

    private string _ExpInYear;

    public string ExpInYear
    {
        get { return _ExpInYear; }
        set { _ExpInYear = value; }
    }

    private string _ExpInMonth;

    public string ExpInMonth
    {
        get { return _ExpInMonth; }
        set { _ExpInMonth = value; }
    }

    private string _CompanyName;
    public string CompanyName
    {
        get { return _CompanyName; }
        set { _CompanyName = value; }
    }

    private string _ContactName;
    public string ContactName
    {
        get { return _ContactName; }
        set { _ContactName = value; }
    }


    private string _Department;
    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }

    private string _Desgination;
    public string Desgination
    {
        get { return _Desgination; }
        set { _Desgination = value; }
    }

    private string _PeriodFrom;
    public string PeriodFrom
    {
        get { return _PeriodFrom; }
        set { _PeriodFrom = value; }
    }

    private string _PeriodTo;
    public string PeriodTo
    {
        get { return _PeriodTo; }
        set { _PeriodTo = value; }
    }

    private string _MobileNo1;
    public string MobileNo1
    {
        get { return _MobileNo1; }
        set { _MobileNo1 = value; }
    }

    private string _MobileNo2;
    public string MobileNo2
    {
        get { return _MobileNo2; }
        set { _MobileNo2 = value; }
    }

    private string _PhoneNo1;
    public string PhoneNo1
    {
        get { return _PhoneNo1; }
        set { _PhoneNo1 = value; }
    }


    private string _PhoneNo2;
    public string PhoneNo2
    {
        get { return _PhoneNo2; }
        set { _PhoneNo2 = value; }
    }


    private string _FamilyDetails;
    public string FamilyDetails
    {
        get { return _FamilyDetails; }
        set { _FamilyDetails = value; }
    }

    private string _Recuritment;
    public string Recuritment
    {
        get { return _Recuritment; }
        set { _Recuritment = value; }
    }

    private string _IdentiMark1;
    public string IdentiMark1
    {
        get { return _IdentiMark1; }
        set { _IdentiMark1 = value; }
    }

    private string _IdentiMark2;
    public string IdentiMark2
    {
        get { return _IdentiMark2; }
        set { _IdentiMark2 = value; }
    }

    private string _ParentsMobile;
    public string ParentsMobile
    {
        get { return _ParentsMobile; }
        set { _ParentsMobile = value; }
    }

  
    private string _ParentsPhone;
    public string ParentsPhone
    {
        get { return _ParentsPhone; }
        set { _ParentsPhone = value; }
    }

    private string _EmpMobile;
    public string EmpMobile
    {
        get { return _EmpMobile; }
        set { _EmpMobile = value; }
    }

    private string _RecurtMobile;
    public string RecurtMobile
    {
        get { return _RecurtMobile; }
        set { _RecurtMobile = value; }
    }

   
    private string _Religion;
    public string Religion
    {
        get { return _Religion; }
        set { _Religion = value; }
    }

    private string _Handicapped;
    public string Handicapped
    {
        get { return _Handicapped; }
        set { _Handicapped = value; }
    }

    private string _Height;
    public string Height
    {
        get { return _Height; }
        set { _Height = value; }
    }

    private string _Weight;
    public string Weight
    {
        get { return _Weight; }
        set { _Weight = value; }
    }

    private string _BloodGroup;
    public string BloodGroup
    {
        get { return _BloodGroup; }
        set { _BloodGroup = value; }
    }

    private string _PermenaneAdd;
    public string PermenaneAdd
    {
        get { return _PermenaneAdd; }
        set { _PermenaneAdd = value; }
    }

    private string _Taulk;
    public string Taulk
    {
        get { return _Taulk; }
        set { _Taulk = value; }
    }

    private string _District;
    public string District
    {
        get { return _District; }
        set { _District = value; }
    }
    private string _TempAddress;
    public string TempAddress
    {
        get { return _TempAddress; }
        set { _TempAddress = value; }
    }

    private string _TempTaulk;
    public string TempTaulk
    {
        get { return _TempTaulk; }
        set { _TempTaulk = value; }
    }



    private string _TempDistrict;
    public string TempDistrict
    {
        get { return _TempDistrict; }
        set { _TempDistrict = value; }
    }





    private string _Department2;
    public string Department2
    {
        get { return _Department2; }
        set { _Department2 = value; }
    }

    private string _Designation2;
    public string Designation2
    {
        get { return _Designation2; }
        set { _Designation2 = value; }
    }

    private string _PayPeriod;
    public string PayPeriod
    {
        get { return _PayPeriod; }
        set { _PayPeriod = value; }
    }
    private string _BaseSalary;
    public string BaseSalary
    {
        get { return _BaseSalary; }
        set { _BaseSalary = value; }
    }

    private string _StdWorkingHrs;
    public string StdWorkingHrs
    {
        get { return _StdWorkingHrs; }
        set { _StdWorkingHrs = value; }
    }

    private string _Nominee;
    public string Nominee
    {
        get { return _Nominee; }
        set { _Nominee = value; }
    }
    private string _OTEligible;
    public string OTEligible
    {
        get { return _OTEligible; }
        set { _OTEligible = value; }
    }

    private string _Qualification;
    public string Qualification
    {
        get { return _Qualification; }
        set { _Qualification = value; }
    }
    private string _Nationality;
    public string Nationality
    {
        get { return _Nationality; }
        set { _Nationality = value; }
    }

    private string _Certificate;
    public string Certificate
    {
        get { return _Certificate; }
        set { _Certificate = value; }
    }

    private string _WorkingHrs;
    public string WorkingHrs
    {
        get { return _WorkingHrs; }
        set { _WorkingHrs = value; }
    }

    private string _OTHrs;
    public string OTHrs
    {
        get { return _OTHrs; }
        set { _OTHrs = value; }
    }

    private string _BusNo;
    public string BusNo
    {
        get { return _BusNo; }
        set { _BusNo = value; }
    }

    private string _BusRoute;
    public string BusRoute
    {
        get { return _BusRoute; }
        set { _BusRoute = value; }
    }

    private string _WeefOFF;
    public string WeefOFF
    {
        get { return _WeefOFF; }
        set { _WeefOFF = value; }
    }

    private string _Reference;
    public string Reference
    {
        get { return _Reference; }
        set { _Reference = value; }
    }

    private string _RoomNo;
    public string RoomNo
    {
        get { return _RoomNo; }
        set { _RoomNo = value; }
    }

    private string _ESICode;
    public string ESICode
    {
        get { return _ESICode; }
        set { _ESICode = value; }
    }

    private string _EligiblePF;
    public string EligiblePF
    {
        get { return _EligiblePF; }
        set { _EligiblePF = value; }
    }

    private string _PFdoj;
    public string PFdoj
    {
        get { return _PFdoj; }
        set { _PFdoj = value; }
    }

    private string _PFNumber;
    public string PFNumber
    {
        get { return _PFNumber; }
        set { _PFNumber = value; }
    }

    private string _EligibleESI;
    public string EligibleESI
    {
        get { return _EligibleESI; }
        set { _EligibleESI = value; }
    }

    private string _ESIdoj;
    public string ESIdoj
    {
        get { return _ESIdoj; }
        set { _ESIdoj = value; }
    }

    private string _ESINumber;
    public string ESINumber
    {
        get { return _ESINumber; }
        set { _ESINumber = value; }
    }


    private string _IDProofOne;
    public string IDProofOne
    {
        get { return _IDProofOne; }
        set { _IDProofOne = value; }
    }

    private string _IDProofTwo;
    public string IDProofTwo
    {
        get { return _IDProofTwo; }
        set { _IDProofTwo = value; }
    }


    private string _IDProofThree;
    public string IDProofThree
    {
        get { return _IDProofThree; }
        set { _IDProofThree = value; }
    }

    private string _IDProofFour;
    public string IDProofFour
    {
        get { return _IDProofFour; }
        set { _IDProofFour = value; }
    }

    private string _PathNameOne;
    public string PathNameOne
    {
        get { return _PathNameOne; }
        set { _PathNameOne = value; }
    }

    private string _PathNameTwo;
    public string PathNameTwo
    {
        get { return _PathNameTwo; }
        set { _PathNameTwo = value; }
    }

    private string _PathNameThree;
    public string PathNameThree
    {
        get { return _PathNameThree; }
        set { _PathNameThree = value; }
    }

    private string _PathNameFour;
    public string PathNameFour
    {
        get { return _PathNameFour; }
        set { _PathNameFour = value; }
    }

    private string _MachineID_Encry;

    public string MachineID_Encry
    {
        get { return _MachineID_Encry; }
        set { _MachineID_Encry = value; }
    }

    private string _GuardianName;

    public string GuardianName
    {
        get { return _GuardianName; }
        set { _GuardianName = value; }
    }
    private string _BasicSalary;

    public string BasicSalary
    {
        get { return _BasicSalary; }
        set { _BasicSalary = value; }
    }




    private string _Deduction1amt;

    public string Deduction1amt
    {
        get { return _Deduction1amt; }
        set { _Deduction1amt = value; }
    }



    private string _Deduction2amt;

    public string Deduction2amt
    {
        get { return _Deduction2amt; }
        set { _Deduction2amt = value; }
    }




    private string _Allowance1amt;

    public string Allowance1amt
    {
        get { return _Allowance1amt; }
        set { _Allowance1amt = value; }
    }




    private string _Allowance2amt;

    public string Allowance2amt
    {
        get { return _Allowance2amt; }
        set { _Allowance2amt = value; }
    }


    private string _PFS;

    public string PFS
    {
        get { return _PFS; }
        set { _PFS = value; }
    }

  
}
