﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Basic_Det_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionEpay;
    string Query = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
       SessionEpay= Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Load_Data();
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
      
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {

        txtBasic.Enabled = true;
        txtSpl.Enabled = true;
        txtHRA.Enabled = true;
        txtvehicle.Enabled = true;
        txtCommunication.Enabled = true;
        txtJornal.Enabled = true;
        txtUniform.Enabled = true;
        txtConv.Enabled = true;
        txtEdu.Enabled = true;
        txtMedi.Enabled = true;
        txtWash.Enabled = true;
        txtIncentive.Enabled = true;
        txtRAI.Enabled = true;
        txtMedi.Enabled = true;
        txtOthers.Enabled = true;
        Load_Data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            String MsgFlag = "Insert";
            DataTable dt = new DataTable();
            Query = "select * from ["+SessionEpay+"]..MstBasicDet where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0)
            {
                Query = "delete from [" + SessionEpay + "]..MstBasicDet where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                MsgFlag = "Update";
            }

            Query = "";
            Query = Query + "Insert into [" + SessionEpay + "]..MstBasicDet(Ccode,Lcode,Category,EmployeeType,BasicDA,HRA,ConvAllow,EduAllow,MediAllow,";
            Query = Query + "RAI,WashingAllow,Spl_Allowance,Uniform,Vehicle_Maintenance,Communication,Journ_Perid_Paper,Incentives,Bonus)Values('" + SessionCcode + "','" + SessionLcode + "',";
            Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + txtBasic.Text + "','" + txtHRA.Text + "','" + txtConv.Text + "','" + txtEdu.Text + "','" + txtMedi.Text + "',";
            Query = Query + "'" + txtRAI.Text + "','" + txtWash.Text + "','" + txtSpl.Text + "','" + txtUniform.Text + "','" + txtvehicle.Text + "','" + txtCommunication.Text + "','" + txtJornal.Text + "','" + txtIncentive.Text + "','" + txtOthers.Text + "')";
            objdata.RptEmployeeMultipleDetails(Query);
            Clear();
            Load_Data();
            if (MsgFlag == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
            }
        }
        catch (Exception Ex)
        {

        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        txtBasic.Text = "0";
        txtSpl.Text = "0";
        txtHRA.Text = "0";
        txtvehicle.Text = "0";
        txtCommunication.Text = "0";
        txtJornal.Text = "0";
        txtUniform.Text = "0";
        txtConv.Text = "0";
        txtEdu.Text = "0";
        txtMedi.Text = "0";
        txtWash.Text = "0";
        txtIncentive.Text = "0";
        txtRAI.Text = "0";
        txtMedi.Text = "0";
        txtOthers.Text = "0";
        ddlCategory.SelectedValue = "0";
        ddlEmployeeType.SelectedValue = "0";
    }

    public void Load_Data()
    {
        DataTable dt = new DataTable();
        Query = "select * from [" + SessionEpay + "]..MstBasicDet where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtBasic.Text = dt.Rows[0]["BasicDA"].ToString();
            txtSpl.Text = dt.Rows[0]["Spl_Allowance"].ToString();
            txtHRA.Text = dt.Rows[0]["HRA"].ToString();
            txtvehicle.Text = dt.Rows[0]["Vehicle_Maintenance"].ToString();
            txtCommunication.Text = dt.Rows[0]["Communication"].ToString();
            txtJornal.Text = dt.Rows[0]["Journ_Perid_Paper"].ToString();
            txtUniform.Text = dt.Rows[0]["Uniform"].ToString();
            txtConv.Text = dt.Rows[0]["ConvAllow"].ToString();
            txtEdu.Text = dt.Rows[0]["EduAllow"].ToString();
            //txtMedi.Text = dt.Rows[0][""].ToString();
            txtWash.Text = dt.Rows[0]["WashingAllow"].ToString();
            txtIncentive.Text = dt.Rows[0]["Incentives"].ToString();
            txtRAI.Text = dt.Rows[0]["RAI"].ToString();
            txtMedi.Text = dt.Rows[0]["MediAllow"].ToString();
            txtOthers.Text= dt.Rows[0]["Bonus"].ToString();
        }
        else
        {
            txtBasic.Text = "0";
            txtSpl.Text = "0";
            txtHRA.Text = "0";
            txtvehicle.Text = "0";
            txtCommunication.Text = "0";
            txtJornal.Text = "0";
            txtUniform.Text = "0";
            txtConv.Text = "0";
            txtEdu.Text = "0";
            txtMedi.Text = "0";
            txtWash.Text = "0";
            txtIncentive.Text = "0";
            txtRAI.Text = "0";
            txtMedi.Text = "0";
            txtOthers.Text = "0";
        }
    }
}