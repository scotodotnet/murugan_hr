﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class Bank_OT_Manual : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDTable = new DataTable();
    string SessionUserType;

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEpay;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionPayroll"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Bank OT Salary Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_WagesType();
            Load_Bank_Name();
        }

        
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        query = "Select * from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Load_Bank_Name()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBankName.Items.Clear();
        query = "Select * from MstBank";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();
    }

    protected void btnBankOTSalary_Click(object sender, EventArgs e)
    {
        //Get Food Allowance Rate
        DataTable DT_Inc = new DataTable();
        string Food_Allowance_Amt = "0";
        SSQL = "Select * from Department_Inc_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + txtEmployeeType.SelectedItem.Text + "'";
        DT_Inc = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Inc.Rows.Count != 0)
        {
            Food_Allowance_Amt = DT_Inc.Rows[0]["Food_Allowance"].ToString();
        }
        GetAttdTable_Weekly_OTHOURS();
        if (AutoDTable.Rows.Count != 0)
        {
            //Excel Write
            string query = "";
            string CmpName = "";
            string Cmpaddress = "";
            string attachment = "attachment;filename=Bank_OT_SalaryDetails.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            DataTable dt = new DataTable();
            query = "Select Cname,Location,Address1,Address2,Location,Pincode from ["+SessionEpay+"]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);

            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='6' style='font-size:20.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("" + CmpName + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='6' style='font-size:13.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("" + SessionLcode + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            if (txtBankName.SelectedValue != "-Select-")
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='6' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write("BANK NAME : " + txtBankName.SelectedValue + "");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='6' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                //Response.Write("" + Cmpaddress + "");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
            }

            //if (rbsalary.SelectedValue == "2")
            //{
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='6' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            //    Response.Write("BANK A/C SALARY DETAILS FOR THE MONTH OF " + ddlMonths.SelectedValue.ToUpper().ToString() + " - " + YR);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");
            //}
            //else
            //{
            Response.Write("<tr align='center'>");
            Response.Write("<td colspan='6' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("BANK A/C OT SALARY DETAILS FROM : " + txtFromDate.Text + " TO : " + txtToDate.Text);
            Response.Write("</td>");
            Response.Write("</tr>");
            //}
            Response.Write("</table>");

            Response.Write("<table style='font-weight:bold;' border='1'><tr align='center'><td>S.NO</td><td>TOKEN NO</td>");
            Response.Write("<td>NAME</td><td>ACCOUNT NO</td><td>NET SALARY</td><td>REMARKS</td></tr></table>");

            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                string Perday_Salary = (Convert.ToDecimal(AutoDTable.Rows[i]["Basic"].ToString()) / Convert.ToDecimal(8)).ToString();
                string OTHrs_Amt = (Convert.ToDecimal(AutoDTable.Rows[i]["MonthOTHrs"].ToString()) * Convert.ToDecimal(Perday_Salary)).ToString();
                OTHrs_Amt = (Math.Round(Convert.ToDecimal(OTHrs_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                string Incentive_Amt = (Convert.ToDecimal(AutoDTable.Rows[i]["SPGTotalHrs"].ToString()) * Convert.ToDecimal(AutoDTable.Rows[i]["OTHrs_Rate"].ToString())).ToString();
                Incentive_Amt = (Math.Round(Convert.ToDecimal(Incentive_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                string Food_Amount = (Convert.ToDecimal(Food_Allowance_Amt) * Convert.ToDecimal(AutoDTable.Rows[i]["Food_Allown_Count"].ToString())).ToString();

                string Final_Amount = (Convert.ToDecimal(OTHrs_Amt) + Convert.ToDecimal(Incentive_Amt) + Convert.ToDecimal(Food_Amount)).ToString();
                string RoundOffNetPay_Bank = "0";
                double d1 = Convert.ToDouble(Final_Amount.ToString());
                int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                RoundOffNetPay_Bank = rounded1.ToString();


                Response.Write("<table style='font-weight:bold;' border='1'><tr>");
                Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
                Response.Write("<td>" + AutoDTable.Rows[i]["TokenNo"].ToString() + "</td>");
                Response.Write("<td>" + AutoDTable.Rows[i]["EmpName"].ToString() + "</td>");
                Response.Write("<td>" + AutoDTable.Rows[i]["AccountNo"].ToString() + "</td>");
                Response.Write("<td>" + RoundOffNetPay_Bank + "</td>");
                Response.Write("<td></td>");
                Response.Write("</tr></table>");

                
            }
            string bank_row_count = "";
            bank_row_count = (Convert.ToDecimal(5) + Convert.ToDecimal(AutoDTable.Rows.Count)).ToString();

            if (AutoDTable.Rows.Count.ToString() != "0")
            {
                Response.Write("<table border='1'>");
                Response.Write("<tr style='font-size:12.0pt;font-weight:bold;'>");
                Response.Write("<td colspan='4' align='right'>Grand Total</td>");
                Response.Write("<td>=sum(E6:E" + bank_row_count.ToString() + ")</td>");
                Response.Write("<td></td>");
                Response.Write("</tr></table>");
            }
            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
        }

        
    }

    public void GetAttdTable_Weekly_OTHOURS()
    {
        AutoDTable.Columns.Add("Basic");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("MonthOTHrs");
        AutoDTable.Columns.Add("SPGTotalHrs");
        AutoDTable.Columns.Add("OTHrs_Rate");
        AutoDTable.Columns.Add("Food_Allown_Count");
        AutoDTable.Columns.Add("AccountNo");

        DataTable DT_MOT = new DataTable();
        int i;
        DataRow dtRow;
        DataTable OTHrs_DS = new DataTable();

        SSQL = "Select (EM.BaseSalary + EM.Alllowance2) as Basic,EM.DeptName as Dept,EM.Designation as Designation,WOT.ExistingCode as TokenNo,('`' + EM.AccountNo) as AccountNo,";
        SSQL = SSQL + " EM.FirstName as EmpName,sum(WOT.OTHrs) as MonthOTHrs,'0' as SPGTotalHrs,'0' as OTHrs_Rate,0 as Food_Allown_Count from Weekly_OTHours WOT";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.LocCode=WOT.LocCode And EM.CompCode=WOT.CompCode And EM.ExistingCode=WOT.ExistingCode";
        SSQL = SSQL + " where WOT.LocCode='" + SessionLcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And WOT.CompCode='" + SessionCcode + "' And EM.CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And Convert(Datetime,WOT.OTDate,103) >= CONVERT(Datetime,'" + txtFromDate.Text + "',103)";
        SSQL = SSQL + " And Convert(Datetime,WOT.OTDate,103) <= CONVERT(Datetime,'" + txtToDate.Text + "',103)";
        SSQL = SSQL + " And WOT.Wages='" + txtEmployeeType.SelectedItem.Text + "' and EM.Wages='" + txtEmployeeType.SelectedItem.Text + "'";
        SSQL = SSQL + " And EM.AccountNo <> ''";

        SSQL = SSQL + " group by EM.BaseSalary,EM.Alllowance2,EM.DeptName,EM.Designation,WOT.ExistingCode,EM.AccountNo,EM.FirstName";
        SSQL = SSQL + " order by WOT.ExistingCode Asc";
        DT_MOT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_MOT.Rows.Count != 0)
        {
            for (i = 0; i < DT_MOT.Rows.Count; i++)
            {
                dtRow = AutoDTable.NewRow();
                dtRow["Basic"] = DT_MOT.Rows[i]["Basic"].ToString();
                dtRow["Dept"] = DT_MOT.Rows[i]["Dept"].ToString();
                dtRow["Designation"] = DT_MOT.Rows[i]["Designation"].ToString();
                dtRow["TokenNo"] = DT_MOT.Rows[i]["TokenNo"].ToString();
                dtRow["EmpName"] = DT_MOT.Rows[i]["EmpName"].ToString();
                dtRow["MonthOTHrs"] = DT_MOT.Rows[i]["MonthOTHrs"].ToString();
                dtRow["AccountNo"] = DT_MOT.Rows[i]["AccountNo"].ToString();

                //Get Spinning OT Hrs Start
                SSQL = "Select isnull(sum(cast(OTHrs as decimal(18,2))),0) as SPGTotalHrs from Weekly_OTHours where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And ExistingCode='" + DT_MOT.Rows[i]["TokenNo"].ToString() + "'";
                SSQL = SSQL + " And SPG_Inc='True'";
                SSQL = SSQL + " And Convert(Datetime,OTDate,103) >= CONVERT(Datetime,'" + txtFromDate.Text + "',103)";
                SSQL = SSQL + " And Convert(Datetime,OTDate,103) <= CONVERT(Datetime,'" + txtToDate.Text + "',103)";
                OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (OTHrs_DS.Rows.Count != 0)
                {
                    dtRow["SPGTotalHrs"] = OTHrs_DS.Rows[0]["SPGTotalHrs"].ToString();
                }
                else
                {
                    dtRow["SPGTotalHrs"] = 0;
                }

                //GET Food Allowance Count
                SSQL = "Select count(ExistingCode) as Food_Count from Weekly_OTHours where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And ExistingCode='" + DT_MOT.Rows[i]["TokenNo"].ToString() + "'";
                SSQL = SSQL + " And Convert(Datetime,OTDate,103) >= CONVERT(Datetime,'" + txtFromDate.Text + "',103)";
                SSQL = SSQL + " And Convert(Datetime,OTDate,103) <= CONVERT(Datetime,'" + txtToDate.Text + "',103)";
                SSQL = SSQL + " And cast(OTHrs as decimal(18,2)) >= 4";
                SSQL = SSQL + " And Food_Inc='True'";
                OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (OTHrs_DS.Rows.Count != 0)
                {
                    dtRow["Food_Allown_Count"] = OTHrs_DS.Rows[0]["Food_Count"].ToString();
                }
                else
                {
                    dtRow["Food_Allown_Count"] = 0;
                }

                //Get Deptartment & Designation Incentive Rate
                SSQL = "Select OTIncAmt from Incentive_Mst where DeptName='" + DT_MOT.Rows[i]["Dept"].ToString() + "'";
                SSQL = SSQL + " And Designation='" + DT_MOT.Rows[i]["Designation"].ToString() + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + txtEmployeeType.SelectedItem.Text + "'";
                OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (OTHrs_DS.Rows.Count != 0)
                {
                    dtRow["OTHrs_Rate"] = OTHrs_DS.Rows[0]["OTIncAmt"].ToString();
                }
                else
                {
                    dtRow["OTHrs_Rate"] = 0;
                }
                AutoDTable.Rows.Add(dtRow);
            }
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
}
