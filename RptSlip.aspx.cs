﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class RptSlip : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;

    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();
        
        Load_DB();
        if (!IsPostBack)
        {
            Load_Report_Type();
            Load_WagesType();
            Agent_load();
            //Load_Department();
            Load_BankName();
            Months_load();
            LoadCategory();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //Load_Division_Name();
            //Master.Visible = false;
            if (SessionUserType == "2")
            {
                IFUser_Fields_Hide();
            }
        }
    }

    private void LoadCategory()
    {
        string SQL = "select CateID,CateName from MstCategory where LocCode='" + SessionLcode.ToString() + "' ";
        ddlCattype.DataSource = objdata.RptEmployeeMultipleDetails(SQL);
        ddlCattype.DataTextField = "CateName";
        ddlCattype.DataValueField = "CateID";
        ddlCattype.DataBind();
        ddlCattype.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Select-","-Select-",true));
    }

    public void IFUser_Fields_Hide()
    {
        //IF_Dept_Hide.Visible = false;
        IF_FromDate_Hide.Visible = false;
        IF_ToDate_Hide.Visible = false;
        //IF_Left_Employee_Hide.Visible = false;
        //IF_State_Hide.Visible = false;
        //btnBankSalary.Visible = false;
        //btnCivilAbstract.Visible = false;
        //BtnDeptManDays.Visible = false;
        //IF_Leave_Credit.Visible = false;
        //rdbPayslipFormat.Visible = false;
        rdbPayslipIFFormat.Visible = true;
        RdbCashBank.SelectedValue = "2";
        RdbPFNonPF.SelectedValue = "1";
        IF_Salary_Through_Hide.Visible = false;
        IF_PF_NON_PF_Hide.Visible = false;
        IF_Report_Type.Visible = true;
        //IF_rdbPayslipFormat_Hide.Visible = false;
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [" + SessionRights + "]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_BankName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBankName.Items.Clear();
        query = "Select distinct BankName from MstBank order by BankName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    private void Agent_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlAgentName.Items.Clear();
        query = "Select AgentName from MstAgent order by AgentName ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["AgentName"] = "0";
        dr["AgentName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlAgentName.DataTextField = "AgentName";
        ddlAgentName.DataValueField = "AgentName";
        ddlAgentName.DataBind();
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";  

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";
                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

               
                if (SessionUserType == "2")
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " " + YR.ToString();
                }
                else
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + txtfrom.ToString() + " - " + txtTo.ToString();
                }

                Basic_Report_Date = "";
                Basic_Report_Type = "OLD";

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                //Check PF Category
                if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5")) //Labour Worker
                {
                    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed, " +
                           " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                           " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                           " (SalDet.GrossEarnings - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, " +
                           " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                           " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5) as NetFinal " +
                           " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                           " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                           " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                           " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                           " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                           " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                           " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                           " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' and SalDet.WorkedDays > 0 ";

                    query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                   " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                   " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }
                else if ((txtEmployeeType.SelectedValue == "1"))// Staff workers
                {
                    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.withpay, " +
                              " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                              " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,(SalDet.NetPay + SalDet.HRAamt + SalDet.TAamt + SalDet.OTHoursAmtNew + SalDet.withpay) as  NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                              " ((SalDet.GrossEarnings + SalDet.HRAamt + SalDet.TAamt + SalDet.OTHoursAmtNew) - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, " +
                              " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.CommsAmt + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                              " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5) as NetFinal,SalDet.HRAamt,SalDet.TAamt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew " +
                              " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                              " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                              " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                              " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                              " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                              " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                              " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                              " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' and SalDet.WorkedDays > 0 ";

                    query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo,SalDet.OTHoursAmtNew,SalDet.withpay, " +
                   " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                   " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HRAamt,SalDet.TAamt,SalDet.OTHoursNew  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }


                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";

                if ((txtEmployeeType.SelectedValue == "2"))
                {
                    GvMonthWorker.DataSource = dt_1;
                    GvMonthWorker.DataBind();
                }
                else if (txtEmployeeType.SelectedValue == "1")
                {
                    GvStaffCheck.DataSource = dt_1;
                    GvStaffCheck.DataBind();
                }
                else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    GvOthers.DataSource = dt_1;
                    GvOthers.DataBind();
                }

                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                NetBase = "0";
                NetFDA = "0";
                NetVDA = "0";
                Nettotal = "0";
                NetPFEarnings = "0";
                NetPF = "0";
                NetESI = "0";
                NetUnion = "0";
                NetAdvance = "0";
                NetAll1 = "0";
                NetAll2 = "0";
                NetAll3 = "0";
                NetAll4 = "0";
                NetDed1 = "0";
                NetDed2 = "0";
                NetDed3 = "0";
                NetDed4 = "0";
                NetAll5 = "0";
                NetDed5 = "0";
                NetLOP = "0";
                NetStamp = "0";
                NetTotalDeduction = "0";
                NetOT = "0";
                NetAmt = "0";
                Network = "0";
                totCL = "0";
                totNFh = "0";
                totweekoff = "0";
                Roundoff = "0";
                Fixedsal = "0";
                FixedOT = "0";
                Tot_OThr = "0";
                //HomeDays = "0";
            

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                if ((txtEmployeeType.SelectedValue == "2"))
                {
                    GvMonthWorker.RenderControl(htextw);
                }
                else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    GvOthers.RenderControl(htextw);
                }
                else if (txtEmployeeType.SelectedValue == "1")
                {
                    GvStaffCheck.RenderControl(htextw);
                }
                
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + CmpName + " - PAYSLIP REPORT");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                string Salary_Head = "";

                if ((txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11"))
                {
                    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                }
                else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                }

                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());
           

                Int32 Grand_Tot_End = 0;

                //Check PF Category

                if ((txtEmployeeType.SelectedValue == "2"))
                {
                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='10'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = GvMonthWorker.Rows.Count + 5;
                                      
                    Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");

                }
                else if (txtEmployeeType.SelectedValue == "1")
                {
                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='10'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = GvStaffCheck.Rows.Count + 5;
                                        
                    Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("<td>=sum(AG6:AG" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AH6:AH" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(AI6:AI" + Grand_Tot_End.ToString() + ")</td>");


                }
                else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='9'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = GvOthers.Rows.Count + 5;

                    Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                    
                    Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");

                }
               

                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
            
            }
          
        }
        catch (Exception)
        {
            
            throw;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        
        bool ErrFlag = false;
        string CmpName = "";
        string Cmpaddress = "";
        int YR = 0;
        string SalaryType = "";
        string query = "";
        string ExemptedStaff = "";
        string report_head = "";
        string Basic_Report_Date = "";
        string Basic_Report_Type = ""; 


        if (ddlMonths.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
        }

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            ErrFlag = true;
        }

        else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            try
            {
                //if (RdbCashBank.SelectedValue != "2")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Bank Option');", true);
                //    ErrFlag = true;
                //}
               //if (txtBankName.SelectedValue == "-Select-")
               // {
               //     ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Bank Name');", true);
               //     ErrFlag = true;
               // }
                if (!ErrFlag)
                {
                    // query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,EmpDet.Designation,EmpDet.AccountNo," +
                    //                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,EmpDet.BankName,EmpDet.BranchCode,EmpDet.IFSC_Code," +
                    //                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance) as DedPF,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                    //                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                    //                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    //                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    //                      " inner Join MstBank MB on MB.BankName = EmpDet.BankName " +
                    //                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                    //                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                    //                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    //                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                    //                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Salary_Through='2' and EmpDet.Eligible_PF='1' and  EmpDet.BankName='" + txtBankName.SelectedItem.Text + "'";

                    // query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,EmpDet.BankName,EmpDet.BranchCode,EmpDet.IFSC_Code," +
                    //" EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,EmpDet.Designation,EmpDet.AccountNo,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                    //" SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                    //" Order by EmpDet.ExistingCode Asc";

                    query = "";
                    query = "Select SalDet.ExisistingCode,isnull(sum(SalDet.RoundOffNetPay),'0') as RoundOffNetPay,EM.FirstName as EmpName,EM.AccountNo from Employee_Mst EM inner join [" + SessionPayroll+ "]..SalaryDetails SalDet";
                    query = query + " on SalDet.MachineNo=EM.MachineID";
                    if (txtBankName.SelectedItem.Text != "-Select-")
                    {
                        query = query + " and EM.BankName='" + txtBankName.Text.ToString().ToUpper() + "'";
                    }
                    query = query + " and Convert(datetime,SalDet.FromDate,103)>=Convert(datetime,'" + txtfrom.Text + "',103) and Convert(datetime,SalDet.ToDate,103)<=Convert(datetime,'" + txtTo.Text + "',103) and SalDet.Month='" + ddlMonths.SelectedItem.Text + "'";
                    query = query + "group by  SalDet.ExisistingCode,EM.ExistingCode,EM.FirstName,EM.AccountNo  order by EM.ExistingCode";

                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.RptEmployeeMultipleDetails(query);

                    Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                    NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";



                    BankGV.DataSource = dt_1;
                    BankGV.DataBind();

                    int colSpan = dt_1.Columns.Count + 2;


                    string attachment = "attachment;filename=NEFT.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    //DataTable dt = new DataTable();
                    DataTable dt = new DataTable();
                    query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(query);
                    if (dt.Rows.Count > 0)
                    {
                        CmpName = dt.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                    }


                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    BankGV.RenderControl(htextw);

                   

                    Response.Write(stw.ToString());

                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    Int32 Grand_Tot_End = 0;

                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = BankGV.Rows.Count + 5;

                    Response.Write("<td>=sum(E6:E" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("</tr></table>");

                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                    
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Report_Type();
    }
    public void Load_Report_Type()
    {
        if (rbtnReportType.SelectedValue == "1")
        {
            Agent_load();
            ddlAgentName.Enabled = false;
            ddlcategory.Enabled = true;
            txtEmployeeType.Enabled = true;
        }
        else
        {
            Load_WagesType();
            ddlAgentName.Enabled = true;
            ddlcategory.Enabled = false;
            txtEmployeeType.Enabled = false;
        
        }
    }
    protected void btnPayslip_Click(object sender, EventArgs e)
    {
        //try
        //{
        bool ErrFlag = false;
        //    string CmpName = "";
        //    string Cmpaddress = "";
        //    int YR = 0;
        //    string SalaryType = "";
        //    string query = "";
        //    string ExemptedStaff = "";
        //    string report_head = "";
        //    string Basic_Report_Date = "";
        //    string Basic_Report_Type = "";

        //    if (ddlMonths.SelectedItem.Text == "January")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedItem.Text == "February")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedItem.Text == "March")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //    }
        //    if (rbtnReportType.SelectedValue == "2")
        //    {
        //        if (ddlAgentName.SelectedValue == "-Select-")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
        //            ErrFlag = true;
        //        }
        //    }
        //    else
        //    {
        //        if (ddlcategory.SelectedValue == "-Select-")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
        //            ErrFlag = true;
        //        }
        //        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
        //            ErrFlag = true;
        //        }
        //    }




        //    if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
        //        ErrFlag = true;
        //    }
        //    string Other_State = "";
        //    string Non_Other_State = "";

        //    if (!ErrFlag)
        //    {
        //        if (ddlcategory.SelectedValue == "STAFF")
        //        {
        //            Stafflabour = "STAFF";
        //        }
        //        else if (ddlcategory.SelectedValue == "LABOUR")
        //        {
        //            Stafflabour = "LABOUR";
        //        }

        //        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        //        SqlConnection con = new SqlConnection(constr);

        //        string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
        //        string Payslip_Format_Type = "";

        //        if (SessionUserType == "2")
        //        {
        //            Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
        //        }
        //        else
        //        {
        //            Payslip_Format_Type = "";
        //        }

        //        string Str_PFType = "";
        //        if (SessionUserType == "2")
        //        {
        //            Str_PFType = RdpIFPF.SelectedValue.ToString();
        //        }
        //        else
        //        {
        //            Str_PFType = RdbPFNonPF.SelectedValue.ToString();
        //        }

        //        string Report_Types = rbtnReportType.SelectedValue;
        //        string AgentName = ddlAgentName.SelectedItem.Text;


        //        string Str_ChkLeft = "";
        //        //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
        //        Str_ChkLeft = "0";


        //        ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type.ToString() + "&PFTypePost=" + Str_PFType.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");

        //    }

        //}
        //catch (Exception)
        //{

        //    throw;
        //}
        string ReportName = "";

        if (ddlSalarytype.SelectedValue != "0")
        {
            if (ddlSalarytype.SelectedValue == "Bank")
            {
                ReportName = "Account_Emp";
            }
            else if(ddlSalarytype.SelectedValue=="Non_Bank")
            {
                ReportName = "Non_Account_Emp";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Salary Type');", true);
            ErrFlag = true;
        }

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            ErrFlag = true;
        }
        if (ddlMonths.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
            ErrFlag = true;
        }
        if (txtfrom.Text == "" || txtTo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Date Properly');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("PaySlipRpt.aspx?RptName=" + ReportName + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString().Trim() + "&FinYearVal=" + ddlFinance.SelectedValue + "&Month=" + ddlMonths.SelectedItem.Text.ToString().Trim() + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text, "_blank", "");
        }
    }

    protected void btnWagesAbstract_Click(object sender, EventArgs e)
    {
        string ReportName = "";
        bool ErrFlag = false;

        if (ddlSalarytype.SelectedValue != "0")
        {
            if (ddlSalarytype.SelectedValue == "Bank")
            {
                ReportName = "Wages_Abstract_Account_Emp";
            }
            else if (ddlSalarytype.SelectedValue == "Non_Bank")
            {
                ReportName = "Wages_Abstract_Non_Account_Emp";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Salary Type');", true);
            ErrFlag = true;
        }
       

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            ErrFlag = true;
        }
        if (ddlMonths.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
            ErrFlag = true;
        }
        if (txtfrom.Text == "" || txtTo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Date Properly');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("PaySlipRpt.aspx?RptName=" + ReportName + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString().Trim() + "&FinYearVal=" + ddlFinance.SelectedValue + "&Month=" + ddlMonths.SelectedItem.Text.ToString().Trim() + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text, "_blank", "");
        }
    }

    protected void BtnPFReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string CmpName = "";
        string Cmpaddress = "";
        int YR = 0;
        string SalaryType = "";
        string query = "";
        string ExemptedStaff = "";
        string report_head = "";
        string Basic_Report_Date = "";
        string Basic_Report_Type = "";


        if (ddlMonths.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
        }

        //if (ddlcategory.SelectedValue == "-Select-")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
        //    ErrFlag = true;
        //}
        //if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
        //    ErrFlag = true;
        //}

        if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
            ErrFlag = true;
        }

        if ((txtfrom.Text == "") || (txtTo.Text == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the From Date And ToDate');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            try
            {                
                if (!ErrFlag)
                {
                    query = "Select EM.ExistingCode,EM.Wages,EM.UAN,EM.FirstName,isnull(SD.ProvidentFund,'0') as ProvidentFund,isnull(SD.ESI,'0') as ESI,";
                    query = query + " isnull(SD.WorkedDays,'0') as WorkedDays,isnull(SD.BasicAndDANew,'0') as BasicAndDANew,isnull(SD.BasicHRA,'0') as BasicHRA,";
                    query = query + " isnull(SD.ConvAllow,'0') as ConvAllow,isnull(SD.EduAllow,'0') as EduAllow,isnull(SD.MediAllow,'0') as MediAllow,";
                    query = query + " isnull(SD.BasicRAI,'0') as BasicRAI,isnull(SD.WashingAllow,'0') as WashingAllow from Employee_Mst EM ";
                    query = query + " Inner join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=EM.MachineID";
                    query = query + " where SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                    query = query + " And SD.Month='" + ddlMonths.SelectedItem.Text.ToString() + "'";
                    query = query + " And EM.Eligible_PF='1'";
                    if (txtfrom.Text.ToString() != "" && txtTo.Text.ToString() != "")
                    {
                        query = query + " and SD.FromDate=CONVERT(datetime,'" + txtfrom.Text + "',103)";
                        query = query + " and SD.ToDate=CONVERT(datetime,'" + txtTo.Text + "',103)";
                    }
                    if (txtEmployeeType.SelectedItem.Text != "-Select-")
                    {
                        query = query + " and EM.Wages='" + txtEmployeeType.SelectedItem.Text.ToString() + "'";
                    }
                    if (ddlcategory.SelectedItem.Text != "-Select-")
                    {
                        query = query + " and EM.CatName='" + ddlcategory.SelectedItem.Text.ToString() + "'";
                    }

                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_1.Rows.Count != 0)
                    {
                        int colSpan = dt_1.Columns.Count + 1;
                        
                        string attachment = "attachment;filename=PF_Report.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        //DataTable dt = new DataTable();
                        DataTable dt = new DataTable();
                        string PF_Base_Salary_Fixed = "6500";
                        query = "Select * from [" + SessionPayroll + "]..MstESIPF where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count != 0)
                        {
                            PF_Base_Salary_Fixed = dt.Rows[0]["StaffSalary"].ToString();
                        }
                        //Get PF Salary_Fixed

                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                        }


                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);

                        Response.Write("<table broder='1'><tr style='font-weight: bold;border-style: solid;'>");
                        Response.Write("<td>S.NO</td><td>T.NO</td><td>EMPLOYEE TYPE</td><td>UAN NO</td><td>EMPNAME</td>");
                        Response.Write("<td>PRESENT DAYS</td><td>PF SOURCE</td><td>PF AMOUNT</td>");
                        Response.Write("</tr></table>");
                        for (int i = 0; i < dt_1.Rows.Count; i++)
                        {
                            Response.Write("<table broder='1'><tr style='border-style: solid;'>");
                            Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ExistingCode"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Wages"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["UAN"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["FirstName"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
                            string PF_Salary_Calculate = "0";
                            PF_Salary_Calculate = (Convert.ToDecimal(dt_1.Rows[i]["BasicAndDANew"].ToString()) + Convert.ToDecimal(dt_1.Rows[i]["BasicRAI"].ToString())).ToString();
                            if (Convert.ToDecimal(PF_Salary_Calculate) >= Convert.ToDecimal(PF_Base_Salary_Fixed))
                            {
                                PF_Salary_Calculate = PF_Base_Salary_Fixed;
                            }
                            Response.Write("<td>" + PF_Salary_Calculate + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
                            Response.Write("</tr></table>");
                        }
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                    }
                    //Response.Write("<table>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + CmpName + " - NEFT TRANSFER REPORT");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + SessionLcode + "");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + Cmpaddress + "");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //string Salary_Head = "";

                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='colspan='" + colSpan + "'>");
                    ////Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                    //Response.Write(Salary_Head);
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    ////}
                    //Response.Write("</table>");

                    //Response.Write(stw.ToString());

                    ////Response.Write("<table border='1'>");
                    ////Response.Write("<tr Font-Bold='true'>");
                    ////Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    ////Response.Write("Grand Total");
                    ////Response.Write("</td>");

                    //Int32 Grand_Tot_End = 0;

                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = BankGV.Rows.Count + 5;

                    //Response.Write("<td>=sum(E6:E" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("</tr></table>");

                    //Response.Write("</tr>");
                    //Response.Write("</table>");

                    

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
     protected void BtnSignList_PDF_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }


            string Other_State = "";
            string Non_Other_State = "";
            //if (RdbOtherState.SelectedValue == "0")
            //{
            //    Other_State = "";
            //    Non_Other_State = "";
            //}
            //else if (RdbOtherState.SelectedValue == "1")
            //{
            //    Other_State = "Yes";
            //    Non_Other_State = "";
            //}
            //else if (RdbOtherState.SelectedValue == "2")
            //{
            //    Other_State = "";
            //    Non_Other_State = "No";
            //}
            //else
            //{
            //    Other_State = "";
            //    Non_Other_State = "";
            //}
            //if (ChkOtherState.Checked == true)
            //{
            //    Other_State = "Yes";
            //}
            //else
            //{
            //    Other_State = "No";
            //}

            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    //}
                    string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                    string Payslip_Format_Type = "";
                    //if (SessionUserType == "2")
                    //{
                    //    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                    //}
                    //else
                    //{
                    //    Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    //}
                    string Str_PFType = "";
                    if (SessionUserType == "2")
                    {
                        Str_PFType = RdpIFPF.SelectedValue.ToString();
                    }
                    else
                    {
                        Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    }

                    string Str_ChkLeft = "";
                    //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                    //if (ChkLeft.Checked == true)
                    //{
                    //    Str_ChkLeft = "1";
                    //}
                    //else { Str_ChkLeft = "0"; }

                    string SalaryType = "";
                    //if (SessionUserType == "2")
                    //{
                    //    SalaryType = rbIFsalary.SelectedValue;
                    //}
                    //else
                    //{
                    //    SalaryType = rbsalary.SelectedValue;
                    //}

                    string ExemptedStaff = "";
                    //if (chkExment.Checked == true)
                    //{
                    //    ExemptedStaff = "1";
                    //}
                    string Bank_Name_Filter = "";
                    if (txtBankName.SelectedValue != "-Select-")
                    {
                        Bank_Name_Filter = txtBankName.SelectedValue.ToString();
                    }


                    ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type.ToString() + "&PFTypePost=" + Str_PFType.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + "" + "&Division=" + "" + "&Report_Type=SignList_Salary_Slip" + "&BankName=" + Bank_Name_Filter + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void BtnESIReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string CmpName = "";
        string Cmpaddress = "";
        int YR = 0;
        string SalaryType = "";
        string query = "";
        string ExemptedStaff = "";
        string report_head = "";
        string Basic_Report_Date = "";
        string Basic_Report_Type = "";


        if (ddlMonths.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
        }

        //if (ddlcategory.SelectedValue == "-Select-")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
        //    ErrFlag = true;
        //}
        //if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
        //    ErrFlag = true;
        //}

        if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
            ErrFlag = true;
        }

        if ((txtfrom.Text == "") || (txtTo.Text == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the From Date And ToDate');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            try
            {
                if (!ErrFlag)
                {
                    query = "Select EM.ExistingCode,EM.Wages,EM.UAN,EM.FirstName,isnull(SD.ProvidentFund,'0') as ProvidentFund,isnull(SD.ESI,'0') as ESI,";
                    query = query + " isnull(SD.WorkedDays,'0') as WorkedDays,isnull(SD.BasicAndDANew,'0') as BasicAndDANew,isnull(SD.BasicHRA,'0') as BasicHRA,";
                    query = query + " isnull(SD.ConvAllow,'0') as ConvAllow,isnull(SD.EduAllow,'0') as EduAllow,isnull(SD.MediAllow,'0') as MediAllow,";
                    query = query + " isnull(SD.BasicRAI,'0') as BasicRAI,isnull(SD.WashingAllow,'0') as WashingAllow,EM.ESINo from Employee_Mst EM ";
                    query = query + " Inner join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=EM.MachineID";
                    query = query + " where SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                    query = query + " And SD.Month='" + ddlMonths.SelectedItem.Text.ToString() + "'";
                    query = query + " And EM.Eligible_ESI='1'";
                    if (txtfrom.Text.ToString() != "" && txtTo.Text.ToString() != "")
                    {
                        query = query + " and SD.FromDate=CONVERT(datetime,'" + txtfrom.Text + "',103)";
                        query = query + " and SD.ToDate=CONVERT(datetime,'" + txtTo.Text + "',103)";
                    }
                    if (txtEmployeeType.SelectedItem.Text != "-Select-")
                    {
                        query = query + " and EM.Wages='" + txtEmployeeType.SelectedItem.Text.ToString() + "'";
                    }
                    if (ddlcategory.SelectedItem.Text != "-Select-")
                    {
                        query = query + " and EM.CatName='" + ddlcategory.SelectedItem.Text.ToString() + "'";
                    }

                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_1.Rows.Count != 0)
                    {
                        int colSpan = dt_1.Columns.Count + 1;


                        string attachment = "attachment;filename=ESI_Report.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        //DataTable dt = new DataTable();
                        DataTable dt = new DataTable();

                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                        }


                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);

                        Response.Write("<table broder='1'><tr style='font-weight: bold;border-style: solid;'>");
                        Response.Write("<td>S.NO</td><td>T.NO</td><td>EMPLOYEE TYPE</td><td>ESI NO</td><td>EMP NAME</td>");
                        Response.Write("<td>PRESENT DAYS</td><td>ESI SOURCE</td><td>ESI AMOUNT</td>");
                        Response.Write("</tr></table>");
                        for (int i = 0; i < dt_1.Rows.Count; i++)
                        {
                            Response.Write("<table broder='1'><tr style='border-style: solid;'>");
                            Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ExistingCode"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Wages"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ESINo"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["FirstName"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
                            string PF_Salary_Calculate = "0";
                            PF_Salary_Calculate = (Convert.ToDecimal(dt_1.Rows[i]["BasicAndDANew"].ToString()) + Convert.ToDecimal(dt_1.Rows[i]["BasicHRA"].ToString())).ToString();
                            PF_Salary_Calculate = (Convert.ToDecimal(PF_Salary_Calculate) + Convert.ToDecimal(dt_1.Rows[i]["ConvAllow"].ToString())).ToString();
                            PF_Salary_Calculate = (Convert.ToDecimal(PF_Salary_Calculate) + Convert.ToDecimal(dt_1.Rows[i]["EduAllow"].ToString())).ToString();
                            PF_Salary_Calculate = (Convert.ToDecimal(PF_Salary_Calculate) + Convert.ToDecimal(dt_1.Rows[i]["MediAllow"].ToString())).ToString();
                            PF_Salary_Calculate = (Convert.ToDecimal(PF_Salary_Calculate) + Convert.ToDecimal(dt_1.Rows[i]["BasicRAI"].ToString())).ToString();
                            PF_Salary_Calculate = (Convert.ToDecimal(PF_Salary_Calculate) + Convert.ToDecimal(dt_1.Rows[i]["WashingAllow"].ToString())).ToString();
                            
                            Response.Write("<td>" + PF_Salary_Calculate + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
                            Response.Write("</tr></table>");
                        }
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                    }
                    //Response.Write("<table>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + CmpName + " - NEFT TRANSFER REPORT");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + SessionLcode + "");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + Cmpaddress + "");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //string Salary_Head = "";

                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='colspan='" + colSpan + "'>");
                    ////Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                    //Response.Write(Salary_Head);
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    ////}
                    //Response.Write("</table>");

                    //Response.Write(stw.ToString());

                    ////Response.Write("<table border='1'>");
                    ////Response.Write("<tr Font-Bold='true'>");
                    ////Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    ////Response.Write("Grand Total");
                    ////Response.Write("</td>");

                    //Int32 Grand_Tot_End = 0;

                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = BankGV.Rows.Count + 5;

                    //Response.Write("<td>=sum(E6:E" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("</tr></table>");

                    //Response.Write("</tr>");
                    //Response.Write("</table>");



                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    protected void BtnSupplementary_Report_Click(object sender, EventArgs e)
    {
        
        bool ErrFlag = false;
        
        string ReportName = "";

        if (ddlSalarytype.SelectedValue != "0")
        {
            if (ddlSalarytype.SelectedValue == "Bank")
            {
                ReportName = "Supplementary_Report_Account_Emp";
            }
            else if (ddlSalarytype.SelectedValue == "Non_Bank")
            {
                ReportName = "Supplementary_Report_Non_Account_Emp";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Salary Type');", true);
            ErrFlag = true;
        }

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            ErrFlag = true;
        }
        if (ddlMonths.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
            ErrFlag = true;
        }
        if (txtfrom.Text == "" || txtTo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Date Properly');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("PaySlipRpt.aspx?RptName=" + ReportName + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString().Trim() + "&FinYearVal=" + ddlFinance.SelectedValue + "&Month=" + ddlMonths.SelectedItem.Text.ToString().Trim() + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text, "_blank", "");
        }
    }

    protected void BtnEmpCoverSlip_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;

        string ReportName = "";

        if (ddlSalarytype.SelectedValue != "0")
        {
            if (ddlSalarytype.SelectedValue == "Bank")
            {
                ReportName = "Employee_Payslip_Cover_Report_Account_Emp";
            }
            else if (ddlSalarytype.SelectedValue == "Non_Bank")
            {
                ReportName = "Employee_Payslip_Cover_Report_Non_Account_Emp";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Salary Type');", true);
            ErrFlag = true;
        }

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            ErrFlag = true;
        }
        if (ddlMonths.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
            ErrFlag = true;
        }
        if (txtfrom.Text == "" || txtTo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Date Properly');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("PaySlipRpt.aspx?RptName=" + ReportName + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString().Trim() + "&FinYearVal=" + ddlFinance.SelectedValue + "&Month=" + ddlMonths.SelectedItem.Text.ToString().Trim() + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text, "_blank", "");
        }
    }

    protected void btnAllCategory_Abstract_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string CmpName = "";
        string Cmpaddress = "";
        int YR = 0;
        string SalaryType = "";
        string query = "";
        string ExemptedStaff = "";
        string report_head = "";
        string Basic_Report_Date = "";
        string Basic_Report_Type = "";


        if (ddlMonths.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
        }

        if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
            ErrFlag = true;
        }

        if ((txtfrom.Text == "") || (txtTo.Text == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the From Date And ToDate');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            try
            {
                if (!ErrFlag)
                {
                    query = "";
                    query = "Select EM.Wages,isnull(Sum(SD.BasicandDA),'0') as BasicandDA,isnull(sum(SD.ProvidentFund),'0') as ProvidentFund,isnull(sum(SD.ESI),'0') as ESI,isnull(sum(SD.Conveyance),'0') as Conveyance,isnull(sum(SD.GrossEarnings),'0') as GrossEarnings,isnull(sum(SD.PerIncentive),'0') as PerIncentive,isnull(sum(SD.allowances1),'0') as allowances1,";
                    query = query + "isnull(sum(SD.allowances2),'0') as allowances2,isnull(sum(SD.allowances3),'0') as allowances3,isnull(sum(SD.allowances4),'0') as allowances4,isnull(sum(SD.allowances5),'0') as allowances5,isnull(sum(SD.Deduction1),'0') as Deduction1,isnull(sum(SD.Deduction4),'0') as Deduction4,isnull(sum(SD.Deduction2),'0') as Deduction2,";
                    query = query + "isnull(sum(SD.Deduction3),'0') as Deduction3,isnull(sum(SD.Deduction5),'0') as Deduction5,isnull(sum(SD.Messdeduction),'0') as Messdeduction,isnull(sum(SD.Losspay),'0') as Losspay,isnull(sum(SD.TotalDeductions),'0') as TotalDeductions,isnull(sum(SD.NetPay),'0') as NetPay,isnull(Sum(SD.OTHoursNew),'0') as OTHoursNew,";
                    query = query + "isnull(Sum(SD.OTHoursAmtNew),'0') as OTHoursAmtNew,isnull(Sum(SD.NFh),'0') as NFh,isnull(Sum(SD.WorkedDays),'0') as WorkedDays,isnull(Sum(SD.WH_Work_Days),'0') as WH_Work_Days,isnull(Sum(SD.BasicAndDANew),'0') as BasicAndDANew,isnull(Sum(SD.BasicHRA),'0') as BasicHRA,isnull(Sum(SD.ThreesidedAmt),'0') as ThreesidedAmt,";
                    query = query + "isnull(Sum(SD.ConvAllow),'0') as ConvAllow,isnull(Sum(SD.EduAllow),'0') as EduAllow,isnull(Sum(SD.MediAllow),'0') as MediAllow,isnull(Sum(SD.BasicRAI),'0') as BasicRAI,isnull(Sum(SD.WashingAllow),'0') as WashingAllow,isnull(Sum(SD.RoundOffNetPay),'0') as RoundOffNetPay,isnull(Sum(SD.DedOthers1),'0') as DedOthers1,";
                    query = query + "isnull(Sum(SD.DedOthers2),'0') as DedOthers2,isnull(Sum(SD.Advance),'0') as Advance,isnull(Sum(SD.EmployeerESI),'0') as EmployeerESI,isnull(sum(SD.Sweeper),'0') as Sweeper,isnull(sum(SD.Unioncg),'0') as Unioncg,isnull(sum(SD.Fine),'0') as Fine from Employee_Mst EM Inner join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=EM.MachineID ";
                    query = query + "where SD.Month='" + ddlMonths.SelectedItem.Text + "' and SD.FinancialYear='" + ddlFinance.SelectedValue.ToString() + "'";
                    if (ddlSalarytype.SelectedValue != "0")
                    {
                        if (ddlSalarytype.SelectedValue == "Bank")
                        {
                            query = query + " and (EM.AccountNo!='' or EM.AccountNo is not null)";
                        }
                        else if (ddlSalarytype.SelectedValue == "Non_Bank")
                        {
                            query = query + " and (EM.AccountNo='' or EM.AccountNo is null)";
                        }
                    }                    
                    if (txtfrom.Text.ToString() != "" && txtTo.Text.ToString() != "")
                    {
                        query = query + " and SD.FromDate=CONVERT(datetime,'" + txtfrom.Text + "',103)";
                        query = query + " and SD.ToDate=CONVERT(datetime,'" + txtTo.Text + "',103)";
                    }
                    query = query + " group by EM.Wages Order by EM.Wages";
                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_1.Rows.Count != 0)
                    {
                        int colSpan = dt_1.Columns.Count + 1;


                        string attachment = "attachment;filename=AllCategory_Abstract_Report.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        //DataTable dt = new DataTable();
                        DataTable dt = new DataTable();
                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                        }


                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);

                        Response.Write("<table broder='1'><tr style='font-weight: bold;border-style: solid;'>");
                        Response.Write("<td>CATEGORY</td><td>BASIC PAY</td><td>DA</td><td>HRA</td>");
                        Response.Write("<td>T.ALLOW</td><td>M.ALLOW</td><td>E.ALLOW</td>");
                        Response.Write("<td>W.ALLOW</td><td>WP AMT</td><td>OT AMT</td>");
                        Response.Write("<td>DAYS ALLOW</td><td>SD ALLOW</td><td>MASTRY ALLOW</td>");
                        Response.Write("<td>OTHERS EARNINGS</td><td>TOTAL EARNINGS</td><td>PF</td>");
                        Response.Write("<td>ESI</td><td>TDS</td><td>LWF</td>");
                        Response.Write("<td>ADVANCE</td><td>MESS</td><td>RENT</td>");
                        Response.Write("<td>SWEEPER</td><td>UNION</td><td>FINE</td>");
                        Response.Write("<td>OTHERS 1</td><td>OTHERS 2</td><td>TOTAL DEDUCTIONS</td>");
                        Response.Write("<td>ROUND OFF DIFFERENCE</td><td>CHECKLIST NET AMOUNT</td>");
                        Response.Write("</tr></table>");
                        for (int i = 0; i < dt_1.Rows.Count; i++)
                        {
                            Response.Write("<table broder='1'><tr style='border-style: solid;'>");
                            Response.Write("<td>" + dt_1.Rows[i]["Wages"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["BasicAndDANew"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["BasicRAI"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["BasicHRA"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ConvAllow"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["MediAllow"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["EduAllow"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["WashingAllow"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ThreesidedAmt"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["OTHoursAmtNew"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");                            
                            Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Sweeper"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Unioncg"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["Fine"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["DedOthers1"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["DedOthers2"].ToString() + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
                            string Total_Earning_Str = dt_1.Rows[i]["NetPay"].ToString();
                            string Total_Deduction_Str = dt_1.Rows[i]["TotalDeductions"].ToString();
                            string Paid_Net_Amt_Str = dt_1.Rows[i]["RoundOffNetPay"].ToString();
                            string With_Out_Round_Val = "0";
                            string With_Round_Val = "0";
                            With_Out_Round_Val = (Convert.ToDecimal(Total_Earning_Str) - Convert.ToDecimal(Total_Deduction_Str)).ToString();
                            With_Round_Val = (Convert.ToDecimal(Paid_Net_Amt_Str) - Convert.ToDecimal(With_Out_Round_Val)).ToString();

                            Response.Write("<td>" + With_Round_Val + "</td>");
                            Response.Write("<td>" + dt_1.Rows[i]["RoundOffNetPay"].ToString() + "</td>");                            
                            Response.Write("</tr></table>");
                        }
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                    }
                    //Response.Write("<table>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + CmpName + " - NEFT TRANSFER REPORT");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + SessionLcode + "");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='" + colSpan + "'>");
                    //Response.Write("" + Cmpaddress + "");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    //string Salary_Head = "";

                    //Response.Write("<tr align='Center'>");
                    //Response.Write("<td colspan='colspan='" + colSpan + "'>");
                    ////Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                    //Response.Write(Salary_Head);
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    ////}
                    //Response.Write("</table>");

                    //Response.Write(stw.ToString());

                    ////Response.Write("<table border='1'>");
                    ////Response.Write("<tr Font-Bold='true'>");
                    ////Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    ////Response.Write("Grand Total");
                    ////Response.Write("</td>");

                    //Int32 Grand_Tot_End = 0;

                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = BankGV.Rows.Count + 5;

                    //Response.Write("<td>=sum(E6:E" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("</tr></table>");

                    //Response.Write("</tr>");
                    //Response.Write("</table>");



                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string ReportName = "Payslip_Report";
        if (ddlcategory.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Category');", true);
            return;
        }
        if (txtEmployeeType.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type');", true);
            return;
        }
        if (ddlFinance.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Financial Year');", true);
            return;
        }
        if (ddlMonths.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month');", true);
            return;
        }
        if (txtfrom.Text == "" || txtTo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the FromDate and To Date Properly');", true);
            return;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("ViewReportNew.aspx?CatName=" + ddlCattype.SelectedValue + "&FinYearVal=" + ddlFinance.SelectedValue + "&finYearVal=" + ddlFinance.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&EmployeeTypeCd=" + txtEmployeeType.SelectedValue + "&RptName=" + ReportName, "_blank", "");
        }
    }

    protected void btnNewReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string ReportName = "Payslip_Report_New";
        if (ddlcategory.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Category');", true);
            return;
        }
        if (txtEmployeeType.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type');", true);
            return;
        }
        if (ddlFinance.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Financial Year');", true);
            return;
        }
        if (ddlMonths.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month');", true);
            return;
        }
        if (txtfrom.Text == "" || txtTo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the FromDate and To Date Properly');", true);
            return;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("ViewReportNew.aspx?CatName=" + ddlCattype.SelectedValue + "&FinYearVal=" + ddlFinance.SelectedValue + "&finYearVal=" + ddlFinance.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&EmployeeTypeCd=" + txtEmployeeType.SelectedValue + "&RptName=" + ReportName, "_blank", "");
        }
    }
}
