﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class MstReelingCountEntry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEpay;
    bool ErrFlg = false;
    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "Payroll::Reeling Wages Slab Master";
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
                Get_Last_Code();
            }

        }
        Load_Data();
       
    }

    private void Get_Last_Code()
    {
        con = new SqlConnection(constr);
        string getNo = "0";
        SSQL = "";
        SSQL = "Select Max(Code) from ["+ SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SqlCommand Cmd = new SqlCommand(SSQL, con);
        con.Open();
        getNo = Cmd.ExecuteScalar().ToString();
        con.Close();
        if (getNo == "0"||getNo==""||getNo==string.Empty||getNo==null)
        {
            getNo = "1";
        }
        else
        {
            getNo = (Convert.ToInt32(getNo) + Convert.ToInt32(1)).ToString();
        }

        txtCode.Text = getNo.ToString();
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtCode.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Code');", true);
            return;
        }
        if (txtPriceRate.Text == "" || txtPriceRate.Text == "0")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Price Rate');", true);
            return;
        }
        if (txtWorkLoad.Text == "" || txtWorkLoad.Text == "0")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Work Load');", true);
            return;
        }
        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Select * from [" + SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and (Code='" + txtCode.Text + "' or WorkLoad='" + txtWorkLoad.Text + "')";
            DataTable DT_Check = new DataTable();
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check != null && DT_Check.Rows.Count > 0)
            {
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Code or WorkLoad is Already Present');", true);
                return;
            }
            if (!ErrFlg)
            {
                if (btnSave.Text == "Update")
                {
                    SSQL = "";
                    SSQL = "Delete from [" + SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " and  WorkLoad='" + txtWorkLoad.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstReelingWagesSlab(Ccode,Lcode,FinYearCode,FinYearVal,Code,PriceRate,WorkLoad,OTPriceRate,CreatedBy,CreatedOn)";
                SSQL = SSQL + " values('"+SessionCcode+"','"+SessionLcode+"','"+ddlfinance.SelectedItem.Text+"','"+ddlfinance.SelectedValue+"','"+txtCode.Text+"',";
                SSQL = SSQL + "'" + txtPriceRate.Text + "','" + txtWorkLoad.Text + "','" + txtOTPriceRate.Text + "','" + SessionUserID + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WorkLoad Added Successfully');", true);
                }
                if(btnSave.Text=="Update")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WorkLoad Updated Successfully');", true);
                }
                btnClr_Click(sender, e);
            }
        }
    }
    protected void btnClr_Click(object sender, EventArgs e)
    {
        Get_Last_Code();
        ddlfinance.ClearSelection();
        txtPriceRate.Text = "0";
        txtWorkLoad.Text = "0";
        txtOTPriceRate.Text = "0";
        btnSave.Text = "Save";
        Load_Data();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + "and Code='" + e.CommandArgument.ToString() + "'";
        DataTable Edit_Data = new DataTable();
        Edit_Data = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Edit_Data != null && Edit_Data.Rows.Count > 0)
        {
            txtCode.Text = Edit_Data.Rows[0]["Code"].ToString();
            txtPriceRate.Text = Edit_Data.Rows[0]["PriceRate"].ToString();
            txtWorkLoad.Text= Edit_Data.Rows[0]["WorkLoad"].ToString();
            txtOTPriceRate.Text = Edit_Data.Rows[0]["OTPriceRate"].ToString();
            btnSave.Text = "Update";
            Load_Data();
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + "and Code='" + e.CommandArgument.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WorkLoad Deleted Successfully');", true);
        Load_Data();
    }
}