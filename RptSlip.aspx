<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptSlip.aspx.cs" Inherits="RptSlip" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="SalPay" runat="server">
        <ContentTemplate>

            <script type="text/javascript">
                //On UpdatePanel Refresh
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                if (prm != null) {
                    prm.add_endRequest(function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            $('.select2').select2();
                            $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                autoclose: true
                            });
                        }
                    });
                };
            </script>


            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Report</a></li>
                    <li class="active">Pay Slip</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Pay Slip</h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Pay Slip</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">

                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <%--<label>Report Type</label>--%>
                                                <asp:RadioButtonList ID="rbtnReportType" runat="server" class="form-control" Style="width: 100%;" Visible="false"
                                                    RepeatDirection="Horizontal" AutoPostBack="true"
                                                    OnSelectedIndexChanged="rbtnReportType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" Text="Category Type" Selected="True" style="padding-right: 40px"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Agent Type" style="padding-right: 40px"></asp:ListItem>
                                                </asp:RadioButtonList>



                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <%--<label>Agent Name</label>--%>
                                                <asp:DropDownList runat="server" Visible="false" ID="ddlAgentName" Enabled="false" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" Enabled="false"
                                                    Style="width: 100%;" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
                                                    <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" Style="width: 100%;" Enabled="false">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fin. Year</label>
                                                <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Month</label>
                                                <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4" runat="server" id="IF_FromDate_Hide">
                                            <div class="form-group">
                                                <label>FromDate</label>
                                                <asp:TextBox ID="txtfrom" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtfrom" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4" runat="server" id="IF_ToDate_Hide">
                                            <div class="form-group">
                                                <label>ToDate</label>
                                                <asp:TextBox ID="txtTo" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtTo" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                        <!-- begin col-4 -->

                                        <div class="col-md-5" runat="server" visible="false" id="IF_Report_Type">
                                            <div class="form-group">
                                                <label>Payslip Format Type</label>
                                                <asp:RadioButtonList ID="rdbPayslipIFFormat" runat="server" RepeatColumns="2"
                                                    TabIndex="3" Width="220" Visible="false" class="form-control" runat="server">
                                                    <asp:ListItem Selected="true" Text="Cover" Value="0"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Sign List" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-6" runat="server" id="IF_Salary_Through_Hide">
                                            <div class="form-group">
                                                <%--<label>Salary Through</label>--%>
                                                <asp:RadioButtonList ID="RdbCashBank" Visible="false" runat="server" RepeatColumns="3" class="form-control">
                                                    <asp:ListItem Selected="true" Text="ALL" style="padding-right: 40px" Value="0"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Cash" style="padding-right: 40px" Value="1"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-6" runat="server" id="IF_PF_NON_PF_Hide">
                                            <div class="form-group">
                                                <%--<label>PF/Non PF</label>--%>

                                                <asp:RadioButtonList Visible="false" ID="RdbPFNonPF" runat="server" RepeatColumns="3" class="form-control">
                                                    <asp:ListItem Selected="true" Text="ALL" style="padding-right: 40px" Value="0"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="PF" style="padding-right: 40px" Value="1"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:RadioButtonList ID="RdpIFPF" runat="server" RepeatColumns="1"
                                                    TabIndex="3" Width="180" Visible="false">
                                                    <asp:ListItem Selected="true" Text="PF" Value="1"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">


                                        <div class="col-md-4">
                                            <div class="form-group" runat="server">
                                                <label>Category Type</label>
                                                <asp:DropDownList runat="server" ID="ddlCattype" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4" runat="server" visible="false">
                                            <div class="form-group" runat="server">
                                                <label>Bank Name</label>
                                                <asp:DropDownList runat="server" ID="txtBankName" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4" runat="server" visible="false">
                                            <div class="form-group" runat="server">
                                                <label>Salary Type</label>
                                                <asp:DropDownList runat="server" ID="ddlSalarytype" class="form-control select2" Style="width: 100%;">
                                                    <asp:ListItem Text="Bank" Value="Bank"></asp:ListItem>
                                                    <asp:ListItem Text="Non_Bank" Value="Non_Bank"></asp:ListItem>
                                                    <asp:ListItem Text="-Select-" Value="0" Selected="True">
                                                    </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-12" align="center">
                                            <br />
                                            <asp:Button runat="server" Visible="false" ID="btnSearch" Text="Export" class="btn btn-warning" ValidationGroup="Validate_PayField" OnClick="btnSearch_Click" />
                                            <asp:Button runat="server" Visible="false" ID="btnExport" Text="Bank Salary" class="btn btn-warning" ValidationGroup="Validate_ExpField" OnClick="btnExport_Click" />
                                            <asp:Button ID="btnPayslip" runat="server" Text="Check List"
                                                class="btn btn-primary" Visible="false" ValidationGroup="Validate_ExpField"
                                                OnClick="btnPayslip_Click" />
                                            <asp:Button runat="server" Visible="false" ID="BtnSignList_PDF" Text="Sign List" class="btn btn-success" OnClick="BtnSignList_PDF_Click" />
                                            <asp:Button ID="BtnEmpCoverSlip" Visible="false" runat="server" Text="Cover Slip"
                                                class="btn btn-primary" ValidationGroup="Validate_ExpField"
                                                OnClick="BtnEmpCoverSlip_Click" />
                                            <asp:Button ID="btnWagesAbstract" Visible="false" runat="server" Text="Wages Abstract"
                                                class="btn btn-info" ValidationGroup="Validate_ExpField"
                                                OnClick="btnWagesAbstract_Click" />
                                            <asp:Button ID="BtnPFReport" Visible="false" runat="server" Text="PF Report"
                                                class="btn btn-info" ValidationGroup="Validate_ExpField"
                                                OnClick="BtnPFReport_Click" />
                                            <asp:Button ID="BtnESIReport" Visible="false" runat="server" Text="ESI Report"
                                                class="btn btn-info" ValidationGroup="Validate_ExpField"
                                                OnClick="BtnESIReport_Click" />
                                            <asp:Button ID="BtnSupplementary_Report" Visible="false" runat="server" Text="Supplementary"
                                                class="btn btn-primary" ValidationGroup="Validate_ExpField"
                                                OnClick="BtnSupplementary_Report_Click" />
                                            <asp:Button ID="btnAllCategory_Abstract" Visible="false" runat="server" Text="ALL Category Abstract"
                                                class="btn btn-info" ValidationGroup="Validate_ExpField"
                                                OnClick="btnAllCategory_Abstract_Click" />
                                            <asp:Button ID="btnReport" runat="server" Text="Report"
                                                class="btn btn-success" ValidationGroup="Validate_ExpField"
                                                OnClick="btnReport_Click" />
                                            <asp:Button ID="btnNewReport" runat="server" Text="New Report"
                                                class="btn btn-primary" ValidationGroup="Validate_ExpField"
                                                OnClick="btnNewReport_Click" />
                                        </div>
                                    </div>

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-10">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                    </div>
                                    <!-- end row -->

                                    <asp:Panel runat="server" ID="PnlOthers" Visible="false">
                                        <tr id="TrOthers" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GvOthers" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAmt" runat="server" Text='<%# Eval("PFNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAmt" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Work Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Nfh Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNfh" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDays" runat="server" Text='<%# Eval("Availabledays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Wages</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFbasic" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>





                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblProvidentFund" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>LIC</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIncomeTax" runat="server" Text='<%# Eval("IncomeTax") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Ded.</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalOne" runat="server" Text='<%# Eval("TotalDedFirst") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Gross Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("TotalOne") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvace2" runat="server" Text='<%# Eval("Advace2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvace3" runat="server" Text='<%# Eval("Advance3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Home</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHouseAmt" runat="server" Text='<%# Eval("HouseAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Temple</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTempleAmt" runat="server" Text='<%# Eval("TempleAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Stores</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>H.Loan</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHloan" runat="server" Text='<%# Eval("Hloan") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Com. Dedt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Othr. Dedt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Ded.</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDed" runat="server" Text='<%# Eval("TotalDed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>RL.OT</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblallowances5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl" runat="server" Text='<%# Eval("NetFinal") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <%-- <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TDS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="PnlMonthWorker" Visible="false">
                                        <tr id="TrMonthWorker" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GvMonthWorker" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Basic</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAmt" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed DA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDaAmt" runat="server" Text='<%# Eval("DaAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed DA Rate</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWH_Work_Days" runat="server" Text='<%# Eval("DaRateFixed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Total</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixTotal" runat="server" Text='<%# Eval("TotalFixed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Work Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Nfh Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNfh" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDays" runat="server" Text='<%# Eval("Availabledays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFbasic" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>DA Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDaFixed" runat="server" Text='<%# Eval("DaFixed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>DA Rate</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDaRateAmt" runat="server" Text='<%# Eval("DaRateAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalBasicAmt" runat="server" Text='<%# Eval("TotalBasicAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblProvidentFund" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>LIC</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIncomeTax" runat="server" Text='<%# Eval("IncomeTax") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalOne" runat="server" Text='<%# Eval("TotalOne") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>HRA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Comms</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCommsAmt" runat="server" Text='<%# Eval("CommsAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Gross Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrossEarnings" runat="server" Text='<%# Eval("GrossOne") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvace2" runat="server" Text='<%# Eval("Advace2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvace3" runat="server" Text='<%# Eval("Advance3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Home</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHouseAmt" runat="server" Text='<%# Eval("HouseAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Temple</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTempleAmt" runat="server" Text='<%# Eval("TempleAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>H.Loan</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHloan" runat="server" Text='<%# Eval("Hloan") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Stores</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Com. Dedt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Othr. Dedt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Ded.</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDed" runat="server" Text='<%# Eval("TotalDed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <%-- <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TDS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>

                                    <asp:Panel ID="PnlStaffScheck" runat="server" Visible="false">
                                        <tr id="TrStaffScheck" visible="false" runat="server">
                                            <td colspan="4">
                                                <asp:GridView ID="GvStaffCheck" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Basic</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAmt" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed DA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDaAmt" runat="server" Text='<%# Eval("DaRateFixed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed DA Rate</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWH_Work_Days" runat="server" Text='0'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Total</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixTotal" runat="server" Text='<%# Eval("TotalFixed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Work Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Nfh Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNfh" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDays" runat="server" Text='<%# Eval("Availabledays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOTdays" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFbasic" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>DA Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDaFixed" runat="server" Text='<%# Eval("DaRateAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>DA Rate</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDaRateAmt" runat="server" Text='0'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalBasicAmt" runat="server" Text='<%# Eval("TotalBasicAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblProvidentFund" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>LIC</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIncomeTax" runat="server" Text='<%# Eval("IncomeTax") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalOne" runat="server" Text='<%# Eval("TotalOne") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>HRA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("HRAamt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Comms</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCommsAmt" runat="server" Text='<%# Eval("TAamt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOtAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Gross Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrossEarnings" runat="server" Text='<%# Eval("GrossOne") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others Allows</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblallowances5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvace2" runat="server" Text='<%# Eval("Advace2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvace3" runat="server" Text='<%# Eval("Advance3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Home</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHouseAmt" runat="server" Text='<%# Eval("HouseAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Temple</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTempleAmt" runat="server" Text='<%# Eval("TempleAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>H.Loan</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHloan" runat="server" Text='<%# Eval("Hloan") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Stores</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Com. Dedt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Othr. Dedt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeduction4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Ded.</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDed" runat="server" Text='<%# Eval("TotalDed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <%-- <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TDS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>


                                    <asp:Panel runat="server" ID="PnlTrStaff" Visible="false">
                                        <!--  STAFF SALARY Details GridView Start  -->
                                        <tr id="TrStaff" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GVStaff" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Designation</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Working Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NFH</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>W.H</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWH_Work_Days" runat="server" Text='<%# Eval("WH_Work_Days") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcl" runat="server" Text='<%# Eval("CL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>LeaveCredit</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveCredit" runat="server" Text='<%# Eval("LeaveCredit") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>LOPDays</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLOPDays" runat="server" Text='<%# Eval("LOPDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>LOPAmount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLOPAmount" runat="server" Text='<%# Eval("Losspay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>HRA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>RAI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Earning</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>P.F</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Canteen</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>TDS</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Roundoff</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--  STAFF SALARY Details GridView End  -->
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="PnltrSub_Staff" Visible="false">
                                        <!--  SUB-STAFF SALARY Details GridView Start  -->
                                        <tr id="trSub_Staff" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GVSub_Staff" runat="server" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Designation</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Working Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NFH</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcl" runat="server" Text='<%# Eval("ThreeSided") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOTHoursNew" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcl" runat="server" Text='<%# Eval("CL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>HRA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>RAI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Days Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOtDaysAmt" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Day Incentive</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Spinning Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Earning</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>P.F</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Canteen</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Roundoff</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--  SUB-STAFF SALARY Details GridView End  -->
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="PnltrRegular" Visible="false">
                                        <!--  REGULAR SALARY Details GridView Start  -->
                                        <tr id="trRegular" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GVRegular" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Designation</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Working Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NFH</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcl" runat="server" Text='<%# Eval("ThreeSided") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcl" runat="server" Text='<%# Eval("CL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>HRA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>RAI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Days Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOtDaysAmt" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Day Incentive</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Spinning Allow</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Earning</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>P.F</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Canteen</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Roundoff</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--  REGULAR SALARY Details GridView End  -->
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="PnlTrHostel" Visible="false">
                                        <!--Hostel Salary Details GridView Start-->
                                        <tr id="TrHostel" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GVHostel" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Ticket No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Working Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("LOPDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--  <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <%-- <asp:TemplateField>
                                                                <HeaderTemplate>OT Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("ThreeSided") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("CL") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--   <asp:TemplateField>
                                                                <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>HRA</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Earning</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>P.F</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpf" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Mess</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Store</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("DedPF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetpayIF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--Hostel Salary Details GridView End-->
                                    </asp:Panel>




                                    <asp:Panel runat="server" ID="PnlTrCivil" Visible="false">
                                        <!--CIVIL Salary Details GridView Start-->
                                        <tr id="TrCivil" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="GVCivil" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Ticket No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>No Of Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <%--   <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOTHoursNew" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Wages</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("NonPFWages") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Attn. Incentive</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAttnIncentive" runat="server" Text='<%# Eval("Dayincentive") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Gross Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarningsOT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPF" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPF" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Mess</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Store</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("DedOthers1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("DedOthers2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>




                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("New_Cash_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF Bank</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBank" runat="server" Text='<%# Eval("New_Bank_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCash" runat="server" Text='<%# Eval("New_Allowance_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Pay</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBalance" runat="server" Text='<%# Eval("New_Tot_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--CIVIL Salary Details GridView End-->
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="Panel3" Visible="false">
                                        <tr id="Tr3" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="gvStaffNonPF" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Ticket No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Actual Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Actual") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Fixed") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>No Of Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <%--   <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOTHoursNew" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Wages</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("NonPFWages") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Attn. Incentive</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAttnIncentive" runat="server" Text='0'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Gross Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarningsOT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPF" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPF" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Mess</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Store</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("DedOthers1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("DedOthers2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>




                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("New_Cash_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF Bank</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBank" runat="server" Text='<%# Eval("New_Bank_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Allowance</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCash" runat="server" Text='<%# Eval("New_Allowance_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Pay</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBalance" runat="server" Text='<%# Eval("New_Tot_Amt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>



                                    <asp:Panel runat="server" ID="PnlStaffNormal" Visible="false">
                                        <tr id="TrStaffNom" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="gvStaffNormal" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Ticket No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Wages Per Day</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>No Of Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <%--   <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOTHoursNew" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Wages</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("NonPFWages") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT Hours Amt</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Attn. Incentive</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAttnIncentive" runat="server" Text='0'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Gross Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarningsOT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>PF</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPF" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ESI</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPF" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Advance</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Mess</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Store</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("DedOthers1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Others2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("DedOthers2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>




                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>

                                    <asp:Panel runat="server" Visible="false" ID="Pnlgv">
                                        <tr id="gv" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="gvSalary" runat="server" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="gvSalary_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Si. No
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1   %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Emp No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Ticket No
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                OldID
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOld" runat="server" Text='<%# Eval("OldID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Department
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Name
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                        <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                        <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Totalworkingdays</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltotalworkingdays" runat="server" Text='<%# Eval("Totalworkingdays") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Working Days
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>Total Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("tot") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>CL</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcl" runat="server" Text='<%# Eval("CL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Week Off</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblweek" runat="server" Text='<%# Eval("Weekoff") %>'></asp:Label>
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>Home Town Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("HomeDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                OT Hours
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Fixed Salary
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Fixed OT Salary
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixedOT" runat="server" Text='<%# Eval("PFS") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Base
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    FDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFDA" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    VDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblvda" runat="server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    TotalEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltot" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    PF Earnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPFEarnings" runat="server" Text='<%# Eval("PFEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                P.F
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                ESI
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Union</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUnion" runat="server" Text='<%# Eval("Unioncg") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Advance1
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Allowance 1
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Allowance 2
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Allowance 3
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Allowance 4
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Allowance 5
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    O/N SHIFT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("HalfNightAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    F/N SHIFT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("FullNightAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    SPINNING AMT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("SpinningAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    DAY INCENTIVE</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    THREE SIDED</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 3</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Deduction 4</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                Deduction 5
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                LOP
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbllop" runat="server" Text='<%# Eval("Losspay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Stamp</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblstamp" runat="server" Text='<%# Eval("Stamp") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Salary</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetSalary") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>OT</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Round off</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblround" runat="server"  Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Signature</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="PnlBankSalary" Visible="false">
                                        <!--  BANK SALARY Details GridView End  -->
                                        <tr id="BankSalary" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="BankSalaryGridView" runat="server" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>S. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NAME</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ACCOUNT NO</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NET SALARY</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetSalary") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>REMARKS</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--  BANK SALARY Details GridView End  -->

                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="Panel1" Visible="false">
                                        <!--  Labour BANK SALARY Others Details GridView End  -->
                                        <tr id="Tr1" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="OtherBankSalaryGridView" runat="server" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>S. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NAME</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ACCOUNT NO</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NET SALARY</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("Bank_Others") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>REMARKS</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <!--  BANK SALARY Details GridView End  -->

                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="Panel2" Visible="false">
                                        <tr id="Tr2" runat="server" visible="false">
                                            <td colspan="4">
                                                <asp:GridView ID="BankGV" runat="server" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>S. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NAME</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%-- <asp:TemplateField>
                                                                <HeaderTemplate>DESIGNATION</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ACCOUNT NO</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>NET CREDIT</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("RoundOffNetPay") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>ATM</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetSalary" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--<asp:TemplateField>
                                                                <HeaderTemplate>BANK CHARGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCharges" runat="server" Text="0.0"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                        <%-- <asp:TemplateField>
                                                                <HeaderTemplate>TOTAL</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text="0.0"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExport" />
            <asp:PostBackTrigger ControlID="BtnPFReport" />
            <asp:PostBackTrigger ControlID="BtnESIReport" />
            <asp:PostBackTrigger ControlID="btnAllCategory_Abstract" />

        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

