﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class LeaderCommissionReportNew : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    DataTable DT = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Leader Commission Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Date = Request.QueryString["Date"].ToString();

            DataTable AutoDT = new DataTable();
            AutoDT.Columns.Add("AgentName");
            AutoDT.Columns.Add("TotNos", typeof(decimal));
            AutoDT.Columns.Add("Grade1", typeof(decimal));
            AutoDT.Columns.Add("Grade2", typeof(decimal));
            AutoDT.Columns.Add("Grade3", typeof(decimal));
            AutoDT.Columns.Add("Grade4", typeof(decimal));
            AutoDT.Columns.Add("Grade5", typeof(decimal));
            AutoDT.Columns.Add("Grade6", typeof(decimal));
            AutoDT.Columns.Add("Shift1", typeof(decimal));
            AutoDT.Columns.Add("Shift2", typeof(decimal));
            AutoDT.Columns.Add("TotComm", typeof(decimal));
            AutoDT.Columns.Add("TotGrade1", typeof(decimal));
            AutoDT.Columns.Add("TotGrade2", typeof(decimal));
            AutoDT.Columns.Add("TotGrade3", typeof(decimal));
            AutoDT.Columns.Add("TotGrade4", typeof(decimal));
            AutoDT.Columns.Add("TotGrade5", typeof(decimal));
            AutoDT.Columns.Add("TotGrade6", typeof(decimal));
            AutoDT.Columns.Add("Tot_Comm", typeof(decimal));



            SSQL = "Select P.AgentName,Count(P.Tot_Nos)[TotNos],ISNULL(sum(P.A),'0')[Grade1],ISNULL(sum(P.[A+]),'0')[Grade2], ";
            SSQL = SSQL + " ISNULL(sum(P.[A+*]),'0')[Grade3],ISNULL(sum(P.[A++]),'0')[Grade4],ISNULL(sum(P.[T]),'0')[Grade5], ";
            SSQL = SSQL + " ISNULL(sum(P.[T+]),'0')[Grade6],sum(P.[8HRS])[Shift1],sum(P.[12HRS])[Shift2],";
            SSQL = SSQL + " (sum(P.[8HRS])*P.Comm1)+(sum(P.[12HRS])*P.Comm2)[TotComm] From ( ";
            SSQL = SSQL + " Select EM.AgentName,LTD.ExistingCode as [Tot_Nos],GRD.GradeName,CAST(Count(LTD.ExistingCode) as decimal(18,2))No_EMP, ";
            SSQL = SSQL + " Case when convert(decimal(18,2),LTD.Total_Hrs) <= CONVERT(decimal(18,2),'12.00') Then CAST( Count(LTD.ExistingCode) as decimal(18,2)) else 0 end [8HRS], ";
            SSQL = SSQL + " Case when convert(decimal(18,2),LTD.Total_Hrs) >= CONVERT(decimal(18,2),'12.01') Then CAST( Count(LTD.ExistingCode) as decimal(18,2)) else 0 end [12HRS], ";
            SSQL = SSQL + " AG.Shift8[Comm1],AG.Shift12[Comm2] from LogTime_Days LTD ";
            SSQL = SSQL + " Inner join Employee_Mst EM on EM.ExistingCode=LTD.ExistingCode and EM.IsActive='Yes' And EM.LocCode=LTD.LocCode  ";
            SSQL = SSQL + " Inner Join AgentMst AG on AG.AgentID=EM.AgentID and AG.LocCode=EM.LocCode inner join MstGrade GRD on GRD.GradeID=LTD.Grade and GRD.CompCode COLLATE DATABASE_DEFAULT=LTD.CompCode COLLATE DATABASE_DEFAULT";
            SSQL = SSQL + " Where LTD.Shift !='Leave' and LTD.LocCode='" + SessionLcode + "' and ";
            SSQL = SSQL + " Convert(datetime,LTD.Attn_Date,105)=CONVERT(datetime,'" + Date + "',105) ";
            SSQL = SSQL + " Group by  GRD.GradeName,LTD.ExistingCode,EM.AgentName,LTD.Total_Hrs,AG.Shift8,AG.Shift12) tmp ";
            SSQL = SSQL + " Pivot (Sum(tmp.No_EMP) for tmp.GradeName in ([A],[A+],[A++],[A+*],[T],[T+]) )as P ";
            SSQL = SSQL + " Group by P.AgentName,P.Comm1,P.Comm2 ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                for (int iRow = 0; iRow < DT.Rows.Count; iRow++)
                {
                    AutoDT.Rows.Add();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["AgentName"] = DT.Rows[iRow]["AgentName"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["TotNos"] = DT.Rows[iRow]["TotNos"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade1"] = DT.Rows[iRow]["Grade1"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade2"] = DT.Rows[iRow]["Grade2"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade3"] = DT.Rows[iRow]["Grade3"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade4"] = DT.Rows[iRow]["Grade4"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade5"] = DT.Rows[iRow]["Grade5"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade6"] = DT.Rows[iRow]["Grade6"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Shift1"] = DT.Rows[iRow]["Shift1"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Shift2"] = DT.Rows[iRow]["Shift2"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["TotComm"] = DT.Rows[iRow]["TotComm"].ToString();
                }
            }

            SSQL = "Select P.AgentName,Count(P.Tot_Nos)[TotNos],ISNULL(sum(P.A),'0')[Grade1],ISNULL(sum(P.[A+]),'0')[Grade2], ";
            SSQL = SSQL + " ISNULL(sum(P.[A+*]),'0')[Grade3],ISNULL(sum(P.[A++]),'0')[Grade4],ISNULL(sum(P.[T]),'0')[Grade5], ";
            SSQL = SSQL + " ISNULL(sum(P.[T+]),'0')[Grade6],sum(P.[8HRS])[Shift1],sum(P.[12HRS])[Shift2],";
            SSQL = SSQL + " (sum(P.[8HRS])*P.Comm1)+(sum(P.[12HRS])*P.Comm2)[TotComm] From ( ";
            SSQL = SSQL + " Select EM.AgentName,LTD.ExistingCode as [Tot_Nos],GRD.GradeName,CAST(Count(LTD.ExistingCode) as decimal(18,2))No_EMP, ";
            SSQL = SSQL + " Case when convert(decimal(18,2),LTD.Total_Hrs) <= CONVERT(decimal(18,2),'12.00') Then CAST( Count(LTD.ExistingCode) as decimal(18,2)) else 0 end [8HRS], ";
            SSQL = SSQL + " Case when convert(decimal(18,2),LTD.Total_Hrs) >= CONVERT(decimal(18,2),'12.01') Then CAST( Count(LTD.ExistingCode) as decimal(18,2)) else 0 end [12HRS], ";
            SSQL = SSQL + " AG.Shift8[Comm1],AG.Shift12[Comm2] from LogTime_Days LTD ";
            SSQL = SSQL + " Inner join Employee_Mst EM on EM.ExistingCode=LTD.ExistingCode and EM.IsActive='Yes' And EM.LocCode=LTD.LocCode  ";
            SSQL = SSQL + " Inner Join AgentMst AG on AG.AgentID=EM.AgentID and AG.LocCode=EM.LocCode inner join MstGrade GRD on GRD.GradeID=LTD.Grade and GRD.CompCode COLLATE DATABASE_DEFAULT=LTD.CompCode COLLATE DATABASE_DEFAULT";
            SSQL = SSQL + " Where LTD.Shift !='Leave' and LTD.LocCode='" + SessionLcode + "' and ";
            SSQL = SSQL + " Convert(datetime,LTD.Attn_Date,105)>=CONVERT(datetime,'01/" + Convert.ToDateTime(Date).ToString("MM/yyyy") + "',105) and ";
            SSQL = SSQL + " Convert(datetime,LTD.Attn_Date,105)<=CONVERT(datetime,'" + Date + "',105) ";
            SSQL = SSQL + " Group by  GRD.GradeName,LTD.ExistingCode,EM.AgentName,LTD.Total_Hrs,AG.Shift8,AG.Shift12) tmp ";
            SSQL = SSQL + " Pivot (Sum(tmp.No_EMP) for tmp.GradeName in ([A],[A+],[A++],[A+*],[T],[T+]) )as P ";
            SSQL = SSQL + " Group by P.AgentName,P.Comm1,P.Comm2 ";

            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                for (int IRow = 0; IRow < dt.Rows.Count; IRow++)
                {
                    DataRow dr = dt.Rows[IRow];
                    var row_Match = (from r in AutoDT.Rows.OfType<DataRow>() where r["AgentName"].ToString() == dr["AgentName"].ToString() select r).FirstOrDefault();
                    if (row_Match == null)
                    {
                        AutoDT.Rows.Add();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["AgentName"] = dt.Rows[IRow]["AgentName"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotNos"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade1"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade2"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade3"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade4"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade5"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Grade6"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Shift1"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Shift2"] = "0";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotComm"] = "0";

                        AutoDT.Rows[AutoDT.Rows.Count - 1]["AgentName"] = dt.Rows[IRow]["AgentName"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotGrade1"] = dt.Rows[IRow]["Grade1"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotGrade2"] = dt.Rows[IRow]["Grade2"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotGrade3"] = dt.Rows[IRow]["Grade3"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotGrade4"] = dt.Rows[IRow]["Grade4"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotGrade5"] = dt.Rows[IRow]["Grade5"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotGrade6"] = dt.Rows[IRow]["Grade6"].ToString();
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["Tot_Comm"] = dt.Rows[IRow]["TotComm"].ToString();
                    }
                    else
                    {
                        for (int i = 0; i < AutoDT.Rows.Count; i++)
                        {
                            if (dr["AgentName"].ToString() == AutoDT.Rows[i]["AgentName"].ToString())
                            {
                                AutoDT.Rows[i]["TotGrade1"] = dr["Grade1"].ToString();
                                AutoDT.Rows[i]["TotGrade2"] = dr["Grade2"].ToString();
                                AutoDT.Rows[i]["TotGrade3"] = dr["Grade3"].ToString();
                                AutoDT.Rows[i]["TotGrade4"] = dr["Grade4"].ToString();
                                AutoDT.Rows[i]["TotGrade5"] = dr["Grade5"].ToString();
                                AutoDT.Rows[i]["TotGrade6"] = dr["Grade6"].ToString();
                                AutoDT.Rows[i]["Tot_Comm"] = dr["TotComm"].ToString();
                                AutoDT.AcceptChanges();

                            }
                        }
                    }
                }
            }

            if (AutoDT.Rows.Count != 0)
            {
                DataTable dt1 = new DataTable();
                SSQL = "Select * from Company_Mst ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();

                ds.Tables.Add(AutoDT);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("New_HR_Report/LeaderCommissionReportNew.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
                report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode.ToString() + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
