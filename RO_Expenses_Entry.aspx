﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="RO_Expenses_Entry.aspx.cs" Inherits="RO_Expenses_Entry" Title="RO Expenses Entry" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Commission</a></li>
				<li class="active">RO Expenses</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">RO Expenses Details</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">RO Expenses Details</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>TransID</label>
								 <asp:Label runat="server" ID="txtTransid" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                               
                               <div class="col-md-4">
                                <div class="form-group">
                                    <label>Trans. Date</label>
                          	        <asp:TextBox runat="server" ID="txtTrans_Date" class="form-control datepicker" placeholder="dd/MM/YYYY">
                          	        </asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtTrans_Date" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTrans_Date" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                         <%--  --%>
                             <div class="col-md-4">
							   <div class="form-group">
								    <label>RO Name</label>
								    <asp:DropDownList runat="server" ID="txtROName" class="form-control select2">
							 	    </asp:DropDownList>
							 	    <asp:RequiredFieldValidator ControlToValidate="txtROName" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
								</div>
                             </div>                          
                          </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Start Date</label>
                          	        <asp:TextBox runat="server" ID="txtStartDate" class="form-control datepicker" placeholder="dd/MM/YYYY" 
                          	            ontextchanged="txtStartDate_TextChanged" AutoPostBack="true"></asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtStartDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtStartDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                           <!-- begin col-4 -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>End Date</label>
                          	        <asp:TextBox runat="server" ID="txtEndDate" class="form-control datepicker" 
                                        placeholder="dd/MM/YYYY" ontextchanged="txtEndDate_TextChanged" AutoPostBack="true"></asp:TextBox>
							        <asp:RequiredFieldValidator ControlToValidate="txtEndDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                           
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtEndDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>No.Of Days</label>
								<asp:Label runat="server" ID="txtNo_of_Days" class="form-control" BackColor="#F3F3F3" Text="0"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                             <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Place Of Visit</label>
								        <asp:TextBox runat="server" ID="txtPlaceOfVisit" class="form-control"></asp:TextBox>
								        <asp:RequiredFieldValidator ControlToValidate="txtPlaceOfVisit" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
									</div>
                                </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Vehicle No</label>
										<asp:TextBox runat="server" ID="txtVehicle_No" class="form-control"></asp:TextBox>
										 <asp:RequiredFieldValidator ControlToValidate="txtVehicle_No" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                  <div class="col-md-4">
									<div class="form-group">
										<label>Driver Name</label>
										<asp:TextBox runat="server" ID="txtDriverName" class="form-control"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtDriverName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
									</div>
                                  </div>
                                    <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Tot.KM Run</label>
										<asp:TextBox runat="server" ID="txtTotalKM_Run" class="form-control" Text="0"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtTotalKM_Run" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                                    <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Own Fuel Consumed(ltrs)</label>
										<asp:TextBox runat="server" ID="txtOwnFuel_Consumed" class="form-control" Text="0"></asp:TextBox>
										 <asp:RequiredFieldValidator ControlToValidate="txtOwnFuel_Consumed" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtOwnFuel_Consumed" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                                
                                  <div class="col-md-4">
									<div class="form-group">
										<label>Outside Fuel Consumed(ltrs)</label>
										<asp:TextBox runat="server" ID="txtOutSideFuel_Consumed" class="form-control" Text="0"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtOutSideFuel_Consumed" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                                  <!-- end col-4 -->
                                <!-- begin col-4 -->
                          </div>
                       <!-- end row -->
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Fuel Cost Own</label>
									<asp:TextBox runat="server" ID="txtFuel_Cost_Own" class="form-control" Text="0" 
									    ontextchanged="txtFuel_Cost_Own_TextChanged" AutoPostBack="true"></asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtFuel_Cost_Own" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>                                       
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtFuel_Cost_Own" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Fuel Cost OutSide</label>
									<asp:TextBox runat="server" ID="txtFuel_Cost_OutSide" class="form-control" Text="0" 
									    ontextchanged="txtFuel_Cost_OutSide_TextChanged" AutoPostBack="true"></asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtFuel_Cost_OutSide" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>                                       
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtFuel_Cost_OutSide" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Boarding/Lodging Exp.</label>
									<asp:TextBox runat="server" ID="txtBoarding_Lodging_Expenses" class="form-control" Text="0" 
									    ontextchanged="txtBoarding_Lodging_Expenses_TextChanged" AutoPostBack="true"></asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtBoarding_Lodging_Expenses" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>                                       
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtBoarding_Lodging_Expenses" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Toll Gate Fee</label>
									<asp:TextBox runat="server" ID="txtToll_Gate_Fees" class="form-control" Text="0" 
									    ontextchanged="txtToll_Gate_Fees_TextChanged" AutoPostBack="true"></asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtToll_Gate_Fees" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>                                       
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtToll_Gate_Fees" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Agent Expenses</label>
									<asp:TextBox runat="server" ID="txtAgent_Expenses" class="form-control" Text="0" 
									    ontextchanged="txtAgent_Expenses_TextChanged" AutoPostBack="true"></asp:TextBox>
									<asp:RequiredFieldValidator ControlToValidate="txtAgent_Expenses" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>                                       
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtAgent_Expenses" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-4">
							   <div class="form-group">
								    <label>Agent Name</label>
								    <asp:DropDownList runat="server" ID="txtAgentName" class="form-control select2" 
                                        onselectedindexchanged="txtAgentName_SelectedIndexChanged" AutoPostBack="true">
							 	    </asp:DropDownList>
							 	    <asp:RequiredFieldValidator ControlToValidate="txtAgentName" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
								</div>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Agent Mobile No</label>
									<asp:TextBox runat="server" ID="txtAgent_MobileNo" class="form-control"></asp:TextBox>
									<%--<asp:RequiredFieldValidator ControlToValidate="txtAgent_MobileNo" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>  --%>                                     
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtAgent_MobileNo" ValidChars="0123456789.+">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Total Expenses</label>
									<asp:Label runat="server" ID="txtTotal_Expenses" class="form-control" BackColor="#F3F3F3" Text="0"></asp:Label>
								</div>
                            </div>    
                        </div>
                       
                         
                      <!-- begin row -->  
                        <div class="row">
                       
                         <div class="col-md-4">
                        
                         </div>
                         
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                      
                 
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"  />
                                         	<asp:Button runat="server" id="btnBack" Text="Back" class="btn btn-warning" 
                                          onclick="btnBack_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->   
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>



