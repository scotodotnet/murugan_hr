﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="ScmReelingProcess.aspx.cs" Inherits="ScmReelingProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Reeling Process</a></li>
            <li class="active">Reeling Process</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Reeling Process</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Reeling Process</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fin. Year</label>
                                                <asp:DropDownList runat="server" ID="ddlfinance" class="form-control select2"
                                                    Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlfinance" TabIndex="1" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" TabIndex="3" AutoComplete="off" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" TabIndex="4" AutoComplete="off" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Token No.</label>
                                                <asp:TextBox runat="server" ID="txtExistingCode" class="form-control" AutoComplete="off" OnTextChanged="txtExistingCode_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <asp:TextBox runat="server" ID="txtEmpName" class="form-control" AutoComplete="off" Enabled="false" Font-Bold="true"></asp:TextBox>

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnProcess" Text="Process" OnClick="btnProcess_Click" ValidationGroup="Validate_Field" TabIndex="5" class="btn btn-success" />
                                                <asp:Button runat="server" ID="btnReport" Text="Report" OnClick="btnReport_Click" ValidationGroup="Validate_Field" TabIndex="6" class="btn btn-primary" />
                                                <asp:Button ID="btnclear" runat="server" Text="Clear" class="btn btn-danger" TabIndex="7"
                                                    OnClick="btnclear_Click" />
                                                <asp:Button runat="server" ID="btnPDF_Report" Text="PDF Report" OnClick="btnPDF_Report_Click" ValidationGroup="Validate_Field" TabIndex="8" class="btn btn-primary" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-2"></div>
                                    </div>
                                    <!-- end row -->
                                    <div class="panel-footer">
                                        <asp:Panel runat="server" ID="Panel2" Visible="false">
                                            <tr id="Tr2" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="gridExcel" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Date</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblprdDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TARGET</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("Target") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ACHIVED</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("Achived") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("ProductionAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div id="Download_loader" style="display: none" />
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnReport"/>
                </Triggers>
            </asp:UpdatePanel>

            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>
    <style>
        .textmode {
            mso-number-format: \@;
        }
    </style>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
</asp:Content>

