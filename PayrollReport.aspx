﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="PayrollReport.aspx.cs" Inherits="PayrollReport" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
 <style type="text/css">
        .CalendarCSS
        {
            background-color:White;
            color:Black;
            border:1px solid #646464;
            font-size:xx-large;
            font-weight: bold;
            margin-bottom: 40px;
            margin-top: 20px;
        }
        </style>
<script type="text/javascript">
        $(document).ready(function () {
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
            $("ul[id*=myid] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li
               
            });
        });
</script>

 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });

                $('.select2').select2();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
	<div id="content" class="content">
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <%--<h4><li class="active">Report</li></h4> --%>
                    </ol>
                    <h1 class="page-header"> Common Report</h1>
                </div>

                                     
        <div id="main-wrapper" class="container">
          <div class="row">
                 <div class="col-md-12">
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                       <ContentTemplate>
                  <div class="col-sm-5">
                         <div class="panel panel-white">
                                <div class="panel panel-primary">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title">Report Type</h3>
                                </div>
                                </div>
                                <div class="panel-body">
                                 
                                    <div>
                                    <div class="form-group col-md-6">
                                                    <label for="exampleInputName">REPORT NAME</label>
                                                      <asp:DropDownList ID="ddlRptName" runat="server" 
                                                        class="form-control col-md-6 select2" AutoPostBack="true" 
                                                        onselectedindexchanged="ddlRptName_SelectedIndexChanged">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="ALL"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="IF"></asp:ListItem>
                                                        
                                                            </asp:DropDownList>
                                                            
                                                            
                                                            </div>
                                                     <!-- <asp:ListItem Value="6" Text="EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES"></asp:ListItem>
                                                            <asp:ListItem Value="7" Text="DAY ATTENDANCE SUMMARY"></asp:ListItem>
                                                            <asp:ListItem Value="8" Text="EMPLOYEE MASTER"></asp:ListItem>
                                                            <asp:ListItem Value="9" Text="EMPLOYEE FULL PROFILE"></asp:ListItem>-->
                                                            
                                               
                                    <div>
                                    
                                        <asp:ListBox ID="ListRptName" runat="server" Width="360px" Height="290px" 
                                            Font-Bold="True" Font-Names="Times New Roman" Font-Size="Medium" 
                                            onselectedindexchanged="ListRptName_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem></asp:ListItem>
                                                </asp:ListBox>
                                                
                                    </div>
                                 
                                    </div>
                                    
                                </div>
                            </div>
                         </div> <!-- col-sm-6 -->  
                         
                                <div class="col-sm-5">
                                <div class="panel panel-white">
                                <div class="panel panel-primary">
                                <div class="panel-heading clearfix">
                                <h3 class="panel-title">Report Fields</h3>
                                </div>
                                </div>
                                <div class="panel-body">
                                <div>
                                    <asp:Label ID="RptLabel" runat="server" Font-Bold="True" Height="100%" 
                                        Width="100%" align="center">Option</asp:Label>
                                </div>
                                <div class="form-group row">
						        </div>
                                <div class="col-md-12">
                                            <div class="row">
										     
                                               <div class="form-group col-md-6">
                                                    <label for="exampleInputName">Department</label>
                                                            <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control select2" Enabled="false">
                                                            </asp:DropDownList>
                                                                <asp:DropDownList ID="ddlDivision" runat="server" Visible="false" 
                                                       class="form-control select2">
                                                        
                                                             
                                                            </asp:DropDownList>
                                               </div>
                                                 <div class="form-group col-md-6">
                                                    <label for="exampleInputName">MachineID</label>
                                                            <asp:DropDownList ID="ddlMachineID" runat="server" class="form-control select2" Enabled="false">
                                                            </asp:DropDownList>
                                               </div>
                                               </div>
                                               <div class="row">
                                               <div class="form-group col-md-6">
                                               <div class="form-group">
								                 <label>Category</label>
								                 <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                                        style="width:100%;" onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								                 <asp:ListItem Value="0">-Select-</asp:ListItem>
								                 <asp:ListItem Value="1">Staff</asp:ListItem>
								                 <asp:ListItem Value="2">Labour</asp:ListItem>
								                </asp:DropDownList>
								                </div>
                                               </div>
                                                <!-- begin col-4 -->
                                                <div class="col-md-6">
							                       <div class="form-group">
								                    <label>Employee Type</label>
								                     <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" AutoPostBack="true"
                                                            style="width:100%;" onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
                                                         <asp:ListItem Selected="True" Text="-Select-" Value="-Select-"></asp:ListItem>
								                     </asp:DropDownList>
								                    </div>
                                                 </div>
                                               <!-- end col-4 -->
                                               </div>
                                               <div class="row">
                                               <div class="form-group col-md-6">
                                                   <label>Fin. Year</label>
								                     <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2" AutoPostBack="true"
                                                            style="width:100%;" onselectedindexchanged="ddlFinYear_SelectedIndexChanged">
							 	                     </asp:DropDownList>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label>Month</label>
								                     <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" AutoPostBack="true"
                                                            style="width:100%;" onselectedindexchanged="ddlMonth_SelectedIndexChanged">
								                     <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								                     <asp:ListItem Value="January">January</asp:ListItem>
								                     <asp:ListItem Value="February">February</asp:ListItem>
								                     <asp:ListItem Value="March">March</asp:ListItem>
								                     <asp:ListItem Value="April">April</asp:ListItem>
								                     <asp:ListItem Value="May">May</asp:ListItem>
								                     <asp:ListItem Value="June">June</asp:ListItem>
								                     <asp:ListItem Value="July">July</asp:ListItem>
								                     <asp:ListItem Value="August">August</asp:ListItem>
								                     <asp:ListItem Value="September">September</asp:ListItem>
								                     <asp:ListItem Value="October">October</asp:ListItem>
								                     <asp:ListItem Value="November">November</asp:ListItem>
								                     <asp:ListItem Value="December">December</asp:ListItem>
							 	                     </asp:DropDownList>
                                               </div>
                                                 
                                               </div>
                                             
                                               <div class="row">
                                                 <div class="form-group col-md-6">
												 <label for="exampleInputName" >From Date</label>
                                                    <%--<asp:TextBox ID="txtFrmdate" class="form-control col-md-6" runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox runat="server" ID="txtFrmdate" class="form-control datepicker col-md-6" AutoComplete="off" placeholder="dd/MM/YYYY" ></asp:TextBox>
                                               <%--<cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtFrmdate">
                                                 </cc1:CalendarExtender>--%>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtFrmdate" ValidChars="0123456789/">
                                                 </cc1:FilteredTextBoxExtender>
												
												</div>
												    <div class="form-group col-md-6">
                                                    <label for="exampleInputName" >To Date</label>
                                                    <%--<asp:TextBox ID="txtTodate" class="form-control col-md-6" runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox runat="server" ID="txtTodate" class="form-control datepicker col-md-6" AutoComplete="off" placeholder="dd/MM/YYYY" ></asp:TextBox>
                                                <%--<cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtTodate">
                                                 </cc1:CalendarExtender>--%>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="txtTodate" ValidChars="0123456789/">
                                                 </cc1:FilteredTextBoxExtender>
												</div>
												 
												 </div>
										</div>
										
										<!-- Button start -->
						<div class="form-group row">
						<div class="txtcenter">
			        <%--<asp:Button ID="btn30days" class="btn btn-info"  runat="server" Text="30 days" />
                    <asp:Button ID="btn60days" class="btn btn-info" runat="server" Text="60 days" />--%>
                   
                    </div>
                    </div>
                 <div class="txtcenter">
					<div class="form-group row">
					     <asp:Button ID="btnExcel" class="btn btn-info" runat="server" Text="Excel" onclick="btnExcel_Click" 
                                 />
						<asp:Button ID="btnReport" class="btn btn-success" runat="server" Text="Report" onclick="btnReport_Click" 
                             />
                        <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear"/>
                       
                    </div>
                    </div>
                    <!-- Button End -->
                    
                                </div>
                                
                                </div>
                                </div>       
                      </ContentTemplate>
                                    </asp:UpdatePanel>
                       
                    </div>   
                 </div>   
              </div>      
                                     
        </div>
  </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

