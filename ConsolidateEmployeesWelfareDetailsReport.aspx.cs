﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class ConsEmpWelDetReport : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL = "";
    string unit = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    DataTable DT = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Scheduled Leave Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Date = Request.QueryString["Date"].ToString();
            unit = Request.QueryString["Unit"].ToString();

            SSQL = "Select A.DeptName,sum(A.TotalPer)[TotalPer],sum(A.DaySalary)[DaySalary],sum(A.NoOfPresent)[NoOfPresent],";
            SSQL = SSQL + " Sum(A.EarnedSal) [EarnedSal] from (";
            SSQL = SSQL + " Select EMP.DeptName,count(Emp.ExistingCode)TotalPer, ";
            SSQL = SSQL + " Case when EMP.CatName='STAFF' then cast(sum(Emp.BaseSalary/26) as decimal(18,2))";
            SSQL = SSQL + " Else cast(sum(Emp.BaseSalary) as decimal(18,2)) end [DaySalary],0[NoOfPresent],0[EarnedSal] ";
            SSQL = SSQL + " From Employee_Mst EMP where (EMP.IsActive = 'YES' or Convert(date,EMP.DOR,103)>=Convert(date,'" + Date+ "',103))";
            if (unit != "ALL")
            {
                SSQL = SSQL + " and EMP.LocCode = '" + unit + "'";
            }

            SSQL = SSQL + " Group By EMP.DeptName,EMP.CatName,Emp.BaseSalary ";

            SSQL = SSQL + " Union All ";

            SSQL = SSQL + " Select LTD.DeptName,0 [TotalPer],0[DaySalary],count(LTD.ExistingCode)NoOfPresent,";
            SSQL = SSQL + " Case when EMP.CatName='STAFF' then cast(sum(Emp.BaseSalary/26) as decimal(18,2)) Else ";
            SSQL = SSQL + " Cast(sum(Emp.BaseSalary) as decimal(18,2)) end [EarnedSal] ";
            SSQL = SSQL + " From LogTime_Days LTD inner join Employee_Mst Emp on Emp.ExistingCode=LTD.ExistingCode and EMP.IsActive='Yes'";
            SSQL = SSQL + " Where Convert(Datetime,LTD.Attn_Date,105)=CONVERT(DateTime,'" + Date + "',105) and LTD.CompCode='"+SessionCcode+ "' and LTD.LocCode='" + SessionLcode + "'";
            if (unit != "ALL")
            {
                SSQL = SSQL + " and LTD.LocCode = '" + unit + "'";
            }
            SSQL = SSQL + " Group By LTD.DeptName,EMP.CatName)A Group By A.DeptName ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                DataTable dt1 = new DataTable();
                SSQL = "Select * from Company_Mst ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();

                ds.Tables.Add(DT);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("New_HR_Report/ConsEmpWelDetReport.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
                report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
