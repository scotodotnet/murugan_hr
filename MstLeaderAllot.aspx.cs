﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;

public partial class MstLeaderAllot : System.Web.UI.Page
{

    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    int intK;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;
    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable(); // mLocalDS
    DataTable DataCell = new DataTable();
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();

            con = new SqlConnection(constr);
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report";
                Load_Leader();
            }
            Load_Data();
        }
    }

    private void Load_Leader()
    {
        DataTable dt = new DataTable();
        string SQL = "select AgentID,AgentName from AgentMst ";
        SQL = SQL + " where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(SQL);
        ddlLeaderName.DataSource = dt;
        ddlLeaderName.DataTextField = "AgentName";
        ddlLeaderName.DataValueField = "AgentID";
        ddlLeaderName.DataBind();
        ddlLeaderName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();

        string SQL = "Select * from LeaderAllotment_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

        dt = objdata.RptEmployeeMultipleDetails(SQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        string message = "Success";
        DataTable AgentName_check = new DataTable();

        DataTable DT = new DataTable();
        DataTable DT_Dept = new DataTable();
        string Designation = "";
        string DeptName = "";
        string AgentName = "";
        if (ddlLeaderName.SelectedValue != "")
        {

            if (ddlLeaderName.SelectedValue != "")
            {
                query = "select AgentName from AgentMst where AgentID='" + ddlLeaderName.SelectedValue + "'";
                query = query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

            }
            AgentName_check = objdata.RptEmployeeMultipleDetails(query);

            if (AgentName_check.Rows.Count != 0)
            {
                AgentName = AgentName_check.Rows[0]["AgentName"].ToString();
            }

            query = "Delete from LeaderAllotment_Mst where AgentID='" + ddlLeaderName.SelectedValue + "'";
            query = query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //Insert into LeaderAllotment_Mst
            query = "insert into LeaderAllotment_Mst(DesignNo,DesignName,DeptCode,DeptName,AgentID,AgentName,";
            query = query + "Total,CompCode,LocCode)values";
            query = query + "('','','','','" + ddlLeaderName.SelectedValue + "',";
            query = query + "'" + ddlLeaderName.SelectedItem.Text + "','" + txtTotal.Text + "',";
            query = query + "'" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Leader Allotted!!!');", true);
            btnCancel_Click(sender, e);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Leader Name')", true);
        }

        //db.Employees.Add(emp);
        //db.SaveChanges();


    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnSave.Text = "Save";
        ddlLeaderName.ClearSelection();
        txtTotal.Text = "0";
        Load_Data();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();

        string SQL = "Select * from LeaderAllotment_Mst where AgentID='" + e.CommandArgument.ToString() + "'";
        SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode.ToString() + "' ";   
        dt = objdata.RptEmployeeMultipleDetails(SQL);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlLeaderName.SelectedValue = dt.Rows[i]["AgentID"].ToString();
            txtTotal.Text = dt.Rows[i]["Total"].ToString();
            btnSave.Text = "Update";
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SQL;
        SQL = "Delete from LeaderAllotment_Mst where AgentID='" + e.CommandArgument.ToString() + "'";
        SQL = SQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode.ToString() + "' ";
        objdata.RptEmployeeMultipleDetails(SQL);
        Load_Data();
    }
}