﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;

public partial class RptReeling : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string SessionUserID;
    string SessionUserName;
    string Query = "";

    string SSQL = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";
    bool ErrFlg = false;

    DataTable AutoDT = new DataTable();

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    System.Web.UI.WebControls.DataGrid grid =
                         new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserID = ss;
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();

        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }


    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }

    protected void btnRpt_Click(object sender, EventArgs e)
    {
        string ReportName = "";
        string FinYearVal = "";
        string FromDate = "";
        string ToDate = "";
        string MonthName = "";
        bool ErrFlg = false;
        if (rdbPayslipFormat.SelectedValue == "2")
        {
            ReportName = "PFCHECKLIST";
            FinYearVal = ddlfinance.SelectedValue;
            if (ddlMonths.SelectedItem.Text == "-Select-")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month')", true);
                return;

            }
            else
            {
                MonthName = ddlMonths.SelectedItem.Text;
            }
            if (txtFrom.Text == "" || txtTo.Text == "")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter FromDate and Todate properly')", true);
                return;

            }
            else
            {
                FromDate = txtFrom.Text;
                ToDate = txtTo.Text;
            }

            if (!ErrFlg)
            {
                ResponseHelper.Redirect("ViewReportNew.aspx?FinYearVal=" + FinYearVal + "&Months=" + MonthName + "&fromdate=" + FromDate + "&ToDate=" + ToDate + "&RptName=" + ReportName, "_blank", "");
            }
        }

        if (rdbPayslipFormat.SelectedValue == "3")
        {
            ReportName = "WAGESLIST";
            FinYearVal = ddlfinance.SelectedValue;
            if (ddlMonths.SelectedItem.Text == "-Select-")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month')", true);
                return;

            }
            else
            {
                MonthName = ddlMonths.SelectedItem.Text;
            }
            if (txtFrom.Text == "" || txtTo.Text == "")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter FromDate and Todate properly')", true);
                return;

            }
            else
            {
                FromDate = txtFrom.Text;
                ToDate = txtTo.Text;
            }

            if (!ErrFlg)
            {
                ResponseHelper.Redirect("ViewReportNew.aspx?FinYearVal=" + FinYearVal + "&Months=" + MonthName + "&fromdate=" + FromDate + "&ToDate=" + ToDate + "&RptName=" + ReportName, "_blank", "");
            }
        }

        if (rdbPayslipFormat.SelectedValue == "4")
        {
            ReportName = "ABSTRACTWAGESLIST";
            FinYearVal = ddlfinance.SelectedValue;
            if (ddlMonths.SelectedItem.Text == "-Select-")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month')", true);
                return;

            }
            else
            {
                MonthName = ddlMonths.SelectedItem.Text;
            }
            if (txtFrom.Text == "" || txtTo.Text == "")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter FromDate and Todate properly')", true);
                return;

            }
            else
            {
                FromDate = txtFrom.Text;
                ToDate = txtTo.Text;
            }

            if (!ErrFlg)
            {
                ResponseHelper.Redirect("ViewReportNew.aspx?FinYearVal=" + FinYearVal + "&Months=" + MonthName + "&fromdate=" + FromDate + "&ToDate=" + ToDate + "&RptName=" + ReportName, "_blank", "");
            }
        }
        if (rdbPayslipFormat.SelectedValue == "6")
        {
            ReportName = "AQUITTANCE LIST";
            FinYearVal = ddlfinance.SelectedValue;
            if (ddlMonths.SelectedItem.Text == "-Select-")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month')", true);
                return;

            }
            else
            {
                MonthName = ddlMonths.SelectedItem.Text;
            }
            if (txtFrom.Text == "" || txtTo.Text == "")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter FromDate and Todate properly')", true);
                return;

            }
            else
            {
                FromDate = txtFrom.Text;
                ToDate = txtTo.Text;
            }

            if (!ErrFlg)
            {
                ResponseHelper.Redirect("ViewReportNew.aspx?FinYearVal=" + FinYearVal + "&Months=" + MonthName + "&fromdate=" + FromDate + "&ToDate=" + ToDate + "&RptName=" + ReportName, "_blank", "");
            }
        }
    }
    protected void btnBonusExcelView_Click(object sender, EventArgs e)
    {
        string ReportName = "";
        string FinYearVal = "";
        string FromDate = "";
        string ToDate = "";
        string MonthName = "";
        bool ErrFlg = false;
        if (rdbPayslipFormat.SelectedValue == "5")
        {
            ReportName = "BANK PAYMENT LIST";
            FinYearVal = ddlfinance.SelectedValue;
            if (ddlMonths.SelectedItem.Text == "-Select-")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Month')", true);
                return;
            }
            else
            {
                MonthName = ddlMonths.SelectedItem.Text;
            }
            if (txtFrom.Text == "" || txtTo.Text == "")
            {
                ErrFlg = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter FromDate and Todate properly')", true);
                return;
            }
            else
            {
                FromDate = txtFrom.Text;
                ToDate = txtTo.Text;
            }

            if (!ErrFlg)
            {
                // ResponseHelper.Redirect("ViewReportNew.aspx?FinYearVal=" + FinYearVal + "&Months=" + MonthName + "&fromdate=" + FromDate + "&ToDate=" + ToDate + "&RptName=" + ReportName, "_blank", "");
                SSQL = "";
                SSQL = "Select ROW_NUMBER() Over (Order by EM.Existingcode) As [S.No],EM.Existingcode as Code,EM.FirstName as EmpName,'&nbsp;&nbsp;'+EM.AccountNo as AccountNo,isnull(sum(cast(RP.PayTotal as decimal(18,0))),'0') as [ToTal Amount] from Employee_Mst EM";
                SSQL = SSQL + " Inner join [" + SessionEpay + "]..ReelingWagesProcess RP on RP.EmpNo=EM.MachineID and RP.Ccode COLLATE DATABASE_DEFAULT=EM.CompCode COLLATE DATABASE_DEFAULT ";
                SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and EM.DeptCode='41' and RP.Ccode='"+SessionCcode+"' and RP.Lcode='"+SessionLcode+"'";
                SSQL = SSQL + " and  Convert(datetime,RP.FromDate,103)>=Convert(datetime,'" + FromDate + "',103) and Convert(datetime,RP.ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";
                SSQL = SSQL + " and RP.Month='" + MonthName + "' and RP.FinYearVal='" + FinYearVal + "'";
                SSQL = SSQL + " group by EM.Existingcode,EM.FirstName,EM.AccountNo order by EM.Existingcode asc";
                DataTable AutoDt = new DataTable();
                AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (AutoDT != null && AutoDT.Rows.Count > 0)
                {
                    string CmpName = "";
                    string Cmpaddress = "";
                    DataTable dt_Cat = new DataTable();

                    SSQL = "";
                    SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_Cat.Rows.Count > 0)
                    {
                        CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                    }


                    grid.DataSource = AutoDT;
                    grid.DataBind();
                    string attachment = "attachment;filename=Bank_List.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    grid.RenderControl(htextw);
                    Response.Write("<table>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='5'>");
                    Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
                    Response.Write("--");
                    Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='5'>");
                    Response.Write("<a style=\"font-weight:bold\">LIST FOR THE PERIOD FROM - " + FromDate + " TO " + ToDate + "</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    //Response.Write("<td colspan='10'>");
                    //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                    //Response.Write("&nbsp;&nbsp;&nbsp;");
                    //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.Write(stw.ToString());
                    Response.Write("<table border=1>");
                    Response.Write("<tr>");
                    Response.Write("<td colspan=4 align='center' style=\"font-weight:bold\"> TOTAL");
                    Response.Write("</td> ");
                    Response.Write("<td> =sum(E4:E" + Convert.ToInt32(AutoDT.Rows.Count + 3) + ")");
                    Response.Write("</td>");

                    Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.End();
                    Response.Clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found');", true);
                }

            }
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
}