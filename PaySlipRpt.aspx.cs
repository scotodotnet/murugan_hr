﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class PaySlipRpt : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;
    string RptName = "";
    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string FinYear = "";
    string Month = "";
    string Fromdate = "";
    string toDate = "";
    string EmpType = "";

    string SessionEpay = "";

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Payslip";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();

            EmpType = Request.QueryString["EmpType"].ToString();
            Fromdate= Request.QueryString["FromDate"].ToString();
            toDate = Request.QueryString["ToDate"].ToString();
            FinYear = Request.QueryString["FinYearVal"].ToString();
            Month = Request.QueryString["Month"].ToString();
            RptName = Request.QueryString["RptName"].ToString();

            if(RptName== "Account_Emp")
            {
                GetReport();
            }
            if (RptName == "Non_Account_Emp")
            {
                GetReport();
            }
            if (RptName == "Supplementary_Report_Account_Emp" || RptName == "Supplementary_Report_Non_Account_Emp")
            {
                GetReport();
            }
            if (RptName == "Employee_Payslip_Cover_Report_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Non_Account_Emp")
            {
                GetReport();
            }
            if (RptName == "Wages_Abstract_Account_Emp")
            {
                Wages_Abstract();
            }
            if (RptName == "Wages_Abstract_Non_Account_Emp")
            {
                Wages_Abstract();
            }
            if (RptName == "IF_PAYSLIP")
            {
                IF_Payslip();
            }
        }
    }
    private void IF_Payslip()
    {
        DataTable AutoDT = new DataTable();

        AutoDT.Columns.Add("EmpNo");
        AutoDT.Columns.Add("BioID");
        AutoDT.Columns.Add("Name");
        AutoDT.Columns.Add("DeptName");
        AutoDT.Columns.Add("AccNo");
        AutoDT.Columns.Add("ActualSal", typeof(decimal));
        AutoDT.Columns.Add("WorkDays", typeof(decimal));
        AutoDT.Columns.Add("WeekPresentDays", typeof(decimal));
        AutoDT.Columns.Add("LOPDays", typeof(decimal));
        AutoDT.Columns.Add("OTHrs", typeof(decimal));


        AutoDT.Columns.Add("Basic", typeof(decimal));
        AutoDT.Columns.Add("DA", typeof(decimal));
        AutoDT.Columns.Add("HRA", typeof(decimal));
        AutoDT.Columns.Add("MediAllow", typeof(decimal));
        AutoDT.Columns.Add("EduAllow", typeof(decimal));
        AutoDT.Columns.Add("WAAllow", typeof(decimal));
        AutoDT.Columns.Add("TA", typeof(decimal));
        AutoDT.Columns.Add("TotalSalary", typeof(decimal));


        AutoDT.Columns.Add("WpAmt", typeof(decimal));
        AutoDT.Columns.Add("OTAmt", typeof(decimal));
        //AutoDT.Columns.Add("IncAllow");// as DaysAllow

        AutoDT.Columns.Add("DaysAllow", typeof(decimal));
        AutoDT.Columns.Add("MasthiriIncentive", typeof(decimal));
        AutoDT.Columns.Add("SDAllow", typeof(decimal));
        AutoDT.Columns.Add("Earnings2", typeof(decimal));

        //AutoDT.Columns.Add("SplAllow");// as masthiri incentive
        AutoDT.Columns.Add("StAl1", typeof(decimal));//SD Allowance
        AutoDT.Columns.Add("StAl2", typeof(decimal));//other Earnings
        AutoDT.Columns.Add("StAl3", typeof(decimal));
        AutoDT.Columns.Add("TotalEarnings", typeof(decimal));

        AutoDT.Columns.Add("NetSalary", typeof(decimal));//Total Earnings


        AutoDT.Columns.Add("PF", typeof(decimal));
        AutoDT.Columns.Add("ESI", typeof(decimal));
        AutoDT.Columns.Add("TDS", typeof(decimal));//Deduc 3
        AutoDT.Columns.Add("LWF", typeof(decimal));//Deduc 4
        AutoDT.Columns.Add("Mess", typeof(decimal));//Deduc 2
        AutoDT.Columns.Add("Advance", typeof(decimal));
        AutoDT.Columns.Add("Rent", typeof(decimal));//Deduc 1
        AutoDT.Columns.Add("SweepeR", typeof(decimal));
        AutoDT.Columns.Add("Fine", typeof(decimal));
        AutoDT.Columns.Add("Union", typeof(decimal));
        AutoDT.Columns.Add("Store", typeof(decimal));//Deduc other 1
        AutoDT.Columns.Add("Cable", typeof(decimal));//Deduc other 2
        AutoDT.Columns.Add("TotalDeduc", typeof(decimal));
        AutoDT.Columns.Add("PaidSalary", typeof(decimal));


        AutoDT.Columns.Add("IncSal", typeof(decimal));
        AutoDT.Columns.Add("PaidDays", typeof(decimal));
        AutoDT.Columns.Add("NH", typeof(decimal));
        AutoDT.Columns.Add("Others1", typeof(decimal));
        AutoDT.Columns.Add("Others2", typeof(decimal));
        AutoDT.Columns.Add("PreWP", typeof(decimal));

        AutoDT.Columns.Add("LastName");
        AutoDT.Columns.Add("DOB");
        AutoDT.Columns.Add("UAN");
        AutoDT.Columns.Add("ESINo");
        AutoDT.Columns.Add("Doj");
        AutoDT.Columns.Add("PfNo");




        SSQL = "";
        SSQL = "Select EM.ExistingCode,EM.MachineID,EM.DeptName,EM.FirstName,EM.AccountNo,EM.BaseSalary,isnull(EM.Salary2,'0') as Salary2,isnull(SD.NetPay,'0') as PaidAmt,'0' as PerWP,isnull(SD.BasicandDA,'0') as BasicandDA,isnull(SD.ProvidentFund,'0') as ProvidentFund,isnull(SD.ESI,'0') as ESI,isnull(SD.Conveyance,'0') as Conveyance,isnull(SD.GrossEarnings,'0') as GrossEarnings,isnull(SD.PerIncentive,'0') as PerIncentive,isnull(SD.allowances1,'0') as allowances1,";
        SSQL = SSQL + " isnull(SD.allowances2,'0') as allowances2,isnull(SD.allowances3,'0') as allowances3,isnull(SD.allowances4,'0') as allowances4,isnull(SD.allowances5,'0') as allowances5,'0' as Sweeper,'0' as Fine,isnull(SD.Unioncg,'0') as Unioncg,isnull(SD.Deduction1,'0') as Deduction1,isnull(SD.Deduction4,'0') as Deduction4,isnull(SD.Deduction2,'0') as Deduction2,isnull(SD.Deduction3,'0') as Deduction3,isnull(SD.Deduction5,'0') as Deduction5,";
        SSQL = SSQL + " isnull(SD.Messdeduction,'0') as Messdeduction,isnull(SD.Losspay,'0') as Losspay,isnull(SD.TotalDeductions,'0') as TotalDeductions,isnull(SD.NetPay,'0') as NetPay,isnull(SD.Month,'0') as Month,isnull(SD.Year,'0') as Year,isnull(SD.LOPDays,'0') as LOPDays,isnull(SD.OTHoursNew,'0') as OTHoursNew,isnull(SD.OTHoursAmtNew,'0') as OTHoursAmtNew,isnull(SD.NFh,'0') as NFh,isnull(SD.WorkedDays,'0') as WorkedDays,isnull(SD.WH_Work_Days,'0') as WH_Work_Days,isnull(SD.BasicAndDANew,'0') as BasicAndDANew,";
        SSQL = SSQL + " isnull(SD.BasicHRA,'0') as BasicHRA,isnull(SD.ThreesidedAmt,'0') as ThreesidedAmt,isnull(SD.ConvAllow,'0') as ConvAllow,isnull(SD.EduAllow,'0') as EduAllow,isnull(SD.MediAllow,'0') as MediAllow,isnull(SD.BasicRAI,'0') as BasicRAI,isnull(SD.WashingAllow,'0') as WashingAllow,isnull(SD.RoundOffNetPay,'0') as RoundOffNetPay,isnull(SD.DedOthers1,'0') as DedOthers1,isnull(SD.DedOthers2,'0') as DedOthers2,isnull(SD.Advance,'0') as Advance,isnull(SD.EmployeerESI,'0') as  EmployeerESI,";
        SSQL = SSQL + " EM.LastName,Convert(varchar(20),EM.BirthDate,103) as DOB,EM.UAN,EM.ESINo,Convert(varchar(20),EM.DOJ,103) as DOJ,isNull(EM.PFNo,'0') as PFno";
        SSQL = SSQL + " from Employee_Mst EM Inner join [" + SessionEpay + "]..SalaryDetails SD on SD.EmpNo=EM.MachineID ";
        SSQL = SSQL + "where SD.Month='" + Month + "' and EM.Wages='" + EmpType + "' and SD.FromDate=CONVERT(datetime,'" + Fromdate + "',103) and SD.ToDate=CONVERT(Datetime,'" + toDate + "',103) and SD.FinancialYear='" + FinYear + "' and SD.WorkedDays > '0'";
        if (RptName == "IF_Payslip")
        {
            SSQL = SSQL + " and (EM.AccountNo!='' or EM.AccountNo is not null)";
        }


        SSQL = SSQL + " order by CAST(EM.ExistingCode as int) Asc";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["EmpNo"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["AccNo"] = dt.Rows[i]["AccountNo"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["IncSal"] = dt.Rows[i]["Salary2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ActualSal"] = dt.Rows[i]["BaseSalary"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WorkDays"] = dt.Rows[i]["WorkedDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WeekPresentDays"] = dt.Rows[i]["WH_Work_Days"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["LOPDays"] = dt.Rows[i]["LOPDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PaidDays"] = dt.Rows[i]["WorkedDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["NH"] = dt.Rows[i]["NFh"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Basic"] = dt.Rows[i]["BasicAndDANew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MediAllow"] = dt.Rows[i]["MediAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DA"] = dt.Rows[i]["BasicRAI"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["HRA"] = dt.Rows[i]["BasicHRA"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WAAllow"] = dt.Rows[i]["WashingAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TA"] = dt.Rows[i]["ConvAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["EduAllow"] = dt.Rows[i]["EduAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DaysAllow"] = dt.Rows[i]["allowances1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MasthiriIncentive"] = dt.Rows[i]["allowances2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalSalary"] = dt.Rows[i]["GrossEarnings"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PF"] = dt.Rows[i]["ProvidentFund"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESI"] = dt.Rows[i]["ESI"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TDS"] = dt.Rows[i]["Deduction3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["LWF"] = dt.Rows[i]["Deduction4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Mess"] = dt.Rows[i]["Deduction2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Advance"] = dt.Rows[i]["Advance"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Rent"] = dt.Rows[i]["Deduction1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Cable"] = dt.Rows[i]["Deduction4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SweepeR"] = dt.Rows[i]["SweepeR"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Fine"] = dt.Rows[i]["Fine"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Store"] = "0";
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Union"] = dt.Rows[i]["Unioncg"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Others1"] = dt.Rows[i]["DedOthers1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Others2"] = dt.Rows[i]["DedOthers2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalDeduc"] = dt.Rows[i]["TotalDeductions"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["PreWP"] = dt.Rows[i]["PerWP"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WpAmt"] = dt.Rows[i]["ThreesidedAmt"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OTHrs"] = dt.Rows[i]["OTHoursNew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OTAmt"] = dt.Rows[i]["OTHoursAmtNew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PaidSalary"] = dt.Rows[i]["PaidAmt"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["SDAllow"] = dt.Rows[i]["allowances3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Earnings2"] = dt.Rows[i]["allowances4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["StAl3"] = dt.Rows[i]["allowances5"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["BioID"] = dt.Rows[i]["MachineID"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DeptName"] = dt.Rows[i]["DeptName"].ToString();


                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalEarnings"] = dt.Rows[i]["NetPay"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["LastName"] = dt.Rows[i]["LastName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DOB"] = dt.Rows[i]["DOB"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["UAN"] = dt.Rows[i]["UAN"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESINo"] = dt.Rows[i]["ESINo"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Doj"] = dt.Rows[i]["DOJ"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PfNo"] = dt.Rows[i]["PFno"].ToString();


            }
        }
        DataView dv = AutoDT.DefaultView;
        dv.Sort = "EmpNo Asc";
        AutoDT = dv.ToTable();
        if (AutoDT.Rows.Count > 0)
        {
            DataTable dts = new DataTable();
            SSQL = "Select * from [" + SessionEpay + "]..AdminRights where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            dts = objdata.RptEmployeeMultipleDetails(SSQL);
            ds.Tables.Add(AutoDT);
            //if (RptName == "IF_Payslip")
            //{
            report.Load(Server.MapPath("Payslip_IF/IF_Payslip.rpt"));
            // }

            //else
            //{
            //    report.Load(Server.MapPath("Payslip/PaySlipRpt.rpt"));
            //}
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dts.Rows[0]["Cname"].ToString() + "'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'" + dts.Rows[0]["LCode"].ToString() + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Fromdate.ToString() + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + toDate.ToString() + "'";
            report.DataDefinition.FormulaFields["Wages"].Text = "'" + EmpType.ToString() + "'";

            report.DataDefinition.FormulaFields["Month_Str"].Text = "'" + Month.ToString() + "'";

            //if (RptName == "Employee_Payslip_Cover_Report_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Non_Account_Emp")
            //{
            //    report.DataDefinition.FormulaFields["Month_Str"].Text = "'" + Month.ToString() + "'";
            //}
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }
    private void Wages_Abstract()
    {
        DataTable AutoDT = new DataTable();

        AutoDT.Columns.Add("EmpNo");
        AutoDT.Columns.Add("AccNo");
        AutoDT.Columns.Add("Name");
        AutoDT.Columns.Add("IncSal");
        AutoDT.Columns.Add("ActualSal");
        AutoDT.Columns.Add("WorkDays");
        AutoDT.Columns.Add("WeekPresentDays");
        AutoDT.Columns.Add("LOPDays");
        AutoDT.Columns.Add("PaidDays");
        AutoDT.Columns.Add("NH");
        AutoDT.Columns.Add("Basic");
        AutoDT.Columns.Add("MediAllow");
        AutoDT.Columns.Add("DA");
        AutoDT.Columns.Add("ExeAllow");
        AutoDT.Columns.Add("HRA");
        AutoDT.Columns.Add("WAAllow");
        AutoDT.Columns.Add("TA");
        AutoDT.Columns.Add("EduAllow");
        AutoDT.Columns.Add("IncAllow");
        AutoDT.Columns.Add("SplAllow");
        AutoDT.Columns.Add("TotalSalary");
        AutoDT.Columns.Add("PF");
        AutoDT.Columns.Add("ESI");
        AutoDT.Columns.Add("TDS");
        AutoDT.Columns.Add("LWF");
        AutoDT.Columns.Add("Mess");
        AutoDT.Columns.Add("Advance");
        AutoDT.Columns.Add("Rent");
        AutoDT.Columns.Add("Cable");
        AutoDT.Columns.Add("SweepeR");
        AutoDT.Columns.Add("Fine");
        AutoDT.Columns.Add("Store");
        AutoDT.Columns.Add("Union");
        AutoDT.Columns.Add("OtherDeduc");
        AutoDT.Columns.Add("OtherDeduc2");
        AutoDT.Columns.Add("TotalDeduc");
        AutoDT.Columns.Add("NetSalary");
        AutoDT.Columns.Add("PreWP");
        AutoDT.Columns.Add("WpAmt");
        AutoDT.Columns.Add("OTHrs");
        AutoDT.Columns.Add("OTAmt");
        AutoDT.Columns.Add("PaidSalary");

        AutoDT.Columns.Add("Days_Allow", typeof(decimal));
        AutoDT.Columns.Add("SDAllow", typeof(decimal));
        AutoDT.Columns.Add("MasthiriIncentive", typeof(decimal));
        

        SSQL = "";
        SSQL = "Select isnull(Sum(SD.BasicandDA),'0') as BasicandDA,isnull(sum(SD.ProvidentFund),'0') as ProvidentFund,isnull(sum(SD.ESI),'0') as ESI,isnull(sum(SD.Conveyance),'0') as Conveyance,isnull(sum(SD.GrossEarnings),'0') as GrossEarnings,isnull(sum(SD.PerIncentive),'0') as PerIncentive,isnull(sum(SD.allowances1),'0') as allowances1,";
        SSQL = SSQL + "isnull(sum(SD.allowances2),'0') as allowances2,isnull(sum(SD.allowances3),'0') as allowances3,isnull(sum(SD.allowances4),'0') as allowances4,isnull(sum(SD.allowances5),'0') as allowances5,isnull(sum(SD.Deduction1),'0') as Deduction1,isnull(sum(SD.Deduction4),'0') as Deduction4,isnull(sum(SD.Deduction2),'0') as Deduction2,";
        SSQL = SSQL + "isnull(sum(SD.Deduction3),'0') as Deduction3,isnull(sum(SD.Deduction5),'0') as Deduction5,isnull(sum(SD.Messdeduction),'0') as Messdeduction,isnull(sum(SD.Losspay),'0') as Losspay,isnull(sum(SD.TotalDeductions),'0') as TotalDeductions,isnull(sum(SD.NetPay),'0') as NetPay,isnull(Sum(SD.OTHoursNew),'0') as OTHoursNew,";
        SSQL = SSQL + "isnull(Sum(SD.OTHoursAmtNew),'0') as OTHoursAmtNew,isnull(Sum(SD.NFh),'0') as NFh,isnull(Sum(SD.WorkedDays),'0') as WorkedDays,isnull(Sum(SD.WH_Work_Days),'0') as WH_Work_Days,isnull(Sum(SD.BasicAndDANew),'0') as BasicAndDANew,isnull(Sum(SD.BasicHRA),'0') as BasicHRA,isnull(Sum(SD.ThreesidedAmt),'0') as ThreesidedAmt,";
        SSQL = SSQL + "isnull(Sum(SD.ConvAllow),'0') as ConvAllow,isnull(Sum(SD.EduAllow),'0') as EduAllow,isnull(Sum(SD.MediAllow),'0') as MediAllow,isnull(Sum(SD.BasicRAI),'0') as BasicRAI,isnull(Sum(SD.WashingAllow),'0') as WashingAllow,isnull(Sum(SD.RoundOffNetPay),'0') as RoundOffNetPay,isnull(Sum(SD.DedOthers1),'0') as DedOthers1,";
        SSQL = SSQL + "isnull(Sum(SD.DedOthers2),'0') as DedOthers2,isnull(Sum(SD.Advance),'0') as Advance,isnull(Sum(SD.EmployeerESI),'0') as EmployeerESI,isnull(sum(SD.Sweeper),'0') as Sweeper,isnull(SUM(SD.Unioncg),'0') as Unioncg,isnull(SUM(SD.Fine),'0') as Fine from Employee_Mst EM Inner join [" + SessionEpay + "]..SalaryDetails SD on SD.EmpNo=EM.MachineID ";
        SSQL = SSQL + "where SD.Month='" + Month + "' and EM.Wages='" + EmpType + "' and SD.FromDate>=CONVERT(datetime,'" + Fromdate + "',103) and SD.ToDate<=CONVERT(Datetime,'" + toDate + "',103) and SD.FinancialYear='" + FinYear + "'";
        if (RptName == "Wages_Abstract_Account_Emp")
        {
            SSQL = SSQL + " and (EM.AccountNo!='' or EM.AccountNo is not null)";

        }
        if (RptName == "Wages_Abstract_Non_Account_Emp")
        {
            SSQL = SSQL + " and (EM.AccountNo='' or EM.AccountNo is null)";
        }
        //SSQL = SSQL + " group by EM.MachineID";
        //SSQL = SSQL + " order by CAST(EM.MachineID as int) asc";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();
               
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Basic"] = dt.Rows[i]["BasicAndDANew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MediAllow"] = dt.Rows[i]["MediAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DA"] = dt.Rows[i]["BasicRAI"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ExeAllow"] = "0".ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["HRA"] = dt.Rows[i]["BasicHRA"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WAAllow"] = dt.Rows[i]["WashingAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TA"] = dt.Rows[i]["ConvAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["EduAllow"] = dt.Rows[i]["EduAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["IncAllow"] = dt.Rows[i]["PerIncentive"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SplAllow"] = dt.Rows[i]["allowances4"].ToString(); ;
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalSalary"] = dt.Rows[i]["GrossEarnings"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PF"] = dt.Rows[i]["ProvidentFund"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESI"] = dt.Rows[i]["ESI"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TDS"] = dt.Rows[i]["Deduction3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["LWF"] = dt.Rows[i]["Deduction4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Mess"] = dt.Rows[i]["Deduction2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Advance"] = dt.Rows[i]["Advance"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Rent"] = dt.Rows[i]["Deduction1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Cable"] = dt.Rows[i]["Deduction5"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SweepeR"] = dt.Rows[i]["Sweeper"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Fine"] = dt.Rows[i]["Fine"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Store"] = "0".ToString(); 
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Union"] = dt.Rows[i]["Unioncg"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OtherDeduc"] = dt.Rows[i]["DedOthers1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OtherDeduc2"] = dt.Rows[i]["DedOthers2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalDeduc"] = dt.Rows[i]["TotalDeductions"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["NetSalary"] = dt.Rows[i]["NetPay"].ToString();
               // AutoDT.Rows[AutoDT.Rows.Count - 1]["PreWP"] = dt.Rows[i]["PerWP"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WpAmt"] = dt.Rows[i]["ThreesidedAmt"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OTHrs"] = dt.Rows[i]["OTHoursNew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OTAmt"] = dt.Rows[i]["OTHoursAmtNew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PaidSalary"] = dt.Rows[i]["RoundOffNetPay"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["Days_Allow"] = dt.Rows[i]["allowances1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SDAllow"] = dt.Rows[i]["allowances3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MasthiriIncentive"] = dt.Rows[i]["allowances2"].ToString();

            }
        }
        if (AutoDT.Rows.Count > 0)
        {
            DataTable dts = new DataTable();
            SSQL = "Select * from [" + SessionEpay + "]..AdminRights where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            dts = objdata.RptEmployeeMultipleDetails(SSQL);
            ds.Tables.Add(AutoDT);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Wages_Abstract.rpt"));

            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dts.Rows[0]["Cname"].ToString() + "'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'" + dts.Rows[0]["LCode"].ToString() + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Fromdate.ToString() + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + toDate.ToString() + "'";
            report.DataDefinition.FormulaFields["Wages"].Text = "'" + EmpType.ToString() + "'";
            report.DataDefinition.FormulaFields["Month"].Text = "'" + Month.ToString() + "'";
            report.DataDefinition.FormulaFields["Years"].Text = "'" + FinYear.ToString() + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }

    private void GetReport()
    {
        DataTable AutoDT = new DataTable();

        AutoDT.Columns.Add("EmpNo");
        AutoDT.Columns.Add("BioID");
        AutoDT.Columns.Add("Name");
        AutoDT.Columns.Add("DeptName");
        AutoDT.Columns.Add("AccNo");
        AutoDT.Columns.Add("ActualSal", typeof(decimal));
        AutoDT.Columns.Add("WorkDays", typeof(decimal));
        AutoDT.Columns.Add("WeekPresentDays", typeof(decimal));
        AutoDT.Columns.Add("LOPDays", typeof(decimal));
        AutoDT.Columns.Add("OTHrs", typeof(decimal));


        AutoDT.Columns.Add("Basic", typeof(decimal));
        AutoDT.Columns.Add("DA", typeof(decimal));
        AutoDT.Columns.Add("HRA", typeof(decimal));
        AutoDT.Columns.Add("MediAllow", typeof(decimal));
        AutoDT.Columns.Add("EduAllow", typeof(decimal));
        AutoDT.Columns.Add("WAAllow", typeof(decimal));
        AutoDT.Columns.Add("TA", typeof(decimal));
        AutoDT.Columns.Add("TotalSalary", typeof(decimal));


        AutoDT.Columns.Add("WpAmt", typeof(decimal));
        AutoDT.Columns.Add("OTAmt", typeof(decimal));
        //AutoDT.Columns.Add("IncAllow");// as DaysAllow

        AutoDT.Columns.Add("DaysAllow", typeof(decimal));
        AutoDT.Columns.Add("MasthiriIncentive", typeof(decimal));
        AutoDT.Columns.Add("SDAllow", typeof(decimal));
        AutoDT.Columns.Add("Earnings2", typeof(decimal));

        //AutoDT.Columns.Add("SplAllow");// as masthiri incentive
        AutoDT.Columns.Add("StAl1", typeof(decimal));//SD Allowance
        AutoDT.Columns.Add("StAl2", typeof(decimal));//other Earnings
        AutoDT.Columns.Add("StAl3", typeof(decimal));
        AutoDT.Columns.Add("TotalEarnings", typeof(decimal));

        AutoDT.Columns.Add("NetSalary", typeof(decimal));//Total Earnings
        

        AutoDT.Columns.Add("PF", typeof(decimal));
        AutoDT.Columns.Add("ESI", typeof(decimal));
        AutoDT.Columns.Add("TDS", typeof(decimal));//Deduc 3
        AutoDT.Columns.Add("LWF", typeof(decimal));//Deduc 4
        AutoDT.Columns.Add("Mess", typeof(decimal));//Deduc 2
        AutoDT.Columns.Add("Advance", typeof(decimal));
        AutoDT.Columns.Add("Rent", typeof(decimal));//Deduc 1
        AutoDT.Columns.Add("SweepeR", typeof(decimal));
        AutoDT.Columns.Add("Fine", typeof(decimal));
        AutoDT.Columns.Add("Union", typeof(decimal));
        AutoDT.Columns.Add("Store", typeof(decimal));//Deduc other 1
        AutoDT.Columns.Add("Cable", typeof(decimal));//Deduc other 2
        AutoDT.Columns.Add("TotalDeduc", typeof(decimal));
        AutoDT.Columns.Add("PaidSalary", typeof(decimal));


        AutoDT.Columns.Add("IncSal", typeof(decimal));
        AutoDT.Columns.Add("PaidDays", typeof(decimal));
        AutoDT.Columns.Add("NH", typeof(decimal));
        AutoDT.Columns.Add("Others1", typeof(decimal));
        AutoDT.Columns.Add("Others2", typeof(decimal));
        AutoDT.Columns.Add("PreWP", typeof(decimal));

        AutoDT.Columns.Add("LastName");
        AutoDT.Columns.Add("DOB");
        AutoDT.Columns.Add("UAN");
        AutoDT.Columns.Add("ESINo");
       
       
      

        SSQL = "";
        SSQL = "Select EM.ExistingCode,EM.MachineID,EM.DeptName,EM.FirstName,EM.AccountNo,EM.BaseSalary,EM.CatName,isnull(EM.Salary2,'0') as Salary2,isnull(SD.PaidAmt,'0') as PaidAmt,isnull(SD.PerWP,'0') as PerWP,isnull(SD.BasicandDA,'0') as BasicandDA,isnull(SD.ProvidentFund,'0') as ProvidentFund,isnull(SD.ESI,'0') as ESI,isnull(SD.Conveyance,'0') as Conveyance,isnull(SD.GrossEarnings,'0') as GrossEarnings,isnull(SD.PerIncentive,'0') as PerIncentive,isnull(SD.allowances1,'0') as allowances1,";
        SSQL = SSQL + " isnull(SD.allowances2,'0') as allowances2,isnull(SD.allowances3,'0') as allowances3,isnull(SD.allowances4,'0') as allowances4,isnull(SD.allowances5,'0') as allowances5,isnull(SD.Sweeper,'0') as Sweeper,isnull(SD.Fine,'0') as Fine,isnull(SD.Unioncg,'0') as Unioncg,isnull(SD.Deduction1,'0') as Deduction1,isnull(SD.Deduction4,'0') as Deduction4,isnull(SD.Deduction2,'0') as Deduction2,isnull(SD.Deduction3,'0') as Deduction3,isnull(SD.Deduction5,'0') as Deduction5,";
        SSQL = SSQL + " isnull(SD.Messdeduction,'0') as Messdeduction,isnull(SD.Losspay,'0') as Losspay,isnull(SD.TotalDeductions,'0') as TotalDeductions,isnull(SD.NetPay,'0') as NetPay,isnull(SD.Month,'0') as Month,isnull(SD.Year,'0') as Year,isnull(SD.LOPDays,'0') as LOPDays,isnull(SD.OTHoursNew,'0') as OTHoursNew,isnull(SD.OTHoursAmtNew,'0') as OTHoursAmtNew,isnull(SD.NFh,'0') as NFh,isnull(SD.WorkedDays,'0') as WorkedDays,isnull(SD.WH_Work_Days,'0') as WH_Work_Days,isnull(SD.BasicAndDANew,'0') as BasicAndDANew,";
        SSQL = SSQL + " isnull(SD.BasicHRA,'0') as BasicHRA,isnull(SD.ThreesidedAmt,'0') as ThreesidedAmt,isnull(SD.ConvAllow,'0') as ConvAllow,isnull(SD.EduAllow,'0') as EduAllow,isnull(SD.MediAllow,'0') as MediAllow,isnull(SD.BasicRAI,'0') as BasicRAI,isnull(SD.WashingAllow,'0') as WashingAllow,isnull(SD.RoundOffNetPay,'0') as RoundOffNetPay,isnull(SD.DedOthers1,'0') as DedOthers1,isnull(SD.DedOthers2,'0') as DedOthers2,isnull(SD.Advance,'0') as Advance,isnull(SD.EmployeerESI,'0') as  EmployeerESI,";
        SSQL = SSQL + " EM.LastName,Convert(varchar(20),EM.BirthDate,103) as DOB,EM.UAN,EM.ESINo";
        SSQL = SSQL + " from Employee_Mst EM Inner join [" + SessionEpay + "]..SalaryDetails SD on SD.EmpNo=EM.MachineID ";
        SSQL = SSQL + "where SD.Month='" + Month + "' and EM.Wages='" + EmpType + "' and SD.FromDate=CONVERT(datetime,'" + Fromdate + "',103) and SD.ToDate=CONVERT(Datetime,'" + toDate + "',103) and SD.FinancialYear='" + FinYear + "' and SD.WorkedDays > '0'";
        if (RptName == "Account_Emp" || RptName == "Supplementary_Report_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Account_Emp")
        {
            SSQL = SSQL + " and (EM.AccountNo!='' or EM.AccountNo is not null)";
        }

        if (RptName == "Non_Account_Emp" || RptName == "Supplementary_Report_Non_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Non_Account_Emp")
        {
            SSQL = SSQL + " and (EM.AccountNo='' or EM.AccountNo is null)";
        }
        SSQL = SSQL + " order by CAST(EM.ExistingCode as int) Asc";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["EmpNo"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["AccNo"] = dt.Rows[i]["AccountNo"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["IncSal"] = dt.Rows[i]["Salary2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ActualSal"] = dt.Rows[i]["BaseSalary"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WorkDays"] = dt.Rows[i]["WorkedDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WeekPresentDays"] = dt.Rows[i]["WH_Work_Days"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["LOPDays"] = dt.Rows[i]["LOPDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PaidDays"] = dt.Rows[i]["WorkedDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["NH"] = dt.Rows[i]["NFh"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Basic"] = dt.Rows[i]["BasicAndDANew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MediAllow"] = dt.Rows[i]["MediAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DA"] = dt.Rows[i]["BasicRAI"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["HRA"] = dt.Rows[i]["BasicHRA"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WAAllow"] = dt.Rows[i]["WashingAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TA"] = dt.Rows[i]["ConvAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["EduAllow"] = dt.Rows[i]["EduAllow"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DaysAllow"] = dt.Rows[i]["allowances1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MasthiriIncentive"] = dt.Rows[i]["allowances2"].ToString();

                if (RptName == "Employee_Payslip_Cover_Report_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Non_Account_Emp")
                {
                    if (dt.Rows[i]["Catname"].ToString() == "STAFF")
                    {
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalSalary"] = dt.Rows[i]["BaseSalary"].ToString();
                    }
                    else
                    {
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalSalary"] = dt.Rows[i]["GrossEarnings"].ToString();
                    }
                }
                else
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalSalary"] = dt.Rows[i]["GrossEarnings"].ToString();
                }


                AutoDT.Rows[AutoDT.Rows.Count - 1]["PF"] = dt.Rows[i]["ProvidentFund"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESI"] = dt.Rows[i]["ESI"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TDS"] = dt.Rows[i]["Deduction3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["LWF"] = dt.Rows[i]["Deduction4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Mess"] = dt.Rows[i]["Deduction2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Advance"] = dt.Rows[i]["Advance"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Rent"] = dt.Rows[i]["Deduction1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Cable"] = dt.Rows[i]["Deduction5"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SweepeR"] = dt.Rows[i]["SweepeR"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Fine"] = dt.Rows[i]["Fine"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Store"] = "0";
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Union"] = dt.Rows[i]["Unioncg"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Others1"] = dt.Rows[i]["DedOthers1"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Others2"] = dt.Rows[i]["DedOthers2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalDeduc"] = dt.Rows[i]["TotalDeductions"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["PreWP"] = dt.Rows[i]["PerWP"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WpAmt"] = dt.Rows[i]["ThreesidedAmt"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OTHrs"] = dt.Rows[i]["OTHoursNew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OTAmt"] = dt.Rows[i]["OTHoursAmtNew"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PaidSalary"] = dt.Rows[i]["PaidAmt"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["SDAllow"] = dt.Rows[i]["allowances3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Earnings2"] = dt.Rows[i]["allowances4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["StAl3"] = dt.Rows[i]["allowances5"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["BioID"] = dt.Rows[i]["MachineID"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DeptName"] = dt.Rows[i]["DeptName"].ToString();


                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalEarnings"] = dt.Rows[i]["NetPay"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["LastName"] = dt.Rows[i]["LastName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["DOB"] = dt.Rows[i]["DOB"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["UAN"] = dt.Rows[i]["UAN"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESINo"] = dt.Rows[i]["ESINo"].ToString();

            }
        }
        DataView dv = AutoDT.DefaultView;
        dv.Sort = "EmpNo Asc";
        AutoDT = dv.ToTable();
        if (AutoDT.Rows.Count > 0)
        {
            DataTable dts = new DataTable();
            SSQL = "Select * from [" + SessionEpay + "]..AdminRights where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            dts = objdata.RptEmployeeMultipleDetails(SSQL);
            ds.Tables.Add(AutoDT);
            if (RptName == "Supplementary_Report_Account_Emp" || RptName == "Supplementary_Report_Non_Account_Emp")
            {
                report.Load(Server.MapPath("Payslip/Supplementary_Report.rpt"));
            }
            else if (RptName == "Employee_Payslip_Cover_Report_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Non_Account_Emp")
            {
                report.Load(Server.MapPath("Payslip/Payslip_Cover_Report.rpt"));
            }
            else
            {
                report.Load(Server.MapPath("Payslip/PaySlipRpt.rpt"));
            }
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'"+dts.Rows[0]["Cname"].ToString()+"'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'"+dts.Rows[0]["LCode"].ToString()+"'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Fromdate.ToString() + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + toDate.ToString() + "'";
            report.DataDefinition.FormulaFields["Wages"].Text = "'" + EmpType.ToString() + "'";
            if (RptName == "Supplementary_Report_Account_Emp" || RptName == "Supplementary_Report_Non_Account_Emp")
            {
                report.DataDefinition.FormulaFields["Month_Str"].Text = "'" + Month.ToString() + "'";
            }
            if (RptName == "Employee_Payslip_Cover_Report_Account_Emp" || RptName == "Employee_Payslip_Cover_Report_Non_Account_Emp")
            {
                report.DataDefinition.FormulaFields["Month_Str"].Text = "'" + Month.ToString() + "'";
            }
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }
}