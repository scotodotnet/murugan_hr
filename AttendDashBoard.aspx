﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="AttendDashBoard.aspx.cs" Inherits="AttendDashBoard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin page-header -->
                <h1 class="page-header">DashBoard </h1>
                <!-- end page-header -->
                <!-- begin row -->
                <div class="row">
                    <!-- Employee Strength Department Wise Start -->
                    <div class="col-sm-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h5 class="panel-title">Employee Strength </h5>
                            </div>
                            <div class="panel-body">
                                <div>
                                    <asp:Chart ID="Employee_Strength_DeptWise" runat="server" Compression="100"
                                        EnableViewState="True" Width="480px">
                                        <Series>
                                            <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Doughnut" Label="#VALX: #VALY"
                                                IsXValueIndexed="True">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- col-sm-6 -->
                    <!-- Employee Strength Department Wise End -->

                    <!-- Employee Attendance Department Wise Start -->
                    <div class="col-sm-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h5 class="panel-title">Employee Attendance </h5>
                            </div>
                            <div class="panel-body">
                                <div>
                                    <asp:Chart ID="UnitoneChart" runat="server" Compression="100"
                                        EnableViewState="True" Width="480px">
                                        <Series>
                                            <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Pie" Label="#VALX: #VALY"
                                                IsXValueIndexed="True">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- col-sm-6 -->
                    <!-- Employee Attendance Department Wise End -->
                </div>
                <!-- Row -->
                <!-- begin row -->
                <div class="row">
                    <!-- Current Year Employee Join Unit Wise Start -->
                    <div class="col-sm-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h5>
                                    <label>Employee New Joinee / Left Employee</label></h5>
                            </div>
                            <div class="panel-body">
                                <div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>New Joinee : </label>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="txtNewJoin" runat="server" Text="0" Style="background-color: Yellow"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Left Employee : </label>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="txtLeft" runat="server" Text="0" Style="background-color: Yellow"></asp:Label>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>

                                    <div>
                                        <asp:Chart ID="Current_Year_Emp_Join" runat="server" Compression="100"
                                            EnableViewState="True" Width="470px">
                                            <%-- <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">style="color: #3cde50;"
                                    </asp:Series>
                                    <asp:Series Name="Series2" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                    
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>--%>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- col-sm-6 -->
                    <!-- Current Year Employee Join Unit Wise End -->
                    <!-- Salary Strength Unitwise Start -->
                    <div class="col-sm-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h5>
                                    <label>Salary Costing </label>
                                </h5>
                            </div>
                            <div class="panel-body">
                                <div>

                                    <div>
                                        <asp:Chart ID="Salary_Strength_Unitwise" runat="server" Compression="100"
                                            EnableViewState="True" Width="470px">
                                            <Series>
                                                <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Pie" Label="#VALX: #VALY"
                                                    IsXValueIndexed="True">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Salary Strength Unitwise End -->

                </div>
                <!-- Row -->
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

