﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;

public partial class RptNewRptDisplay : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();


    int tot1 = 0;
    int tot2 = 0;
    int tot3 = 0;
    int Total = 0;
    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;
    string Emp_Wages_Type = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Report";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();

            //HR COST REPORT UPDATE
            if (Request.QueryString["RptName"].ToString() == "HR COST REPORT UPDATE")
            {
                string Date = Request.QueryString["FromDate"].ToString();
                GetHR_Cost_Rpt_Update(Date);
            }

            //HR DAILY TEA AND SNACKS COST REPORT
            if (Request.QueryString["RptName"].ToString() == "HR DAILY TEA AND SNACKS COST REPORT")
            {
                string Date = Request.QueryString["FromDate"].ToString();
                GetHR_Daily_Tea_Snacks_Cost_Rpt(Date);
            }

            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED - STRENGTH")
            {
                GetHRConsolidatesStrength();
            }
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED - STRENGTH")
            {
                GetHRConsolidatesStrength();
            }
            if (Request.QueryString["RptName"].ToString() == "HR TRACER")
            {
                string DeptName = Request.QueryString["DeptName"].ToString();
                string FromDate = Request.QueryString["FromDate"].ToString();
                string ToDate = Request.QueryString["ToDate"].ToString();
                string CurrDate = Request.QueryString["CurrentDate"].ToString();
                GetHRTracer(DeptName, FromDate, ToDate, CurrDate);
            }
            if (Request.QueryString["RptName"].ToString() == "HR LEAVE REPORT - LABOUR WISE")
            {
                string FromDate = Request.QueryString["FromDate"].ToString();
                string ToDate = Request.QueryString["ToDate"].ToString();
                GetHRLeaveLabourWise(FromDate, ToDate);
            }

            //New Reports 
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATE REPORT UPDATE")
            {
                string Date = Request.QueryString["Date"].ToString();
                GetHRConsolidate_Report_Update(Date);
            }

            //New Reports 
            if (Request.QueryString["RptName"].ToString() == "HR CANTEEN OPERATOR LEADER WISE REPORT")
            {
                string FromDate = Request.QueryString["FromDate"].ToString();
                string ToDate = Request.QueryString["ToDate"].ToString();
                string LeaderName = Request.QueryString["LeaderName"].ToString();
                string CanOptName = Request.QueryString["CanOptName"].ToString();
                GetHR_CANTEEN_OPERATOR_LEADER_WISE_REPORT(FromDate, ToDate, LeaderName, CanOptName);
            }

            //New Reports 
            if (Request.QueryString["RptName"].ToString() == "HR CANTEEN CONSOLIDATE ALL UNITS REPORT")
            {
                string Date = Request.QueryString["Date"].ToString();
                GetHR_Canteen_Consolidate_All_UNIT(Date);
            }
            //New Reports 
            if (Request.QueryString["RptName"].ToString() == "HR DAILY TEA AND SNACKS CONSOLIDATE ALL UNITS REPORT")
            {
                string Date = Request.QueryString["Date"].ToString();
                GetHR_Daily_Tea_Snacks_Consolidate_All_UNIT(Date);
            }
            //New Reports 
            if (Request.QueryString["RptName"].ToString() == "HR COCK PIT REPORT UPDATE")
            {
                string Date = Request.QueryString["Date"].ToString();
                GetHR_COCKPIT_RPT(Date);
            }
            //New Reports 
            if (Request.QueryString["RptName"].ToString() == "HR STRENGTH ALL UNITS REPORT")
            {
                string Date = Request.QueryString["Date"].ToString();
                GetHR_Strength_ALL_UNIT_RPT(Date);
            }
            //New Report by Suresh
            if (Request.QueryString["RptName"].ToString() == "HR 24X7 CONSOLIDATE REPORT")
            {
                string Date = Request.QueryString["FromDate"].ToString();
                GetHR_Consolidate_Rpt(Date);
            }

            //New Report by Suresh
            if (Request.QueryString["RptName"].ToString() == "HR DEPARTMENT AVAILABLE REPORT")
            {
                string Dept = Request.QueryString["Dept"].ToString();
                GetOEDeptAvailable_Report(Dept);
            }
            if (Request.QueryString["RptName"].ToString() == "HR CONSOLIDATED - DEPT AND GRADE WISE REPORT")
            {
                string Date3_str = Request.QueryString["FromDate"].ToString();
                GetHRConsolidatesDeptAndGradeWise(Date3_str);
            }
            if (Request.QueryString["RptName"].ToString() == "HR DAILY CANTEEN REPORT")
            {
                string Date3_str = Request.QueryString["Date"].ToString();
                GetHR_DailyCanteen_Rpt(Date3_str);
            }
        }
    }

    private void GetHR_DailyCanteen_Rpt(string date3_str)
    {

        string attachment = "attachment;filename=HR DAILY CANTEEN REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='13'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write(" ");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("&nbsp;&nbsp; &nbsp;&nbsp;");
        Response.Write("<a style=\"font-weight:bold\">DATE</a>");
        Response.Write(":");
        Response.Write("<a style=\"font-weight:bold\">" + date3_str + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='13'>");
        Response.Write("<a style=\"font-weight:bold\">DAILY CANTEEN REPORT</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");


        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan ='2'>");
        Response.Write("<a style=\"font-weight:bold\">Particulars</a>");
        Response.Write("</td>");

        Response.Write("<td colspan ='3'>");
        Response.Write("<a style=\"font-weight:bold\">ON DUTY</a>");
        Response.Write("</td>");


        SSQL = "";
        SSQL = "select CanID,CanteenName,Amount from MstCanteen where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
        DataTable dt_cantenn = new DataTable();
        dt_cantenn = objdata.RptEmployeeMultipleDetails(SSQL);

        Response.Write("<td colspan ='" + (dt_cantenn.Rows.Count + 1) + "'>");
        Response.Write("<a style=\"font-weight:bold\">ON DATE BILL</a>");
        Response.Write("</td>");

        Response.Write("<td colspan ='" + (dt_cantenn.Rows.Count + 1) + "'>");
        Response.Write("<a style=\"font-weight:bold\">UP DATE BILL</a>");
        Response.Write("</td>");

        Response.Write("</tr>");


        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">AVAIL</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">PRESENT</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">ABSENT</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">TOT.EMP</a>");
        Response.Write("</td>");

        if (dt_cantenn.Rows.Count > 0)
        {
            for (int iCanRw = 0; iCanRw < dt_cantenn.Rows.Count; iCanRw++)
            {
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">" + dt_cantenn.Rows[iCanRw]["CanteenName"].ToString() + "</a>");
                Response.Write("</td>");
            }
        }
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">TOT</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">TOT.Cost</a>");
        Response.Write("</td>");


        if (dt_cantenn.Rows.Count > 0)
        {
            for (int iCanRw = 0; iCanRw < dt_cantenn.Rows.Count; iCanRw++)
            {
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">" + dt_cantenn.Rows[iCanRw]["CanteenName"].ToString() + "</a>");
                Response.Write("</td>");
            }
        }
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">TOT</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">TOT.Cost</a>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("</table>");

        Response.Write("<table border='1'>");

        SSQL = "";
        //SSQL = "Select deptname,DeptCode from Department_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";

        SSQL = "Select AgentName,AgentID from AgentMst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt_dept = new DataTable();
        dt_dept = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_dept.Rows.Count > 0)
        {
            for (int iDeptRw = 0; iDeptRw < dt_dept.Rows.Count; iDeptRw++)
            {
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td>");
                Response.Write("<a>" + dt_dept.Rows[iDeptRw]["AgentName"].ToString() + "</a>");
                Response.Write("</td>");


                //SSQL = "";
                //SSQL = " Select Count(machineID) as Avail from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                //SSQL = SSQL + " and (isActive='Yes' or Convert(date,dor,103)>=Convert(date,'" + date3_str + "',103)) and (EligibleCateen='1' or EligibleCateen='Yes')";
                //SSQL = SSQL + " and DeptName='" + dt_dept.Rows[iDeptRw]["DeptName"].ToString() + "'";
                SSQL = "";
                SSQL = " Select Count(machineID) as Avail from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and (isActive='Yes' or Convert(date,dor,103)>=Convert(date,'" + date3_str + "',103)) and (EligibleCateen='1' or EligibleCateen='Yes')";
                SSQL = SSQL + " and AgentName='" + dt_dept.Rows[iDeptRw]["AgentName"].ToString() + "'";
                DataTable dt_EmpAvail = new DataTable();
                dt_EmpAvail = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_EmpAvail.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write("<a>" + dt_EmpAvail.Rows[0]["Avail"].ToString() + "</a>");
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");
                }

                SSQL = " Select sum(Present) as present,sum(Absent) as Absent,(Sum(Present)+Sum(Absent)) as total from (";
                SSQL = SSQL + " Select isnull(Count(LD.MachineID),0) as Present,0 as Absent from Logtime_days LD inner join Employee_mst Em on Em.machineID=LD.machineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                SSQL = SSQL + " where Convert(date,Attn_date,103)=Convert(date,'" + date3_str + "',103) and (EM.isActive='Yes' or Convert(date,EM.dor,103)>=Convert(date,'" + date3_str + "',103))";
                SSQL = SSQL + " and (EM.EligibleCateen='1' or EM.EligibleCateen='Yes') and LD.CompCode='" + SessionCcode + "' and LD.locCode='" + SessionLcode + "' and EM.AgentName='" + dt_dept.Rows[iDeptRw]["AgentName"].ToString() + "' and LD.Present>0";
                SSQL = SSQL + " UNION ALL";
                SSQL = SSQL + " Select 0 as Present,isnull(Count(LD.MachineID),0) as Absent from Logtime_days LD inner join Employee_mst Em on Em.machineID=LD.machineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                SSQL = SSQL + " where Convert(date,Attn_date,103)=Convert(date,'" + date3_str + "',103) and (EM.isActive='Yes' or Convert(date,EM.dor,103)>=Convert(date,'" + date3_str + "',103))";
                SSQL = SSQL + " and (EM.EligibleCateen='1' or EM.EligibleCateen='Yes') and LD.CompCode='" + SessionCcode + "' and LD.locCode='" + SessionLcode + "' and EM.AgentName='" + dt_dept.Rows[iDeptRw]["AgentName"].ToString() + "' and LD.Present=0) as tbl";

                DataTable dt_Present_Absent = new DataTable();
                dt_Present_Absent = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Present_Absent.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write("<a>" + dt_Present_Absent.Rows[0]["present"].ToString() + "</a>");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("<a>" + dt_Present_Absent.Rows[0]["Absent"].ToString() + "</a>");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("<a>" + dt_Present_Absent.Rows[0]["total"].ToString() + "</a>");
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");
                }

                //On Date Bill
                int tot = 0;
                if (dt_cantenn.Rows.Count > 0)
                {
                    decimal totalCost = 0;
                    for (int iCanRw = 0; iCanRw < dt_cantenn.Rows.Count; iCanRw++)
                    {
                        SSQL = " Select isnull(Count(LD.MachineID),0) as Present,isnull(Count(LD.MachineID),0)*" + dt_cantenn.Rows[iCanRw]["Amount"].ToString() + " as Amount from Logtime_days LD inner join Employee_mst Em on Em.machineID=LD.machineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                        SSQL = SSQL + " where Convert(date,Attn_date,103)=Convert(date,'" + date3_str + "',103) and (EM.isActive='Yes' or Convert(date,EM.dor,103)>=Convert(date,'" + date3_str + "',103))";
                        SSQL = SSQL + " and (EM.EligibleCateen='1' or EM.EligibleCateen='Yes') and LD.CompCode='" + SessionCcode + "' and LD.locCode='" + SessionLcode + "' and EM.AgentName='" + dt_dept.Rows[iDeptRw]["AgentName"].ToString() + "' and LD.Present>0";
                        SSQL = SSQL + " and EM.Canteenname='" + dt_cantenn.Rows[iCanRw]["CanID"].ToString() + "'";

                        DataTable dt_CantPresent_Absent = new DataTable();
                        dt_CantPresent_Absent = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_CantPresent_Absent.Rows.Count > 0)
                        {
                           
                            Response.Write("<td>");
                            Response.Write("<a>" + dt_CantPresent_Absent.Rows[0]["Present"].ToString() + "</a>");
                            Response.Write("</td>");
                            tot += Convert.ToInt32(dt_CantPresent_Absent.Rows[0]["Present"].ToString());
                            //totalCost = totalCost + Convert.ToDecimal(dt_CantPresent_Absent.Rows[0]["Amount"].ToString());
                            totalCost = tot * 65;
                        }
                        else
                        {
                            Response.Write("<td>");
                            Response.Write("<a>0</a>");
                            Response.Write("</td>");
                        }
                    }
                   // tot += tot;
                    Response.Write("<td>");
                    Response.Write("<a>" + tot + "</a>");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("<a>" + totalCost + "</a>");
                    Response.Write("</td>");

                    totalCost = 0;
                    int tot1 = 0;
                    for (int iCanRw = 0; iCanRw < dt_cantenn.Rows.Count; iCanRw++)
                    {

                        SSQL = " Select isnull(Count(LD.MachineID),0) as Present,isnull(Count(LD.MachineID),0)*" + dt_cantenn.Rows[iCanRw]["Amount"].ToString() + " as Amount from Logtime_days LD inner join Employee_mst Em on Em.machineID=LD.machineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                        SSQL = SSQL + " where  (EM.isActive='Yes' or Convert(date,EM.dor,103)>=Convert(date,'" + date3_str + "',103))";
                        SSQL = SSQL + " and (EM.EligibleCateen='1' or EM.EligibleCateen='Yes') and LD.CompCode='" + SessionCcode + "' and LD.locCode='" + SessionLcode + "' and EM.AgentName='" + dt_dept.Rows[iDeptRw]["AgentName"].ToString() + "' and LD.Present>0";
                        SSQL = SSQL + " and EM.Canteenname='" + dt_cantenn.Rows[iCanRw]["CanID"].ToString() + "'";
                        SSQL = SSQL + " and Convert(date,Attn_date,103)>=Convert(date,'01/" + Convert.ToDateTime(date3_str).ToString("MM/yyyy") + "',103) and Convert(date,Attn_date,103)<=Convert(date,'" + date3_str + "',103)";

                        DataTable dt_CantPresent_Absent = new DataTable();
                        dt_CantPresent_Absent = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_CantPresent_Absent.Rows.Count > 0)
                        {
                            Response.Write("<td>");
                            Response.Write("<a>" + dt_CantPresent_Absent.Rows[0]["Present"].ToString() + "</a>");
                            Response.Write("</td>");
                            tot1 += Convert.ToInt32(dt_CantPresent_Absent.Rows[0]["Present"].ToString());
                            //totalCost = totalCost + Convert.ToDecimal(dt_CantPresent_Absent.Rows[0]["Amount"].ToString());
                            totalCost = tot1 * 65;
                        }
                        else
                        {
                            Response.Write("<td>");
                            Response.Write("<a>0</a>");
                            Response.Write("</td>");
                        }
                    }
                    Response.Write("<td>");
                    Response.Write("<a>" + tot1 + "</a>");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("<a>" + totalCost + "</a>");
                    Response.Write("</td>");
                }

                Response.Write("</tr>");
            }
        }

        Response.Write("</table>");

        Response.End();
        Response.Clear();
    }

    private void GetHR_Daily_Tea_Snacks_Cost_Rpt(string date)
    {

        string attachment = "attachment;filename=HR DAILY TEA SNACKS COST REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='17'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write(" ");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("&nbsp;&nbsp; &nbsp;&nbsp;");
        Response.Write("<a style=\"font-weight:bold\">DATE</a>");
        Response.Write(":");
        Response.Write("<a style=\"font-weight:bold\">" + date + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='17'>");
        Response.Write("<a style=\"font-weight:bold\">DAILY TEA & SNACKS COST REPORT</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");

        Response.Write("<table border='1'>");
        Response.Write("<tr>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ALCA</a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">ON DUTY</a>");
        Response.Write("</td>");

        Response.Write("<td rowspan='2'>");

        DataTable dt_Items = new DataTable();
        DataTable dt_TeaItem = new DataTable();
        dt_Items.Columns.Add("ItemName");

        SSQL = "";
        SSQL = "Select CONVERT(varchar(15),time,22) as time,Count(ItemName) as Count from MstTeaSnacks where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Convert(date,Dte,103)=Convert(date,'" + date + "',103) group by time";
        DataTable dt_TeaTime = new DataTable();
        dt_TeaTime = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_TeaTime.Rows.Count > 0)
        {
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");

            for (int ITeaRw = 0; ITeaRw < dt_TeaTime.Rows.Count; ITeaRw++)
            {
                Response.Write("<td colspan='" + dt_TeaTime.Rows[ITeaRw]["Count"].ToString() + "'>");
                Response.Write("<a style=\"font-weight:bold\">" + Convert.ToDateTime(dt_TeaTime.Rows[ITeaRw]["time"].ToString()).ToString("hh:mm tt") + "</a>");
                Response.Write("</td>");
            }
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            for (int ITeaRw = 0; ITeaRw < dt_TeaTime.Rows.Count; ITeaRw++)
            {
                SSQL = "";
                SSQL = "Select ItemName from MstTeaSnacks where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Convert(varchar(15),time,22)=Convert(varchar(15),'" + dt_TeaTime.Rows[ITeaRw]["time"] + "',22) and Convert(date,Dte,103)=Convert(date,'" + date + "',103)";

                dt_TeaItem = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_TeaItem.Rows.Count > 0)
                {
                    for (int iTeaItem = 0; iTeaItem < dt_TeaItem.Rows.Count; iTeaItem++)
                    {
                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + dt_TeaItem.Rows[iTeaItem]["ItemName"].ToString() + "</a>");
                        Response.Write("</td>");

                        dt_Items.Rows.Add();
                        dt_Items.Rows[dt_Items.Rows.Count - 1]["ItemName"] = dt_TeaItem.Rows[iTeaItem]["ItemName"].ToString();
                    }
                }
            }

            Response.Write("</tr>");
            Response.Write("</table>");
        }


        Response.Write("</td>");

        List<string> CountryList = new List<string>();

        dt_Items.AsEnumerable().Select(s => s.Field<string>("ItemName").ToString()).ToList().ForEach(c => c.ToString().Split('|').ToList().ForEach(l => CountryList.Add(l)));

        int colspan = 0;
        foreach (string str in CountryList.Distinct())
        {
            Response.Write("<td rowspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">TOTAL " + str.ToString() + "</a>");
            Response.Write("</td>");
            colspan = colspan + 1;
        }
        Response.Write("<td rowspan='2'>");

        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='" + colspan + "'>");
        Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        foreach (string str in CountryList.Distinct())
        {

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + str.ToString() + "</a>");
            Response.Write("</td>");
        }
        Response.Write("</tr>");

        Response.Write("</table>");

        Response.Write("</td>");

        Response.Write("<td rowspan=2>");
        Response.Write("<a style=\"font-weight:bold\">TOTAL COST</a>");
        Response.Write("</td>");
        Response.Write("<td rowspan=2>");
        Response.Write("<a style=\"font-weight:bold\">UPDATE COST</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");


        Response.Write("<table border='1'>");

        SSQL = "";
        SSQL = "Select DeptName,DeptCode from Department_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt_Dept = new DataTable();
        dt_Dept = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Dept.Rows.Count > 0)
        {
            SSQL = "";
            SSQL = "Select Shiftdesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            DataTable dt_Shift = new DataTable();
            dt_Shift = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int IShiftRw = 0; IShiftRw < dt_Shift.Rows.Count; IShiftRw++)
            {
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">" + dt_Shift.Rows[IShiftRw]["Shiftdesc"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                for (int iDeptRow = 0; iDeptRow < dt_Dept.Rows.Count; iDeptRow++)
                {
                    DataTable dt_OnDateTotal = new DataTable();
                    dt_OnDateTotal.Columns.Add("Itemname");
                    dt_OnDateTotal.Columns.Add("TotalCount");
                    dt_OnDateTotal.Columns.Add("Cost", typeof(decimal));

                    foreach (string str in CountryList.Distinct())
                    {
                        dt_OnDateTotal.Rows.Add();
                        dt_OnDateTotal.Rows[dt_OnDateTotal.Rows.Count - 1]["ItemName"] = str.ToString();
                        dt_OnDateTotal.Rows[dt_OnDateTotal.Rows.Count - 1]["TotalCount"] = 0;
                        dt_OnDateTotal.Rows[dt_OnDateTotal.Rows.Count - 1]["Cost"] = 0;

                    }

                    Response.Write("<tr>");
                    Response.Write("<td>");
                    Response.Write("<a>" + dt_Dept.Rows[iDeptRow]["DeptName"].ToString() + "</a>");
                    Response.Write("</td>");

                    SSQL = "";
                    SSQL = "Select Count(MachineID) as Alca from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and DeptCode='" + dt_Dept.Rows[iDeptRow]["DeptCode"].ToString() + "'";
                    SSQL = SSQL + " and (isActive='Yes' or Convert(date,dor,103)>=Convert(date,'" + date + "',103)) and (EligibleSnacks='1' or EligibleSnacks='Yes')";
                    DataTable dt_Acla = new DataTable();
                    dt_Acla = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_Acla.Rows.Count > 0)
                    {
                        Response.Write("<td>");
                        Response.Write("<a>" + dt_Acla.Rows[0]["Alca"].ToString() + "</a>");
                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<td>");
                        Response.Write("<a>0</a>");
                        Response.Write("</td>");
                    }

                    SSQL = "";
                    SSQL = "Select Count(LD.MachineID) as onduty from Logtime_Days LD inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode Collate Database_Default=LD.LocCode COLLATE Database_Default where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "' and LD.DeptName='" + dt_Dept.Rows[iDeptRow]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " and LD.Present!='0.0' and (EM.EligibleSnacks='1' or EM.EligibleSnacks='Yes') and LD.Shift='" + dt_Shift.Rows[IShiftRw]["Shiftdesc"].ToString() + "'";
                    SSQL = SSQL + " and Convert(date,LD.Attn_date,103)=Convert(date,'" + date + "',103)";
                    DataTable dt_onduty = new DataTable();
                    dt_onduty = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_onduty.Rows.Count > 0)
                    {
                        Response.Write("<td>");
                        Response.Write("<a>" + dt_onduty.Rows[0]["onduty"].ToString() + "</a>");
                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<td>");
                        Response.Write("<a>0</a>");
                        Response.Write("</td>");
                    }

                    for (int ITeaRw = 0; ITeaRw < dt_TeaTime.Rows.Count; ITeaRw++)
                    {
                        SSQL = "";
                        SSQL = "Select ItemName,Amount from MstTeaSnacks where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Convert(varchar(15),time,22)=Convert(varchar(15),'" + dt_TeaTime.Rows[ITeaRw]["time"] + "',22) and Convert(date,Dte,103)=Convert(date,'" + date + "',103)";
                        dt_TeaItem = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dt_TeaItem.Rows.Count > 0)
                        {
                            for (int iTeaItem = 0; iTeaItem < dt_TeaItem.Rows.Count; iTeaItem++)
                            {
                                SSQL = "";
                                SSQL = " Select Count(LD.MachineID) as count from Logtime_Days LD inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";

                                SSQL = SSQL + " where  EM.EligibleSnacks='1' and Convert(time,'" + dt_TeaTime.Rows[ITeaRw]["time"].ToString() + "',22) >= Convert(time,TimeIN,22) and LD.DeptName='" + dt_Dept.Rows[iDeptRow]["DeptName"].ToString() + "'";
                                SSQL = SSQL + " and Convert(time,'" + dt_TeaTime.Rows[ITeaRw]["time"].ToString() + "',22) <= Convert(time,TimeOut,22) and LD.Present>0  ";
                                SSQL = SSQL + " and Convert(date,Attn_date,103)=Convert(date,'" + date + "',103) and LD.Shift='" + dt_Shift.Rows[IShiftRw]["Shiftdesc"].ToString() + "'";
                                DataTable dt_ondate = new DataTable();
                                dt_ondate = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_ondate.Rows.Count > 0)
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a>" + dt_ondate.Rows[0]["count"].ToString() + "</a>");
                                    Response.Write("</td>");

                                    foreach (DataRow dr in dt_OnDateTotal.Rows)
                                    {
                                        if (dr["Itemname"].ToString() == dt_TeaItem.Rows[iTeaItem]["ItemName"].ToString())
                                        {
                                            dr["TotalCount"] = Convert.ToDecimal(Convert.ToDecimal(dr["TotalCount"]) + Convert.ToDecimal(dt_ondate.Rows[0]["count"].ToString())).ToString("0");
                                            dr["Cost"] = (Convert.ToDecimal(Convert.ToDecimal(dr["Cost"])) + (Convert.ToDecimal((Convert.ToDecimal(dt_TeaItem.Rows[iTeaItem]["Amount"].ToString()) * Convert.ToDecimal(dt_ondate.Rows[0]["Count"].ToString()))))).ToString("0");
                                            dt_OnDateTotal.AcceptChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a>0</a>");
                                    Response.Write("</td>");
                                }
                            }
                        }
                    }

                    //foreach (string str in CountryList.Distinct())
                    //{
                    //    foreach (DataRow dr in dt_OnDateTotal.Rows)
                    //    {
                    //        if (dr["Itemname"].ToString() == str.ToString())
                    //        {
                    //            Response.Write("<td>");
                    //            Response.Write("<a>0</a>");
                    //            Response.Write("</td>");
                    //        }
                    //    }
                    //}
                    for (int i = 0; i < dt_OnDateTotal.Rows.Count; i++)
                    {
                        Response.Write("<td>");
                        Response.Write("<a>" + dt_OnDateTotal.Rows[i]["TotalCount"].ToString() + "</a>");
                        Response.Write("</td>");
                    }

                    for (int i = 0; i < dt_OnDateTotal.Rows.Count; i++)
                    {
                        Response.Write("<td>");
                        Response.Write("<a>" + dt_OnDateTotal.Rows[i]["Cost"].ToString() + "</a>");
                        Response.Write("</td>");
                    }

                    Response.Write("<td>");
                    Response.Write("<a>" + dt_OnDateTotal.AsEnumerable().Sum(x => x.Field<decimal>("Cost")).ToString() + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }
            }
        }

        Response.Write("</table>");
        Response.End();
        Response.Clear();

    }

    private void GetHR_Cost_Rpt_Update(string str_date)
    {
        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable DeptMst = new DataTable();
        DataTable Attn = new DataTable();
        DataTable GradeMst = new DataTable();
        DataTable LbrAllotmst = new DataTable();

            
        string attachment = "attachment;filename=HR COST REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table border='1'>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='17'>");
        Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
        Response.Write(" ");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='right'>");
        Response.Write("<td colspan='17'>");
        Response.Write("<a style=\"font-weight:bold\">Date: " + str_date.ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td rowspan='3'>");
        Response.Write("<a style=\"font-weight:bold\">PRODUCTION</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a>OPTIMUM</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a>ONDATE</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='10'>");
        Response.Write("<a>HR COST REPORT</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a>UP DATE</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td >");
        Response.Write("<a >PRDN</a>");
        Response.Write("</td>");

        string OptProduction = "0";
        string OnDateProduction = "0";
        string OptPrdEff = "100";
        string OnDatePrdEff = "0";
        string UpDatePrdn = "0";
        string UpDatePrdnEff = "0";

        //Get Optimum Production
        SSQL = "";
        SSQL = "Select ProductionFixed,ProductionEarned from ProductionOnDate where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Convert(date,Tarns_Date,103)=Convert(date,'" + str_date + "',103)";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            OptProduction = Convert.ToDecimal(dt.Rows[0]["ProductionFixed"]).ToString();
            OnDateProduction = Convert.ToDecimal(dt.Rows[0]["ProductionEarned"]).ToString();

            OnDatePrdEff = Convert.ToDecimal(OptProduction) > 0 ? (Convert.ToDecimal(OnDateProduction) / Convert.ToDecimal(OptProduction)).ToString() : "0";
            OnDatePrdEff = Math.Round((Convert.ToDecimal(OnDatePrdEff) * 100), 0, MidpointRounding.AwayFromZero).ToString();
        }


        Response.Write("<td >");
        Response.Write("<a >" + OptProduction + "</a>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a >PRDN</a>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a style=\"font-weight:bold\">" + OnDateProduction + "</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='10'>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a >PRDN</a>");
        Response.Write("</td>");

        //Get Update Production
        SSQL = "";
        SSQL = "Select SUM(ProductionFixed) as SumOndatePrdn,SUM(ProductionEarned) as SumUpDatePrdn from ProductionOnDate where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Convert(date,Tarns_Date,103)>=Convert(date,'01/" + Convert.ToDateTime(str_date).ToString("MM/yyyy") + "',103)";
        SSQL = SSQL + " and Convert(date,Tarns_Date,103)<=Convert(date,'" + str_date + "',103)";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            UpDatePrdn = dt.Rows[0]["SumUpDatePrdn"].ToString();

            UpDatePrdnEff = Convert.ToDecimal(UpDatePrdn) > 0 ? (Convert.ToDecimal(UpDatePrdn) / Convert.ToDecimal(dt.Rows[0]["SumOndatePrdn"].ToString())).ToString() : "0";
            UpDatePrdnEff = Math.Round((Convert.ToDecimal(UpDatePrdnEff) * 100), 0, MidpointRounding.AwayFromZero).ToString();

        }


        Response.Write("<td >");
        Response.Write("<a style=\"font-weight:bold\">" + UpDatePrdn + "</a>");
        Response.Write("</td>");

        Response.Write("</tr>");


        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td >");
        Response.Write("<a >EFF</a>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a style=\"font-weight:bold\">100</a>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a>EFF</a>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a style=\"font-weight:bold\">" + OnDatePrdEff + "</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='10'>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a >EFF</a>");
        Response.Write("</td>");

        Response.Write("<td >");
        Response.Write("<a style=\"font-weight:bold\">" + UpDatePrdnEff + "</a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td rowspan='3'>");
        Response.Write("<a style=\"font-weight:bold\">HR</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a>OPTIMUM</a>");
        Response.Write("</td>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a>ON DATE</a>");
        Response.Write("</td>");

        //Get Grade Master
        SSQL = "";
        SSQL = "select GradeID,GradeName from MstGrade where LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "'";
        SSQL = SSQL + "  order by isnull(NewCode*0,1),NewCode ASC";
        GradeMst = objdata.RptEmployeeMultipleDetails(SSQL);
        if (GradeMst.Rows.Count > 0)
        {
            for (int IGrRow = 0; IGrRow < GradeMst.Rows.Count; IGrRow++)
            {
                Response.Write("<td colspan='2'>");
                Response.Write("<a>" + GradeMst.Rows[IGrRow]["GradeName"].ToString() + "</a>");
                Response.Write("</td>");
            }
        }

        Response.Write("<td colspan='2'>");
        Response.Write("<a>UP DATE</a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td>");
        Response.Write("<a>NOS</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>COST</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>NOS</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>COST</a>");
        Response.Write("</td>");


        if (GradeMst.Rows.Count > 0)
        {
            for (int IGrRow = 0; IGrRow < GradeMst.Rows.Count; IGrRow++)
            {
                Response.Write("<td>");
                Response.Write("<a>NOS</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>MTL</a>");
                Response.Write("</td>");
            }
        }

        Response.Write("<td>");
        Response.Write("<a>NOS</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>COST</a>");
        Response.Write("</td>");

        Response.Write("</tr>");


        SSQL = "";
        SSQL = "Select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "select	DeptCode,DeptName from Department_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DeptMst = objdata.RptEmployeeMultipleDetails(SSQL);

        int OptimumSumNos = 0;
        int OptimumSumCost = 0;
        int OnSumNos = 0;
        int OnSumCost = 0;
        int TNos = 0;
        int TMt = 0;
        int OnUpdateSumNos = 0;
        int OnUpdateSumCost = 0;

        if (dt1.Rows.Count > 0)
        {
            for (int IShiftRw = 0; IShiftRw < dt1.Rows.Count; IShiftRw++)
            {
                Response.Write("<tr Font-Bold='true' align='center' >");

                Response.Write("<td colspan='17'>");
                Response.Write("<a>" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("</tr>");

                Response.Write("<tr >");


                if (DeptMst.Rows.Count > 0)
                {
                    for (int IDeptRw = 0; IDeptRw < DeptMst.Rows.Count; IDeptRw++)
                    {
                        Response.Write("<td>");
                        Response.Write("<a>" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "</a>");
                        Response.Write("</td>");
                        string Shift = "";
                        if (dt1.Rows[IShiftRw]["ShiftDesc"].ToString() == "GENERAL")
                        { Shift = "Cost"; }
                        else if (dt1.Rows[IShiftRw]["ShiftDesc"].ToString() == "SHIFT1") { Shift = "Shift1Cost"; }
                        else if (dt1.Rows[IShiftRw]["ShiftDesc"].ToString() == "SHIFT2") { Shift = "Shift2Cost"; }
                        else if (dt1.Rows[IShiftRw]["ShiftDesc"].ToString() == "SHIFT3") { Shift = "Shift3Cost"; }
                        else if (dt1.Rows[IShiftRw]["ShiftDesc"].ToString() == "SHIFT4") { Shift = "SHIFT4COST"; }
                        else if (dt1.Rows[IShiftRw]["ShiftDesc"].ToString() == "SHIFT5") { Shift = "SHIFT5COST"; }
                        //if (IShiftRw < 4)
                        //{
                        //SSQL = "";
                        //SSQL = "select SUM(CAST(" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + " as decimal(18,0))) as Nos,SUM(CASE when CAST(" + Shift + " as decimal(18,0))>1 then  CAST(Cost as decimal(18,0)) else 0 end) as Cost from LabourAllotment_Mst where LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "'";
                        //SSQL = SSQL + " and DeptCode='" + DeptMst.Rows[IDeptRw]["DeptCode"].ToString() + "' group by DeptCode";

                        SSQL = "";
                        SSQL = "select SUM(CAST(" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + " as decimal(18,0))) as Nos,SUM(CAST(" + Shift + " as decimal(18,0)))as Cost from LabourAllotment_Mst where LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "'";
                        SSQL = SSQL + " and DeptCode='" + DeptMst.Rows[IDeptRw]["DeptCode"].ToString() + "' group by DeptCode";
                        LbrAllotmst = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (LbrAllotmst.Rows.Count > 0)
                            {
                                Response.Write("<td>");
                                Response.Write("<a>" + LbrAllotmst.Rows[0]["Nos"].ToString() + "</a>");
                                Response.Write("</td>");
                                //SumNos + = LbrAllotmst.Rows[0]["Nos"];
                                OptimumSumNos += Convert.ToInt32(LbrAllotmst.Rows[0]["Nos"].ToString());

                                Response.Write("<td>");
                                Response.Write("<a>" + LbrAllotmst.Rows[0]["Cost"].ToString() + "</a>");
                                Response.Write("</td>");
                                OptimumSumCost += Convert.ToInt32(LbrAllotmst.Rows[0]["Cost"].ToString());
                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("<a>0</a>");
                                Response.Write("</td>");

                                Response.Write("<td>");
                                Response.Write("<a>0</a>");
                                Response.Write("</td>");
                            }
                        //}
                        //else
                        //{
                        //    Response.Write("<td>");
                        //    Response.Write("<a>0</a>");
                        //    Response.Write("</td>");

                        //    Response.Write("<td>");
                        //    Response.Write("<a>0</a>");
                        //    Response.Write("</td>");
                        //}

                        SSQL = "";
                        SSQL = "Select isnull(CAST(sum(LD.Present) as decimal(18,0)),0) as Nos,ISNULL(SUM(CASE when LD.Present!='0.0' then (CASE when EM.CatName='STAFF' then CAST(CAST(CAST(LD.BasicSalary as decimal(18,0))/26 as decimal(18,0)) * LD.Present as decimal(18,0)) else  CAST(CAST(LD.BasicSalary as decimal(18,0))*LD.Present as decimal(18,0))   end) else 0 end),0) as Cost ";
                        SSQL = SSQL + " from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID=LD.MachineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and LD.DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "' and LD.Shift='" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + "'";
                        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(str_date).ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And LD.Shift!='Leave' AND LD.Wages!='Weekly Labour'";
                        Attn = objdata.RptEmployeeMultipleDetails(SSQL);


                        if (Attn.Rows.Count > 0)
                        {
                            Response.Write("<td>");
                            Response.Write("<a>" + Attn.Rows[0]["Nos"].ToString() + "</a>");
                            Response.Write("</td>");
                            OnSumNos += Convert.ToInt32(Attn.Rows[0]["Nos"].ToString());

                            Response.Write("<td>");
                            Response.Write("<a>" + Attn.Rows[0]["Cost"].ToString() + "</a>");
                            Response.Write("</td>");
                            OnSumCost += Convert.ToInt32(Attn.Rows[0]["Cost"].ToString());
                        }
                        else
                        {
                            Response.Write("<td>");
                            Response.Write("<a>0</a>");
                            Response.Write("</td>");

                            Response.Write("<td>");
                            Response.Write("<a>0</a>");
                            Response.Write("</td>");
                        }

                        if (GradeMst.Rows.Count > 0)
                        {
                            int DaysInc = 15;
                            for (int IGrRow = 0; IGrRow < GradeMst.Rows.Count; IGrRow++)
                            {

                                SSQL = "";
                                SSQL = "Select LD.MachineID from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                                SSQL = SSQL + " where LD.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "' and LD.DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "'";
                                SSQL = SSQL + " and LD.Grade='" + GradeMst.Rows[IGrRow]["GradeID"].ToString() + "' and LD.Shift='" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + "'";
                                SSQL = SSQL + " and CONVERT(date,LD.Attn_Date,103)=CONVERT(date,'" + str_date + "',103)";
                                SSQL = SSQL + " AND (EM.IsActive='Yes' or CONVERT(date,EM.dor,103)>=CONVERT(date,'" + str_date + "',103))";
                                SSQL = SSQL + " And  (Present_Absent='Present' or Present_Absent='Half Day') and LD.Wages!='Weekly Labour'";

                                DataTable dtAttn = new DataTable();
                                dtAttn = objdata.RptEmployeeMultipleDetails(SSQL);
                                DataTable dt_Less15 = new DataTable();
                                dt_Less15.Columns.Add("MachineID");
                                DataTable dt_More15 = new DataTable();
                                dt_More15.Columns.Add("MachineID");
                                if (dtAttn.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtAttn.Rows.Count; i++)
                                    {
                                        SSQL = "";
                                        SSQL = "Select MachineID from Logtime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " and MachineID='" + dtAttn.Rows[i]["MachineID"] + "' ";
                                        SSQL = SSQL + " and DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "'";
                                        SSQL = SSQL + " and Grade='" + GradeMst.Rows[IGrRow]["GradeID"].ToString() + "' and Shift='" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + "'";
                                        SSQL = SSQL + " and CONVERT(date,Attn_Date,103)<=CONVERT(date,'" + str_date + "',103)";
                                        SSQL = SSQL + " And (Present_Absent='Present' or Present_Absent='Half Day') and Wages!='Weekly Labour' group by MachineID having SUm(Present)<=15";
                                        DataTable dt_Check = new DataTable();
                                        dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (dt_Check.Rows.Count > 0)
                                        {
                                            dt_Less15.Rows.Add();
                                            dt_Less15.Rows[dt_Less15.Rows.Count - 1]["MachineID"] = dt_Check.Rows[0]["MachineID"].ToString();
                                        }

                                        SSQL = "";
                                        SSQL = "Select MachineID from Logtime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " and MachineID='" + dtAttn.Rows[i]["MachineID"] + "' ";
                                        SSQL = SSQL + " and DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "'";
                                        SSQL = SSQL + " and Grade='" + GradeMst.Rows[IGrRow]["GradeID"].ToString() + "' and Shift='" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + "'";
                                        SSQL = SSQL + " and CONVERT(date,Attn_Date,103)<=CONVERT(date,'" + str_date + "',103)";
                                        SSQL = SSQL + " And (Present_Absent='Present' or Present_Absent='Half Day') and Wages!='Weekly Labour' group by MachineID having SUm(Present)>15";

                                        dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (dt_Check.Rows.Count > 0)
                                        {
                                            dt_More15.Rows.Add();
                                            dt_More15.Rows[dt_More15.Rows.Count - 1]["MachineID"] = dt_Check.Rows[0]["MachineID"].ToString();
                                        }
                                    }
                                }

                                if (dtAttn.Rows.Count > 0)
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a>" + dt_Less15.Rows.Count + "</a>");
                                    Response.Write("</td>");
                                   TNos += Convert.ToInt32(dt_Less15.Rows.Count);


                                    Response.Write("<td>");
                                    Response.Write("<a>" + dt_More15.Rows.Count + "</a>");
                                    Response.Write("</td>");
                                    TMt += Convert.ToInt32(dt_More15.Rows.Count);

                                }
                                else
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a>0</a>");
                                    Response.Write("</td>");

                                    Response.Write("<td>");
                                    Response.Write("<a>0</a>");
                                    Response.Write("</td>");
                                }

                                DaysInc = DaysInc + 15;
                            }
                        }
                        SSQL = "";
                        SSQL = " Select Count(LD.Present) as Nos,isnull(Sum(   CASE when EM.CatName='STAFF' then  CAST(CAST(CAST(LD.BasicSalary as decimal(18,0))/26 as decimal(18,0))*LD.Present as decimal(18,0))  else CAST(CAST(LD.BasicSalary as decimal(18,0))*LD.Present as decimal(18,0)) end  ),0) as Cost from Logtime_days LD";
                        SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                        SSQL = SSQL + " where (EM.IsActive='Yes' or Convert(date,EM.DOR,103)>=Convert(date,'" + str_date + "',103))";
                        SSQL = SSQL + " and LD.Shift='" + dt1.Rows[IShiftRw]["ShiftDesc"].ToString() + "' and LD.DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "'";
                        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)>= CONVERT(DATETIME,'01/" + Convert.ToDateTime(str_date).ToString("MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(str_date).ToString("dd/MM/yyyy") + "',103) and LD.Wages!='Weekly Labour'";
                        DataTable AttnUpdate = new DataTable();
                        AttnUpdate = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (AttnUpdate.Rows.Count > 0)
                        {
                            Response.Write("<td>");
                            Response.Write("<a>" + AttnUpdate.Rows[0]["Nos"].ToString() + "</a>");
                            Response.Write("</td>");
                            OnUpdateSumNos += Convert.ToInt32(AttnUpdate.Rows[0]["Nos"].ToString());

                            Response.Write("<td>");
                            Response.Write("<a>" + AttnUpdate.Rows[0]["Cost"].ToString() + "</a>");
                            Response.Write("</td>");
                            OnUpdateSumCost += Convert.ToInt32(AttnUpdate.Rows[0]["Cost"].ToString());

                        }
                        else
                        {
                            Response.Write("<td>");
                            Response.Write("<a>0</a>");
                            Response.Write("</td>");

                            Response.Write("<td>");
                            Response.Write("<a>0</a>");
                            Response.Write("</td>");
                        }

                        Response.Write("</tr>");

                      
                    }

                }
               
            }
        }

        Response.Write("<tr Font-Bold='true'>");

        Response.Write("<td>");
        Response.Write("<a>TRAINEES COST</a>");
        Response.Write("</td>");

        DataTable Attn2 = new DataTable();

        SSQL = "";
        SSQL = "  select sum(cast(traineenos as decimal(18,0))) as Nos,sum(cast(traineecost as decimal(18,0))) as Cost from LabourAllotment_Mst  ";
        //SSQL = SSQL + " from LogTime_Days LD inner join Employee_Mst EM on  EM.MachineID=LD.MachineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode ";
        //SSQL = SSQL + " COLLATE DATABASE_DEFAULT   inner join LabourAllotment_Mst LA on LA.DeptCode=EM.DeptCode inner join MstGrade GD on gd.GradeID=em.Grade";
        SSQL = SSQL + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Attn2 = objdata.RptEmployeeMultipleDetails(SSQL);

        Response.Write("<td>");
        Response.Write("<a>" + Attn2.Rows[0]["Nos"].ToString() + "</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>" + Attn2.Rows[0]["Cost"].ToString() + "</a>");
        Response.Write("</td>");

        SSQL = "";
        SSQL = "Select isnull(CAST(sum(LD.Present) as decimal(18,0)),0) as Nos,ISNULL(SUM(CASE when LD.Present!='0.0' then (CASE when EM.CatName='STAFF'  ";
        SSQL = SSQL + "  then CAST(CAST(CAST(LD.BasicSalary as decimal(18,0))/26 as decimal(18,0)) * LD.Present as decimal(18,0)) else  CAST(CAST(LD.BasicSalary ";
        SSQL = SSQL + "   as decimal(18,0))*LD.Present as decimal(18,0))   end) else 0 end),0) as Cost ";
        SSQL = SSQL + " from LogTime_Days LD inner join Employee_Mst EM on  EM.MachineID=LD.MachineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode ";
        SSQL = SSQL + " COLLATE DATABASE_DEFAULT   inner join LabourAllotment_Mst LA on LA.DeptCode=EM.DeptCode ";
        SSQL = SSQL + " inner join MstGrade GD on gd.GradeID=em.Grade  where (GD.GradeName='T' or GD.GradeName='T+') and  ";
        SSQL = SSQL + " LD.Present!='0.0' ";
        SSQL = SSQL + " and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "' And CONVERT(DATETIME,LD.Attn_Date_Str,103) =CONVERT(DATETIME,'" + Convert.ToDateTime(str_date).ToString("dd/MM/yyyy") + "',103) And LD.Shift!='Leave'";
        Attn = objdata.RptEmployeeMultipleDetails(SSQL);

        Response.Write("<td>");
        Response.Write("<a>" + Attn.Rows[0]["Nos"].ToString() + "</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>" + Attn.Rows[0]["Cost"].ToString() + "</a>");
        Response.Write("</td>");
        if (GradeMst.Rows.Count > 0)
        {

            for (int IGrRow = 0; IGrRow < GradeMst.Rows.Count; IGrRow++)
            {

                Response.Write("<td>");
                Response.Write("<a></a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a></a>");
                Response.Write("</td>");
            }
        }

        SSQL = "";
        SSQL = " Select Count(LD.Present) as Nos,isnull(Sum(CASE when EM.CatName='STAFF' then  CAST(CAST(CAST(LD.BasicSalary as decimal(18,0))/26 as decimal(18,0))*LD.Present as decimal(18,0))  else CAST(CAST(LD.BasicSalary as decimal(18,0))*LD.Present as decimal(18,0)) end  ),0) as Cost from Logtime_days LD";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT inner join MstGrade GD on gd.GradeID=em.Grade ";
        SSQL = SSQL + " where (EM.IsActive='Yes' or Convert(date,EM.DOR,103)>=Convert(date,'" + str_date + "',103))";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)>= CONVERT(DATETIME,'01/" + Convert.ToDateTime(str_date).ToString("MM/yyyy") + "',103)";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(str_date).ToString("dd/MM/yyyy") + "',103) AND (GD.GradeName='T' or GD.GradeName='T+') and LD.Present!='0.0'";
        DataTable AttnUpdate2 = new DataTable();
        AttnUpdate2 = objdata.RptEmployeeMultipleDetails(SSQL);

        Response.Write("<td>");
        Response.Write("<a>" + AttnUpdate2.Rows[0]["Nos"].ToString() + "</a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a>" + AttnUpdate2.Rows[0]["Cost"].ToString() + "</a>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' >");

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>GRAND TOTAL</a>");
        Response.Write("</td>");

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>" + OptimumSumNos + "</a>");
        Response.Write("</td>");

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>" + OptimumSumCost + "</a>");
        Response.Write("</td>");

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>" + OnSumNos + "</a>");
        Response.Write("</td>");

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>" + OnSumCost + "</a>");
        Response.Write("</td>");

      

        for (int IGrRow = 0; IGrRow < GradeMst.Rows.Count; IGrRow++)
        {
            Response.Write("<td Font-Bold='true'>");
            Response.Write("<a></a>");
            Response.Write("</td Font-Bold='true'>");

            Response.Write("<td Font-Bold='true'>");
            Response.Write("<a></a>");
            Response.Write("</td Font-Bold='true'>");
        }

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>" + OnUpdateSumNos + "</a>");
        Response.Write("</td Font-Bold='true'>");

        Response.Write("<td Font-Bold='true'>");
        Response.Write("<a>" + OnUpdateSumCost + "</a>");
        Response.Write("</td Font-Bold='true'>");


        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true'>");

        Response.Write("<td>");
        Response.Write("<a>Weekly</a>");
        Response.Write("</td>");


        SSQL = "";
        SSQL = "  select sum(cast(Weeknos as decimal(18,0))) as Nos,sum(cast(WeekCost as decimal(18,0))) as Cost from LabourAllotment_Mst  ";
        //SSQL = SSQL + " from LogTime_Days LD inner join Employee_Mst EM on  EM.MachineID=LD.MachineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode ";
        //SSQL = SSQL + " COLLATE DATABASE_DEFAULT   inner join LabourAllotment_Mst LA on LA.DeptCode=EM.DeptCode inner join MstGrade GD on gd.GradeID=em.Grade";
        SSQL = SSQL + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        LbrAllotmst = objdata.RptEmployeeMultipleDetails(SSQL);
        if (LbrAllotmst.Rows.Count > 0)
        {
            Response.Write("<td>");
            Response.Write("<a>" + LbrAllotmst.Rows[0]["Nos"].ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a>" + LbrAllotmst.Rows[0]["Cost"].ToString() + "</a>");
            Response.Write("</td>");
        }
        else
        {
            Response.Write("<td>");
            Response.Write("<a>0</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a>0</a>");
            Response.Write("</td>");
        }


        SSQL = "";
        SSQL = "Select isnull(CAST(sum(LD.Present) as decimal(18,0)),0) as Nos,ISNULL(SUM(CASE when LD.Present!='0.0' then (CASE when EM.CatName='STAFF' then CAST(CAST(CAST(LD.BasicSalary as decimal(18,0))/26 as decimal(18,0)) * LD.Present as decimal(18,0)) else  CAST(CAST(LD.BasicSalary as decimal(18,0))*LD.Present as decimal(18,0))   end) else 0 end),0) as Cost ";
        SSQL = SSQL + " from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID=LD.MachineID and Em.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(str_date).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And LD.Shift!='Leave' And EM.Wages='Weekly Labour'";
        SSQL = SSQL + " And LD.DeptName!='CONTRACT'";
        Attn = objdata.RptEmployeeMultipleDetails(SSQL);


        if (Attn.Rows.Count > 0)
        {
            Response.Write("<td>");
            Response.Write("<a>" + Attn.Rows[0]["Nos"].ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a>" + Attn.Rows[0]["Cost"].ToString() + "</a>");
            Response.Write("</td>");
        }
        else
        {
            Response.Write("<td>");
            Response.Write("<a>0</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a>0</a>");
            Response.Write("</td>");
        }

        if (GradeMst.Rows.Count > 0)
        {
            int DaysInc = 15;
            for (int IGrRow = 0; IGrRow < GradeMst.Rows.Count; IGrRow++)
            {

                SSQL = "";
                SSQL = "Select LD.MachineID from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
                SSQL = SSQL + " where LD.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " and CONVERT(date,LD.Attn_Date,103)=CONVERT(date,'" + str_date + "',103)";
                SSQL = SSQL + " AND (EM.IsActive='Yes' or CONVERT(date,EM.dor,103)>=CONVERT(date,'" + str_date + "',103))";
                SSQL = SSQL + " And  (Present_Absent='Present' or Present_Absent='Half Day') AND EM.Wages='Weekly Labour'";

                DataTable dtAttn = new DataTable();
                dtAttn = objdata.RptEmployeeMultipleDetails(SSQL);
                DataTable dt_Less15 = new DataTable();
                dt_Less15.Columns.Add("MachineID");
                DataTable dt_More15 = new DataTable();
                dt_More15.Columns.Add("MachineID");
                if (dtAttn.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAttn.Rows.Count; i++)
                    {
                        SSQL = "";
                        SSQL = "Select MachineID from Logtime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and MachineID='" + dtAttn.Rows[i]["MachineID"] + "' ";
                        //SSQL = SSQL + " and DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "'";
                        SSQL = SSQL + " and Grade='" + GradeMst.Rows[IGrRow]["GradeID"].ToString() + "'";
                        SSQL = SSQL + " and CONVERT(date,Attn_Date,103)<=CONVERT(date,'" + str_date + "',103)";
                        SSQL = SSQL + " And (Present_Absent='Present' or Present_Absent='Half Day') AND Wages='Weekly Labour' group by MachineID having SUm(Present)<=15";
                        DataTable dt_Check = new DataTable();
                        dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_Check.Rows.Count > 0)
                        {
                            dt_Less15.Rows.Add();
                            dt_Less15.Rows[dt_Less15.Rows.Count - 1]["MachineID"] = dt_Check.Rows[0]["MachineID"].ToString();
                        }

                        SSQL = "";
                        SSQL = "Select MachineID from Logtime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and MachineID='" + dtAttn.Rows[i]["MachineID"] + "' ";
                       // SSQL = SSQL + " and DeptName='" + DeptMst.Rows[IDeptRw]["DeptName"].ToString() + "'";
                        SSQL = SSQL + " and Grade='" + GradeMst.Rows[IGrRow]["GradeID"].ToString() + "'";
                        SSQL = SSQL + " and CONVERT(date,Attn_Date,103)<=CONVERT(date,'" + str_date + "',103)";
                        SSQL = SSQL + " And (Present_Absent='Present' or Present_Absent='Half Day') AND Wages='Weekly Labour' group by MachineID having SUm(Present)>15";

                        dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_Check.Rows.Count > 0)
                        {
                            dt_More15.Rows.Add();
                            dt_More15.Rows[dt_More15.Rows.Count - 1]["MachineID"] = dt_Check.Rows[0]["MachineID"].ToString();
                        }
                    }
                }

                if (dtAttn.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write("<a>" + dt_Less15.Rows.Count + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a>" + dt_More15.Rows.Count + "</a>");
                    Response.Write("</td>");

                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");
                }

                DaysInc = DaysInc + 15;
            }
        }


        SSQL = "";
        SSQL = " Select Count(LD.Present) as Nos,isnull(Sum(   CASE when EM.CatName='STAFF' then  CAST(CAST(CAST(LD.BasicSalary as decimal(18,0))/26 as decimal(18,0))*LD.Present as decimal(18,0))  else CAST(CAST(LD.BasicSalary as decimal(18,0))*LD.Present as decimal(18,0)) end  ),0) as Cost from Logtime_days LD";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID=LD.MachineID and EM.LocCode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive='Yes' or Convert(date,EM.DOR,103)>=Convert(date,'" + str_date + "',103))";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)>= CONVERT(DATETIME,'01/" + Convert.ToDateTime(str_date).ToString("MM/yyyy") + "',103)";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(str_date).ToString("dd/MM/yyyy") + "',103) And EM.Wages='Weekly Labour'";
        DataTable AttnUpdate1 = new DataTable();
        AttnUpdate1 = objdata.RptEmployeeMultipleDetails(SSQL);
        if (AttnUpdate1.Rows.Count > 0)
        {
            Response.Write("<td>");
            Response.Write("<a>" + AttnUpdate1.Rows[0]["Nos"].ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a>" + AttnUpdate1.Rows[0]["Cost"].ToString() + "</a>");
            Response.Write("</td>");

        }
        else
        {
            Response.Write("<td>");
            Response.Write("<a>0</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a>0</a>");
            Response.Write("</td>");
        }


        Response.Write("</tr>");
        Response.Write("</table>");
        //Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
    }


    public void GetHRConsolidatesDeptAndGradeWise(string Date3_str)
    {
        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable GradeMst = new DataTable();

        AutoDTable.Columns.Add("UNIT");
        AutoDTable.Columns.Add("DEPT");
        AutoDTable.Columns.Add("Allot");
        AutoDTable.Columns.Add("ON");
        AutoDTable.Columns.Add("OFF");
        AutoDTable.Columns.Add("Tot");

        AutoDTable.Columns.Add("T");
        AutoDTable.Columns.Add("T+");
        AutoDTable.Columns.Add("A");
        AutoDTable.Columns.Add("A+");
        AutoDTable.Columns.Add("A++");
        AutoDTable.Columns.Add("A+*");
        AutoDTable.Columns.Add("Total");

        AutoDTable.Columns.Add("OFFT");
        AutoDTable.Columns.Add("OFFT+");
        AutoDTable.Columns.Add("OFFA");
        AutoDTable.Columns.Add("OFFA+");
        AutoDTable.Columns.Add("OFFA++");
        AutoDTable.Columns.Add("OFFA+*");
        AutoDTable.Columns.Add("OFFTotal");

        //SSQL = "select DeptName,DeptCode,'0' as Allot_Total,SUM(One) as One,SUM(Two) as Two,SUM(Three) as Three,SUM(Four) as Four,SUM(Five) as Five,SUM(Six) as Six,";
        //SSQL = SSQL + " (SUM(One) + SUM(Two)+ SUM(Three) + SUM(Four) + SUM(Five)+SUM(Six)) as Final_Total from (select DM.DeptName,DM.DeptCode,DM.ShortCode,CASE WHEN GM.GradeName = 'T' THEN count(EM.EmpNo) else 0 END AS One,";
        //SSQL = SSQL + " CASE WHEN GM.GradeName = 'T+' THEN count(EM.EmpNo) else 0 END AS Two,CASE WHEN GM.GradeName = 'A' THEN count(EM.EmpNo) else 0 END AS Three,";
        //SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+' THEN count(EM.EmpNo) else 0 END AS Four,CASE WHEN GM.GradeName = 'A++' THEN count(EM.EmpNo) else 0 END AS Five,";
        //SSQL = SSQL + " CASE WHEN GM.GradeName = 'A+*' THEN count(EM.EmpNo) else 0 END AS Six";
        //SSQL = SSQL + " from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeName and EM.LocCode=GM.LocCode inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode";
        //SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        //SSQL = SSQL + " and GM.CompCode='" + SessionCcode.ToString() + "' And GM.LocCode='" + SessionLcode.ToString() + "'";
        //SSQL = SSQL + " and DM.CompCode='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "' and IsActive='Yes'";
        //SSQL = SSQL + " group by DM.DeptName,GM.GradeName,DM.DeptCode,DM.ShortCode ) as PPV group by DeptName,DeptCode,ShortCode Order by ShortCode Asc";
        SSQL = "select UNIT,DeptName,cast(Allot as int) as Allot,isnull(Present,0) as Present,isnull(Absen,0) as Absen,isnull(Ptot1,0) as Ptot1,isnull(Ptot2,0) as Ptot2,isnull(Ptot3,0) as Ptot3,isnull(Ptot4,0) as Ptot4,isnull(Ptot5,0) as Ptot5,isnull(Ptot6,0) as Ptot6,isnull(Atot1,0) as Atot1,";
        SSQL = SSQL + " isnull(Atot2,0) as Atot2,isnull(Atot3,0) as Atot3,isnull(Atot4,0) as Atot4,isnull(Atot5,0) as Atot5,isnull(Atot6,0) as Atot6 from ";
        SSQL = SSQL + " (select ld.LocCode as UNIT, dm.DeptName as DeptName,EAL.Total as Allot,case when ld.Present_Absent='Present' then count(ld.Present_Absent) end as Present,";
        SSQL = SSQL + " case when ld.Present_Absent = 'Absent' then count(ld.Present_Absent) end as Absen,case when ld.Grade = 'A+*' and ld.Present_Absent = 'Present' then count(ld.grade) end as Ptot1,";
        SSQL = SSQL + " case when ld.Grade = 'A++' and ld.Present_Absent = 'Present' then count(ld.grade) end as Ptot2,case when ld.Grade = 'A+' and ld.Present_Absent = 'Present' then count(ld.grade) end as Ptot3,";
        SSQL = SSQL + " case when ld.Grade = 'A' and ld.Present_Absent = 'Present' then count(ld.grade) end as Ptot4,case when ld.Grade = 'T+' and ld.Present_Absent = 'Present' then count(ld.grade) end as Ptot5,";
        SSQL = SSQL + " case when ld.Grade = 'T' and ld.Present_Absent = 'Present' then count(ld.grade) end as Ptot6,case when ld.Grade = 'A+*' and ld.Present_Absent = 'Absent' then count(ld.grade) end as Atot1,";
        SSQL = SSQL + " case when ld.Grade = 'A++' and ld.Present_Absent = 'Absent' then count(ld.grade) end as Atot2,case when ld.Grade = 'A+' and ld.Present_Absent = 'Absent' then count(ld.grade) end as Atot3,";
        SSQL = SSQL + " case when ld.Grade = 'A' and ld.Present_Absent = 'Absent' then count(ld.grade) end as Atot4,case when ld.Grade = 'T+' and ld.Present_Absent = 'Absent' then count(ld.grade) end as Atot5,";
        SSQL = SSQL + " case when ld.Grade = 'T' and ld.Present_Absent = 'Absent' then count(ld.grade) end as Atot6 from Employee_Mst EM inner join MstGrade GM on EM.Grade = GM.GradeName and EM.LocCode = GM.LocCode";
        SSQL = SSQL + " inner join Department_Mst DM  on DM.DeptCode = EM.DeptCode and DM.LocCode = EM.LocCode inner join LabourAllotment_Mst EAL on EAL.DeptCode=em.DeptCode and EAL.LocCode = em.LocCode inner join LogTime_Days ld on ld.MachineID=em.MachineID and ld.LocCode = em.LocCode";
        SSQL = SSQL + "  where IsActive = 'Yes' and Attn_Date_str = '" + Date3_str + "'group by ld.LocCode, dm.DeptName,EAL.Total,LD.Present_Absent,ld.Grade) as Grade";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        //SSQL = "select EAL.Emp_Count as Allot,ld.Present_Absent as Attend from Employee_Mst EM inner join MstGrade GM on EM.Grade=GM.GradeName and EM.LocCode=GM.LocCode inner join Department_Mst DM ";
        //SSQL = SSQL + " on DM.DeptCode=EM.DeptCode inner join Employee_Allotment_Mst EAL on EAL.LocCode=em.LocCode inner join LogTime_Days ld on ld.LocCode=em.LocCode ";
        //SSQL = SSQL + " and DM.LocCode=EM.LocCode where IsActive='Yes' and Attn_Date_str='" + Date3_str + "' group by EAL.Emp_Count,LD.Present_Absent ";
        //dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        //SSQL = "select  em.Emp_Count as Allot,ld.Present_Absent as Attend from LogTime_Days ld inner join Employee_Allotment_Mst em on ld.LocCode=em.LocCode where Attn_Date_str='" + Date3_str + "'";
        //dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["DeptName"].ToString();
                //int Presentcnt = 0;
                //int Absent = 0;
                //for (int j = 0; j < dt1.Rows.Count; j++)
                //{
                //    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Allot"] = dt1.Rows[j]["Allot"].ToString();
                //    string Attendance = dt1.Rows[j]["Attend"].ToString();
                //    if (Attendance == "Present")
                //    {
                //        Presentcnt++;
                //    }
                //    else if (Attendance == "Absent" || Attendance == "Leave")
                //    {
                //        Absent++;
                //    }
                //}
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["UNIT"] = dt.Rows[i]["UNIT"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Allot"] = dt.Rows[i]["Allot"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ON"] = dt.Rows[i]["Present"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFF"] = dt.Rows[i]["Absen"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Tot"] = Convert.ToInt32(dt.Rows[i]["Present"].ToString()) + Convert.ToInt32(dt.Rows[i]["Absen"].ToString());

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = dt.Rows[i]["Ptot6"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = dt.Rows[i]["Ptot5"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = dt.Rows[i]["Ptot4"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+"] = dt.Rows[i]["Ptot3"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++"] = dt.Rows[i]["Ptot2"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A+*"] = dt.Rows[i]["Ptot1"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Convert.ToInt32(dt.Rows[i]["Ptot1"].ToString()) + Convert.ToInt32(dt.Rows[i]["Ptot2"].ToString()) + Convert.ToInt32(dt.Rows[i]["Ptot3"].ToString()) + Convert.ToInt32(dt.Rows[i]["Ptot4"].ToString()) + Convert.ToInt32(dt.Rows[i]["Ptot5"].ToString()) + Convert.ToInt32(dt.Rows[i]["Ptot6"].ToString());

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFT"] = dt.Rows[i]["Atot6"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFT+"] = dt.Rows[i]["Atot5"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFA"] = dt.Rows[i]["Atot4"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFA+"] = dt.Rows[i]["Atot3"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFA++"] = dt.Rows[i]["Atot2"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFA+*"] = dt.Rows[i]["Atot1"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OFFTotal"] = Convert.ToInt32(dt.Rows[i]["Atot1"].ToString()) + Convert.ToInt32(dt.Rows[i]["Atot2"].ToString()) + Convert.ToInt32(dt.Rows[i]["Atot3"].ToString()) + Convert.ToInt32(dt.Rows[i]["Atot4"].ToString()) + Convert.ToInt32(dt.Rows[i]["Atot5"].ToString()) + Convert.ToInt32(dt.Rows[i]["Atot6"].ToString());

            }

            int TotCount = AutoDTable.Rows.Count;

            double Sum = 0;
            double Sum1 = 0;
            double Sum2 = 0;
            double Sum3 = 0;
            double Sum4 = 0;
            double Sum5 = 0;
            double Sum6 = 0;
            double Sum7 = 0;
            double Sum8 = 0;
            double Sum9 = 0;
            double Sum10 = 0;

            for (int m = 0; m < AutoDTable.Rows.Count; m++)
            {
                Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
                Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
                Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
                Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
                Sum4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
                Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
                Sum7 += Convert.ToDouble(AutoDTable.Rows[m]["A+*"].ToString());
                Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
                Sum8 += Convert.ToDouble(AutoDTable.Rows[m]["ON"].ToString());
                Sum9 += Convert.ToDouble(AutoDTable.Rows[m]["OFF"].ToString());
                Sum10 += Convert.ToDouble(AutoDTable.Rows[m]["Tot"].ToString());
            }
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[TotCount]["DEPT"] = "Total";
            AutoDTable.Rows[TotCount]["Allot"] = Sum;
            AutoDTable.Rows[TotCount]["T"] = Sum1;
            AutoDTable.Rows[TotCount]["T+"] = Sum2;
            AutoDTable.Rows[TotCount]["A"] = Sum3;
            AutoDTable.Rows[TotCount]["A+"] = Sum4;
            AutoDTable.Rows[TotCount]["A++"] = Sum5;
            AutoDTable.Rows[TotCount]["A+*"] = Sum7;
            AutoDTable.Rows[TotCount]["Total"] = Sum6;
            AutoDTable.Rows[TotCount]["ON"] = Sum8;
            AutoDTable.Rows[TotCount]["OFF"] = Sum9;
            AutoDTable.Rows[TotCount]["Tot"] = Sum10;

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HR CONSOLIDATED - DEPT AND GRADE WISE REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='38'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + Date3_str + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='38'>");
            Response.Write("<a style=\"font-weight:bold\"> HR CONSOLIDATED - DEPT AND GRADE WISE REPORT &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + Date3_str + "</a>");
            Response.Write("</td>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\"> ON DUTY &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("<a style=\"font-weight:bold\">" + Date3_str + "</a>");
            Response.Write("</td>");
            Response.Write("<td colspan='14'>");
            Response.Write("<a style=\"font-weight:bold\"> OFF DUTY &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("<a style=\"font-weight:bold\">" + Date3_str + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
            Response.Write("</td>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Allot</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">ON</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">OFF</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">T</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">T+</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A+</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A++</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A+*</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");
            Response.Write("</td>");


            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">T</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">T+</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A+</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A++</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A+*</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");


            Response.Write("</td>");
            Response.Write("</tr>");

            string Unit = "";
            string Unit1 = "";
            for (int k = 0; k < AutoDTable.Rows.Count; k++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
                Response.Write("</td>");

                //Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Allot"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["ON"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFF"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Tot"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A+*"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
                Response.Write("</td>");


                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFT+"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFA"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFA+"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFA++"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFA+*"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OFFTotal"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("</tr>");

            }

            Response.Write("</table>");
            // Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!')", true);
        }

    }
    //New Function By Suresh
    private void GetHR_Consolidate_Rpt(string FromDate)
    {
        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        DataTable dtPresent = new DataTable();
        DataTable dtAbsent = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable GradeMst = new DataTable();

        AutoDTable.Columns.Add("DEPT");
        AutoDTable.Columns.Add("ALLOT");
        AutoDTable.Columns.Add("OPG");
        AutoDTable.Columns.Add("REL");
        AutoDTable.Columns.Add("ADD");
        AutoDTable.Columns.Add("Total");

        //JOINERS
        AutoDTable.Columns.Add("REF");
        AutoDTable.Columns.Add("DEPTS");
        AutoDTable.Columns.Add("IDNO");
        AutoDTable.Columns.Add("NAME");
        AutoDTable.Columns.Add("GR");
        AutoDTable.Columns.Add("RJOI");
        AutoDTable.Columns.Add("NJOI");
        AutoDTable.Columns.Add("REMARKS");

               
        SSQL = "select dm.DeptName as Dept,Sum(LM.Total) as Total from Department_Mst DM inner join LabourAllotment_Mst lm on lm.DeptCode=dm.DeptCode and lm.LocCode=DM.LocCode";
        SSQL = SSQL + " where   dm.CompCode='" + SessionCcode.ToString() + "' and dm.LocCode='" + SessionLcode.ToString() + "' group by DM.DeptName";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = dt.Rows[i]["Dept"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = dt.Rows[i]["Total"].ToString();

                //Present
                //SSQL = "select Count(*) as Present from LogTime_Days where Present_Absent='present' and DeptName='" + dt.Rows[i]["Dept"] + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
                //SSQL = SSQL + " and Convert(date,Attn_Date,103)=Convert(date,'" + FromDate + "',103)";
                SSQL = " select A.DeptName,Count(*) as Present from Employee_Mst A inner join Department_Mst B on A.DeptCode=b.DeptCode ";
                SSQL = SSQL + " where A.CompCode='" + SessionCcode.ToString() + "' and A.LocCode='" + SessionLcode.ToString() + "' and A.IsActive='Yes' and A.DeptName='"+ dt.Rows[i]["Dept"].ToString() + "' group by A.DeptName";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt1.Rows.Count > 0)
                {
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = dt1.Rows[0]["Present"].ToString();
                }
                else
                {
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OPG"] = "0";
                }
                //Relieve
                SSQL = "select count(em.MachineID) as REL from Employee_Mst em inner join Department_Mst dm on em.DeptCode=dm.DeptCode and EM.locCode=DM.LocCode";
                SSQL = SSQL + " where   Convert(date,EM.dor,103)=Convert(date,'" + FromDate + "',103) and dm.DeptName='" + dt.Rows[i]["Dept"] + "' and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "' ";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt2.Rows.Count > 0)
                {
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = dt2.Rows[0]["REL"].ToString();
                }
                else
                {
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REL"] = "0";
                }
                //Added
                SSQL = "select count(em.MachineID) as ADDED from Employee_Mst em inner join Department_Mst dm on em.DeptCode=dm.DeptCode and EM.locCode=DM.LocCode";
                SSQL = SSQL + " where  Convert(date,em.DOJ,103)=Convert(DateTime,'" + FromDate + "',103) and dm.DeptName='" + dt.Rows[i]["Dept"] + "' and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "' ";
                dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt3.Rows.Count > 0)
                {
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = dt3.Rows[0]["ADDED"].ToString();
                }
                else
                {
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ADD"] = "0";
                }

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Convert.ToInt32(AutoDTable.Rows[i]["OPG"]) + Convert.ToInt32(AutoDTable.Rows[i]["REL"]) + Convert.ToInt32(AutoDTable.Rows[i]["ADD"]);
            }
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HR 24X7 CONSOLIDATED REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + " ** " + SessionLcode.ToString() + "</a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\"> RPT No.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HR DAILY REPORT --- " + FromDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\"> HR 24X7 CONSOLIDATED REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">DEPATMENT</a>");
            Response.Write("</td>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">OPG</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">REL</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">ADD</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
            Response.Write("</td>");

            Response.Write("<td rowspan='1' colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">HOSPITAL REPORT(HINDI BOYS)</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">ID.NO</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NAME</a>");
            Response.Write("</td>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">REASON</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");

            Response.Write("</tr>");


            for (int k = 0; k < AutoDTable.Rows.Count; k++)
            {

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["DEPT"].ToString() + " </a>");
                Response.Write("</td>");

                //Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["ALLOT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["OPG"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["REL"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["ADD"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["TOTAL"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("</tr>");


            }
            //JOINERS
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">JOINERS</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">REF</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">IDNO</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NAME</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">GRADE</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">RJOI</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NJOI</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">REMARKS</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            SSQL = "select em.Refrence as Ref,em.DeptName as Dept,em.EmpNo as IDNO,em.FirstName as Fname,GM.GradeName as Grade,Convert(varchar,em.RejoinDate,105) as RJoin,Convert(varchar,em.DOJ,105) as NJoin ";
            SSQL = SSQL + " from Employee_Mst em inner join Department_Mst dm on em.DeptCode=dm.DeptCode inner join MstGrade GM on GM.GradeID=EM.Grade and GM.LocCode COLLATE DATABASE_DEFAULT=EM.LocCode COLLATE DATABASE_DEFAULT";
            SSQL = SSQL + " where  IsActive='Yes' and em.DOJ=Convert(DateTime,'" + FromDate + "',103) and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {

                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Ref"].ToString() + " </a>");
                    Response.Write("</td>");

                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Dept"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["IDNO"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Fname"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Grade"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["RJoin"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["NJoin"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");
                }
            }


            //RELIEVERS
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">RELIEVERS</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">REF</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">IDNO</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NAME</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">GRADE</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">RJOI</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NJOI</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">REMARKS</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            SSQL = "select em.Refrence as Ref,em.DeptName as Dept,em.EmpNo as IDNO,em.FirstName as Fname,em.Grade as Grade,em.RelieveDate as RJoin,em.DOJ as NJoin";
            SSQL = SSQL + " from Employee_Mst em inner join Department_Mst dm on em.DeptCode=dm.DeptCode";
            SSQL = SSQL + " where IsActive='Yes' and em.RelieveDate='" + FromDate + "' and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "'";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {

                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Ref"].ToString() + " </a>");
                    Response.Write("</td>");

                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Dept"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["IDNO"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Fname"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td >");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Grade"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["RJoin"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["NJoin"].ToString() + " </a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");
                }
            }
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">NIL</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            //24X7 Working Report
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">24X7 WORKING REPORT</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">SOURCE</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">AVAIL</a>");
            Response.Write("</td>");


            SSQL = "Select distinct(ShiftDesc) from Shift_Mst where compcode='" + SessionCcode.ToString() + "' and Loccode='" + SessionLcode.ToString() + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Response.Write("<td rowspan='1'>");
                Response.Write("<a style=\"font-weight:bold\">" + dt.Rows[i]["ShiftDesc"] + "</a>");
                Response.Write("<table border='1'>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">PRESENT</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a style=\"font-weight:bold\">ABSENT</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

            }



            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">REMARKS</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            SSQL = "select am.AgentName as AgentName,Count(*) as Avail from Employee_Mst em inner join AgentMst am on em.AgentID=am.AgentID";
            SSQL = SSQL + " where  IsActive='Yes' and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "' group by am.AgentName";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {

                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    Response.Write("<table border=1>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["AgentName"].ToString() + " </a>");
                    Response.Write("</td>");

                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:normal\">" + dt.Rows[k]["Avail"].ToString() + " </a>");
                    Response.Write("</td>");

                    string Shift = "";
                    SSQL = "Select distinct(ShiftDesc) from Shift_Mst where LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "'";
                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        Response.Write("<td rowspan='1'>");
                        Response.Write("<table border=1>");
                        Response.Write("<tr Font-Bold='true' align='center'>");
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            //Leader Allot
                            Shift = dt1.Rows[i]["ShiftDesc"].ToString();
                            //SSQL = "select " + Shift + " as shift,sum(cast(count as int)) as Total from LabourAllotment_Mst where  CompCode='" + SessionCcode.ToString() + "' and LocCode ='" + SessionLcode.ToString() + "' and Shift='" + Shift + "' and AgentName='" + dt.Rows[k]["AgentName"].ToString() + "' group by shift";
                            SSQL = "";
                            SSQL = "Select Total from LeaderAllotment_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and AgentName='" + dt.Rows[k]["AgentName"].ToString() + "'";
                            dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                            //Present
                            SSQL = "select count(*) as Present from Employee_Mst em ";
                            SSQL = SSQL + " inner join LogTime_Days ld on ld.MachineID=em.MachineID  where  IsActive='Yes'";
                            SSQL = SSQL + " and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "' and ld.Present >='0.5' and Convert(date,Attn_Date,103)=Convert(date,'" + FromDate + "',103) and Shift='" + Shift + "' and em.AgentName='" + dt.Rows[k]["AgentName"].ToString() + "' ";
                            dtPresent = objdata.RptEmployeeMultipleDetails(SSQL);
                            //Absent
                            SSQL = "select count(*) as Absent from Employee_Mst em ";
                            SSQL = SSQL + " inner join LogTime_Days ld on ld.MachineID=em.MachineID where  IsActive='Yes'";
                            SSQL = SSQL + " and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "' and Convert(date,Attn_Date,103)=Convert(date,'" + FromDate + "',103) and ld.Present='0.0' and Shift='" + Shift + "' and em.AgentName='" + dt.Rows[k]["AgentName"].ToString() + "'";
                            dtAbsent = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt2.Rows.Count > 0)
                            {

                                Response.Write("<td>");
                                Response.Write(dt2.Rows[0]["Total"].ToString());
                                Response.Write("</td>");
                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("0");
                                Response.Write("</td>");
                            }
                            if (dtPresent.Rows.Count > 0)
                            {
                                Response.Write("<td>");
                                Response.Write(dtPresent.Rows[0]["Present"].ToString());
                                Response.Write("</td>");
                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("0");
                                Response.Write("</td>");
                            }
                            if (dtAbsent.Rows.Count > 0)
                            {
                                Response.Write("<td>");
                                Response.Write(dtAbsent.Rows[0]["Absent"].ToString());
                                Response.Write("</td>");
                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("0");
                                Response.Write("</td>");
                            }

                        }
                        Response.Write("<td>");
                        Response.Write("");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("</td>");
                    }
                }

                Response.Write("</tr>");

                //GRADE FUNCTION
                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='8'>");
                Response.Write("<a style=\"font-weight:bold\">BP & OE PRESENT GRADE REPORT 24X7</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">DEPT</a>");
                Response.Write("</td>");
                SSQL = "select GradeName as Grade from MstGrade WHERE LocCode='" + SessionLcode.ToString() + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    Response.Write("<td rowspan='1'>");
                    Response.Write("<table border='1'>");
                    Response.Write("<tr>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + dt.Rows[i]["Grade"] + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.Write("</td>");
                    Response.Write("</td>");
                }
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">REMARKS</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                //GETTING THE RECORDS

                SSQL = " select Deptname as Dept from Department_Mst  where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {


                        Response.Write("<table border='1'>");
                        Response.Write("<tr>");

                        Response.Write("<td>");
                        Response.Write(dt.Rows[i]["Dept"].ToString());
                        Response.Write("</td>");

                        SSQL = "select GradeName as Grade,GradeID from MstGrade WHERE CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
                        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

                        int GradeTotal = 0;
                        for (int l = 0; l < dt2.Rows.Count; l++)
                        {
                            //SSQL = "select count(*) as Total from Employee_Mst em inner join Department_Mst dm on";
                            //SSQL = SSQL + " dm.DeptCode=em.DeptCode and dm.LocCode=em.LocCode where  em.IsActive='Yes' and em.CompCode='" + SessionCcode.ToString() + "' and em.LocCode='" + SessionLcode.ToString() + "' and dm.deptname='" + dt.Rows[i]["Dept"].ToString() + "' and em.grade='" + dt2.Rows[l]["GradeID"].ToString() + "' ";
                            SSQL = "Select Cast(isnull(Sum(Present),0) as decimal(18,0)) as Total from Logtime_Days where DeptName='" + dt.Rows[i]["Dept"].ToString() + "' and Grade='" + dt2.Rows[l]["GradeID"].ToString() + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and Convert(date,Attn_Date,103)=Convert(date,'" + FromDate + "',103)";
                            dt3 = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt3.Rows.Count > 0)
                            {

                                Response.Write("<td>");
                                // Response.Write("0");
                                Response.Write(dt3.Rows[0]["Total"].ToString());
                                Response.Write("</td>");
                                GradeTotal += Convert.ToInt32(dt3.Rows[0]["Total"].ToString());

                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("0");
                                Response.Write("</td>");
                            }

                        }
                        Response.Write("<td>");
                        Response.Write(GradeTotal);
                        Response.Write("</td>");
                        Response.Write("<td>");

                        Response.Write("</td>");

                    }


                    Response.Write("</tr>");
                    Response.Write("</table>");

                    //CANTEEN STATUS
                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<td colspan='9'>");
                    Response.Write("<a style=\"font-weight:bold\">CANTEEN STATUS</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">CANTEEN REPORT</a>");
                    Response.Write("</td>");

                    SSQL = "select shift,CanteenName as cname from MstCanteenItem where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
                    dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        Response.Write("<td rowspan='1'>");
                        Response.Write(dt2.Rows[i]["cname"]);
                        Response.Write("<table border='1'>");
                        Response.Write("<tr>");
                        Response.Write("<td>");
                        Response.Write(dt2.Rows[i]["shift"]);
                        Response.Write("</td>");

                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("</td>");

                    }
                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.Write("<table border='1'>");
                    Response.Write("<tr>");
                    Response.Write("<td>");
                    Response.Write("QUALITY");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td>");
                    Response.Write("QUANTITY");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("</tr>");
                    Response.Write("<td>");
                    Response.Write("WASTAGES");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");
                }

            }

            Response.Write("</table>");

            // Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!')", true);
        }
    }


    private void GetOEDeptAvailable_Report(string Dept)
    {

        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable GradeMst = new DataTable();

        AutoDTable.Columns.Add("UNIT");
        AutoDTable.Columns.Add("AGENT");
        AutoDTable.Columns.Add("ALLOT");
        AutoDTable.Columns.Add("REQUIRED");
        AutoDTable.Columns.Add("AVAILABLE");
        AutoDTable.Columns.Add("SHORTAGE");
        AutoDTable.Columns.Add("A++A+A");
        AutoDTable.Columns.Add("T+");
        AutoDTable.Columns.Add("T");
        AutoDTable.Columns.Add("Total");
        AutoDTable.Columns.Add("TotalAvailable");

        SSQL = "select AM.LocCode as UNIT,Am.AgentName AS AGENT,cast(EL.Total as int) as ALLOT,el.Required as REQUIRE,";
        SSQL = SSQL + " count(em.EmpNo) as AVAILABLE,el.Required-Count(em.EmpNo) as SHORTAGE,dm.DeptName from Employee_Mst EM";
        SSQL = SSQL + " inner join LabourAllotment_Mst as El on El.LocCode=em.LocCode and El.DeptCode=EM.DeptCode inner join MstGrade GM on EM.Grade=GM.GradeID ";
        SSQL = SSQL + " and EM.LocCode=GM.LocCode inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode ";
        SSQL = SSQL + " inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=El.LocCode  where  IsActive='Yes' and dm.DeptName='" + Dept + "' group by AM.LocCode,Am.AgentName,EL.Total,el.Required,dm.DeptName ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "select em.Grade as GradePer,count(EM.DeptName) as totalcount,dm.DeptName  from Employee_Mst EM inner join";
        SSQL = SSQL + " LabourAllotment_Mst as El on El.LocCode=em.LocCode inner join MstGrade GM on EM.Grade=GM.GradeID and EM.LocCode=GM.LocCode ";
        SSQL = SSQL + " inner join Department_Mst DM on DM.DeptCode=EM.DeptCode and DM.LocCode=EM.LocCode inner join AgentMst AM on AM.AgentID=EM.AgentID and AM.LocCode=El.LocCode ";
        SSQL = SSQL + " where  IsActive='Yes' and dm.DeptName='" + Dept + "' group by em.Grade,em.AgentName,dm.DeptName ";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["UNIT"] = dt.Rows[i]["ALLOT"].ToString();

                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    string grade = dt1.Rows[j]["GradePer"].ToString();

                    if (grade == "A++" || grade == "A+" || grade == "A")
                    {

                        tot1++;

                    }
                    else if (grade == "T+")
                    {
                        tot2++;
                    }
                    else if (grade == "T")
                    {
                        tot3++;
                    }

                    Total = tot1 + tot2 + tot3;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalAvailable"] = dt1.Rows[j]["totalcount"].ToString();
                }

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["UNIT"] = dt.Rows[i]["UNIT"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AGENT"] = dt.Rows[i]["AGENT"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ALLOT"] = dt.Rows[i]["ALLOT"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["REQUIRED"] = dt.Rows[i]["REQUIRE"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AVAILABLE"] = dt.Rows[i]["AVAILABLE"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SHORTAGE"] = dt.Rows[i]["SHORTAGE"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A++A+A"] = tot1;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T+"] = tot2;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["T"] = tot3;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Total;


            }

            //int TotCount = AutoDTable.Rows.Count;

            //double Sum = 0;
            //double Sum1 = 0;
            //double Sum2 = 0;
            //double Sum3 = 0;
            //double Sum4 = 0;
            //double Sum5 = 0;
            //double Sum6 = 0;
            //double Sum7 = 0;

            //for (int m = 0; m < AutoDTable.Rows.Count; m++)
            //{
            //    Sum += Convert.ToDouble(AutoDTable.Rows[m]["Allot"].ToString());
            //    Sum1 += Convert.ToDouble(AutoDTable.Rows[m]["T"].ToString());
            //    Sum2 += Convert.ToDouble(AutoDTable.Rows[m]["T+"].ToString());
            //    Sum3 += Convert.ToDouble(AutoDTable.Rows[m]["A"].ToString());
            //    f4 += Convert.ToDouble(AutoDTable.Rows[m]["A+"].ToString());
            //    Sum5 += Convert.ToDouble(AutoDTable.Rows[m]["A++"].ToString());
            //    Sum7 += Convert.ToDouble(AutoDTable.Rows[m]["A+*"].ToString());
            //    Sum6 += Convert.ToDouble(AutoDTable.Rows[m]["Total"].ToString());
            //}
            //AutoDTable.NewRow();
            //AutoDTable.Rows.Add();
            //AutoDTable.Rows[TotCount]["DEPT"] = "Total";
            //AutoDTable.Rows[TotCount]["Allot"] = Sum;
            //AutoDTable.Rows[TotCount]["T"] = Sum1;
            //AutoDTable.Rows[TotCount]["T+"] = Sum2;
            //AutoDTable.Rows[TotCount]["A"] = Sum3;
            //AutoDTable.Rows[TotCount]["A+"] = Sum4;
            //AutoDTable.Rows[TotCount]["A++"] = Sum5;
            //AutoDTable.Rows[TotCount]["A+*"] = Sum7;
            //AutoDTable.Rows[TotCount]["Total"] = Sum6;

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=DEPT AVAILABLE REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='22'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode.ToString() + "</a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\">" + SessionLcode.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='22'>");
            Response.Write("<a style=\"font-weight:bold\"> DEPT AVAILABLE REPORT &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">UNIT</a>");
            Response.Write("</td>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">AGENT</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">ALLOT</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">REQUIRED</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">AVAILABLE</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">SHORTAGE</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">A++,A+,A</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">T+</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\">T</a>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' >");
            Response.Write("<a style=\"font-weight:bold\">TotalAvailable</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            for (int k = 0; k < AutoDTable.Rows.Count; k++)
            {

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["UNIT"].ToString() + " </a>");
                Response.Write("</td>");

                //Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["AGENT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["ALLOT"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["REQUIRED"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["AVAILABLE"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["SHORTAGE"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["A++A+A"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T+"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["T"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["Total"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:normal\">" + AutoDTable.Rows[k]["TotalAvailable"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("</tr>");


            }

            Response.Write("</table>");

            // Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!')", true);
        }
    }

    private void GetHR_Strength_ALL_UNIT_RPT(string date)
    {
        SSQL = "";
        SSQL = "Select distinct AgentName from AgentMst";
        DataTable dt_agent = new DataTable();
        dt_agent = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "Select LocCode from Location_Mst ";
        DataTable Dt_Loc = new DataTable();
        Dt_Loc = objdata.RptEmployeeMultipleDetails(SSQL);

        string attachment = "attachment;filename=STRENGTH ALL UNITS REPORT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">MURUGAN TEXTILES HR TEAMS     DATE : " + date + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">STRENGTH ALL UNITS REPORT</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td rowspan='1'>");
        Response.Write("<a style=\"font-weight:bold\">AGENT</a>");
        Response.Write("</td>");


        for (int UnitRW = 0; UnitRW < Dt_Loc.Rows.Count; UnitRW++)
        {

            Response.Write("<td rowspan='1'>");
            Response.Write("<a style=\"font-weight:bold\">" + Dt_Loc.Rows[UnitRW]["LocCode"].ToString() + "</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">AVIL</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">ON</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">OFF</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">TOT</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");

        }

        Response.Write("<td rowspan='1'>");
        Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">AVIL</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">ON</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">OFF</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">TOT</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");

        Response.Write("<table border='1'>");

        decimal availTotal = 0;
        decimal onTotal = 0;
        decimal offTotal = 0;
        decimal MainTotal = 0;
        decimal totalTotal = 0;

        for (int AgeRw = 0; AgeRw < dt_agent.Rows.Count; AgeRw++)
        {
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + dt_agent.Rows[AgeRw]["AgentName"].ToString() + "</a>");
            Response.Write("</td>");
            availTotal = 0;
            onTotal = 0;
            offTotal = 0;
            MainTotal = 0;

            for (int UnitRw = 0; UnitRw < Dt_Loc.Rows.Count; UnitRw++)
            {
                DataTable dt_get = new DataTable();
                totalTotal = 0;
                SSQL = "";
                SSQL = "Select isnull(Count(MachineID),0) as Cnt from Employee_Mst   where CompCode='" + SessionCcode + "' and LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' and AgentName='" + dt_agent.Rows[AgeRw]["AgentName"].ToString() + "'";
                SSQL = SSQL + " and (isActive='Yes' or Convert(date,DOR,103)>=Convert(date,'" + date + "',103)) group by LocCode";
                dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_get.Rows.Count > 0)
                {

                    availTotal = availTotal + Convert.ToDecimal(dt_get.Rows[0]["Cnt"].ToString());
                    Response.Write("<td>");
                    Response.Write(dt_get.Rows[0]["Cnt"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }


                SSQL = "";
                SSQL = "select Sum(LD.Present) as Cnt from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID and LD.LocCode=EM.LocCode where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' and EM.AgentName='" + dt_agent.Rows[AgeRw]["AgentName"].ToString() + "'";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) and LD.Present>0 group by LD.LocCode";
                dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_get.Rows.Count > 0)
                {
                    totalTotal = totalTotal + Convert.ToDecimal(dt_get.Rows[0]["Cnt"].ToString());
                    onTotal = onTotal + Convert.ToDecimal(dt_get.Rows[0]["Cnt"].ToString());
                    Response.Write("<td>");
                    Response.Write(dt_get.Rows[0]["Cnt"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }


                SSQL = "";
                SSQL = "select Count(LD.Present) as Cnt from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID and LD.LocCode=EM.LocCode where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' and EM.AgentName='" + dt_agent.Rows[AgeRw]["AgentName"].ToString() + "'";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) and LD.Present=0 group by LD.LocCode";
                dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_get.Rows.Count > 0)
                {
                    totalTotal = totalTotal + Convert.ToDecimal(dt_get.Rows[0]["Cnt"].ToString());
                    offTotal = offTotal + Convert.ToDecimal(dt_get.Rows[0]["Cnt"].ToString());
                    Response.Write("<td>");
                    Response.Write(dt_get.Rows[0]["Cnt"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }

                MainTotal = MainTotal + totalTotal;
                Response.Write("<td>");
                Response.Write(totalTotal);
                Response.Write("</td>");

            }

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + availTotal + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + onTotal + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + offTotal + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + MainTotal + "</a>");
            Response.Write("</td>");

            Response.Write("</tr>");
        }
        Response.End();
        Response.Clear();
    }

    private void GetHR_COCKPIT_RPT(string date)
    {
        AutoDTable.Columns.Add("UnitName");
        AutoDTable.Columns.Add("Fix_Emp", typeof(decimal));
        AutoDTable.Columns.Add("Fix_Cost", typeof(decimal));
        AutoDTable.Columns.Add("Fix_Prod", typeof(decimal));


        AutoDTable.Columns.Add("OnDT_Emp_Pre", typeof(decimal));
        AutoDTable.Columns.Add("OnDT_Cost", typeof(decimal));
        AutoDTable.Columns.Add("OnDT_Prod", typeof(decimal));

        AutoDTable.Columns.Add("Mon_Fix_Cost", typeof(decimal));
        AutoDTable.Columns.Add("Mon_Fix_Prod", typeof(decimal));

        AutoDTable.Columns.Add("Mon_Cost", typeof(decimal));
        AutoDTable.Columns.Add("Mon_Prod", typeof(decimal));

        SSQL = "";
        SSQL = "Select LocCode from Location_Mst ";
        DataTable Dt_Loc = new DataTable();
        Dt_Loc = objdata.RptEmployeeMultipleDetails(SSQL);
        DataRow dr;

        for (int UnitRw = 0; UnitRw < Dt_Loc.Rows.Count; UnitRw++)
        {
            dr = AutoDTable.NewRow();
            dr["UnitName"] = Dt_Loc.Rows[UnitRw]["LocCode"].ToString();

            //For OPT
            SSQL = "";
            SSQL = "select Lcode, Sum(nos) as nos ,Sum(cost) as cost,sum(Production) as production from (";

            //SSQL = SSQL + "Select LocCode as Lcode,Count(MachineID) as nos,Sum(CASE When EM.CatName='STAFF' then CAST(BaseSalary/26 as decimal(18,2)) when EM.CatName='LABOUR' then BaseSalary end) as Cost,0 as Production";
            //SSQL = SSQL + " from Employee_Mst as EM where LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' and (isActive='Yes' or Convert(date,DOR,103)>=Convert(date,'" + date + "',103)) group by LocCode";

            SSQL = SSQL + "Select LocCode as Lcode,SUM(Total) as nos,Sum(Cost)as Cost,0 as Production from LabourAllotment_Mst where LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "'  group by LocCode";

            SSQL = SSQL + " UNION ALL";
            SSQL = SSQL + " select LCode,0 as nos,0 as cost,Production from MstProduction where LCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "'";

            SSQL = SSQL + " ) as tabl group by Lcode";
            DataTable dt_optGet = new DataTable();
            dt_optGet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_optGet.Rows.Count > 0)
            {
                dr["Fix_Emp"] = Convert.ToDecimal(dt_optGet.Rows[0]["nos"].ToString());
                dr["Fix_Cost"] = Convert.ToDecimal(dt_optGet.Rows[0]["cost"].ToString());
                dr["Fix_Prod"] = Convert.ToDecimal(dt_optGet.Rows[0]["production"].ToString());
                //dr["Fix_Emp"] = Convert.ToDecimal(dt_optGet.Rows[0]["nos"].ToString());
            }
            else
            {
                dr["Fix_Emp"] = Convert.ToDecimal(0);
                dr["Fix_Cost"] = Convert.ToDecimal(0);
                dr["Fix_Prod"] = Convert.ToDecimal(0);
            }

            //For Update
            SSQL = "";
            SSQL = "Select Lcode, Sum(nos) as nos ,Sum(cost) as cost,sum(Production) as production from (";

            SSQL = SSQL + " Select LD.LocCode as Lcode, Sum(LD.present) as nos,Sum(CASE When EM.CatName='STAFF' then CAST(BaseSalary/26 as decimal(18,2)) when EM.CatName='LABOUR' then BaseSalary end) as Cost,0 as production";
            SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID";
            SSQL = SSQL + " where Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) and LD.LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' group by LD.LocCode";
            SSQL = SSQL + " UNION ALL";
            SSQL = SSQL + " select LCode,0 as nos,0 as cost,ProductionEarned as production from ProductionOnDate where LCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "'";
            SSQL = SSQL + " and Convert(date,Tarns_Date,103)=Convert(date,'" + date + "',103)";

            SSQL = SSQL + " ) as tabl group by Lcode";
            //   DataTable dt_optGet = new DataTable();
            dt_optGet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_optGet.Rows.Count > 0)
            {
                dr["OnDT_Emp_Pre"] = Convert.ToDecimal(dt_optGet.Rows[0]["nos"].ToString());
                dr["OnDT_Cost"] = Convert.ToDecimal(dt_optGet.Rows[0]["cost"].ToString());
                dr["OnDT_Prod"] = Convert.ToDecimal(dt_optGet.Rows[0]["production"].ToString());
                //dr["Fix_Emp"] = Convert.ToDecimal(dt_optGet.Rows[0]["nos"].ToString());
            }
            else
            {
                dr["OnDT_Emp_Pre"] = Convert.ToDecimal(0);
                dr["OnDT_Cost"] = Convert.ToDecimal(0);
                dr["OnDT_Prod"] = Convert.ToDecimal(0);
            }


            //For update Calculation
            SSQL = "";
            SSQL = "Select Lcode,Sum(cost) as cost,sum(Production * (DATEDIFF(day, '" + Convert.ToDateTime(date).ToString("yyyy/MM") + "/01', '" + Convert.ToDateTime(date).ToString("yyyy/MM/dd") + "')+1)) as production from (";
            SSQL = SSQL + " Select EM.LocCode as Lcode, (DATEDIFF(day, '" + Convert.ToDateTime(date).ToString("yyyy/MM") + "/01', '" + Convert.ToDateTime(date).ToString("yyyy/MM/dd") + "')+1) * ( CASE When EM.CatName='STAFF' then CAST(EM.BaseSalary/26 as decimal(18,2)) when EM.CatName='LABOUR' then EM.BaseSalary end) as Cost, 0 as Production";
            SSQL = SSQL + " from  Employee_Mst EM  where EM.LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' group by EM.LocCode,EM.CatName,EM.BaseSalary";


            SSQL = SSQL + " UNION ALL";
            SSQL = SSQL + " select LCode,0 as cost,Production from MstProduction where LCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "'";
            SSQL = SSQL + " ) as tabl group by Lcode";

            dt_optGet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_optGet.Rows.Count > 0)
            {
                dr["Mon_Fix_Cost"] = Convert.ToDecimal(dt_optGet.Rows[0]["cost"].ToString());
                dr["Mon_Fix_Prod"] = Convert.ToDecimal(dt_optGet.Rows[0]["production"].ToString());
            }
            else
            {
                dr["Mon_Fix_Cost"] = Convert.ToDecimal(0);
                dr["Mon_Fix_Prod"] = Convert.ToDecimal(0);
            }

            //Update Cost and Production Calculation
            SSQL = "";
            SSQL = "Select Lcode,Sum(cost) as cost,sum(Production) as production from (";
            SSQL = SSQL + " Select LD.LocCode as Lcode, SUM(CASE When EM.CatName='STAFF' then CAST(EM.BaseSalary/26 as decimal(18,2)) when EM.CatName='LABOUR' then EM.BaseSalary end) as Cost, 0 as Production";
            SSQL = SSQL + " from Logtime_Days LD inner join Employee_Mst EM on EM.MachineId=LD.MachineID where EM.LocCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' and  Convert(date,Attn_date,103) BETWEEN Convert(date,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "',103) and Convert(date,'" + date + "',103) group by LD.LocCode,EM.CatName,EM.BaseSalary";

            SSQL = SSQL + " UNION ALL";
            SSQL = SSQL + " select LCode,0 as cost,ProductionEarned as Production from ProductionOnDate where LCode='" + Dt_Loc.Rows[UnitRw]["LocCode"].ToString() + "' and Convert(date,Tarns_Date,103) BETWEEN Convert(date,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "',103) and Convert(date,'" + date + "',103)";
            SSQL = SSQL + " ) as tabl group by Lcode";

            dt_optGet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_optGet.Rows.Count > 0)
            {
                dr["Mon_Cost"] = Convert.ToDecimal(dt_optGet.Rows[0]["cost"].ToString());
                dr["Mon_Prod"] = Convert.ToDecimal(dt_optGet.Rows[0]["production"].ToString());
            }
            else
            {
                dr["Mon_Cost"] = Convert.ToDecimal(0);
                dr["Mon_Prod"] = Convert.ToDecimal(0);
            }

            AutoDTable.Rows.Add(dr);
            AutoDTable.AcceptChanges();
        }
        if (AutoDTable.Rows.Count > 0)
        {
            ds.Tables.Add(AutoDTable);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("New_HR_Report/HR_CockPit_Rpt.rpt"));
            report.DataDefinition.FormulaFields["Date"].Text = "'" + date + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }

    private void GetHR_Daily_Tea_Snacks_Consolidate_All_UNIT(string date)
    {
        //For getting the Canteen Name from Database
        SSQL = "";
        SSQL = "select Distinct CanteenName from MstCanteen";
        DataTable dt_getCanteen = new DataTable();
        dt_getCanteen = objdata.RptEmployeeMultipleDetails(SSQL);

        string attachment = "attachment;filename=DAILY TEA SNACKS CONSOLIDATE ALL UNIT REPORTS.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">MURUGAN TEXTILES HR TEAMS     DATE : " + date + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">DAILY TEA & SNACKS CONSOLIDATE ALL UNITS REPORTS </a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td rowspan='1'>");
        Response.Write("<a style=\"font-weight:bold\">DETAILS</a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='1'>");
        Response.Write("<a style=\"font-weight:bold\">ONDATE OPTIMUM</a>");
        Response.Write("</td>");

        for (int CanRw = 0; CanRw < dt_getCanteen.Rows.Count; CanRw++)
        {
            Response.Write("<td rowspan='1'>");
            Response.Write("<a style=\"font-weight:bold\">ON " + dt_getCanteen.Rows[CanRw]["CanteenName"].ToString() + "</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NOS</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">COST</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");
        }

        Response.Write("<td rowspan='1'>");
        Response.Write("<a style=\"font-weight:bold\">UPDATE OPTIMUM</a>");
        Response.Write("</td>");

        for (int CanRw = 0; CanRw < dt_getCanteen.Rows.Count; CanRw++)
        {
            Response.Write("<td rowspan='1'>");
            Response.Write("<a style=\"font-weight:bold\">UP " + dt_getCanteen.Rows[CanRw]["CanteenName"].ToString() + "</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NOS</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">COST</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");
        }

        Response.Write("</tr>");
        Response.Write("</table>");

        //For Getting data from Database
        SSQL = "";
        SSQL = "Select LocCode from Location_Mst ";
        DataTable Dt_Loc = new DataTable();
        Dt_Loc = objdata.RptEmployeeMultipleDetails(SSQL);

        Response.Write("<table border='1'>");
        for (int UniRw = 0; UniRw < Dt_Loc.Rows.Count; UniRw++)
        {
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "</a>");
            Response.Write("</td>");

            SSQL = "";
            SSQL = "Select SUM(LD.Present*(TSM.Amount)) as OnDateCost from Employee_mst EM inner join Logtime_Days LD on LD.MachineID=EM.MachineID and LD.CompCode=Em.CompCode and LD.LocCode=EM.LocCode ";
            SSQL = SSQL + " inner join MstTeaSnacks TSM on TSM.CanteenName=EM.SnacksOperator and TSM.Shift=LD.Shift ";
            SSQL = SSQL + " Where EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
            SSQL = SSQL + " and EM.EligibleSnacks='1'";
            SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) group by EM.LocCode";
            DataTable dt_OndateoptCost = new DataTable();
            dt_OndateoptCost = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_OndateoptCost.Rows.Count > 0)
            {
                Response.Write("<td>");
                Response.Write(dt_OndateoptCost.Rows[0]["OnDateCost"].ToString());
                Response.Write("</td>");
            }
            else
            {
                Response.Write("<td>");
                Response.Write("0");
                Response.Write("</td>");
            }


            for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
            {
                SSQL = "";
                SSQL = "Select SUM(LD.Present) as Present,SUM(LD.Present * (TSM.Amount)) as Cost from Employee_Mst EM inner join LogTime_Days LD";
                SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode  ";
                SSQL = SSQL + " inner join MstTeaSnacks TSM on TSM.CanteenName=EM.SnacksOperator and TSM.Shift=LD.Shift ";
                SSQL = SSQL + " where TSM.CanteenName='" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "'";
                SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
                SSQL = SSQL + " and EM.EligibleSnacks='1'";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) group by EM.LocCode";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write(dt.Rows[0]["Present"].ToString());
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write(dt.Rows[0]["Cost"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }
            }

            SSQL = "";
            SSQL = "Select SUM(LD.Present*(TSM.Amount)) as UpDateCost from Employee_mst EM inner join Logtime_Days LD on LD.MachineID=EM.MachineID and LD.CompCode=EM.CompCode and LD.LocCode=EM.LocCode";
            SSQL = SSQL + " inner join MstTeaSnacks TSM on TSM.CanteenName=EM.SnacksOperator and TSM.Shift=LD.Shift";
            SSQL = SSQL + " Where EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
            SSQL = SSQL + " and EM.EligibleSnacks='1'";
            SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)>=Convert(date,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "',103)";
            SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)<=Convert(date,'" + date + "',103) group by EM.LocCode";
            DataTable dt_UPdateoptCost = new DataTable();
            dt_UPdateoptCost = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_UPdateoptCost.Rows.Count > 0)
            {
                Response.Write("<td>");
                Response.Write(dt_UPdateoptCost.Rows[0]["UpDateCost"].ToString());
                Response.Write("</td>");
            }
            else
            {
                Response.Write("<td>");
                Response.Write("0");
                Response.Write("</td>");
            }

            for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
            {
                SSQL = "";
                SSQL = "Select SUM(LD.Present) as Present,SUM(LD.Present * (TSM.Amount)) as Cost from Employee_Mst EM inner join LogTime_Days LD";
                SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode ";
                SSQL = SSQL + " inner join MstTeaSnacks TSM on TSM.CanteenName=EM.SnacksOperator and TSM.Shift=LD.Shift";
                SSQL = SSQL + " where TSM.CanteenName='" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "'";
                SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
                SSQL = SSQL + " and EM.EligibleSnacks='1'";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)>=Convert(date,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "',103)";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) group by EM.LocCode";
                DataTable dt_updateCanteen = new DataTable();
                dt_updateCanteen = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_updateCanteen.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write(dt_updateCanteen.Rows[0]["Present"].ToString());
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write(dt_updateCanteen.Rows[0]["Cost"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
        Response.Clear();
    }

    private void GetHR_Canteen_Consolidate_All_UNIT(string date)
    {

        SSQL = "";
        SSQL = "select Distinct CanteenNameOpt from MstCanteenOpt";
        DataTable dt_getCanopt = new DataTable();
        dt_getCanopt = objdata.RptEmployeeMultipleDetails(SSQL);

        //For getting the Canteen Name from Database
        SSQL = "";
        SSQL = "select Distinct CanteenName from MstCanteen";
        DataTable dt_getCanteen = new DataTable();
        dt_getCanteen = objdata.RptEmployeeMultipleDetails(SSQL);

        string attachment = "attachment;filename=CANTEEN CONSOLIDATE ALL UNIT REPORTS.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        // grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //  grid.RenderControl(htextw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">MURUGAN TEXTILES HR TEAMS     DATE : " + date + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">CANTEEN CONSOLIDATE ALL UNITS REPORTS </a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td rowspan='1'>");
        Response.Write("<a style=\"font-weight:bold\">UNITS</a>");
        Response.Write("</td>");


        // For Creating the Header of the Report


        //On Date Nos and Cost

        for (int CMRow = 0; CMRow < dt_getCanopt.Rows.Count; CMRow++)
        {
            Response.Write("<td colspan='" + (dt_getCanteen.Rows.Count * 2) + "'>");
            Response.Write("<a style=\"font-weight:bold\">" + dt_getCanopt.Rows[CMRow]["CanteenNameOpt"].ToString() + "</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
            {

                Response.Write("<td colspan='2'>");
                Response.Write("<a style=\"font-weight:bold\">" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "</a>");
                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NOS</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

            }
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");
        }
        Response.Write("<td>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
        {

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">ON DATE " + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NOS</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">COST</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");

        }
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("</td>");

        //ON Date Total
        Response.Write("<td colspan='2'>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td colspan='2' rowspan='2' >");
        Response.Write("<a style=\"font-weight:bold\">ON DATE TOTAL</a>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">NOS</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">COST</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("</td>");
        //ON Date Total END

        //Update on Separate
        Response.Write("<td colspan='" + (dt_getCanteen.Rows.Count * 2) + "'>");
        Response.Write("<a style=\"font-weight:bold\">UPDATE</a>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
        {

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "</a>");
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">NOS</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">COST</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("</td>");

        }
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        //Update on Separate Header END

        //Update Total
        Response.Write("<td>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td colspan='2'>");
        Response.Write("<a style=\"font-weight:bold\">UPDATE TOTAL</a>");
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">NOS</a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\">COST</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("</td>");

        //Update Total End

        Response.Write("</tr>");
        Response.Write("</table>");
        //Heading End


        //Data Start

        SSQL = "";
        SSQL = "Select LocCode from Location_Mst ";
        DataTable Dt_Loc = new DataTable();
        Dt_Loc = objdata.RptEmployeeMultipleDetails(SSQL);

        Response.Write("<table border='1'>");
        for (int UniRw = 0; UniRw < Dt_Loc.Rows.Count; UniRw++)
        {
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "</a>");
            Response.Write("</td>");

            for (int CnoptRw = 0; CnoptRw < dt_getCanopt.Rows.Count; CnoptRw++)
            {
                for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
                {

                    SSQL = "";
                    SSQL = "Select SUM(LD.Present) as Present,SUM(LD.Present * CM.Amount) as Cost from Employee_Mst EM inner join LogTime_Days LD";
                    SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode INNER JOIN MstCanteen CM on CM.CanID=EM.CanteenName INNER JOIN MstCanteenOpt COP on COP.CanOptID=EM.CanteenOperator";
                    SSQL = SSQL + " where COP.CanteenNameOpt='" + dt_getCanopt.Rows[CnoptRw]["CanteenNameOpt"].ToString() + "' and CM.CanteenName='" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "'";
                    SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
                    SSQL = SSQL + " and EM.EligibleCateen='1'";
                    SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) group by EM.LocCode";
                    DataTable dt = new DataTable();
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt.Rows.Count > 0)
                    {
                        Response.Write("<td>");
                        Response.Write(dt.Rows[0]["Present"].ToString());
                        Response.Write("</td>");
                        Response.Write("<td>");
                        Response.Write(dt.Rows[0]["Cost"].ToString());
                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<td>");
                        Response.Write("0");
                        Response.Write("</td>");
                        Response.Write("<td>");
                        Response.Write("0");
                        Response.Write("</td>");
                    }
                }
            }

            for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
            {
                if (dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() == "SANTHOSH")
                {
                    string stop = "";
                }

                SSQL = "";
                SSQL = "Select SUM(LD.Present) as no,SUM(LD.Present * CM.Amount) as Cost from Employee_Mst EM inner join LogTime_Days LD";
                SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode INNER JOIN MstCanteen CM on CM.CanID=EM.CanteenName INNER JOIN MstCanteenOpt COP on COP.CanOptID=EM.CanteenOperator";
                SSQL = SSQL + " where CM.CanteenName='" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "'";
                SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
                SSQL = SSQL + " and EM.EligibleCateen='1'";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) group by EM.LocCode";
                DataTable dt_getOndatecantenn = new DataTable();
                dt_getOndatecantenn = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_getOndatecantenn.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write(dt_getOndatecantenn.Rows[0]["no"].ToString());
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write(dt_getOndatecantenn.Rows[0]["Cost"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }
            }

            SSQL = "";
            SSQL = "Select SUM(LD.Present) as no,SUM(LD.Present * CM.Amount) as Cost from Employee_Mst EM inner join LogTime_Days LD";
            SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode INNER JOIN MstCanteen CM on CM.CanID=EM.CanteenName INNER JOIN MstCanteenOpt COP on COP.CanOptID=EM.CanteenOperator";
            SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
            SSQL = SSQL + " and EM.EligibleCateen='1'";
            SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)=Convert(date,'" + date + "',103) group by EM.LocCode";
            DataTable dt_onDateTotal = new DataTable();
            dt_onDateTotal = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_onDateTotal.Rows.Count > 0)
            {
                Response.Write("<td>");
                Response.Write(dt_onDateTotal.Rows[0]["no"].ToString());
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write(dt_onDateTotal.Rows[0]["Cost"].ToString());
                Response.Write("</td>");
            }
            else
            {
                Response.Write("<td>");
                Response.Write("0");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("0");
                Response.Write("</td>");
            }

            for (int COPRow = 0; COPRow < dt_getCanteen.Rows.Count; COPRow++)
            {
                SSQL = "";
                SSQL = "Select SUM(LD.Present) as no,SUM(LD.Present * CM.Amount) as Cost from Employee_Mst EM inner join LogTime_Days LD";
                SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode INNER JOIN MstCanteen CM on CM.CanID=EM.CanteenName INNER JOIN MstCanteenOpt COP on COP.CanOptID=EM.CanteenOperator";
                SSQL = SSQL + " where CM.CanteenName='" + dt_getCanteen.Rows[COPRow]["CanteenName"].ToString() + "'";
                SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
                SSQL = SSQL + " and EM.EligibleCateen='1'";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)>=Convert(date,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "',103)";
                SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)<=Convert(date,'" + date + "',103) group by EM.LocCode";
                DataTable dt_getupdatecantenn = new DataTable();
                dt_getupdatecantenn = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_getupdatecantenn.Rows.Count > 0)
                {
                    Response.Write("<td>");
                    Response.Write(dt_getupdatecantenn.Rows[0]["no"].ToString());
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write(dt_getupdatecantenn.Rows[0]["Cost"].ToString());
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                    Response.Write("<td>");
                    Response.Write("0");
                    Response.Write("</td>");
                }
            }
            SSQL = "";
            SSQL = "Select SUM(LD.Present) as no,SUM(LD.Present * CM.Amount) as Cost from Employee_Mst EM inner join LogTime_Days LD";
            SSQL = SSQL + " on EM.MachineID=LD.MachineID and Em.CompCode=LD.CompCode and EM.LocCode=LD.LocCode INNER JOIN MstCanteen CM on CM.CanID=EM.CanteenName INNER JOIN MstCanteenOpt COP on COP.CanOptID=EM.CanteenOperator";
            SSQL = SSQL + " and EM.LocCode='" + Dt_Loc.Rows[UniRw]["LocCode"].ToString() + "'";
            SSQL = SSQL + " and EM.EligibleCateen='1'";
            SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)>=Convert(date,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "',103)";
            SSQL = SSQL + " and Convert(date,LD.Attn_Date,103)<=Convert(date,'" + date + "',103) group by EM.LocCode";
            DataTable dt_getupdate = new DataTable();
            dt_getupdate = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_getupdate.Rows.Count > 0)
            {
                Response.Write("<td>");
                Response.Write(dt_getupdate.Rows[0]["no"].ToString());
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write(dt_getupdate.Rows[0]["Cost"].ToString());
                Response.Write("</td>");
            }
            else
            {
                Response.Write("<td>");
                Response.Write("0");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("0");
                Response.Write("</td>");
            }

            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
        Response.Clear();
    }

    private void GetHR_CANTEEN_OPERATOR_LEADER_WISE_REPORT(string fromDate, string ToDate, string leaderName, string canOptName)
    {
        SSQL = "";
        SSQL = "Select CanteenName as CanteenOptName,AgentName as LeaderName, MachineID as EmpNo,Firstname as EmpName,CanteenDays as CanAttn,Sum(Wdays) as WorkDay,Amount as Amount,0 as DedDay from (";
        SSQL = SSQL + " Select CM.CanteenName,EM.AgentName,EM.MachineID,EM.FirstName,";
        SSQL = SSQL + " CASE when (DATEDIFF(DAY, CASE when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)<=Convert(date,'" + fromDate + "',103) then Convert(date,'" + ToDate + "',103) ";
        SSQL = SSQL + " when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)>CONVERT(date,'" + fromDate + "',103) then CONVERT(date,EM.DOJ,103) end ,Convert(date, '" + ToDate + "',103))+1)<=0 then 0 else (DATEDIFF(DAY, CASE when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)<=Convert(date,'" + fromDate + "',103) then Convert(date,'" + fromDate + "',103) ";
        SSQL = SSQL + " when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)>CONVERT(date,'" + fromDate + "',103) then CONVERT(date,EM.DOJ,103) end, Convert(date,'" + ToDate + "',103))+1) end as CanteenDays,";
        SSQL = SSQL + " LD.Present as Wdays,";
        SSQL = SSQL + " (CASE when (DATEDIFF(DAY, CASE when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)<=Convert(date,'" + fromDate + "',103) then Convert(date,'" + fromDate + "',103) ";
        SSQL = SSQL + " when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)>CONVERT(date,'" + fromDate + "',103) then CONVERT(date,EM.DOJ,103) end,Convert(date, '" + ToDate + "',103))+1)<=0 then 0 else (DATEDIFF(DAY, CASE when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)<=Convert(date,'" + fromDate + "',103) then Convert(date,'" + fromDate + "',103) ";
        SSQL = SSQL + " when EM.DOJ is not null and CONVERT(date,EM.DOJ,103)>CONVERT(date,'" + fromDate + "',103) then CONVERT(date,EM.DOJ,103) end, Convert(date,'" + ToDate + "',103))+1) end) * CM.Amount as Amount";
        SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID";
        SSQL = SSQL + " inner join MstCanteenOpt COM on COM.CanOptID=EM.CanteenOperator inner join MstCanteen CM on CM.CanID=EM.CanteenName";
        SSQL = SSQL + " where EM.EligibleCateen='1' and CONVERT(date,LD.Attn_Date,103) BETWEEN CONVERT(date,'" + fromDate + "',103) and CONVERT(date,'" + ToDate + "',103)";
        if (leaderName != "")
        {
            SSQL = SSQL + " and EM.AgentID='" + leaderName + "'";
        }
        if (canOptName != "")
        {
            SSQL = SSQL + " and EM.CanteenOperator='" + canOptName + "'";
        }
        SSQL = SSQL + " ) as tble group by CanteenName,AgentName,MachineID,FirstName,CanteenDays,Amount";

        DataTable dt_Get = new DataTable();
        dt_Get = objdata.RptEmployeeMultipleDetails(SSQL);
        ds.Tables.Add(dt_Get);
        if (dt_Get.Rows.Count > 0)
        {

            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("New_HR_Report/HR_Canteen_Operator_Leader_Wise_Rpt.rpt"));
            report.DataDefinition.FormulaFields["CompName"].Text = "'" + "MURUGAN TEXTILES" + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromDate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
            report.DataDefinition.FormulaFields["Date"].Text = "'" + DateTime.Now.ToString("dd/MM/yyyy") + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!');", true);
        }
    }

    private void GetHRConsolidate_Report_Update(string date)
    {
        AutoDTable = new DataTable();


        SSQL = "";
        SSQL = "Select LocCode as UnitName,Sum(CAST(OptHRCnt  as decimal(18,0))) as HR_CST_Fix_No,Sum(CAST(OptHRsal  as decimal(18,0))) as HR_CST_Fix_COST,Sum(CAST(ActHRCst  as decimal(18,0))) as HR_CST_Act_No,Sum(CAST(ActHRsal  as decimal(18,0))) as HR_CST_Act_COST,Sum(CAST(OptCanteenAmt  as decimal(18,0))) as HR_CAN_Fix_COST,Sum(CAST(ActCanteenCnt  as decimal(18,0))) as HR_CAN_Act_No,Sum(CAST(ActCanteenAmt  as decimal(18,0))) as HR_CAN_Act_COST,";
        SSQL = SSQL + "Sum(CAST(OptTeaSancksAmt  as decimal(18,0))) as HR_TEA_Fix_COST,Sum(CAST(ActTeaSancksCnt  as decimal(18,0))) as HR_TEA_Act_No,Sum(CAST(ActTeasnacksAmt  as decimal(18,0))) as HR_TEA_Act_COST,Sum(CAST(OptLdrCnt  as decimal(18,0))) as HR_CSN_Fix_No,Sum(CAST(OptLDAmt  as decimal(18,0))) as HR_CSN_Fix_COST,Sum(CAST(ActLdrCnt  as decimal(18,0))) as HR_CSN_Act_No,Sum(CAST(ActLdrsal  as decimal(18,0))) as HR_CSN_Act_COST,Sum(CAST(OptESICnt  as decimal(18,0))) as HR_PFESI_Fix_No,Sum(CAST(OPTESISal  as decimal(18,0))) as HR_PFESI_Fix_COST,";
        SSQL = SSQL + "Sum(CAST(ActESICnt  as decimal(18,0))) as HR_PFESI_Act_No,Sum(CAST(ActESIsal  as decimal(18,0))) as HR_PFESI_Act_COST,Sum(CAST(OptUpdate as decimal(18,0))) as HR_SUM_Fix_COST,Sum(CAST(ActUpdate as decimal(18,0))) as HR_SUM_Act_COST from(";


        //For ON Date Report

        //SSQL = SSQL + "select LocCode,Count(cnt) as OptHRCnt,Sum(Sal) as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        //SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        //SSQL = SSQL + " select LocCode, MachineID as cnt, (CASE when CatName = 'STAFF' then CAST(BaseSalary / 26 as decimal(18, 2)) else CAST(BaseSalary as decimal(18, 2)) end) as Sal from Employee_Mst";
        //SSQL = SSQL + " where (IsActive = 'Yes' or Convert(datetime, RelieveDate,103)>= CONVERT(datetime, '" + date + "', 103))) as table1  group by LocCode";

        SSQL = SSQL + "select LocCode,Sum(cnt) as OptHRCnt,Sum(Sal) as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " select LocCode, Total as cnt, Cost as Sal from LabourAllotment_Mst";
        SSQL = SSQL + " ) as table1  group by LocCode";

        SSQL = SSQL + "   UNION ALL";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,Count(cnt) as ActHRCst,Sum(Sal) as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " select LD.LocCode, LD.MachineID as cnt, (CASE when LD.CatName = 'STAFF' then CAST(EM.BaseSalary / 26 as decimal(18, 2)) else CAST(EM.BaseSalary as decimal(18, 2)) end) as Sal  from LogTime_Days LD inner join Employee_Mst EM";
        SSQL = SSQL + " on LD.MachineID = EM.MachineID and LD.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or Convert(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)= CONVERT(datetime, '" + date + "', 103)";
        SSQL = SSQL + " )as table2  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,Sum(Amt) as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        //SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        //SSQL = SSQL + " select EM.LocCode, MC.Amount as Amt  from Employee_Mst EM inner join MstCanteen MC on MC.CanID = EM.CanteenName where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103))";
        //SSQL = SSQL + " and EM.EligibleCateen = '1'";
        //SSQL = SSQL + " )as table3  group by LocCode";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,Sum(Amt) as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " select LocCode, Canteen as Amt  from LabourAllotment_Mst ";
        SSQL = SSQL + " )as table3  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,Count(cnt) as ActCanteenCnt,Sum(Amt) as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " select LD.LocCode, LD.MachineID as cnt, MC.Amount as Amt from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " inner join MstCanteen MC on MC.CanID  = Em.CanteenName  and MC.CompCode COLLATE DATABASE_DEFAULT = LD.CompCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)= CONVERT(datetime, '" + date + "', 103) and EM.EligibleCateen = '1'";
        SSQL = SSQL + " ) as table4  group by LocCode";

        SSQL = SSQL + " UNION ALL ";

        //SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,Sum(Amt) as OptTeaSancksAmt,";
        //SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        //SSQL = SSQL + " Select EM.LocCode,( CAST(MTS.SnacksAmount as Decimal(18, 2)) + Cast(MTS.TeaAmount as decimal(18, 2)) )as amt from Employee_Mst EM ";
        //SSQL = SSQL + " inner join MstTeaSnacks MTS on MTS.CanteenName = EM.SnacksOperator";
        //SSQL = SSQL + " and MTS.Shift = EM.Shift_Name  and MTS.Lcode = EM.LocCode ";
        //SSQL = SSQL + "  where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103))  and EM.EligibleSnacks = '1'";
        //SSQL = SSQL + " ) as table5  group by LocCode";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,Sum(Amt) as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " Select LocCode,TeaSnacks as amt from LabourAllotment_Mst  ";
        SSQL = SSQL + " ) as table5  group by LocCode";


        SSQL = SSQL + " UNION ALL ";

        //Tea Snacks Actual Salary
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 OptTeaSancksAmt,";
        SSQL = SSQL + " Count(cnt) as ActTeaSancksCnt,Sum(Amt) as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " Select LD.LocCode, LD.MachineID as cnt, (CAST(MTS.Amount as decimal(18,2))) as amt from Employee_Mst EM inner join LogTime_Days LD";
        SSQL = SSQL + " on LD.MachineID = EM.MachineID and LD.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " inner join MstTeaSnacks MTS on MTS.CanteenName = EM.SnacksOperator and MTS.Shift COLLATE DATABASE_DEFAULT = EM.Shift_Name COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where   (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)= CONVERT(datetime, '" + date + "', 103) and EligibleSnacks = '1'";
        SSQL = SSQL + " ) as table6  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //Leade Commision Optimum
        //SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        //SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,Count(cnt) as OptLdrCnt,Sum(Amt) as OptLDAmt, 0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        //SSQL = SSQL + " Select EM.LocCode, EM.MachineID as cnt, (CAST(AM.Shift12 as decimal(18, 2)) + CAST(AM.Shift8 as decimal(18, 2))) as amt from Employee_Mst EM inner join AgentMst AM";
        //SSQL = SSQL + " on AM.AgentID = EM.AgentID and AM.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        //SSQL = SSQL + " where  (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103)) and EM.EligibleLeader = '1'";
        //SSQL = SSQL + " ) as table7  group by LocCode";


        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,Sum(cnt) as OptLdrCnt,Sum(Amt) as OptLDAmt, 0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " Select LocCode, LrNo as cnt, LrComm as amt from LabourAllotment_Mst ";
        SSQL = SSQL + " ) as table7  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //Leader Actual
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,Count(cnt) as ActLdrCnt,Sum(Amt) as ActLdrsal ,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " Select LD.LocCode, LD.MachineID as cnt, CASE when(CAST(LD.Total_Hrs as decimal(18, 2))) < (CAST('12.00' as decimal(18, 2))) then CAST(AM.Shift8 as decimal(18, 2)) when";
        SSQL = SSQL + " (CAST(LD.Total_Hrs as decimal(18, 2))) >= (CAST('12.00' as decimal(18, 2))) then CAST(AM.Shift12 as decimal(18, 2)) else 0 end as amt from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " inner join AgentMst AM on AM.AgentID = EM.AgentID";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)= CONVERT(datetime, '" + date + "', 103) and EM.EligibleLeader = '1'";
        SSQL = SSQL + " )as table8  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //ESI / PF Optimum
        //SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        //SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal ,Count(cnt) as OptESICnt,Sum(sal) as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        //SSQL = SSQL + " select EM.LocCode, EM.MachineID as cnt, CASE when EM.CatName = 'STAFF' then CAST(CAST(EM.BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        //SSQL = SSQL + " CAST(CAST(BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) when EM.CatName = 'LABOUR' then CAST(CAST(BaseSalary as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        //SSQL = SSQL + " CAST(CAST(BaseSalary as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) else 0 end as sal from Employee_Mst EM inner join Murugan_Epay..MstESIPF EP";
        //SSQL = SSQL + " on EP.Ccode COLLATE DATABASE_DEFAULT = Em.CompCode COLLATE DATABASE_DEFAULT";
        //SSQL = SSQL + " and EP.Lcode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        //SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103))  and (EM.Eligible_ESI = '1' OR EM.Eligible_PF = '1')";
        //SSQL = SSQL + " )as table9  group by LocCode";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal ,sum(cnt) as OptESICnt,Sum(sal) as OPTESISal,0 as ActESICnt,0 as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " select LocCode, ESINo+PFNo as cnt, ESI+PF as sal from LabourAllotment_Mst";
        SSQL = SSQL + " )as table9  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //ESI / PF Actual
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal ,0 as OptESICnt,0 as OPTESISal,Count(cnt) as ActESICnt,Sum(sal) as ActESIsal,0 as OptUpdate,0 as ActUpdate from(";
        SSQL = SSQL + " select LD.LocCode, LD.MachineID as cnt, CASE when LD.CatName = 'STAFF' then CAST(CAST(EM.BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        SSQL = SSQL + " CAST(CAST(EM.BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) when EM.CatName = 'LABOUR' then CAST(CAST(BaseSalary as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        SSQL = SSQL + " CAST(CAST(EM.BaseSalary as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) else 0 end as sal from Employee_Mst EM inner join";
        SSQL = SSQL + " LogTime_Days LD on LD.MachineID = Em.MachineID and LD.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT inner join Murugan_Epay..MstESIPF EP";
        SSQL = SSQL + " on EP.Ccode COLLATE DATABASE_DEFAULT = Em.CompCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " and EP.Lcode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '" + date + "', 103)) and CONVERT(datetime, LD.Attn_Date,103) = CONVERT(datetime, '" + date + "', 103) and (EM.Eligible_ESI = '1' OR EM.Eligible_PF = '1')";
        SSQL = SSQL + " )as table10  group by LocCode";

        SSQL = SSQL + " UNION ALL ";

        //For Uptodate Report
        SSQL = SSQL + "Select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal ,0 as OptESICnt,";
        SSQL = SSQL + " 0 as OPTESISal,0 as ActESICnt,0 as ActESIsal, ";
        SSQL = SSQL + " (SUM(CAST(OptHRsal as decimal(18,2)))+SUM(CAST(OptCanteenAmt as decimal(18,2)))+SUM(CAST(OptTeaSancksAmt as decimal(18,2)))+SUM(CAST(OptLDAmt as decimal(18,2)))+SUM(CAST(OPTESISal as decimal(18,2)))) as OptUpdate,(SUM(CAST(ActHRsal as decimal(18,2)))+SUM(CAST(ActCanteenAmt as decimal(18,2)))+SUM(CAST(ActTeasnacksAmt as decimal(18,2)))+SUM(CAST(ActLdrsal as decimal(18,2)))+SUM(CAST(ActESIsal as decimal(18,2)))) as ActUpdate from ( ";
        SSQL = SSQL + " select LocCode,Count(cnt) as OptHRCnt,Sum(Sal) as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " select LocCode, MachineID as cnt, (CASE when CatName = 'STAFF' then CAST(BaseSalary / 26 as decimal(18, 2)) else CAST(BaseSalary as decimal(18, 2)) end) as Sal from Employee_Mst";
        SSQL = SSQL + " where (IsActive = 'Yes' or (Convert(datetime, RelieveDate,103)>= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)))) as table1  group by LocCode";

        SSQL = SSQL + "   UNION ALL";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,Count(cnt) as ActHRCst,Sum(Sal) as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " select LD.LocCode, LD.MachineID as cnt, (CASE when LD.CatName = 'STAFF' then CAST(EM.BaseSalary / 26 as decimal(18, 2)) else CAST(EM.BaseSalary as decimal(18, 2)) end) as Sal  from LogTime_Days LD inner join Employee_Mst EM";
        SSQL = SSQL + " on LD.MachineID = EM.MachineID and LD.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or Convert(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)) and CONVERT(datetime, LD.Attn_Date,103) <= CONVERT(datetime, '" + date + "', 103)";
        SSQL = SSQL + " )as table2  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,Sum(Amt) as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " select EM.LocCode, MC.Amount as Amt  from Employee_Mst EM inner join MstCanteen MC on MC.CanID = EM.CanteenName where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103))";
        SSQL = SSQL + " and EM.EligibleCateen = '1'";
        SSQL = SSQL + " )as table3  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,Count(cnt) as ActCanteenCnt,Sum(Amt) as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " select LD.LocCode, LD.MachineID as cnt, MC.Amount as Amt from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " inner join MstCanteen MC on MC.CanID  = Em.CanteenName  and MC.CompCode COLLATE DATABASE_DEFAULT = LD.CompCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= CONVERT(datetime,'01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)<= CONVERT(datetime, '" + date + "', 103) and EM.EligibleCateen = '1'";
        SSQL = SSQL + " ) as table4  group by LocCode";

        SSQL = SSQL + " UNION ALL ";

        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,Sum(Amt) as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " Select EM.LocCode,(CAST(MTS.Amount as decimal(18, 2)))as amt from Employee_Mst EM ";
        SSQL = SSQL + " inner join MstTeaSnacks MTS on MTS.CanteenID = EM.SnacksOperator";
        SSQL = SSQL + " and MTS.Shift = EM.Shift_Name  and MTS.Lcode = EM.LocCode ";
        SSQL = SSQL + "  where (EM.IsActive = 'Yes' or CONVERT(datetime, EM.RelieveDate, 103) >= '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "')  and EM.EligibleSnacks = '1'";
        SSQL = SSQL + " ) as table5  group by LocCode";

        SSQL = SSQL + " UNION ALL ";

        //Tea Snacks Actual Salary
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 OptTeaSancksAmt,";
        SSQL = SSQL + " Count(cnt) as ActTeaSancksCnt,Sum(Amt) as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " Select LD.LocCode, LD.MachineID as cnt, (CAST(MTS.Amount as decimal(18, 2))) as amt from Employee_Mst EM inner join LogTime_Days LD";
        SSQL = SSQL + " on LD.MachineID = EM.MachineID and LD.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " inner join MstTeaSnacks MTS on MTS.CanteenID = EM.CanteenName and MTS.Shift COLLATE DATABASE_DEFAULT = EM.Shift_Name COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where   (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)<= CONVERT(datetime, '" + date + "', 103) and EligibleSnacks = '1'";
        SSQL = SSQL + " ) as table6  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //Leade Commision Optimum
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,Count(cnt) as OptLdrCnt,Sum(Amt) as OptLDAmt, 0 as ActLdrCnt,0 as ActLdrsal,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " Select EM.LocCode, EM.MachineID as cnt, (CAST(AM.Shift12 as decimal(18, 2)) + CAST(AM.Shift8 as decimal(18, 2))) as amt from Employee_Mst EM inner join AgentMst AM";
        SSQL = SSQL + " on AM.AgentID = EM.AgentID and AM.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where  (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)) and EM.EligibleLeader = '1'";
        SSQL = SSQL + " ) as table7  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //Leader Actual
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,Count(cnt) as ActLdrCnt,Sum(Amt) as ActLdrsal ,0 as OptESICnt,0 as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " Select LD.LocCode, LD.MachineID as cnt, CASE when(CAST(LD.Total_Hrs as decimal(18, 2))) < (CAST('12.00' as decimal(18, 2))) then CAST(AM.Shift8 as decimal(18, 2)) when";
        SSQL = SSQL + " (CAST(LD.Total_Hrs as decimal(18, 2))) >= (CAST('12.00' as decimal(18, 2))) then CAST(AM.Shift12 as decimal(18, 2)) else 0 end as amt from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " inner join AgentMst AM on AM.AgentID = EM.AgentID";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)<= CONVERT(datetime, '" + date + "', 103) and EM.EligibleLeader = '1'";
        SSQL = SSQL + " )as table8  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //ESI / PF Optimum
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal ,Count(cnt) as OptESICnt,Sum(sal) as OPTESISal,0 as ActESICnt,0 as ActESIsal from(";
        SSQL = SSQL + " select EM.LocCode, EM.MachineID as cnt, CASE when EM.CatName = 'STAFF' then CAST(CAST(EM.BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        SSQL = SSQL + " CAST(CAST(BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) when EM.CatName = 'LABOUR' then CAST(CAST(BaseSalary as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        SSQL = SSQL + " CAST(CAST(BaseSalary as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) else 0 end as sal from Employee_Mst EM inner join Murugan_Epay..MstESIPF EP";
        SSQL = SSQL + " on EP.Ccode COLLATE DATABASE_DEFAULT = Em.CompCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " and EP.Lcode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103))  and (EM.Eligible_ESI = '1' OR EM.Eligible_PF = '1')";
        SSQL = SSQL + " )as table9  group by LocCode";

        SSQL = SSQL + " UNION ALL";

        //ESI / PF Actual
        SSQL = SSQL + " select LocCode,0 as OptHRCnt,0 as OptHRsal,0 as ActHRCst,0 as ActHRsal,0 as OptCanteenAmt,0 as ActCanteenCnt,0 as ActCanteenAmt,0 as OptTeaSancksAmt,";
        SSQL = SSQL + " 0 as ActTeaSancksCnt,0 as ActTeasnacksAmt ,0 as OptLdrCnt,0 as OptLDAmt,0 as ActLdrCnt,0 as ActLdrsal ,0 as OptESICnt,0 as OPTESISal,Count(cnt) as ActESICnt,Sum(sal) as ActESIsal from(";
        SSQL = SSQL + " select LD.LocCode, LD.MachineID as cnt, CASE when LD.CatName = 'STAFF' then CAST(CAST(EM.BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        SSQL = SSQL + " CAST(CAST(EM.BaseSalary as decimal(18, 2)) / CAST('26' as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) when EM.CatName = 'LABOUR' then CAST(CAST(BaseSalary as decimal(18, 2)) * EP.PF_Per as decimal(18, 2)) +";
        SSQL = SSQL + " CAST(CAST(EM.BaseSalary as decimal(18, 2)) * EP.ESI_Per as decimal(18, 2)) else 0 end as sal from Employee_Mst EM inner join";
        SSQL = SSQL + " LogTime_Days LD on LD.MachineID = Em.MachineID and LD.LocCode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT inner join Murugan_Epay..MstESIPF EP";
        SSQL = SSQL + " on EP.Ccode COLLATE DATABASE_DEFAULT = Em.CompCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " and EP.Lcode COLLATE DATABASE_DEFAULT = EM.LocCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " where (EM.IsActive = 'Yes' or CONVERT(datetime, RelieveDate, 103) >= CONVERT(datetime, '01/" + Convert.ToDateTime(date).ToString("MM/yyyy") + "', 103)) and CONVERT(datetime, LD.Attn_Date,103)<= CONVERT(datetime, '" + date + "', 103) and (EM.Eligible_ESI = '1' OR EM.Eligible_PF = '1')";
        SSQL = SSQL + " )as table10  group by LocCode";
        SSQL = SSQL + " )as tableUpdate  group by LocCode";

        SSQL = SSQL + " ) as Mailtable group by LocCode";

        DataTable dt_Get = new DataTable();
        dt_Get = objdata.RptEmployeeMultipleDetails(SSQL);
        ds.Tables.Add(dt_Get);
        if (dt_Get.Rows.Count > 0)
        {

            string CompName = "";
            string Address = "";
            SSQL = "";
            SSQL = "select * from Company_Mst ";
            DataTable dt_Comp = new DataTable();
            dt_Comp = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Comp.Rows.Count > 0)
            {
                CompName = dt_Comp.Rows[0]["CompName"].ToString();
                Address = dt_Comp.Rows[0]["Add1"].ToString();
            }

            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("New_HR_Report/HR_Consolidate_Rpt.rpt"));
            //report.DataDefinition.FormulaFields["Company"].Text = "'" + "Murugan" + "'";
            //report.DataDefinition.FormulaFields["Location"].Text = "'" + SessionLcode + "'";
            //report.DataDefinition.FormulaFields["Wages"].Text = "'" + DeptName + "'";
            //report.DataDefinition.FormulaFields["From"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["Date"].Text = "'" + date + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!');", true);
        }
    }

    public void GetHRTracer(string DeptName, string FromDate, string ToDate, string CurrentDate)
    {
        if (SessionUserType == "2")
        {
            NonAdminGetHRTracer_Changes(DeptName, FromDate, ToDate, CurrentDate);
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable IsActive = new DataTable();
            DataTable NonActive = new DataTable();
            DataTable CurrentEmp = new DataTable();

            AutoDTable.Columns.Add("S.No");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("GRD");
            AutoDTable.Columns.Add("FD", typeof(decimal));
            AutoDTable.Columns.Add("ON", typeof(decimal));
            AutoDTable.Columns.Add("AS ON", typeof(decimal));
            AutoDTable.Columns.Add("OFF", typeof(decimal));
            AutoDTable.Columns.Add("Total", typeof(decimal));

            int dayCount = (int)((Convert.ToDateTime(ToDate) - Convert.ToDateTime(FromDate)).TotalDays);
            dayCount = dayCount + 1;
            int SNo = 1;
            int intK = 0;


            double Total = 0;
            string Count_Present = "";

            string[] sp_Date = FromDate.Split('/');
            string Fd = sp_Date[0];

            DataTable da_Val = new DataTable();
            SSQL = " select distinct  SUM(LD.Present) as Present,EM.DOJ,LD.MachineID,LD.FirstName,MG.GradeName ";
            SSQL = SSQL + "  from Employee_Mst EM inner join MstGrade MG on  EM.LocCode COLLATE DATABASE_DEFAULT=MG.LocCode COLLATE DATABASE_DEFAULT and EM.Grade =MG.GradeID  ";

            SSQL = SSQL + " inner join LogTime_Days LD on LD.LocCode COLLATE DATABASE_DEFAULT=EM.LocCode COLLATE DATABASE_DEFAULT and LD.MachineID=EM.MachineID and LD.Grade COLLATE DATABASE_DEFAULT=EM.Grade COLLATE DATABASE_DEFAULT where  ";
            SSQL = SSQL + " CONVERT(DATETIME,LD.Attn_Date,103) >= CONVERT(DATETIME,'" + FromDate + "' ,103) ";
            SSQL = SSQL + "And CONVERT(DATETIME,LD.Attn_Date,103) <= CONVERT(DATETIME,'" + ToDate + "',103)";
            SSQL = SSQL + " and (EM.IsActive='Yes' or CONVERT(DATETIME,EM.dor, 103)<=CONVERT(Datetime,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)) and  EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            if (DeptName != "0")
            {
                SSQL = SSQL + " And EM.DeptCode='" + DeptName + "'";
            }
            SSQL = SSQL + "    group by EM.DOJ,LD.MachineID,LD.FirstName,MG.GradeName";
            da_Val = objdata.RptEmployeeMultipleDetails(SSQL);


            string Tot_ON = "0";
            string Tot_ASON = "0";
            string Tot_OFF = "0";
            string Tot_Total = "0";

            for (int i = 0; i < da_Val.Rows.Count; i++)
            {

                string MID = da_Val.Rows[i]["MachineID"].ToString();
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[intK]["S.No"] = SNo.ToString();
                AutoDTable.Rows[intK]["Name"] = da_Val.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[intK]["GRD"] = da_Val.Rows[i]["GradeName"].ToString();
                AutoDTable.Rows[intK]["Total"] = dayCount;

                Tot_Total = (Convert.ToInt32(Tot_Total) + Convert.ToInt32(dayCount)).ToString();

                Count_Present = da_Val.Rows[i]["Present"].ToString();
                double OFF = (Convert.ToDouble(dayCount) - Convert.ToDouble(Count_Present));

                AutoDTable.Rows[intK]["AS ON"] = da_Val.Rows[i]["Present"].ToString();
                AutoDTable.Rows[intK]["OFF"] = OFF;

                Tot_ASON = (Convert.ToDouble(Tot_ASON) + Convert.ToDouble(da_Val.Rows[i]["Present"].ToString())).ToString();
                Tot_OFF = (Convert.ToDouble(Tot_OFF) + Convert.ToDouble(OFF)).ToString();

                SSQL = "select convert(varchar(2),'" + FromDate + "',103) as FD,Present from LogTime_Days where MachineID='" + MID + "'";  //and Present='1'";
                SSQL = SSQL + " And CONVERT(date,Attn_Date,103) >= CONVERT(date,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(date,Attn_Date,103) <= CONVERT(date,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                CurrentEmp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (CurrentEmp.Rows.Count != 0)
                {
                    //AutoDTable.Rows[intK]["FD"] = Fd;
                    AutoDTable.Rows[intK]["ON"] = CurrentEmp.Rows[0]["Present"].ToString();

                    Tot_ON = (Convert.ToDouble(Tot_ON) + Convert.ToDouble(CurrentEmp.Rows[0]["Present"].ToString())).ToString();
                }
                else
                {
                    //AutoDTable.Rows[intK]["FD"] = Fd;
                    AutoDTable.Rows[intK]["ON"] = "0";

                    Tot_ON = (Convert.ToDouble(Tot_ON) + Convert.ToDouble(0)).ToString();
                }


                if (da_Val.Rows[i]["DOJ"].ToString() != "")
                {
                    if ((Convert.ToDateTime(da_Val.Rows[i]["DOJ"].ToString()) >= Convert.ToDateTime(FromDate)) && (Convert.ToDateTime(da_Val.Rows[i]["DOJ"].ToString()) <= Convert.ToDateTime(ToDate)))
                    {
                        string DOJ_FD = Convert.ToDateTime(da_Val.Rows[i]["DOJ"].ToString()).ToString("dd/MM/yyyy");
                        string[] FD_Split = DOJ_FD.Split('/');
                        AutoDTable.Rows[intK]["FD"] = FD_Split[0];
                    }
                    else
                    {
                        AutoDTable.Rows[intK]["FD"] = Fd;
                    }
                }
                else
                {
                    AutoDTable.Rows[intK]["FD"] = Fd;
                }

                SNo = SNo + 1;
                intK = intK + 1;

            }

            string str_deptname = "";

            SSQL = " Select DeptName from Department_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And DeptCode='" + DeptName + "' ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt1.Rows.Count > 0)
            {
                str_deptname = dt1.Rows[0]["Deptname"].ToString();
            }

            ds.Tables.Add(AutoDTable);
            if (AutoDTable.Rows.Count > 0)
            {


                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/HRTracer.rpt"));
                report.DataDefinition.FormulaFields["Company"].Text = "'" + "Murugan" + "'";
                report.DataDefinition.FormulaFields["Location"].Text = "'" + SessionLcode + "'";
                report.DataDefinition.FormulaFields["Wages"].Text = "'" + str_deptname + "'";
                report.DataDefinition.FormulaFields["From"].Text = "'" + FromDate + "'";
                report.DataDefinition.FormulaFields["To"].Text = "'" + ToDate + "'";
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!');", true);
            }
            //Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            //Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            //System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            ////  writer.PageEvent = new HeaderFooter();
            //document.Open();


            //PdfPTable table = new PdfPTable(5);
            //float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f };
            //table.SetWidths(widths);
            //table.WidthPercentage = 100;

            //PdfPTable table1 = new PdfPTable(8);
            //float[] widths1 = new float[] { 15f, 40f, 15f, 15f, 15f, 15f, 15f, 15f };
            //table1.SetWidths(widths1);
            //table1.WidthPercentage = 100;

            //PdfPTable table2 = new PdfPTable(4);
            //float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            //table2.SetWidths(widths2);
            //table2.WidthPercentage = 100;

            //PdfPCell cell;

            //if (AutoDTable.Rows.Count > 0)
            //{
            //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    if (DeptName != "0")
            //    {
            //        cell = PhraseCell(new Phrase(" " + dt1.Rows[0]["DeptName"] + " " + " - " + " " + " ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);
            //    }
            //    else
            //    {
            //        cell = PhraseCell(new Phrase("ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = 0;
            //        cell.Colspan = 5;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table.AddCell(cell);
            //    }

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    table.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = 0;
            //    //cell.Colspan = 2;
            //    //cell.PaddingTop = 5f;
            //    //cell.PaddingBottom = 5f;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.TOP_BORDER;
            //    //cell.Colspan = 8;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table1.AddCell(cell); Allot

            //    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);


            //    cell = PhraseCell(new Phrase("GRD", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("FD", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("ON", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("AS ON", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("OFF", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.BOTTOM_BORDER;
            //    //cell.Colspan = 8;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table1.AddCell(cell);





            //    for (int i = 0; i < AutoDTable.Rows.Count; i++)
            //    {


            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["S.No"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Name"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["GRD"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FD"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ON"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["AS ON"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["OFF"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //    }

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + Tot_ON, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);


            //    cell = PhraseCell(new Phrase(" " + Tot_ASON, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + Tot_OFF, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + Tot_Total, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);
            //}
            //else
            //{
            //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("OE VAJPAI - ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER;
            //    cell.Colspan = 8;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.BOTTOM_BORDER;
            //    cell.Colspan = 8;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //}

            //table.SpacingBefore = 10f;
            //table.SpacingAfter = 10f;

            //document.Add(table);

            ////table1.SpacingBefore = 5f;
            ////table1.SpacingAfter = 5f;

            //document.Add(table1);


            //document.Close();

            //byte[] bytes = memoryStream.ToArray();
            //memoryStream.Close();
            //Response.Clear();
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", "attachment; filename=HR_Trace.pdf");
            //Response.ContentType = "application/pdf";
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.BinaryWrite(bytes);
            //Response.End();
            //Response.Close();



        }

    }

    public void NonAdminGetHRTracer_Changes(string DeptName, string FromDate, string ToDate, string CurrentDate)
    {
        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable IsActive = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable CurrentEmp = new DataTable();

        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("GRD");
        AutoDTable.Columns.Add("FD");
        AutoDTable.Columns.Add("ON");
        AutoDTable.Columns.Add("AS ON");
        AutoDTable.Columns.Add("OFF");
        AutoDTable.Columns.Add("Total");

        SSQL = "Select AM.AgentID,AM.AgentName,MG.GradeID,MG.GradeName,sum(LM.Total) As Total from Employee_Mst EM";
        SSQL = SSQL + " inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignNo";
        SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        SSQL = SSQL + " inner join MstGrade MG on EM.Grade=MG.GradeID where EM.BrokerName!='0' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";

        if (DeptName != "0")
        {
            SSQL = SSQL + " And EM.DeptName='" + DeptName + "'";
        }

        SSQL = SSQL + " Group by AM.AgentID,AM.AgentName,MG.GradeID,MG.GradeName";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        int SNo = 1;
        int intK = 0;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            double Total = 0;

            double Fd = Convert.ToDouble(dt.Rows[i]["Total"].ToString());

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[intK]["S.No"] = SNo.ToString();
            AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["AgentName"].ToString();
            AutoDTable.Rows[intK]["GRD"] = dt.Rows[i]["GradeName"].ToString();
            AutoDTable.Rows[intK]["FD"] = Fd;

            SSQL = "Select AM.AgentID,AM.AgentName,EM.Grade,COUNT(EM.EmpNo) As EmpNo from Employee_Mst EM inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignNo";
            SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
            SSQL = SSQL + " inner join LogTime_Days LD on LM.DesignNo = LD.Designation where EM.IsActive='Yes' And EM.BrokerName ='" + dt.Rows[i]["AgentID"].ToString() + "'";
            SSQL = SSQL + " And EM.Grade='" + dt.Rows[i]["GradeID"].ToString() + "' And LD.Present !='0.0' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }
            if (ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }

            SSQL = SSQL + " Group by AM.AgentID,AM.AgentName,EM.Grade";

            IsActive = objdata.RptEmployeeMultipleDetails(SSQL);

            if (IsActive.Rows.Count > 0)
            {
                AutoDTable.Rows[intK]["ON"] = IsActive.Rows[0]["EmpNo"].ToString();

            }
            else
            {
                AutoDTable.Rows[intK]["ON"] = "0";
            }



            SSQL = "Select AM.AgentID,AM.AgentName,EM.Grade,COUNT(EM.EmpNo) As EmpNo from Employee_Mst EM inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignNo";
            SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
            SSQL = SSQL + " inner join LogTime_Days LD on LM.DesignNo = LD.Designation where EM.IsActive='No' And EM.BrokerName ='" + dt.Rows[i]["AgentID"].ToString() + "'";
            SSQL = SSQL + " And EM.Grade='" + dt.Rows[i]["GradeID"].ToString() + "' And LD.Present !='0.0' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }
            if (ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }

            SSQL = SSQL + " Group by AM.AgentID,AM.AgentName,EM.Grade";

            NonActive = objdata.RptEmployeeMultipleDetails(SSQL);

            if (NonActive.Rows.Count > 0)
            {
                AutoDTable.Rows[intK]["OFF"] = NonActive.Rows[0]["EmpNo"].ToString();

            }
            else
            {
                AutoDTable.Rows[intK]["OFF"] = "0";
            }

            SSQL = "Select AM.AgentID,AM.AgentName,EM.Grade,COUNT(EM.EmpNo) As EmpNo from Employee_Mst EM inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignNo";
            SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
            SSQL = SSQL + " inner join LogTime_Days LD on LM.DesignNo = LD.Designation where EM.IsActive='Yes' And EM.BrokerName ='" + dt.Rows[i]["AgentID"].ToString() + "'";
            SSQL = SSQL + " And EM.Grade='" + dt.Rows[i]["GradeID"].ToString() + "' And LD.Present !='0.0' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";

            if (CurrentDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,120) = CONVERT(DATETIME,'" + Convert.ToDateTime(CurrentDate).ToString("yyyy/MM/dd") + "',120)";
            }


            SSQL = SSQL + " Group by AM.AgentID,AM.AgentName,EM.Grade";

            CurrentEmp = objdata.RptEmployeeMultipleDetails(SSQL);

            if (CurrentEmp.Rows.Count > 0)
            {
                AutoDTable.Rows[intK]["AS ON"] = CurrentEmp.Rows[0]["EmpNo"].ToString();

            }
            else
            {
                AutoDTable.Rows[intK]["AS ON"] = "0";
            }

            Total = Convert.ToDouble(AutoDTable.Rows[intK]["AS ON"].ToString()) + Convert.ToDouble(AutoDTable.Rows[intK]["OFF"].ToString());

            AutoDTable.Rows[intK]["Total"] = Total;



            SNo = SNo + 1;
            intK = intK + 1;
        }

        SSQL = " Select DeptName from Department_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And DeptCode='" + DeptName + "' ";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        ds.Tables.Add(AutoDTable);
        if (AutoDTable.Rows.Count > 0)
        {
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/HRTracer.rpt"));
            report.DataDefinition.FormulaFields["Company"].Text = "'" + "Murugan" + "'";
            report.DataDefinition.FormulaFields["Location"].Text = "'" + SessionLcode + "'";
            report.DataDefinition.FormulaFields["Wages"].Text = "'" + DeptName + "'";
            report.DataDefinition.FormulaFields["From"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["To"].Text = "'" + ToDate + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
        }
        //Document document = new Document(PageSize.A4, 25, 25, 45, 40);
        //Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
        //System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
        //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

        //writer.PageEvent = new HeaderFooter();
        //document.Open();


        //PdfPTable table = new PdfPTable(5);
        //float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f };
        //table.SetWidths(widths);
        //table.WidthPercentage = 100;

        //PdfPTable table1 = new PdfPTable(8);
        //float[] widths1 = new float[] { 15f, 40f, 15f, 15f, 15f, 15f, 15f, 15f };
        //table1.SetWidths(widths1);
        //table1.WidthPercentage = 100;

        //PdfPTable table2 = new PdfPTable(4);
        //float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
        //table2.SetWidths(widths2);
        //table2.WidthPercentage = 100;

        //PdfPCell cell;

        //if (AutoDTable.Rows.Count > 0)
        //{
        //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    if (DeptName != "0")
        //    {
        //        cell = PhraseCell(new Phrase(" " + dt1.Rows[0]["DeptName"] + " " + " - " + " " + " ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 5;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);
        //    }
        //    else
        //    {
        //        cell = PhraseCell(new Phrase("ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 5;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);
        //    }

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 2;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Current Date: " + CurrentDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 2;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //    table.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = 0;
        //    //cell.Colspan = 2;
        //    //cell.PaddingTop = 5f;
        //    //cell.PaddingBottom = 5f;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.TOP_BORDER;
        //    //cell.Colspan = 8;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table1.AddCell(cell); Allot

        //    cell = PhraseCell(new Phrase("SNo", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);


        //    cell = PhraseCell(new Phrase("GRD", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("FD", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("ON", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("AS ON", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("OFF", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.BOTTOM_BORDER;
        //    //cell.Colspan = 8;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table1.AddCell(cell);





        //    for (int i = 0; i < AutoDTable.Rows.Count; i++)
        //    {


        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["S.No"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Name"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["GRD"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["FD"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["ON"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["AS ON"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["OFF"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Total"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //    }




        //}
        //else
        //{
        //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("OE VAJPAI - ON / OFF DUTY REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER;
        //    cell.Colspan = 8;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.BOTTOM_BORDER;
        //    cell.Colspan = 8;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //}

        //table.SpacingBefore = 10f;
        //table.SpacingAfter = 10f;

        //document.Add(table);

        ////table1.SpacingBefore = 5f;
        ////table1.SpacingAfter = 5f;

        //document.Add(table1);


        //document.Close();

        //byte[] bytes = memoryStream.ToArray();
        //memoryStream.Close();
        //Response.Clear();
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("Content-Disposition", "attachment; filename=HR_Trace.pdf");
        //Response.ContentType = "application/pdf";
        //Response.Buffer = true;
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.BinaryWrite(bytes);
        //Response.End();
        //Response.Close();

    }

    public void GetHRLeaveLabourWise(string FromDate, string ToDate)
    {
        if (SessionUserType == "2")
        {
            NonAdminHRLeaveLabourWise_Changes(FromDate, ToDate);
        }
        else
        {
            string SSQL = "";
            string[] FromMonth = FromDate.Split('/');
            string[] ToMonth = ToDate.Split('/');
            DataTable dt = new DataTable();
            //SSQL = "select EM.EmpNo,AM.AgentName,ISNULL(EM.FirstName,'') As FirstName,ME.FromDate,ME.ToDate,ME.TotayDays from Employee_Mst EM";
            //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID And EM.CompCode = ME.CompCode And EM.LocCode = ME.LocCode";
            //SSQL = SSQL + " inner join ManLeave_Entry ME ON EM.EmpNo = ME.EmpNo where EM.IsActive='Yes'";
            //SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            //SSQL = SSQL + " And ME.CompCode='" + SessionCcode.ToString() + "' And ME.LocCode='" + SessionLcode.ToString() + "'";

            //if (FromDate != "")
            //{
            //    SSQL = SSQL + "And CONVERT(datetime,ME.FromDate,103) >= CONVERT(datetime,'" + FromDate + "',103)";
            //}

            //if (ToDate != "")
            //{
            //    SSQL = SSQL + " And CONVERT(datetime,ME.ToDate,103) <= CONVERT(datetime,'" + ToDate + "',103)";
            //}

            //SSQL = SSQL + "";

            SSQL = "select EM.existingCode as empno,AM.AgentName as Source,ISNULL(EM.FirstName,'') As Name,EM.LeaveFrom as [From],EM.LeaveTo as [To],DATEDIFF(day, convert(date,EM.LeaveFrom,103),convert(date,EM.LeaveTo,103)) as TotDays from Employee_Mst EM";
            SSQL = SSQL + " inner join AgentMst AM on EM.AgentID = AM.AgentID And EM.CompCode = AM.CompCode And EM.LocCode = AM.LocCode";
            SSQL = SSQL + " where EM.IsActive='Yes' and EM.LeaveFrom<>''";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";

            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(datetime,EM.LeaveFrom,103) >= CONVERT(datetime,'" + FromDate + "',103)";
            }

            if (ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,EM.LeaveTo,103) <= CONVERT(datetime,'" + ToDate + "',103)";
            }

            SSQL = SSQL + " order by CONVERT(datetime,EM.LeaveFrom,103) Asc";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ds.Tables.Add(dt);
            if (dt.Rows.Count > 0)
            {
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/HRLeaveLabourWise.rpt"));
                report.DataDefinition.FormulaFields["Company"].Text = "'" + "Murugan" + "'";
                report.DataDefinition.FormulaFields["Location"].Text = "'" + SessionLcode + "'";
                report.DataDefinition.FormulaFields["From"].Text = "'" + FromDate + "'";
                report.DataDefinition.FormulaFields["To"].Text = "'" + ToDate + "'";
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found');", true);
            }
            //Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            //Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            //System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            //writer.PageEvent = new HeaderFooter();
            //document.Open();


            //PdfPTable table = new PdfPTable(5);
            //float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f };
            //table.SetWidths(widths);
            //table.WidthPercentage = 100;

            //PdfPTable table1 = new PdfPTable(6);
            //float[] widths1 = new float[] { 15f, 45f, 45f, 25f, 25f, 15f };
            //table1.SetWidths(widths1);
            //table1.WidthPercentage = 100;

            //PdfPTable table2 = new PdfPTable(4);
            //float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            //table2.SetWidths(widths2);
            //table2.WidthPercentage = 100;

            //PdfPCell cell;

            //if (dt.Rows.Count > 0)
            //{
            //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("HR LEAVE REPORT - LABOUR WISE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 3;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.TOP_BORDER;
            //    //cell.Colspan = 8;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table1.AddCell(cell); Allot


            //    cell = PhraseCell(new Phrase("ID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("SOURCE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("NAME", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("FROM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("TO", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("TOT DAYS", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.BOTTOM_BORDER;
            //    //cell.Colspan = 8;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table1.AddCell(cell);

            //    int SNo = 1;



            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {


            //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["EmpNo"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["AgentName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["LeaveFrom1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["LeaveTo1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);

            //        int DaysCount = 0;

            //        if (dt.Rows[i]["LeaveFrom1"].ToString() != "" && dt.Rows[i]["LeaveTo1"].ToString() != "")
            //        {

            //            DateTime Fdate = Convert.ToDateTime(dt.Rows[i]["LeaveFrom1"].ToString());
            //            DateTime Tdate = Convert.ToDateTime(dt.Rows[i]["LeaveTo1"].ToString());

            //            DaysCount = (int)(Tdate - Fdate).TotalDays;
            //            DaysCount = DaysCount + 1;
            //        }
            //        else
            //        {

            //            DaysCount = 0;
            //        }


            //        cell = PhraseCell(new Phrase(" " + DaysCount, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //        cell.PaddingTop = 5f;
            //        cell.PaddingBottom = 5f;
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        table1.AddCell(cell);



            //    }




            //}
            //else
            //{
            //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("HR LEAVE REPORT - LABOUR WISE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 3;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER;
            //    cell.Colspan = 6;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 6;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.BOTTOM_BORDER;
            //    cell.Colspan = 6;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //}

            //table.SpacingBefore = 10f;
            //table.SpacingAfter = 10f;

            //document.Add(table);

            ////table1.SpacingBefore = 5f;
            ////table1.SpacingAfter = 5f;

            //document.Add(table1);


            //document.Close();

            //byte[] bytes = memoryStream.ToArray();
            //memoryStream.Close();
            //Response.Clear();
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", "attachment; filename=HR_LEAVE_REPORT_LABOUR_WISE.pdf");
            //Response.ContentType = "application/pdf";
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.BinaryWrite(bytes);
            //Response.End();
            //Response.Close();


        }

    }

    public void NonAdminHRLeaveLabourWise_Changes(string FromDate, string ToDate)
    {

        string SSQL = "";
        DataTable dt = new DataTable();
        //SSQL = "select EM.EmpNo,AM.AgentName,ISNULL(EM.FirstName,'') As FirstName,ME.FromDate,ME.ToDate,ME.TotayDays from Employee_Mst EM";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join ManLeave_Entry ME ON EM.EmpNo = ME.EmpNo where EM.IsActive='Yes'";
        //SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        //SSQL = SSQL + " And ME.CompCode='" + SessionCcode.ToString() + "' And ME.LocCode='" + SessionLcode.ToString() + "'";

        //if (FromDate != "")
        //{
        //    SSQL = SSQL + "And CONVERT(datetime,ME.FromDate,103) >= CONVERT(datetime,'" + FromDate + "',103)";
        //}

        //if (ToDate != "")
        //{
        //    SSQL = SSQL + " And CONVERT(datetime,ME.ToDate,103) <= CONVERT(datetime,'" + ToDate + "',103)";
        //}

        //SSQL = SSQL + "";

        SSQL = "select EM.EmpNo,AM.AgentName,ISNULL(EM.FirstName,'') As FirstName,EM.LeaveFrom1,EM.LeaveTo1 from Employee_Mst EM";
        SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID And EM.CompCode = AM.CompCode And EM.LocCode = AM.LocCode";
        SSQL = SSQL + " where EM.IsActive='Yes'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' And AM.LocCode='" + SessionLcode.ToString() + "'";

        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(datetime,EM.LeaveFrom1,103) >= CONVERT(datetime,'" + FromDate + "',103)";
        }

        if (ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(datetime,EM.LeaveFrom1,103) <= CONVERT(datetime,'" + ToDate + "',103)";
        }

        SSQL = SSQL + " And EM.SubCatName='INSIDER'";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ds.Tables.Add(dt);
        if (dt.Rows.Count > 0)
        {
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/HRLeaveLabourWise.rpt"));
            report.DataDefinition.FormulaFields["Company"].Text = "'" + "Murugan" + "'";
            report.DataDefinition.FormulaFields["Location"].Text = "'" + SessionLcode + "'";
            report.DataDefinition.FormulaFields["From"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["To"].Text = "'" + ToDate + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found');", true);
        }
        //Document document = new Document(PageSize.A4, 25, 25, 45, 40);
        //Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
        //System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
        //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

        //writer.PageEvent = new HeaderFooter();
        //document.Open();


        //PdfPTable table = new PdfPTable(5);
        //float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f };
        //table.SetWidths(widths);
        //table.WidthPercentage = 100;

        //PdfPTable table1 = new PdfPTable(6);
        //float[] widths1 = new float[] { 15f, 45f, 45f, 25f, 25f, 15f };
        //table1.SetWidths(widths1);
        //table1.WidthPercentage = 100;

        //PdfPTable table2 = new PdfPTable(4);
        //float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
        //table2.SetWidths(widths2);
        //table2.WidthPercentage = 100;

        //PdfPCell cell;

        //if (dt.Rows.Count > 0)
        //{
        //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("HR LEAVE REPORT - LABOUR WISE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 2;
        //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 3;
        //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.TOP_BORDER;
        //    //cell.Colspan = 8;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table1.AddCell(cell); Allot


        //    cell = PhraseCell(new Phrase("ID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("SOURCE", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("NAME", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("FROM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("TO", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("TOT DAYS", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.BOTTOM_BORDER;
        //    //cell.Colspan = 8;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table1.AddCell(cell);

        //    int SNo = 1;



        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {


        //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["EmpNo"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["AgentName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["LeaveFrom1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" " + dt.Rows[i]["LeaveTo1"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);

        //        int DaysCount = 0;

        //        if (dt.Rows[i]["LeaveFrom1"].ToString() != "" && dt.Rows[i]["LeaveTo1"].ToString() != "")
        //        {

        //            DateTime Fdate = Convert.ToDateTime(dt.Rows[i]["LeaveFrom1"].ToString());
        //            DateTime Tdate = Convert.ToDateTime(dt.Rows[i]["LeaveTo1"].ToString());

        //            DaysCount = (int)(Tdate - Fdate).TotalDays;
        //        }
        //        else
        //        {

        //            DaysCount = 0;
        //        }


        //        cell = PhraseCell(new Phrase(" " + DaysCount, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table1.AddCell(cell);



        //    }




        //}
        //else
        //{
        //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("HR LEAVE REPORT - LABOUR WISE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 3;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 2;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER;
        //    cell.Colspan = 6;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 6;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.BOTTOM_BORDER;
        //    cell.Colspan = 6;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //}

        //table.SpacingBefore = 10f;
        //table.SpacingAfter = 10f;

        //document.Add(table);

        ////table1.SpacingBefore = 5f;
        ////table1.SpacingAfter = 5f;

        //document.Add(table1);


        //document.Close();

        //byte[] bytes = memoryStream.ToArray();
        //memoryStream.Close();
        //Response.Clear();
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("Content-Disposition", "attachment; filename=HR_LEAVE_REPORT_LABOUR_WISE.pdf");
        //Response.ContentType = "application/pdf";
        //Response.Buffer = true;
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.BinaryWrite(bytes);
        //Response.End();
        //Response.Close();

    }

    public void GetHRConsolidatesStrength()
    {
        if (SessionUserType == "2")
        {
            NonAdminGetHRConsolidatesStrength_Changes();
        }
        else
        {
            string SSQL = "";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable NonActive = new DataTable();
            DataTable GradeMst = new DataTable();
            DataTable NewTable = new DataTable();
            DataTable dt_Available = new DataTable();

            AutoDTable.Columns.Add("S.No");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("W.Code");
            AutoDTable.Columns.Add("Designation");
            AutoDTable.Columns.Add("Required", typeof(int));
            AutoDTable.Columns.Add("Day", typeof(int));
            AutoDTable.Columns.Add("HalfNight", typeof(int));
            AutoDTable.Columns.Add("Night", typeof(int));
            AutoDTable.Columns.Add("Total", typeof(int));
            AutoDTable.Columns.Add("Avail", typeof(int));


            SSQL = "Select DeptCode,DeptName from Department_Mst "; //inner join Designation_Mst DS on DM.DeptCode=DS.DeptCode ";
            SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' And  LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " Order by DeptCode Asc";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            int TotCount;

            int IntK = 0;
            int SNo = 1;
            //double Total;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["DeptCode"].ToString() == "6")
                {
                    string StopProcess;
                    StopProcess = "1";
                }

                SSQL = "select DM.DeptName,LM.WorkerCode,LM.DesignName,(CAST(LM.GENERAL as int) + CAST(LM.SHIFT1 as int)) as Dayss,";
                SSQL = SSQL + " (LM.SHIFT2) as Half,(LM.SHIFT3) as Night,cast(LM.Total as int) Total,(Required) as Req ";
                SSQL = SSQL + " from LabourAllotment_Mst LM inner join Department_Mst DM on LM.DeptCode=DM.DeptCode and LM.LocCode=DM.LocCode ";
                // SSQL = SSQL + " Where EM.IsActive='Yes' And EM.DeptName='" + dt.Rows[i]["DeptCode"].ToString() + "'";
                SSQL = SSQL + " Where LM.DeptCode='" + dt.Rows[i]["DeptCode"].ToString() + "'";
                SSQL = SSQL + " And DM.CompCode ='" + SessionCcode.ToString() + "' And DM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And LM.CompCode ='" + SessionCcode.ToString() + "' And LM.LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " Order by LM.WorkerCode Asc";

                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt1.Rows.Count > 0)
                {

                    for (int i1 = 0; i1 < dt1.Rows.Count; i1++)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        AutoDTable.Rows[IntK]["S.No"] = SNo;
                        AutoDTable.Rows[IntK]["DeptName"] = dt1.Rows[i1]["DeptName"].ToString();
                        AutoDTable.Rows[IntK]["W.Code"] = dt1.Rows[i1]["WorkerCode"].ToString();
                        AutoDTable.Rows[IntK]["Designation"] = dt1.Rows[i1]["DesignName"].ToString();
                        AutoDTable.Rows[IntK]["Required"] = dt1.Rows[i1]["Req"].ToString();
                        AutoDTable.Rows[IntK]["Day"] = dt1.Rows[i1]["Dayss"].ToString();
                        AutoDTable.Rows[IntK]["HalfNight"] = dt1.Rows[i1]["Half"].ToString();
                        AutoDTable.Rows[IntK]["Night"] = dt1.Rows[i1]["Night"].ToString();
                        AutoDTable.Rows[IntK]["Total"] = dt1.Rows[i1]["Total"].ToString();

                        SNo = SNo + 1;
                        IntK = IntK + 1;
                    }

                }

                //IntK = 0;
                SNo = 1;

            }

            for (int p = 0; p <= AutoDTable.Rows.Count - 1; p++)
            {
                SSQL = " select COUNT(*) as Available from Employee_Mst EM inner join Designation_Mst DS on EM.Designation=DS.DesignName and EM.DeptCode=DS.DeptCode and EM.LocCode=DS.LocCode ";
                SSQL = SSQL + " where IsActive='Yes' and DS.DesignName='" + AutoDTable.Rows[p]["Designation"] + "' and  EM.LocCode='" + SessionLcode.ToString() + "' and DS.LocCode='" + SessionLcode.ToString() + "' ";
                SSQL = SSQL + " and DS.DeptName='" + AutoDTable.Rows[p]["DeptName"] + "'";
                dt_Available = objdata.RptEmployeeMultipleDetails(SSQL);
                if (AutoDTable.Rows[p]["Designation"].ToString() == "OE - OPR")
                {
                    string sop = "";
                }
                if (dt_Available.Rows.Count > 0)
                {
                    AutoDTable.Rows[p]["Avail"] = dt_Available.Rows[0]["Available"].ToString();
                }
                else
                {
                    AutoDTable.Rows[p]["Avail"] = "0";
                }
            }
            if (AutoDTable.Rows.Count > 0)
            {

                ds.Tables.Add(AutoDTable);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/HRConsolidateStrength.rpt"));
                report.DataDefinition.FormulaFields["Company"].Text = "'" + "Murugan" + "'";
                report.DataDefinition.FormulaFields["Location"].Text = "'" + SessionLcode + "'";
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!')", true);
            }

            //string SS = "0";
            //Document document = new Document(PageSize.A4, 25, 25, 45, 40);
            //iTextSharp.text.Font NormalFont = FontFactory.GetFont("Times New Roman", 12, iTextSharp.text.Font.NORMAL);
            //System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            //writer.PageEvent = new HeaderFooter();
            //document.Open();


            //PdfPTable table = new PdfPTable(5);
            //float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
            //table.SetWidths(widths);
            //table.WidthPercentage = 100;

            //PdfPTable table1 = new PdfPTable(10);
            //float[] widths1 = new float[] { 15f, 40f, 15f, 40f, 15f, 15f, 15f, 15f, 15f, 15f };
            //table1.SetWidths(widths1);
            //table1.WidthPercentage = 100;

            //PdfPTable table2 = new PdfPTable(10);
            //float[] widths2 = new float[] { 15f, 40f, 15f, 40f, 15f, 15f, 15f, 15f, 15f, 15f };
            //table2.SetWidths(widths2);
            //table2.WidthPercentage = 100;

            //PdfPCell cell;

            //if (AutoDTable.Rows.Count > 0)
            //{
            //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("HR CONSOLIDATED - STRENGTH", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);


            //    cell = PhraseCell(new Phrase("", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Alloted ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Rowspan = 1;
            //    //cell.Border = 0;
            //    //cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.TOP_BORDER;
            //    //cell.Colspan = 8;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table1.AddCell(cell); Allot

            //    cell = PhraseCell(new Phrase("S.No", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Rowspan = 1;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Department", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Rowspan = 1;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("W.Code", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Rowspan = 1;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Desgination", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Rowspan = 1;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);



            //    //cell = PhraseCell(new Phrase("Allot", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    //cell.Colspan = 4;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    //table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Day", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("HalfNight", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Night", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Rowspan = 1;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Required", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase("Avai", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    //cell.Border = Rectangle.BOTTOM_BORDER;
            //    //cell.Colspan = 8;
            //    //cell.HorizontalAlignment = Element.ALIGN_CENTER; 
            //    //table1.AddCell(cell);

            //    double count = 0;
            //    double count1 = 0;
            //    double Sum = 0;
            //    double Sum1 = 0;
            //    double Sum2 = 0;
            //    double Sum3 = 0;
            //    double TotalReq = 0;
            //    double TotalAvailable = 0;

            //    double one = 0;
            //    double two = 0;
            //    double three = 0;
            //    double four = 0;
            //    double five = 0;
            //    double six = 0;

            //    for (int k = 0; k < dt.Rows.Count; k++)
            //    {
            //        string Dept = dt.Rows[k]["DeptName"].ToString();

            //        count = 0;
            //        count1 = 0;

            //        for (int i = 0; i < AutoDTable.Rows.Count; i++)
            //        {
            //            string Dept1 = AutoDTable.Rows[i]["Department"].ToString();

            //            if (Dept == Dept1)
            //            {
            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["S.No"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                if (count == 0)
            //                {
            //                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Department"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                    cell.PaddingTop = 5f;
            //                    cell.PaddingBottom = 5f;
            //                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                    table1.AddCell(cell);
            //                }
            //                else
            //                {
            //                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                    cell.PaddingTop = 5f;
            //                    cell.PaddingBottom = 5f;
            //                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                    table1.AddCell(cell);
            //                }

            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["W.Code"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Designation"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);



            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Day"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["HalfNight"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Night"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Allot"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);


            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Required"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Available"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                cell.PaddingTop = 5f;
            //                cell.PaddingBottom = 5f;
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                table1.AddCell(cell);

            //                //cell = PhraseCell(new Phrase(" " + "0", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //                //cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //                //cell.PaddingTop = 5f;
            //                //cell.PaddingBottom = 5f;
            //                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                //table1.AddCell(cell);

            //                TotalReq = TotalReq + Convert.ToDouble(AutoDTable.Rows[i]["Required"].ToString());
            //                Sum = Sum + Convert.ToDouble(AutoDTable.Rows[i]["Day"].ToString());
            //                Sum1 = Sum1 + Convert.ToDouble(AutoDTable.Rows[i]["HalfNight"].ToString());
            //                Sum2 = Sum2 + Convert.ToDouble(AutoDTable.Rows[i]["Night"].ToString());
            //                Sum3 = Sum3 + Convert.ToDouble(AutoDTable.Rows[i]["Allot"].ToString());
            //                TotalAvailable = TotalAvailable + Convert.ToDouble(AutoDTable.Rows[i]["Available"].ToString());
            //                count1 = count1 + 1;
            //                count = count + 1;
            //            }
            //        }


            //        if (count1 > 0)
            //        {
            //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);


            //            cell = PhraseCell(new Phrase("Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);



            //            cell = PhraseCell(new Phrase(" " + Sum, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + Sum1, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + Sum2, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + Sum3, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + TotalReq, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            cell = PhraseCell(new Phrase(" " + TotalAvailable, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            cell.PaddingTop = 5f;
            //            cell.PaddingBottom = 5f;
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            table1.AddCell(cell);

            //            //cell = PhraseCell(new Phrase(" " , new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //            //cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //            //cell.PaddingTop = 5f;
            //            //cell.PaddingBottom = 5f;
            //            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            //table1.AddCell(cell);


            //        }

            //        one = one + Sum;
            //        two = two + Sum1;
            //        three = three + Sum2;
            //        four = four + Sum3;
            //        five = five + TotalReq;
            //        six = six + TotalAvailable;
            //        TotalReq = 0;
            //        Sum = 0;
            //        Sum1 = 0;
            //        Sum2 = 0;
            //        Sum3 = 0;
            //        count1 = 0;
            //        count = 0;
            //        TotalAvailable = 0;

            //    }

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);

            //    cell.Colspan = 10;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);


            //    cell = PhraseCell(new Phrase("Grand Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + one, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + two, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + three, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + four, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + five, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" " + six, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table2.AddCell(cell);


            //}
            //else
            //{
            //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase("WORK CODE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = 0;
            //    cell.Colspan = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER;
            //    cell.Colspan = 9;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
            //    cell.Colspan = 9;
            //    cell.PaddingTop = 5f;
            //    cell.PaddingBottom = 5f;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
            //    cell.Border = Rectangle.BOTTOM_BORDER;
            //    cell.Colspan = 9;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    table1.AddCell(cell);

            //}

            //table.SpacingBefore = 10f;
            //table.SpacingAfter = 10f;

            //document.Add(table);

            ////table1.SpacingBefore = 5f;
            ////table1.SpacingAfter = 5f;

            //document.Add(table1);

            //document.Add(table2);


            //document.Close();

            //byte[] bytes = memoryStream.ToArray();
            //memoryStream.Close();
            //Response.Clear();
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", "attachment; filename=HR_Consolidated_Strength_Report.pdf");
            //Response.ContentType = "application/pdf";
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.BinaryWrite(bytes);
            //Response.End();
            //Response.Close();

        }

    }

    public void NonAdminGetHRConsolidatesStrength_Changes()
    {

        string SSQL = "";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable NonActive = new DataTable();
        DataTable GradeMst = new DataTable();
        DataTable NewTable = new DataTable();

        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("Department");
        AutoDTable.Columns.Add("W.Code");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("Required");
        AutoDTable.Columns.Add("Day");
        AutoDTable.Columns.Add("HalfNight");
        AutoDTable.Columns.Add("Night");
        AutoDTable.Columns.Add("Allot");


        SSQL = "Select DM.DeptCode,DM.DeptName from Employee_Mst EM";
        SSQL = SSQL + " Inner Join Department_Mst DM on EM.DeptName = DM.DeptCode";
        SSQL = SSQL + " Where EM.IsActive='Yes' And EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " Group By  DM.DeptCode,DM.DeptName Order by DM.DeptCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        int TotCount;

        int IntK = 0;
        int SNo = 1;
        //double Total;

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            SSQL = "Select LM.WorkerCode,LM.DesignNo,LM.DesignName,SUM(cast (LM.Total as int)) as Total";
            SSQL = SSQL + " from Employee_Mst EM";
            SSQL = SSQL + " inner join LabourAllotment_Mst LM on EM.Designation = LM.DesignNo";
            SSQL = SSQL + " Where EM.IsActive='Yes' And EM.DeptName='" + dt.Rows[i]["DeptCode"].ToString() + "'";
            SSQL = SSQL + " And EM.CompCode ='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " Group By  LM.WorkerCode,LM.DesignNo,LM.DesignName Order by LM.DesignNo Asc";

            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            decimal Day = 0;
            decimal HalfNight = 0;
            decimal Night = 0;
            decimal Total = 0;

            for (int i1 = 0; i1 < dt1.Rows.Count; i1++)
            {

                //SSQL = " SELECT * FROM ( SELECT Shift FROM LogTime_Days where CONVERT(DATETIME,Attn_Date_Str,120) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                //SSQL = SSQL + " And Designation='" + dt1.Rows[i1]["DesignNo"].ToString() + "' And DeptName='" + dt.Rows[i]["DeptCode"].ToString() + "' And Present !='0' ) as s";
                //SSQL = SSQL + " PIVOT (count(Shift) FOR [Shift] IN (GENERAL,SHIFT1,SHIFT2,SHIFT3,SHIFT4) )AS pvt ";
                //NewTable = objdata.RptEmployeeMultipleDetails(SSQL);

                if (NewTable.Rows.Count > 0)
                {
                    Day = Convert.ToDecimal(NewTable.Rows[0]["GENERAL"].ToString()) + Convert.ToDecimal(NewTable.Rows[0]["SHIFT1"].ToString());
                    HalfNight = Convert.ToDecimal(NewTable.Rows[0]["SHIFT2"].ToString());
                    Night = Convert.ToDecimal(NewTable.Rows[0]["SHIFT4"].ToString());
                }

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[IntK]["S.No"] = SNo;

                AutoDTable.Rows[IntK]["Department"] = dt.Rows[i]["DeptName"].ToString();

                AutoDTable.Rows[IntK]["W.Code"] = dt1.Rows[i1]["WorkerCode"].ToString();
                AutoDTable.Rows[IntK]["Designation"] = dt1.Rows[i1]["DesignName"].ToString();
                AutoDTable.Rows[IntK]["Required"] = dt1.Rows[i1]["Total"].ToString();
                AutoDTable.Rows[IntK]["Day"] = Day.ToString();
                AutoDTable.Rows[IntK]["HalfNight"] = HalfNight.ToString();
                AutoDTable.Rows[IntK]["Night"] = Night.ToString();

                Total = Convert.ToDecimal(Day) + Convert.ToDecimal(HalfNight) + Convert.ToDecimal(Night);

                AutoDTable.Rows[IntK]["Allot"] = Total;

                SNo = SNo + 1;
                IntK = IntK + 1;
                //Sum = Sum + Convert.ToDouble(dt1.Rows[i1]["Day"].ToString());
                //Sum1 = Sum1 + Convert.ToDouble(dt1.Rows[i1]["Night"].ToString());
                //Sum2 = Sum2 + Total;
                Day = 0;
                HalfNight = 0;
                Night = 0;
                Total = 0;
            }

            //AutoDTable.NewRow();
            //AutoDTable.Rows.Add();

            //AutoDTable.Rows[IntK]["Designation"] = "Total";
            //AutoDTable.Rows[IntK]["Day"] = Sum;
            //AutoDTable.Rows[IntK]["Night"] = Sum1;
            //AutoDTable.Rows[IntK]["Total"] = Sum2;

            //IntK = IntK + 1;

        }
        ds.Tables.Add(AutoDTable);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/HRConsolidateStrength.rpt"));

        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;

        //string SS = "0";
        //Document document = new Document(PageSize.A4, 25, 25, 45, 40);
        //Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
        //System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
        //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

        //writer.PageEvent = new HeaderFooter();
        //document.Open();


        //PdfPTable table = new PdfPTable(5);
        //float[] widths = new float[] { 30f, 10f, 40f, 10f, 10f };
        //table.SetWidths(widths);
        //table.WidthPercentage = 100;

        //PdfPTable table1 = new PdfPTable(9);
        //float[] widths1 = new float[] { 15f, 40f, 15f, 40f, 15f, 15f, 15f, 15f, 15f };
        //table1.SetWidths(widths1);
        //table1.WidthPercentage = 100;

        //PdfPTable table2 = new PdfPTable(4);
        //float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
        //table2.SetWidths(widths2);
        //table2.WidthPercentage = 100;

        //PdfPCell cell;

        //if (AutoDTable.Rows.Count > 0)
        //{
        //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("WORK CODE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);


        //    cell = PhraseCell(new Phrase("", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.TOP_BORDER;
        //    //cell.Colspan = 8;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table1.AddCell(cell); Allot

        //    cell = PhraseCell(new Phrase("S.No", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Rowspan = 1;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Department", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Rowspan = 1;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("W.Code", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Rowspan = 1;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Desgination", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Rowspan = 1;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Required", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Rowspan = 1;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    //cell = PhraseCell(new Phrase("Allot", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    //cell.Colspan = 4;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    //table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Day", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("HalfNight", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Night", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase("Allot", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    //cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    //cell.Border = Rectangle.BOTTOM_BORDER;
        //    //cell.Colspan = 8;
        //    //cell.HorizontalAlignment = Element.ALIGN_CENTER; 
        //    //table1.AddCell(cell);

        //    double count = 0;
        //    double count1 = 0;
        //    double Sum = 0;
        //    double Sum1 = 0;
        //    double Sum2 = 0;
        //    double Sum3 = 0;

        //    for (int k = 0; k < dt.Rows.Count; k++)
        //    {
        //        string Dept = dt.Rows[k]["DeptName"].ToString();

        //        count = 0;
        //        count1 = 0;

        //        for (int i = 0; i < AutoDTable.Rows.Count; i++)
        //        {
        //            string Dept1 = AutoDTable.Rows[i]["Department"].ToString();

        //            if (Dept == Dept1)
        //            {
        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["S.No"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                if (count == 0)
        //                {
        //                    cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Department"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                    cell.PaddingTop = 5f;
        //                    cell.PaddingBottom = 5f;
        //                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                    table1.AddCell(cell);
        //                }
        //                else
        //                {
        //                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                    cell.PaddingTop = 5f;
        //                    cell.PaddingBottom = 5f;
        //                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                    table1.AddCell(cell);
        //                }

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["W.Code"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Designation"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Required"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Day"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["HalfNight"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Night"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                cell = PhraseCell(new Phrase(" " + AutoDTable.Rows[i]["Allot"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                table1.AddCell(cell);

        //                Sum = Sum + Convert.ToDouble(AutoDTable.Rows[i]["Day"].ToString());
        //                Sum1 = Sum1 + Convert.ToDouble(AutoDTable.Rows[i]["HalfNight"].ToString());
        //                Sum2 = Sum2 + Convert.ToDouble(AutoDTable.Rows[i]["Night"].ToString());
        //                Sum3 = Sum3 + Convert.ToDouble(AutoDTable.Rows[i]["Allot"].ToString());
        //                count1 = count1 + 1;
        //                count = count + 1;
        //            }
        //        }


        //        if (count1 > 0)
        //        {
        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);


        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase("Total", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" " + Sum, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" " + Sum1, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" " + Sum2, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" " + Sum3, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table1.AddCell(cell);
        //        }

        //        Sum = 0;
        //        Sum1 = 0;
        //        Sum2 = 0;
        //        Sum3 = 0;
        //        count1 = 0;
        //        count = 0;
        //    }


        //}
        //else
        //{
        //    cell = PhraseCell(new Phrase("" + SessionCcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("" + SessionLcode.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase("WORK CODE REPORT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = 0;
        //    cell.Colspan = 5;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER;
        //    cell.Colspan = 9;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" Data Not Found", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //    cell.Colspan = 9;
        //    cell.PaddingTop = 5f;
        //    cell.PaddingBottom = 5f;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //    cell.Border = Rectangle.BOTTOM_BORDER;
        //    cell.Colspan = 9;
        //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //    table1.AddCell(cell);

        //}

        //table.SpacingBefore = 10f;
        //table.SpacingAfter = 10f;

        //document.Add(table);

        ////table1.SpacingBefore = 5f;
        ////table1.SpacingAfter = 5f;

        //document.Add(table1);


        //document.Close();

        //byte[] bytes = memoryStream.ToArray();
        //memoryStream.Close();
        //Response.Clear();
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("Content-Disposition", "attachment; filename=HR_Consolidated_Strength_Report.pdf");
        //Response.ContentType = "application/pdf";
        //Response.Buffer = true;
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.BinaryWrite(bytes);
        //Response.End();
        //Response.Close();


    }
}