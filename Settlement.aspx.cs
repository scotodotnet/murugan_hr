﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Settlement : System.Web.UI.Page
{
    private int[] monthDay = new int[12] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    private DateTime fromDate;
    private DateTime toDate;
    private int year;
    private int month;
    private int day; 

    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SessionEpay;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
           
            Load_TktNo();
            Load_Department();
            Load_Designation();
        }
    }

    protected void ddlTokeNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        
        DataTable dtdsupp = new DataTable();
        DataTable dtdExpr = new DataTable();

        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo,MachineID,FirstName,DeptName,Designation,DOJ,DOR,Wages,Address1 from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='No' and ExistingCode='"+ddlTokeNo.SelectedItem.Text+"'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        if (dtdsupp.Rows.Count != 0)
        {
            txtExisting.Text = dtdsupp.Rows[0]["EmpNo"].ToString();
            txtEmployeeNo.Text = dtdsupp.Rows[0]["MachineID"].ToString();
            txtEmployeeName.Text = dtdsupp.Rows[0]["FirstName"].ToString();
            ddldept.SelectedItem.Text = dtdsupp.Rows[0]["DeptName"].ToString();
            ddlDesignation.SelectedItem.Text = dtdsupp.Rows[0]["Designation"].ToString();
            txtDOJ.Text = Convert.ToDateTime(dtdsupp.Rows[0]["DOJ"].ToString()).ToString("dd/MM/yyyy");
            txtLeavingDate.Text = Convert.ToDateTime(dtdsupp.Rows[0]["DOR"].ToString()).ToString("dd/MM/yyyy");
            ddlResign.SelectedItem.Text = "Resigned";
            txtAddress.Text = dtdsupp.Rows[0]["Address1"].ToString();

            if (txtLeavingDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Enter the Leave Date');", true);
            }
            else
            {
                //Total period of service
                query = "SELECT DOJ,convert(varchar(3),DATEDIFF(MONTH, DOJ,CONVERT(dateTime, '" + txtLeavingDate.Text + "',103))/12) +' years '+";
                query = query + "convert(varchar(2),DATEDIFF(MONTH, DOJ,CONVERT(dateTime, '" + txtLeavingDate.Text + "',103)) % 12)+ ' months' ";
                query = query + " AS EXPERIENCE FROM Employee_Mst where MachineID='" + txtEmployeeNo.Text + "'";
                dtdExpr = objdata.RptEmployeeMultipleDetails(query);
                if (dtdExpr.Rows.Count != 0)
                {
                    txtTotalPeriod.Text = dtdExpr.Rows[0]["EXPERIENCE"].ToString();
                }
            }
        }
    }
    private void Load_TktNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlTokeNo.Items.Clear();
        query = "Select CONVERT(varchar(10), ExistingCode) as ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive ='No' ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlTokeNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "0";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlTokeNo.DataTextField = "ExistingCode";
        ddlTokeNo.DataValueField = "ExistingCode";
        ddlTokeNo.DataBind();
    }
  

    
    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddldept.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddldept.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddldept.DataTextField = "DeptName";
        ddldept.DataValueField = "DeptCode";
        ddldept.DataBind();
    }
    private void Load_Designation()
    {
        string query = "";
        DataTable dtddes = new DataTable();
        ddlDesignation.Items.Clear();
        query = "Select * from Designation_Mst";
        dtddes = objdata.RptEmployeeMultipleDetails(query);
        ddlDesignation.DataSource = dtddes;
        DataRow dr = dtddes.NewRow();
        dr["DesignName"] = "0";
        dr["DesignName"] = "-Select-";
        dtddes.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable da_Delete = new DataTable();
        DataTable da_Insert = new DataTable();
        DataTable dtdsupp = new DataTable();
        DataTable dtwage = new DataTable();
        DataTable dtdBonus = new DataTable();
        string wages = "0";
        string Gratuity_payable;
        double Wages_Val = 0;
        string CatName = "";
        string Total_Amt = "";
        string Bonus_Amt = "";

        bool Errflag = false;
        string SSQL = "";
        if (ddlTokeNo.SelectedValue == "-Select-")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the Token No');", true);
        }
        else if(txtEmployeeNo.Text=="")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the EmployeeNo');", true);
        }
        else if(txtExisting.Text=="")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('select the ExisitingCode');", true);
        }
        else if(txtEmployeeName.Text=="")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the EmpName');", true);
        }
        
        else if (ddldept.SelectedValue == "1")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the DeptName');", true);
        }
        else if (ddlDesignation.SelectedValue == "1")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the Designation');", true);
        }

        else if (txtDOJ.Text == "") 
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the DOJ');", true);
        }
        else if (txtAddress.Text == "")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the Address');", true);
        }
        else if (txtLeavingDate.Text == "")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the Date of leaving service');", true);
        }
        else if (ddlResign.SelectedValue == "0")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the Reason of leaving service');", true);
        }
        else if (txtTotalPeriod.Text == "")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the Total period of service');", true);
        }
        else if(txteligibleYear.Text=="")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SavemsgAlert('Select the Total eligible years');", true);
        }
       

        if (Errflag == false)
        {
            string[] sp_Year;
            sp_Year = txtLeavingDate.Text.Split('/');



            SSQL = "Select BaseSalary,CatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And IsActive='No' and MachineID='" + txtEmployeeNo.Text + "'";
            dtdsupp = objdata.RptEmployeeMultipleDetails(SSQL);

            CatName = dtdsupp.Rows[0]["CatName"].ToString();
            //Total wages
            if (CatName == "STAFF")
            {
                wages = (Convert.ToDouble(dtdsupp.Rows[0]["BaseSalary"].ToString()) / Convert.ToDouble(26)).ToString();
                wages = (Math.Round(Convert.ToDecimal(wages), 0, MidpointRounding.AwayFromZero)).ToString();
                wages = (Convert.ToDouble(wages) * Convert.ToDouble(55) / 100).ToString();
             
            }
            else
            {
                wages = dtdsupp.Rows[0]["BaseSalary"].ToString();
                wages = (Convert.ToDouble(wages) * Convert.ToDouble(55) / 100).ToString();
               
            }

            //Gratuity payable
            Gratuity_payable = (Convert.ToDouble(wages) * Convert.ToDouble(15) * Convert.ToDouble(txteligibleYear.Text)).ToString();
         
            //Bonus payable
            SSQL = "select * from ["+SessionEpay+"].. Bonus_Details where MachineID='" + txtEmployeeNo.Text + "'";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Bonus_Year='" + sp_Year[2] + "' ";
            dtdBonus = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtdBonus.Rows.Count != 0)
            {
                Bonus_Amt = dtdBonus.Rows[0]["Round_Bonus"].ToString();
                txtBonus.Text = Bonus_Amt;
            }
            else
            {
                txtBonus.Text = "0";
            }

            //Wages payable
            SSQL = "select isnull(SUM(WorkedDays),0) as WorkedDays from ["+SessionEpay+"].. SalaryDetails where Year='" + sp_Year[2] + "' and MachineNo='" + txtEmployeeNo.Text + "'";
            dtwage = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtwage.Rows.Count != 0)
            {

                Wages_Val = Convert.ToDouble(dtwage.Rows[0]["WorkedDays"].ToString()) / (Convert.ToDouble(20));
                Wages_Val = ((Convert.ToDouble(Wages_Val)) * (Convert.ToDouble(wages)));
            }
            //Total Amount

            Total_Amt = (Convert.ToDouble(Wages_Val) + Convert.ToDouble(txtBonus.Text) + Convert.ToDouble(Gratuity_payable)).ToString();
            double Val_Round = (Math.Round(Convert.ToDouble(Total_Amt) / 10.0)) * 10;
            txtRoundoff.Text = Val_Round.ToString();



            SSQL = "delete from Settlement where TokenNo='" + ddlTokeNo.SelectedItem.Text + "' and Year='" + sp_Year[2] + "' ";
            SSQL = SSQL + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            da_Delete = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "insert into Settlement(CompCode,LocCode,TokenNo,EmpNo,ExistingCode,FirstName,DeptName,Designation,";
            SSQL = SSQL + " DOJ,Address,DOR,Reason,Total_period,eligible_years,Last_wages,Gratuity_payable,Bonus_payable,";
            SSQL = SSQL + " wages_payable,Total_amount,Roundoff,Year,CreateDate)values('" + SessionCcode + "','" + SessionLcode + "',";
            SSQL = SSQL + " '" + ddlTokeNo.SelectedItem.Text + "','" + txtEmployeeNo.Text + "','" + txtExisting.Text + "',";
            SSQL = SSQL + " '" + txtEmployeeName.Text + "','" + ddldept.SelectedItem.Text + "','" + ddlDesignation.SelectedItem.Text + "',";
            SSQL = SSQL + " convert(dateTime,'" + txtDOJ.Text + "',103),'" + txtAddress.Text + "',convert(dateTime,'" + txtLeavingDate.Text + "',103),'Resigned','" + txtTotalPeriod.Text + "',";
            SSQL = SSQL + " '" + txteligibleYear.Text + "','" + wages + "','" + Gratuity_payable + "','" + Bonus_Amt + "',";
            SSQL = SSQL + " '" + Wages_Val + "','" + Total_Amt + "','" + Val_Round + "','" + sp_Year[2] + "',GetDate())";
            da_Insert = objdata.RptEmployeeMultipleDetails(SSQL);
           
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Saved Successfully..');", true);

        }
        
    }
   
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string ReportName = "SETTLEMENT REPORT";
        string[] sp_Year;
        sp_Year = txtLeavingDate.Text.Split('/');

        ResponseHelper.Redirect("RptSettlement.aspx?Year=" + sp_Year[2] + "&ReportName=" + ReportName + "&TokenNo=" + ddlTokeNo.SelectedItem.Text, "_blank", "");

    }
   
    private static PdfPCell PhraseCell(Phrase phrase, int align)
    {
        PdfPCell cell = new PdfPCell(phrase);
        //cell.BorderColor = Color.WHITE;
        //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 2f;
        cell.PaddingTop = 0f;
        return cell;
    }

    protected void btnBonus_Click(object sender, EventArgs e)
    {
        

        string[] sp_Year;
        sp_Year = txtLeavingDate.Text.Split('/');

        


        string ReportName = "BONUS REPORT";

        ResponseHelper.Redirect("RptSettlement.aspx?ReportName=" + ReportName + "&Year=" + sp_Year[2] + "&TokenNo=" + ddlTokeNo.SelectedItem.Text, "_blank", "");
    }
}
