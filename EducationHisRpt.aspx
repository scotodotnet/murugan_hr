﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EducationHisRpt.aspx.cs" Inherits="EducationHisRpt" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>


<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Trainee Module</a></li>
				<li class="active">Trainee Module Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Trainee Module Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Trainee Module Report</h4>
                        </div>
                        <div class="panel-body">
                     
								 <div class="row">
								  <div class="form-group col-md-3">
    								  <label>Course</label>
									  <asp:DropDownList runat="server" ID="ddlPursuing" class="form-control select2" style="width:100%;">
								      </asp:DropDownList>
								 </div>
                               <div class="form-group col-md-3">
    								  <label>Pursuing</label>
									  <asp:DropDownList runat="server" ID="ddlStatus" class="form-control select2" style="width:100%;">
									  <asp:ListItem>-Select-</asp:ListItem>
									  <asp:ListItem>Pursuing</asp:ListItem>
									  <asp:ListItem>Completed</asp:ListItem>
								      </asp:DropDownList>
								 </div>
								 
						
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReport" Text="Report" class="btn btn-info" 
                                         onclick="btnReport_Click" />
									 
								 </div>
                               </div>
                               
                              <!-- end col-4 -->
                          
                              </div>
                        <!-- end row -->
                    
                    
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>


</asp:UpdatePanel>




</asp:Content>

