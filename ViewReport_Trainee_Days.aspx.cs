﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class ViewReport_Trainee_Days : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; 
    string SessionUserType;
    string SessionEpay;
    string query;
    string Workdays_Date = "";
    string From_Date = "";
    string To_Date = "";
    string RptName = "";
    string SSQL = "";
    string Employee_Type_Str = "";
    string Token_No_Str = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();        
        RptName = Request.QueryString["RptName"].ToString();

        if (RptName == "Trainee_Work_Days_Report")
        {
            Workdays_Date = Request.QueryString["Workdays_Date"].ToString();
            Employee_Type_Str = Request.QueryString["Employee_Type"].ToString();
            Trainee_Worked_Days_Load_Report();
        }

        if (RptName == "Reeling_Entry_Between_Date_Report")
        {
            Token_No_Str = Request.QueryString["Token_No"].ToString();
            From_Date = Request.QueryString["From_Date"].ToString();
            To_Date = Request.QueryString["To_Date"].ToString();
            Reeling_Entry_Between_Date_Report();
        }
    }

    private void Trainee_Worked_Days_Load_Report()
    {
        DataTable DT_N = new DataTable();

        query = "Select A.Wages,A.MachineID,A.ExistingCode,A.FirstName,A.DeptName,A.Emp_Training_Mode,";
        query = query + " Convert(varchar(20),A.DOJ,103) as DOJ,isnull(sum(B.Present),0) as Emp_W_Days from Employee_Mst A";
        query = query + " inner join LogTime_Days B on A.CompCode=B.CompCode And A.LocCode=B.LocCode And A.ExistingCode=B.ExistingCode And A.MachineID=B.MachineID";
        query = query + " where A.CompCode='" + SessionCcode + "' And A.LocCode='" + SessionLcode + "' And A.EmpLevel='Trainee' And A.Emp_Training_Mode <> '0'";
        query = query + " And B.CompCode='" + SessionCcode + "' And B.LocCode='" + SessionLcode + "'";
        query = query + " And CONVERT(Datetime,b.Attn_Date,103) >= CONVERT(Datetime,A.DOJ,103)";
        query = query + " And CONVERT(Datetime,b.Attn_Date,103) <= CONVERT(Datetime,'" + Workdays_Date + "',103)";
        if (Employee_Type_Str != "")
        {
            query = query + " And A.Wages='" + Employee_Type_Str + "'";
        }
        query = query + " group by A.Wages,A.MachineID,A.ExistingCode,A.FirstName,A.DeptName,A.Emp_Training_Mode,A.DOJ";
        query = query + " order by A.Wages,Cast(A.ExistingCode as decimal(18,2)) Asc";
        DT_N = objdata.RptEmployeeMultipleDetails(query);
        if (DT_N.Rows.Count != 0)
        {
            string CmpName = "";
            string Cmpaddress = "";
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }
            rd.Load(Server.MapPath("crystal/Trainee_Worked_Days_Det.rpt"));
            rd.SetDataSource(DT_N);
            string Comp_Name_Full = CmpName + " - " + SessionLcode;
            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Comp_Name_Full + "'";            
            rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";            
            rd.DataDefinition.FormulaFields["WorkDays_Date"].Text = "'" + Workdays_Date + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

    private void Reeling_Entry_Between_Date_Report()
    {
        DataTable DT_N = new DataTable();

        query = "";
        query = "Select cast(ExistingCode as decimal(18,2)) as ExistingCode,Name,Date,Target,Achived,ProductionAmt from [" + SessionEpay + "]..Reeling_Process";
        query = query + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        query = query + " and Convert(datetime,Date,103)>=Convert(datetime,'" + From_Date + "',103) and Convert(datetime,Date,103)<=Convert(datetime,'" + To_Date + "',103)";
        if (Token_No_Str != "")
        {
            SSQL = SSQL + " and ExistingCode='" + Token_No_Str + "'";
        }
        query = query + " order by cast(ExistingCode as decimal(18,2)),Date asc";
        DT_N = objdata.RptEmployeeMultipleDetails(query);
        
        if (DT_N.Rows.Count != 0)
        {
            string CmpName = "";
            string Cmpaddress = "";
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }
            rd.Load(Server.MapPath("crystal/Reeling_Det_Report.rpt"));
            rd.SetDataSource(DT_N);
            string Comp_Name_Full = CmpName + " - " + SessionLcode;
            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Comp_Name_Full + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            rd.DataDefinition.FormulaFields["From_Date"].Text = "'" + From_Date + "'";
            rd.DataDefinition.FormulaFields["To_Date"].Text = "'" + To_Date + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }
}
