﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstCanteenOperator : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Canteen Operator Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();

        string SQL = "Select * from MstCanteenOpt where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'  ";
        //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode.ToString() + "' ";

        dt = objdata.RptEmployeeMultipleDetails(SQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from MstCanteenOpt where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and CanOptID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtCanteenOperatorName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Please Entre the Canteen Operator Name)", true);
            return;
        }
        if (!ErrFlag)
        {
            string SSQL = "";
            SSQL = "Delete from MstCanteenOpt where CanteenNameOpt='" + txtCanteenOperatorName.Text + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "insert into MstCanteenOpt(CanteenNameOpt,CompCode,LocCode)values";
            SSQL = SSQL + "('" + txtCanteenOperatorName.Text + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
            btnClear_Click(sender, e);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtCanteenOperatorName.Text = "";
    }
}