﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstDoctor : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Doctor Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data_Doctor();
    }

    private void Load_Data_Doctor()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDoctor";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstDoctor where DoctorName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstDoctor where DoctorName='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Doctor Deleted Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Doctor();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstDoctor where DoctorName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            
            txtDoctorName.Text = DT.Rows[0]["DoctorName"].ToString();
            txtMobileNo.Text = DT.Rows[0]["Mobile"].ToString();
            Txtclinicname.Text = DT.Rows[0]["Clinic"].ToString();
            txtSpecialist.Text = DT.Rows[0]["Specialist"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            txtDoctorName.Text = "";
            txtMobileNo.Text = "";
            btnSave.Text = "Save";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();


        if (btnSave.Text == "Update")
        {
            SaveMode = "Update";

            query = "Update MstDoctor set Mobile='" + txtMobileNo.Text + "',Clinic='" + Txtclinicname.Text + "',Specialist='"+txtSpecialist.Text+"'";
            query = query + " Where DoctorName='" + txtDoctorName.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

        }
        else
        {
            query = "select * from MstDoctor where DoctorName='" + txtDoctorName.Text.ToUpper() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Already";
            }
            else
            {
                SaveMode = "Insert";
                query = "Insert into MstDoctor (DoctorName,Mobile,Clinic,Specialist)";
                query = query + "values('" + txtDoctorName.Text.ToUpper() + "','" + txtMobileNo.Text + "','" + Txtclinicname.Text + "','" + txtSpecialist.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

        }

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Doctor Details Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Doctor Details Saved Successfully...!');", true);
        }
        else if (SaveMode == "Already")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Doctor Details Already Exists!');", true);
        }

        Load_Data_Doctor();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtDoctorName.Text = "";
        txtMobileNo.Text = "";
        btnSave.Text = "Save";
        
    }

}
