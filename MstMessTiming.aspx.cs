﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstMessTiming : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Mess Timing";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data_Grade();
    }
    private void Load_Data_Grade()
    {
        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select * from MstGrade";
        //DT = objdata.RptEmployeeMultipleDetails(query);
        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        //string query;
        //DataTable DT = new DataTable();


        //query = "select * from MstGrade where GradeID='" + e.CommandName.ToString() + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count > 0)
        //{
        //    query = "delete from MstGrade where GradeID='" + e.CommandName.ToString() + "'";
        //    objdata.RptEmployeeMultipleDetails(query);

        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Grade Deleted Successfully...!');", true);
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        //}
        //Load_Data_Grade();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        //string query;
        //DataTable DT = new DataTable();


        //query = "select * from MstGrade where GradeID='" + e.CommandName.ToString() + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count > 0)
        //{
        //    txtGradeID.Value = DT.Rows[0]["GradeID"].ToString();
        //    txtGradeName.Text = DT.Rows[0]["GradeName"].ToString();


        //    btnSave.Text = "Update";
        //}
        //else
        //{
        //    txtGradeID.Value = "";
        //    txtGradeName.Text = "";

        //    btnSave.Text = "Save";
        //}
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        query = "Select *From MessTime_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And TypetDesc='" + txtType.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete From MessTime_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And TypetDesc='" + txtType.SelectedItem.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        query = "Insert into MessTime_Mst(CompCode,LocCode,TypetDesc,StartTime,EndTime)";
        query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtType.SelectedItem.Text + "','" + txtStartTime.Text + "','" + txtEndTime.Text + "')";
        objdata.RptEmployeeMultipleDetails(query);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtType.SelectedValue = "-Select-";
        txtStartTime.Text = "";
        txtEndTime.Text = "";
        //txtGradeID.Value = "";
        //txtGradeName.Text = "";

        btnSave.Text = "Save";
    }
}
