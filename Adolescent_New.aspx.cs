﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.CompilerServices;
using System.IO;

public partial class Adolescent_New : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string WagesType;
    string Division;
    string AdolescentType;
    BALDataAccess objdata = new BALDataAccess();
    // string SSQL;
    DataTable mDataSet = new DataTable();

    DataTable AutoDataTable = new DataTable();
    string[] delimiters = { "-", ">" };


    string[] Time_Minus_Value_Check;

    string Empcode;
    string Date1 = "";
    string Date2 = "";

    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();

    DataSet ds = new DataSet();
    DataTable DataCell = new DataTable();
    string State;
    string SSQL = "";
    DataTable dt1 = new DataTable();
    string FromDate = "";
    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Relieving Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //string TempWages = Request.QueryString["Wages"].ToString();
            //  WagesType = TempWages.Replace("_", "&");

            FromDate = Request.QueryString["FromDate"].ToString();

            GetDetails();

        }
    }


    public void GetDetails()
    {
        DataTable Emp_DS = new DataTable();
       DataCell.Columns.Add("S.No");
        DataCell.Columns.Add("Token No");
        DataCell.Columns.Add("Name of the employee");
        
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("Date Of Birth");
        DataCell.Columns.Add("Age");
        DataCell.Columns.Add("DOJ");

            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }
           SSQL = SSQL + " and  convert(datetime, BirthDate, 103) <= CONVERT(datetime, '" +FromDate +"', 103) ";        
           SSQL = SSQL + " Order by ExistingCode Asc";

        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Emp_DS.Rows.Count != 0)
        {
            int sno = 0;
            int srno = 1;

            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                 {
                     DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[sno]["S.No"] = srno;
                    DataCell.Rows[sno]["Name of the employee"] = Emp_DS.Rows[intRow]["FirstName"].ToString();
                    DataCell.Rows[sno]["Token No"] = Emp_DS.Rows[intRow]["ExistingCode"].ToString();
                    DataCell.Rows[sno]["Department"] = Emp_DS.Rows[intRow]["DeptName"].ToString();
                    DataCell.Rows[sno]["Designation"] = Emp_DS.Rows[intRow]["Designation"].ToString();
                    DataCell.Rows[sno]["Date Of Birth"] = Convert.ToDateTime(Emp_DS.Rows[intRow]["BirthDate"]).ToString("dd/MM/yyyy");
                    DataCell.Rows[sno]["DOJ"] = Convert.ToDateTime(Emp_DS.Rows[intRow]["DOJ"]).ToString("dd/MM/yyyy");

                    if (FromDate != "")
                    {

                        string bday = Convert.ToDateTime(Emp_DS.Rows[intRow]["BirthDate"]).ToString("dd/MM/yyyy");
                        string[] splitBday = bday.Split('/');
                        string BirthYr = splitBday[2];

                        string[] splitFDay = FromDate.Split('/');
                        string FromYr = splitFDay[2];

                        
                        int datediff = 0;
                        datediff = (Convert.ToInt32(FromYr) - Convert.ToInt32(BirthYr));
                        
                        if (datediff >= 0)
                        {
                            string age = Convert.ToString(datediff);
                            DataCell.Rows[sno]["Age"] = age;

                        }
                    }
                  //  DataCell.Rows[sno]["Age"] = Emp_DS.Rows[intRow]["age"].ToString(); 
                    srno += 1;
                    sno += 1;

                }

            }
            SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();


            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Adolescent.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">ADOLESCENT WORKERS LIST </a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">FROM : " +FromDate +"</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }

    }



}
