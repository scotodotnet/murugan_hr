﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class Uniform_Report_Display : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_fromdate;
    string str_todate;
    string str_ItemName;
    string str_SizeName;
    string str_Token_No;
    string str_Wages;
    string str_Trans_ID;

    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; 
    string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string SessionEpay;
    string query;
    string Get_Report_Type;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionAdmin = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        Get_Report_Type = Request.QueryString["Report_Type"].ToString();
        DataTable DT = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from ["+SessionEpay+"]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count > 0)
        {
            CmpName = DT.Rows[0]["Cname"].ToString();
            Cmpaddress = (DT.Rows[0]["Address1"].ToString() + ", " + DT.Rows[0]["Address2"].ToString() + ", " + DT.Rows[0]["Location"].ToString() + "-" + DT.Rows[0]["Pincode"].ToString());
        }

        if (Get_Report_Type == "Uniform_Stock_Inward_Det")
        {
            str_ItemName = Request.QueryString["ItemName"].ToString();
            str_SizeName = Request.QueryString["SizeName"].ToString();
            str_fromdate = Request.QueryString["FromDate"].ToString();
            str_todate = Request.QueryString["ToDate"].ToString();
            Uniform_Stock_Inward_Report();
        }
        if (Get_Report_Type == "Uniform_Stock_Adjustment_Report_Det")
        {
            str_ItemName = Request.QueryString["ItemName"].ToString();
            str_SizeName = Request.QueryString["SizeName"].ToString();
            str_fromdate = Request.QueryString["FromDate"].ToString();
            str_todate = Request.QueryString["ToDate"].ToString();
            Uniform_Stock_Adjustment_Report();
        }
        if (Get_Report_Type == "Uniform_Issue_Det")
        {
            str_ItemName = Request.QueryString["ItemName"].ToString();
            str_SizeName = Request.QueryString["SizeName"].ToString();
            str_fromdate = Request.QueryString["FromDate"].ToString();
            str_todate = Request.QueryString["ToDate"].ToString();
            str_Wages = Request.QueryString["Wages"].ToString();
            str_Token_No = Request.QueryString["TokenNo"].ToString();
            Uniform_Issue_Report();
        }
        if (Get_Report_Type == "Uniform_Issue_Return_Det")
        {
            str_ItemName = Request.QueryString["ItemName"].ToString();
            str_SizeName = Request.QueryString["SizeName"].ToString();
            str_fromdate = Request.QueryString["FromDate"].ToString();
            str_todate = Request.QueryString["ToDate"].ToString();
            str_Wages = Request.QueryString["Wages"].ToString();
            str_Token_No = Request.QueryString["TokenNo"].ToString();
            Uniform_Issue_Return_Report();
        }
        if (Get_Report_Type == "Uniform_Current_Stock_Report_Det")
        {
            str_ItemName = Request.QueryString["ItemName"].ToString();
            str_SizeName = Request.QueryString["SizeName"].ToString();
            str_fromdate = Request.QueryString["FromDate"].ToString();
            str_todate = Request.QueryString["ToDate"].ToString();
            Uniform_Current_Stock_Report();
        }
        if (Get_Report_Type == "Uniform_Issue_Slip_Det")
        {
            str_ItemName = Request.QueryString["ItemName"].ToString();
            str_SizeName = Request.QueryString["SizeName"].ToString();
            str_fromdate = Request.QueryString["FromDate"].ToString();
            str_todate = Request.QueryString["ToDate"].ToString();
            str_Wages = Request.QueryString["Wages"].ToString();
            str_Token_No = Request.QueryString["TokenNo"].ToString();
            str_Trans_ID = Request.QueryString["Trans_ID"].ToString();

            Uniform_Issue_Slip_Report();
        }

    }

    private void Uniform_Stock_Inward_Report()
    {
        DataTable DT_Res = new DataTable();
        DataSet ds = new DataSet();

        query = "Select SIM.TransID,SIM.TransDate,SIM.PO_Ref_Date,SIM.PO_Ref_No,SIM.Remarks,SIS.ItemName,SIS.SizeName,SIS.Qty,SIS.ItemRate,SIS.LineTotal";
        query = query + " from Uniform_Stock_Inward_Main SIM inner join Uniform_Stock_Inward_Main_Sub SIS on SIM.Ccode=SIS.Ccode and SIM.Lcode=SIS.Lcode And SIM.TransID=SIS.TransID";
        query = query + " where SIM.Ccode='" + SessionCcode + "' And SIM.Lcode='" + SessionLcode + "' and SIS.Ccode='" + SessionCcode + "' And SIS.Lcode='" + SessionLcode + "'";

        if (str_ItemName != "")
        {
            query = query + " And SIS.ItemName='" + str_ItemName + "'";
        }
        if (str_SizeName!= "")
        {
            query = query + " And SIS.SizeName='" + str_SizeName+ "'";
        }
        if (str_fromdate != "" && str_todate != "")
        {
            query = query + " And CONVERT(Datetime,SIM.TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,SIM.TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
            query = query + " And CONVERT(Datetime,SIS.TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,SIS.TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
        }
        DT_Res = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Res.Rows.Count != 0)
        {
            ds.Tables.Add(DT_Res);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Uniform_Stock_Inward.rpt"));

            //getCompany Name
            DataTable DT = new DataTable();
            query = "";

            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + str_fromdate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + str_todate + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    private void Uniform_Stock_Adjustment_Report()
    {
        DataTable DT_Res = new DataTable();
        DataSet ds = new DataSet();

        query = "Select SAM.TransID,SAM.TransDate,SAM.Remarks,SAS.ItemName,SAS.SizeName,(Case SAS.Add_Type When 'Add' Then SAS.Qty Else 0 End) as Add_Qty,";
        query = query + " (Case SAS.Add_Type When 'Minus' Then SAS.Qty Else 0 End) as Minus_Qty,(Case SAS.Add_Type When 'Add' Then SAS.LineTotal Else 0 End) as Add_LineTotal,";
        query = query + " (Case SAS.Add_Type When 'Minus' Then SAS.LineTotal Else 0 End) as Minus_LineTotal,SAS.ItemRate from Uniform_Stock_Adjustment_Main SAM";
        query = query + " inner join Uniform_Stock_Adjustment_Main_Sub SAS on SAS.Ccode=SAM.Ccode and SAS.Lcode=SAM.Lcode And SAM.TransDate=SAS.TransDate And SAM.TransID=SAS.TransID";
        query = query + " where SAM.Ccode='" + SessionCcode + "' And SAM.Lcode='" + SessionLcode + "' and SAS.Ccode='" + SessionCcode + "' And SAS.Lcode='" + SessionLcode + "'";

        if (str_ItemName != "")
        {
            query = query + " And SAS.ItemName='" + str_ItemName + "'";
        }
        if (str_SizeName != "")
        {
            query = query + " And SAS.SizeName='" + str_SizeName + "'";
        }
        if (str_fromdate != "" && str_todate != "")
        {
            query = query + " And CONVERT(Datetime,SAM.TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,SAM.TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
            query = query + " And CONVERT(Datetime,SAS.TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,SAS.TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
        }
        DT_Res = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Res.Rows.Count != 0)
        {
            ds.Tables.Add(DT_Res);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Uniform_Stock_Adjustment.rpt"));

            //getCompany Name
            DataTable DT = new DataTable();
            query = "";

            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + str_fromdate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + str_todate + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    private void Uniform_Issue_Report()
    {
        DataTable DT_Res = new DataTable();
        DataSet ds = new DataSet();

        query = "select TransID,TransDate,WagesType,TokenNo,EmpName,ItemName,SizeName,Qty,LineTotal,ItemRate from Uniform_Issue_Main_Sub";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        if (str_ItemName != "")
        {
            query = query + " And ItemName='" + str_ItemName + "'";
        }
        if (str_SizeName != "")
        {
            query = query + " And SizeName='" + str_SizeName + "'";
        }
        if (str_fromdate != "" && str_todate != "")
        {
            query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
        }
        if (str_Wages != "")
        {
            query = query + " And WagesType='" + str_Wages + "'";
        }
        if (str_Token_No != "")
        {
            query = query + " And TokenNo='" + str_Token_No + "'";
        }

        DT_Res = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Res.Rows.Count != 0)
        {
            ds.Tables.Add(DT_Res);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Uniform_Issue.rpt"));

            //getCompany Name
            DataTable DT = new DataTable();
            query = "";

            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + str_fromdate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + str_todate + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    private void Uniform_Issue_Slip_Report()
    {
        DataTable DT_Res = new DataTable();
        DataSet ds = new DataSet();

        query = "select TransID,TransDate,WagesType,TokenNo,EmpName,ItemName,SizeName,Qty,LineTotal,ItemRate from Uniform_Issue_Main_Sub";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        if (str_ItemName != "")
        {
            query = query + " And ItemName='" + str_ItemName + "'";
        }
        if (str_SizeName != "")
        {
            query = query + " And SizeName='" + str_SizeName + "'";
        }
        if (str_fromdate != "" && str_todate != "")
        {
            query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
        }
        if (str_Wages != "")
        {
            query = query + " And WagesType='" + str_Wages + "'";
        }
        if (str_Token_No != "")
        {
            query = query + " And TokenNo='" + str_Token_No + "'";
        }
        if (str_Trans_ID != "")
        {
            query = query + " And TransID='" + str_Trans_ID + "'";
        }

        DT_Res = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Res.Rows.Count != 0)
        {
            if (DT_Res.Rows.Count < 7)
            {
                int row_c = Convert.ToInt32(7) - Convert.ToInt32(DT_Res.Rows.Count);
                row_c = row_c + 1;
                for (int i = 0; i < row_c; i++)
                {
                    DT_Res.NewRow();
                    DT_Res.Rows.Add();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TransDate"] = DT_Res.Rows[0]["TransDate"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TransID"] = DT_Res.Rows[0]["TransID"].ToString();
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["WagesType"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["TokenNo"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["EmpName"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["ItemName"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["SizeName"] = "";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["Qty"] = "0";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["ItemRate"] = "0";
                    DT_Res.Rows[DT_Res.Rows.Count - 1]["LineTotal"] = "0";


                }
            }
            ds.Tables.Add(DT_Res);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Uniform_Issue_Slip.rpt"));

            //getCompany Name
            DataTable DT = new DataTable();
            query = "";

            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + str_fromdate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + str_todate + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    private void Uniform_Issue_Return_Report()
    {
        DataTable DT_Res = new DataTable();
        DataSet ds = new DataSet();

        query = "select TransID,TransDate,WagesType,TokenNo,EmpName,ItemName,SizeName,Qty,LineTotal,ItemRate from Uniform_Issue_Return_Main_Sub";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        if (str_ItemName != "")
        {
            query = query + " And ItemName='" + str_ItemName + "'";
        }
        if (str_SizeName != "")
        {
            query = query + " And SizeName='" + str_SizeName + "'";
        }
        if (str_fromdate != "" && str_todate != "")
        {
            query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
        }
        if (str_Wages != "")
        {
            query = query + " And WagesType='" + str_Wages + "'";
        }
        if (str_Token_No != "")
        {
            query = query + " And TokenNo='" + str_Token_No + "'";
        }

        DT_Res = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Res.Rows.Count != 0)
        {
            ds.Tables.Add(DT_Res);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Uniform_Issue_Return.rpt"));

            //getCompany Name
            DataTable DT = new DataTable();
            query = "";

            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + str_fromdate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + str_todate + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    private void Uniform_Current_Stock_Report()
    {
        DataTable DT_Res = new DataTable();
        DataSet ds = new DataSet();
        DataTable AutoDataTable = new DataTable();

        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("SizeName");
        AutoDataTable.Columns.Add("OP_Qty");
        AutoDataTable.Columns.Add("OP_Val");
        AutoDataTable.Columns.Add("IN_Qty");
        AutoDataTable.Columns.Add("IN_Val");
        AutoDataTable.Columns.Add("Issue_Qty");
        AutoDataTable.Columns.Add("Issue_Val");
        AutoDataTable.Columns.Add("Issue_Rtn_Qty");
        AutoDataTable.Columns.Add("Issue_Rtn_Val");
        AutoDataTable.Columns.Add("ST_Adj_Qty");
        AutoDataTable.Columns.Add("ST_Adj_Val");

        query = "Select '1' as Group_ID, (Sum(Add_Qty)-sum(Minus_Qty)) as Curr_Qty,ItemName,SizeName,0 as OP_Qty,0 as OP_Val,0 as IN_Qty,";
        query = query + " 0 as IN_Val,0 as Issue_Qty,0 as Issue_Val,0 as Issue_Rtn_Qty,0 as Issue_Rtn_Val,0 as ST_Adj_Add_Qty,";
        query = query + " 0 as ST_Adj_Add_Val,0 as ST_Adj_Minus_Qty,0 as ST_Adj_Minus_Val";
        query = query + " from Uniform_Stock_Ledger_All";
        query = query + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        if (str_ItemName != "")
        {
            query = query + " And ItemName='" + str_ItemName + "'";
        }
        if (str_SizeName != "")
        {
            query = query + " And SizeName='" + str_SizeName + "'";
        }
        if (str_fromdate != "" && str_todate != "")
        {
            query = query + " And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
        }
        query = query + "group by ItemName,SizeName having (Sum(Add_Qty)-sum(Minus_Qty)) > 0 order by ItemName,SizeName";
        DataTable DT_Item = new DataTable();
        DT_Item = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Item.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Item.Rows.Count; i++)
            {
                string OP_Qty = "0"; string OP_Val = "0";
                string IN_Qty = "0"; string IN_Val = "0";
                string Issue_Qty = "0"; string Issue_Val = "0";
                string Issue_Rtn_Qty = "0"; string Issue_Rtn_Val = "0";
                string ST_Adj_Add_Qty = "0"; string ST_Adj_Add_Val = "0";
                string ST_Adj_Minus_Qty = "0"; string ST_Adj_Minus_Val = "0";
                //OPening Qty get
                if (str_fromdate != "" && str_todate != "")
                {
                    query = "Select isnull((Sum(Add_Qty)-sum(Minus_Qty)),0) as ST_Qty,isnull((Sum(Add_Value)-sum(Minus_Value)),0) as ST_Val from Uniform_Stock_Ledger_All";
                    query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                    query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                    query = query + " And CONVERT(Datetime,TransDate,103) < CONVERT(Datetime,'" + str_fromdate + "',103)";
                    DT_Res = objdata.RptEmployeeMultipleDetails(query);
                    if (DT_Res.Rows.Count != 0)
                    {
                        OP_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                        OP_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                    }
                }
                else
                {
                    query = "Select isnull((Sum(Add_Qty)-sum(Minus_Qty)),0) as ST_Qty,isnull((Sum(Add_Value)-sum(Minus_Value)),0) as ST_Val from Uniform_Stock_Ledger_All";
                    query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                    query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                    query = query + " And TransType='Opening'";
                    //query = query + " And CONVERT(Datetime,TransDate,103) < CONVERT(Datetime,'" + str_fromdate + "',103)";
                    DT_Res = objdata.RptEmployeeMultipleDetails(query);
                    if (DT_Res.Rows.Count != 0)
                    {
                        OP_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                        OP_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                    }
                }

                //Stock Inward
                query = "Select isnull((Sum(Add_Qty)-sum(Minus_Qty)),0) as ST_Qty,isnull((Sum(Add_Value)-sum(Minus_Value)),0) as ST_Val from Uniform_Stock_Ledger_All";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                query = query + " And TransType='Stock Inward'";
                if (str_fromdate != "" && str_todate != "")
                {
                    query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
                }
                DT_Res = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Res.Rows.Count != 0)
                {
                    IN_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                    IN_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                }
                //Stock Issue
                query = "Select isnull(sum(Minus_Qty),0) as ST_Qty,isnull(sum(Minus_Value),0) as ST_Val from Uniform_Stock_Ledger_All";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                query = query + " And TransType='Issue'";
                if (str_fromdate != "" && str_todate != "")
                {
                    query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
                }
                DT_Res = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Res.Rows.Count != 0)
                {
                    Issue_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                    Issue_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                }
                //Stock Issue Return
                query = "Select isnull(sum(Add_Qty),0) as ST_Qty,isnull(sum(Add_Value),0) as ST_Val from Uniform_Stock_Ledger_All";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                query = query + " And TransType='Issue Return'";
                if (str_fromdate != "" && str_todate != "")
                {
                    query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
                }
                DT_Res = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Res.Rows.Count != 0)
                {
                    Issue_Rtn_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                    Issue_Rtn_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                }
                //Stock Adjustment Add Qty
                query = "Select isnull(sum(Add_Qty),0) as ST_Qty,isnull(sum(Add_Value),0) as ST_Val from Uniform_Stock_Ledger_All";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                query = query + " And TransType='Stock Adjustment'";
                if (str_fromdate != "" && str_todate != "")
                {
                    query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
                }
                DT_Res = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Res.Rows.Count != 0)
                {
                    ST_Adj_Add_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                    ST_Adj_Add_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                }
                //Stock Adjustment Minus Qty
                query = "Select isnull(sum(Minus_Qty),0) as ST_Qty,isnull(sum(Minus_Value),0) as ST_Val from Uniform_Stock_Ledger_All";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + DT_Item.Rows[i]["ItemName"].ToString() + "'";
                query = query + " And SizeName='" + DT_Item.Rows[i]["SizeName"].ToString() + "'";
                query = query + " And TransType='Stock Adjustment'";
                if (str_fromdate != "" && str_todate != "")
                {
                    query = query + " And CONVERT(Datetime,TransDate,103) >= CONVERT(Datetime,'" + str_fromdate + "',103) And CONVERT(Datetime,TransDate,103) <= CONVERT(Datetime,'" + str_todate + "',103)";
                }
                DT_Res = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Res.Rows.Count != 0)
                {
                    ST_Adj_Minus_Qty = DT_Res.Rows[0]["ST_Qty"].ToString();
                    ST_Adj_Minus_Val = DT_Res.Rows[0]["ST_Val"].ToString();
                }

                //Datatable Add
                DT_Item.Rows[i]["OP_Qty"] = Convert.ToDecimal(OP_Qty);
                DT_Item.Rows[i]["OP_Val"] = Convert.ToDecimal(OP_Val);
                DT_Item.Rows[i]["IN_Qty"] = Convert.ToDecimal(IN_Qty);
                DT_Item.Rows[i]["IN_Val"] = Convert.ToDecimal(IN_Val);
                DT_Item.Rows[i]["Issue_Qty"] = Convert.ToDecimal(Issue_Qty);
                DT_Item.Rows[i]["Issue_Val"] = Convert.ToDecimal(Issue_Val);
                DT_Item.Rows[i]["Issue_Rtn_Qty"] = Convert.ToDecimal(Issue_Rtn_Qty);
                DT_Item.Rows[i]["Issue_Rtn_Val"] = Convert.ToDecimal(Issue_Rtn_Val);
                DT_Item.Rows[i]["ST_Adj_Add_Qty"] = Convert.ToDecimal(ST_Adj_Add_Qty);
                DT_Item.Rows[i]["ST_Adj_Add_Val"] = Convert.ToDecimal(ST_Adj_Add_Val);
                DT_Item.Rows[i]["ST_Adj_Minus_Qty"] = Convert.ToDecimal(ST_Adj_Minus_Qty);
                DT_Item.Rows[i]["ST_Adj_Minus_Val"] = Convert.ToDecimal(ST_Adj_Minus_Val);
            }
            ds.Tables.Add(DT_Item);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Uniform_Current_Stock_Det.rpt"));

            //getCompany Name
            DataTable DT = new DataTable();
            query = "";

            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + str_fromdate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + str_todate + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            //No Result
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
