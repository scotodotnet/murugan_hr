﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;


public partial class RptPayELCL : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SessionEpay;

    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_Division();

            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        query = "Select *from ["+SessionEpay+"]..MstEmployeeType";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            query = query + " where EmpCategory='1'";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            query = query + " where EmpCategory='2'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Load_Division()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtDivision.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtDivision.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "-Select-";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtDivision.DataTextField = "Division";
        txtDivision.DataValueField = "Division";
        txtDivision.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }

    protected void btnELView_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;



            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }
                    string EligibleWorkDays = txtEligbleDays.Text.ToString();
                    string Str_ChkBelow = "";
                    if (ChkBelow.Checked == true)
                    {
                        Str_ChkBelow = "1";
                    }
                    else { Str_ChkBelow = "0"; }

                    string Str_ChkLeft = "";
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else { Str_ChkLeft = "0"; }
                    //&CashBank=" + RdbCashBank.SelectedValue + "
                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();

                    ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + txtFinancial_Year.SelectedValue + "&fromdate=" + "" + "&ToDate=" + "" + "&Salary=" + "" + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + "" + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type + "&PFTypePost=" + Str_PFType + "&Left_Emp=" + Str_ChkLeft + "&EligbleWDays=" + EligibleWorkDays + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.SelectedItem.Text.ToString() + "&Report_Type=ELCL_REPORT" + "&BelowCheck=" + Str_ChkBelow, "_blank", "");
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
}
