﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class ImproperWorkerIn : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

   
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Department Improper Punch";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Status = Request.QueryString["Status"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                GetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_Change();
            }
        }
        
    }

    public void GetAttdDayWise_Change()
    {
        string TableName = "";

        string ng = string.Format(Date, "MM-dd-yyyy");
        Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
        DateTime date1 = Convert.ToDateTime(ng);
        DateTime date2 = date1.AddDays(1);

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("INHouse_IN");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,EM.MachineID_Encrypt from LogTime_Days LD";
        SSQL = SSQL + " inner join " + TableName + " EM on EM.MachineID = LD.MachineID";
        //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        //SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' ANd AM.LocCode='" + SessionLcode.ToString() + "'";
        //SSQL = SSQL + " And MG.CompCode='" + SessionCcode.ToString() + "' ANd MG.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And LD.Wages <> 'MANAGER' And LD.Wages <> 'STAFF' And LD.Wages <> 'Watch & Ward'";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            if (ShiftType1 == "GENERAL")
            {
                //SSQL = SSQL + " And (LD.Wages='MANAGER' or LD.Wages='STAFF' or LD.Wages='Watch & Ward')";
            }
            else
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        //if (ShiftType1 == "GENERAL")
        //{
        //    SSQL = SSQL + "  And TimeIN!=''  ";
        //}
        //else
        //{
        //    SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        //}
        SSQL = SSQL + " Order by LD.ExistingCode Asc";
        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                bool Record_Add = false;
                if (AutoDTable.Rows[i]["TimeIN"].ToString() == "")
                {
                    SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + AutoDTable.Rows[i]["MachineID_Encrypt"].ToString() + "' ";
                    SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt.Rows.Count != 0)
                    {
                        Record_Add = true;
                    }
                    else
                    {
                        Record_Add = false;
                    }
                }
                else
                {
                    Record_Add = true;
                }

                if (Record_Add == true)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();


                    DataCell.Rows[DataCell.Rows.Count - 1]["SNo"] = sno;
                    DataCell.Rows[DataCell.Rows.Count - 1]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                    if (ShiftType1 == "GENERAL")
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                        //DataCell.Rows[i]["Shift"] = "GENERAL";//AutoDTable.Rows[i]["Shift"].ToString();
                    }
                    else
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    }
                    DataCell.Rows[DataCell.Rows.Count - 1]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ShiftDate"] = Date;
                    DataCell.Rows[DataCell.Rows.Count - 1]["CompanyName"] = name.ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["LocationName"] = SessionLcode;





                    SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + AutoDTable.Rows[i]["MachineID_Encrypt"].ToString() + "' ";
                    SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt.Rows.Count != 0)
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["INHouse_IN"] = String.Format("{0:hh:mm tt}", dt.Rows[0]["TimeIN"]);
                    }
                    else
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["INHouse_IN"] = "";
                    }

                    sno += 1;
                }


            }
            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/AttendanceWithWorkingHouse_Improper.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
