﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Globalization;

public partial class Muster_Rpt_PDF : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();
    ReportDocument report = new ReportDocument();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    string ReportName = "";

    string SessionEpay = "";

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();
            TokenNo = Request.QueryString["TokenNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ReportName = Request.QueryString["ReportName"].ToString();


            if (ReportName == "DAY ATTENDANCE - BETWEEN DATES MISS-SHIFT")
            {
                Between_Date_Query_Miss_Shift_Output();
            }
            else
            {

                string SessionUserType_1 = SessionUserType;
                if (SessionUserType == "2")
                {
                    NonAdminBetweenDates();
                }
                else if (SessionUserType_1 == SessionUserType)
                {

                    Between_Date_Query_Output();
                }
                else
                {

                    string TableName = "";

                    if (Status == "Pending")
                    {
                        TableName = "Employee_Mst_New_Emp";
                    }

                    else
                    {
                        TableName = "Employee_Mst";
                    }

                    double Total_Time_get;
                    DataTable dt = new DataTable();
                    DataTable dt1 = new DataTable();
                    DataTable mLocalDS = new DataTable();

                    AutoDTable.Columns.Add("SNo");
                    AutoDTable.Columns.Add("DeptName");
                    AutoDTable.Columns.Add("MachineID");
                    AutoDTable.Columns.Add("ExistingCode");
                    AutoDTable.Columns.Add("FirstName");

                    //AutoDTable.Columns.Add("Category");
                    //AutoDTable.Columns.Add("Source");
                    //AutoDTable.Columns.Add("Grade");

                    date1 = Convert.ToDateTime(FromDate);
                    string dat = ToDate;
                    Date2 = Convert.ToDateTime(dat);

                    int daycount = (int)((Date2 - date1).TotalDays);
                    int daysAdded = 0;

                    while (daycount >= 0)
                    {
                        DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                        //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                        AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                        daycount -= 1;
                        daysAdded += 1;
                    }

                    AutoDTable.Columns.Add("Total Days");


                    SSQL = "select MachineID,isnull(FirstName,'') as FirstName,ExistingCode,DeptName,";
                    SSQL = SSQL + " OTEligible,WeekOff,DOJ from " + TableName + " ";
                    //   SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
                    //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID ";
                    //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID ";
                    //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID ";
                    SSQL = SSQL + " Where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "' and IsActive='Yes' ";
                    // SSQL = SSQL + " And DM.CompCode='" + Session["Ccode"].ToString() + "' And DM.LocCode='" + Session["Lcode"].ToString() + "'";
                    //  SSQL = SSQL + " And AM.CompCode='" + Session["Ccode"].ToString() + "' And AM.LocCode='" + Session["Lcode"].ToString() + "'";
                    // SSQL = SSQL + " And MG.CompCode='" + Session["Ccode"].ToString() + "' And MG.LocCode='" + Session["Lcode"].ToString() + "'";
                    if (TokenNo != "")
                    {
                        SSQL = SSQL + " And ExistingCode='" + TokenNo + "'";
                    }

                    if (WagesType != "-Select-")
                    {
                        SSQL = SSQL + " And Wages='" + WagesType + "'";
                    }
                    if (Division != "-Select-")
                    {
                        SSQL = SSQL + " And Division = '" + Division + "'";
                    }

                    SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName,OTEligible,WeekOff,DOJ order by ExistingCode";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);



                    intK = 0;

                    Present_WH_Count = 0;

                    int SNo = 1;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();

                        string MachineID = dt.Rows[i]["MachineID"].ToString();

                        if (MachineID == "10004")
                        {
                            string CheckVal = "12";
                        }

                        AutoDTable.Rows[intK]["SNo"] = SNo;
                        AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                        AutoDTable.Rows[intK]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                        AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                        AutoDTable.Rows[intK]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
                        string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
                        string DOJ_Date_Str = "";
                        if (dt.Rows[i]["DOJ"].ToString() != "")
                        {
                            DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                        }
                        else
                        {
                            DOJ_Date_Str = "";
                        }
                        //AutoDTable.Rows[intK]["Source"] = dt.Rows[i]["AgentName"].ToString();
                        //AutoDTable.Rows[intK]["Grade"] = dt.Rows[i]["GradeName"].ToString();



                        string OTEllible = "";
                        OTEllible = dt.Rows[i]["OTEligible"].ToString();

                        int count = 5;
                        decimal count1 = 0;
                        decimal count2 = 0;
                        decimal count3 = 0;

                        if (dt.Rows[i]["MachineID"].ToString() == "305")
                        {
                            string Val;
                            Val = "1";
                        }
                        if (dt.Rows[i]["MachineID"].ToString() == "7406")
                        {
                            string Val;
                            Val = "1";
                        }
                        DateTime Query_Date_Check = new DateTime();
                        DateTime DOJ_Date_Check_Emp = new DateTime();


                        for (int j = 0; j < daysAdded; j++)
                        {
                            DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                            string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                            if (DOJ_Date_Str != "")
                            {

                                Query_Date_Check = Convert.ToDateTime(Date_Value_Str.ToString());
                                DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                                if (DOJ_Date_Check_Emp <= Query_Date_Check)
                                {

                                    SSQL = "select Wh_Present_Count,Present from LogTime_Days where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "'";
                                    SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,103) = CONVERT(datetime,'" + Date1 + "',103)";

                                    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                                    if (dt1.Rows.Count > 0)
                                    {
                                        Present_WH_Count = Convert.ToDouble(dt1.Rows[0]["Wh_Present_Count"].ToString());
                                        // NFH_Days_Present_Count = Convert.ToDouble(dt1.Rows[0]["NFH_Present_Count"].ToString());
                                        Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                                        //  Total_Time_get = Convert.ToDouble(dt1.Rows[0]["OTHours"].ToString());


                                        //NFh Days
                                        SSQL = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                                        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (mLocalDS.Rows.Count > 0)
                                        {

                                            if (Present_Count == 1)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / X" + "</b></span>";
                                                count1 = count1 + 1;
                                                NFH_Days_Present_Count = 1;

                                            }
                                            else if (Present_Count == 0.5)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / H" + "</b></span>";

                                                count1 = count1 + Convert.ToDecimal(0.5);
                                                NFH_Days_Present_Count = 0.5;
                                            }
                                            else
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "NH / A" + "</b></span>";

                                            }

                                        }


                                        string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                                        if (Emp_WH == Attn_Date_Day)
                                        {
                                            if (Present_WH_Count == 1)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/X" + "</b></span>";

                                                count1 = count1 + 1;
                                            }
                                            else if (Present_WH_Count == 0.5)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/H" + "</b></span>";

                                                count1 = count1 + Convert.ToDecimal(0.5);
                                            }
                                            else
                                            {
                                                if (Present_Count == 0.0)
                                                {
                                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "WH/A" + "</b></span>";
                                                }

                                            }
                                        }

                                        if (Present_Count == 1)
                                        {
                                            if (NFH_Days_Present_Count != 1 && Present_WH_Count != 1.0)
                                            {

                                                AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                                count1 = count1 + 1;
                                            }
                                        }


                                        else if (Present_Count == 0.5)
                                        {
                                            if (NFH_Days_Present_Count != 0.5 && Present_WH_Count != 0.5)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";

                                                count1 = count1 + Convert.ToDecimal(0.5);
                                            }
                                        }
                                        else
                                        {
                                            if (Emp_WH != Attn_Date_Day)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                                            }
                                        }

                                        NFH_Days_Present_Count = 0;
                                        Present_WH_Count = 0;
                                        Present_Count = 0;
                                    }

                                }
                                else
                                {
                                    AutoDTable.Rows[intK][count] = "";
                                }
                            }

                            count = count + 1;
                        }

                        AutoDTable.Rows[intK]["Total Days"] = count1;

                        intK = intK + 1;

                        SNo = SNo + 1;

                        count1 = 0;
                        //count2 = 0;

                    }

                    grid.DataSource = AutoDTable;
                    grid.DataBind();
                    string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES ROSTER SHIFT REPORT.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    grid.RenderControl(htextw);
                    Response.Write("<table>");
                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    //Response.Write("<td colspan='10'>");
                    //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
                    //Response.Write("--");
                    //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    Response.Write("<tr Font-Bold='true' align='left'>");
                    Response.Write("<td colspan='10'>");
                    Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES ROSTER SHIFT REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

                    Response.Write("</td>");
                    Response.Write("</tr>");
                    //Response.Write("<tr Font-Bold='true' align='center'>");
                    //Response.Write("<td colspan='10'>");
                    //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                    //Response.Write("&nbsp;&nbsp;&nbsp;");
                    //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                    //Response.Write("</td>");
                    //Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();

                }
            }
        }
    }

    private void Between_Date_Query_Miss_Shift_Output()
    {
        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("EmpNo", typeof(int));
        AutoDTable.Columns.Add("TkNo", typeof(int));
        AutoDTable.Columns.Add("EmpName");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.ToString("dd/MM/yyyy")));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.ToString("dd/MM/yyyy") + "],'NA') as [" + dayy.ToString("dd/MM/yyyy") + "]";
                Query_Pivot = "[" + dayy.ToString("dd/MM/yyyy") + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.ToString("dd/MM/yyyy") + "],'A') as [" + dayy.ToString("dd/MM/yyyy") + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.ToString("dd/MM/yyyy") + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

        //AutoDTable.Columns.Add("Pre", typeof(decimal));
        //AutoDTable.Columns.Add("WP", typeof(decimal));
        //AutoDTable.Columns.Add("LV", typeof(decimal));
        //AutoDTable.Columns.Add("Abse", typeof(decimal));
        //AutoDTable.Columns.Add("Woff", typeof(decimal));
        //AutoDTable.Columns.Add("LOF", typeof(decimal));
        //AutoDTable.Columns.Add("TD", typeof(decimal));
                                                             
        AutoDTable.Columns.Add("Improper", typeof(decimal));
       // AutoDTable.Columns.Add("LV", typeof(decimal));
        //AutoDTable.Columns.Add("Abse", typeof(decimal));
        AutoDTable.Columns.Add("UnMatch", typeof(decimal));
        AutoDTable.Columns.Add("Match", typeof(decimal));
        //AutoDTable.Columns.Add("LOF", typeof(decimal));
        AutoDTable.Columns.Add("TD", typeof(decimal));


        string query = "";

        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff,Shift,Roster_Type,Roster_Shift,Match_Shift," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,Convert(varchar, Attn_Date_str,105) as Day_V, ";
        //query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 and (isnull(LD.LateIN,0) + isnull(LD.LateOUT,0))=0 THEN 'WH/X' WHEN LD.Present=1.0 and (isnull(LD.LateIN,0) + isnull(LateOUT,0))>0 then 'WH/H' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
        //query = query + " else (CASE WHEN LD.Present = 1.0 and (isnull(LD.LateIN,0) + isnull(LD.LateOUT,0))=0  THEN 'X' WHEN LD.Present=1.0 and (isnull(LD.LateIN,0) + isnull(LD.LateOUT,0))>0 then 'H' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
        query = query + " (CASE WHEN EM.Shift_Type='Shift Roster' then ";
        query = query + " (CASE when LD.Shift='No Shift' then '(Improper)'+'R-'+LEFT(LD.Roster_Shift,1)+RIGHT(LD.Roster_Shift,1)+'/'+'M-'+LEFT(LD.Match_Shift,1)+RIGHT(LD.Match_Shift,1) when LD.Shift!='No Shift' and LD.Shift!=LD.Roster_Shift then '(UnMatched)'+'R-'+LEFT(LD.Roster_Shift,1)+RIGHT(LD.Roster_Shift,1)+'/'+'S-'+LEFT(LD.Shift,1)+RIGHT(LD.Shift,1) when LD.Shift=LD.Roster_Shift then LEFT(LD.Shift,1)+RIGHT(LD.Shift,1) end) ";
        query = query + " When EM.Shift_Type='Shift' then (CASE When LD.Shift='No Shift' then '(Improper)'+'A-'+Left(EM.Shift_Name,1)+Right(EM.Shift_Name,1)+'/'+'M-'+Left(LD.Match_Shift,1)+Right(LD.Match_Shift,1) When LD.Shift!='No Shift' and LD.Shift!=EM.Shift_Name then '(UnMatched)'+'A-'+Left(EM.Shift_Name,1)+Right(EM.Shift_Name,1)+'/'+'M-'+Left(LD.Match_Shift,1)+Right(LD.Match_Shift,1) when LD.Shift=EM.Shift_Name then Left(LD.Shift,1)+Right(LD.Shift,1) end)";
        query = query + " End)as Presents, LD.Shift,LD.Roster_Type,LD.Roster_Shift,LD.Match_Shift";
        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        query = query + " where LD.LocCode='" + SessionLcode + "'";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
        query = query + " and LD.Present!='0.0'";
        if (TokenNo != "")
        {
            query = query + " And LD.ExistingCode='" + TokenNo + "' And EM.ExistingCode='" + TokenNo + "'";
        }
        if (WagesType != "-Select-")
        {
            query = query + " And EM.Wages='" + WagesType + "'";
            query = query + " And LD.Wages='" + WagesType + "'";
        }
        if (Division != "-Select-")
        {
            query = query + " And EM.Division = '" + Division + "'";
        }
        query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);
                                

        ////NFH Check start
        //query = "Select NFHDate from NFH_Mst where convert(datetime,NFHDate,103) >=convert(datetime,'" + FromDate + "',103)";
        //query = query + " And convert(datetime,NFHDate,103) <=convert(datetime,'" + ToDate + "',103) order by NFHDate Asc";
        //DataTable DT_NH = new DataTable();
        //DT_NH = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_NH.Rows.Count != 0)
        //{
        //    for (int ih = 0; ih < DT_NH.Rows.Count; ih++)
        //    {
        //        DateTime dayy = Convert.ToDateTime(DT_NH.Rows[ih]["NFHDate"].ToString());

        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            string DOJ_Date_Str = "";
        //            if (dt.Rows[i]["DOJ"].ToString() != "")
        //            {
        //                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
        //            }
        //            else
        //            {
        //                DOJ_Date_Str = "";
        //            }
        //            if (DOJ_Date_Str != "")
        //            {
        //                Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
        //                DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
        //                if (DOJ_Date_Check_Emp <= Query_Date_Check)
        //                {
        //                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/X" + "</b></span>"; }
        //                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/H" + "</b></span>"; }
        //                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }
        //                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/X" + "</b></span>"; }
        //                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/H" + "</b></span>"; }
        //                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }

        //                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/X"; }
        //                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/H"; }
        //                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/A"; }
        //                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/X"; }
        //                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/H"; }
        //                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/A"; }



        //                }
        //                else
        //                {
        //                    dt.Rows[i]["" + dayy.Day.ToString() + ""] = "";
        //                }
        //            }
        //        }
        //    }
        //}
       // //NFH Check End

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            decimal Improper = 0;
            decimal Leave = 0;
            decimal Hlf_Abent = 0;
            decimal absent = 0;
            decimal UnMatched = 0;
            decimal Matched = 0;
            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["S.No"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[DT_Row]["EmpNo"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[DT_Row]["TKno"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["EmpName"] = dt.Rows[i]["FirstName"].ToString();

            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString();
                        if(!dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("Improper") && !dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("UnMatched") && !dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("NA"))
                        {
                            Matched = Matched + 1;
                        }
                        else if(dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("Improper"))
                        {
                            Improper = Improper + 1;
                            AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = "<span style=color:Red><b>" + dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString() + "</b></span>";
                        }
                        else if(dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("UnMatched"))
                        {
                            UnMatched = UnMatched + 1;
                            AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = "<span style=color:'Red'><b>" + dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""] + "</b></span>";
                        }

                        //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "X"; }
                        //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H")
                        //{
                        //    AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "H";
                        //    Hlf_Abent = Hlf_Abent + Convert.ToDecimal(0.5);
                        //}
                        //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A")
                        //{
                        //    SSQL = "";
                        //    SSQL = "Select LeaveDesc from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + dt.Rows[i]["MachineID"].ToString() + "'";
                        //    SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)>=CONVERT(datetime,FromDate,103)";
                        //    SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)<=CONVERT(datetime,ToDate,103) and LeaveStatus='1'";
                        //    DataTable Leav_DT = new DataTable();
                        //    Leav_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                        //    if (Leav_DT != null && Leav_DT.Rows.Count > 0)
                        //    {
                        //        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "L";
                        //        Leave = Leave + 1;
                        //    }
                        //    else
                        //    {
                        //        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "A";
                        //        absent = absent + 1;
                        //    }
                        //}
                        //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WP"; }
                        //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WH/H"; }
                        //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "W"; }

                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = "";
                    }                                                 
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString();
                    if (!dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("Improper") && !dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("UnMatched"))
                    {
                        Matched = Matched + 1;
                    }
                    else if (dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("Improper"))
                    {
                        Improper = Improper + 1;
                        AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = "<span style=color:'Red'><b>" + dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString() + "</b></span>";
                    }
                    else if (dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""].ToString().Contains("UnMatched"))
                    {
                        UnMatched = UnMatched + 1;
                        AutoDTable.Rows[DT_Row]["" + dayy.ToString("dd/MM/yyyy") + ""] = "<span style=color:'Red'><b>" + dt.Rows[i]["" + dayy.ToString("dd/MM/yyyy") + ""] + "</b></span>";
                    }
                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "X"; }
                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H")
                    //{
                    //    AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "H";
                    //    Hlf_Abent = Hlf_Abent + Convert.ToDecimal(0.5);
                    //}
                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A")
                    //{

                    //    SSQL = "";
                    //    SSQL = "Select LeaveDesc from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + dt.Rows[i]["MachineID"].ToString() + "'";
                    //    SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)>=CONVERT(datetime,FromDate,103)";
                    //    SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)<=CONVERT(datetime,ToDate,103) and LeaveStatus='1'";
                    //    DataTable Leav_DT = new DataTable();
                    //    Leav_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //    if (Leav_DT != null && Leav_DT.Rows.Count > 0)
                    //    {
                    //        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "L";
                    //        Leave = Leave + 1;
                    //    }
                    //    else
                    //    {
                    //        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "A";
                    //        absent = absent + 1;
                    //    }
                    //}
                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WP"; }
                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WH/H"; }
                    //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "W"; }
                }

                daycount -= 1;
                daysAdded += 1;
            }

            ////Get Total Days
            //query = "Select ((isnull(sum(present),0) - isnull(sum(Wh_Present_Count),0))-SUM(Case when Wh_Present_Count=0 and (LateIN + LateOUT)>0 then 0.5 else 0 end)) as Total_Days,isnull(sum(Wh_Count),0) as Woff,isnull(sum(Wh_Present_Count),0)-SUM(Case when Wh_Present_Count=1 and (LateIN + LateOUT)>0 then 0.5 else 0 end) as WP from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            //query = query + " And LocCode='" + SessionLcode + "'";
            //query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            //query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
            //dt1 = objdata.RptEmployeeMultipleDetails(query);
            //if (dt1.Rows.Count != 0)
            //{
            //    AutoDTable.Rows[DT_Row]["Pre"] = dt1.Rows[0]["Total_Days"].ToString();
            //    AutoDTable.Rows[DT_Row]["WP"] = dt1.Rows[0]["WP"].ToString();
            //    AutoDTable.Rows[DT_Row]["Woff"] = (Convert.ToDecimal(dt1.Rows[0]["Woff"].ToString()) - Convert.ToDecimal(dt1.Rows[0]["WP"].ToString()));
            //}
            //else
            //{
                AutoDTable.Rows[DT_Row]["Improper"] = Improper;
                AutoDTable.Rows[DT_Row]["UnMatch"] = UnMatched;
                AutoDTable.Rows[DT_Row]["Match"] = Matched;
            //}


            decimal bal = 0;


            //query = "";
            //query = "Select isnull(count(present),0) as absent from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and present='0.0' and ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "' ";
            //query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            //query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
            //DataTable dts_abs = new DataTable();
            //dts_abs = objdata.RptEmployeeMultipleDetails(query);
            //if (dts_abs != null && dts_abs.Rows.Count > 0)
            //{
            //    absent = Convert.ToDecimal(dts_abs.Rows[0]["absent"]);
            //}
            //else
            //{
            //    absent = 0;
            //}

            //SSQL = "";
            //SSQL = "Select isnull(Sum(case when DATEDIFF(d,case when Convert(datetime,'" + FromDate + "',103)>=Convert(datetime,FromDate,103) then Convert(datetime,'" + FromDate + "',103) else Convert(datetime,FromDate,103) end,Case when Convert(datetime,'" + ToDate + "',103)<=Convert(datetime,ToDate,103) then Convert(datetime,'" + ToDate + "',103) else Convert(datetime,ToDate,103) end)='0' then 1 else DATEDIFF(d,case when Convert(datetime,'" + FromDate + "',103)>=Convert(datetime,FromDate,103) then Convert(datetime,'" + FromDate + "',103) else Convert(datetime,FromDate,103) end,Case when Convert(datetime,'" + ToDate + "',103)<=Convert(datetime,ToDate,103) then Convert(datetime,'" + ToDate + "',103) else Convert(datetime,ToDate,103) end +1)  end ),'0') as Lv ";
            //SSQL = SSQL + " from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            //SSQL = SSQL + " and Convert(datetime,'" + FromDate + "',103)<=CONVERT(datetime,ToDate,103)";
            ////SSQL = SSQL + " and Convert(datetime,'" + ToDate + "',103)<=CONVERT(datetime,ToDate,103)";
            //SSQL = SSQL + " and LeaveStatus='1'";
            //DataTable Lv_dt = new DataTable();
            //Lv_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (Lv_dt != null && Lv_dt.Rows.Count > 0)
            //{
            //    Leave = Convert.ToDecimal(Lv_dt.Rows[0]["Lv"]);
            //}
            //else
            //{
            //    Leave = 0;
            //}

            bal = absent + Hlf_Abent;
            //AutoDTable.Rows[DT_Row]["LV"] = Leave;
            //AutoDTable.Rows[DT_Row]["Abse"] = bal;
            //AutoDTable.Rows[DT_Row]["LOF"] = "0.0";
            AutoDTable.Rows[DT_Row]["TD"] = (Date2 - date1).TotalDays + 1;
        }

        if (AutoDTable.Rows.Count != 0)
        {
            DataSet ds = new DataSet();
            DataTable dt2 = new DataTable();
            DataTable dts = new DataTable();
            SSQL = "Select * from [" + SessionEpay + "]..AdminRights where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            dts = objdata.RptEmployeeMultipleDetails(SSQL);
            //dt2.Columns.Add("S.No");
            //dt2.Columns.Add("DeptName");
            //dt2.Columns.Add("EmpNo");
            //dt2.Columns.Add("TkNo");
            //dt2.Columns.Add("EmpName");
            //dt2.Columns.Add("D1");
            //dt2.Columns.Add("D2");
            //dt2.Columns.Add("D3");
            //dt2.Columns.Add("D4");
            //dt2.Columns.Add("D5");
            //dt2.Columns.Add("D6");
            //dt2.Columns.Add("D7");
            //dt2.Columns.Add("D8");
            //dt2.Columns.Add("D9");
            //dt2.Columns.Add("D10");
            //dt2.Columns.Add("D11");
            //dt2.Columns.Add("D12");
            //dt2.Columns.Add("D13");
            //dt2.Columns.Add("D14");
            //dt2.Columns.Add("D15");
            //dt2.Columns.Add("D16");
            //dt2.Columns.Add("D17");
            //dt2.Columns.Add("D18");
            //dt2.Columns.Add("D19");
            //dt2.Columns.Add("D20");
            //dt2.Columns.Add("D21");
            //dt2.Columns.Add("D22");
            //dt2.Columns.Add("D23");
            //dt2.Columns.Add("D24");
            //dt2.Columns.Add("D25");
            //dt2.Columns.Add("D26");
            //dt2.Columns.Add("D27");
            //dt2.Columns.Add("D28");
            //dt2.Columns.Add("D29");
            //dt2.Columns.Add("D30");
            //dt2.Columns.Add("D31");
            //dt2.Columns.Add("Per");
            //dt2.Columns.Add("WP");
            //dt2.Columns.Add("LV");
            //dt2.Columns.Add("Abse");
            //dt2.Columns.Add("Woff");
            //dt2.Columns.Add("LOF");
            //dt2.Columns.Add("TD");

            //foreach(DataRow dr in AutoDTable.Rows)
            //{
            //    dt2.Columns.Add("S.No");
            //    dt2.Columns.Add("DeptName");
            //    dt2.Columns.Add("EmpNo");
            //    dt2.Columns.Add("TkNo");
            //    dt2.Columns.Add("EmpName");
            //    dt2.Columns.Add("D1");
            //    dt2.Columns.Add("D2");
            //    dt2.Columns.Add("D3");
            //    dt2.Columns.Add("D4");
            //    dt2.Columns.Add("D5");
            //    dt2.Columns.Add("D6");
            //    dt2.Columns.Add("D7");
            //    dt2.Columns.Add("D8");
            //    dt2.Columns.Add("D9");
            //    dt2.Columns.Add("D10");
            //    dt2.Columns.Add("D11");
            //    dt2.Columns.Add("D12");
            //    dt2.Columns.Add("D13");
            //    dt2.Columns.Add("D14");
            //    dt2.Columns.Add("D15");
            //    dt2.Columns.Add("D16");
            //    dt2.Columns.Add("D17");
            //    dt2.Columns.Add("D18");
            //    dt2.Columns.Add("D19");
            //    dt2.Columns.Add("D20");
            //    dt2.Columns.Add("D21");
            //    dt2.Columns.Add("D22");
            //    dt2.Columns.Add("D23");
            //    dt2.Columns.Add("D24");
            //    dt2.Columns.Add("D25");
            //    dt2.Columns.Add("D26");
            //    dt2.Columns.Add("D27");
            //    dt2.Columns.Add("D28");
            //    dt2.Columns.Add("D29");
            //    dt2.Columns.Add("D30");
            //    dt2.Columns.Add("D31");
            //    dt2.Columns.Add("Per");
            //    dt2.Columns.Add("WP");
            //    dt2.Columns.Add("LV");
            //    dt2.Columns.Add("Abse");
            //    dt2.Columns.Add("Woff");
            //    dt2.Columns.Add("LOF");
            //    dt2.Columns.Add("TD");
            //}

            ds.Tables.Add(AutoDTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("crystal/Muster_Rpt_PDF_Miss.rpt"));

            //report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dts.Rows[0]["Cname"].ToString() + "'";
            //report.DataDefinition.FormulaFields["LocationName"].Text = "'" + dts.Rows[0]["LCode"].ToString() + "'";
            //report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            //report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            //report.DataDefinition.FormulaFields["Wages"].Text = "'" + WagesType.ToString() + "'";
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES MISS SHIFT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {

        }
    }

    public void NonAdminBetweenDates()
    {

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");

        //AutoDTable.Columns.Add("Category");
        //AutoDTable.Columns.Add("Source");
        //AutoDTable.Columns.Add("Grade");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("Total Days");

        SSQL = "select MachineID,isnull(FirstName,'') as FirstName,ExistingCode,DeptName,";
        SSQL = SSQL + " OTEligible,WeekOff,DOJ from Employee_Mst ";
        SSQL = SSQL + " Where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "' and IsActive='Yes' ";
        SSQL = SSQL + " And Eligible_PF='1' ";

        if (WagesType != "-Select-")
        {
            SSQL = SSQL + " And Wages='" + WagesType + "'";
        }
        if (TokenNo != "")
        {
            SSQL = SSQL + " And ExistingCode='" + TokenNo + "'";
        }

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }

        SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName,OTEligible order by MachineID";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);




        intK = 0;

        Present_WH_Count = 0;

        int SNo = 1;

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();

            AutoDTable.Rows[intK]["SNo"] = SNo;
            AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[intK]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[intK]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }
            //AutoDTable.Rows[intK]["Source"] = dt.Rows[i]["AgentName"].ToString();
            //AutoDTable.Rows[intK]["Grade"] = dt.Rows[i]["GradeName"].ToString();



            string OTEllible = "";
            OTEllible = dt.Rows[i]["OTEligible"].ToString();

            int count = 5;
            decimal count1 = 0;
            decimal count2 = 0;
            decimal count3 = 0;

            if (dt.Rows[i]["MachineID"].ToString() == "7404")
            {
                string Val;
                Val = "1";
            }
            if (dt.Rows[i]["MachineID"].ToString() == "7406")
            {
                string Val;
                Val = "1";
            }
            DateTime Query_Date_Check = new DateTime();
            DateTime DOJ_Date_Check_Emp = new DateTime();


            for (int j = 0; j < daysAdded; j++)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                if (DOJ_Date_Str != "")
                {

                    Query_Date_Check = Convert.ToDateTime(Date_Value_Str.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {

                        SSQL = "select Wh_Present_Count,Present from LogTime_Days where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,103) = CONVERT(datetime,'" + Date1 + "',103)";

                        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dt1.Rows.Count > 0)
                        {
                            Present_WH_Count = Convert.ToDouble(dt1.Rows[0]["Wh_Present_Count"].ToString());
                            // NFH_Days_Present_Count = Convert.ToDouble(dt1.Rows[0]["NFH_Present_Count"].ToString());
                            Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                            //  Total_Time_get = Convert.ToDouble(dt1.Rows[0]["OTHours"].ToString());


                            //NFh Days
                            SSQL = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (mLocalDS.Rows.Count > 0)
                            {

                                if (Present_Count == 1)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / X" + "</b></span>";
                                    count1 = count1 + 1;
                                    NFH_Days_Present_Count = 1;

                                }
                                else if (Present_Count == 0.5)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / H" + "</b></span>";

                                    count1 = count1 + Convert.ToDecimal(0.5);
                                    NFH_Days_Present_Count = 0.5;
                                }
                                else
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "NH / A" + "</b></span>";

                                }

                            }


                            string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                            if (Emp_WH == Attn_Date_Day)
                            {
                                if (Present_WH_Count == 1)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/X" + "</b></span>";

                                    count1 = count1 + 1;
                                }
                                else if (Present_WH_Count == 0.5)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/H" + "</b></span>";

                                    count1 = count1 + Convert.ToDecimal(0.5);
                                }
                                else
                                {
                                    if (Present_Count == 0.0)
                                    {
                                        AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "WH/A" + "</b></span>";
                                    }

                                }
                            }

                            if (Present_Count == 1)
                            {
                                if (NFH_Days_Present_Count != 1 && Present_WH_Count != 1.0)
                                {

                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                    count1 = count1 + 1;
                                }
                            }


                            else if (Present_Count == 0.5)
                            {
                                if (NFH_Days_Present_Count != 0.5 && Present_WH_Count != 0.5)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";

                                    count1 = count1 + Convert.ToDecimal(0.5);
                                }
                            }
                            else
                            {
                                if (Emp_WH != Attn_Date_Day)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                                }
                            }

                            NFH_Days_Present_Count = 0;
                            Present_WH_Count = 0;
                            Present_Count = 0;
                        }

                    }
                    else
                    {
                        AutoDTable.Rows[intK][count] = "";
                    }
                }

                count = count + 1;
            }

            AutoDTable.Rows[intK]["Total Days"] = count1;

            intK = intK + 1;

            SNo = SNo + 1;

            count1 = 0;
            //count2 = 0;

        }


        grid.DataSource = AutoDTable;
        grid.DataBind();
        string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
        //Response.Write("--");
        //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='left'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

        Response.Write("</td>");
        Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
        //Response.Write("&nbsp;&nbsp;&nbsp;");
        //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();


    }

    private void Between_Date_Query_Output()
    {
        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("EmpNo",typeof(int));
        AutoDTable.Columns.Add("TkNo", typeof(int));
        AutoDTable.Columns.Add("EmpName");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add("D" + Convert.ToString(dayy.Day.ToString()));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("Pre",typeof(decimal));
        AutoDTable.Columns.Add("WP", typeof(decimal));
        AutoDTable.Columns.Add("LV", typeof(decimal));
        AutoDTable.Columns.Add("Abse", typeof(decimal));
        AutoDTable.Columns.Add("Woff", typeof(decimal));
        AutoDTable.Columns.Add("LOF", typeof(decimal));
        AutoDTable.Columns.Add("TD", typeof(decimal));

        string query = "";

        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,DATENAME(dd, Attn_Date) as Day_V, ";
        query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 and (isnull(LD.LateIN,0) + isnull(LD.LateOUT,0))=0 THEN 'WH/X' WHEN LD.Present=1.0 and (isnull(LD.LateIN,0) + isnull(LateOUT,0))>0 then 'WH/H' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
        query = query + " else (CASE WHEN LD.Present = 1.0 and (isnull(LD.LateIN,0) + isnull(LD.LateOUT,0))=0  THEN 'X' WHEN LD.Present=1.0 and (isnull(LD.LateIN,0) + isnull(LD.LateOUT,0))>0 then 'H' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        query = query + " where LD.LocCode='" + SessionLcode + "' ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
        if (TokenNo != "")
        {
            query = query + " And LD.ExistingCode='" + TokenNo + "' And EM.ExistingCode='" + TokenNo + "'";
        }
        if (WagesType != "-Select-")
        {
            query = query + " And EM.Wages='" + WagesType + "'";
            query = query + " And LD.Wages='" + WagesType + "'";
        }
        if (Division != "-Select-")
        {
            query = query + " And EM.Division = '" + Division + "'";
        }
        query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);


        //NFH Check start
        query = "Select NFHDate from NFH_Mst where convert(datetime,NFHDate,103) >=convert(datetime,'" + FromDate + "',103)";
        query = query + " And convert(datetime,NFHDate,103) <=convert(datetime,'" + ToDate + "',103) order by NFHDate Asc";
        DataTable DT_NH = new DataTable();
        DT_NH = objdata.RptEmployeeMultipleDetails(query);
        if (DT_NH.Rows.Count != 0)
        {
            for (int ih = 0; ih < DT_NH.Rows.Count; ih++)
            {
                DateTime dayy = Convert.ToDateTime(DT_NH.Rows[ih]["NFHDate"].ToString());

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string DOJ_Date_Str = "";
                    if (dt.Rows[i]["DOJ"].ToString() != "")
                    {
                        DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                    }
                    else
                    {
                        DOJ_Date_Str = "";
                    }
                    if (DOJ_Date_Str != "")
                    {
                        Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                        DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                        if (DOJ_Date_Check_Emp <= Query_Date_Check)
                        {
                            //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/X" + "</b></span>"; }
                            //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/H" + "</b></span>"; }
                            //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }
                            //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/X" + "</b></span>"; }
                            //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/H" + "</b></span>"; }
                            //if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }

                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/X"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/H"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/A"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/X"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/H"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "NH/A"; }



                        }
                        else
                        {
                            dt.Rows[i]["" + dayy.Day.ToString() + ""] = "";
                        }
                    }
                }
            }
        }
        //NFH Check End

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            decimal Leave = 0;
            decimal Hlf_Abent = 0;
            decimal absent = 0;
            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["S.No"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[DT_Row]["EmpNo"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[DT_Row]["TKno"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["EmpName"] = dt.Rows[i]["FirstName"].ToString();

            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {
                        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "X"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") {
                            AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "H";
                            Hlf_Abent = Hlf_Abent + Convert.ToDecimal(0.5);
                        }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A")
                        {
                            SSQL = "";
                            SSQL = "Select LeaveDesc from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + dt.Rows[i]["MachineID"].ToString() + "'";
                            SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)>=CONVERT(datetime,FromDate,103)";
                            SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)<=CONVERT(datetime,ToDate,103) and LeaveStatus='1'";
                            DataTable Leav_DT = new DataTable();
                            Leav_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (Leav_DT != null && Leav_DT.Rows.Count > 0)
                            {
                                AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "L";
                                Leave = Leave + 1;
                            }
                            else
                            {
                                AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "A";
                                absent = absent + 1;
                            }
                        }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WP"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WH/H"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "W"; }

                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "";
                    }
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "X"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H")
                    {
                        AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "H";
                        Hlf_Abent = Hlf_Abent + Convert.ToDecimal(0.5);
                    }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") {
                        
                        SSQL = "";
                        SSQL = "Select LeaveDesc from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + dt.Rows[i]["MachineID"].ToString() + "'";
                        SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)>=CONVERT(datetime,FromDate,103)";
                        SSQL = SSQL + " and Convert(datetime,'" + Query_Date_Check.ToString("dd/MM/yyyy") + "',103)<=CONVERT(datetime,ToDate,103) and LeaveStatus='1'";
                        DataTable Leav_DT = new DataTable();
                        Leav_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (Leav_DT != null && Leav_DT.Rows.Count > 0)
                        {
                            AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "L";
                            Leave = Leave + 1;
                        }
                        else
                        {
                            AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "A";
                            absent = absent + 1;
                        }
                    }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WP"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "WH/H"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["D" + dayy.Day.ToString() + ""] = "W"; }
                }

                daycount -= 1;
                daysAdded += 1;
            }

            //Get Total Days
            query = "Select ((isnull(sum(present),0) - isnull(sum(Wh_Present_Count),0))-SUM(Case when Wh_Present_Count=0 and (LateIN + LateOUT)>0 then 0.5 else 0 end)) as Total_Days,isnull(sum(Wh_Count),0) as Woff,isnull(sum(Wh_Present_Count),0)-SUM(Case when Wh_Present_Count=1 and (LateIN + LateOUT)>0 then 0.5 else 0 end) as WP from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                AutoDTable.Rows[DT_Row]["Pre"] = dt1.Rows[0]["Total_Days"].ToString();
                AutoDTable.Rows[DT_Row]["WP"] = dt1.Rows[0]["WP"].ToString();
                AutoDTable.Rows[DT_Row]["Woff"] = (Convert.ToDecimal(dt1.Rows[0]["Woff"].ToString())- Convert.ToDecimal(dt1.Rows[0]["WP"].ToString()));
            }
            else
            {
                AutoDTable.Rows[DT_Row]["Pre"] = "0";
                AutoDTable.Rows[DT_Row]["WP"] = "0";
                AutoDTable.Rows[DT_Row]["Woff"] = "0";
            }

            
            decimal bal = 0;
            

            //query = "";
            //query = "Select isnull(count(present),0) as absent from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and present='0.0' and ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "' ";
            //query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            //query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
            //DataTable dts_abs = new DataTable();
            //dts_abs = objdata.RptEmployeeMultipleDetails(query);
            //if (dts_abs != null && dts_abs.Rows.Count > 0)
            //{
            //    absent = Convert.ToDecimal(dts_abs.Rows[0]["absent"]);
            //}
            //else
            //{
            //    absent = 0;
            //}

            //SSQL = "";
            //SSQL = "Select isnull(Sum(case when DATEDIFF(d,case when Convert(datetime,'" + FromDate + "',103)>=Convert(datetime,FromDate,103) then Convert(datetime,'" + FromDate + "',103) else Convert(datetime,FromDate,103) end,Case when Convert(datetime,'" + ToDate + "',103)<=Convert(datetime,ToDate,103) then Convert(datetime,'" + ToDate + "',103) else Convert(datetime,ToDate,103) end)='0' then 1 else DATEDIFF(d,case when Convert(datetime,'" + FromDate + "',103)>=Convert(datetime,FromDate,103) then Convert(datetime,'" + FromDate + "',103) else Convert(datetime,FromDate,103) end,Case when Convert(datetime,'" + ToDate + "',103)<=Convert(datetime,ToDate,103) then Convert(datetime,'" + ToDate + "',103) else Convert(datetime,ToDate,103) end +1)  end ),'0') as Lv ";
            //SSQL = SSQL + " from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            //SSQL = SSQL + " and Convert(datetime,'" + FromDate + "',103)<=CONVERT(datetime,ToDate,103)";
            ////SSQL = SSQL + " and Convert(datetime,'" + ToDate + "',103)<=CONVERT(datetime,ToDate,103)";
            //SSQL = SSQL + " and LeaveStatus='1'";
            //DataTable Lv_dt = new DataTable();
            //Lv_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (Lv_dt != null && Lv_dt.Rows.Count > 0)
            //{
            //    Leave = Convert.ToDecimal(Lv_dt.Rows[0]["Lv"]);
            //}
            //else
            //{
            //    Leave = 0;
            //}

            bal = absent + Hlf_Abent;
            AutoDTable.Rows[DT_Row]["LV"] = Leave;
            AutoDTable.Rows[DT_Row]["Abse"] = bal;
            AutoDTable.Rows[DT_Row]["LOF"] = "0.0";
            AutoDTable.Rows[DT_Row]["TD"] = (Date2 - date1).TotalDays + 1;
        }

        if (AutoDTable.Rows.Count != 0)
        {
            DataSet ds = new DataSet();
            DataTable dt2 = new DataTable();
            DataTable dts = new DataTable();
            SSQL = "Select * from [" + SessionEpay + "]..AdminRights where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            dts = objdata.RptEmployeeMultipleDetails(SSQL);
            //dt2.Columns.Add("S.No");
            //dt2.Columns.Add("DeptName");
            //dt2.Columns.Add("EmpNo");
            //dt2.Columns.Add("TkNo");
            //dt2.Columns.Add("EmpName");
            //dt2.Columns.Add("D1");
            //dt2.Columns.Add("D2");
            //dt2.Columns.Add("D3");
            //dt2.Columns.Add("D4");
            //dt2.Columns.Add("D5");
            //dt2.Columns.Add("D6");
            //dt2.Columns.Add("D7");
            //dt2.Columns.Add("D8");
            //dt2.Columns.Add("D9");
            //dt2.Columns.Add("D10");
            //dt2.Columns.Add("D11");
            //dt2.Columns.Add("D12");
            //dt2.Columns.Add("D13");
            //dt2.Columns.Add("D14");
            //dt2.Columns.Add("D15");
            //dt2.Columns.Add("D16");
            //dt2.Columns.Add("D17");
            //dt2.Columns.Add("D18");
            //dt2.Columns.Add("D19");
            //dt2.Columns.Add("D20");
            //dt2.Columns.Add("D21");
            //dt2.Columns.Add("D22");
            //dt2.Columns.Add("D23");
            //dt2.Columns.Add("D24");
            //dt2.Columns.Add("D25");
            //dt2.Columns.Add("D26");
            //dt2.Columns.Add("D27");
            //dt2.Columns.Add("D28");
            //dt2.Columns.Add("D29");
            //dt2.Columns.Add("D30");
            //dt2.Columns.Add("D31");
            //dt2.Columns.Add("Per");
            //dt2.Columns.Add("WP");
            //dt2.Columns.Add("LV");
            //dt2.Columns.Add("Abse");
            //dt2.Columns.Add("Woff");
            //dt2.Columns.Add("LOF");
            //dt2.Columns.Add("TD");

            //foreach(DataRow dr in AutoDTable.Rows)
            //{
            //    dt2.Columns.Add("S.No");
            //    dt2.Columns.Add("DeptName");
            //    dt2.Columns.Add("EmpNo");
            //    dt2.Columns.Add("TkNo");
            //    dt2.Columns.Add("EmpName");
            //    dt2.Columns.Add("D1");
            //    dt2.Columns.Add("D2");
            //    dt2.Columns.Add("D3");
            //    dt2.Columns.Add("D4");
            //    dt2.Columns.Add("D5");
            //    dt2.Columns.Add("D6");
            //    dt2.Columns.Add("D7");
            //    dt2.Columns.Add("D8");
            //    dt2.Columns.Add("D9");
            //    dt2.Columns.Add("D10");
            //    dt2.Columns.Add("D11");
            //    dt2.Columns.Add("D12");
            //    dt2.Columns.Add("D13");
            //    dt2.Columns.Add("D14");
            //    dt2.Columns.Add("D15");
            //    dt2.Columns.Add("D16");
            //    dt2.Columns.Add("D17");
            //    dt2.Columns.Add("D18");
            //    dt2.Columns.Add("D19");
            //    dt2.Columns.Add("D20");
            //    dt2.Columns.Add("D21");
            //    dt2.Columns.Add("D22");
            //    dt2.Columns.Add("D23");
            //    dt2.Columns.Add("D24");
            //    dt2.Columns.Add("D25");
            //    dt2.Columns.Add("D26");
            //    dt2.Columns.Add("D27");
            //    dt2.Columns.Add("D28");
            //    dt2.Columns.Add("D29");
            //    dt2.Columns.Add("D30");
            //    dt2.Columns.Add("D31");
            //    dt2.Columns.Add("Per");
            //    dt2.Columns.Add("WP");
            //    dt2.Columns.Add("LV");
            //    dt2.Columns.Add("Abse");
            //    dt2.Columns.Add("Woff");
            //    dt2.Columns.Add("LOF");
            //    dt2.Columns.Add("TD");
            //}

            ds.Tables.Add(AutoDTable);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Muster_Rpt_PDF.rpt"));

            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dts.Rows[0]["Cname"].ToString() + "'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'" + dts.Rows[0]["LCode"].ToString() + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            report.DataDefinition.FormulaFields["Wages"].Text = "'" + WagesType.ToString() + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
            
            //grid.DataSource = AutoDTable;
            //grid.DataBind();
            //string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
            //Response.ClearContent();
            //Response.AddHeader("content-disposition", attachment);
            //Response.ContentType = "application/ms-excel";
            //grid.HeaderStyle.Font.Bold = true;
            //System.IO.StringWriter stw = new System.IO.StringWriter();
            //HtmlTextWriter htextw = new HtmlTextWriter(stw);
            //grid.RenderControl(htextw);
            //Response.Write("<table>");
            ////Response.Write("<tr Font-Bold='true' align='center'>");
            ////Response.Write("<td colspan='10'>");
            ////Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
            ////Response.Write("--");
            ////Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
            ////Response.Write("</td>");
            ////Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='left'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

            //Response.Write("</td>");
            //Response.Write("</tr>");
            ////Response.Write("<tr Font-Bold='true' align='center'>");
            ////Response.Write("<td colspan='10'>");
            ////Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            ////Response.Write("&nbsp;&nbsp;&nbsp;");
            ////Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            ////Response.Write("</td>");
            ////Response.Write("</tr>");
            //Response.Write("</table>");
            //Response.Write(stw.ToString());
            //Response.End();
            //Response.Clear();
        }
        else
        {

        }
    }
}
