﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class AttendanceUpload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();
        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }
    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (Convert.ToInt32(txtdays.Text) == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));
            }
            if (!ErrFlag)
            {
                int Month_Mid_Total_Days_Count = 0;
                Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).TotalDays);
                Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                txtdays.Text = Month_Mid_Total_Days_Count.ToString();
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                int total_check = 0;
                decimal days = 0;
                string from_date_check = txtFrom.Text.ToString();
                string to_date_check = txtTo.Text.ToString();
                from_date_check = from_date_check.Replace("/", "-");
                to_date_check = to_date_check.Replace("/", "-");
                txtFrom.Text = from_date_check;
                txtTo.Text = to_date_check;

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));
                }
                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }
                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        


                        for (int i = 0; i < dt.Rows.Count; i++)
                        {


                            //string FD = (Convert.ToDecimal(daycount) - Convert.ToDecimal(DT.Rows[i]["Wh_Count"].ToString())).ToString();
                            string SSQL = "";
                            string Department = dt.Rows[i][4].ToString();
                            string DeptCode = "0";
                            DataTable DT_Dept = new DataTable();
                            SSQL = "Select * from Department_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And DeptName='" + Department.Trim() + "'";
                            DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (DT_Dept.Rows.Count != 0)
                            {
                                DeptCode = DT_Dept.Rows[0]["DeptCode"].ToString();
                            }

                            //string MachineID = dt.Rows[i][0].ToString();
                            //string EmpNo = dt.Rows[i][0].ToString();
                            //string ExistingCode = dt.Rows[i][1].ToString();
                            //string FirstName = dt.Rows[i][2].ToString();
                            //string WorkingDays = dt.Rows[i][4].ToString();
                            //string CL = dt.Rows[i][5].ToString();
                            //string TotDays = dt.Rows[i][16].ToString();
                            ////string AbsentDays = dt.Rows[i][7].ToString();
                            //string weekoff = dt.Rows[i][7].ToString();
                            //string NF_Count = dt.Rows[i][9].ToString();
                            //string NFH_Present = dt.Rows[i][10].ToString();
                            //string WH_Count = dt.Rows[i][7].ToString();
                            //string WH_Present = dt.Rows[i][8].ToString();
                            //string OT_Hours = dt.Rows[i][5].ToString();
                            //string FD = dt.Rows[i][13].ToString();
                            //string Canteen = dt.Rows[i][12].ToString();

                            /*Modified*/

                            string MachineID = dt.Rows[i][1].ToString();
                            string EmpNo = dt.Rows[i][1].ToString();
                            string ExistingCode = dt.Rows[i][2].ToString();
                            string FirstName = dt.Rows[i][3].ToString();
                            string WorkingDays = dt.Rows[i][5].ToString();
                            string CL = dt.Rows[i][7].ToString();
                            string TotDays = dt.Rows[i][14].ToString();
                            //string AbsentDays = dt.Rows[i][7].ToString();
                            string weekoff = dt.Rows[i][8].ToString();
                            string NF_Count = dt.Rows[i][10].ToString();
                            string NFH_Present = dt.Rows[i][10].ToString();
                            string WH_Count = dt.Rows[i][8].ToString();
                            string WH_Present = dt.Rows[i][9].ToString();
                            string OT_Hours = dt.Rows[i][6].ToString();
                            string FD = dt.Rows[i][14].ToString();
                            string Canteen = dt.Rows[i][13].ToString();

                            int j = 0;
                            if (Department == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Department. The Row Number is " + j + "')</script>");
                            }
                            else if (MachineID == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the MachineID. The Row Number is " + j + "')</script>");
                            }
                            else if (ExistingCode == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the ExistingCode. The Row Number is " + j + "')</script>");
                            }
                            else if (WorkingDays == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Days. The Row Number is " + j + "')</script>");
                            }
                            else if (OT_Hours == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the OT Hours. The Row Number is " + j + "')</script>");
                            }
                            else if (WH_Count == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Week Off. The Row Number is " + j + "')</script>");
                            }
                            else if (NF_Count == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the NFH Count. The Row Number is " + j + "')</script>");
                            }
                            else if (NFH_Present == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the NFH Present. The Row Number is " + j + "')</script>");
                            }
                            else if (WH_Present == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Week Off Present. The Row Number is " + j + "')</script>");

                            }

                            else if (FD == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Fixed Working Days. The Row Number is " + j + "')</script>");
                            }
                            else if (Canteen == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Canteen. The Row Number is " + j + "')</script>");
                            }


                            if (!ErrFlag)
                            {
                                DataTable DT_Check1 = new DataTable();
                                SSQL = "Select * from [Murugan_Epay]..AttenanceDetails where EmpNo='" + EmpNo + "' And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtFrom.Text).ToString("yyyy/MM/dd") + "',120)";
                                SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtTo.Text).ToString("yyyy/MM/dd") + "',120)";
                                SSQL = SSQL + " And Months='" + ddlMonths.SelectedItem.Text + "'";
                                SSQL = SSQL + " And Ccode='" + SessionCcode.ToString() + "' And Lcode='" + SessionLcode.ToString() + "'";
                                DT_Check1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (DT_Check1.Rows.Count != 0)
                                {
                                    SSQL = "Delete from [Murugan_Epay]..AttenanceDetails where EmpNo='" + EmpNo + "' And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtFrom.Text).ToString("yyyy/MM/dd") + "',120)";
                                    SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtTo.Text).ToString("yyyy/MM/dd") + "',120)";
                                    SSQL = SSQL + " And Months='" + ddlMonths.SelectedItem.Text + "'";
                                    SSQL = SSQL + " And Ccode='" + SessionCcode.ToString() + "' And Lcode='" + SessionLcode.ToString() + "'";
                                    objdata.RptEmployeeMultipleDetails(SSQL);
                                }


                                SSQL = "insert into ["+SessionEpay+"]..AttenanceDetails(DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,TotalDays,NFh,WorkingDays,CL,AbsentDays,";
                                SSQL = SSQL + "weekoff,FromDate,ToDate,Modeval,home,halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                SSQL = SSQL + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,Day_Shift,Night_Shift,WH_Work_Present,OTDays,";
                                SSQL = SSQL + "Ccode,Lcode)values";
                                SSQL = SSQL + "('" + DeptCode + "','" + EmpNo + "','" + ExistingCode + "',";
                                SSQL = SSQL + "'" + WorkingDays + "','" + ddlMonths.SelectedItem.Text + "','" + ddlfinance.SelectedValue + "',GetDate(),'" + TotDays.ToString() + "','" + NF_Count.ToString() + "',";
                                SSQL = SSQL + "'" + TotDays.ToString() + "','0.0','0.0','" + WH_Count + "','" + Convert.ToDateTime(txtFrom.Text).ToString("yyyy/MM/dd") + "','" + Convert.ToDateTime(txtTo.Text).ToString("yyyy/MM/dd") + "',";
                                SSQL = SSQL + "'0','0.0','0.0','0.0','" + Canteen + "','" + OT_Hours + "','" + FD + "','" + WH_Present.ToString() + "','" + NFH_Present.ToString() + "',";
                                SSQL = SSQL + "'0.0','0.0','0.0','0.0','0.0','0.0',";
                                SSQL = SSQL + "'" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
                                objdata.RptEmployeeMultipleDetails(SSQL);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                sSourceConnection.Close();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                                sSourceConnection.Close();
                            }
                            // Status = "Success";
                            sSourceConnection.Close();
                        }


                        if (ErrFlag == true)
                        {
                            sSourceConnection.Close();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                            //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }
}
