﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class SalaryConsolidate : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionRights;
    string ss = "";


    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Status = Request.QueryString["Status"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}

            GetAttdDayWise_Change();
        }
    }
    public void GetAttdDayWise_Change()
    {
        string TableName = "";

        string GrandTotal = "";
        string GrandTotalWages = "";

        int generalcount = 0;
        int shift1count = 0;
        int shift2count = 0;
        int shift3count = 0;
        int shift4count = 0;
        int shift5count = 0;
        int shift6count = 0;
        int noshiftcount = 0;

        int general = 0;
        int shift1 = 0;
        int shift2 = 0;
        int shift3 = 0;
        int shift4 = 0;
        int shift5 = 0;
        int shift6 = 0;
        int noshift = 0;

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        decimal rounded = 0;
        decimal roundedsalary = 0;
        decimal staffonedaysalary;

        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("DayWages" ,typeof(decimal));
        AutoDTable.Columns.Add("TotalMIN", typeof(decimal));
        AutoDTable.Columns.Add("Wages", typeof(decimal));
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("SubCatName");
        AutoDTable.Columns.Add("Grade"); 
        AutoDTable.Columns.Add("AgentName");
        AutoDTable.Columns.Add("OTSal", typeof(decimal));
        AutoDTable.Columns.Add("Total");

        AutoDTable.Columns.Add("GradeTotalCount");
        AutoDTable.Columns.Add("GeneralCount");
        AutoDTable.Columns.Add("Shift1Count");
        AutoDTable.Columns.Add("Shift2Count");
        AutoDTable.Columns.Add("Shift3Count");

        AutoDTable.Columns.Add("ShiftWages");

        AutoDTable.Columns.Add("OTHours",typeof(decimal));

        DataTable Shift_DS = new DataTable();

        SSQL = "select EM.MachineID,EM.Wages,EM.DeptName,EM.Shift,isnull(EM.FirstName,'') as FirstName,EM.TimeIN,EM.TimeOUT,";
        SSQL = SSQL + "DEP.SubCatName,EM.Total_Hrs,EM.Total_Hrs1,DEP.CatName,EM.BasicSalary as BaseSalary,DEP.StdWrkHrs,DEP.OTEligible,DEP.IsActive,GM.GradeName,DEP.AgentName from LogTime_Days EM  inner join Employee_Mst DEP on  EM.MachineID= DEP.MachineID ";
        SSQL = SSQL + "inner join MstGrade GM on GM.GradeID=EM.Grade and GM.LocCode=EM.LocCode";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEP.CompCode='" + SessionCcode + "' ANd DEP.LocCode='" + SessionLcode + "'";

        SSQL = SSQL + " And EM.Shift!='Leave'";
        SSQL = SSQL + " And DEP.DeptName!='CONTRACT'";

        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " And  (Present_Absent='Present' or Present_Absent='Half Day') ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "Select Count(*) from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " and TypeName='Improper'";

        DataTable imp_dt = new DataTable();
        imp_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        int impcount = 0;

        if (imp_dt.Rows.Count > 0)
        {
            impcount = Convert.ToInt32(imp_dt.Rows[0][0].ToString());
        }

        string staffsalary = "";
        string staffwages = "";

        string TempWorkHours = "";

        string[] TempWorkHourSlip;
        string Machineid = "";
        string Deptname = "";
        string workinghours = "";
        string stdwrkhrs = "";
        decimal staffonehoursalary;
        decimal Totalwages;
        staffonedaysalary = 0;

        string OThours = ""; //Edit by Narmatha
        string OTSal = "0"; //Edit by Narmatha

        string OTEligible = "";//By Selva
        string IsActive = "";//By Selva

        DataTable dt1 = new DataTable();

        if (dt.Rows.Count > 0)
        {
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                Machineid = dt.Rows[k]["MachineID"].ToString();
                staffwages = dt.Rows[k]["CatName"].ToString();
                Deptname = dt.Rows[k]["DeptName"].ToString();
                stdwrkhrs = dt.Rows[k]["StdWrkHrs"].ToString();

                OTEligible = dt.Rows[k]["OTEligible"].ToString();//By Selva
                IsActive = dt.Rows[k]["IsActive"].ToString();//By Selva

                OTSal = "0";//Edit by Narmatha

                if (stdwrkhrs == "" || stdwrkhrs == "0")
                {
                    stdwrkhrs = "8";
                }

                //Check Above 8 hours conditions
                TempWorkHours = dt.Rows[k]["Total_Hrs1"].ToString();
                TempWorkHourSlip = TempWorkHours.Split(':');

                workinghours = dt.Rows[k]["Total_Hrs"].ToString();
                OThours = "0"; //Edit by Narmatha

                if (Machineid == "920016")
                {
                    Machineid = "920016";
                }
                if (staffwages == "STAFF" || Deptname == "FITTER & ELECTRICIANS" || Deptname == "SECURITY" || Deptname == "DRIVERS")
                {
                    if (dt.Rows[k]["OTEligible"].ToString().Trim() == "1")
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();



                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(check) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 2;
                            //Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(staffonehoursalary, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                    else
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(8) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = "0";
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 8;
                            Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                }
                else
                {
                    if (dt.Rows[k]["MachineID"].ToString() == "631")
                    {
                        string pass = "";
                    }
                    DataTable da_Days = new DataTable();
                    staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                    staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                    roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                    //OT salary for Labour by Narmatha
                    string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();

                    //By Selva v
                    if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) > 8))
                    {
                        OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                        workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                    }
                    else
                    {
                        OThours = "0";
                    }
                    //By Selva ^

                    if (dt.Rows[k]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                    {
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());

                        OTSal = (Convert.ToDecimal(staffonehoursalary) * Convert.ToDecimal(OThours.ToString())).ToString();
                        OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                        //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(stdwrkhrs.ToString());
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());

                        OTSal = (Convert.ToDecimal(staffonehoursalary) * Convert.ToDecimal(OThours.ToString())).ToString();
                        OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                        //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                    }
                }


                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[k]["ExCode"] = dt.Rows[k]["MachineID"].ToString();
                AutoDTable.Rows[k]["Dept"] = dt.Rows[k]["DeptName"].ToString();
                AutoDTable.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                AutoDTable.Rows[k]["TimeIN"] = dt.Rows[k]["TimeIN"].ToString();
                AutoDTable.Rows[k]["TimeOUT"] = dt.Rows[k]["TimeOUT"].ToString();
                AutoDTable.Rows[k]["DayWages"] = rounded;

                AutoDTable.Rows[k]["TotalMIN"] = workinghours;// dt.Rows[k]["Total_Hrs"].ToString();
                AutoDTable.Rows[k]["Wages"] = roundedsalary.ToString();
                AutoDTable.Rows[k]["Shift"] = dt.Rows[k]["Shift"].ToString();
                AutoDTable.Rows[k]["SubCatName"] = dt.Rows[k]["SubCatName"].ToString();
                AutoDTable.Rows[k]["Grade"] = dt.Rows[k]["Gradename"].ToString(); 
                AutoDTable.Rows[k]["AgentName"] = dt.Rows[k]["AgentName"].ToString();
                AutoDTable.Rows[k]["OTHours"] = OThours;
                AutoDTable.Rows[k]["OTSal"] = OTSal;


                if (dt.Rows[k]["Shift"].ToString() == "GENERAL")
                {
                    generalcount = generalcount + 1;
                    general = Convert.ToInt32(Convert.ToDecimal(general) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT1")
                {
                    shift1count = shift1count + 1;
                    shift1 = Convert.ToInt32(Convert.ToDecimal(shift1) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT2")
                {
                    shift2count = shift2count + 1;
                    shift2 = Convert.ToInt32(Convert.ToDecimal(shift2) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT3")
                {
                    shift3count = shift3count + 1;
                    shift3 = Convert.ToInt32(Convert.ToDecimal(shift3) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT4")
                {
                    shift4count = shift4count + 1;
                    shift4 = Convert.ToInt32(Convert.ToDecimal(shift4) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT5")
                {
                    shift5count = shift5count + 1;
                    shift5 = Convert.ToInt32(Convert.ToDecimal(shift5) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT6")
                {
                    shift6count = shift6count + 1;
                    shift6 = Convert.ToInt32(Convert.ToDecimal(shift6) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "No Shift")
                {
                    noshiftcount = noshiftcount + 1;
                    noshift = Convert.ToInt32(Convert.ToDecimal(noshift) + Convert.ToDecimal(rounded));
                }

                GrandTotal = (Convert.ToDecimal(generalcount) + Convert.ToDecimal(shift1count) + Convert.ToDecimal(shift2count) + Convert.ToDecimal(shift3count) + Convert.ToDecimal(shift4count) + Convert.ToDecimal(shift5count) + Convert.ToDecimal(shift6count) + Convert.ToDecimal(noshiftcount)).ToString();

                GrandTotalWages = (Convert.ToDecimal(general) + Convert.ToDecimal(shift1) + Convert.ToDecimal(shift2) + Convert.ToDecimal(shift3) + Convert.ToDecimal(shift4) + Convert.ToDecimal(shift5) + Convert.ToDecimal(shift6) + Convert.ToDecimal(noshift)).ToString();

            }
        }

        SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable Depart = new DataTable();

        SSQL = "select distinct  DeptName from Department_Mst ";
        SSQL = SSQL + " where DeptName!='CONTRACT' order by DeptName Asc"; //Edit By Narmatha
        Depart = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable SubCat = new DataTable();

        SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by SubCatName ";
        SubCat = objdata.RptEmployeeMultipleDetails(SSQL);

        string CName = "";
        string LocName = "";
        SSQL = "";
        SSQL = "Select * from [" + SessionRights + "]..AdminRights where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable Company_Dt = new DataTable();
        Company_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Company_Dt.Rows.Count > 0)
        {
            CName = Company_Dt.Rows[0]["Cname"].ToString().ToUpper();
            LocName = Company_Dt.Rows[0]["Lcode"].ToString().ToUpper() + " - " + Company_Dt.Rows[0]["Location"].ToString().ToUpper();
        }


        ds.Tables.Add(AutoDTable);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/SalaryConsolidateNew.rpt"));
        report.DataDefinition.FormulaFields["GradeTotalCount"].Text = "'" + GrandTotal + "'";

        report.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + Date + "'";
        report.DataDefinition.FormulaFields["GeneralCount"].Text = "'" + generalcount + "'";
        report.DataDefinition.FormulaFields["Shift1Count"].Text = "'" + shift1count + "'";
        report.DataDefinition.FormulaFields["Shift2Count"].Text = "'" + shift2count + "'";
        report.DataDefinition.FormulaFields["Shift3Count"].Text = "'" + shift3count + "'";
        report.DataDefinition.FormulaFields["Shift4Count"].Text = "'" + shift4count + "'";
        report.DataDefinition.FormulaFields["Shift5Count"].Text = "'" + shift5count + "'";
        report.DataDefinition.FormulaFields["Shift6Count"].Text = "'" + shift6count + "'";
        report.DataDefinition.FormulaFields["NoShiftCount"].Text = "'" + noshiftcount + "'";

        report.DataDefinition.FormulaFields["Cname"].Text = "'" + "MURUGAN" + "'";
        report.DataDefinition.FormulaFields["Lname"].Text = "'" + SessionLcode + "'";

        //Get All Salary
        report.DataDefinition.FormulaFields["GrandTotalWages"].Text = "'" + GrandTotalWages + "'";
        report.DataDefinition.FormulaFields["GenWages"].Text = "'" + general + "'";
        report.DataDefinition.FormulaFields["S1Wages"].Text = "'" + shift1 + "'";
        report.DataDefinition.FormulaFields["S2Wages"].Text = "'" + shift2 + "'";
        report.DataDefinition.FormulaFields["S3Wages"].Text = "'" + shift3 + "'";
        report.DataDefinition.FormulaFields["S4Wages"].Text = "'" + shift4 + "'";
        report.DataDefinition.FormulaFields["S5Wages"].Text = "'" + shift5 + "'";
        report.DataDefinition.FormulaFields["S6Wages"].Text = "'" + shift6 + "'";
        report.DataDefinition.FormulaFields["NoShiftWages"].Text = "'" + noshift + "'";
        report.DataDefinition.FormulaFields["impcount"].Text = "'" + impcount + "'";

        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;

    }
}
