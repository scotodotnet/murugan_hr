﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstLeaderAllot.aspx.cs" Inherits="MstLeaderAllot" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Master</a></li>
            <li><a href="javascript:;">Leader Allotment</a></li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"><small>Leader Allotment</small></h1>
        <!-- end page-header -->
        <!--Form Page-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            @*<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>*@
                        </div>
                        <h4 class="panel-title">Leader Allotment</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <div class="form-horizontal form-bordered">
                            <div id="LeaderSub" style="display: block;">

                                <div class="form-group">
                                    <div class="row">
                                        <div class=" col-md-3">
                                            <label for="exampleInputName">Leader Name</label>
                                            <asp:DropDownList ID="ddlLeaderName" runat="server"
                                                class="form-control col-md-6 select2">
                                            </asp:DropDownList>

                                        </div>
                                        <div class=" col-md-3">
                                            <label for="exampleInputName">Total</label>
                                            <asp:TextBox runat="server" ID="txtTotal" Text="0" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4 col-sm-4"></label>
                                        <div class="col-md-4 col-sm-4">
                                            <div></div>
                                            <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnCancel_Click" />
                                        </div>
                                        <div class="col-md-4 col-sm-4"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-footer">
                                <div id="LeaderMain" style="display: block;">
                                    <div class="form-group">
                                        <br />
                                        <div class="row">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Leader Name</th>
                                                                <th>Total</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("AgentName")%></td>
                                                        <td><%# Eval("Total")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument='<%# Eval("AgentID")%>' CommandName="Edit">
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument='<%# Eval("AgentID")%>' CommandName="Delete"
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Bank details?');">
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Script Section -->
    <script type="text/javascript" src="assets/js/master_list_jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/master_list_jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('#example2').dataTable();
            $('#example3').dataTable();
            $('#example4').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>


</asp:Content>

