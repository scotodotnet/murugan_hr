﻿
<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Pay_Basic_Det_Mst.aspx.cs" Inherits="Pay_Basic_Det_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="Basic" runat="server">
   <ContentTemplate>
     <!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Salary Category</li>
	</ol>
	<h1 class="page-header">Salary Category</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Salary Category</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Basic</label>
								    <asp:TextBox runat="server" ID="txtBasic" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Spl. Allowance</label>
								    <asp:TextBox runat="server" ID="txtSpl" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>HRA</label>
								    <asp:TextBox runat="server" ID="txtHRA" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Uniform</label>
								    <asp:TextBox runat="server" ID="txtUniform" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Vehicle Maintenance</label>
								    <asp:TextBox runat="server" ID="txtvehicle" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Communication</label>
								    <asp:TextBox runat="server" ID="txtCommunication" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Journals & Periodical Paper</label>
								    <asp:TextBox runat="server" ID="txtJornal" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Convey</label>
								    <asp:TextBox runat="server" ID="txtConv" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Edu.Allow</label>
								    <asp:TextBox runat="server" ID="txtEdu" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                           
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>DA</label>
								    <asp:TextBox runat="server" ID="txtRAI" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Washing Allow</label>
								    <asp:TextBox runat="server" ID="txtWash" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Incentives</label>
								    <asp:TextBox runat="server" ID="txtIncentive" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-3">
							    <div class="form-group">
								    <label>Medical Allow</label>
								    <asp:TextBox runat="server" ID="txtMedi" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Others</label>
								    <asp:TextBox runat="server" ID="txtOthers" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   </ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

