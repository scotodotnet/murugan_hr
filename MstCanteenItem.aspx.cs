﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstCanteenItem : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Canteen Item Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Canteen();
            Load_Shift();
        }
        Load_Data();
    }
    private void Load_Canteen()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlCname.Items.Clear();
        query = "select CanID,CanteenName from MstCanteen where CompCode='" + SessionCcode + "'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCname.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CanteenName"] = "-Select-";
        dr["CanteenName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCname.DataTextField = "CanteenName";
        ddlCname.DataValueField = "CanID";
        ddlCname.DataBind();

    }
    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlShift.Items.Clear();
        query = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftDesc"] = "-Select-";
        dr["ShiftDesc"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();

    }
    private void Load_Data()
    {
        DataTable dt = new DataTable();

        string SQL = "Select * from MstCanteenItem where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'  ";
        dt = objdata.RptEmployeeMultipleDetails(SQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from MstCanteenItem where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and CanItemID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtItemName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Please Entre the Item Name)", true);
            return;
        }
        if (!ErrFlag)
        {
            string SSQL = "";
            SSQL = "Delete from MstCanteenItem where ItemName='" + txtItemName.Text + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "insert into MstCanteenItem(CanteenName,Shift,ItemName,Date,CompCode,LocCode)values";
            SSQL = SSQL + "('" + ddlCname.SelectedItem+ "','" + ddlShift.SelectedValue + "','" + txtItemName.Text + "',convert(DateTime,'" + txtDate.Text + "',103),'" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
           
            Load_Data();
            btnClear_Click(sender, e);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDate.Text = "";
        ddlCname.SelectedItem.Text = "-Select-";
        ddlShift.SelectedValue = "-Select-";
        txtItemName.Text = "";
    }
}