﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstUserCreation : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
   
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
  

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: User Registration";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Location();
        }
       
        Load_Data();
    }

    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlUnit.DataSource = dtempty;
        ddlUnit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlUnit.DataSource = dt;
       
        ddlUnit.DataTextField = "LCode";
        ddlUnit.DataValueField = "LCode";
        ddlUnit.DataBind();

        ddlUnit.SelectedValue = SessionLcode;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool Error = false;

        //check New Password And Conform Password
        if (txtNewPassword.Text != txtConformPassword.Text)
        {
            Error = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with New Password And Conform Password...!');", true);
        }

        //if(txtFormName.SelectedValue.ToString() == "0")
        //{
        //    Error = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Default Load Form Name...!');", true);
        //}

        bool Rights_Check = false;
        //User Rights Check
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "2", "New User");
        //    if (Rights_Check == false)
        //    {
        //        Error = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify User Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "2", "New User");
        //    if (Rights_Check == false)
        //    {
        //        Error = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New User..');", true);
        //    }
        //}

        if (!Error)
        {
            query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + txtUserID.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + txtUserID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    query = "Delete from MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + txtUserID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                DataTable FormDT = new DataTable();
                string New_Password = UTF8Encryption(txtNewPassword.Text);
                string Conform_Password = UTF8Encryption(txtConformPassword.Text);

                //string FormName = txtFormName.SelectedItem.Text.ToString();
                //string FormID = txtFormName.SelectedValue.ToString();

                string FormName = "";
                string FormID = "";

                //query = "select Link_Url from MstModuleFormDet where FormID='" + FormID + "'";
                //FormDT = objdata.RptEmployeeMultipleDetails(query);


                //Insert Compnay Details
                query = "Insert Into [" + SessionRights + "]..MstUsers(CompCode,LocationCode,UserCode,UserName,IsAdmin,Password,FormID,FormName,Link_Url)";
                query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtUserID.Text + "',";
                query = query + "'" + txtUserFullName.Text + "','" + txtUserType.SelectedValue + "','" + New_Password + "',";
                query = query + "'" + FormID + "','" + FormName + "',";
                query = query + "'')";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Insert Into MstUsers(CompCode,LocationCode,UserCode,UserName,IsAdmin,Password,FormID,FormName,Link_Url)";
                query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtUserID.Text + "',";
                query = query + "'" + txtUserFullName.Text + "','" + txtUserType.SelectedValue + "','" + New_Password + "',";
                query = query + "'" + FormID + "','" + FormName + "',";
                query = query + "'')";
                objdata.RptEmployeeMultipleDetails(query);

                //if (FormName != "Dashboard")
                //{
                //    query = query + "'" + FormDT.Rows[0]["Link_Url"].ToString() + "')";
                //}
                //else
                //{
                //    query = query + "'')";
                //}
                //objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('New User Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('New User Details Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
                txtUserID.Enabled = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('User Details Already Saved Successfully');", true);
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtUserID.Text = ""; txtUserFullName.Text = ""; txtUserType.SelectedValue= "1"; txtNewPassword.Text = ""; //txtFormName.Text = "0";
        txtConformPassword.Text = "";
        btnSave.Text = "Save";
        txtUserID.Enabled = true;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + txtUserID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtUserFullName.Text = DT.Rows[0]["UserName"].ToString();
            txtUserType.SelectedValue= DT.Rows[0]["IsAdmin"].ToString();
            txtNewPassword.Text = DT.Rows[0]["Password"].ToString();
            txtConformPassword.Text = DT.Rows[0]["Password"].ToString();

            //if (DT.Rows[0]["FormID"].ToString() != "")
            //{

            //    txtFormName.Text = DT.Rows[0]["FormID"].ToString();
            //}

            txtUserID.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select UserCode,UserName,(case IsAdmin when '1' then 'Admin' When '3' then 'Normal User' when '2' then 'IF User' when '4' then 'MD' else 'End User' end) as IsAdmin from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode <> 'Scoto'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtUserID.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check
        bool ErrFlag = false;
        //bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "2", "New User");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete User Details..');", true);
        //}
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);


                query = "Delete from MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('New User Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();
            }
        }
    }

    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void btnUnitChange_Click(object sender, EventArgs e)
    {
        Session.Remove("Lcode");
        Session["Lcode"] = ddlUnit.SelectedValue;
        Response.Redirect("MstUserCreation.aspx");
    }
}
