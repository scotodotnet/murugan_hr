﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DailyCanteenRpt : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    DataTable DT = new DataTable();
    DataSet ds = new DataSet();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Scheduled Leave Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Date = Request.QueryString["Date"].ToString();

            //SSQL = "";
            //SSQL= " Select DeptName,TotAvail,Precent,Abcent,TotalEmp,Canteen1,Canteen2,Canteen3,TotalEmp,TotalCast from (";
            //SSQL = SSQL + " Select PV.DeptName,sum(PV.TotalEmp)[TotAvail] ,isnull(sum(PV.Pre),0)[Precent],Isnull(Sum(PV.Ab),0)[Abcent], ";
            //SSQL = SSQL + " isnull(sum(PV.Pre),0)+Isnull(Sum(PV.Ab),0)[TotalEmp],isnull(Sum(PV.NONE),0)[Canteen1],";
            //SSQL = SSQL + " isnull(Sum(PV.SANTHOSH),0)[Canteen2],isnull(Sum(VELUMANI),0)[Canteen3],";
            //SSQL = SSQL + " isnull(Sum(PV.NONE),0)+isnull(Sum(PV.SANTHOSH),0)+isnull(Sum(VELUMANI),0)[TotalEmp], ";
            //SSQL = SSQL + " isnull(sum(PV.Pre),0)*isnull(PV.Amt,0)[TotalCast] From ( ";
            //SSQL = SSQL + " Select EMP.DeptName,Count(EMP.ExistingCode) [TotalEmp], ";
            //SSQL = SSQL + " Case when cast(LTD.Total_Hrs as decimal(18,2))=CAST('0.00' as decimal(18,2)) then Count(LTD.ExistingCode) else 0 end [Pre], ";
            //SSQL = SSQL + " Case when cast(LTD.Total_Hrs as decimal(18,2))=CAST('0.00' as decimal(18,2)) then Count(LTD.ExistingCode) else 0 end [Pre1], ";
            //SSQL = SSQL + " Case when cast(LTD.Total_Hrs as decimal(18,2))!=CAST('0.00' as decimal(18,2)) then Count(LTD.ExistingCode) else 0 end [Ab], ";
            //SSQL = SSQL + " CTN.CanteenName[CanteenName],CTN.Amount[Amt] from Employee_Mst EMP ";
            //SSQL = SSQL + " Inner Join LogTime_Days LTD on LTD.ExistingCode=EMP.ExistingCode and LTD.LocCode=EMP.LocCode  ";
            //SSQL = SSQL + " Inner Join MstCanteen CTN on CTN.CanID=EMP.CanteenName ";
            //SSQL = SSQL + " Where convert(DateTime,LTD.Attn_Date ,105)=convert(datetime,'" + Date + "',105) ";
            //SSQL = SSQL + " Group by EMP.DeptName,LTD.Total_Hrs,CTN.CanteenName,CTN.Amount) A  ";
            //SSQL = SSQL + " Pivot (sum(A.Pre1) for A.CanteenName in (NONE,SANTHOSH,VELUMANI)) as PV Group by PV.DeptName,PV.Amt  ";

            //SSQL = SSQL + " UNION ALL";




            //DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (DT.Rows.Count != 0)
            //{
            //    DataTable dt1 = new DataTable();
            //    SSQL = "Select * from Company_Mst ";
            //    dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            //    string name = dt1.Rows[0]["CompName"].ToString();

            //    ds.Tables.Add(DT);
            //    //ReportDocument report = new ReportDocument();
            //    report.Load(Server.MapPath("New_HR_Report/DailyCanteenRpt.rpt"));

            //    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //    report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
            //    report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode.ToString() + "'";

            //    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //    CrystalReportViewer1.ReportSource = report;
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            //}
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
