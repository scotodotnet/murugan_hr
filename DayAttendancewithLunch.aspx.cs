﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class DayAttendancewithLunch : System.Web.UI.Page
{


    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    string Datestr = "";
    string Datestr1 = "";
    int time_Check_dbl;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string status = "";
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable Lunchin_dt = new DataTable();
    DataTable Lunchout_dt = new DataTable();
        DataTable dt = new DataTable();

        string Time_IN_Str;
        string Time_OUT_Str;
        public string Left_Val(string Value, int Length)
        {
            if (Value.Length >= Length)
            {
                return Value.Substring(0, Length);
            }
            else
            {
                return Value;
            }
        }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-DAY ATTENDANCE WITH LUNCH - DAY WISE";

                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            // SessionUserType = Session["UserType"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division=Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();
            //if (SessionUserType == "1")
            //{
            //    AdminGetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                //NonAdminGetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_ChangeLunch();
            }
        }
    }

    public void GetAttdDayWise_ChangeLunch()
    {
        string TableName = "";

        if (status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

         DataCell.Columns.Add("SNo");
    DataCell.Columns.Add("Dept");
    DataCell.Columns.Add("Type");
    DataCell.Columns.Add("Shift");
 

    DataCell.Columns.Add("EmpCode");
    DataCell.Columns.Add("ExCode");
    DataCell.Columns.Add("Name");
    DataCell.Columns.Add("TimeIN");
    DataCell.Columns.Add("LOUT");
    DataCell.Columns.Add("LIN");
   
  
    DataCell.Columns.Add("TimeOUT");
    DataCell.Columns.Add("MachineID");
    DataCell.Columns.Add("Category");
    DataCell.Columns.Add("SubCategory");
    DataCell.Columns.Add("TotalMIN");
    DataCell.Columns.Add("GrandTOT");
    DataCell.Columns.Add("ShiftDate");
    DataCell.Columns.Add("CompanyName");
    DataCell.Columns.Add("LocationName");
         
                SSQL="";
                SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
                SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,EM.MachineID_Encrypt as MachineEncrpt from LogTime_Days LD";
                SSQL = SSQL + " inner join ";


                if (status == "Approval")
                {
                    SSQL = SSQL + "Employee_Mst";
                }
                else
                {
                    SSQL = SSQL + "Employee_Mst_New_Emp";
                }
                SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";
                //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
                //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
                //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
                SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
                SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
                //SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' ANd AM.LocCode='" + SessionLcode.ToString() + "'";
                //SSQL = SSQL + " And MG.CompCode='" + SessionCcode.ToString() + "' ANd MG.LocCode='" + SessionLcode.ToString() + "'";


                 if (Division != "-Select-")
                 {
                     SSQL = SSQL + " And EM.Division = '" + Division + "'";
                 }
                if (ShiftType1 != "ALL")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (Date != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
                }

                SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";

               
                AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Select * from Company_Mst ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt.Rows[0]["CompName"].ToString();
                if (AutoDTable.Rows.Count != 0)
                {
                    int sno = 1;
                    for (int i = 0; i < AutoDTable.Rows.Count; i++)
                    {
                        DataCell.NewRow();
                        DataCell.Rows.Add();
                        string machineencrypt = "";
                        machineencrypt = AutoDTable.Rows[i]["MachineEncrpt"].ToString();

                        DataCell.Rows[i]["SNo"] = sno;
                        DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                        DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                        DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                        DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                        DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                        DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                        DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                        DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                        DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                        DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                        DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                      //  DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                        DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                        DataCell.Rows[i]["ShiftDate"] = Date;
                        DataCell.Rows[i]["CompanyName"] = name.ToString();
                        DataCell.Rows[i]["LocationName"] = SessionLcode;

                        string ng = string.Format(Date, "MM-dd-yyyy");
                        Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
                        Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
                        DateTime date1 = Convert.ToDateTime(ng);
                        DateTime date2 = date1.AddDays(1);

                        Time_IN_Str = "";
                        Time_OUT_Str = "";
                        //SSQL = "Select TimeOUT from  LogTimeLunch_OUT where MachineID= '" + machineencrypt + "' and ";
                        //SSQL = SSQL + " CompCode='" + Session["Ccode"].ToString() + "' and LocCode='" + Session["Lcode"].ToString() + "' ";

                        SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + machineencrypt + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00' Order by TimeOUT ASC";

                        Lunchout_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (Lunchout_dt.Rows.Count != 0)
                        {

                            if (Lunchout_dt.Rows[0]["TimeOUT"] == "" || Lunchout_dt.Rows[0]["TimeOUT"] == "00:00")
                            {
                                DataCell.Rows[i]["LOUT"] = "";

                            }
                            else
                            {

                                DataCell.Rows[i]["LOUT"] = String.Format("{0:hh:mm tt}", Lunchout_dt.Rows[0]["TimeOUT"]);
                            }
                        }
                        else
                        {
                            DataCell.Rows[i]["LOUT"] = "";

                        }
                        SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + machineencrypt + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00" + "' Order by TimeIN desc";


                        //SSQL = "Select TimeIN from  LogTimeLunch_IN where MachineID= '" + machineencrypt + "' and ";
                        //SSQL = SSQL + " CompCode='" + Session["Ccode"].ToString() + "' and LocCode='" + Session["Lcode"].ToString() + "' ";
                        Lunchin_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (Lunchin_dt.Rows.Count != 0)
                        {
                            if (Lunchin_dt.Rows[0]["TimeIN"] == "" || Lunchin_dt.Rows[0]["TimeIN"] == "00:00")
                            {
                                DataCell.Rows[i]["LIN"] = "";


                            }
                            else
                            {

                                DataCell.Rows[i]["LIN"] = String.Format("{0:hh:mm tt}", Lunchin_dt.Rows[0]["TimeIN"]);
                            }

                        }
                        else
                        {
                            DataCell.Rows[i]["LIN"] = "";
                        }




                        //Lunch Calculation

                        String Emp_Total_Work_Time_1 = "00:00";
                        if (Lunchout_dt.Rows.Count > 1)
                        {
                            for (int tin = 0; tin < Lunchout_dt.Rows.Count; tin++)
                            {
                                Time_IN_Str = Lunchout_dt.Rows[tin][0].ToString();

                                if (Lunchin_dt.Rows.Count > tin)
                                {
                                    Time_OUT_Str = Lunchin_dt.Rows[tin][0].ToString();
                                }
                                else if (Lunchin_dt.Rows.Count > Lunchout_dt.Rows.Count)
                                {
                                    Time_OUT_Str = Lunchin_dt.Rows[Lunchin_dt.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_OUT_Str = "";
                                }

                                TimeSpan ts4;
                                ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (Lunchin_dt.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = Lunchout_dt.Rows[tin][0].ToString();
                                }

                                if (Time_IN_Str == "" || Time_OUT_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {
                                    DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                    TimeSpan ts1;
                                    ts1 = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                    string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                        ts1 = date4.Subtract(date3);
                                        ts1 = date4.Subtract(date3);
                                        ts4 = ts4.Add(ts1);
                                        Total_Time_get = ts1.Hours.ToString();
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {

                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {

                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        ts4 = ts4.Add(ts1);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        }
                                    }
                                }

                                DataCell.Rows[i]["TotalMIN"] = Emp_Total_Work_Time_1;
                               
                            
                                

                            }
                        }
                        else
                        {
                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (Lunchout_dt.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = Lunchout_dt.Rows[0][0].ToString();
                            }
                            for (int tout = 0; tout <= Lunchin_dt.Rows.Count; tout++)
                            {
                                if (Lunchin_dt.Rows.Count <= 0)
                                {
                                    Time_OUT_Str = "";
                                }
                                else
                                {
                                    Time_OUT_Str = Lunchin_dt.Rows[0][0].ToString();
                                }

                            }
                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = 0;
                            }
                            else
                            {
                                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    string s1 = ts4.Minutes.ToString();
                                    if (s1.Length == 1)
                                    {
                                        string ts_str = "0" + ts4.Minutes;
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                    }
                                    else
                                    {
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    }
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                                DataCell.Rows[i]["TotalMIN"] = Emp_Total_Work_Time_1;

                             


                            }

                        }


















                        sno += 1;
                    }


                    ds.Tables.Add(DataCell);
                    //ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("crystal/BreakAttendance.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
                }
}

    
}
