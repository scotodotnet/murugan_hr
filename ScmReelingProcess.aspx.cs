﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;

public partial class ScmReelingProcess : System.Web.UI.Page
{
    string SessionAdmin;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SessionUserID;
    string SessionUserName;
    string Query = "";

    string SSQL = "";
    bool ErrFlg = false;

    DataTable AutoDT = new DataTable();

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserID = ss;
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }

    protected void btnProcess_Click(object sender, EventArgs e)
    {
        _ValiDateFieldCheck();
       
        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Update RP set RP.ProductionAmt=((EM.BaseSalary/RP.Target) * RP.Achived) from [" + SessionEpay + "]..Reeling_Process RP";
            SSQL = SSQL + " inner join Employee_Mst EM on RP.MachineID=EM.MachineID";
            SSQL = SSQL + " where RP.Ccode='" + SessionCcode + "' and RP.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(datetime,RP.Date,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(datetime,RP.Date,103)<=Convert(datetime,'" + txtToDate.Text + "',103)";
            if (txtExistingCode.Text != "")
            {
                SSQL = SSQL + " and RP.ExistingCode='" + txtExistingCode.Text + "'";
            }
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Process Completed!!!')", true);
            btnclear_Click(sender,e);
        }
    }

    private void _ValiDateFieldCheck()
    {
        if (ddlfinance.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Select the Financial Year')", true);
            return;
        }
        if (txtFromDate.Text == "" || txtToDate.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Enter the Date Properly')", true);
            return;
        }
        else if (Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text))
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Enter the Date Properly')", true);
            return;
        }
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        ddlfinance.ClearSelection();
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtExistingCode.Text = "";
        txtEmpName.Text = "";
    }

    protected void txtExistingCode_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select (FirstName +'.'+LastName ) as EmpName from Employee_Mst where Compcode='" + SessionCcode + "' and Loccode='" + SessionLcode + "' and MachineID='" + txtExistingCode.Text + "'";
        DataTable dt_EmpName = new DataTable();
        dt_EmpName = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_EmpName.Rows.Count > 0)
        {
            txtEmpName.Text = dt_EmpName.Rows[0]["Empname"].ToString();
        }else
        {
            txtEmpName.Text = "";
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        _ValiDateFieldCheck();
        DataTable dt = new DataTable();
        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Select * from ["+SessionEpay+"]..Reeling_Process";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(datetime,Date,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(datetime,Date,103)<=Convert(datetime,'" + txtToDate.Text + "',103)";
            if (txtExistingCode.Text != "")
            {
                SSQL = SSQL + " and ExistingCode='" + txtExistingCode.Text + "'";
            }
            SSQL = SSQL + " order by cast(ExistingCode as decimal(18,2)),Date asc";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count > 0)
            {
                gridExcel.DataSource = dt;
                gridExcel.DataBind();
                string attachment = "attachment;filename=Reeling.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                string CmpName = "";
                string Cmpaddress = "";
                DataTable dt_Company = new DataTable();
                SSQL = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }


                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                gridExcel.RenderControl(htextw);
                int colSpan = gridExcel.Columns.Count;

                Response.Write("<table>");
                Response.Write("<tr align='center'>");
                Response.Write("<td colspan='" + colSpan + "'>");
                Response.Write("" + CmpName + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='" + colSpan + "'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='" + colSpan + "'>");
                Response.Write("Reeling - " + txtFromDate.Text + " - " + txtToDate.Text + "");
                Response.Write("</td>");
                Response.Write("</tr>");
             
                string Salary_Head = "";

                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='colspan='" + colSpan + "'>");
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());

                Int32 Grand_Tot_End = 0;

                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true'>");
                Response.Write("<td font-Bold='true' align='right' colspan='6'>");
                Response.Write("Grand Total");
                Response.Write("</td>");

                Grand_Tot_End = gridExcel.Rows.Count + 5;

                Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Flush();
                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
            }
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }

    protected void btnPDF_Report_Click(object sender, EventArgs e)
    {
        _ValiDateFieldCheck();
        DataTable dt = new DataTable();
        if (!ErrFlg)
        {
            ResponseHelper.Redirect("ViewReport_Trainee_Days.aspx?Token_No=" + txtExistingCode.Text + "&From_Date=" + txtFromDate.Text + "&To_Date=" + txtToDate.Text + "&RptName=" + "Reeling_Entry_Between_Date_Report", "_blank", "");
        }
    }
}