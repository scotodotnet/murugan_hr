﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstMyAttendace.aspx.cs" Inherits="MstMyAttendace" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .well1 {
            padding: 15px;
            background: #eeeeee;
            box-shadow: none;
            -webkit-box-shadow: none;
        }

        .LabelColor {
            color: #116dca;
        }

        .BorderStyle {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }

        .select2 {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }
    </style>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Employee Self Serivce Portal</a></li>
            <li class="active">My Activity</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Attendance</h1>
        <!-- end page-header -->
        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <div>
                    <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                            <h4 class="panel-title">My Attendance</h4>
                        </div>
                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <fieldset>
                                       
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label >From Date</label>
                                                        <asp:TextBox runat="server" ID="txtfromDate" class="form-control datepicker">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label >To Date</label>
                                                        <asp:TextBox runat="server" ID="txtTodate" class="form-control datepicker">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <br />
                                                <asp:Button ID="btnGetAttendace" runat="server" Text="Get" CssClass="btn btn-success" OnClick="btnGetAttendace_Click" />
                                            </div>
                                            <div class="row">
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="true">
                                                            <HeaderTemplate>
                                                                <table id="example" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Date</th>
                                                                            <th>IN</th>
                                                                            <th>OUT</th>
                                                                            <th>Total Hrs</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("Date")%></td>
                                                                    <td><%# Eval("IN")%></td>
                                                                    <td><%# Eval("OUT")%></td>
                                                                    <td><%# Eval("TotalHrs")%></td>
                                                                    <td><%# Eval("Status")%></td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->
                                            </div>
                                        </div>
                                    </fieldset>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Script Section -->
    <script type="text/javascript" src="assets/js/master_list_jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/master_list_jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('#example1').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
           
         
        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('#example1').dataTable();
                    $("#wizard").bwizard();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                   
                }
            });
        };
    </script>
</asp:Content>

