﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;


public partial class DayAttendanceSummary : System.Web.UI.Page
{

    DataTable AutoDataTable = new DataTable();
    string ModeType;
    string Date1;
    string Date2;
    string SSQL;
    string SSQL_OUT;
    DataTable dsEmployee = new DataTable();
    DataTable DataCells = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Machine_ID_Str;
    string Date_Value_Str;
    string Date_Value_Str1;
    int shiftCount = 0;
    DataTable mLocalDS = new DataTable();
    DataTable mLocalDS1 = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    DateTime fromdate;
    DateTime todate;
    string SessionUserType;
    string Division = "";


    System.Web.UI.WebControls.DataGrid GridView1 =
                 new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
        if (SessionAdmin == "2")
        {
            Response.Redirect("DayAttendanceSummary.aspx");

        }



        if (!IsPostBack)
        {

            Page.Title = "Spay Module | Report-Day Attendance Summary";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
            //li.Attributes.Add("class", "droplink active open");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();
        //SessionUserType = Session["UserType"].ToString();

        //ModeType = Request.QueryString["ModeType"].ToString();
        Date1 = Request.QueryString["Date1"].ToString();
        Division = Request.QueryString["Division"].ToString();
        //Date2 = Request.QueryString["Date2"].ToString();

        DataTable dtIPaddress = new DataTable();

        //dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());
        //if (dtIPaddress.Rows.Count > 0)
        //{
        //    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
        //    {
        //        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
        //        {
        //            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
        //        }
        //        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
        //        {
        //            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
        //        }
        //    }
        //}

        Fill_Multi_timeIN();
        Write_MultiIN();



        GridView1.HeaderStyle.Font.Bold = true;
        GridView1.DataSource = DataCells;
        GridView1.DataBind();
        string attachment = "attachment;filename=DAY ATTENDANCE SUMMARY.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        GridView1.RenderControl(htextw);
        Response.Write("<table>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td font-Bold='true' colspan='12'>");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
        //Response.Write("--");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='left'>");
        Response.Write("<td font-Bold='true' colspan='12'>");
        Response.Write("<a style=\"font-weight:bold\"> DAY ATTENDANCE SUMMARY REPORT &nbsp;&nbsp;&nbsp;" + Date1 + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td font-Bold='true' colspan='8'>");
        //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();

    }

    public void Fill_Multi_timeIN()
    {
        try
        {
            //DateTime fromdate = Convert.ToDateTime(Date1);
            //DateTime todate = Convert.ToDateTime(Date2);
            //int dayCount = (int)((todate - fromdate).TotalDays);
            //if (dayCount > 0)
            //{
            //AutoDataTable.Columns.Add("DAY ATTENDANCE SUMMARY");
            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add();


            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add("DAY ATTENDANCE SUMMARY");
            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add();
            //AutoDataTable.Columns.Add("SNo");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("Designation");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("MachineID_Encrypt");
            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("FirstName");
            AutoDataTable.Columns.Add("TimeIN1");
            AutoDataTable.Columns.Add("TimeIN2");
            AutoDataTable.Columns.Add("TimeIN3");
            AutoDataTable.Columns.Add("TimeIN4");
            AutoDataTable.Columns.Add("TimeOUT1");
            AutoDataTable.Columns.Add("TimeOUT2");
            AutoDataTable.Columns.Add("TimeOUT3");
            AutoDataTable.Columns.Add("TimeOUT4");



            SSQL = "";
            SSQL = "select isnull(DeptName,'') as [DeptName],Cast(MachineID As int) As MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName],isnull(Designation,'')as[Designation]";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            SSQL = SSQL + " and (isActive='Yes' or Convert(date,DOR,103)>=Convert(date,'" + Convert.ToDateTime(Date1) + "',103))";
            //  And EmpCatCode='" + Division + "'
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='1'";
            //}
            SSQL = SSQL + " Order By DeptName, MachineID";

            dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dsEmployee.Rows.Count <= 0)
            {
                return;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dsEmployee.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[DSVAL][0] = dsEmployee.Rows[i]["DeptName"].ToString();
                    AutoDataTable.Rows[DSVAL][1] = dsEmployee.Rows[i]["Designation"].ToString();
                    AutoDataTable.Rows[DSVAL][2] = dsEmployee.Rows[i]["MachineID"].ToString();
                    AutoDataTable.Rows[DSVAL][3] = dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                    AutoDataTable.Rows[DSVAL][4] = dsEmployee.Rows[i]["EmpNo"].ToString();
                    AutoDataTable.Rows[DSVAL][5] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                    AutoDataTable.Rows[DSVAL][6] = dsEmployee.Rows[i]["FirstName"].ToString();

                    DSVAL += 1;
                }
            }
            //}
        }
        catch (Exception e)
        {
        }
    }




    public void Write_MultiIN()
    {
        try
        {
            int intI = 1;
            int intK = 1;
            int intCol = 0;


            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("Designation");
            DataCells.Columns.Add("MachineNo");
            DataCells.Columns.Add("EmpNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("FirstName");
            DataCells.Columns.Add("TimeIN1");
            DataCells.Columns.Add("TimeIN2");
            DataCells.Columns.Add("TimeIN3");
            DataCells.Columns.Add("TimeIN4");
            DataCells.Columns.Add("TimeOUT1");
            DataCells.Columns.Add("TimeOUT2");
            DataCells.Columns.Add("TimeOUT3");
            DataCells.Columns.Add("TimeOUT4");


            for (intCol = 0; intCol < AutoDataTable.Rows.Count; intCol++)
            {
                DataCells.NewRow();
                DataCells.Rows.Add();



                DataCells.Rows[intCol]["SNo"] = intCol + 1;
                DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intCol]["DeptName"];
                DataCells.Rows[intCol]["Designation"] = AutoDataTable.Rows[intCol]["Designation"];
                DataCells.Rows[intCol]["MachineNo"] = AutoDataTable.Rows[intCol]["MachineID"];
                DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intCol]["EmpNo"];
                DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intCol]["ExistingCode"];
                DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intCol]["FirstName"];
            }

            intCol = 5;


            fromdate = Convert.ToDateTime(Date1);
            //todate = Convert.ToDateTime(Date2);
            //int dayCount = (int)((todate - fromdate).TotalDays);

            //int daysAdded = 0;

            intK = 1;
            intI = 1;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {
                intK = 1;
                int colIndex = intK;
                bool isPresent = false;
                isPresent = false;
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = "";
                string Date_Value_Str = "";
                string Date_Value_Str1 = "";

                string Time_IN_Str = "";
                string Time_Out_Str = "";
                string Total_Time_get = "";
                Int32 j = 0;
                double time_Check_dbl = 0;
                isPresent = false;


                Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                Date_Value_Str = string.Format(Date1, "dd-MM-yyyy");

                DateTime Dateval = Convert.ToDateTime(Date_Value_Str).AddDays(1);
                if (Machine_ID_Str == "ODAyNQ==")
                {
                    Machine_ID_Str = "ODAyNQ==";
                }


                if (Machine_ID_Str == "ODAyNg==")
                {
                    Machine_ID_Str = "ODAyNg==";
                }

                Date_Value_Str1 = string.Format(Convert.ToString(Dateval), "dd-MM-yyyy");

                //Shift Check shift1
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                Int32 k = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time = null;
                string Shift_End_Time = null;
                string Employee_Time = "";
                DateTime ShiftdateStartIN = default(DateTime);
                DateTime ShiftdateEndIN = default(DateTime);
                string Final_InTime = "";
                string Final_OutTime = "";
                string From_Time_Str = "";
                string To_Time_Str = "";

                DateTime EmpdateIN = default(DateTime);
                DayOfWeek day = fromdate.DayOfWeek;
                string WeekofDay = day.ToString();

                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                if (mLocalDS.Rows.Count != 0)
                {
                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                    Shift_Check_blb = false;
                    Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                    for (k = 0; k < Shift_DS.Rows.Count; k++)
                    {

                        string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                        int b = Convert.ToInt16(a.ToString());
                        Shift_Start_Time = fromdate.AddDays(b).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                        string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                        int b1 = Convert.ToInt16(a1.ToString());
                        Shift_End_Time = fromdate.AddDays(b1).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                        {
                            Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                            Shift_Check_blb = true;
                            break;
                        }
                    }

                    if (Shift_Check_blb == false)
                    {

                        SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                    }
                    else
                    {
                        if (Final_Shift == "SHIFT1")
                        {
                            SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "06:30' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                        }
                        else
                        {
                            SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                        }
                    }

                }
                else
                {
                    SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                }


                //if (Final_Shift == "SHIFT1")
                //{
                //    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                //    SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                //    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                //}

                //else
                //{
                //    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                //    SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                //    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                //}


                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL_OUT);
                if (mLocalDS.Rows.Count >= 1)
                {

                    for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                    {
                        int ival1 = 7;
                        if (ival == 0)
                        {

                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                        else if (ival == 1)
                        {
                            ival1 = 7 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                        else if (ival == 2)
                        {
                            ival1 = 7 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                        else if (ival == 3)
                        {
                            ival1 = 7 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                    }
                }



                if (mLocalDS1.Rows.Count >= 1)
                {
                    for (int ival = 0; ival < mLocalDS1.Rows.Count; ival++)
                    {
                        int ival1 = 11;

                        if (ival == 0)
                        {
                            //  ival1 = ival1 ;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                        }
                        else if (ival == 1)
                        {
                            ival1 = 11 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                        }
                        else if (ival == 2)
                        {
                            ival1 = 11 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                        }
                        else if (ival == 3)
                        {
                            ival1 = 11 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                        }
                    }
                }


                colIndex += shiftCount;
                intK += 1;
                intI += 1;
            }
        }


        catch (Exception ex)
        {
        }
    }

    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string value0 = "DAY ATTENDANCE SUMMARY" + "-" + fromdate.AddDays(0);

        string XlsPath = Server.MapPath(@"~/Add_data/DAY ATTENDANCE SUMMARY.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";


            //Response.Write("<table>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan='15'>");
            //Response.Write("" + value0 + " ");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            //Response.Write("</table>");

            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }

            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }

}
