﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="PaySlip_VoucherOrderWise.aspx.cs" Inherits="PaySlip_VoucherOrderWise" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Report</a></li>
            <li class="active">Pay Slip</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Pay Slip</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">Pay Slip Voucher Order Wise</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2"
                                            Style="width: 100%;" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                                            <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                            <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
                                            <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Employee Type</label>
                                        <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fin. Year</label>
                                        <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <!-- end row -->
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Month</label>
                                        <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>From Date</label>
                                        <asp:TextBox ID="txtFromDate" CssClass="form-control datepicker" placeholder="dd/MM/yyyy" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <asp:TextBox ID="txtToDate" CssClass="form-control datepicker" placeholder="dd/MM/yyyy" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Mode</label>
                                        <asp:RadioButtonList ID="Rbtmode" runat="server" CssClass="form-control" RepeatColumns="3" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Category" Value="1" Selected="True"  style="padding:15px"></asp:ListItem>
                                            <asp:ListItem Text="Department" Value="2" style="padding:15px"></asp:ListItem>
                                            <asp:ListItem Text="Leader Name" Value="3" style="padding:15px"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>

                                <!-- begin row -->
                                <div class="row">

                                    <div class="col-md-12" align="center">
                                        <br />
                                        <asp:Button runat="server" ID="BtnVoucher" Text="Report" class="btn btn-success" ValidationGroup="Validate_PayField" OnClick="BtnVoucher_Click" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
