﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;

public partial class ScmReelingWorkloadEntry : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SessionUserID;
    string SessionUserName;

    string SSQL = "";
    bool ErrFlg = false;

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserID = ss;
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            Load_ExistingCode();
            ddlfinance_SelectedIndexChanged(sender,e);
        }
    }
    private void Load_ExistingCode()
    {
        SSQL = "";
        SSQL = "select * from Employee_Mst where Compcode='" + SessionCcode + "' and Loccode='" + SessionLcode + "' and isActive='Yes'";
        DataTable emp_dt = new DataTable();
        emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (emp_dt.Rows.Count > 0)
        {
            ddlExistingCode.DataSource = emp_dt;
            ddlExistingCode.DataTextField = "ExistingCode";
            ddlExistingCode.DataValueField = "MachineID";
            ddlExistingCode.DataBind();
            ddlExistingCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        ddlfinance.ClearSelection();
        txtDate.Text = string.Empty;
        ddlExistingCode.ClearSelection();
        ddlTarget.ClearSelection();
        txtWorkloadAchive.Text = string.Empty;
        txtEmpName.Text = string.Empty;

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlfinance.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Choose the Finacial Year')", true);
            return;
        }
        if (ddlExistingCode.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Choose the Existing Code')", true);
            return;
        }
        if (txtDate.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Enter the Date')", true);
            return;
        }
        if (ddlTarget.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Choose the Target Workload')", true);
            return;
        }
        if (txtWorkloadAchive.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Enter the Workload Achivement')", true);
            return;
        }
        if (!ErrFlg)
        {
            GetIPAndName getIPAndName = new GetIPAndName();
            string _IP = getIPAndName.GetIP();
            string _HostName = getIPAndName.GetName();

            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..Reeling_Process where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MachineID='" + ddlExistingCode.SelectedValue + "'";
            SSQL = SSQL + " and Date='" + txtDate.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "insert into [" + SessionEpay + "]..Reeling_Process (Ccode,Lcode,FinYearVal,FinYearCode,ExistingCode,MachineID,Name,Date,Target,WorkLoad_Code,Achived,ProductionAmt,CreatedOn,CreatedBy,HostIP,HostName)";
            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlfinance.SelectedItem.Text + "','" + ddlfinance.SelectedItem.Value + "','" + ddlExistingCode.SelectedItem.Text + "','" + ddlExistingCode.SelectedValue + "'";
            SSQL = SSQL + ",'" + txtEmpName.Text + "','" + txtDate.Text + "','" + ddlTarget.SelectedItem.Text + "','" + ddlTarget.SelectedValue + "','" + txtWorkloadAchive.Text + "','0','" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "','" + SessionUserID + "','" + _IP + "','" + _HostName + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Entry Saved!!!')", true);
            Clear_Entry();
        }
    }

    private void Clear_Entry()
    {
        ddlExistingCode.ClearSelection();
        txtEmpName.Text = "";
        txtWorkloadAchive.Text = "0";
    }

    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlfinance.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select Target,Code from [" + SessionEpay + "]..MstReeling where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + ddlfinance.SelectedValue + "' order by Code asc";
            ddlTarget.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlTarget.DataTextField = "Target";
            ddlTarget.DataValueField = "Code";
            ddlTarget.DataBind();
        }else
        {
            ddlTarget.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void ddlExistingCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlConnection cn = new SqlConnection(constr);
        SSQL = "";
        SSQL = "Select (FirstName +'.'+LastName ) as EmpName from Employee_Mst where Compcode='" + SessionCcode + "' and Loccode='" + SessionLcode + "' and MachineID='" + ddlExistingCode.SelectedValue + "'";
        SqlCommand cmd = new SqlCommand(SSQL,cn);
        cn.Open();
        txtEmpName.Text = cmd.ExecuteScalar().ToString();
        cn.Close();
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ddlExistingCode.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Choose the Existing Code')", true);
            return;
        }
        if (txtDate.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Enter the Date')", true);
            return;
        }

        if (!ErrFlg)
        {
            
            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..Reeling_Process where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MachineID='" + ddlExistingCode.SelectedValue + "'";
            SSQL = SSQL + " and Date='" + txtDate.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Entry Deleted!!!')", true);
            Clear_Entry();
        }
    }
}