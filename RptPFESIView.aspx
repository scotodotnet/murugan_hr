﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptPFESIView.aspx.cs" Inherits="RptPFESIView" %>

<%@ Register assembly="CrystalDecisions.Web, Version=12.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <asp:Panel runat="server" ID="PnltrPF" Visible="false">
                    <!--  REGULAR SALARY Details GridView Start  -->
                    <tr id="trPF" runat="server" visible="false">
                        <td colspan="4">
                            <asp:GridView ID="GVPF" runat="server" AutoGenerateColumns="false"
                                OnSelectedIndexChanged="GVPF_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>UAN</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbldept" runat="server" Text='<%# Eval("UAN") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>MEMBER NAME</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>GROSS WAGES</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblExist" runat="server" Text='<%# Eval("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>EPF WAGES</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%# Eval("EPFWages") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>EPS WAGES</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("EPSWages") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>EDLIWages</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("EDLIWages") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>EPF CONTRI REMITTED</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcl" runat="server" Text='<%# Eval("EPF") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>EPS CONTRI REMITTED</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcl" runat="server" Text='<%# Eval("EPS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>EPF EPS DIFF REMITTED</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("EPFDiffer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>NCP DAYS</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBase" runat="server" Text='<%# Eval("Leave") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>REFUND OF ADVANCES</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBasicAndDA" runat="server" Text='0'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <!--  REGULAR SALARY Details GridView End  -->
                </asp:Panel>

                <asp:Panel runat="server" ID="PaneltrGVESI" Visible="false">
                    <!--  REGULAR SALARY Details GridView Start  -->
                    <tr id="trGVESI" runat="server" visible="false">
                        <td colspan="4">
                            <asp:GridView ID="GVESI" runat="server" AutoGenerateColumns="false"
                                OnSelectedIndexChanged="GVESI_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>IP NUMBER</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbldept" runat="server" Text='<%# Eval("ESINO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>IP NAME</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>No of Days for which wages paid/payable during the month</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblExist" runat="server" Text='<%# Eval("Days") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderTemplate>Total Monthly Wages</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblExist" runat="server" Text='<%# Eval("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Reason Code for Zero workings days(numeric only;<br />
                                            provide 0 for all other reasons- Click on the link for reference)</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='0'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>Last Working Day</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <!--  REGULAR SALARY Details GridView End  -->
                </asp:Panel>
            </table>
        </div>
    </form>


</body>
</html>
