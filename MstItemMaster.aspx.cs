﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstItemMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Item Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data_Grade();
    }
    private void Load_Data_Grade()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstItems";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "select * from MstItems where Auto_ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstItems where Auto_ID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Grade();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstItems where Auto_ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            txtAuto_ID.Value = DT.Rows[0]["Auto_ID"].ToString();
            txtItemName.Text = DT.Rows[0]["ItemName"].ToString();
            txtAmount.Text = DT.Rows[0]["Amount"].ToString();

            btnSave.Text = "Update";
        }
        else
        {

            txtItemName.Text = "";
            txtAmount.Text = "";
            btnSave.Text = "Save";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();


        if (btnSave.Text == "Update")
        {
            SaveMode = "Update";

            query = "Update MstItems set ItemName='" + txtItemName.Text.ToUpper() + "',Amount='" + txtAmount.Text + "' where Auto_Id='" + txtAuto_ID.Value + "'";
            objdata.RptEmployeeMultipleDetails(query);

        }
        else
        {
            query = "select * from MstItems where ItemName='" + txtItemName.Text.ToUpper() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Already";
            }
            else
            {
                SaveMode = "Insert";
                query = "Insert into MstItems (CompCode,LocCode,ItemName,Amount)";
                query = query + "values('"+SessionCcode.ToString()+ "','" + SessionLcode.ToString() + "','" + txtItemName.Text.ToUpper() + "','" + txtAmount.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

        }

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Saved Successfully...!');", true);
        }
        else if (SaveMode == "Already")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Already Exists!');", true);
        }

        Load_Data_Grade();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtAuto_ID.Value = "";
        txtItemName.Text = "";
        txtAmount.Text = "";
        btnSave.Text = "Save";
    }
}