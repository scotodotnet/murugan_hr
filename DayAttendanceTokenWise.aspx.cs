﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class DayAttendanceTokenWise : System.Web.UI.Page
{


    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
           // SessionUserType = Session["UserType"].ToString();

            Status = Request.QueryString["Status"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
         Division=Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    AdminGetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                //NonAdminGetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_Change();
            }
        }
    }
    public void GetAttdDayWise_Change()
    {
        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join " + TableName + " EM on EM.MachineID = LD.MachineID";
        //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        //SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' ANd AM.LocCode='" + SessionLcode.ToString() + "'";
        //SSQL = SSQL + " And MG.CompCode='" + SessionCcode.ToString() + "' ANd MG.LocCode='" + SessionLcode.ToString() + "'";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            if (ShiftType1 == "GENERAL")
            {
                SSQL = SSQL + " And (LD.Wages='MANAGER' or LD.Wages='STAFF' or LD.Wages='Watch & Ward')";
            }
            else
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (ShiftType1 == "GENERAL")
        {
            SSQL = SSQL + "  And TimeIN!=''  ";
        }
        else
        {
            SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        }
        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                if (ShiftType1 == "GENERAL")
                {
                    DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    //DataCell.Rows[i]["Shift"] = "GENERAL";//AutoDTable.Rows[i]["Shift"].ToString();
                }
                else
                {
                    DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                }
                DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["ShiftDate"] = Date;
                DataCell.Rows[i]["CompanyName"] = name.ToString();
                DataCell.Rows[i]["LocationName"] = SessionLcode;


                sno += 1;
            }


            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Attendance_TokenNoWise.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }






















        //AutoDTable.Columns.Add("SNo");
        //AutoDTable.Columns.Add("Dept");
        //AutoDTable.Columns.Add("Type");
        //AutoDTable.Columns.Add("Shift");


        //AutoDTable.Columns.Add("EmpCode");
        //AutoDTable.Columns.Add("ExCode");
        //AutoDTable.Columns.Add("Name");
        //AutoDTable.Columns.Add("TimeIN");
        //AutoDTable.Columns.Add("TimeOUT");
        //AutoDTable.Columns.Add("MachineID");
        //AutoDTable.Columns.Add("Category");
        //AutoDTable.Columns.Add("SubCategory");
        //AutoDTable.Columns.Add("TotalMIN");
        //AutoDTable.Columns.Add("GrandTOT");
        //AutoDTable.Columns.Add("ShiftDate");
        //AutoDTable.Columns.Add("CompanyName");
        //AutoDTable.Columns.Add("LocationName");

        //DataCell.Columns.Add("CompanyName");
        //DataCell.Columns.Add("LocationName");
        //DataCell.Columns.Add("ShiftDate");
        //DataCell.Columns.Add("SNo");
        //DataCell.Columns.Add("Dept");
        //DataCell.Columns.Add("Type");
        //DataCell.Columns.Add("Shift");
        //DataCell.Columns.Add("Category");
        //DataCell.Columns.Add("SubCategory");
        //DataCell.Columns.Add("EmpCode");
        //DataCell.Columns.Add("ExCode");
        //DataCell.Columns.Add("Name");
        //DataCell.Columns.Add("TimeIN");
        //DataCell.Columns.Add("TimeOUT");

        //DataCell.Columns.Add("TotalMIN");
        //DataCell.Columns.Add("GrandTOT");
        //DataCell.Columns.Add("MachineID");
        //DataCell.Columns.Add("PrepBy");
        //DataCell.Columns.Add("PrepDate");






        //DataTable mLocalDS = new DataTable();

        //string ng = string.Format(Date, "MM-dd-yyyy");
        //Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        //Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
        //DateTime date1 = Convert.ToDateTime(ng);
        //DateTime date2 = date1.AddDays(1);

        //SSQL = "";
        //SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        //SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        //SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
        //SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        //if (ShiftType1 != "ALL")
        //{
        //    SSQL = SSQL + " And shiftDesc='" + ShiftType1 + "'";
        //}
        //SSQL = SSQL + " Order By shiftDesc";

        //mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (SessionLcode == "UNIT I")
        //{
        //    for (int i = 0; i < mLocalDS.Rows.Count; i++)
        //    {
        //        if (mLocalDS.Rows[i]["shiftDesc"] == "SHIFT10")
        //        {
        //            mLocalDS.Rows[i]["StartIN_Days"] = -1;
        //            mLocalDS.Rows[i]["EndIN_Days"] = 0;
        //        }
        //    }

        //}

        //int mStartINRow = 0;
        //int mStartOUTRow = 0;

        //for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        //{
        //    if (AutoDTable.Rows.Count <= 1)
        //    {
        //        mStartOUTRow = 0;
        //    }
        //    else
        //    {
        //        mStartOUTRow = AutoDTable.Rows.Count - 1;
        //    }

        //    if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
        //    {
        //        ShiftType = "GENERAL";

        //    }
        //    else
        //    {
        //        ShiftType = "SHIFT";
        //    }
        //    string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
        //    double sINdays = Convert.ToDouble(sIndays_str);
        //    string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
        //    double eINdays = Convert.ToDouble(eIndays_str);
        //    if (ShiftType == "GENERAL")
        //    {
        //        SSQL = "";
        //        SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join " + TableName + " EM on EM.MachineID_Encrypt=LT.MachineID ";
        //        SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
        //        SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "' ";



        //        SSQL = SSQL + "And LT.TimeIN >='" + date1.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
        //        SSQL = SSQL + "And LT.TimeIN <='" + date2.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
        //        if (Division != "-Select-")
        //        {
        //            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        //        }


        //        SSQL = SSQL + " AND EM.ShiftType='" + ShiftType + "' And EM.Compcode='" + SessionCcode + "'";
        //        SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
        //        SSQL = SSQL + " Group By LT.MachineID";
        //        SSQL = SSQL + " Order By Min(LT.TimeIN)";



        //    }

        //    else if (ShiftType == "SHIFT")
        //    {

        //        SSQL = "";
        //        SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join " + TableName + " EM on EM.MachineID_Encrypt=LT.MachineID ";
        //        SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
        //        SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'";
        //        SSQL = SSQL + " And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";



        //        if (Division != "-Select-")
        //        {
        //            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        //        }


        //        SSQL = SSQL + " And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "' ";
        //        SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";

        //        SSQL = SSQL + " Group By LT.MachineID";
        //        SSQL = SSQL + " Order By Min(LT.TimeIN)";
        //    }
        //    else
        //    {
        //        SSQL = "";
        //        SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join " + TableName + " EM on EM.MachineID_Encrypt=LT.MachineID ";
        //        SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
        //        SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
        //        if (Division != "-Select-")
        //        {
        //            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        //        }


        //        SSQL = SSQL + "  And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "' ";
        //        SSQL = SSQL + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "' ";
        //        SSQL = SSQL + " AND EM.ShiftType='" + ShiftType + "' and EM.Compcode='" + SessionCcode + "'  And EM.LocCode= '" + SessionLcode + "' ";
        //        SSQL = SSQL + " Group By LT.MachineID Order By Min(LT.TimeIN)";




        //    }



        //    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);


        //    if (mDataSet.Rows.Count > 0)
        //    {
        //        string MachineID;

        //        for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
        //        {

        //            Boolean chkduplicate = false;
        //            chkduplicate = false;

        //            for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
        //            {
        //                string id = mDataSet.Rows[iRow]["MachineID"].ToString();

        //                if (id == AutoDTable.Rows[ia][9].ToString())
        //                {
        //                    chkduplicate = true;
        //                }
        //            }




        //            MachineID = UTF8Decryption(mDataSet.Rows[iRow]["MachineID"].ToString());
        //            // 'Get Employee Week OF DAY
        //            DataTable DS_WH = new DataTable();
        //            string Emp_WH_Day = "";
        //            string DOJ_Date_Str = "";


        //            // 'Check SHIFT CHANGE TIME DETAILS
        //            string Query = "";
        //            DataTable Shift_Change_DS = new DataTable();

        //            Query = "Select * from ShiftChange_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        //            Query = Query + " And ShiftDate='" + Date + "' and Machine_Encrypt='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "'";
        //            Shift_Change_DS = objdata.RptEmployeeMultipleDetails(Query);
        //            if (Shift_Change_DS.Rows.Count != 0)
        //            {
        //                if (ShiftType1 == "ALL" || ShiftType == Shift_Change_DS.Rows[0]["ShiftDesc"])
        //                {

        //                }
        //                else
        //                {
        //                    chkduplicate = true;
        //                }

        //            }

        //            if (chkduplicate == false)
        //            {
        //                AutoDTable.NewRow();
        //                AutoDTable.Rows.Add();

        //                AutoDTable.Rows[AutoDTable.Rows.Count - 1][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();

        //                if (ShiftType == "SHIFT")
        //                {
        //                    string str = mDataSet.Rows[iRow][1].ToString();

        //                    AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);

        //                }
        //                else
        //                {

        //                    AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);

        //                }
        //                MachineID = (UTF8Decryption(mDataSet.Rows[iRow]["MachineID"].ToString()));

        //                AutoDTable.Rows[AutoDTable.Rows.Count - 1][9] = MachineID.ToString();


        //            }
        //            mStartINRow += 1;
        //            ColumnName = "";
        //        }

        //    }


        //    SSQL = "";
        //    SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";


        //    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
        //    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";


        //    if (mLocalDS.Rows[iTabRow]["shiftDesc"] == "SHIFT1")
        //    {
        //        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        //        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        //    }
        //    else
        //    {
        //        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        //        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        //    }


        //    SSQL = SSQL + " Group By MachineID";
        //    SSQL = SSQL + " Order By Max(TimeOUT)";

        //    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        //    string InMachine_IP = "";
        //    DataTable mLocalDS_out = new DataTable();
        //    long Random_No_Fixed = 1;

        //    for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
        //    {
        //        InMachine_IP = UTF8Encryption(AutoDTable.Rows[iRow2][9].ToString());

        //        SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

        //        //'Day Atten. Time Order by Change (Eveready MILL)
        //        if (AutoDTable.Rows[iRow2][3] == "SHIFT1")
        //        {
        //            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "08:00" + "'";
        //            SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:45" + "' Order by TimeOUT Asc";

        //        }

        //        else
        //        {
        //            SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
        //            SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
        //        }

        //        mLocalDS_out = objdata.RptEmployeeMultipleDetails(SSQL);


        //        if (mLocalDS_out.Rows.Count <= 0)
        //        {

        //        }
        //        else
        //        {
        //            if (AutoDTable.Rows[iRow2][9].ToString() == UTF8Decryption(mLocalDS_out.Rows[0][0].ToString()))
        //            {
        //                AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
        //            }

        //        }


        //        Time_IN_Str = "";
        //        Time_Out_Str = "";

        //        Date_Value_Str = string.Format(Date, "yyyy/MM/dd");
        //        if (SessionLcode == "UNIT I" && mLocalDS.Rows[iTabRow]["shiftDesc"] == "SHIFT10")
        //        {
        //            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
        //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' And TimeIN <='" + date1.AddDays(-1).ToString("yyyy/MM/dd") + " " + "00:59' Order by TimeIN ASC";

        //        }
        //        else
        //        {
        //            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
        //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

        //        }

        //        mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

        //        DateTime InTime_Check = new DateTime();
        //        DateTime InToTime_Check = new DateTime();
        //        TimeSpan InTime_TimeSpan;
        //        string From_Time_Str = "";
        //        string To_Time_Str = "";
        //        DataTable DS_Time = new DataTable();
        //        DataTable DS_InTime = new DataTable();
        //        string Final_InTime = "";
        //        string Final_OutTime = "";
        //        string Final_Shift = "";
        //        DataTable Shift_DS = new DataTable();
        //        int K = 0;
        //        Boolean Shift_Check_blb = false;



        //        if (AutoDTable.Rows[iRow2][3] == "SHIFT1" || AutoDTable.Rows[iRow2][3] == "SHIFT2")
        //        {

        //            InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
        //            InToTime_Check = InTime_Check.AddHours(2);

        //            string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
        //            string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
        //            InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
        //            From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
        //            InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
        //            To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;



        //            //'Two Hours OutTime Check
        //            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
        //            DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

        //            if (DS_Time.Rows.Count != 0)
        //            {
        //                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
        //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
        //                DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
        //                if (DS_InTime.Rows.Count != 0)
        //                {
        //                    Final_InTime = DS_InTime.Rows[0][0].ToString();


        //                    //'Check With IN Time Shift
        //                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
        //                    Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        //                    Shift_Check_blb = false;
        //                    for (int k = 0; k < Shift_DS.Rows.Count; k++)
        //                    {
        //                        string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
        //                        int b = Convert.ToInt16(a.ToString());
        //                        Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
        //                        string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
        //                        int b1 = Convert.ToInt16(a1.ToString());
        //                        Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

        //                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
        //                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

        //                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
        //                        if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
        //                        {
        //                            Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
        //                            Shift_Check_blb = true;
        //                        }
        //                    }
        //                    if (Shift_Check_blb == true)
        //                    {
        //                        //'IN Time And Shift Update
        //                        AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();


        //                        AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Final_InTime);

        //                        // 'IN Time Query Update
        //                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
        //                        SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
        //                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
        //                        mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

        //                        // 'Out Time Query Update
        //                        if (Final_Shift == "SHIFT2")
        //                        {
        //                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                            SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
        //                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";

        //                        }
        //                        else
        //                        {
        //                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                            SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
        //                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:40' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

        //                        }

        //                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

        //                        if (DS_Time.Rows.Count != 0)
        //                        {

        //                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


        //                        }

        //                    }
        //                    else
        //                    {
        //                        if (AutoDTable.Rows[iRow2][3] == "SHIFT1")
        //                        {
        //                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";

        //                        }
        //                        else
        //                        {
        //                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";

        //                        }
        //                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
        //                        // 'Out Time Update
        //                        if (DS_Time.Rows.Count != 0)
        //                        {

        //                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

        //                        }

        //                    }
        //                }
        //                else
        //                {
        //                    if (AutoDTable.Rows[iRow2][3] == "SHIFT1")
        //                    {
        //                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "14:00' Order by TimeOUT Asc";

        //                    }
        //                    else
        //                    {
        //                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";

        //                    }


        //                    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
        //                    //'Out Time Update
        //                    if (DS_Time.Rows.Count != 0)
        //                    {
        //                        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);
        //                    }

        //                }

        //            }
        //            else
        //            {
        //                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

        //            }

        //        }


        //        else
        //        {
        //            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
        //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

        //        }
        //        mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);

        //        string Emp_Total_Work_Time_1 = "00:00";
        //        if (mLocalDS_INTAB.Rows.Count > 1)
        //        {
        //            for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
        //            {
        //                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
        //                if (mLocalDS_OUTTAB.Rows.Count > tin)
        //                {
        //                    Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
        //                }
        //                else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
        //                {
        //                    Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
        //                }

        //                else
        //                {
        //                    Time_Out_Str = "";
        //                }
        //                TimeSpan ts4;
        //                ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;

        //                if (mLocalDS.Rows.Count <= 0)
        //                {
        //                    Time_IN_Str = "";
        //                }
        //                else
        //                {
        //                    Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
        //                }
        //                if (Time_IN_Str == "" || Time_Out_Str == "")
        //                {
        //                    time_Check_dbl = time_Check_dbl;
        //                }
        //                else
        //                {
        //                    DateTime date3 = Convert.ToDateTime(Time_IN_Str);
        //                    DateTime date4 = Convert.ToDateTime(Time_Out_Str);
        //                    TimeSpan ts1;
        //                    ts1 = date4.Subtract(date3);
        //                    Total_Time_get = ts1.Hours.ToString();
        //                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

        //                    string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

        //                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                    {
        //                        Emp_Total_Work_Time_1 = "00:00";
        //                    }
        //                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
        //                    {
        //                        Emp_Total_Work_Time_1 = "00:00";
        //                    }
        //                    if (Left_Val(Total_Time_get, 1) == "-")
        //                    {
        //                        date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
        //                        ts1 = date4.Subtract(date3);
        //                        ts1 = date4.Subtract(date3);
        //                        ts4 = ts4.Add(ts1);
        //                        Total_Time_get = ts1.Hours.ToString();
        //                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
        //                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
        //                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


        //                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                        {

        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }
        //                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
        //                        {

        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }

        //                    }

        //                    else
        //                    {


        //                        ts4 = ts4.Add(ts1);
        //                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
        //                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
        //                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                        {
        //                            Emp_Total_Work_Time_1 = "00:00";
        //                        }
        //                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
        //                        {
        //                            Emp_Total_Work_Time_1 = "00:00";
        //                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
        //                        }
        //                    }
        //                }




        //                AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;

        //            }

        //        }
        //        else
        //        {
        //            TimeSpan ts4;
        //            ts4 = Convert.ToDateTime(String.Format("{0:h:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
        //            if (mLocalDS_INTAB.Rows.Count <= 0)
        //            {
        //                Time_IN_Str = "";
        //            }
        //            else
        //            {
        //                Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
        //            }

        //            for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
        //            {
        //                if (mLocalDS_OUTTAB.Rows.Count <= 0)
        //                {
        //                    Time_Out_Str = "";
        //                }
        //                else
        //                {
        //                    Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
        //                }

        //            }
        //            //'Emp_Total_Work_Time
        //            if (Time_IN_Str == "" || Time_Out_Str == "")
        //            {
        //                time_Check_dbl = 0;
        //            }
        //            else
        //            {
        //                DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
        //                DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
        //                TimeSpan ts1;

        //                ts1 = date4.Subtract(date3);
        //                ts1 = date4.Subtract(date3);
        //                Total_Time_get = ts1.Hours.ToString(); //'& ":" & Trim(ts.Minutes)


        //                if (Left_Val(Total_Time_get, 1) == "-")
        //                {
        //                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
        //                    ts1 = date4.Subtract(date3);
        //                    ts1 = date4.Subtract(date3);
        //                    ts4 = ts4.Add(ts1);
        //                    Total_Time_get = ts1.Hours.ToString();
        //                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
        //                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
        //                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                    {

        //                        Emp_Total_Work_Time_1 = "00:00";
        //                    }
        //                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
        //                    {

        //                        Emp_Total_Work_Time_1 = "00:00";
        //                    }
        //                }
        //                else
        //                {
        //                    ts4 = ts4.Add(ts1);
        //                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
        //                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
        //                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
        //                    {
        //                        Emp_Total_Work_Time_1 = "00:00";
        //                    }
        //                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
        //                    {
        //                        Emp_Total_Work_Time_1 = "00:00";
        //                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
        //                    }

        //                }

        //                AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;







        //            }


        //        }
        //    }
        //}
        //DataTable mEmployeeDS = new DataTable();

        //SSQL = "";
        //SSQL = "select Distinct isnull(EM.MachineID,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
        //SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
        //SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
        //SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
        //SSQL = SSQL + ",DM.DeptCode from " + TableName + " EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
        //SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "'  And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
        //if (Division != "-Select-")
        //{
        //    SSQL = SSQL + " And EM.Division = '" + Division + "'";
        //}

        //SSQL = SSQL + " order by DM.DeptCode Asc";

        //mEmployeeDS = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (mEmployeeDS.Rows.Count > 0)
        //{
        //    for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
        //    {
        //        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
        //        {

        //            if (AutoDTable.Rows[iRow2][9].ToString() == mEmployeeDS.Rows[iRow1]["MachineID"].ToString())
        //            {
        //                AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
        //                AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
        //                AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
        //                AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
        //                AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
        //                AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
        //                AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];

        //            }
        //        }
        //    }
        //}


        //DataTable DtdIFReport = new DataTable();

        //SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);


        //string name = dt.Rows[0]["CompName"].ToString();

        //DataTable table_DT = new DataTable();
        //table_DT.Columns.Add("CompanyName");
        //table_DT.Columns.Add("LocationName");
        //table_DT.Columns.Add("ShiftDate");

        //table_DT.Columns.Add("SNo");
        //table_DT.Columns.Add("Dept");
        //table_DT.Columns.Add("Type");
        //table_DT.Columns.Add("Shift");
        //table_DT.Columns.Add("Category");
        //table_DT.Columns.Add("SubCategory");
        //table_DT.Columns.Add("EmpCode");
        //table_DT.Columns.Add("ExCode");
        //table_DT.Columns.Add("Name");
        //table_DT.Columns.Add("TimeIN");
        //table_DT.Columns.Add("TimeOUT");
        //table_DT.Columns.Add("MachineID");
        //table_DT.Columns.Add("PrepBy");
        //table_DT.Columns.Add("PrepDate");
        //table_DT.Columns.Add("TotalMIN");
        //table_DT.Columns.Add("GrandTOT");

        //int sno = 1;
        //for (int iRow1 = 0; iRow1 < AutoDTable.Rows.Count; iRow1++)
        //{
        //    dtRow = table_DT.NewRow();
        //    dtRow["CompanyName"] = name;
        //    dtRow["LocationName"] = SessionLcode;
        //    dtRow["ShiftDate"] = Date;
        //    dtRow["SNo"] = sno;
        //    dtRow["Dept"] = AutoDTable.Rows[iRow1][1].ToString();
        //    dtRow["Type"] = AutoDTable.Rows[iRow1][2].ToString();
        //    dtRow["Shift"] = AutoDTable.Rows[iRow1][3].ToString();
        //    dtRow["Category"] = AutoDTable.Rows[iRow1][10].ToString();
        //    dtRow["SubCategory"] = AutoDTable.Rows[iRow1][11].ToString();
        //    dtRow["EmpCode"] = AutoDTable.Rows[iRow1][4].ToString();
        //    dtRow["ExCode"] = AutoDTable.Rows[iRow1][5].ToString();
        //    dtRow["Name"] = AutoDTable.Rows[iRow1][6].ToString();

        //    if (AutoDTable.Rows[iRow1][7].ToString() != "")
        //    {
        //        ColumnName = AutoDTable.Rows[iRow1][7].ToString();
        //        if (ColumnName.ToString() == "WH")
        //        {
        //            dtRow["TimeIN"] = "WH";
        //        }
        //        else
        //        {

        //            dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());


        //        }


        //    }

        //    else
        //    {
        //        dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());
        //    }





        //    if (AutoDTable.Rows[iRow1][8].ToString() != "")
        //    {
        //        ColumnName = AutoDTable.Rows[iRow1][8].ToString();
        //        if (ColumnName.ToString() == "WH")
        //        {
        //            dtRow["TimeIN"] = "WH";
        //        }
        //        else
        //        {

        //            dtRow["TimeOUT"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][8].ToString());


        //        }


        //    }

        //    else
        //    {
        //        dtRow["TimeOUT"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][8].ToString());
        //    }




        //    dtRow["MachineID"] = AutoDTable.Rows[iRow1][9].ToString();

        //    dtRow["PrepBy"] = "User";
        //    dtRow["PrepDate"] = Date;

        //    if (AutoDTable.Rows[iRow1][12].ToString() != "")
        //    {
        //        ColumnName = AutoDTable.Rows[iRow1][12].ToString();
        //        if (ColumnName.ToString() == "WH")
        //        {
        //            dtRow["TotalMIN"] = "WH";
        //        }
        //        else
        //        {

        //            dtRow["TotalMIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][12].ToString());

        //        }
        //    }

        //    if (AutoDTable.Rows[iRow1][8].ToString() == "" || AutoDTable.Rows[iRow1][13].ToString() == "00:00")
        //    {
        //        dtRow["GrandTOT"] = "00:00";
        //    }

        //    else
        //    {
        //        if (AutoDTable.Rows[iRow1][13].ToString() != "")
        //        {

        //            dtRow["GrandTOT"] = AutoDTable.Rows[iRow1][13].ToString();
        //        }
        //        else
        //        {

        //            dtRow["GrandTOT"] = AutoDTable.Rows[iRow1][13].ToString();

        //        }
        //    }
        //    table_DT.Rows.Add(dtRow);

        //    sno = sno + 1;

        //}
        //int ssno = 1;
        //for (int iRow = 0; iRow < table_DT.Rows.Count; iRow++)
        //{

        //    DataCell.NewRow();
        //    DataCell.Rows.Add();


        //    DataCell.Rows[iRow]["CompanyName"] = table_DT.Rows[iRow]["CompanyName"].ToString();
        //    DataCell.Rows[iRow]["LocationName"] = table_DT.Rows[iRow]["LocationName"].ToString();
        //    DataCell.Rows[iRow]["ShiftDate"] = table_DT.Rows[iRow]["ShiftDate"].ToString();
        //    DataCell.Rows[iRow]["SNo"] = ssno;
        //    DataCell.Rows[iRow]["Dept"] = table_DT.Rows[iRow]["Dept"].ToString();
        //    DataCell.Rows[iRow]["Type"] = table_DT.Rows[iRow]["Type"].ToString();
        //    DataCell.Rows[iRow]["Shift"] = table_DT.Rows[iRow]["Shift"].ToString();
        //    DataCell.Rows[iRow]["Category"] = table_DT.Rows[iRow]["Category"].ToString();
        //    DataCell.Rows[iRow]["SubCategory"] = table_DT.Rows[iRow]["SubCategory"].ToString();
        //    DataCell.Rows[iRow]["EmpCode"] = table_DT.Rows[iRow]["EmpCode"].ToString();
        //    DataCell.Rows[iRow]["ExCode"] = table_DT.Rows[iRow]["ExCode"].ToString();
        //    DataCell.Rows[iRow]["Name"] = table_DT.Rows[iRow]["Name"].ToString();
        //    DataCell.Rows[iRow]["TimeIN"] = table_DT.Rows[iRow]["TimeIN"].ToString();
        //    DataCell.Rows[iRow]["TimeOUT"] = table_DT.Rows[iRow]["TimeOUT"].ToString();

        //    DataCell.Rows[iRow]["TotalMIN"] = table_DT.Rows[iRow]["TotalMIN"].ToString();
        //    DataCell.Rows[iRow]["GrandTOT"] = table_DT.Rows[iRow]["GrandTOT"].ToString();
        //    DataCell.Rows[iRow]["MachineID"] = table_DT.Rows[iRow]["MachineID"].ToString();
        //    DataCell.Rows[iRow]["PrepBy"] = table_DT.Rows[iRow]["PrepBy"].ToString();
        //    DataCell.Rows[iRow]["PrepDate"] = table_DT.Rows[iRow]["PrepDate"].ToString();


        //    ssno = ssno + 1;
        //}

        //ds.Tables.Add(DataCell);
        ////ReportDocument report = new ReportDocument();
        //report.Load(Server.MapPath("crystal/Attendance_TokenNoWise.rpt"));
        
        //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        //CrystalReportViewer1.ReportSource = report;

    }

     private static string UTF8Decryption(string encryptpwd)
     {
         string decryptpwd = "0";
         BALDataAccess objdata_new = new BALDataAccess();

         string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
         DataTable DT = new DataTable();
         DT = objdata_new.RptEmployeeMultipleDetails(query);
         if (DT.Rows.Count != 0)
         {
             decryptpwd = DT.Rows[0]["MachineID"].ToString();
         }
         else
         {
             query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
             DT = objdata_new.RptEmployeeMultipleDetails(query);
             if (DT.Rows.Count != 0)
             {
                 decryptpwd = DT.Rows[0]["MachineID"].ToString();
             }
             else
             {
                 decryptpwd = "0";
             }
         }

         //UTF8Encoding encodepwd = new UTF8Encoding();
         //Decoder Decode = encodepwd.GetDecoder();
         //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
         //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
         //char[] decoded_char = new char[charCount];
         //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
         //decryptpwd = new String(decoded_char);
         return decryptpwd;
     }
     public static string GetRandom(int Min, int Max)
     {
         System.Random Generator = new System.Random();

         return Generator.Next(Min, Max).ToString();


     }
     public static string UTF8Encryption_OLD(string password)
     {
         string strmsg = string.Empty;
         byte[] encode = new byte[password.Length];
         encode = Encoding.UTF8.GetBytes(password);
         strmsg = Convert.ToBase64String(encode);
         return strmsg;
     }

     public string UTF8Encryption(string mvarPlanText)
     {
         string cipherText = "";
         try
         {
             string passPhrase = "Altius";
             string saltValue = "info@altius.co.in";
             string hashAlgorithm = "SHA1";
             string initVector = "@1B2c3D4e5F6g7H8";
             int passwordIterations = 2;
             int keySize = 256;
             byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
             byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
             byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
             PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
             byte[] keyBytes = password.GetBytes(keySize / 8);
             RijndaelManaged symmetricKey = new RijndaelManaged();
             symmetricKey.Mode = CipherMode.CBC;
             ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
             MemoryStream memoryStream = new MemoryStream();
             CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
             cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
             cryptoStream.FlushFinalBlock();
             byte[] cipherTextBytes = memoryStream.ToArray();
             memoryStream.Close();
             cryptoStream.Close();
             cipherText = Convert.ToBase64String(cipherTextBytes);
         }
         catch (Exception ex)
         {
             throw new Exception(ex.Message);
         }

         return cipherText;
     }

     protected void Page_Unload(object sender, EventArgs e)
     {
         CrystalReportViewer1.Dispose();
     }
}
