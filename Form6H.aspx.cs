﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Collections;

public partial class Form6H : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    BALDataAccess objdata = new BALDataAccess();
    string cc = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
   
    string SSQL = "";
    DataTable dsEmployee = new DataTable();
    DataTable mDataSet = new DataTable();
    string[] Time_Minus_Value_Check;
    int shiftCount;
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string SessionCcode;
    string SessionLcode;
    string fromdate;
    string todate;
    string cmbWages;
    DataTable mLocalDS = new DataTable();
    DateTime dtime;
    DateTime dtime1;
    double OT_Time = 0;
    DateTime date2 = new DateTime();
    string Encry_MachineID;

    bool isPresent = false;
    string Time_IN_Str;
    string Time_Out_Str;
    int j = 0;

    string Date_Value_Str1;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;




    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = null;
    string Date_Value_Str = "";
    string Total_Time_get = "";
    string Final_OT_Work_Time_1 = "00:00";
    // Decimal Month_Mid_Join_Total_Days_Count;
    DateTime NewDate1;


    string To_Time_Str = "";
    DataTable DS_Time = new DataTable();
    DataTable DS_InTime = new DataTable();
    string Final_InTime = "";
    string Final_OutTime = "";
    string Final_Shift = "";
    DataTable Shift_DS_Change = new DataTable();
    Int32 K = 0;
    bool Shift_Check_blb = false;
    string Shift_Start_Time_Change = null;
    string Shift_End_Time_Change = null;
    string Employee_Time_Change = "";
    DateTime ShiftdateStartIN_Change = default(DateTime);
    DateTime ShiftdateEndIN_Change = default(DateTime);
    DateTime EmpdateIN_Change = default(DateTime);

    string Fin_Year;
    string Months_Full_Str;
    //string Months_Full_Str;
    string Date_Col_Str;
    string Month_Int;
    string Spin_Wages;
    string Wages;
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;

    //string Att_Year_Check = "";
    string Token_No_Get;
    TimeSpan ts4 = new TimeSpan();

    Decimal Total_Days_Count;
    double Final_Count;

    double Present_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    double FullNight_Shift_Count;
    double NFH_Week_Off_Minus_Days;

    Boolean NFH_Day_Check = false;
    Boolean Check_Week_Off_DOJ = false;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    int Appsent_Count;
    string SessionAdmin;
    string SessionUserType;
    string SessionCompanyName;
    string SessionLocationName;
    int dayCount;


    string mIpAddress_IN;
    string mIpAddress_OUT;
    string year;

    string Emp_WH_Day;


    DateTime WH_DOJ_Date_Format;
    DateTime Week_Off_Date;

    DataTable Datacells = new DataTable();
    DataTable mEmployeeDS = new DataTable();

    Double NFH_Final_Disp_Days;
    System.Web.UI.WebControls.DataGrid grid =
                                     new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report Form6H";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }
            //Wages = Request.QueryString["Wages"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            //fromdate = Request.QueryString["FromDate"].ToString();
            //todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");
            //Wages = Request.QueryString["WagesType"].ToString();
            year = Request.QueryString["Year"].ToString();

            FORM6H();
           
    }
    //FORM6H code start
    public void FORM6H()
    {
        DataTable NFH_DS = new DataTable();
        DataTable TeamCount = new DataTable();
        DataTable YarnType = new DataTable();
        DataTable ShiftDT = new DataTable();
        DataTable KG_DT = new DataTable();
        DataTable Total_DT = new DataTable();

        string Grand_sum = "0";

        //Get the NFHDate for Particular year
        SSQL = "Select * from NFH_Mst where Year='" + year + "' Order by NFHDate Asc";
        NFH_DS = objdata.RptEmployeeMultipleDetails(SSQL);



        grid.DataSource = NFH_DS;
        grid.DataBind();
        string attachment = "attachment;filename=FORM6H.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);


        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");


        Response.Write("<td colspan='" + Convert.ToInt16(NFH_DS.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> FORM VI - NFH </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a> Holidays allowed </a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> H </a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");
        Response.Write("<td colspan='" + Convert.ToInt16(NFH_DS.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> See Sub Rule ( 1) of Rule 7 ) </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a> For Work on Double Wages </a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> W/D </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");
        Response.Write("<td colspan='" + Convert.ToInt16(NFH_DS.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> REGISTER  OF NATIONAL  AND FESTIVAL  HOLIDAYS </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a> Substitute Holidays </a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> W/H </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");
        Response.Write("<td colspan='" + Convert.ToInt16(NFH_DS.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> For the year - " + year + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='2'>");
        Response.Write("<a> If not eligible of Wages </a>");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> NE </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");
        Response.Write("<td rowspan='3'>");
        Response.Write("<a style=\"font-weight:bold\"> SNO </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='3'>");
        Response.Write("<a style=\"font-weight:bold\"> Name of  the employee </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='3'>");
        Response.Write("<a style=\"font-weight:bold\"> Token No </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(NFH_DS.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> Days, Dates, Months  & Year on which National  & Festival holidays allowed </a>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");

        for (int i = 1; i <= NFH_DS.Rows.Count; i++)
        {
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + i + "</a>");
            Response.Write("</td>");
        }


        Response.Write("<td rowspan='2' colspan='3'>");
        Response.Write("<a style=\"font-weight:bold\"> Remarks </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td></td>");
        for (int i = 1; i <= NFH_DS.Rows.Count; i++)
        {
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> Name & Date of Festival </a>");
            Response.Write("</td>");
        }
        Response.Write("</tr>");

        string date_Chk = "01/01/" + year;
        DataTable Emp_DS = new DataTable();
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='" + Wages + "'";
        SSQL = SSQL + " And(IsActive='Yes' or CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(date_Chk).ToString("dd/MM/yyyy") + "',103))";
        //Check User Type
        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And Eligible_PF='1'";
        }
        SSQL = SSQL + " Order by ExistingCode Asc";

        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Emp_DS.Rows.Count != 0)
        {

            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                int colIndex;
                Boolean isPresent = false;
                Double Worked_Days_Count = 0;

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td></td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">" + Convert.ToInt16(intRow + 1) + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">" + Emp_DS.Rows[intRow]["FirstName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">" + Emp_DS.Rows[intRow]["ExistingCode"].ToString() + "</a>");
                Response.Write("</td>");


                for (int intCol = 0; intCol < NFH_DS.Rows.Count; intCol++)
                {
                    isPresent = false;
                    String Machine_ID_Str = "";
                    String OT_Week_OFF_Machine_No = "";
                    String Date_Value_Str = "";
                    String Date_value_str1 = "";
                    DataTable mLocalDS = new DataTable();
                    String Time_IN_Str = "";
                    String Time_Out_Str = "";
                    String Total_Time_get = "";
                    int j = 0;
                    Double time_Check_dbl = 0;

                    String Emp_Total_Work_Time_1 = "00:00";

                    Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    Date_Value_Str = (Convert.ToDateTime(NFH_DS.Rows[intCol]["DateStr"])).ToString("yyyy/MM/dd");
                    Date_value_str1 = (Convert.ToDateTime(Date_Value_Str).AddDays(1)).ToString("yyyy/MM/dd");

                    Boolean NFH_Form25_Prescent_CHeck_Bol = false;

                    if (NFH_DS.Rows[intCol]["Form25_NFH_Present"].ToString().ToUpper() == "Yes".ToUpper())
                    {
                        NFH_Form25_Prescent_CHeck_Bol = true;
                    }
                    else
                    {
                        NFH_Form25_Prescent_CHeck_Bol = false;
                    }


                    //NFH Present Check Start

                    if (NFH_Form25_Prescent_CHeck_Bol == true)
                    {

                        //Get Employee Week OF DAY
                        DataTable DS_WH = new DataTable();
                        string Emp_WH_Day = "";
                        string DOJ_Date_Str = "";
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DS_WH.Rows.Count != 0)
                        {
                            Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                            if (DS_WH.Rows[0]["DOJ"].ToString() == "")
                            {
                                DOJ_Date_Str = "";
                            }
                            else
                            {
                                DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                            }
                        }
                        else
                        {
                            Emp_WH_Day = "";
                            DOJ_Date_Str = "";
                        }


                        //Date OF Joining Check Start
                        DateTime NFH_Date;
                        bool DOJ_NFH_Day_Check = false;
                        DateTime DOJ_Date_Format;
                        if (DOJ_Date_Str != "")
                        {
                            NFH_Date = Convert.ToDateTime(Date_Value_Str);
                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                DOJ_NFH_Day_Check = false;
                            }
                            else
                            {
                                //Skip
                                DOJ_NFH_Day_Check = true;
                            }
                        }
                        else
                        {
                            DOJ_NFH_Day_Check = false;
                        }
                        //Date OF Joining Check End


                        DataTable Log_DT = new DataTable();
                        SSQL = "Select *from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).ToString("dd/MM/yyyy") + "'";
                        Log_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (Log_DT.Rows.Count != 0)
                        {
                            if (Log_DT.Rows[0]["present"].ToString() != "0.0")
                            {
                                isPresent = true;
                            }
                        }


                        if (DOJ_NFH_Day_Check == true)
                        {
                            Response.Write("<td>");
                            Response.Write("<a style=\"font-weight:bold\">--</a>");
                            Response.Write("</td>");
                        }
                        else
                        {
                            if (NFH_Form25_Prescent_CHeck_Bol == true)
                            {
                                if (isPresent == true)
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a style=\"font-weight:bold\">W/D</a>");
                                    Response.Write("</td>");

                                }
                                else
                                {
                                    //'Check In Week OF Days
                                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                                    if ((Emp_WH_Day).ToUpper() == (Attn_Date_Day).ToUpper())
                                    {
                                        Response.Write("<td>");
                                        Response.Write("<a style=\"font-weight:bold\">W/H</a>");
                                        Response.Write("</td>");

                                    }
                                    else
                                    {
                                        Response.Write("<td>");
                                        Response.Write("<a style=\"font-weight:bold\">H</a>");
                                        Response.Write("</td>");
                                    }
                                }
                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("<a style=\"font-weight:bold\">H</a>");
                                Response.Write("</td>");
                            }

                        }


                    }
                    else
                    {
                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">H</a>");
                        Response.Write("</td>");
                    }
                    //NFH Present Check End
                }
                Response.Write("<td colspan='3'>");
                Response.Write("</td>");
                Response.Write("</tr>");

            }

        }


        Response.Write("</table>");


        Response.End();
        Response.Clear();

    }
    //FORM6H code end


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }
   


}
