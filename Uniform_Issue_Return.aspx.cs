﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Uniform_Issue_Return : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL; string SessionTransNo;
    string SessionAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Uniform Issue Return Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Initial_Data_Referesh();
            Load_Data_ItemName();
            Load_Data_Size();
            Load_Trans_ID();
            Load_Wages_Type();
            Load_Token_No();
            if (Session["TransID"] == null)
            {

            }
            else
            {
                SessionTransNo = Session["TransID"].ToString();
                txtTransID.Text = SessionTransNo;
                btnSearch_Click(sender, e);
            }

        }

        Load_OLD_data();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search DirectPurchase
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Uniform_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["TransDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            //JobWork_Main_Sub Table Load

            DataTable dt = new DataTable();
            query = "Select WagesType,TokenNo,EmpName,ItemID,ItemName,SizeName,Qty,ItemRate,LineTotal,(TokenNo + '|' + SizeName) as SizeName_Token from Uniform_Issue_Return_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();


            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Wages_Type()
    {
        string query = "";
        DataTable DT = new DataTable();
        txtWages_Type.Items.Clear();
        query = "Select Distinct Wages from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes' Order by Wages Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtWages_Type.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["Wages"] = "-Select-";
        dr["Wages"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtWages_Type.DataTextField = "Wages";
        txtWages_Type.DataValueField = "Wages";
        txtWages_Type.DataBind();
    }
    private void Load_Token_No()
    {
        string query = "";
        DataTable DT = new DataTable();
        txtToken_No.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes' And Wages='" + txtWages_Type.SelectedValue + "' Order by ExistingCode Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtToken_No.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtToken_No.DataTextField = "ExistingCode";
        txtToken_No.DataValueField = "ExistingCode";
        txtToken_No.DataBind();
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        Total_Add();
    }

    private void Load_Data_ItemName()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtItemName.Items.Clear();
        query = "Select (sum(Add_Qty)-Sum(Minus_Qty)),ItemName from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " group by ItemName having (sum(Add_Qty)-Sum(Minus_Qty)) > 0 order by ItemName Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtItemName.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemName"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemName.DataTextField = "ItemName";
        txtItemName.DataValueField = "ItemName";
        txtItemName.DataBind();
    }

    private void Load_Data_Size()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtSize.Items.Clear();
        query = "Select (sum(Add_Qty)-Sum(Minus_Qty)),SizeName from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And ItemName='" + txtItemName.SelectedItem.Text + "' group by SizeName having (sum(Add_Qty)-Sum(Minus_Qty)) > 0 order by SizeName Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtSize.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SizeName"] = "-Select-";
        dr["SizeName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtSize.DataTextField = "SizeName";
        txtSize.DataValueField = "SizeName";
        txtSize.DataBind();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }

    private void Clear_All_Field()
    {
        txtTransID.Text = "";
        txtItemID.Value = "";
        txtDate.Text = ""; txtRemarks.Text = "";
        txtWages_Type.SelectedValue = "-Select-";
        Load_Token_No();
        txtToken_No.SelectedValue = "-Select-";
        txtEmpName.Text = "";
        txtItemName.SelectedValue = "-Select-";
        Load_Data_Size();
        txtSize.SelectedValue = "-Select-";
        txtQty.Text = "1";
        txtRate.Text = "0";
        Initial_Data_Referesh();
        Session.Remove("TransID");
        Load_Trans_ID();
        txtQtyTotal.Text = "0";
        txtTotalAmount.Text = "0";
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt1 = new DataTable();
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;


        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Employee Details..');", true);
        }
        if (!ErrFlag)
        {
            SSQL = "Select * from Uniform_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt1.Rows.Count != 0)
            {
                SSQL = "Delete from Uniform_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Uniform_Issue_Return_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            SSQL = "Insert into Uniform_Issue_Return_Main(Ccode,Lcode,TransID,TransDate,Remarks,Qty_Total,Amt_Total)values(";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtTransID.Text + "','" + txtDate.Text + "',";
            SSQL = SSQL + "'" + txtRemarks.Text.ToString() + "','" + txtQtyTotal.Text + "','" + txtTotalAmount.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert into Uniform_Issue_Return_Main_Sub(Ccode,Lcode,TransID,TransDate,WagesType,TokenNo,EmpName,ItemID,ItemName,SizeName,Qty,ItemRate,LineTotal)values(";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtTransID.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["WagesType"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["TokenNo"].ToString() + "','" + dt.Rows[i]["EmpName"].ToString() + "','" + dt.Rows[i]["ItemID"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["SizeName"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["ItemRate"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (SessionAdmin == "1")
            {
                Stock_Add(txtTransID.Text.ToString());
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Uniform Issue Return Details Saved Successfully..!');", true);
            Clear_All_Field();
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";



        if (!ErrFlag)
        {

            //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemID"].ToString().ToUpper() == txtItemName.SelectedValue.ToString().ToUpper() && dt.Rows[i]["SizeName"].ToString().ToUpper() == txtSize.SelectedValue.ToString().ToUpper() && dt.Rows[i]["TokenNo"].ToString().ToUpper() == txtToken_No.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Details Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["WagesType"] = txtWages_Type.SelectedValue;
                    dr["TokenNo"] = txtToken_No.SelectedValue;
                    dr["EmpName"] = txtEmpName.Text;
                    dr["ItemID"] = txtItemID.Value;
                    dr["ItemName"] = txtItemName.SelectedItem.Text.ToString();
                    dr["SizeName"] = txtSize.Text;
                    dr["Qty"] = txtQty.Text;
                    dr["ItemRate"] = txtRate.Text;
                    string Size_Token = txtToken_No.SelectedValue.ToString() + "|" + txtSize.SelectedValue.ToString();
                    dr["SizeName_Token"] = Size_Token;
                    string LineTotal = (Convert.ToDecimal(txtQty.Text.ToString()) * Convert.ToDecimal(txtRate.Text.ToString())).ToString();
                    LineTotal = (Math.Round(Convert.ToDecimal(LineTotal), 2, MidpointRounding.AwayFromZero)).ToString();
                    dr["LineTotal"] = LineTotal;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    //txtWages_Type.SelectedValue = "-Select-";
                    txtToken_No.SelectedValue = "-Select-";
                    txtEmpName.Text = "";
                    txtItemName.SelectedValue = "-Select-"; txtSize.SelectedValue = "-Select-";
                    txtQty.Text = "1"; txtRate.Text = "0";
                    Total_Add();

                }
            }
            else
            {
                dr = dt.NewRow();
                dr["WagesType"] = txtWages_Type.SelectedValue;
                dr["TokenNo"] = txtToken_No.SelectedValue;
                dr["EmpName"] = txtEmpName.Text;
                dr["ItemID"] = txtItemID.Value;
                dr["ItemName"] = txtItemName.SelectedItem.Text.ToString();
                dr["SizeName"] = txtSize.Text;
                dr["Qty"] = txtQty.Text;
                dr["ItemRate"] = txtRate.Text;
                string Size_Token = txtToken_No.SelectedValue.ToString() + "|" + txtSize.SelectedValue.ToString();
                dr["SizeName_Token"] = Size_Token;

                string LineTotal = (Convert.ToDecimal(txtQty.Text.ToString()) * Convert.ToDecimal(txtRate.Text.ToString())).ToString();
                LineTotal = (Math.Round(Convert.ToDecimal(LineTotal), 2, MidpointRounding.AwayFromZero)).ToString();
                dr["LineTotal"] = LineTotal;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //txtWages_Type.SelectedValue = "-Select-"; 
                txtToken_No.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtItemName.SelectedValue = "-Select-"; txtSize.SelectedValue = "-Select-";
                txtQty.Text = "1"; txtRate.Text = "0";
                Total_Add();

            }
        }

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("WagesType", typeof(string)));
        dt.Columns.Add(new DataColumn("TokenNo", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemID", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("SizeName", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRate", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("SizeName_Token", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";
        string[] Token_Size = e.CommandArgument.ToString().Split('|');
        string Token_No_Str = Token_Size[0].ToString();
        string Size_Name_Str = Token_Size[1].ToString();

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemID"].ToString() == e.CommandName.ToString() && dt.Rows[i]["SizeName"].ToString() == Size_Name_Str && dt.Rows[i]["TokenNo"].ToString() == Token_No_Str)
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_Trans_ID()
    {
        string query = "";
        DataTable DT_T = new DataTable();
        query = "Select * from Uniform_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by TransID Desc";
        DT_T = objdata.RptEmployeeMultipleDetails(query);
        if (DT_T.Rows.Count != 0)
        {
            string Last_Trans_ID = DT_T.Rows[0]["TransID"].ToString();
            string Final_Trans_ID = (Convert.ToDecimal(Last_Trans_ID) + Convert.ToDecimal(1)).ToString();
            txtTransID.Text = Final_Trans_ID.ToString();
        }
        else
        {
            txtTransID.Text = "1";
        }
    }

    protected void txtToken_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "Select FirstName from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " And Wages='" + txtWages_Type.SelectedValue + "' And ExistingCode='" + txtToken_No.SelectedValue + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
        }
        else
        {
            txtEmpName.Text = "";
        }
    }
    protected void txtSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Rate
        string query = "Select cast( isnull((Sum(Add_Value)-Sum(Minus_Value)) / (Sum(Add_Qty)-Sum(Minus_Qty)),0) as decimal(18,2)) as ItemRate";
        query = query + " from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + txtItemName.SelectedItem.Text + "'";
        query = query + " And SizeName='" + txtSize.SelectedValue + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtRate.Text = DT.Rows[0]["ItemRate"].ToString();
        }
        else
        {
            txtRate.Text = "0";
        }

    }
    protected void txtWages_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Token_No();
    }
    protected void txtItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Item ID
        string query = "Select ItemID from Uniform_item_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ItemName='" + txtItemName.SelectedItem.Text + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtItemID.Value = DT.Rows[0]["ItemID"].ToString();
        }
        else
        {
            txtItemID.Value = "";
        }

        //Load_Size
        Load_Data_Size();
    }

    private void Stock_Add(string Transaction_No)
    {
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT_CH = new DataTable();
        query = "Select * from Uniform_Issue_Return_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            query = "Select * from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "' And TransType='Issue Return'";
            DT_CH = objdata.RptEmployeeMultipleDetails(query);
            if (DT_CH.Rows.Count != 0)
            {
                query = "Delete from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "' And TransType='Issue Return'";
                DT_CH = objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Stock Ledger
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string ItemDesc_Full = DT.Rows[i]["ItemName"].ToString() + " Size:" + DT.Rows[i]["SizeName"].ToString();
                query = "Insert Into Uniform_Stock_Ledger_All(Ccode,Lcode,TransID,TransDate,TransDate_Str,TransType,ItemDesc,ItemName,SizeName,Add_Qty,Add_Rate,";
                query = query + " Add_Value,Minus_Qty,Minus_Rate,Minus_Value,Party_Name,Token_No,Wages_Type) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + Transaction_No + "',Convert(Datetime,'" + DT.Rows[i]["TransDate"].ToString() + "',103),";
                query = query + " '" + DT.Rows[i]["TransDate"].ToString() + "','Issue Return','" + ItemDesc_Full + "','" + DT.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + DT.Rows[i]["SizeName"].ToString() + "','" + DT.Rows[i]["Qty"].ToString() + "','" + DT.Rows[i]["ItemRate"].ToString() + "',";
                query = query + " '" + DT.Rows[i]["LineTotal"].ToString() + "','0.00','0.00','0.00','" + DT.Rows[i]["EmpName"].ToString() + "','" + DT.Rows[i]["TokenNo"].ToString() + "','" + DT.Rows[i]["WagesType"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
        }
    }

    private void Total_Add()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        string Qty_Tot = "0";
        string Amt_Tot = "0";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            Qty_Tot = (Convert.ToDecimal(dt.Rows[i]["Qty"].ToString()) + Convert.ToDecimal(Qty_Tot)).ToString();
            Amt_Tot = (Convert.ToDecimal(dt.Rows[i]["LineTotal"].ToString()) + Convert.ToDecimal(Amt_Tot)).ToString();
        }
        txtQtyTotal.Text = Qty_Tot;
        txtTotalAmount.Text = Amt_Tot;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("Uniform_Issue_Return_Main.aspx");
    }
}


