﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Uniform_Current_Stock_Report : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL; string SessionTransNo;
    string SessionAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Uniform Current Stock Report Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Data_Size();
            Load_Data_ItemName();


        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string ItemName_Str = "";
            string SizeName_Str = "";
            string Fromdate_Str = "";
            string ToDate_Str = "";


            if (txtItemName.SelectedItem.Text != "-Select-")
            {
                ItemName_Str = txtItemName.SelectedItem.Text.ToString();
            }
            if (txtSize.SelectedItem.Text != "-Select-")
            {
                SizeName_Str = txtSize.SelectedItem.Text.ToString();
            }
            if (txtFromDate.Text != "")
            {
                Fromdate_Str = txtFromDate.Text.ToString();
            }
            if (txtToDate.Text != "")
            {
                ToDate_Str = txtToDate.Text.ToString();
            }


            if (!ErrFlag)
            {
                ResponseHelper.Redirect("Uniform_Report_Display.aspx?ItemName=" + ItemName_Str + "&SizeName=" + SizeName_Str + "&FromDate=" + Fromdate_Str + "&ToDate=" + ToDate_Str + "&Report_Type=Uniform_Current_Stock_Report_Det", "_blank", "");
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void Load_Data_ItemName()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtItemName.Items.Clear();
        query = "Select cast(ItemID as varchar(20)) as ItemID,ItemName from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtItemName.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemName"] = "-Select-";
        dr["ItemID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemName.DataTextField = "ItemName";
        txtItemName.DataValueField = "ItemID";
        txtItemName.DataBind();
    }

    private void Load_Data_Size()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtSize.Items.Clear();
        query = "Select * from Uniform_Size_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtSize.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SizeName"] = "-Select-";
        dr["SizeName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtSize.DataTextField = "SizeName";
        txtSize.DataValueField = "SizeName";
        txtSize.DataBind();
    }
}


