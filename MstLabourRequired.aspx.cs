﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Default2 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Labour Required Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Unit();
        }

        Load_Data_LabourRequired();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        query = "select * from LabourRequired_Mst where Unit='" + ddlUnit.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            SaveMode = "Update";
            query = "delete from LabourRequired_Mst where Unit='" + ddlUnit.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        query = "Insert into LabourRequired_Mst(Unit,Required,Date)";
        query = query + "values('" + ddlUnit.SelectedValue + "','" + txtReq.Text + "','" + Convert.ToDateTime(txtDate.Text).ToString("yyyy/MM/dd") + "')";
        objdata.RptEmployeeMultipleDetails(query);

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Required Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Required Saved Successfully...!');", true);
        }

        Load_Data_LabourRequired();
        Clear_All_Field();
    }
    private void Load_Data_LabourRequired()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select unit,Required,CONVERT(varchar(10),Date,103) AS Date from LabourRequired_Mst";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LocCode"] = "-Select-";
        dr["LocCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "LocCode";
        ddlUnit.DataValueField = "LocCode";
        ddlUnit.DataBind();

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from LabourRequired_Mst where Required='" + e.CommandName.ToString() + "' And Unit='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {

            txtReq.Text = DT.Rows[0]["Required"].ToString();
            txtDate.Text = Convert.ToDateTime(DT.Rows[0]["Date"]).ToString("dd/MM/yyyy");
              ddlUnit.SelectedValue = DT.Rows[0]["Unit"].ToString();
            ddlUnit.Enabled = false;
            btnSave.Text = "Update";
            
        }
        else
        {
            txtReq.Text = "";
            txtDate.Text = "";
            ddlUnit.SelectedValue = "-Select-";
            btnSave.Text = "Save";
        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from LabourRequired_Mst where Required='" + e.CommandName.ToString() + "' And Unit='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from LabourRequired_Mst where Required='" + e.CommandName.ToString() + "' And Unit='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Required Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_LabourRequired();
    }
    private void Clear_All_Field()
    {
        txtDate.Text = "";
        txtReq.Text = "";
        ddlUnit.SelectedValue = "-Select-";
        ddlUnit.Enabled = true;
        btnSave.Text = "Save";
    }
}