﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
using System.IO;

public partial class RptPFESIView : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string SSQL = "";
    string query;
    string fromdate;
    string ToDate;
    string ReportType;
    string Emp_ESI_Code;
    string SessionPayroll;

    string EmployeeType;
    string PayslipType;

    string PFTypeGet;
    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Division_Name = "";

    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";

    string CateName = "";
    string EmpType = "";
    

    protected void Page_Load(object sender, EventArgs e)
    {
        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
      
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();
        //Load_DB();

        
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        ReportType = Request.QueryString["ReportType"].ToString();
        EmpType= Request.QueryString["Emptype"].ToString();

        Left_Employee = Request.QueryString["Left_Emp"].ToString();
        Left_Date = Request.QueryString["Leftdate"].ToString();

        fromdate = Request.QueryString["FromDate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();

        CateName = Request.QueryString["CateName"].ToString();


        int YR = 0;
        string report_head = "";
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }

        //Get Report_Date
        query = "Select * from [" + SessionPayroll + "]..MstBasic_Report_Date where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DataTable DT_RPT = new DataTable();
        string Query_Date_Check = "";
        DT_RPT = objdata.RptEmployeeMultipleDetails(query);
        if (DT_RPT.Rows.Count != 0)
        {
            Basic_Report_Date = DT_RPT.Rows[0]["Report_Date"].ToString();
        }
        else
        {
            Basic_Report_Date = "";
        }
        if (Basic_Report_Date != "")
        {
            if (str_month != "")
            {
                Query_Date_Check = "01/" + DateTime.ParseExact(str_month, "MMMM", CultureInfo.InvariantCulture).Month;
                Query_Date_Check = Query_Date_Check + "/" + YR.ToString();
                DateTime InputDate_Chk = Convert.ToDateTime(Query_Date_Check);
                DateTime DB_Basic_Report_Date = Convert.ToDateTime(Basic_Report_Date);
                if (InputDate_Chk > DB_Basic_Report_Date)
                {
                    Basic_Report_Type = "NEW";
                }
                else
                {
                    Basic_Report_Type = "OLD";
                }
            }
            else
            {
                Basic_Report_Type = "OLD";
            }
        }
        else
        {
            Basic_Report_Type = "OLD";
        }

        DataTable dt = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }

        Get_Division_Name = Request.QueryString["Division"].ToString();

        //query = "Select '' as UAN,SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
        //    " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
        //    " SalDet.WH_Work_Days,'' as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
        //    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
        //    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
        //    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
        //    " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
        //    " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
        //    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
        //    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
        //    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," +
        //    " SalDet.EmployeerPFone,SalDet.EmployeerPFTwo,SalDet.EmployeerESI,ET.EmpTypeCd as EmployeeType," +
        //    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
        //    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives" +
        //    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
        //    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
        //    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo And SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
        //    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
        //    " inner join MstEmployeeType ET on ET.EmpType=EmpDet.Wages" +
        //    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CompCode='" + SessionCcode + "'" +
        //    " And AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
        //    " and EmpDet.LocCode='" + SessionLcode + "'" +
        //    " and (ET.EmpTypeCd='1' or ET.EmpTypeCd='3' or ET.EmpTypeCd='4' or ET.EmpTypeCd='6' or ET.EmpTypeCd='7')" +
        //    " And SalDet.Ccode='" + SessionCcode + "' And SalDet.Lcode='" + SessionLcode + "'" +
        //    " And AttnDet.Ccode='" + SessionCcode + "' And AttnDet.Lcode='" + SessionLcode + "'";

        // query = "Select '' as UAN,SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
        //  " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
        //  " SalDet.WH_Work_Days,'' as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
        //  " SalDet.GrossEarnings as BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
        //  " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
        //  " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
        //  " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
        //  " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
        //  " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
        //  " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
        //  " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," +
        //  " SalDet.EmployeerPFone,SalDet.EmployeerPFTwo,SalDet.EmployeerESI,ET.EmpTypeCd as EmployeeType," +
        //  " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
        //  " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives" +
        //  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
        //  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
        //  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo And SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
        //  //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
        //  " inner join MstEmployeeType ET on ET.EmpType=EmpDet.Wages" +
        //  " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CompCode='" + SessionCcode + "'" +
        //  " And AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
        //  " and EmpDet.LocCode='" + SessionLcode + "'" +
        ////  " and (ET.EmpTypeCd='1' or ET.EmpTypeCd='2' or ET.EmpTypeCd='4' or ET.EmpTypeCd='6' or ET.EmpTypeCd='7')" +
        //  " And SalDet.Ccode='" + SessionCcode + "' And SalDet.Lcode='" + SessionLcode + "'" +
        //  " And AttnDet.Ccode='" + SessionCcode + "' And AttnDet.Lcode='" + SessionLcode + "'";

        // //PF Check
        // if (ReportType.ToString() == "1" || ReportType.ToString() == "2")
        // {
        //     //ESI
        //     query = query + " and (EmpDet.Eligible_PF='1') And SalDet.ESI >= 1";
        // }
        // else
        // {
        //     query = query + " and (EmpDet.Eligible_PF='1') And SalDet.ProvidentFund >= 1";
        // }
        // //Activate Employee Check
        // if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
        // //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
        // else
        // {
        //     //if (Left_Date != "") { query = query + " and EmpDet.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
        //     if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
        // }

        // //Add Division Condition
        // if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
        // {
        //     query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
        // }
        // query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
        // " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
        // " SalDet.WH_Work_Days,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
        // " SalDet.GrossEarnings,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
        // " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
        // " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
        // " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
        // " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
        // " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
        // " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,SalDet.EmployeerPFone,SalDet.EmployeerPFTwo,SalDet.EmployeerESI,ET.EmpTypeCd," +
        // " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
        // " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives";
        // if (ReportType.ToString() == "2")
        // {
        //     query = query + " Order by EmpDet.ESINo Asc";
        // }
        // else if (ReportType.ToString() == "4")
        // {
        //     query = query + " Order by EmpDet.PFNo Asc";
        // }
        // else
        // {
        //     query = query + " Order by EmpDet.ExistingCode Asc";
        // }

        //ESI
        if (ReportType.ToString() == "2")
        {
            SSQL = "";
            SSQL = "Select ('&nbsp;'+EmpDet.ESINo) as ESINO ,EmpDet.FirstName,SUM(isnull(SalDet.WorkedDays,'0')) as Days,SUM(isnull(CAST(SalDet.GrossEarnings as decimal(18,0)),'0')) as Gross,'' as Resion,";
            SSQL = SSQL + "(case when EmpDet.IsActive='Yes' then '' when Convert(Datetime,DOR,103)>=Convert(datetime,'" + fromdate.ToString() + "',103) then Convert(Varchar,DOR,103) end) as DOJ";
            SSQL = SSQL + " from Employee_mst EmpDet inner join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo";
            SSQL = SSQL + " where SalDet.Month='" + str_month + "'";
            SSQL = SSQL + " and Convert(datetime,FromDate,103)>=Convert(datetime,'" + fromdate.ToString() + "',103) and Convert(datetime,ToDate,103)<= Convert(Datetime,'" + ToDate.ToString() + "',103)";

            if (ReportType.ToString() == "1" || ReportType.ToString() == "2")
            {
                //ESI
                SSQL = SSQL + " and (EmpDet.Eligible_PF='1') And SalDet.ESI >= 1";
            }
            if (CateName != "-Select-")
            {
                SSQL = SSQL + " and EmpDet.CatName='" + CateName + "'";
            }
            if (EmpType != "-Select-")
            {
                SSQL = SSQL + " and EmpDet.Wages='" + EmpType + "'";
            }
            else
            {
                SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
            }


            SSQL = SSQL + " group by EmpDet.ESINo,EmpDet.Firstname,EmpDet.IsActive,EmpDet.DOR order by Cast(EmpDet.ESINo as BIGINT) ASC";
        }

        //PF
        if (ReportType.ToString() == "4")
        {
            SSQL = "";
            SSQL = SSQL + " Select ('&nbsp;'+EmpDet.UAN) as UAN,EmpDet.FirstName,SUM(isnull(CAST(SalDet.GrossEarnings as decimal(18,0)),'0')) as Gross,sum(isnull(CAST(SalDet.GrossEarnings as decimal(18,0)),'0')) as EPFWages,";
            SSQL = SSQL + " (CASE WHEN (sum(isnull(CAST(SalDet.GrossEarnings as decimal(18,0)),'0')) < PFM.StaffSalary) then CAST(SalDet.GrossEarnings as decimal(18,0)) else PFM.StaffSalary end) as EPSWages, ";
            SSQL = SSQL + "(CASE WHEN (sum(isnull(CAST(SalDet.GrossEarnings as decimal(18,0)),'0')) < PFM.StaffSalary) then CAST(SalDet.GrossEarnings as decimal(18,0)) else PFM.StaffSalary end) as EDLIWages,";
            SSQL = SSQL + "sum(isnull(SalDet.ProvidentFund,'0')) as EPF,cast(((ROUND((CAST(SalDet.GrossEarnings as decimal(18,0)) * Cast(PFM.EmployeerPFone as decimal(18,2))), -1) / 100)) as int) as EPS,";
            SSQL = SSQL + "(SalDet.ProvidentFund-cast(((ROUND((CAST(SalDet.GrossEarnings as decimal(18,0)) * Cast(PFM.EmployeerPFone as decimal(18,2))), -1) / 100)) as int)) as EPFDiffer ,";
            SSQL = SSQL + " (sum(31))-(Sum(isnull(SalDet.WorkedDays,'0'))) as Leave";
            SSQL = SSQL + " from Employee_Mst EmpDet inner join [" + SessionPayroll + "]..MstESIPF PFM on PFM.CCode='" + SessionCcode + "' and PFM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " inner join [" + SessionPayroll + "]..SalaryDetails SalDet on Saldet.EmpNo=EmpDet.EmpNo";
            SSQL = SSQL + " where SalDet.Month='" + str_month + "'";
            SSQL = SSQL + " and Convert(datetime,FromDate,103)>=Convert(datetime,'" + fromdate.ToString() + "',103) and Convert(datetime,ToDate,103)<= Convert(Datetime,'" + ToDate.ToString() + "',103)";
            SSQL = SSQL + " and (EmpDet.Eligible_PF='1') And SalDet.ProvidentFund >= 1";
            if (CateName != "-Select-")
            {
                SSQL = SSQL + " and EmpDet.CatName='" + CateName + "'";
            }
            if (EmpType != "-Select-")
            {
                SSQL = SSQL + " and EmpDet.Wages='" + EmpType + "'";
            }
            else
            {
                SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
            } 
            SSQL = SSQL + " group by EmpDet.UAN,EmpDet.FirstName,PFM.StaffSalary,CAST(SalDet.GrossEarnings as decimal(18,0)),PFM.EmployeerPFone,SalDet.ProvidentFund,SalDet.Emp_Pf";
            SSQL = SSQL + " Order by EmpDet.UAN Asc";
        }

        SqlCommand cmd = new SqlCommand(SSQL, con);
        cmd.CommandTimeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SqlCommandTimeOut"]);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();

        //DataTable dt_1 = new DataTable();
        //DataSet ds1 = new DataSet();
        //dt_1 = objdata.RptEmployeeMultipleDetails(query);
        //ds1.Tables.Add(dt_1);

        if (dt_1.Rows.Count > 0)
        {
            if (ReportType.ToString() == "4")
            {
                GVPF.DataSource = dt_1;
                GVPF.DataBind();
                string attachment = "attachment;filename=PF ONLINE.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GVPF.RenderControl(htextw);
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                GVESI.DataSource = dt_1;
                GVESI.DataBind();
                string attachment = "attachment;filename=ESI ONLINE.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GVESI.RenderControl(htextw);
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }


        //if (ds1.Tables[0].Rows.Count > 0)
        //{
        //    if (Basic_Report_Type == "NEW")
        //    {
        //        //New PF & ESI Report Start
        //        if (ReportType == "1")
        //        {
        //            rd.Load(Server.MapPath("Payslip_New_Component/ESIReport.rpt"));
        //        }
        //        else if (ReportType == "2")
        //        {
        //            rd.Load(Server.MapPath("Payslip_New_Component/ESIOnline.rpt"));
        //        }

        //        else if (ReportType == "3")
        //        {
        //            rd.Load(Server.MapPath("Payslip_New_Component/PFReport.rpt"));
        //        }

        //        else if (ReportType == "4")
        //        {
        //            rd.Load(Server.MapPath("Payslip_New_Component/PFOnline.rpt"));
        //        }
        //        else
        //        {
        //            //rd.Load(Server.MapPath("PaySlip1.rpt"));
        //        }
        //        //New PF & ESI Report end
        //    }
        //    else
        //    {
        //        //OLD PF & ESI Report Start
        //        if (ReportType == "1")
        //        {
        //            rd.Load(Server.MapPath("Payslip/ESIReport.rpt"));
        //        }
        //        else if (ReportType == "2")
        //        {
        //            rd.Load(Server.MapPath("Payslip/ESIOnline.rpt"));
        //        }

        //        else if (ReportType == "3")
        //        {
        //            rd.Load(Server.MapPath("Payslip/PFReport.rpt"));
        //        }

        //        else if (ReportType == "4")
        //        {
        //            rd.Load(Server.MapPath("Payslip/PFOnline.rpt"));
        //        }
        //        else
        //        {
        //            //rd.Load(Server.MapPath("PaySlip1.rpt"));
        //        }
        //        //OLD PF & ESI Report end
        //    }
        //    //Get PF And ESI Percentage
        //    query = "Select * from [" + SessionPayroll + "]..MstESIPF where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //    DataTable PF_ESI_Percnt_ds = new DataTable();
        //    PF_ESI_Percnt_ds = objdata.RptEmployeeMultipleDetails(query);

        //    //Get UAN NO
        //    for (int k = 0; k < ds1.Tables[0].Rows.Count; k++)
        //    {
        //        //Get UAN No in eAlert Employee_Mst Table
        //        DataTable DT_UAN = new DataTable();
        //        query = "Select isnull(UAN,'') as UAN from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //        query = query + " And ExistingCode='" + ds1.Tables[0].Rows[k]["ExisistingCode"].ToString() + "'";
        //        DT_UAN = objdata.RptEmployeeMultipleDetails(query);
        //        if (DT_UAN.Rows.Count != 0)
        //        {
        //            ds1.Tables[0].Rows[k]["UAN"] = DT_UAN.Rows[0]["UAN"];
        //        }
        //    }
        //    rd.SetDataSource(ds1.Tables[0]);
        //    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
        //    rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
        //    rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

        //    if (ReportType == "3" || ReportType == "1")
        //    {
        //        if (PF_ESI_Percnt_ds.Rows.Count != 0)
        //        {
        //            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
        //            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
        //            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
        //        }
        //        else
        //        {
        //            rd.DataDefinition.FormulaFields["PF_Percent_One"].Text = "'" + "3.67" + "'";
        //            rd.DataDefinition.FormulaFields["PF_Percent_Two"].Text = "'" + "8.33" + "'";
        //            rd.DataDefinition.FormulaFields["ESI_Percent_One"].Text = "'" + "4.75" + "'";
        //        }
        //    }
        //    if (ReportType == "1")
        //    {
        //        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "ESI MONTHLY STATEMENT FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
        //    }
        //    else if (ReportType == "2")
        //    {
        //        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "ESI MONTHLY RETURN FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
        //    }
        //    else if (ReportType == "3")
        //    {
        //        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "PF MONTHLY STATEMENT FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
        //    }
        //    else if (ReportType == "4")
        //    {
        //        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + " " + "'";
        //    }
        //    else
        //    {
        //        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + " " + "'";
        //    }
        //    rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month.ToUpper() + " - " + YR.ToString() + "'";
        //    rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

        //    rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

        //    CrystalReportViewer1.ReportSource = rd;
        //    CrystalReportViewer1.RefreshReport();
        //    CrystalReportViewer1.DataBind();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        //}
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
       // CrystalReportViewer1.Dispose();
    }

    protected void GVPF_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void GVESI_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
