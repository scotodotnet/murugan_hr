﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Murugan :: HR Module</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet" />
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.min.css" rel="stylesheet" />
    <link href="assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top">
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <!-- end #page-loader -->

    <div class="login-cover">
        <div class="login-cover-image">
            <img src="assets/img/login-bg/Login_bg_4.jpg" data-id="login-cover-image" alt="" style="height: 100%; width: 100%" /></div>
        <div class="login-cover-bg"></div>
    </div>
    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated fadeIn">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <p style="line-height: 10px; padding-top: 1.5em;"><span class="logsso"></span><span style="font-family: Colonna MT; font-size: 35px; color: #F8C63E;">SCOTO</span> ::  HR Module</p>
                    <small style="color: #fff; font-size: 12px;">Biometric Automation :: "Time Spent On Hiring Is Time Well Spent."</small>
                </div>
                <%--<div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>--%>
            </div>
            <!-- end brand -->
            <div class="login-content">
                <form class="margin-bottom-0" runat="server">

                    <div class="form-group m-b-20">
                        <asp:DropDownList runat="server" ID="ddlCode" class="form-control input-lg" AutoPostBack="true" OnSelectedIndexChanged="ddlCode_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group m-b-20">
                        <asp:DropDownList runat="server" ID="ddlLocation" class="form-control input-lg">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group m-b-20" runat="server" visible="false">
                        <asp:DropDownList runat="server" ID="ddlLoginType" class="form-control input-lg">
                            <asp:ListItem Value="System Admin" Text="System Admin"></asp:ListItem>
                             <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group m-b-20">
                        <asp:TextBox class="form-control input-lg" placeholder="User ID" ID="txtusername" required runat="server" />
                    </div>
                    <div class="form-group m-b-20">
                        <asp:TextBox TextMode="Password" ID="txtpassword" class="form-control input-lg" placeholder="Password" required runat="server" />
                    </div>
                    <div class="checkbox m-b-20">
                        <label>
                            <input type="checkbox" />
                            Remember Me
                       
                        </label>
                        <small runat="server" id="ErrorDisplay">User ID and Password Incorrect...</small>
                    </div>
                    <div class="login-buttons">
                        <asp:Button type="submit" class="btn btn-success btn-block btn-lg" runat="server" Text="Sign me in" ID="btnLogin" OnClick="btnLogin_Click" />
                    </div>
                </form>
            </div>
        </div>
        <!-- end login -->
    </div>
    <!-- end page container -->
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="assets/js/login-v2.demo.min.js"></script>
    <script src="assets/js/apps.min.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->

    <script>
        $(document).ready(function () {
            App.init();
            LoginV2.init();
        });
	</script>
</body>
</html>
