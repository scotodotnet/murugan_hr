﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EmployeeApproval.aspx.cs" Inherits="EmployeeApproval" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Employee Profile</a></li>
				<li class="active">Employee Approval</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Employee Approval </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Employee Approval</h4>
                        </div>
                        <div class="panel-body">
                       
                        <!-- begin row -->
			            <div class="row"> 
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Division</label>
                          <asp:DropDownList ID="ddlDivision" runat="server" class="form-control select2" style="width:100%">
                          </asp:DropDownList>
                        </div>
			            </div>
                        <!-- begin col-4 -->
                        <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSearch" Text="Search" class="btn btn-success" onclick="btnSearch_Click"/>
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>
                        <!-- end row -->
                        
                          <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>Emp No</label>
                          <asp:Label ID="lblEmpNo" runat="server" CssClass="form-control" BackColor="#F3F3F3"></asp:Label>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        <!-- begin col-3 -->
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Emp Name</label>
                          <asp:Label ID="lblEmpName" runat="server" CssClass="form-control"  BackColor="#F3F3F3"></asp:Label>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        <!-- begin col-3 -->
			            <div class="col-md-4">
			            <div class="form-group">
                          <label>Reason</label>
                          <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        <!-- begin col-4 -->
                                <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnCancel" Text="Cancel" class="btn btn-danger" 
                                         onclick="btnCancel_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>EmpNo</th>
                                                <th>FirstName</th>
                                                <th>DeptName</th>
                                                <th>Status</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("EmpNo")%></td>
                                    <td><%# Eval("FirstName")%></td>
                                    <td><%# Eval("DeptName")%></td>
                                    <td><%# Eval("Emp_Status")%></td>
                                    <td>
                                    <asp:LinkButton ID="btnViewEnquiry_Grid" class="btn btn-success btn-sm fa fa-print"  runat="server" 
                                           Text="" OnCommand="GridViewEnquiryClick" CommandArgument='<%# Eval("FirstName")%>' CommandName='<%# Eval("EmpNo")%>'>
                                     </asp:LinkButton>
                                     
                                     <asp:LinkButton ID="btnCancelEnquiry_Grid" class="btn btn-danger btn-sm fa fa-ban"  runat="server" 
                                        Text="" OnCommand="GridCancelEnquiryClick" CommandArgument='<%# Eval("FirstName")+","+ Eval("Cancel_Reson")%>' CommandName='<%# Eval("EmpNo")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Employee details?');">
                                     </asp:LinkButton>
                                     <asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn btn-success btn-sm fa fa-check"  runat="server" 
                                           Text="" OnCommand="GridApproveEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("EmpNo")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Employee details?');">
                                     </asp:LinkButton>
                                     <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("MachineID")%>'>
                                                    </asp:LinkButton>
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

