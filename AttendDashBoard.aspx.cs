﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Drawing;

public partial class AttendDashBoard : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEpay;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
        }
        if (!IsPostBack)
        {
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu-Dashboard"));

            Current_Employee_Strength_Departmentwise();
            Current_Employee_Attendance_Departmentwise();
            Current_Year_Employee_Join();
            Salary_Strengtn_Unitwise();
        }

    }

    private void Current_Employee_Strength_Departmentwise()
    {
        DataTable dt = new DataTable();
        string query = "";

        query = "Select DeptName ,count(EmpNo) as present ";
        query = query + " from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' ";
        query = query + " group by DeptName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Employee_Strength_DeptWise.DataSource = dt;
        Employee_Strength_DeptWise.Series["Series1"].XValueMember = "DeptName";
        Employee_Strength_DeptWise.Series["Series1"].YValueMembers = "present";

        this.Employee_Strength_DeptWise.Series[0]["PieLabelStyle"] = "Outside";
        this.Employee_Strength_DeptWise.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Employee_Strength_DeptWise.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Employee_Strength_DeptWise.Series[0].BorderWidth = 1;
        this.Employee_Strength_DeptWise.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Employee_Strength_DeptWise.Legends.Clear();
        this.Employee_Strength_DeptWise.Legends.Add("Legend1");
        this.Employee_Strength_DeptWise.Legends[0].Enabled = true;
        this.Employee_Strength_DeptWise.Legends[0].Docking = Docking.Bottom;
        this.Employee_Strength_DeptWise.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Employee_Strength_DeptWise.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Employee_Strength_DeptWise.DataBind();
    }

    private void Current_Employee_Attendance_Departmentwise()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt_break = new DataTable();
        DataTable dt_latein = new DataTable();
        DataTable dt_Amt = new DataTable();

        DataTable Datacells = new DataTable();
        string SSQL = "";
        DateTime todaydate = DateTime.Now.AddDays(-1);
        string date = "20/03/2021";
        DateTime dtdate = Convert.ToDateTime(todaydate.ToString());

        Datacells.Columns.Add("TypeName");
        Datacells.Columns.Add("Count");

        SSQL = "";
        SSQL = "select Count(LD.MachineID)as presentCount from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID  ";
        SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "' And EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtdate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        SSQL = "select Count(MachineID) as Improper";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode + "' ANd LocCode='"+SessionLcode+"'";
        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtdate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt1.Rows[0]["Improper"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }


        SSQL = "Select Count(MachineID)as Hostel from LogTimeHostel_Lunch where  CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " And TimeIN >='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
        SSQL = SSQL + " And TimeIN <='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00" + "' ";
        dt_break = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (dt_break.Rows.Count != 0)
        //{
        //    Datacells.NewRow();
        //    Datacells.Rows.Add();
        //    Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
        //    Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_break.Rows[0]["Hostel"].ToString();

        //}
        //else
        //{
        //    Datacells.NewRow();
        //    Datacells.Rows.Add();
        //    Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
        //    Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        //}

        SSQL = "";
        SSQL = "Select Count(MachineID)as presentCount from LogTime_Days ";
        SSQL = SSQL + " where CompCode='" + SessionCcode + "' ANd LocCode='"+SessionLcode+"'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtdate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift ='No Shift' And TimeIN !=''  ";
        dt_latein = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_latein.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_latein.Rows[0]["presentCount"].ToString();
        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;
        }

        SSQL = "";
        SSQL = " select  isnull (sum((case EM.CatName when 'LABOUR' then BaseSalary else (BaseSalary/26) end)),0) as Amt ";
        SSQL = SSQL + "from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID =LD.MachineID ";
        SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='"+SessionLcode+"'";
        SSQL = SSQL + " and EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='"+SessionLcode+"'";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtdate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And LD.Present !='0.0'  ";
        dt_Amt = objdata.RptEmployeeMultipleDetails(SSQL);


        //if (dt_Amt.Rows.Count != 0)
        //{
        //    Decimal amt = Convert.ToDecimal(dt_Amt.Rows[0]["Amt"].ToString());
        //    decimal rounded = Math.Round(amt, 2);
        //    Datacells.NewRow();
        //    Datacells.Rows.Add();
        //    Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
        //    Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = rounded;

        //}
        //else
        //{
        //    Datacells.NewRow();
        //    Datacells.Rows.Add();
        //    Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
        //    Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        //}
        UnitoneChart.DataSource = Datacells;
        UnitoneChart.Series["Series1"].XValueMember = "TypeName";
        UnitoneChart.Series["Series1"].YValueMembers = "Count";

        this.UnitoneChart.Series[0]["PieLabelStyle"] = "Outside";
        this.UnitoneChart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.UnitoneChart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.UnitoneChart.Series[0].BorderWidth = 1;
        this.UnitoneChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.UnitoneChart.Legends.Clear();
        this.UnitoneChart.Legends.Add("Legend1");
        this.UnitoneChart.Legends[0].Enabled = true;
        this.UnitoneChart.Legends[0].Docking = Docking.Bottom;
        this.UnitoneChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.UnitoneChart.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        UnitoneChart.DataBind();
    }

    private void Current_Year_Employee_Join()
    {
        DataTable DT1 = new DataTable();
        DataTable dt = new DataTable();
        string query = "";

        query = "Select isnull([Yes],0) as YES,isnull([No],0) as No from ";
        query = query + "(select (IsActive) as Shift_Join, count(EmpNo) as present ";
        query = query + "from Employee_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And DATEPART(yyyy, (CONVERT(DATETIME,DOJ, 103))) = DATEPART(yyyy, GETDATE()) ";
        query = query + "group by  IsActive ) as p  pivot (max(present)";
        query = query + " for Shift_Join in ( [Yes],[No] )) as pvtt";
        DT1 = objdata.RptEmployeeMultipleDetails(query);

        if (DT1.Rows.Count != 0)
        {
            txtNewJoin.Text = DT1.Rows[0]["YES"].ToString();
            txtLeft.Text = DT1.Rows[0]["No"].ToString();
        }


        query = "Select LocCode,isnull([Yes],0) as YES,isnull([No],0) as No";
        query = query + " from (select LocCode ,(IsActive) as Shift_Join, count(EmpNo) as present ";
        query = query + " from Employee_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And DATEPART(yyyy, (CONVERT(DATETIME,DOJ, 103))) = DATEPART(yyyy, GETDATE()) ";
        query = query + " group by LocCode, IsActive ) as p ";
        query = query + " pivot (max(present) for Shift_Join in ( [Yes],[No] )) as pvtt";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Current_Year_Emp_Join.DataSource = dt;
        Current_Year_Emp_Join.ChartAreas.Clear();
        Current_Year_Emp_Join.Series.Clear();
        Current_Year_Emp_Join.Legends.Clear();
        Current_Year_Emp_Join.ChartAreas.Add("ChartArea2");
        Current_Year_Emp_Join.ChartAreas[0].AxisX.Title = "UNIT";
        Current_Year_Emp_Join.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Verdasa", 11, System.Drawing.FontStyle.Bold);

        //Current_Year_Emp_Join.ChartAreas[0].AxisY.Title = "Join/Left";
        Current_Year_Emp_Join.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Verdasa", 11, System.Drawing.FontStyle.Bold);

        Current_Year_Emp_Join.ChartAreas[0].BorderDashStyle = ChartDashStyle.Solid;
        Current_Year_Emp_Join.ChartAreas[0].BorderWidth = 2;


        Current_Year_Emp_Join.Series.Add("Join");
        Current_Year_Emp_Join.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
        Current_Year_Emp_Join.Series[0].XValueMember = "LocCode";
        Current_Year_Emp_Join.Series[0].YValueMembers = "YES";

        Current_Year_Emp_Join.Series[0].IsVisibleInLegend = true;
        Current_Year_Emp_Join.Series[0].IsValueShownAsLabel = true;
        Current_Year_Emp_Join.Series[0].ToolTip = "Data Point Y value:#VALY(G)";

        Current_Year_Emp_Join.Series[0].BorderWidth = 3;
        Current_Year_Emp_Join.Series[0].Color = Color.Green;


        Current_Year_Emp_Join.Series.Add("Left");
        Current_Year_Emp_Join.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
        Current_Year_Emp_Join.Series[1].XValueMember = "LocCode";
        Current_Year_Emp_Join.Series[1].YValueMembers = "No";


        Current_Year_Emp_Join.Series[1].IsVisibleInLegend = true;
        Current_Year_Emp_Join.Series[1].IsValueShownAsLabel = true;
        Current_Year_Emp_Join.Series[1].ToolTip = "Data Point Y value:#VALY(G)";

        Current_Year_Emp_Join.Series[1].BorderWidth = 3;
        Current_Year_Emp_Join.Series[1].Color = Color.Red;

        Current_Year_Emp_Join.Legends.Add("Legend1");
        Current_Year_Emp_Join.Legends[0].Enabled = true;
        Current_Year_Emp_Join.Legends[0].Docking = Docking.Bottom;
        Current_Year_Emp_Join.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        Current_Year_Emp_Join.Series[0].LegendText = "JOIN";

        Current_Year_Emp_Join.Legends[0].Enabled = true;
        Current_Year_Emp_Join.Legends[0].Docking = Docking.Bottom;
        Current_Year_Emp_Join.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        Current_Year_Emp_Join.Series[1].LegendText = "LEFT";

        //Current_Year_Emp_Join.Series["Series1"].XValueMember = "LocCode";
        //Current_Year_Emp_Join.Series["Series1"].YValueMembers = "YES";
        //Current_Year_Emp_Join.Series["Series2"].XValueMember = "LocCode";
        //Current_Year_Emp_Join.Series["Series2"].YValueMembers = "YES";

        //this.Current_Year_Emp_Join.Series[0]["PieLabelStyle"] = "Outside";
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Enable3D = true;
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Inclination = 1;
        //this.Current_Year_Emp_Join.Series[0].BorderWidth = 1;
        //this.Current_Year_Emp_Join.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        //this.Current_Year_Emp_Join.Legends.Add("Legend1");
        //this.Current_Year_Emp_Join.Legends[0].Enabled = true;
        //this.Current_Year_Emp_Join.Legends[0].Docking = Docking.Bottom;
        //this.Current_Year_Emp_Join.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        //this.Current_Year_Emp_Join.Series[0].LegendText = "#PERCENT";

        //this.Current_Year_Emp_Join.Series[1]["PieLabelStyle"] = "Outside";
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Enable3D = true;
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Inclination = 1;
        //this.Current_Year_Emp_Join.Series[1].BorderWidth = 1;
        //this.Current_Year_Emp_Join.Series[1].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        //this.Current_Year_Emp_Join.Legends.Add("Legend2");
        //this.Current_Year_Emp_Join.Legends[1].Enabled = true;
        //this.Current_Year_Emp_Join.Legends[1].Docking = Docking.Bottom;
        //this.Current_Year_Emp_Join.Legends[1].Alignment = System.Drawing.StringAlignment.Center;
        //this.Current_Year_Emp_Join.Series[1].LegendText = "#PERCENT";
        ////this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        //Current_Year_Emp_Join.DataBind();
    }


    //private void Current_Employee_Attendance_Departmentwise()
    //{

    //    DataTable dt = new DataTable();
    //    string query = "";
    //    DataTable DT_Date = new DataTable();
    //    DateTime Curr_Date = new DateTime();

    //    query = "Select GetDate() as CurrDate";
    //    DT_Date = objdata.RptEmployeeMultipleDetails(query);
    //    if (DT_Date.Rows.Count != 0)
    //    {
    //        Curr_Date = Convert.ToDateTime(DT_Date.Rows[0]["CurrDate"].ToString());
    //    }


    //    query = "Select DeptName ,count(MachineID) as present ";
    //    query = query + " from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
    //    query = query + " And Attn_Date_Str='03/05/2019' And Present!='0.0'";
    //    //query = query + " And Attn_Date_Str='" + Curr_Date.ToString("dd/MM/yyyy") + "' And Present!='0.0'";
    //    query = query + " group by DeptName";

    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    Employee_Attendance_DeptWise.DataSource = dt;
    //    Employee_Attendance_DeptWise.Series["Series1"].XValueMember = "DeptName";
    //    Employee_Attendance_DeptWise.Series["Series1"].YValueMembers = "present";

    //    this.Employee_Attendance_DeptWise.Series[0]["PieLabelStyle"] = "Outside";
    //    this.Employee_Attendance_DeptWise.ChartAreas[0].Area3DStyle.Enable3D = true;
    //    this.Employee_Attendance_DeptWise.ChartAreas[0].Area3DStyle.Inclination = 1;
    //    this.Employee_Attendance_DeptWise.Series[0].BorderWidth = 1;
    //    this.Employee_Attendance_DeptWise.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
    //    this.Employee_Attendance_DeptWise.Legends.Clear();
    //    this.Employee_Attendance_DeptWise.Legends.Add("Legend1");
    //    this.Employee_Attendance_DeptWise.Legends[0].Enabled = true;
    //    this.Employee_Attendance_DeptWise.Legends[0].Docking = Docking.Bottom;
    //    this.Employee_Attendance_DeptWise.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
    //    this.Employee_Attendance_DeptWise.Series[0].LegendText = "#PERCENT";
    //    //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
    //    Employee_Attendance_DeptWise.DataBind();
    //}

    private void Salary_Strengtn_Unitwise()
    {
        DataTable DT1 = new DataTable();
        DataTable dt = new DataTable();
        string query = "";
       
        DateTime todaydate = DateTime.Now.AddDays(-1);
        string date = "20/03/2021";
        DateTime dtdate = Convert.ToDateTime(todaydate.ToString());

        query = "";
        query = "Select EM.DeptName,isnull (cast(sum((case EM.CatName when 'LABOUR' then BaseSalary else (BaseSalary/26) end))as decimal(18,0)),0) as Amt ";
        query = query + "from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID =LD.MachineID ";
        query = query + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
        query = query + " and EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        query = query + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtdate).ToString("dd/MM/yyyy") + "',103)";
        query = query + "  And LD.Present !='0.0' group by EM.DeptName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Salary_Strength_Unitwise.DataSource = dt;
        Salary_Strength_Unitwise.Series["Series1"].XValueMember = "DeptName";
        Salary_Strength_Unitwise.Series["Series1"].YValueMembers = "Amt";

        this.Salary_Strength_Unitwise.Series[0]["PieLabelStyle"] = "Outside";
        this.Salary_Strength_Unitwise.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Salary_Strength_Unitwise.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Salary_Strength_Unitwise.Series[0].BorderWidth = 1;
        this.Salary_Strength_Unitwise.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Salary_Strength_Unitwise.Legends.Clear();
        this.Salary_Strength_Unitwise.Legends.Add("Legend1");
        this.Salary_Strength_Unitwise.Legends[0].Enabled = true;
        this.Salary_Strength_Unitwise.Legends[0].Docking = Docking.Bottom;
        this.Salary_Strength_Unitwise.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Salary_Strength_Unitwise.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Salary_Strength_Unitwise.DataBind();
    }

}
