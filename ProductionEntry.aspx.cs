﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ProductionEntry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Location();
        }
        Load_New_Data();
    }

    private void Load_New_Data()
    {
        SSQL = "";
        SSQL = "Select * from ProductionOnDate";
        Repeater2.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataBind();
    }

    private void Load_Location()
    {
        SSQL = "";
        SSQL = "Select LocCode from Location_Mst ";
        DataTable Dt_Loc = new DataTable();
        Dt_Loc = objdata.RptEmployeeMultipleDetails(SSQL);
        ddLocation.DataSource = Dt_Loc;
        ddLocation.DataTextField = "LocCode";
        ddLocation.DataValueField = "LocCode";
        ddLocation.DataBind();
        ddLocation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddLocation.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Location');", true);
            return;
        }
        if (txtproduction.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Fixed Production Error');", true);
            return;
        }
        if (txtProductionEarned.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Production');", true);
            return;
        }
        if (txtDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Production Date');", true);
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "delete from ProductionOnDate where lcode='" + ddLocation.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into ProductionOnDate(Ccode,LCode,ProductionFixed,ProductionEarned,Tarns_Date,Tarns_Date_str,UserName)";
            SSQL = SSQL + " values('" + SessionCcode + "','" + ddLocation.SelectedValue + "','" + txtproduction.Text + "','" + txtProductionEarned.Text + "',Convert(date,'" + txtDate.Text + "',103),";
            SSQL = SSQL + " '" + txtDate.Text + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert(Data Saved Successfully!!!);", true);
            Load_New_Data();
            btnClear_Click(sender, e);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddLocation.ClearSelection();
        txtproduction.Text = "";
        txtProductionEarned.Text = "";

    }

    protected void btnEdit_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from ProductionOnDate where Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt_Edit = new DataTable();
        dt_Edit = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Edit.Rows.Count > 0)
        {
            ddLocation.SelectedValue = dt_Edit.Rows[0]["LCode"].ToString();
            txtproduction.Text = dt_Edit.Rows[0]["ProductionFixed"].ToString();
            txtProductionEarned.Text = dt_Edit.Rows[0]["ProductionEarned"].ToString();
        }
        Load_New_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from ProductionOnDate where Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_New_Data();
    }

    protected void ddLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select Production from mstProduction where Ccode='" + SessionCcode + "' and LCode='" + ddLocation.SelectedValue + "'";
        DataTable dt_get = new DataTable();
        dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_get.Rows.Count > 0)
        {
            txtproduction.Text = dt_get.Rows[0]["Production"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Fixed Production Error');", true);
            txtproduction.Text = "0";
        }
    }
}