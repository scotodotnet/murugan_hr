﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstProduction.aspx.cs" Inherits="MstProduction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                }
            });
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>


            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Master</a></li>
                    <li><a href="javascript:;">Production</a></li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header"><small>Production</small></h1>
                <!-- end page-header -->
                <!--Form Page-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    @*<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>*@
                                </div>
                                <h4 class="panel-title">Production</h4>
                            </div>
                            <div class="panel-body panel-form">
                                <div class="form-horizontal form-bordered">
                                    <div id="LabourSub" style="display: block;">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Location</label>
                                                    <asp:DropDownList ID="ddLocation" runat="server"
                                                        class="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Production</label>
                                                    <asp:TextBox ID="txtProduction" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="control-label col-md-4 col-sm-4"></label>
                                                <div class="col-md-4 col-sm-4">
                                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                                </div>
                                                <div class="col-md-4 col-sm-4"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="LabourMain" style="display: block;">
                                        <div class="form-group">
                                            <br />
                                            <div class="row">
                                                <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="display table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Location</th>
                                                                    <th>Production</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("LCode")%></td>
                                                            <td><%# Eval("Production")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEdit" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEdit_Command" CommandArgument="Edit" CommandName='<%# Eval("Auto_ID")%>'
                                                                    CausesValidation="true">
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Labour Allotment details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

