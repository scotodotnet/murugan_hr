﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;


public partial class DailyMDMail_UNITII : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionRights;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    ReportDocument report2 = new ReportDocument();
    ReportDocument report3 = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;
    DataTable DataCells = new DataTable();

    MailMessage Mail = new MailMessage("SCM_OE <aatm2005@gmail.com>", "selvakumar.kv@scoto.in");
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | MD Mailing Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

           Date = Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString();
           // Date = Convert.ToDateTime("14/06/2019").ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            GetAttdDayWise_Change();

            GetImproperPunch();

            SalaryConsolidate();

            MailReport();

        }
    }
    //protected void Page_Unload(object sender, EventArgs e)
    //{
    //    CrystalReportViewer1.Dispose();
    //    CrystalReportViewer2.Dispose();
    //    CrystalReportViewer3.Dispose();
    //}

    private void MailReport()
    {
        //Mail.CC = "padmanaban";
        Mail.Subject = "Daily Attendance Report";
        Mail.Body = "Dear Sir, Please find the Attachement below for (" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + ") Attendance Report";
        // string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        AttachfileName_Miss = Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";
        if (File.Exists(AttachfileName_Miss))
        {
            Mail.Attachments.Add(new Attachment(AttachfileName_Miss));
        }
        Mail.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        NetworkCredential NetworkCred = new NetworkCredential("aatm2005@gmail.com", "Altius@2005");
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(Mail);
        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
        //File.Delete(Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy") + ".pdf");
    }

    private void SalaryConsolidate()
    {
        string TableName = "";

        string GrandTotal = "";
        DataTable AnutoDT = new DataTable();
        string GrandTotalWages = "";

        int generalcount = 0;
        int shift1count = 0;
        int shift2count = 0;
        int shift3count = 0;
        int shift4count = 0;
        int shift5count = 0;
        int shift6count = 0;
        int noshiftcount = 0;

        int general = 0;
        int shift1 = 0;
        int shift2 = 0;
        int shift3 = 0;
        int shift4 = 0;
        int shift5 = 0;
        int shift6 = 0;
        int noshift = 0;

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        decimal rounded = 0;
        decimal roundedsalary = 0;
        decimal staffonedaysalary;
        // AnutoDT.Reset();

        AnutoDT.Columns.Add("ExCode");
        AnutoDT.Columns.Add("Dept");
        AnutoDT.Columns.Add("Name");
        AnutoDT.Columns.Add("TimeIN");
        AnutoDT.Columns.Add("TimeOUT");
        AnutoDT.Columns.Add("DayWages");
        AnutoDT.Columns.Add("TotalMIN");
        AnutoDT.Columns.Add("Wages");
        AnutoDT.Columns.Add("Shift");
        AnutoDT.Columns.Add("SubCatName");
        AnutoDT.Columns.Add("GradeName");
        AnutoDT.Columns.Add("OTSal");
        AnutoDT.Columns.Add("Total");

        AnutoDT.Columns.Add("GradeTotalCount");
        AnutoDT.Columns.Add("GeneralCount");
        AnutoDT.Columns.Add("Shift1Count");
        AnutoDT.Columns.Add("Shift2Count");
        AnutoDT.Columns.Add("Shift3Count");

        AnutoDT.Columns.Add("ShiftWages");

        AnutoDT.Columns.Add("OTHours");

        DataTable Shift_DS = new DataTable();

        SSQL = "select EM.MachineID,EM.Wages,DEP.DeptName,EM.Shift,isnull(EM.FirstName,'') as FirstName,EM.TimeIN,EM.TimeOUT,";
        SSQL = SSQL + "DEP.SubCatName,EM.Total_Hrs,EM.Total_Hrs1,DEP.CatName,DEP.BaseSalary,DEP.StdWrkHrs,DEP.OTEligible,DEP.IsActive from LogTime_Days EM  inner join Employee_Mst DEP on  EM.MachineID= DEP.MachineID ";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEP.CompCode='" + SessionCcode + "' ANd DEP.LocCode='" + SessionLcode + "'";

        SSQL = SSQL + " And EM.Shift!='Leave'";
        SSQL = SSQL + " And DEP.DeptName!='CONTRACT'";

        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " And  (Present_Absent='Present' or Present_Absent='Half Day') ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "Select Count(*) from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " and TypeName='Improper'";

        DataTable imp_dt = new DataTable();
        imp_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        int impcount = 0;

        if (imp_dt.Rows.Count > 0)
        {
            impcount = Convert.ToInt32(imp_dt.Rows[0][0].ToString());
        }

        string staffsalary = "";
        string staffwages = "";

        string TempWorkHours = "";

        string[] TempWorkHourSlip;
        string Machineid = "";
        string Deptname = "";
        string workinghours = "";
        string stdwrkhrs = "";
        decimal staffonehoursalary;
        decimal Totalwages;
        staffonedaysalary = 0;

        string OThours = ""; //Edit by Narmatha
        string OTSal = "0"; //Edit by Narmatha

        string OTEligible = "";//By Selva
        string IsActive = "";//By Selva

        DataTable dt1 = new DataTable();

        if (dt.Rows.Count > 0)
        {
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                Machineid = dt.Rows[k]["MachineID"].ToString();
                staffwages = dt.Rows[k]["CatName"].ToString();
                Deptname = dt.Rows[k]["DeptName"].ToString();
                stdwrkhrs = dt.Rows[k]["StdWrkHrs"].ToString();

                OTEligible = dt.Rows[k]["OTEligible"].ToString();//By Selva
                IsActive = dt.Rows[k]["IsActive"].ToString();//By Selva

                OTSal = "0";//Edit by Narmatha

                if (stdwrkhrs == "" || stdwrkhrs == "0")
                {
                    stdwrkhrs = "8";
                }

                //Check Above 8 hours conditions
                TempWorkHours = dt.Rows[k]["Total_Hrs1"].ToString();
                TempWorkHourSlip = TempWorkHours.Split(':');

                workinghours = dt.Rows[k]["Total_Hrs"].ToString();
                OThours = "0"; //Edit by Narmatha

                if (Machineid == "920016")
                {
                    Machineid = "920016";
                }
                if (staffwages == "STAFF" || Deptname == "FITTER & ELECTRICIANS" || Deptname == "SECURITY" || Deptname == "DRIVERS")
                {
                    if (dt.Rows[k]["OTEligible"].ToString().Trim() == "1")
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();

                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(check) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 2;
                            //Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(staffonehoursalary, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                    else
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(8) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = "0";
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 8;
                            Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                }
                else
                {
                    if (dt.Rows[k]["MachineID"].ToString() == "631")
                    {
                        string pass = "";
                    }
                    DataTable da_Days = new DataTable();
                    staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                    staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                    roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                    //OT salary for Labour by Narmatha
                    string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();

                    //By Selva v
                    if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) > 8))
                    {
                        OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                        workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                    }
                    else
                    {
                        OThours = "0";
                    }
                    //By Selva ^

                    if (dt.Rows[k]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                    {
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());

                        OTSal = (Convert.ToDecimal(staffonehoursalary) * Convert.ToDecimal(OThours.ToString())).ToString();
                        OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                        //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(stdwrkhrs.ToString());
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());

                        OTSal = (Convert.ToDecimal(staffonehoursalary) * Convert.ToDecimal(OThours.ToString())).ToString();
                        OTSal = Math.Round(Convert.ToDecimal(OTSal), 0, MidpointRounding.AwayFromZero).ToString();

                        //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                    }
                }


                AnutoDT.NewRow();
                AnutoDT.Rows.Add();
                AnutoDT.Rows[k]["ExCode"] = dt.Rows[k]["MachineID"].ToString();
                AnutoDT.Rows[k]["Dept"] = dt.Rows[k]["DeptName"].ToString();
                AnutoDT.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                AnutoDT.Rows[k]["TimeIN"] = dt.Rows[k]["TimeIN"].ToString();
                AnutoDT.Rows[k]["TimeOUT"] = dt.Rows[k]["TimeOUT"].ToString();
                AnutoDT.Rows[k]["DayWages"] = rounded;

                AnutoDT.Rows[k]["TotalMIN"] = workinghours;// dt.Rows[k]["Total_Hrs"].ToString();
                AnutoDT.Rows[k]["Wages"] = roundedsalary.ToString();
                AnutoDT.Rows[k]["Shift"] = dt.Rows[k]["Shift"].ToString();
                AnutoDT.Rows[k]["SubCatName"] = dt.Rows[k]["SubCatName"].ToString();
                AnutoDT.Rows[k]["GradeName"] = "A+";
                AnutoDT.Rows[k]["OTHours"] = OThours;
                AnutoDT.Rows[k]["OTSal"] = OTSal;


                if (dt.Rows[k]["Shift"].ToString() == "GENERAL")
                {
                    generalcount = generalcount + 1;
                    general = Convert.ToInt32(Convert.ToDecimal(general) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT1")
                {
                    shift1count = shift1count + 1;
                    shift1 = Convert.ToInt32(Convert.ToDecimal(shift1) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT2")
                {
                    shift2count = shift2count + 1;
                    shift2 = Convert.ToInt32(Convert.ToDecimal(shift2) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT3")
                {
                    shift3count = shift3count + 1;
                    shift3 = Convert.ToInt32(Convert.ToDecimal(shift3) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT4")
                {
                    shift4count = shift4count + 1;
                    shift4 = Convert.ToInt32(Convert.ToDecimal(shift4) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT5")
                {
                    shift5count = shift5count + 1;
                    shift5 = Convert.ToInt32(Convert.ToDecimal(shift5) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT6")
                {
                    shift6count = shift6count + 1;
                    shift6 = Convert.ToInt32(Convert.ToDecimal(shift6) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "No Shift")
                {
                    noshiftcount = noshiftcount + 1;
                    noshift = Convert.ToInt32(Convert.ToDecimal(noshift) + Convert.ToDecimal(rounded));
                }

                GrandTotal = (Convert.ToDecimal(generalcount) + Convert.ToDecimal(shift1count) + Convert.ToDecimal(shift2count) + Convert.ToDecimal(shift3count) + Convert.ToDecimal(shift4count) + Convert.ToDecimal(shift5count) + Convert.ToDecimal(shift6count) + Convert.ToDecimal(noshiftcount)).ToString();

                GrandTotalWages = (Convert.ToDecimal(general) + Convert.ToDecimal(shift1) + Convert.ToDecimal(shift2) + Convert.ToDecimal(shift3) + Convert.ToDecimal(shift4) + Convert.ToDecimal(shift5) + Convert.ToDecimal(shift6) + Convert.ToDecimal(noshift)).ToString();

            }
        }

        SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable Depart = new DataTable();

        SSQL = "select distinct  DeptName from Department_Mst ";
        SSQL = SSQL + " where DeptName!='CONTRACT' order by DeptName Asc"; //Edit By Narmatha
        Depart = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable SubCat = new DataTable();

        SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by SubCatName ";
        SubCat = objdata.RptEmployeeMultipleDetails(SSQL);

        string CName = "";
        string LocName = "";
        SSQL = "";
        SSQL = "Select * from [" + SessionRights + "]..AdminRights where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable Company_Dt = new DataTable();
        Company_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Company_Dt.Rows.Count > 0)
        {
            CName = Company_Dt.Rows[0]["Cname"].ToString().ToUpper();
            LocName = Company_Dt.Rows[0]["Lcode"].ToString().ToUpper() + " - " + Company_Dt.Rows[0]["Location"].ToString().ToUpper();
        }

        ds.Tables.Add(AnutoDT);
        //ReportDocument report = new ReportDocument();
        report3.Load(Server.MapPath("crystal/SalaryConsolidateNew.rpt"));
        report3.DataDefinition.FormulaFields["GradeTotalCount"].Text = "'" + GrandTotal + "'";

        report3.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "'";
        report3.DataDefinition.FormulaFields["GeneralCount"].Text = "'" + generalcount + "'";
        report3.DataDefinition.FormulaFields["Shift1Count"].Text = "'" + shift1count + "'";
        report3.DataDefinition.FormulaFields["Shift2Count"].Text = "'" + shift2count + "'";
        report3.DataDefinition.FormulaFields["Shift3Count"].Text = "'" + shift3count + "'";
        report3.DataDefinition.FormulaFields["Shift4Count"].Text = "'" + shift4count + "'";
        report3.DataDefinition.FormulaFields["Shift5Count"].Text = "'" + shift5count + "'";
        report3.DataDefinition.FormulaFields["Shift6Count"].Text = "'" + shift6count + "'";
        report3.DataDefinition.FormulaFields["NoShiftCount"].Text = "'" + noshiftcount + "'";
        
        report3.DataDefinition.FormulaFields["Cname"].Text = "'" + CName + "'";
        report3.DataDefinition.FormulaFields["Lname"].Text = "'" + LocName + "'";
        
        //Get All Salary
        report3.DataDefinition.FormulaFields["GrandTotalWages"].Text = "'" + GrandTotalWages + "'";
        report3.DataDefinition.FormulaFields["GenWages"].Text = "'" + general + "'";
        report3.DataDefinition.FormulaFields["S1Wages"].Text = "'" + shift1 + "'";
        report3.DataDefinition.FormulaFields["S2Wages"].Text = "'" + shift2 + "'";
        report3.DataDefinition.FormulaFields["S3Wages"].Text = "'" + shift3 + "'";
        report3.DataDefinition.FormulaFields["S4Wages"].Text = "'" + shift4 + "'";
        report3.DataDefinition.FormulaFields["S5Wages"].Text = "'" + shift5 + "'";
        report3.DataDefinition.FormulaFields["S6Wages"].Text = "'" + shift6 + "'";
        report3.DataDefinition.FormulaFields["NoShiftWages"].Text = "'" + noshift + "'";
        report3.DataDefinition.FormulaFields["impcount"].Text = "'" + impcount + "'";

        report3.Database.Tables[0].SetDataSource(AnutoDT);
        // report2.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

        //CrystalReportViewer2.ReportSource = report2;
        string Server_Path = Server.MapPath("~");
        string AttachfileName_Miss = "";
        //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/SalaryConsolidate_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

        if (File.Exists(AttachfileName_Miss))
        {
            File.Delete(AttachfileName_Miss);
        }

        report3.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
        report3.Close();
    }

    public void GetImproperPunch()
    {
        DataSet ds = new DataSet();

        DataCells.Columns.Add("CompanyName");
        DataCells.Columns.Add("LocationName");
        DataCells.Columns.Add("ShiftDate");

        DataCells.Columns.Add("SNo");
        DataCells.Columns.Add("Dept");
        DataCells.Columns.Add("Type");
        DataCells.Columns.Add("Shift");
        DataCells.Columns.Add("Category");
        DataCells.Columns.Add("SubCategory");
        DataCells.Columns.Add("EmpCode");
        DataCells.Columns.Add("ExCode");
        DataCells.Columns.Add("Name");
        DataCells.Columns.Add("TimeIN");
        DataCells.Columns.Add("TimeOUT");
        DataCells.Columns.Add("MachineID");
        DataCells.Columns.Add("PrepBy");
        DataCells.Columns.Add("PrepDate");
        DataCells.Columns.Add("TotalMIN");
        DataCells.Columns.Add("GrandTOT");
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt3 = new DataTable();
        double Count = 0;
        double Count1;

        DateTime dayy = Convert.ToDateTime(Date);



        string SSQL = "";

        DataTable dt2 = new DataTable();
        SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
        SSQL = SSQL + "Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }

        SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);



        SSQL = "";

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";



        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And (TypeName='Proper' or TypeName='Absent') And Shift!='No Shift' And TimeIN!=''";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') as FirstName,TypeName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst";
        dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt2.Rows.Count != 0)
        {
            if (dt1.Rows.Count != 0)
            {
                int sno = 1;
                DataTable Emp_dt = new DataTable();
                string category;
                string subCat;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                    SSQL = "Select * from ";
                   
                        SSQL = SSQL + " Employee_Mst";
                 
                    SSQL = SSQL + " where MachineID='" + dt1.Rows[i]["MachineID"].ToString() + "' ";
                    Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    category = "";
                    subCat = "";
                    if (Emp_dt.Rows.Count != 0)
                    {
                        category = Emp_dt.Rows[0]["CatName"].ToString();
                        subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                    }


                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[i]["CompanyName"] = dt3.Rows[0]["CompName"].ToString();
                    DataCells.Rows[i]["LocationName"] = SessionLcode;
                    DataCells.Rows[i]["ShiftDate"] = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                    DataCells.Rows[i]["SNo"] = sno;


                    DataCells.Rows[i]["Dept"] = dt1.Rows[i]["DeptName"].ToString();
                    DataCells.Rows[i]["Type"] = dt1.Rows[i]["TypeName"].ToString();
                    DataCells.Rows[i]["Shift"] = dt1.Rows[i]["Shift"].ToString();
                    DataCells.Rows[i]["Category"] = category;

                    DataCells.Rows[i]["SubCategory"] = subCat;
                    DataCells.Rows[i]["EmpCode"] = dt1.Rows[i]["MachineID"].ToString();
                    DataCells.Rows[i]["ExCode"] = dt1.Rows[i]["ExistingCode"].ToString();
                    DataCells.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();

                    DataCells.Rows[i]["TimeIN"] = dt1.Rows[i]["TimeIN"].ToString();
                    DataCells.Rows[i]["TimeOUT"] = dt1.Rows[i]["TimeOUT"].ToString();
                    DataCells.Rows[i]["MachineID"] = dt1.Rows[i]["MachineID"].ToString();

                    DataCells.Rows[i]["PrepBy"] = dt2.Rows.Count;
                    DataCells.Rows[i]["PrepDate"] = dt.Rows.Count;
                    DataCells.Rows[i]["TotalMIN"] = "";
                    DataCells.Rows[i]["TotalMIN"] = "";


                    sno = sno + 1;

                }
            }
        }

        if (DataCells.Rows.Count > 0)
        {
            ds.Tables.Add(DataCells);

            //ReportDocument report = new ReportDocument();
            report2.Load(Server.MapPath("crystal/PresentAbstract.rpt"));

            report2.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //string SSQL1 = "";
            DataTable dt21 = new DataTable();

            SSQL = "";
            SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
            SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
            SSQL = SSQL + " inner join ";
           
                SSQL = SSQL + " Employee_Mst";
            
            SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";

            SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";

            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            if (ShiftType1 != "ALL")
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
            if (Date != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
            }

            SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
            dt21 = objdata.RptEmployeeMultipleDetails(SSQL);
            report2.DataDefinition.FormulaFields["New_Formula"].Text = "'" + dt2.Rows.Count.ToString() + "'";


            // report2.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            //CrystalReportViewer2.ReportSource = report2;
            string Server_Path = Server.MapPath("~");
            string AttachfileName_Miss = "";
            //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
            AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Improper_Punch_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

            if (File.Exists(AttachfileName_Miss))
            {
                File.Delete(AttachfileName_Miss);
            }

            report2.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
            report2.Close();
        }
        else
        {
            //CrystalReportViewer2.Dispose();
        }
    }


    private void GetAttdDayWise_Change()
    {
        string TableName = "";

       // Date = (Convert.ToDateTime(DateTime.Now.AddDays(-1)).ToString("dd/MM/yyyy")).ToString();

        TableName = "Employee_Mst";
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join " + TableName + " EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";

        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";

        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();

                DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();

                DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["ShiftDate"] = Date;
                DataCell.Rows[i]["CompanyName"] = name.ToString();
                DataCell.Rows[i]["LocationName"] = SessionLcode;

                sno += 1;
            }

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Attendance.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;
            //Mail.Attachments.Add(new Attachment(CrystalReportViewer1.PrintMode, "Day Attendance Day Wise"));
            string Server_Path = Server.MapPath("~");
            string AttachfileName_Miss = "";
          //  string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
            AttachfileName_Miss = Server_Path + "/Daily_Report/Daily_Attendance_For_" + Convert.ToDateTime(Date).ToString("dd_MM_yyyy_HHmmss") + ".pdf";

            if (File.Exists(AttachfileName_Miss))
            {
                File.Delete(AttachfileName_Miss);
            }

            report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
            report.Close();
            report.Dispose();
        }
        else
        {
           // CrystalReportViewer1.Dispose();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
}