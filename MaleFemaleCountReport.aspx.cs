﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class MaleFemaleCountReport : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType; 


    BALDataAccess objdata = new BALDataAccess();
    //string SessionAdmin = "admin";
    string cc = "EVEREADY SPINING MILL";
    //string SessionCcode = "ESM";
    //string SessionLcode = "UNIT I";
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;
    string SSQL = "";

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    DataTable Abstract_table = new DataTable();
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    int OT_Hour;
    int OT_Min;
    int OT_Time;
    string OT_Hour1 = "";
    string mUser = "";
    DataTable AutoDTable = new DataTable();
    string Division = "";
    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
           

            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
                DataSet ds = new DataSet();


                SSQL = "";
                SSQL = "select EM.EmpNo as EmpCode,LD.MachineID,LD.ExistingCode as ExCode,isnull(LD.DeptName,'') As Dept,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
                SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,EM.TypeName as Type from LogTime_Days LD";
                SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
                //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
                //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
                //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
                SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
                SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
                //SSQL = SSQL + " And AM.CompCode='" + SessionCcode.ToString() + "' ANd AM.LocCode='" + SessionLcode.ToString() + "'";
                //SSQL = SSQL + " And MG.CompCode='" + SessionCcode.ToString() + "' ANd MG.LocCode='" + SessionLcode.ToString() + "'";

                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }
                if (ShiftType1 != "ALL")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (Date != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
                }

                SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
                // 
                AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

               

               
                GetMaleorFemale_New();
                ds.Tables.Add(Abstract_table);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Male or Female.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

            

            }
        }
    }
    public void NonAdminGetAttdDayWise_Change()
    {
        DataSet ds = new DataSet();
        DataTable AutoDTable = new DataTable();
        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("OTHour");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Desgination");



        //if (mReportName.Text == "DAY ATTENDANCE - DAY WISE :: BELOW 4 HOURS")
        //{
        DataTable dtIPaddress = new DataTable();
        dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }



        if (ModeType == "IN/OUT")
        {
            if (ShiftType1 != "ALL")
            {
                LocalDT = objdata.ShiftMSt(SessionCcode.ToString(), SessionLcode.ToString(), ShiftType1, ddlShiftType);
            }
            else
            {
                LocalDT = objdata.ShiftMStForAll(SessionCcode.ToString(), SessionLcode.ToString(), ddlShiftType);
            }
        }

        string ShiftType;

        if (LocalDT.Rows.Count > 0)
        {
            string ng = string.Format(Date, "dd-MM-yyyy");
            DateTime date1 = Convert.ToDateTime(ng);
            DateTime date2 = date1.AddDays(1);
            string Start_IN = "";
            string End_In;

            string End_In_Days = "";
            string Start_In_Days = "";


            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
            {
                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }

                if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }


                string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);
                string ss = "";

                if (ShiftType == "GENERAL")
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + "And LT.TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "And LT.TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "AND em.ShiftType='" + ddlShiftType + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsNonAdmin='1' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";
                }


                else if (ShiftType == "SHIFT")
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.IsNonAdmin='1' And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "'";
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                }
                else
                {
                    ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.IsNonAdmin='1' And EM.IsActive='yes' And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                    ss = ss + " AND EM.ShiftType='" + ddlShiftType + "' ";
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeIN)";
                }


                mdatatable = objdata.ReturnMultipleValue(ss);

                if (mdatatable.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mdatatable.Rows.Count - 1; iRow++)
                    {
                        Boolean chkduplicate = false;
                        chkduplicate = false;

                        for (int ia = 0; ia < AutoDTable.Rows.Count - 1; ia++)
                        {
                            string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();


                            // AutoDTable.Rows.Add();

                            // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                            AutoDTable.Rows[mStartINRow][3] = LocalDT.Rows[iTabRow]["ShiftDesc"].ToString();
                            if (ShiftType == "SHIFT")
                            {
                                string str = mdatatable.Rows[iRow][1].ToString();
                                // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeIN"]);
                            }


                            MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();
                            AutoDTable.Rows[mStartINRow][9] = MachineID.ToString();
                            mStartINRow += 1;
                        }
                    }
                }

                DataTable nnDatatable = new DataTable();

                string SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT  Where Compcode='" + SessionCcode.ToString() + "'";
                SSQL = SSQL + " and LocCode = ' " + SessionLcode.ToString() + "'";
                if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT")
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                }
                else
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                }
                SSQL = SSQL + " Group By MachineID";
                SSQL = SSQL + " Order By Max(TimeOUT)";

                mdatatable = objdata.ReturnMultipleValue(SSQL);


                string InMachine_IP;
                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string mach = AutoDTable.Rows[iRow2][9].ToString();

                    InMachine_IP = mach.ToString();

                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "SHIFT1")
                    {
                        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                    }
                    else
                    {
                        SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                        SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";
                    }
                    mLocalDS_out = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS_out.Rows.Count <= 0)
                    {

                    }
                    else
                    {
                        if (AutoDTable.Rows[iRow2][9].ToString() == mLocalDS_out.Rows[0][0].ToString())
                        {
                            AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                        }

                    }
                    // Above coddings are correct:

                    Time_IN_Str = "";
                    Time_OUT_Str = "";
                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");

                    if (SessionLcode == "UNIT I" && mLocalDS_out.Rows[iTabRow]["ShiftDesc"].ToString() == "SHIFT10")
                    {
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(-1).ToString("yyyy/MM/dd") + " " + "23:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:59' Order by TimeIN ASC";
                    }
                    else
                    {
                        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                    }

                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                    if (AutoDTable.Rows[iRow2][3] == "SHIFT1" || AutoDTable.Rows[iRow2][3] == "SHIFT2")
                    {


                        InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);

                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        string InTime_Check_str = String.Format("HH:mm:ss", InTime_Check);
                        string InToTime_Check_str = String.Format("HH:mm:ss", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                        SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";

                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' ";
                            SSQL = SSQL + " And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            DS_InTime = objdata.ReturnMultipleValue(SSQL);

                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;



                                for (int k = 0; k <= Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString() + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString() + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                    }
                                }

                                if (Shift_Check_blb == true)
                                {
                                    AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                    AutoDTable.Rows[iRow2][7] = String.Format("HH:mm:ss", Final_InTime);

                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                    mLocalDS_INTAB = objdata.ReturnMultipleValue(SSQL);

                                    if (Final_Shift == "SHIFT2")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                    }
                                    DS_Time = objdata.ReturnMultipleValue(SSQL);
                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);
                                    }
                                }

                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";
                                    DS_Time = objdata.ReturnMultipleValue(SSQL);

                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);
                                    }

                                }
                            }
                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeOUT Asc";
                                DS_Time = objdata.ReturnMultipleValue(SSQL);

                                if (DS_Time.Rows.Count != 0)
                                {
                                    AutoDTable.Rows[iRow2][8] = String.Format("HH:mm:ss", DS_Time.Rows[0][0]);

                                }
                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                        }
                    }


                    else
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    }



                    mLocalDS_OUTTAB = objdata.ReturnMultipleValue(SSQL);
                    String Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_OUT_Str = "";
                            }

                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (LocalDT.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }

                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }

                        }
                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_OUT_Str = "";
                            }
                            else
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }


                        }

                    }
                    // Bellow codings are correct:

                }
                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string MID = AutoDTable.Rows[iRow2][9].ToString();

                    SSQL = "";
                    SSQL = "select Distinct isnull(EM.MachineID_Encrypt,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
                    SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
                    SSQL = SSQL + ",DM.DeptCode from Employee_Mst EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.MachineID_Encrypt='" + MID + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    SSQL = SSQL + " And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsNonAdmin='1' And EM.IsActive='Yes'" + " order by DM.DeptCode Asc";


                    mEmployeeDS = objdata.ReturnMultipleValue(SSQL);

                    if (mEmployeeDS.Rows.Count > 0)
                    {
                        for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                        {


                            string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                            //if (AutoDTable.Rows[iRow2][9] == mEmployeeDS.Rows[iRow1]["MachineID"])
                            //{
                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                            AutoDTable.Rows[iRow2][16] = cc.ToString();
                            AutoDTable.Rows[iRow2][17] = SessionLcode.ToString();
                            AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];

                            //}
                        }
                    }
                }

            }
        }
        //  }
        GetMaleorFemale_New();

        ds.Tables.Add(Abstract_table);
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/Male or Female"));
        
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        CrystalReportViewer1.ReportSource = report;


    }




    public void GetMaleorFemale_New()
    {


        DataTable table = new DataTable();
        Int32 Get_Actual_Present = 0;
        Int32 Time_In_Present = 0;
        string Wages_Type_Gender_Join = "";
        string Adolescent_Check = "0";
        string Get_Time_IN = "";
        string Get_Time_Out = "";
        string Get_Total_Hours = "";
        string Get_ShiftName = "";




        int j = 0;
        table.Columns.Add("CompanyName", typeof(string));
        table.Columns.Add("LocationName", typeof(string));
        table.Columns.Add("ShiftDate", typeof(DateTime));
        table.Columns.Add("SNo", typeof(int));
        table.Columns.Add("Dept", typeof(string));
        table.Columns.Add("Type", typeof(string));
        table.Columns.Add("Shift", typeof(string));
        table.Columns.Add("Category", typeof(string));
        table.Columns.Add("SubCategory", typeof(string));
        table.Columns.Add("EmpCode", typeof(string));
        table.Columns.Add("ExCode", typeof(string));
        table.Columns.Add("Name", typeof(string));
        table.Columns.Add("TimeIN", typeof(string));
        table.Columns.Add("TimeOUT", typeof(string));
        table.Columns.Add("MachineID", typeof(string));
        table.Columns.Add("PrepBy", typeof(string));
        table.Columns.Add("PrepDate", typeof(DateTime));
        table.Columns.Add("TotalMIN", typeof(string));
        table.Columns.Add("GrandTOT", typeof(string));
        //table.Columns.Add("DayWages", GetType(String))
        //table.Columns.Add("Wages_Type", GetType(String))
        DataTable mDelete = new DataTable();
        string SSQLL = "";
        SSQLL = "delete from Day_Attn_Record_Det";
        mDelete = objdata.RptEmployeeMultipleDetails(SSQLL);

        DataRow dtRow = null;
        DataRow Dpt_Abstract_dtRow = null;

        for (int iRow = 0; iRow < AutoDTable.Rows.Count; iRow++)
        {


            table.NewRow();
            table.Rows.Add();
            table.Rows[j]["CompanyName"] = SessionCcode;
            table.Rows[j]["LocationName"] = SessionLcode;
            table.Rows[j]["ShiftDate"] = Date;
            //dtRow("SNo") = Conversion.Val(fg.get_TextMatrix(iRow, 0));
            table.Rows[j]["Dept"] = AutoDTable.Rows[iRow]["Dept"].ToString();
            table.Rows[j]["Type"] = AutoDTable.Rows[iRow]["Type"].ToString();

            table.Rows[j]["Shift"] = AutoDTable.Rows[iRow]["Shift"].ToString();
            Get_ShiftName = AutoDTable.Rows[iRow]["Shift"].ToString();

            table.Rows[j]["Category"] = AutoDTable.Rows[iRow]["CatName"].ToString();
            table.Rows[j]["SubCategory"] = AutoDTable.Rows[iRow]["SubCatName"].ToString();
            table.Rows[j]["EmpCode"] = AutoDTable.Rows[iRow]["EmpCode"].ToString();
            table.Rows[j]["ExCode"] = AutoDTable.Rows[iRow]["ExCode"].ToString();
            table.Rows[j]["Name"] = AutoDTable.Rows[iRow]["FirstName"].ToString();
            if (!string.IsNullOrEmpty(AutoDTable.Rows[iRow]["TimeIN"].ToString()))
            {
                table.Rows[j]["TimeIN"] = AutoDTable.Rows[iRow]["TimeIN"].ToString();
                Get_Time_IN = AutoDTable.Rows[iRow]["TimeIN"].ToString();
            }
            else
            {
                table.Rows[j]["TimeIN"] = AutoDTable.Rows[iRow]["TimeIN"].ToString();
                Get_Time_IN = AutoDTable.Rows[iRow]["TimeIN"].ToString();
            }
            if (!string.IsNullOrEmpty(AutoDTable.Rows[iRow]["TimeOUT"].ToString()))
            {
                table.Rows[j]["TimeOUT"] = AutoDTable.Rows[iRow]["TimeOUT"].ToString();
                Get_Time_Out = AutoDTable.Rows[iRow]["TimeOUT"].ToString();
            }
            else
            {
                table.Rows[j]["TimeOUT"] = AutoDTable.Rows[iRow]["TimeOUT"].ToString();
                Get_Time_Out = AutoDTable.Rows[iRow]["TimeOUT"].ToString();
            }

            table.Rows[j]["MachineID"] = AutoDTable.Rows[iRow]["MachineID"].ToString();
            table.Rows[j]["PrepBy"] = "User";
            table.Rows[j]["PrepDate"] = Date;

            table.Rows[j]["TotalMIN"] = AutoDTable.Rows[iRow]["Total_Hrs"].ToString();

            if ((string.IsNullOrEmpty(AutoDTable.Rows[iRow]["TimeIN"].ToString())) | (AutoDTable.Rows[iRow]["Total_Hrs"].ToString() == ""))
            {
                table.Rows[j]["GrandTOT"] = "00:00";
                Get_Total_Hours = "00:00";
                Get_Actual_Present = 0;
            }
            else
            {
                if (AutoDTable.Rows[iRow]["Total_Hrs"].ToString() != "")
                {
                    table.Rows[j]["GrandTOT"] = AutoDTable.Rows[iRow]["Total_Hrs"].ToString();
                    Get_Total_Hours = AutoDTable.Rows[iRow]["Total_Hrs"].ToString();
                    //Time Spilit
                    string[] Time_Split_Str = null;
                    Int32 Time_4Hrs_Check = 0;
                    string Time_Split = AutoDTable.Rows[iRow]["Total_Hrs"].ToString();
                    Time_Split_Str = Time_Split.Split(':');
                    Time_4Hrs_Check = Convert.ToInt16(Time_Split_Str[0]);
                    if (Time_4Hrs_Check >= 4)
                    {
                        Get_Actual_Present = 1;
                    }
                    else
                    {
                        Get_Actual_Present = 0;
                    }
                }
                else
                {
                    table.Rows[j]["GrandTOT"] = AutoDTable.Rows[iRow]["Total_Hrs"].ToString();
                    Get_Total_Hours = AutoDTable.Rows[iRow]["Total_Hrs"].ToString();
                    Get_Actual_Present = 1;
                }
            }


            //Insert Table Reocrd
            DataTable Emp_DS = new DataTable();
            Time_In_Present = 1;
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID = '" + AutoDTable.Rows[iRow]["MachineID"].ToString() + "'";
            Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Emp_DS.Rows.Count != 0)
            {
                if ((Emp_DS.Rows[0]["Wages"].ToString().ToUpper() == "REGULAR") | (Emp_DS.Rows[0]["Wages"].ToString().ToUpper() == "HOSTEL") | (Emp_DS.Rows[0]["Wages"].ToString().ToUpper() == "STAFF"))
                {
                    Wages_Type_Gender_Join = Emp_DS.Rows[0]["Wages"].ToString();
                    if (Emp_DS.Rows[0]["Gender"].ToString() == "Female")
                    {
                        Wages_Type_Gender_Join = Wages_Type_Gender_Join + " - " + Emp_DS.Rows[0]["Gender"].ToString();
                    }
                    else
                    {
                        Wages_Type_Gender_Join = Wages_Type_Gender_Join + " - " + "Male";
                    }
                }
                else
                {
                    Wages_Type_Gender_Join = Emp_DS.Rows[0]["Wages"].ToString();
                }

                //Insert Record

                DataTable mInsertRecord = new DataTable();

                SSQL = "Insert Into Day_Attn_Record_Det(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,Department,Wages_Type,";
                SSQL = SSQL + "Wages_Gender_Join,Gender,Actual_Present,TimeINPresent,TimeIN,TimeOut,TotalHrs,Shift) Values('" + SessionCcode + "'";
                SSQL = SSQL + ",'" + SessionLcode + "','" + Emp_DS.Rows[0]["MachineID"].ToString() + "','" + Emp_DS.Rows[0]["ExistingCode"].ToString() + "','" + Emp_DS.Rows[0]["FirstName"] + "'";
                SSQL = SSQL + ",'" + Emp_DS.Rows[0]["LastName"].ToString() + "','" + Emp_DS.Rows[0]["DeptName"].ToString() + "','" + Emp_DS.Rows[0]["Wages"].ToString() + "','" + Wages_Type_Gender_Join + "'";
                SSQL = SSQL + ",'" + Emp_DS.Rows[0]["Gender"].ToString() + "','" + Get_Actual_Present + "','" + Time_In_Present + "'";
                SSQL = SSQL + ",'" + Get_Time_IN + "','" + Get_Time_Out + "','" + Get_Total_Hours + "','" + Get_ShiftName + "')";
                mInsertRecord = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            j = j + 1;

        }

        DataTable mDataSet = new DataTable();

        Abstract_table.Columns.Add("CompanyName", typeof(string));
        Abstract_table.Columns.Add("LocationName", typeof(string));
        Abstract_table.Columns.Add("ShiftDate", typeof(string));
        Abstract_table.Columns.Add("Dept", typeof(string));
        Abstract_table.Columns.Add("General_Male", typeof(decimal));
        Abstract_table.Columns.Add("Shift1_Male", typeof(decimal));
        Abstract_table.Columns.Add("Shift2_Male", typeof(decimal));
        Abstract_table.Columns.Add("Shift3_Male", typeof(decimal));
        //Abstract_table.Columns.Add("Shift4_Male", typeof(decimal));
        //Abstract_table.Columns.Add("Shift5_Male", typeof(decimal));
        Abstract_table.Columns.Add("General_FeMale", typeof(decimal));
        Abstract_table.Columns.Add("Shift1_FeMale", typeof(decimal));
        Abstract_table.Columns.Add("Shift2_FeMale", typeof(decimal));
        Abstract_table.Columns.Add("Shift3_FeMale", typeof(decimal));
        //Abstract_table.Columns.Add("Shift4_FeMale", typeof(decimal));
        //Abstract_table.Columns.Add("Shift5_FeMale", typeof(decimal));

        SSQL = "select Department,isnull([GENERAL-Male],0) as GEN_Male,isnull([SHIFT1-Male],0)";
        SSQL = SSQL + " as SH1_Male,isnull([SHIFT2-Male],0) as SH2_Male,isnull([SHIFT3-Male],0) as SH3_Male";
        //SSQL = SSQL + ",isnull([SHIFT4-Male],0) as SH4_Male,isnull([SHIFT5-Male],0) as SH5_Male";

        SSQL = SSQL + " ,isnull([GENERAL-Female],0) as GEN_Female,isnull([SHIFT1-Female],0) as SH1_Female,";
        SSQL = SSQL + " isnull([SHIFT2-Female],0) as SH2_Female,isnull([SHIFT3-Female],0) as SH3_Female ";
        //SSQL = SSQL + "  ,isnull([SHIFT4-Female],0) as SH4_Female,isnull([SHIFT5-Female],0) as SH5_Female ";
        SSQL = SSQL + " from (select Department ,(Shift + '-' + Gender) as Shift_Join, sum(Actual_Present) as present";
        SSQL = SSQL + " from Day_Attn_Record_Det  group by Department, Shift,Gender ) as p ";
        SSQL = SSQL + " pivot (max(present) for Shift_Join in ( [GENERAL-Male],[SHIFT1-Male],[SHIFT2-Male],[SHIFT3-Male],";
        SSQL = SSQL + " [GENERAL-Female],[SHIFT1-Female],";
        SSQL = SSQL + " [SHIFT2-Female],[SHIFT3-Female] )) as pvtt";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        int i = 0;
        if (mDataSet.Rows.Count != 0)
        {
            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
                Abstract_table.NewRow();
                Abstract_table.Rows.Add();
                Abstract_table.Rows[i]["CompanyName"] = SessionCcode;
                Abstract_table.Rows[i]["LocationName"] = SessionLcode;
                Abstract_table.Rows[i]["ShiftDate"] = Date;
                Abstract_table.Rows[i]["Dept"] = mDataSet.Rows[iRow]["Department"].ToString();
                Abstract_table.Rows[i]["General_Male"] = mDataSet.Rows[iRow]["GEN_Male"];
                Abstract_table.Rows[i]["Shift1_Male"] = mDataSet.Rows[iRow]["SH1_Male"];
                Abstract_table.Rows[i]["Shift2_Male"] = mDataSet.Rows[iRow]["SH2_Male"];
                Abstract_table.Rows[i]["Shift3_Male"] = mDataSet.Rows[iRow]["SH3_Male"];
                //Abstract_table.Rows[i]["Shift4_Male"] = mDataSet.Rows[iRow]["SH4_Male"];
                //Abstract_table.Rows[i]["Shift5_Male"] = mDataSet.Rows[iRow]["SH5_Male"];
                Abstract_table.Rows[i]["General_FeMale"] = mDataSet.Rows[iRow]["GEN_Female"];
                Abstract_table.Rows[i]["Shift1_FeMale"] = mDataSet.Rows[iRow]["SH1_Female"];
                Abstract_table.Rows[i]["Shift2_FeMale"] = mDataSet.Rows[iRow]["SH2_Female"];
                Abstract_table.Rows[i]["Shift3_FeMale"] = mDataSet.Rows[iRow]["SH3_Female"];
                //Abstract_table.Rows[i]["Shift4_FeMale"] = mDataSet.Rows[iRow]["SH4_Female"];
                //Abstract_table.Rows[i]["Shift5_FeMale"] = mDataSet.Rows[iRow]["SH5_Female"];

                i = i + 1;
            }
        }


    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
