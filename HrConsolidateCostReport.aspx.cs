﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class HrConsolidateCostReport : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string unit = "";
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL = "";
    System.Web.UI.WebControls.DataGrid grid = new System.Web.UI.WebControls.DataGrid();
    BALDataAccess objdata = new BALDataAccess();
    DataTable DT = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Scheduled Leave Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Date = Request.QueryString["Date"].ToString();
            unit = Request.QueryString["unit"].ToString();

            SSQL = " Select PV.DeptName,sum(PV.TotalEmp)[TotAvail] ,isnull(sum(PV.Pre),0)[Precent],Isnull(Sum(PV.Ab),0)[Abcent], ";
            SSQL = SSQL + " isnull(sum(PV.Pre),0)+Isnull(Sum(PV.Ab),0)[TotalEmp],isnull(Sum(PV.NONE),0)[Canteen1],";
            SSQL = SSQL + " isnull(Sum(PV.SANTHOSH),0)[Canteen2],isnull(Sum(VELUMANI),0)[Canteen3],";
            SSQL = SSQL + " isnull(Sum(PV.NONE),0)+isnull(Sum(PV.SANTHOSH),0)+isnull(Sum(VELUMANI),0)[TotalEmp], ";
            SSQL = SSQL + " isnull(sum(PV.Pre),0)*isnull(PV.Amt,0)[TotalCast] From ( ";

            SSQL = SSQL + " Select EMP.DeptName,Count(EMP.ExistingCode)[TotalEmp], ";
            SSQL = SSQL + " Case when cast(LTD.Total_Hrs as decimal(18,2))=CAST('0.00' as decimal(18,2)) then Count(LTD.ExistingCode) else 0 end [Pre], ";
            SSQL = SSQL + " Case when cast(LTD.Total_Hrs as decimal(18,2))=CAST('0.00' as decimal(18,2)) then Count(LTD.ExistingCode) else 0 end [Pre1], ";
            SSQL = SSQL + " Case when cast(LTD.Total_Hrs as decimal(18,2))!=CAST('0.00' as decimal(18,2)) then Count(LTD.ExistingCode) else 0 end [Ab], ";
            SSQL = SSQL + " CTN.CanteenName[CanteenName],CTN.Amount[Amt] from Employee_Mst EMP ";
            SSQL = SSQL + " Inner Join LogTime_Days LTD on LTD.ExistingCode=EMP.ExistingCode and LTD.LocCode=EMP.LocCode  ";
            SSQL = SSQL + " Inner Join MstCanteen CTN on CTN.CanID=EMP.CanteenName ";

            SSQL = SSQL + " Where convert(DateTime,LTD.Attn_Date ,105)=convert(datetime,'" + Date + "',105) ";
            if (unit != "ALL")
            {
                SSQL = SSQL + " and EMP.LocCode='" + unit + "'";
            }
            SSQL = SSQL + " Group by EMP.DeptName,LTD.Total_Hrs,CTN.CanteenName,CTN.Amount) A  ";
            SSQL = SSQL + " Pivot (sum(A.Pre1) for A.CanteenName in (NONE,SANTHOSH,VELUMANI)) as PV Group by PV.DeptName,PV.Amt ";


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                DataTable dt = new DataTable();
                string Compname = "";
                string daycolspan = "0";

                string HR_ACT_NO = "";
                string EPF_ACT_NO = "";
                string CT_ACT_NO = "";
                string TS_ACT_NO = "";
                string TA_ACT_NO = "";
                string LC_ACT_NO = "";


                string HR_ACT_COST = "";
                string EPF_ACT_COST = "";
                string CT_ACT_COST = "";
                string TS_ACT_COST = "";
                string TA_ACT_COST = "";
                string LC_ACT_COST = "";

                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count != 0)
                {
                    Compname = dt.Rows[0]["CompName"].ToString();
                }
                //grid.DataSource = DT;
                //grid.DataBind();
                string attachment = "attachment;filename=HR CONSOLIDATE COST REPORT.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);

                Response.Write("<table border=1px>");

                Response.Write("<tr Font-Bold='true' border=1px align='center'>");
                Response.Write("<td border=1px colspan='" + Convert.ToInt32(daycolspan + 7) + "'>");
                Response.Write("<a style=\"font-weight:bold\">" + Compname + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + unit + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 7) + "'>");
                Response.Write("<a style=\"font-weight:bold\">HR CONSOLIDATE COST REPORT</a>");
                Response.Write("  ");

                Response.Write("</td>");
                Response.Write("</tr>");


                Response.Write("<tr align='center' >");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">HR COST</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr align='center'>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">OPTIMUM</a>");
                Response.Write("<table border=1px >");
                Response.Write("<tr align='center'>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr align='center' >");
                Response.Write("<td>");

                SSQL = "Select Cast(Count(ExistingCode) as decimal(18,2))  HR_FIX_No from Employee_Mst where ";
                SSQL = SSQL + "  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Response.Write(DT.Rows[0]["HR_FIX_No"].ToString());

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select cast(Sum(A.ST_CAST)+sum(A.LB_CAST) as decimal(18,2))HR_FIX_CAST from ";
                SSQL = SSQL + " (Select BasicSalary/26 ST_CAST,0 LB_CAST from  ";
                SSQL = SSQL + " Employee_Mst where (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) and CatName='STAFF' ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + " UNION ALL ";
                SSQL = SSQL + " Select 0 ST_CAST,BasicSalary LB_CAST from  ";
                SSQL = SSQL + " Employee_Mst where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))  and CatName!='STAFF' ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + ")A";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Response.Write(DT.Rows[0]["HR_FIX_CAST"].ToString());

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ACTUAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select Count(ExistingCode) [HR_ACT_No] from LogTime_Days where  ";
                SSQL = SSQL + " Convert (dateTime, Attn_Date,105)=Convert(DateTime,'" + Date + "',105) and Shift !='Leave'";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    HR_ACT_NO = DT.Rows[0]["HR_ACT_No"].ToString();
                    Response.Write(DT.Rows[0]["HR_ACT_No"].ToString());
                }
                else
                {
                    HR_ACT_NO = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select  Sum(A.ST_CAST)+sum(A.LB_CAST)HR_FIX_CAST From  ( ";

                SSQL = SSQL + " Select Cast(Cast(LTD.BasicSalary as Decimal(18,2))/ Cast(26 as Decimal(18,2)) as Decimal(18,2)) [ST_CAST],";
                SSQL = SSQL + " '0.00' [LB_CAST] From LogTime_Days LTD ";
                SSQL = SSQL + " Inner Join Employee_Mst EM On EM.ExistingCode=LTD.ExistingCode and (EM.Eligible_ESI='1' or Eligible_PF='1') ";
                SSQL = SSQL + " Where   LTD.CatName='STAFF' and ";
                SSQL = SSQL + " Convert(datetime,Attn_Date,105)=CONVERT(datetime,'" + Date + "',105) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }

                SSQL = SSQL + " UNION ALL ";

                SSQL = SSQL + " Select '0.00' [ST_CAST], Cast(LTD.BasicSalary as Decimal(18,2)) [LB_CAST] From LogTime_Days LTD ";
                SSQL = SSQL + " inner Join Employee_Mst EM On EM.ExistingCode=LTD.ExistingCode and (EM.Eligible_ESI='1' or Eligible_PF='1') ";
                SSQL = SSQL + " where LTD.CatName!='STAFF' and ";
                SSQL = SSQL + " Convert(datetime,Attn_Date,105)=CONVERT(datetime,'" + Date + "',105)  ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + ")A";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    HR_ACT_COST = DT.Rows[0]["HR_FIX_CAST"].ToString();

                    Response.Write(DT.Rows[0]["HR_FIX_CAST"].ToString());
                }
                else
                {
                    HR_ACT_COST = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">EPF/ESI</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">OPTIMUM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select Cast(count(ExistingCode) as Decimal(18,2)) PF_FIX_No from Employee_Mst where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))  and ";
                SSQL = SSQL + " (Eligible_PF='1' or Eligible_ESI='1')  ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }



                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Response.Write(DT.Rows[0]["PF_FIX_No"].ToString());

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select (Sum(EPF_STF_AMT)+sum(EPF_LAB_AMT))[EPF_FIX_COST] From ( ";
                SSQL = SSQL + " Select Cast(Sum(BaseSalary/26) as Decimal(18,2)) EPF_STF_AMT,0 EPF_LAB_AMT from  ";
                SSQL = SSQL + " Employee_Mst where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))  and Eligible_PF='1' and CatName='Staff'";
                SSQL = SSQL + " UNION ALL ";
                SSQL = SSQL + " Select 0 EPF_STF_AMT, cast(Sum(BaseSalary) as decimal(18,2)) EPF_LAB_AMT from  ";
                SSQL = SSQL + " Employee_Mst where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))  and Eligible_ESI='1' and CatName!='Staff' ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + ")A";


                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Response.Write(DT.Rows[0]["EPF_FIX_COST"].ToString());

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ACTUAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select Cast(count(LTD.ExistingCode) as Decimal(18,2)) EPF_ACT_No from LogTime_Days LTD ";
                SSQL = SSQL + " Inner Join Employee_Mst EM on EM.ExistingCode=LTD.ExistingCode And  EM.LocCode=LTD.LocCode ";
                SSQL = SSQL + " Where (EM.Eligible_PF='1' or EM.Eligible_ESI='1')  and ";
                SSQL = SSQL + " Convert(Datetime,LTD.Attn_Date,105)=convert(datetime,'" + Date + "',105) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    EPF_ACT_NO = DT.Rows[0]["EPF_ACT_No"].ToString();
                    Response.Write(DT.Rows[0]["EPF_ACT_No"].ToString());
                }
                else
                {
                    EPF_ACT_NO = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");


                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select(Sum(EPF_STF_AMT) + sum(EPF_LAB_AMT))[EPF_ACT_COST] From( ";
                SSQL = SSQL + " Select Cast(Cast(LTD.BasicSalary as Decimal(18, 2)) / Cast(26 as Decimal(18, 2)) as Decimal(18, 2)) EPF_STF_AMT,";
                SSQL = SSQL + " 0 EPF_LAB_AMT from LogTime_Days LTD  ";
                SSQL = SSQL + " Inner Join Employee_Mst EM on EM.ExistingCode = LTD.ExistingCode and EM.LocCode = LTD.LocCode ";
                SSQL = SSQL + " where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) and Eligible_PF = '1' and ";
                SSQL = SSQL + " LTD.CatName = 'Staff' and  LTD.Shift != 'Leave' and ";
                SSQL = SSQL + " convert(Datetime, LTD.Attn_Date, 105) = CONVERT(datetime, '" + Date + "', 105) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + " UNION ALL ";
                SSQL = SSQL + " Select 0 EPF_STF_AMT, Cast(LTD.BasicSalary as Decimal(18, 2)) EPF_LAB_AMT from LogTime_Days LTD ";
                SSQL = SSQL + " Inner Join Employee_Mst EM on EM.ExistingCode = LTD.ExistingCode and EM.LocCode = LTD.LocCode ";
                SSQL = SSQL + " where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) and LTD.CatName != 'Staff' ";
                SSQL = SSQL + " and Eligible_ESI = '1' and LTD.Shift != 'Leave' and  ";
                SSQL = SSQL + " Convert(DateTime, LTD.Attn_Date, 105) = CONVERT(datetime, '" + Date + "', 105) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + ")A";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    EPF_ACT_COST = DT.Rows[0]["EPF_ACT_COST"].ToString();
                    Response.Write(DT.Rows[0]["EPF_ACT_COST"].ToString());
                }
                else
                {
                    EPF_ACT_COST = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">CANTEEN</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">OPTIMUM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select Cast(Count(ExistingCode) as Decimal(18,2)) CAN_FIX_NO from Employee_Mst where  EligibleCateen='1' ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + " And (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Response.Write(DT.Rows[0]["CAN_FIX_NO"].ToString());


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select isnull(Count(ExistingCode)*(CAT.Amount),0) [CAN_FIX_COST] from Employee_Mst EMP ";
                SSQL = SSQL + " Inner Join MstCanteen CAT on CAT.CanID=EMP.CanteenName and CAT.LocCode=EMP.LocCode Where ";
                SSQL = SSQL + " EMP.EligibleCateen='1' And (EMP.IsActive='YES' or Convert(date,EMP.DOR,103)>=Convert(date,'" + Date + "',103))  ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and EMP.LocCode='" + unit + "'";
                }
                SSQL = SSQL + " Group by CAT.Amount";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    Response.Write(DT.Rows[0]["CAN_FIX_COST"].ToString());
                }
                else
                {
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ACTUAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");


                SSQL = " Select Cast(Count(LTD.ExistingCode) as Decimal(18,2)) CAN_ACT_NO from LogTime_Days LTD ";
                SSQL = SSQL + " Inner Join  Employee_Mst EM on EM.ExistingCode=LTD.ExistingCode and EM.LocCode=LTD.LocCode ";
                SSQL = SSQL + " Where  EM.EligibleCateen='1' And (EM.IsActive='YES' or Convert(date,EM.DOR,103)>=Convert(date,'" + Date + "',103))  ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + " and  CONVERT(Datetime,LTD.Attn_Date,105)=Convert(datetime,'" + Date + "',105) ";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    CT_ACT_NO = DT.Rows[0]["CAN_ACT_NO"].ToString();
                    Response.Write(DT.Rows[0]["CAN_ACT_NO"].ToString());
                }
                else
                {
                    CT_ACT_NO = "0.00";
                    Response.Write("0.00");
                }


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select isnull(Count(LTD.ExistingCode) *(CAT.Amount),0) [CAN_ACT_COST] from LogTime_Days LTD ";
                SSQL = SSQL + " Inner Join Employee_Mst EMP on EMP.ExistingCode =LTD.ExistingCode and EMP.LocCode=LTD.LocCode ";
                SSQL = SSQL + " Inner Join MstCanteen CAT on CAT.CanID=EMP.CanteenName and CAT.LocCode=EMP.LocCode ";
                SSQL = SSQL + " Where   EMP.EligibleCateen='1' And (EMP.IsActive='YES' or Convert(date,EMP.DOR,103)>=Convert(date,'" + Date + "',103)) and ";
                SSQL = SSQL + " Convert(Datetime, Attn_Date,105)=CONVERT(datetime,'" + Date + "',105)";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + " Group by CAT.Amount";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    CT_ACT_COST = DT.Rows[0]["CAN_ACT_COST"].ToString();
                    Response.Write(DT.Rows[0]["CAN_ACT_COST"].ToString());
                }
                else
                {
                    CT_ACT_COST = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">TEA/SNACKS</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">OPTIMUM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select Cast(Count(ExistingCode) as Decimal(18,2)) [TEA_FIX_NO] from Employee_Mst where (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + " and EligibleSnacks='1' ";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    Response.Write(DT.Rows[0]["TEA_FIX_NO"].ToString());
                }
                else
                {
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select (sum(A.Fix_TS_No)*sum(A.Amt))FIX_TS_COST from ( ";
                SSQL = SSQL + " Select Count(EMP.ExistingCode)[Fix_TS_No],0[Amt] from Employee_Mst EMP ";
                SSQL = SSQL + " Where EMP.EligibleSnacks='1' and  (EMP.IsActive='YES' or Convert(date,EMP.DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + " Union All ";
                SSQL = SSQL + " Select 0[Fix_TS_No],TS.Amount[Amt] from MstTeaSnacks TS )A ";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    Response.Write(DT.Rows[0]["FIX_TS_COST"].ToString());
                }
                else
                {
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ACTUAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select Cast(Count(ExistingCode) as Decimal(18,2)) [TEA_ACT_NO] from Employee_Mst where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LocCode='" + unit + "'";
                }
                SSQL = SSQL + " and EligibleSnacks='1' ";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    TS_ACT_NO = DT.Rows[0]["TEA_ACT_NO"].ToString();
                    Response.Write(DT.Rows[0]["TEA_ACT_NO"].ToString());
                }
                else
                {
                    TS_ACT_NO = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select (sum(A.ACT_TS_No)*sum(A.Amt))ACT_TS_COST from ( ";
                SSQL = SSQL + " Select Count(LTD.ExistingCode)[ACT_TS_No],0[Amt] from LogTime_Days LTD ";
                SSQL = SSQL + " inner Join Employee_Mst EMP on EMP.ExistingCode=LTD.ExistingCode and EMP.LocCode=LTD.LocCode ";
                SSQL = SSQL + " Where EMP.EligibleSnacks='1' and  (EMP.IsActive='YES' or Convert(date,EMP.DOR,103)>=Convert(date,'" + Date + "',103))  and ";
                SSQL = SSQL + " CONVERT(DateTime, LTD.Attn_Date,105)=CONVERT(datetime,'" + Date + "',105) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + " Union All ";

                SSQL = SSQL + " Select 0[ACT_TS_No],TS.Amount[Amt] from MstTeaSnacks TS )A ";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    TS_ACT_COST = DT.Rows[0]["ACT_TS_COST"].ToString();
                    Response.Write(DT.Rows[0]["ACT_TS_COST"].ToString());
                }
                else
                {
                    TS_ACT_COST = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");


                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");



                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">PVM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">OPTIMUM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("0.00");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("0.00");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ACTUAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("0.00");
                TA_ACT_NO = "0.00";
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("0.00");
                TA_ACT_COST = "0.00";
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");


                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">LEADER COMM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">OPTIMUM</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select Cast(Count(ExistingCode) as Decimal(18,2)) [LED_FIX_NO] from Employee_Mst EM  ";
                SSQL = SSQL + " Inner join AgentMst AG on AG.AgentID=Em.AgentID and AG.LocCode=EM.LocCode ";
                SSQL = SSQL + " Where (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and EM.LocCode='" + unit + "'";
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    Response.Write(DT.Rows[0]["LED_FIX_NO"].ToString());
                }
                else
                {
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select(sum(A.LED_FIX_NO) * sum(A.Amt))[LED_FIX_COST] From( ";
                SSQL = SSQL + " Select Cast(Count(ExistingCode) as Decimal(18, 2))[LED_FIX_NO], 0[Amt] From Employee_Mst EM ";
                SSQL = SSQL + " Inner join AgentMst AG on AG.AgentID = Em.AgentID and AG.LocCode = EM.LocCode";
                SSQL = SSQL + " Where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))  ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and EM.LocCode='" + unit + "'";
                }
                //SSQL = SSQL + " and Convert (datetime ,LTD.Attn_Date,105)=CONVERT(datetime,'19/03/2021',105) ";
                SSQL = SSQL + " union All ";
                SSQL = SSQL + " Select 0[LED_FIX_NO], Sum(cast(AG.Shift8 as decimal(18, 2)) + cast(AG.Shift12 as decimal(18, 2)))[Amt] ";
                SSQL = SSQL + " from Employee_Mst EM ";
                SSQL = SSQL + " Inner join AgentMst AG on AG.AgentID = Em.AgentID and AG.LocCode = EM.LocCode";
                SSQL = SSQL + " Where  (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103))";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and EM.LocCode='" + unit + "'";
                }
                SSQL = SSQL + ")A";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    Response.Write(DT.Rows[0]["LED_FIX_COST"].ToString());
                }
                else
                {
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ACTUAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = "Select Cast(Count(ExistingCode) as Decimal(18,2)) [LED_ACT_NO] from Employee_Mst EM  ";
                SSQL = SSQL + " Inner join AgentMst AG on AG.AgentID=Em.AgentID and AG.LocCode=EM.LocCode ";
                SSQL = SSQL + " Where (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and EM.LocCode='" + unit + "'";
                }

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    LC_ACT_NO = DT.Rows[0]["LED_ACT_NO"].ToString();
                    Response.Write(DT.Rows[0]["LED_ACT_NO"].ToString());
                }
                else
                {
                    LC_ACT_NO = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                SSQL = " Select(sum(A.LED_FIX_NO) * sum(A.Amt))[LED_Act_COST] From(  ";
                SSQL = SSQL + " Select Cast(Count(LTD.ExistingCode) as Decimal(18, 2))[LED_FIX_NO], 0[Amt] From LogTime_Days LTD ";
                SSQL = SSQL + " Inner JOIN Employee_Mst EM on EM.ExistingCode=LTD.ExistingCode and EM.LocCode=LTD.LocCode ";
                SSQL = SSQL + " Inner join AgentMst AG on AG.AgentID = Em.AgentID and AG.LocCode = EM.LocCode ";
                SSQL = SSQL + " Where   (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + " Union All ";

                SSQL = SSQL + " Select 0[LED_FIX_NO], Sum(cast(AG.Shift8 as decimal(18, 2)) + cast(AG.Shift12 as decimal(18, 2)))[Amt] ";
                SSQL = SSQL + " From LogTime_Days LTD ";
                SSQL = SSQL + " Inner Join Employee_Mst EM on LTD.ExistingCode = Em.ExistingCode and LTD.LocCode = EM.LocCode ";
                SSQL = SSQL + " Inner join AgentMst AG on AG.AgentID = Em.AgentID and AG.LocCode = EM.LocCode ";
                SSQL = SSQL + "  Where   (IsActive='YES' or Convert(date,DOR,103)>=Convert(date,'" + Date + "',103)) ";
                if (unit != "ALL")
                {
                    SSQL = SSQL + " and LTD.LocCode='" + unit + "'";
                }
                SSQL = SSQL + ")A";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count > 0)
                {
                    LC_ACT_COST = DT.Rows[0]["LED_Act_COST"].ToString();
                    Response.Write(DT.Rows[0]["LED_Act_COST"].ToString());
                }
                else
                {
                    LC_ACT_COST = "0.00";
                    Response.Write("0.00");
                }

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">ONDATE</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                string TotODNo = "";

                TotODNo = (Convert.ToDecimal(HR_ACT_NO) + Convert.ToDecimal(EPF_ACT_NO) + Convert.ToDecimal(CT_ACT_NO) + Convert.ToDecimal(TS_ACT_NO) + Convert.ToDecimal(TA_ACT_NO) + Convert.ToDecimal(LC_ACT_NO)).ToString();

                Response.Write(TotODNo);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                string TotODCost = "";

                TotODCost = (Convert.ToDecimal(HR_ACT_COST) + Convert.ToDecimal(EPF_ACT_COST) + Convert.ToDecimal(CT_ACT_COST) + Convert.ToDecimal(TS_ACT_COST) + Convert.ToDecimal(TA_ACT_COST) + Convert.ToDecimal(LC_ACT_COST)).ToString();

                Response.Write(TotODCost);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">UPDATE</a>");
                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">NO</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                string TotActNo = "";

                TotActNo = (Convert.ToDecimal(HR_ACT_NO) + Convert.ToDecimal(EPF_ACT_NO) + Convert.ToDecimal(CT_ACT_NO) + Convert.ToDecimal(TS_ACT_NO) + Convert.ToDecimal(TA_ACT_NO) + Convert.ToDecimal(LC_ACT_NO)).ToString();

                Response.Write(TotActNo);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">COST</a>");

                Response.Write("<table border=1px>");
                Response.Write("<tr>");
                Response.Write("<td>");

                string TotActCost = "";

                TotActCost = (Convert.ToDecimal(HR_ACT_COST) + Convert.ToDecimal(EPF_ACT_COST) + Convert.ToDecimal(CT_ACT_COST) + Convert.ToDecimal(TS_ACT_COST) + Convert.ToDecimal(TA_ACT_COST) + Convert.ToDecimal(LC_ACT_COST)).ToString();

                Response.Write(TotActCost);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("</td>");

                Response.Write("</tr>");

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
