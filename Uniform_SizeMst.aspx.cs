﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Uniform_SizeMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Uniform Size Details";

        }
        Load_Data_Size();
    }

    private void Load_Data_Size()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Uniform_Size_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Uniform_Size_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SizeName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Uniform_Size_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SizeName='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Size Details Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Size Name Not Found');", true);
        }
        Load_Data_Size();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

        if (txtSize.Text.ToString() == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Size Details.!');", true);
        }
        

        if (!ErrFlag)
        {
            query = "select * from Uniform_Size_mst where SizeName='" + txtSize.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from Uniform_Size_mst where SizeName='" + txtSize.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into Uniform_Size_mst(Ccode,Lcode,SizeName)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtSize.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
        Load_Data_Size();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtSize.Text = "";
        btnSave.Text = "Save";
    }
}
