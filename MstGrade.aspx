﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstGrade.aspx.cs" Inherits="MstGrade" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();

        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                }
            });
        };
    </script>

    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Master</a></li>
                    <li class="active">Grade</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Grade </h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Grade</h4>
                            </div>
                            <div class="panel-body">
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Grade Name</label>
                                            <asp:TextBox runat="server" ID="txtGradeName" class="form-control" Style="text-transform: uppercase">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="txtGradeID" runat="server" />
                                            <asp:RequiredFieldValidator ControlToValidate="txtGradeName" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>

                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Percentage</label>
                                            <asp:TextBox runat="server" ID="txtPercentage" class="form-control" Style="text-transform: uppercase">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtPercentage" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>

                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                OnClick="btnClear_Click" />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->

                                </div>
                                <!-- end row -->

                                <!-- table start -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                            <HeaderTemplate>
                                                <table id="example" class="display table table-responsive table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Unit</th>
                                                            <th>GradeName</th>
                                                            <th>Percentage</th>
                                                            <th>Mode</th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("LocCode")%></td>
                                                    <td><%# Eval("GradeName")%></td>
                                                    <td><%# Eval("Percentage")%></td>

                                                    <td>
                                                        <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                            Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("GradeID")%>'>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                            Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("GradeID")%>'
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Grade details?');">
                                                        </asp:LinkButton>
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <!-- table End -->

                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>

