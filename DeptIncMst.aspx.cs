﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class DeptIncMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Department Incentive";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            
            //Load_OTData();
            Load_WagesType();
            Load_Data();
            Load_Department();
            Load_Designation();
        }
        Load_Data_Incentive();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType where (EmpType='HOSTEL' or EmpType='REGULAR')";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }

    private void Load_Designation()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDesignation.Items.Clear();
        query = "Select *from Designation_Mst where DeptName='" + ddlDepartment.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDesignation.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DesignName"] = "-Select-";
        dr["DesignName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data();
        Load_Data_Incentive();
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Designation();
    }

    public void Load_Data()
    {

        DataTable dt = new DataTable();

        SSQL = "Select *from Department_Inc_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count != 0)
        {
            rdbType.SelectedValue = dt.Rows[0]["DeptType"].ToString();
            txtShiftInc_MinDays.Text = dt.Rows[0]["Min_Days"].ToString();
            txtFoodAllowance.Text = dt.Rows[0]["Food_Allowance"].ToString();
            //txtIncAmt.Text = dt.Rows[0]["IncAmt"].ToString();
        }
        else
        {
            rdbType.SelectedValue = "2";
            txtShiftInc_MinDays.Text = "0";
            txtFoodAllowance.Text = "0";
            //txtIncAmt.Text = "0";
        }
    }

    public void Load_OTData()
    {
        DataTable dt = new DataTable();

        SSQL = "Select *from OT_Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count != 0)
        {
            txtOTIncAmt.Text = dt.Rows[0]["OTIncAmt"].ToString();
        }
        else
        {
            txtOTIncAmt.Text = "0";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SSQL = "Delete from Department_Inc_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        if (txtShiftInc_MinDays.Text.ToString() == "") { txtShiftInc_MinDays.Text = "0"; }

        SSQL = "Insert into Department_Inc_Mst(Ccode,Lcode,DeptType,Wages,Min_Days,Food_Allowance)values('" + SessionCcode + "','" + SessionLcode + "','" + rdbType.SelectedValue + "','" + ddlWages.SelectedItem.Text + "','" + txtShiftInc_MinDays.Text + "','" + txtFoodAllowance.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully.!');", true);
        Load_Data();

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWages.SelectedValue = "-Select-";
        rdbType.SelectedValue = "2";
        //txtIncAmt.Text = "0";
        Load_Data();
    }

    protected void btnOTSave_Click(object sender, EventArgs e)
    {
        SSQL = "Delete from Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + ddlDepartment.SelectedValue + "' And Designation='" + ddlDesignation.SelectedValue + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Insert into Incentive_Mst(Ccode,Lcode,DeptCode,DeptName,Designation,IncAmt,OTIncAmt,Wages)values('" + SessionCcode + "','" + SessionLcode + "',";
        SSQL = SSQL + "'" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "','" + ddlDesignation.SelectedItem.Text + "','" + txtIncAmt.Text + "',";
        SSQL = SSQL + "'" + txtOTIncAmt.Text + "','" + ddlWages.SelectedItem.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully.!');", true);
        //Load_OTData();
        btnOTClear_Click(sender, e);
    }

    protected void btnOTClear_Click(object sender, EventArgs e)
    {
        ddlWages.SelectedValue = "-Select-";
        txtOTIncAmt.Text = "0";
        txtIncAmt.Text = "0";
        txtFoodAllowance.Text = "0";
        ddlDepartment.SelectedValue = "0";
        ddlDesignation.SelectedValue = "-Select-";
        //Load_OTData();
        btnOTSave.Text = "Save";
        Load_Data_Incentive();
    }

    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "' And Designation='" + e.CommandArgument.ToString() + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            ddlDepartment.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            ddlDepartment_SelectedIndexChanged(sender, e);
            ddlDesignation.SelectedValue = DT.Rows[0]["Designation"].ToString();
            txtIncAmt.Text = DT.Rows[0]["IncAmt"].ToString();
            txtOTIncAmt.Text = DT.Rows[0]["OTIncAmt"].ToString();
            btnOTSave.Text = "Update";
        }
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "' And Designation='" + e.CommandArgument.ToString() + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "' And Designation='" + e.CommandArgument.ToString() + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }

}
