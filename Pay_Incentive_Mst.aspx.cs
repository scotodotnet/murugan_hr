﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Incentive_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionEpay;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Load_MonthIncentive();
            Load_CivilIncentive();
            Load_Emptype();
        }
        Load_Data_Incentive();
        Load_Mesthiri();
        Load_Fine();
    }

    private void Load_Emptype()
    {
        string SSQL = "";
        SSQL = "Select * from MstEmployeeType";
        ddlEmpType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmpType.DataTextField = "Emptype";
        ddlEmpType.DataValueField = "EmpTypeCd";
        ddlEmpType.DataBind();
        ddlEmpType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And MonthDays='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MonthDays='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtDaysOfMonth.Text.Trim() == "") || (txtDaysOfMonth.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
                ErrFlag = true;
            }
            else if ((txtMinDaysWorked.Text.Trim() == "") || (txtMinDaysWorked.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Minimum Days Worked');", true);
                ErrFlag = true;
            }
            else if ((txtIncent_Amount.Text.Trim() == "") || (txtIncent_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Incentive Amount');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select * from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MonthDays='" + txtDaysOfMonth.Text + "'";
                Query = Query + " and WorkerDays='" + txtDaysWorked.Text + "' and MinDays='" + txtMinDaysWorked.Text + "' and MaxDays='" + txtMaxDaysWorked.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtDaysOfMonth.Text + "'";
                    Query = Query + " and WorkerDays='" + txtDaysWorked.Text + "' and MinDays='" + txtMinDaysWorked.Text + "' and MaxDays='" + txtMaxDaysWorked.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [" + SessionEpay + "]..WorkerIncentive_mst (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays,MinDays,MaxDays,Basic)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtDaysWorked.Text + "','" + txtIncent_Amount.Text + "','" + txtDaysOfMonth.Text + "'";
                Query = Query + ",'" + txtMinDaysWorked.Text + "','" + txtMaxDaysWorked.Text + "','" + txtBasicSalary.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);


                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Data_Incentive();
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
                txtMaxDaysWorked.Text = "0";
                txtDaysWorked.Text = "0";
            }
        }
        catch (Exception Ex)
        {

        }
    }
    protected void btnIncen_Full_Amt_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtIncent_Month_Full_Amount.Text.Trim() == "") || (txtIncent_Month_Full_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                //Query = "Select Amt from ["+SessionEpay+"]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                Query = "Select Amt from [" + SessionEpay + "]..MstMesthiri where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionEpay + "]..MstMesthiri where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                //Query = "Insert Into ["+SessionEpay+"]..HostelIncentive (Ccode,Lcode,Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays)";
                //Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtIncent_Month_Full_Amount.Text + "','" + WorkDays.Checked.ToString() + "','" + Hallowed.Checked.ToString() + "',";
                //Query = Query + "'" + OTdays.Checked.ToString() + "','" + NFHdays.Checked.ToString() + "','" + NFHwork.Checked.ToString() + "','" + CalWorkdays.Checked.ToString() + "',";
                //Query = Query + "'" + CalHallowed.Checked.ToString() + "','" + CalOTdays.Checked.ToString() + "','" + CalNFHdays.Checked.ToString() + "','" + CalNFHwork.Checked.ToString() + "')";
                Query = "Insert Into [" + SessionEpay + "]..MstMesthiri (Ccode,Lcode,Amt)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtIncent_Month_Full_Amount.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                txtIncent_Month_Full_Amount.Text = "0";
                Load_Mesthiri();
            }
        }
        catch (Exception Ex)
        {

        }
    }
    private void Load_Mesthiri()
    {
        Query = "";
        Query = "select * from [" + SessionEpay + "]..MstMesthiri where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtIncent_Month_Full_Amount.Text = dt.Rows[0]["Amt"].ToString();
        }
    }
    public void Load_MonthIncentive()
    {
        DataTable dt = new DataTable();
        Query = "Select Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays from [" + SessionEpay + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtIncent_Month_Full_Amount.Text = dt.Rows[0]["Amt"].ToString();
            if (dt.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper()) { WorkDays.Checked = true; } else { WorkDays.Checked = false; }
            if (dt.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper()) { Hallowed.Checked = true; } else { Hallowed.Checked = false; }
            if (dt.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper()) { OTdays.Checked = true; } else { OTdays.Checked = false; }
            if (dt.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper()) { NFHdays.Checked = true; } else { NFHdays.Checked = false; }
            if (dt.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { NFHwork.Checked = true; } else { NFHwork.Checked = false; }

            if (dt.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper()) { CalWorkdays.Checked = true; } else { CalWorkdays.Checked = false; }
            if (dt.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper()) { CalHallowed.Checked = true; } else { CalHallowed.Checked = false; }
            if (dt.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper()) { CalOTdays.Checked = true; } else { CalOTdays.Checked = false; }
            if (dt.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHdays.Checked = true; } else { CalNFHdays.Checked = false; }
            if (dt.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHwork.Checked = true; } else { CalNFHwork.Checked = false; }
        }
        else
        {
            txtIncent_Month_Full_Amount.Text = "0.00";
            WorkDays.Checked = false; Hallowed.Checked = false; OTdays.Checked = false; NFHdays.Checked = false;
            NFHwork.Checked = false; CalWorkdays.Checked = false; CalHallowed.Checked = false; CalOTdays.Checked = false;
            CalNFHdays.Checked = false; CalNFHwork.Checked = false;
        }
    }
    protected void btnCivil_Incent_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = true;
            DataTable dt = new DataTable();
            if (txtEligible_Days.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
                ErrFlag = true;
            }
            if (txtCivil_Incen_Amt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Query = "";
                Query = Query + "select * from [" + SessionEpay + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = Query + "delete from [" + SessionEpay + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    SaveFlag = false;
                }
                Query = "Insert into [" + SessionEpay + "]..CivilIncentive(Ccode,Lcode,Amt,ElbDays) values(";
                Query = Query + "'" + SessionCcode + "','" + SessionLcode + "','" + txtEligible_Days.Text + "','" + txtCivil_Incen_Amt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
                }
                txtEligible_Days.Text = "0";
                txtCivil_Incen_Amt.Text = "0";
                Load_CivilIncentive();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void Load_CivilIncentive()
    {
        DataTable dt = new DataTable();
        Query = "";
        Query = Query + "select * from [" + SessionEpay + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtEligible_Days.Text = dt.Rows[0]["ElbDays"].ToString();
            txtCivil_Incen_Amt.Text = dt.Rows[0]["Amt"].ToString();
        }
    }
    protected void btnEditEnquiry_Click_Command(object sender, CommandEventArgs e)
    {
        //string maxDays = "";
        //string minDays = "";
        //string monthDay = "";
        //string WorkerDays = "";
        //string[] Split = e.CommandArgument.ToString().Split(',');
        //monthDay = Split[0].ToString();
        //WorkerDays = Split[1].ToString();
        //Split = e.CommandName.ToString().Split(',');
        //maxDays = Split[0].ToString();
        //minDays = Split[1].ToString();
        //string SSQL = "";
        //SSQL = "Select * from ["+SessionEpay+ "]..WorkerIncentive_mst";
        //SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " and WorkerDays='"+WorkerDays+"' and MinDays='"+minDays+ "' and MaxDays='"+maxDays+"'";
        //SSQL = SSQL + " and MonthDays='" + monthDay + "'";
        //DataTable dt_Check = new DataTable();
        //dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt_Check.Rows.Count > 0)
        //{

        //}    
    }

    protected void BtnDeptIncAmt_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstFine where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "Insert into [" + SessionEpay + "]..MstFine values('" + SessionCcode + "','" + SessionLcode + "','" + txtDeptIncAmt.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Fine Amount Saved Successfully....! ');", true);

        Load_Fine();
    }

    private void Load_Fine()
    {
        string SSQL = "";
        SSQL = "Select Amt from [" + SessionEpay + "]..MstFine where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtDeptIncAmt.Text = dt.Rows[0]["Amt"].ToString();
        }
    }

    protected void btnSaveInct_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        if (ddlEmpType.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type');", true);
            return;
        }
        if (txtDays.Text == "0" || txtDays.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Days');", true);
            return;
        }
        if (txtAmt.Text == "0" || txtAmt.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Amount');", true);
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "Delete from [" + SessionEpay + "]..Incentive_Mst where CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "'";
            SSQL = SSQL + " and Employee_type='" + ddlEmpType.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Insert into Incentive_Mst(CompCode,LocCode,Employee_Type,Days,Amount)";
            SSQL = SSQL + "values('" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','" + ddlEmpType.SelectedValue + "','" + txtDays.Text + "'";
            SSQL = SSQL + ",'" + txtAmt.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            btnClrinct_Click(sender, e);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Incentive Saved Successfully');", true);
        }
    }

    protected void btnClrinct_Click(object sender, EventArgs e)
    {
        txtAmt.Text = "0";
        txtDays.Text = "0";
        ddlEmpType.ClearSelection();
    }
}
