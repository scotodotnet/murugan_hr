﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstWorkCategory : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Work Category Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data();
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();

        string SQL = "Select * from MstCategory where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'  ";
        //SQL = SQL + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode.ToString() + "' ";

        dt = objdata.RptEmployeeMultipleDetails(SQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from MstCategory where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and CateID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtWorkCategory.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Please Entre the Category Name)", true);
            return;
        }
        if (!ErrFlag)
        {
            string SSQL = "";
            SSQL = "Delete from MstCategory where CateName='" + txtWorkCategory.Text + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "insert into MstCategory(CateName,CompCode,LocCode)values";
            SSQL = SSQL + "('" + txtWorkCategory.Text + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
            btnClear_Click(sender, e);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWorkCategory.Text = "";
    }
}