﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst24X7LabourAllot : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: 24X7 Labour Allotment Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Agent();
            Load_Shift();
        }
        Load_Data();
    }
    private void Load_Agent()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlAname.Items.Clear();
        query = "select AgentID,AgentName from AgentMst where CompCode='" + SessionCcode + "'";
        query = query + " And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlAname.DataSource = dtdsupp;
        ddlAname.DataTextField = "AgentName";
        ddlAname.DataValueField = "AgentID";
        ddlAname.DataBind();
        ddlAname.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlShift.Items.Clear();
        query = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftDesc"] = "-Select-";
        dr["ShiftDesc"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();

    }
    private void Load_Data()
    {
        DataTable dt = new DataTable();

        string SQL = "Select Auto_ID,AgentName,Shift,cast(count as int) as Count from LabourAllotmentCons_Mst where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'  ";
        dt = objdata.RptEmployeeMultipleDetails(SQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from LabourAllotmentCons_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtCount.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Please Entre the Count)", true);
            return;
        }
        if (!ErrFlag)
        {
            string SSQL = "";
            //SSQL = "Delete from LabourAllotment24X7_Mst where count='" + txtCount.Text + "' and CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "'";
            //objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "insert into LabourAllotmentCons_Mst(Agentid,AgentName,Shift,count,Date,CompCode,LocCode,Userid)values";
            SSQL = SSQL + "('" + ddlAname.SelectedValue + "','" + ddlAname.SelectedItem + "','" + ddlShift.SelectedValue + "','" + txtCount.Text + "',GetDate(),'" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','"+SessionUserID.ToString()+"')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
            Load_Agent();
            btnClear_Click(sender, e);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert(Data Saved Successfully)", true);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtCount.Text = "";
        ddlAname.SelectedItem.Text = "-Select-";
        ddlAname.SelectedValue = "-Select-";
        ddlShift.SelectedValue = "-Select-";
      
    }
}