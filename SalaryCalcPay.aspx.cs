﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class SalaryCalcPay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    DateTime mydate;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    string FromBankACno, FromBankName, FromBranch, ProvisionalTax = "0";
    SqlConnection con;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    string Cash;
    string SSQL;

    SalaryClass objSal = new SalaryClass();
    string CCA = "0";

    string SessionUserType;
    static string EmployeeDays = "0";
    static string Work = "0";
    static string NFh = "0";
    static string lopdays = "0";
    static string WeekOff = "0";
    static string Home = "0";
    static string halfNight = "0";
    static string FullNight = "0";
    static string ThreeSided = "0";
    static string HalfNight_Amt = "0";
    static string FullNight_Amt = "0";
    static string ThreeSided_Amt = "0";
    static string SpinningAmt = "0";
    static string DayIncentive = "0";
    static string Transport_Charges_Regular = "0";
    static string cl = "0";
    static string OTDays = "0";
    static string basic = "0";
    static string All1 = "0";
    static string All2 = "0";
    static string All3 = "0";
    static string Al4 = "0";
    static string All5 = "0";
    static string Ded1 = "0";
    static string ded2 = "0";
    static string ded3 = "0";
    static string ded4 = "0";
    static string ded5 = "0";
    static string lop = "0";
    static string Advance = "0";
    static string pfAmt = "0";
    static string ESI = "0";
    static string stamp = "0";
    static string Union = "0";
    static string TotalEarnings = "0";
    static string TotalDeductions = "0";
    static string NetPay = "0";
    static string RoundOffNetPay = "0";
    static string Words = "";
    static string PFS = "0.00";
    static string ExistNo = "";
    static string PF_salary;
    string TempDate;
    DateTime TransDate;
    DateTime MyDate;
    static int d = 0;
    static decimal AdvAmt;
    static string ID;
    static bool isUK = false;
    string SMonth;
    string Year;
    int dd;
    string MyMonth;
    static decimal val;
    string WagesType;
    static decimal basicsalary = 0;
    static string Basic_For_SM = "0";
    static string OTHoursNew_Str = "0";
    static string OTHoursAmtNew_Str = "0";
    static string query2 = "0";

    static string Fixed_Work_Days = "0";
    static string WH_Work_Days = "0";
    static string NFH_Work_Days_Allow_Days = "0";
    static string DedOthers1 = "0";
    static string DedOthers2 = "0";
    static string MedicalReiburse = "0", PerIncentive = "0", SalaryAdvanceTrans = "0", IncomTax = "0", MessDeduction = "0", HostelDeduction = "0", Totalpay = "0";
    static string EmployeerPFOne = "0.00";
    static string EmployeerPFTwo = "0.00";
    static string EmployeerESI = "0.00";
    string SessionEpay;
    static string Temp_Con_Days = "";
    static string Temp_Con_OT = "";
    static string Temp_Spl_Days = "";
    static string Temp_Spl_OT = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            lblError_Display.Text = "";
            DateTime currentdate = Convert.ToDateTime(DateTime.Now);
            string Tdate = currentdate.ToString("dd/MM/yyyy");
            txtTransDate.Text = Tdate;
            if (SessionAdmin == "2")
            {
                IF_Civil_Inc.Visible = false;
                IF_Token_Checkbox.Visible = false;
                IF_Token_No.Visible = false;
            }

        }
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    public void Load_TwoDates()
    {
        if (ddlMonth.SelectedValue != "-Select-" && ddlFinYear.SelectedValue != "-Select-" && ddlEmployeeType.SelectedValue != "")
        {
            if (ddlEmployeeType.SelectedValue != "2" && ddlEmployeeType.SelectedValue != "5" && ddlEmployeeType.SelectedValue != "10")
            {
                decimal Month_Total_days = 0;
                string Month_Last = "";
                string Year_Last = "0";
                string Temp_Years = "";
                string[] Years;
                string FromDate = "";
                string ToDate = "";

                Temp_Years = ddlFinYear.SelectedItem.Text;
                Years = Temp_Years.Split('-');
                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
                {
                    Month_Total_days = 31;
                }
                else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
                {
                    Month_Total_days = 30;
                }

                else if (ddlMonth.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(Years[0]) + 1);
                    if ((yrs % 4) == 0)
                    {
                        Month_Total_days = 29;
                    }
                    else
                    {
                        Month_Total_days = 28;
                    }
                }
                switch (ddlMonth.SelectedItem.Text)
                {
                    case "January":
                        Month_Last = "01";
                        break;
                    case "February":
                        Month_Last = "02";
                        break;
                    case "March":
                        Month_Last = "03";
                        break;
                    case "April":
                        Month_Last = "04";
                        break;
                    case "May":
                        Month_Last = "05";
                        break;
                    case "June":
                        Month_Last = "06";
                        break;
                    case "July":
                        Month_Last = "07";
                        break;
                    case "August":
                        Month_Last = "08";
                        break;
                    case "September":
                        Month_Last = "09";
                        break;
                    case "October":
                        Month_Last = "10";
                        break;
                    case "November":
                        Month_Last = "11";
                        break;
                    case "December":
                        Month_Last = "12";
                        break;
                    default:
                        break;
                }

                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
                {
                    Year_Last = Years[1];
                }
                else
                {
                    Year_Last = Years[0];
                }
                FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
                ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

                txtFromDate.Text = FromDate.ToString();
                txtToDate.Text = ToDate.ToString();

            }
            else
            {
                txtToDate.Text = "";
                txtFromDate.Text = "";
            }

        }
    }

    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEmployeeType.SelectedValue.ToString().ToUpper() == "CIVIL".ToString().ToUpper())
        {
            //PanelCivilIncenDate.Visible = true;
        }
        else
        {
            PanelCivilIncenDate.Visible = false;
            Load_TwoDates();
        }

    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType,EmpTypeCd from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
    }
    protected void ChkTokenNoSalaryprocess_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkTokenNoSalaryprocess.Checked == true)
        {
            txtTokenNo.Enabled = true;

        }
        else
        {
            txtTokenNo.Enabled = false;
            txtTokenNo.Text = "";
        }
    }

    protected void ChkCivilIncentive_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkCivilIncentive.Checked == true)
        {
            PanelCivilIncenDate.Visible = true;

        }
        else
        {
            PanelCivilIncenDate.Visible = false;
        }
    }

    protected void btnSalCalc_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool SaveFlag = false;
        string BasicPFDays = "26";

        //Basic Spilit
        string BasicAndDA = "0.00";
        string BasicHRA = "0.00";
        string ConvAllow = "0.00";
        string EduAllow = "0.00";
        string MediAllow = "0.00";
        string BasicRAI = "0.00";
        string WashingAllow = "0.00";

        string Basic_Spl_Allowance = "0.00";
        string Basic_Uniform = "0.00";
        string Basic_Vehicle_Maintenance = "0.00";
        string Basic_Communication = "0.00";
        string Basic_Journ_Perid_Paper = "0.00";
        string Basic_Incentives = "0.00";
        string Basic_Incentive_Text = "Incentive";

        string Leaver_Credit_Status = "0";
        string Leave_Credit_Year = "0";
        string Leave_Credit_Days_Month = "0";
        string Leave_Credit_Add_Minus = "0";
        string Leave_Credit_Type = "";


        DataTable Advance_Mst_DT = new DataTable();
        DataTable Advance_Pay_DT = new DataTable();
        DataTable dt_Others = new DataTable();
        string Advance_Amt_Check = "0";
        string Advance_Paid_Count = "0";
        string Employeer_ESI_IF = "0";
        string Employee_ESI_IF = "0";
        string Total_Deduction_IF = "0";
        string RoundOffNetPay_IF = "0";
        string Adolescent_Date_Str = "";
        lblError_Display.Text = "";
        string Emp_Token_No_Search_Sal_Process = "";
        ///btnSalaryCalculate.Enabled = false;
        basicsalary = 0;
        try
        {
            if (ddlEmployeeType.SelectedItem.Text.ToString().ToUpper() == "REGULAR" || ddlEmployeeType.SelectedItem.Text.ToString().ToUpper() == "HOSTEL")
            {
                string a = "1";
                string b = (Convert.ToDecimal(a) + Convert.ToDecimal(12)).ToString();
            }
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFromDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtToDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            //else if (Convert.ToInt32(txtdays.Text) > 31)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}


            decimal Month_Total_days = 0;
            if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonth.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(ddlFinYear.SelectedValue) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }

            TempDate = Convert.ToDateTime(txtFromDate.Text).AddMonths(0).ToShortDateString();
            string MonthName = TempDate;
            DateTime mtn;
            mtn = Convert.ToDateTime(TempDate);
            d = mtn.Month;
            int mons = mtn.Month;
            int yrs1 = mtn.Month;
            #region MonthName
            string DB_Mont = "";
            switch (d)
            {
                case 1:
                    DB_Mont = "January";
                    break;

                case 2:
                    DB_Mont = "February";
                    break;
                case 3:
                    DB_Mont = "March";
                    break;
                case 4:
                    DB_Mont = "April";
                    break;
                case 5:
                    DB_Mont = "May";
                    break;
                case 6:
                    DB_Mont = "June";
                    break;
                case 7:
                    DB_Mont = "July";
                    break;
                case 8:
                    DB_Mont = "August";
                    break;
                case 9:
                    DB_Mont = "September";
                    break;
                case 10:
                    DB_Mont = "October";
                    break;
                case 11:
                    DB_Mont = "November";
                    break;
                case 12:
                    DB_Mont = "December";
                    break;
                default:
                    break;
            }
            #endregion

            if (ChkCivilIncentive.Checked == true)
            {
                if (txtIstWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the First Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (txtLastWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Last Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            //Check Token No Correct or Not
            if (ChkTokenNoSalaryprocess.Checked == true)
            {
                Emp_Token_No_Search_Sal_Process = txtTokenNo.Text;
                DataTable Token_Sch_DT = new DataTable();

                if (SessionUserType == "2")
                {
                    query2 = "Select MachineID from Employee_Mst";
                    query2 = query2 + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query2 = query2 + " And Wages='" + ddlEmployeeType.SelectedItem.Text + "' And ExistingCode='" + Emp_Token_No_Search_Sal_Process + "'";
                    query2 = query2 + " And Wages!='CIVIL' and Eligible_PF='1'";
                }
                else
                {
                    query2 = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query2 = query2 + " And Wages='" + ddlEmployeeType.SelectedItem.Text + "' And ExistingCode='" + Emp_Token_No_Search_Sal_Process + "'";
                }
                Token_Sch_DT = objdata.RptEmployeeMultipleDetails(query2);
                if (Token_Sch_DT.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You have to Enter Token No Proberly...');", true);
                    ErrFlag = true;
                }
            }

            //System.Threading.Thread.Sleep(60000);

            ////Date Checking Process
            //string date_1 = "";
            //date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2017".ToString()).ToString();
            //string dtserver = objdata.ServerDate();
            //if (txtEmployeeType.SelectedValue == "5" || txtEmployeeType.SelectedValue == "3" || txtEmployeeType.SelectedValue == "4")
            //{
            //    //Skip
            //}
            //else
            //{
            //    if (SessionLcode.ToUpper().ToString() == "UNIT I".ToUpper().ToString() && txtEmployeeType.SelectedValue == "3")
            //    {
            //        //Skip
            //    }
            //    else
            //    {
            //        if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
            //        {
            //        }
            //        else
            //        {
            //            System.Threading.Thread.Sleep(60000);
            //            ErrFlag = true;
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
            //        }
            //    }
            //}

            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFromDate.Text);
                DateTime dtto = Convert.ToDateTime(txtToDate.Text);
                DateTime MyDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
                DateTime MyDate2 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
                Leave_Credit_Year = MyDate1.Year.ToString();

                //Get Adolescent_Date
                int month = DateTime.ParseExact(ddlMonth.SelectedItem.Text.ToString(), "MMMM", CultureInfo.CurrentCulture).Month;
                string Year_Get_Adol = "";
                if (ddlMonth.SelectedValue.ToUpper().ToString() == "January".ToUpper().ToString() || ddlMonth.SelectedValue.ToUpper().ToString() == "February".ToUpper().ToString() || ddlMonth.SelectedValue.ToUpper().ToString() == "March".ToUpper().ToString())
                {
                    Year_Get_Adol = (Convert.ToDecimal(ddlFinYear.SelectedValue.ToString()) + Convert.ToDecimal(1)).ToString();
                }
                else
                {
                    Year_Get_Adol = ddlFinYear.SelectedValue.ToString();
                }
                string Last_Day = DateTime.DaysInMonth(Convert.ToInt32(Year_Get_Adol), month).ToString();
                string Month_Str = "1";
                if (month.ToString().Length == 1) { Month_Str = "0" + month.ToString(); }
                else { Month_Str = month.ToString(); }
                Adolescent_Date_Str = Last_Day + "/" + Month_Str + "/" + Year_Get_Adol;


                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtFromDate.Text = null;
                    txtToDate.Text = null;
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    DataTable dt = new DataTable();
                    string Staff_Labour_Cat = "";
                    if (ddlCategory.SelectedValue == "1")
                    {
                        Staff_Labour_Cat = "S";
                    }
                    else
                    {
                        Staff_Labour_Cat = "L";
                    }
                    //Get Employee Details
                    query2 = "Select MstDpt.DeptName,AD.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,'0' as All3,'0' as All4,";
                    query2 = query2 + " '0' as All5,'0' as Ded3,'0' as Ded4,'0' as Ded5,'0' as Advance,'0' as DedOthers1,'0' as DedOthers2,EmpDet.MachineID,'0' as LOPDays";
                    query2 = query2 + " from Employee_Mst EmpDet inner Join [" + SessionEpay + "]..AttenanceDetails AD on EmpDet.EmpNo=AD.EmpNo";
                    query2 = query2 + " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode where";
                    query2 = query2 + " AD.Months='" + ddlMonth.SelectedValue + "' AND";
                    query2 = query2 + " AD.FinancialYear='" + ddlFinYear.SelectedValue + "' ";
                    query2 = query2 + " And EmpDet.Wages='" + ddlEmployeeType.SelectedItem.Text + "' And EmpDet.CompCode='" + SessionCcode + "'";
                    query2 = query2 + " And EmpDet.LocCode='" + SessionLcode + "' And AD.FromDate = convert(datetime,'" + MyDate1 + "', 105)";
                    query2 = query2 + " And AD.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
                    query2 = query2 + " And AD.Lcode='" + SessionLcode + "'";

                    //Check Token No
                    if (ChkTokenNoSalaryprocess.Checked == true)
                    {
                        query2 = query2 + " And EmpDet.ExistingCode='" + txtTokenNo.Text.ToString() + "'";
                    }
                    query2 = query2 + " Order by EmpDet.ExistingCode Asc";
                    dt = objdata.RptEmployeeMultipleDetails(query2);



                    if (dt.Rows.Count != 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Get Allowence And Deduction
                            DataTable Allo_Ded_DT = new DataTable();
                            query2 = "Select * from [" + SessionEpay + "]..eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                            query2 = query2 + " And Month='" + ddlMonth.SelectedValue + "' And FinancialYear='" + ddlFinYear.SelectedValue + "'";
                            query2 = query2 + " And MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";
                            query2 = query2 + " And FromDate = convert(datetime,'" + MyDate1 + "', 105)";
                            query2 = query2 + " And ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
                            Allo_Ded_DT = objdata.RptEmployeeMultipleDetails(query2);
                            if (Allo_Ded_DT.Rows.Count != 0)
                            {
                                dt.Rows[i]["All3"] = Allo_Ded_DT.Rows[0]["allowances3"].ToString();
                                dt.Rows[i]["All4"] = Allo_Ded_DT.Rows[0]["allowances4"].ToString();
                                dt.Rows[i]["All5"] = Allo_Ded_DT.Rows[0]["allowances5"].ToString();
                                dt.Rows[i]["Ded3"] = Allo_Ded_DT.Rows[0]["Deduction3"].ToString();
                                dt.Rows[i]["Ded4"] = Allo_Ded_DT.Rows[0]["Deduction4"].ToString();
                                dt.Rows[i]["Ded5"] = Allo_Ded_DT.Rows[0]["Deduction5"].ToString();
                                dt.Rows[i]["Advance"] = Allo_Ded_DT.Rows[0]["Advance"].ToString();
                                dt.Rows[i]["DedOthers1"] = Allo_Ded_DT.Rows[0]["DedOthers1"].ToString();
                                dt.Rows[i]["DedOthers2"] = Allo_Ded_DT.Rows[0]["DedOthers2"].ToString();
                                dt.Rows[i]["LOPDays"] = Allo_Ded_DT.Rows[0]["LOPDays"].ToString();
                            }

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];


                        bool sucess = false;
                        if (!ErrFlag)
                        {
                            sucess = false;
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //btnSalaryCalculate.Enabled = false;
                                string Updateval = "1";
                                bool PassFlag = false;
                                bool ErrVerify = false;
                                string EmpNo = "";
                                string EmpType = "";
                                string SalarythroughType = "";
                                string Get_Wagestype = "";
                                basic = "0.00";
                                All1 = "0.00";
                                All2 = "0.00";
                                All3 = "0.00";
                                All5 = "0.00";
                                Al4 = "0.00";
                                Ded1 = "0.00";
                                ded2 = "0.00";
                                ded3 = "0.00";
                                ded4 = "0.00";
                                ded5 = "0.00";


                                //All Veriable Null Set Start
                                Work = "0";
                                NFh = "0";
                                lopdays = "0";
                                TotalEarnings = "0.0";
                                TotalDeductions = "0.0";
                                NetPay = "0.0";
                                RoundOffNetPay = "0.0";
                                Words = "";
                                EmployeeDays = "0.0";
                                WeekOff = "0.0";
                                cl = "0";
                                HalfNight_Amt = "0.0";
                                FullNight_Amt = "0.0";
                                ThreeSided_Amt = "0.0";
                                SpinningAmt = "0.0";
                                DayIncentive = "0.0";
                                PFS = "0";
                                ESI = "0";
                                Advance = "0";
                                lop = "0";
                                OTDays = "0";
                                lopdays = "0";

                                //Basic Spilit Det
                                BasicAndDA = "0.0";
                                BasicHRA = "0.0";
                                ConvAllow = "0.0";
                                EduAllow = "0.0";
                                MediAllow = "0.0";
                                BasicRAI = "0.0";
                                WashingAllow = "0.0";
                                OTHoursAmtNew_Str = "0";
                                OTHoursNew_Str = "0";
                                Fixed_Work_Days = "0";
                                WH_Work_Days = "0";

                                DedOthers1 = "0.0";
                                DedOthers2 = "0.0";

                                //Employeer PF And ESI
                                EmployeerPFOne = "0";
                                EmployeerPFTwo = "0";
                                EmployeerESI = "0";
                                //All Veriable Null Set End


                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                string qry_dpt1 = "Select DeptCode from Department_Mst where DeptName = '" + dt.Rows[i][0].ToString() + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());

                                string qry_emp = "Select EmpNo from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + dt.Rows[i][1].ToString() + "' and " +
                                                 "Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and Process_Mode='1' and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105)";

                                string qry_EmpType = "select ET.EmpTypeCd from Employee_Mst EM inner join MstEmployeeType ET on EM.Wages=ET.EmpType where EM.MachineID='" + dt.Rows[i][1].ToString() + "' AND EM.CompCode='" + SessionCcode + "' and EM.LocCode = '" + SessionLcode + "'";
                                SqlCommand cmd_EmpType = new SqlCommand(qry_EmpType, cn);
                                EmpType = (cmd_EmpType.ExecuteScalar()).ToString();


                                //Get Employee Wagestype and Salarythrough
                                string wages_query = "";
                                SqlCommand cmd_wages = new SqlCommand();
                                //wages_query = "Select Wagestype from Officialprofile where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                //SqlCommand cmd_wages = new SqlCommand(wages_query, cn);
                                //Get_Wagestype = (cmd_wages.ExecuteScalar()).ToString();

                                if (ddlEmployeeType.SelectedItem.Text == "CIVIL")
                                {
                                    Get_Wagestype = "1";
                                }
                                else
                                {
                                    Get_Wagestype = "2";

                                }

                                wages_query = "Select Salary_Through from Employee_Mst where EmpNo='" + dt.Rows[i][1].ToString() + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SqlCommand cmd_cash_bank = new SqlCommand(wages_query, cn);
                                SalarythroughType = (cmd_cash_bank.ExecuteScalar()).ToString();



                                // " and Process_Mode='1' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFromDate.Text + "',105)";

                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                //DateTime dfrom = Convert.ToDateTime(txtFromDate.Text);
                                //DateTime dtto = Convert.ToDateTime(txtToDate.Text);
                                //DateTime MyDate1 = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", null);
                                //DateTime MyDate2 = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", null);
                                TempDate = Convert.ToDateTime(txtFromDate.Text).AddMonths(0).ToShortDateString();
                                EmpNo = dt.Rows[i][1].ToString();
                                All3 = dt.Rows[i][4].ToString();
                                Al4 = dt.Rows[i][5].ToString();
                                All5 = dt.Rows[i][6].ToString();
                                ded3 = dt.Rows[i][7].ToString();
                                ded4 = dt.Rows[i][8].ToString();
                                ded5 = dt.Rows[i][9].ToString();
                                Advance = dt.Rows[i][10].ToString();
                                ExistNo = dt.Rows[i][2].ToString();

                                //Deduction Others
                                DedOthers1 = dt.Rows[i][11].ToString();
                                DedOthers2 = dt.Rows[i][12].ToString();

                                Transport_Charges_Regular = "0";

                                if (EmpNo == "7398")
                                {
                                    string Check = "";
                                }

                                //Advance Amt Check And Update Start
                                DataTable Advance_Check_DT = new DataTable();
                                //query2 = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                //query2 = query2 + " And Month='" + ddlMonth.SelectedValue + "' And FinancialYear='" + ddlFinYear.SelectedValue + "'";
                                //query2 = query2 + " And MachineID='" + dt.Rows[i]["BiometricID"].ToString() + "'";
                                //query2 = query2 + " And FromDate = convert(datetime,'" + MyDate1 + "', 105)";
                                //query2 = query2 + " And ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
                                //Advance_Check_DT = objdata.RptEmployeeMultipleDetails(query2);
                                Advance_Amt_Check = "0";
                                //if (Advance_Check_DT.Rows.Count != 0)
                                //{
                                //    if (Advance_Check_DT.Rows[0]["Advance_Check"].ToString() == "1")
                                //    {
                                //        Advance = dt.Rows[i][10].ToString();
                                //        Advance_Amt_Check = "1";
                                //    }
                                //    else { Advance_Amt_Check = "0"; }
                                //}

                                if (Convert.ToDecimal(Advance.ToString()) > 0)
                                {
                                    Advance_Amt_Check = "1";
                                }
                                else { Advance_Amt_Check = "0"; }

                                string Advance_Paid_Amt = "0";
                                string Balance_Amt = "0";
                                string Monthly_Paid_Amt = "0";
                                DataTable Update_Advance = new DataTable();

                                //Check Already Paid Or Not
                                query2 = "Select * from [" + SessionEpay + "]..Advancerepayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And TransDate=convert(datetime,'" + MyDate1 + "', 105)";
                                Advance_Pay_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Advance_Pay_DT.Rows.Count != 0)
                                {
                                    string Advance_ID_Get = "0";
                                    Advance_ID_Get = Advance_Pay_DT.Rows[0]["AdvanceId"].ToString();
                                    //Delete OLD Amount Paid...
                                    query2 = "Delete from [" + SessionEpay + "]..Advancerepayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                    query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And TransDate=convert(datetime,'" + MyDate1 + "', 105)";
                                    Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                    //Update Advance Status
                                    query2 = "Update [" + SessionEpay + "]..AdvancePayment set Completed='N' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                    query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ID='" + Advance_ID_Get + "'";
                                    Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                }

                                if (Advance_Amt_Check.ToString() == "0")
                                {
                                    query2 = "Select * from [" + SessionEpay + "]..AdvancePayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                    query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Completed='N'";
                                    Advance_Mst_DT = objdata.RptEmployeeMultipleDetails(query2);
                                    if (Advance_Mst_DT.Rows.Count != 0)
                                    {


                                        //get Balance Amt
                                        if (Advance_Amt_Check.ToString() == "0")
                                        {

                                            query2 = "Select isnull(sum(Amount),0) as Paid_Amt from [" + SessionEpay + "]..Advancerepayment where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                                            cmd_wages = new SqlCommand(query2, cn);
                                            Advance_Paid_Amt = (cmd_wages.ExecuteScalar()).ToString();
                                            Balance_Amt = (Convert.ToDecimal(Advance_Mst_DT.Rows[0]["Amount"].ToString()) - Convert.ToDecimal(Advance_Paid_Amt)).ToString();
                                            Monthly_Paid_Amt = Advance_Mst_DT.Rows[0]["MonthlyDeduction"].ToString();
                                            if (Convert.ToDecimal(Monthly_Paid_Amt.ToString()) >= Convert.ToDecimal(Balance_Amt.ToString()))
                                            {
                                                Advance = Balance_Amt;
                                            }
                                            else
                                            {
                                                Advance = Monthly_Paid_Amt;
                                            }
                                        }
                                        if (Convert.ToDecimal(Advance.ToString()) > 0)
                                        {
                                            //Paid Count Get
                                            query2 = "Select isnull(count(Amount),0) as Paid_Amt from [" + SessionEpay + "]..Advancerepayment where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                                            cmd_wages = new SqlCommand(query2, cn);
                                            Advance_Paid_Count = (cmd_wages.ExecuteScalar()).ToString();
                                            Advance_Paid_Count = (Convert.ToDecimal(Advance_Paid_Count.ToString()) + Convert.ToDecimal(1)).ToString();

                                            //Update Advance
                                            string Advance_Status = "N";
                                            Balance_Amt = (Convert.ToDecimal(Balance_Amt.ToString()) - Convert.ToDecimal(Advance.ToString())).ToString();
                                            Balance_Amt = (Math.Round(Convert.ToDecimal(Balance_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(Balance_Amt.ToString()) <= 0)
                                            {
                                                Advance_Status = "Y";
                                            }
                                            else { Advance_Status = "N"; }
                                            query2 = "Update [" + SessionEpay + "]..AdvancePayment set BalanceAmount='" + Balance_Amt + "',Completed='" + Advance_Status + "',IncreaseMonth='" + Advance_Paid_Count + "',ReductionMonth='0' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                            query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ID='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                                            Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                            //Insert Paid Amount
                                            query2 = "Insert Into [" + SessionEpay + "]..Advancerepayment(EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode) values ('" + dt.Rows[i]["EmpNo"].ToString() + "',";
                                            query2 = query2 + "convert(datetime, '" + MyDate1 + "', 105),'" + ddlMonth.SelectedValue + "','" + Advance + "','" + Advance_Mst_DT.Rows[0]["ID"] + "',";
                                            query2 = query2 + "'" + SessionCcode + "','" + SessionLcode + "')";
                                            Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                        }

                                    }
                                }


                                //Advance Amt Check And Update End


                                //Get Basic Incentive Text Display Payslip Start
                                string Ince_query = "";
                                DataTable DT_Inc_Q = new DataTable();
                                Ince_query = "Select * from [" + SessionEpay + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and Month='" + ddlMonth.Text + "' and Years='" + ddlFinYear.SelectedValue + "'";
                                DT_Inc_Q = objdata.RptEmployeeMultipleDetails(Ince_query);
                                if (DT_Inc_Q.Rows.Count != 0)
                                {
                                    Basic_Incentive_Text = DT_Inc_Q.Rows[0]["Incentive_Text"].ToString();
                                }
                                else
                                {
                                    Basic_Incentive_Text = "Incentive";
                                }
                                //Get Basic Incentive Text Display Payslip End

                                string Month = TempDate;
                                DateTime m;
                                m = Convert.ToDateTime(TempDate);
                                d = m.Month;
                                int mon = m.Month;
                                int yr = m.Year;
                                #region Month
                                string Mont = "";
                                switch (d)
                                {
                                    case 1:
                                        Mont = "January";
                                        break;

                                    case 2:
                                        Mont = "February";
                                        break;
                                    case 3:
                                        Mont = "March";
                                        break;
                                    case 4:
                                        Mont = "April";
                                        break;
                                    case 5:
                                        Mont = "May";
                                        break;
                                    case 6:
                                        Mont = "June";
                                        break;
                                    case 7:
                                        Mont = "July";
                                        break;
                                    case 8:
                                        Mont = "August";
                                        break;
                                    case 9:
                                        Mont = "September";
                                        break;
                                    case 10:
                                        Mont = "October";
                                        break;
                                    case 11:
                                        Mont = "November";
                                        break;
                                    case 12:
                                        Mont = "December";
                                        break;
                                    default:
                                        break;
                                }
                                #endregion

                                SMonth = Convert.ToString(mon);
                                Year = Convert.ToString(yr);
                                dd = DateTime.DaysInMonth(yr, mon);
                                MyMonth = Mont;
                                DataTable dtAttenance = new DataTable();

                                OTHoursNew_Str = "0";
                                OTHoursAmtNew_Str = "0";
                                lop = "0.00";
                                Query = "";
                                Query = Query + "Select DeptName,EmpNo,Days,Months,FinancialYear,TotalDays,NFh,CL,AbsentDays,weekoff,home,halfNight,";
                                Query = Query + "FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days from [" + SessionEpay + "]..AttenanceDetails where EmpNo='" + EmpNo + "' and ";
                                Query = Query + "DeptName='" + DepartmentCode + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Months='" + Mont + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime, FromDate, 105 ) = Convert(datetime, '" + MyDate1 + "',105) ";
                                Query = Query + "and convert(datetime, ToDate, 105) = convert(Datetime, '" + MyDate2 + "', 105) ";
                                dtAttenance = objdata.RptEmployeeMultipleDetails(Query);
                                //dtAttenance = objdata.Salary_attenanceDetails(EmpNo, Mont, ddlFinYear.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                if (dtAttenance.Rows.Count <= 0)
                                {
                                    //i = i + 2;
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found " + i + "');", true);
                                    //ErrVerify = true;
                                    EmployeeDays = "0";
                                    Work = "0";
                                    NFh = "0";
                                    cl = "0";
                                    lopdays = "0";
                                    WeekOff = "0";
                                    Home = "0";
                                    halfNight = "0";
                                    FullNight = "0";
                                    ThreeSided = "0";
                                    OTHoursNew_Str = "0";
                                    OTHoursAmtNew_Str = "0";
                                    Fixed_Work_Days = "0";
                                    WH_Work_Days = "0";
                                    NFH_Work_Days_Allow_Days = "0";
                                }
                                else
                                {
                                    if (dtAttenance.Rows.Count > 0)
                                    {

                                        EmployeeDays = dtAttenance.Rows[0]["Days"].ToString();
                                        Work = dtAttenance.Rows[0]["TotalDays"].ToString();
                                        NFh = dtAttenance.Rows[0]["NFh"].ToString();
                                        cl = dtAttenance.Rows[0]["CL"].ToString();
                                        lopdays = dtAttenance.Rows[0]["AbsentDays"].ToString();
                                        WeekOff = dtAttenance.Rows[0]["weekoff"].ToString();
                                        Home = dtAttenance.Rows[0]["home"].ToString();
                                        halfNight = dtAttenance.Rows[0]["halfNight"].ToString();
                                        FullNight = dtAttenance.Rows[0]["FullNight"].ToString();
                                        ThreeSided = dtAttenance.Rows[0]["ThreeSided"].ToString();
                                        OTHoursNew_Str = dtAttenance.Rows[0]["OTHoursNew"].ToString();
                                        Fixed_Work_Days = dtAttenance.Rows[0]["Fixed_Work_Days"].ToString();
                                        WH_Work_Days = dtAttenance.Rows[0]["WH_Work_Days"].ToString();
                                        NFH_Work_Days_Allow_Days = dtAttenance.Rows[0]["NFH_Work_Days"].ToString();
                                        //txtlossofpay.Text = (Convert.ToDecimal(txtwork.Text) - (Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtNFh.Text) + (Convert.ToDecimal(txtcl.Text))))).ToString();
                                        DataTable dt_ot = new DataTable();
                                        Query = "";
                                        Query = Query + "Select netAmount from [" + SessionEpay + "]..OverTime where EmpNo='" + EmpNo + "' and Department='" + DepartmentCode + "' and Financialyr='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "'";
                                        Query = Query + "and Lcode='" + SessionLcode + "' and Month='" + Mont + "' and FromDate=convert(datetime, '" + MyDate1 + "', 105) and ToDate=convert(datetime,'" + MyDate2 + "',105)";
                                        dt_ot = objdata.RptEmployeeMultipleDetails(Query);
                                        //dt_ot = objdata.Attenance_ot(EmpNo, Mont, ddlFinYear.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                        if (dt_ot.Rows.Count > 0)
                                        {
                                            OTDays = dt_ot.Rows[0]["netAmount"].ToString();
                                        }
                                        else
                                        {
                                            OTDays = "0";
                                        }
                                    }
                                }
                                string Salarymonth = "";
                                Query = "";
                                Query = Query + "Select Month from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                Query = Query + "and Month='" + ddlMonth.Text + "' and convert(datetime,FromDate,105) = convert(datetime,'" + MyDate1 + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + MyDate2 + "', 105) ";
                                dt_Others = objdata.RptEmployeeMultipleDetails(Query);
                                if (dt_Others.Rows.Count > 0) { Salarymonth = dt_Others.Rows[0]["Month"].ToString(); } else { Salarymonth = "0"; }

                                //Salarymonth = objdata.SalaryMonthFromLeave(EmpNo, ddlFinYear.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);

                                string MonthEEE = Salarymonth.Replace(" ", "");

                                int result = string.Compare(Salarymonth, Mont, true);
                                //int i = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
                                string PayDay = "";
                                Query = "";
                                Query = Query + "Select LeavePaydayStaff From  [" + SessionEpay + "]..MstLeavePayMaster  Where Leaveid=1";
                                dt_Others = objdata.RptEmployeeMultipleDetails(Query);
                                if (dt_Others.Rows.Count > 0) { PayDay = dt_Others.Rows[0]["LeavePaydayStaff"].ToString(); } else { PayDay = "0"; }
                                //PayDay = objdata.SalaryPayDay();
                                // txtattendancewithpay.Text = PayDay;

                                if (Name_Upload == "")
                                {
                                    if (Mont == MonthEEE)
                                    {
                                        Updateval = "1";
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                        //ErrVerify = true;
                                    }
                                    else
                                    {
                                        Updateval = "1";
                                    }

                                }
                                else
                                {
                                    //Updateval = "0"; 
                                }
                                //if (Mont == MonthEEE)
                                //{
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                //    ErrVerify = true;
                                //}
                                //else
                                {
                                    if (EmpNo == "9602")
                                    {
                                        string asdasda = "";
                                    }
                                    Leaver_Credit_Status = "0";
                                    if (ddlCategory.SelectedValue == "1") //Staff Salary Calculation
                                    {
                                        AdvAmt = 0;
                                        ID = "";
                                        basicsalary = 0;
                                        Basic_For_SM = "0";
                                        DataTable dt1 = new DataTable();
                                        Query = "select EmpNo,BaseSalary,FDA,VDA,HRA,Total,Unioncharges,Alllowance1,Alllowance2,Deduction1,Deduction2,PFS from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                                        dt1 = objdata.RptEmployeeMultipleDetails(Query);
                                        if (dt1.Rows.Count != 0)
                                        {
                                            if (dt1.Rows[0]["Alllowance1"].ToString() == "") { dt1.Rows[0]["Alllowance1"] = "0"; }
                                            if (dt1.Rows[0]["Alllowance2"].ToString() == "") { dt1.Rows[0]["Alllowance2"] = "0"; }
                                            if (dt1.Rows[0]["Deduction1"].ToString() == "") { dt1.Rows[0]["Deduction1"] = "0"; }
                                            if (dt1.Rows[0]["Deduction2"].ToString() == "") { dt1.Rows[0]["Deduction2"] = "0"; }
                                            if (dt1.Rows[0]["PFS"].ToString() == "") { dt1.Rows[0]["PFS"] = "0"; }
                                            //if (dt1.Rows[0]["BaseSalary"].ToString() != "") 
                                            //{
                                            //    dt1.Rows[0]["BaseSalary"] = (Convert.ToDecimal(123) + Convert.ToDecimal(dt1.Rows[0]["BaseSalary"])).ToString();
                                            //}
                                        }

                                        DataTable dteligible = new DataTable();
                                        DataTable dtpf = new DataTable();
                                        Query = "";
                                        Query = Query + "Select PF_per,ESI_per,StaffSalary,StampCg,VDA1,VDA2,Union_val,Spinning,DayIncentive,";
                                        Query = Query + "halfNight,FullNight,ThreeSideAmt,EmployeerPFone,EmployeerPFTwo,EmployeerESI from [" + SessionEpay + "]..MstESIPF where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        dtpf = objdata.RptEmployeeMultipleDetails(Query);


                                        Query = "";
                                        Query = Query + "Select (Eligible_ESI)as ElgibleESI,(Eligible_PF) as EligiblePF,'0' as PF_Type from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                                        dteligible = objdata.RptEmployeeMultipleDetails(Query);
                                        //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                        //if (dtpf.Rows[0]["Union_val"].ToString() == "1")
                                        //{
                                        //    txtunion.Text = dt1.Rows[0]["Unioncharges"].ToString();
                                        //}
                                        //else
                                        //{
                                        //    txtunion.Text = "0";
                                        //}

                                        DataTable dtBasicPercent = new DataTable();
                                        Query = "";
                                        Query = Query + "Select * from [" + SessionEpay + "]..MstBasicDet where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + EmpType.ToString() + "'";
                                        dtBasicPercent = objdata.RptEmployeeMultipleDetails(Query);

                                        if (dt1.Rows.Count > 0)
                                        {

                                            //string dt_ot1 = "";
                                            //dt_ot1 = objdata.OT_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode, Mont);
                                            //if (dt_ot1.Trim() != "")
                                            //{
                                            //    txtOT.Text = dt_ot1.ToString();
                                            //}
                                            //else
                                            //{
                                            //    txtOT.Text = "0";
                                            //}
                                            //PFS = dt1.Rows[0]["PFS"].ToString();
                                            string basic1 = "0.00";

                                            DataTable Get_Leave_Credit = new DataTable();
                                            DataTable Get_Leave_EmpDet = new DataTable();
                                            DataTable Update_Leave = new DataTable();
                                            string query = "";
                                            string OLD_Leave_Credit = "0";
                                            string EmployeeDays_Sal_Cal = "0";
                                            query = "Select * from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' And AttnFinYear='" + Leave_Credit_Year + "'";
                                            Get_Leave_Credit = objdata.RptEmployeeMultipleDetails(query);


                                            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                                            Get_Leave_EmpDet = objdata.RptEmployeeMultipleDetails(query);

                                            if (Get_Leave_Credit.Rows.Count != 0)
                                            {
                                                OLD_Leave_Credit = Get_Leave_Credit.Rows[0]["LeaveCredit"].ToString();
                                            }

                                            Leave_Credit_Add_Minus = "0";
                                            Leave_Credit_Days_Month = "0";
                                            Leave_Credit_Type = "";
                                            string Staff_Work_Days_Check = "0";
                                            //Check Salary Details Allready Saved Or Not
                                            query = "Select * from [" + SessionEpay + "]..SalaryDetails where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And EmpNo='" + EmpNo + "'";
                                            query = query + " And Month='" + ddlMonth.Text + "' And FinancialYear='" + ddlFinYear.SelectedValue + "'";
                                            Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                            if (Update_Leave.Rows.Count != 0)
                                            {
                                                if (Update_Leave.Rows[0]["Leave_Credit_Type"].ToString() == "Add")
                                                {
                                                    OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) + Convert.ToDecimal(Update_Leave.Rows[0]["Leave_Credit_Add"].ToString())).ToString();
                                                }
                                                else
                                                {
                                                    if (Update_Leave.Rows[0]["Leave_Credit_Type"].ToString() == "LOPDOJ")
                                                    {
                                                        OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) - Convert.ToDecimal(Update_Leave.Rows[0]["Leave_Credit_Add"].ToString())).ToString();
                                                        //Staff_Work_Days_Check = (Convert.ToDecimal(Update_Leave.Rows[0]["WorkedDays"].ToString()) + Convert.ToDecimal(Update_Leave.Rows[0]["NFh"].ToString()) + Convert.ToDecimal(Update_Leave.Rows[0]["WH_Work_Days"].ToString())).ToString();
                                                        //Staff_Work_Days_Check = (Convert.ToDecimal(Staff_Work_Days_Check) - Convert.ToDecimal(Update_Leave.Rows[0]["Fixed_Work_Days"].ToString())).ToString();
                                                        //OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) - Convert.ToDecimal(Staff_Work_Days_Check)).ToString();
                                                    }
                                                    else if (Update_Leave.Rows[0]["Leave_Credit_Type"].ToString() == "MIDDOJ")
                                                    {
                                                        OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) - Convert.ToDecimal(Update_Leave.Rows[0]["Leave_Credit_Add"].ToString())).ToString();
                                                    }
                                                    else
                                                    {
                                                        Staff_Work_Days_Check = (Convert.ToDecimal(Update_Leave.Rows[0]["WorkedDays"].ToString()) + Convert.ToDecimal(Update_Leave.Rows[0]["NFh"].ToString()) + Convert.ToDecimal(Update_Leave.Rows[0]["WH_Work_Days"].ToString())).ToString();
                                                        Staff_Work_Days_Check = (Convert.ToDecimal(Staff_Work_Days_Check) - Convert.ToDecimal(Update_Leave.Rows[0]["Fixed_Work_Days"].ToString())).ToString();
                                                        OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) - Convert.ToDecimal(Staff_Work_Days_Check)).ToString();
                                                    }
                                                }
                                            }

                                            Basic_For_SM = "0";
                                            Basic_For_SM = dt1.Rows[0]["BaseSalary"].ToString();
                                            if (EmpType == "2")
                                            {
                                                basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString())).ToString();
                                                //basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh))).ToString();
                                            }
                                            else if (EmpType == "1" || EmpType == "6" || EmpType == "7")
                                            {
                                                basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Fixed_Work_Days)).ToString();

                                                //Check DOJ Current Month
                                                string query_doj = "";
                                                bool DOJ_Check_True_or_False = false;
                                                DataTable DT_Q_DOJ = new DataTable();
                                                query_doj = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And EmpNo='" + EmpNo + "'";
                                                query_doj = query_doj + " And DATENAME(MM,DOJ)='" + ddlMonth.Text + "' And YEAR(DOJ)='" + Leave_Credit_Year + "'";
                                                DT_Q_DOJ = objdata.RptEmployeeMultipleDetails(query_doj);
                                                if (DT_Q_DOJ.Rows.Count != 0)
                                                {
                                                    DOJ_Check_True_or_False = true;
                                                }
                                                else
                                                {
                                                    DOJ_Check_True_or_False = false;
                                                }

                                                //Manul LOP Check
                                                if (Convert.ToDecimal(dt.Rows[i]["LOPDays"].ToString()) != 0)
                                                {
                                                    string LOP_Staff_Work_Days = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh) + Convert.ToDecimal(WH_Work_Days)).ToString();
                                                    string LOP_Staff_Leave_Credit = "0";
                                                    if (Convert.ToDecimal(LOP_Staff_Work_Days) > Convert.ToDecimal(Fixed_Work_Days))
                                                    {
                                                        LOP_Staff_Leave_Credit = (Convert.ToDecimal(LOP_Staff_Work_Days) - Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                    }
                                                    else
                                                    {
                                                        LOP_Staff_Leave_Credit = "0";
                                                    }

                                                    Leave_Credit_Add_Minus = LOP_Staff_Leave_Credit;
                                                    Leave_Credit_Days_Month = "0";
                                                    Leave_Credit_Type = "LOPDOJ";

                                                    //Loss Of Pay Days And Amt Add
                                                    lop = "0.00";
                                                    lopdays = dt.Rows[i]["LOPDays"].ToString();
                                                    lop = ((Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())) * Convert.ToDecimal(lopdays)).ToString();
                                                    lop = (Math.Round(Convert.ToDecimal(lop), 2, MidpointRounding.AwayFromZero)).ToString();

                                                    string WorkDays_Lop_Minus_Manual = "0";
                                                    basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())).ToString();
                                                    WorkDays_Lop_Minus_Manual = (Convert.ToDecimal(Work.ToString()) - Convert.ToDecimal(lopdays.ToString())).ToString();
                                                    basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(WorkDays_Lop_Minus_Manual))).ToString();

                                                    OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) + Convert.ToDecimal(Leave_Credit_Add_Minus)).ToString();

                                                    //Delete Leave Credit
                                                    query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                    query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                    Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                    //Insert Leave Credit
                                                    query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                    query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                    query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                    query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                    Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                    Leaver_Credit_Status = "1";

                                                }
                                                else if (DOJ_Check_True_or_False == true) //Month Mid DOJ Employee Leave Credit Process Start
                                                {
                                                    //Get Month Total Days
                                                    string W_query = "";
                                                    string DOJ_Date = "";
                                                    string DOJ_Date_Format = "";
                                                    string Week_off_Count = "0";
                                                    string Weekoff_Text = "";
                                                    DataTable DT_W = new DataTable();
                                                    W_query = "Select convert(varchar,DOJ ,105) as DOJ,WeekOff from Employee_Mst where ExistingCode='" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                                    DT_W = objdata.RptEmployeeMultipleDetails(W_query);
                                                    if (DT_W.Rows.Count != 0)
                                                    {
                                                        DOJ_Date = DT_W.Rows[0]["DOJ"].ToString();
                                                        Weekoff_Text = DT_W.Rows[0]["WeekOff"].ToString().ToUpper().ToString();
                                                        if (DOJ_Date != "")
                                                        {
                                                            W_query = "Select isnull(Count(*),0) as Week_Count from LogTime_Days where ExistingCode='" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                                            W_query = W_query + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Weekoff_Text + "')";
                                                            W_query = W_query + " And convert(datetime,Attn_Date ,105) >= convert(datetime,'" + DOJ_Date + "',105)";
                                                            W_query = W_query + " And convert(datetime,Attn_Date ,105) <= convert(datetime,'" + txtToDate.Text + "',105)";
                                                            DT_W = objdata.RptEmployeeMultipleDetails(W_query);
                                                            if (DT_W.Rows.Count != 0)
                                                            {
                                                                Week_off_Count = DT_W.Rows[0]["Week_Count"].ToString();
                                                            }
                                                        }
                                                    }

                                                    string Total_Month_Tot_Days = (DateTime.DaysInMonth(MyDate1.Year, MyDate1.Month)).ToString();
                                                    string Staff_Work_Days = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh) + Convert.ToDecimal(WH_Work_Days)).ToString();
                                                    string Staff_Work_Days_1 = "0";
                                                    string Staff_Leave_Credit = "0";
                                                    if (Convert.ToDecimal(Staff_Work_Days) > Convert.ToDecimal(Fixed_Work_Days))
                                                    {
                                                        Staff_Leave_Credit = (Convert.ToDecimal(Staff_Work_Days) - Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                        EmployeeDays_Sal_Cal = (Convert.ToDecimal(Staff_Work_Days) - Convert.ToDecimal(Staff_Leave_Credit)).ToString();

                                                        basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Total_Month_Tot_Days.ToString())).ToString();
                                                        basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays_Sal_Cal) + Convert.ToDecimal(Week_off_Count))).ToString();
                                                        Leave_Credit_Days_Month = Staff_Leave_Credit;
                                                        Leave_Credit_Add_Minus = "0";
                                                        Leave_Credit_Type = "MIDDOJ";

                                                        OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) + Convert.ToDecimal(Leave_Credit_Days_Month)).ToString();

                                                        //Delete Leave Credit
                                                        query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                        query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        //Insert Leave Credit
                                                        query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                        query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                        query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                        query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        Leaver_Credit_Status = "1";
                                                    }
                                                    else if (Convert.ToDecimal(Staff_Work_Days) == Convert.ToDecimal(Fixed_Work_Days))
                                                    {
                                                        Leave_Credit_Add_Minus = "0";
                                                        Leave_Credit_Days_Month = "0";
                                                        Leave_Credit_Type = "MIDDOJ";
                                                        basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Total_Month_Tot_Days.ToString())).ToString();
                                                        basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Staff_Work_Days) + Convert.ToDecimal(Week_off_Count))).ToString();

                                                        //Delete Leave Credit
                                                        query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                        query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        //Insert Leave Credit
                                                        query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                        query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                        query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                        query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        Leaver_Credit_Status = "1";
                                                    }
                                                    else
                                                    {
                                                        Leave_Credit_Add_Minus = "0";
                                                        Leave_Credit_Days_Month = "0";
                                                        Leave_Credit_Type = "MIDDOJ";

                                                        //Loss Of Pay Days And Amt Add
                                                        lop = "0.00";
                                                        lopdays = (Convert.ToDecimal(Fixed_Work_Days) - Convert.ToDecimal(Staff_Work_Days)).ToString();
                                                        lop = ((Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Total_Month_Tot_Days.ToString())) * Convert.ToDecimal(lopdays)).ToString();
                                                        lop = (Math.Round(Convert.ToDecimal(lop), 2, MidpointRounding.AwayFromZero)).ToString();

                                                        string WorkDays_Lop_Minus = Staff_Work_Days;
                                                        basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())).ToString();
                                                        basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(WorkDays_Lop_Minus) + Convert.ToDecimal(Week_off_Count))).ToString();

                                                        //Delete Leave Credit
                                                        query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                        query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        //Insert Leave Credit
                                                        query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                        query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                        query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                        query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        Leaver_Credit_Status = "1";
                                                    }
                                                }//Month Mid DOJ Employee Leave Credit Process End

                                                else  //Leave Credit Checked Start
                                                {
                                                    //Leave Credit Checked Start

                                                    //Check Working Days Morthen 26 days
                                                    string Staff_Work_Days = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh) + Convert.ToDecimal(WH_Work_Days)).ToString();
                                                    string Staff_Work_Days_1 = "0";
                                                    string Staff_Leave_Credit = "0";
                                                    if (Convert.ToDecimal(Staff_Work_Days) > Convert.ToDecimal(Fixed_Work_Days))
                                                    {
                                                        Staff_Leave_Credit = (Convert.ToDecimal(Staff_Work_Days) - Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                        EmployeeDays_Sal_Cal = (Convert.ToDecimal(Staff_Work_Days) - Convert.ToDecimal(Staff_Leave_Credit)).ToString();

                                                        basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays_Sal_Cal))).ToString();
                                                        //EmployeeDays = EmployeeDays_Sal_Cal;

                                                        Leave_Credit_Days_Month = Staff_Leave_Credit;
                                                        Leave_Credit_Add_Minus = "0";
                                                        Leave_Credit_Type = "";

                                                        OLD_Leave_Credit = (Convert.ToDecimal(OLD_Leave_Credit) + Convert.ToDecimal(Staff_Leave_Credit)).ToString();
                                                        //Delete Leave Credit
                                                        query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                        query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        //Insert Leave Credit
                                                        query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                        query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                        query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                        query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        Leaver_Credit_Status = "1";
                                                    }
                                                    else if (Convert.ToDecimal(Staff_Work_Days) == Convert.ToDecimal(Fixed_Work_Days))
                                                    {
                                                        Leave_Credit_Add_Minus = "0";
                                                        Leave_Credit_Days_Month = "0";
                                                        Leave_Credit_Type = "";
                                                        basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Staff_Work_Days))).ToString();
                                                        //EmployeeDays = EmployeeDays;

                                                        //Delete Leave Credit
                                                        query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                        query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                        //Insert Leave Credit
                                                        query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                        query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                        query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                        query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                        Update_Leave = objdata.RptEmployeeMultipleDetails(query);

                                                    }
                                                    else
                                                    {
                                                        //Leave Credit Check
                                                        if (Get_Leave_Credit.Rows.Count == 0)
                                                        {
                                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Staff_Work_Days))).ToString();
                                                            //EmployeeDays = EmployeeDays;

                                                            Leave_Credit_Add_Minus = "0";
                                                            Leave_Credit_Days_Month = "0";
                                                            Leave_Credit_Type = "";

                                                            //Loss Of Pay Days And Amt Add
                                                            lop = "0.00";
                                                            lopdays = (Convert.ToDecimal(Fixed_Work_Days) - Convert.ToDecimal(Staff_Work_Days)).ToString();
                                                            lop = ((Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())) * Convert.ToDecimal(lopdays)).ToString();
                                                            lop = (Math.Round(Convert.ToDecimal(lop), 2, MidpointRounding.AwayFromZero)).ToString();

                                                            string WorkDays_Lop_Minus = "0";
                                                            basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())).ToString();
                                                            WorkDays_Lop_Minus = (Convert.ToDecimal(Work.ToString()) - Convert.ToDecimal(lopdays.ToString())).ToString();
                                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(WorkDays_Lop_Minus))).ToString();

                                                            //Delete Leave Credit
                                                            query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                            query = query + " and EmpNo='" + EmpNo + "'and AttnFinYear='" + Leave_Credit_Year + "'";
                                                            Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                            //Insert Leave Credit
                                                            query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                            query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                            query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                            query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                            Update_Leave = objdata.RptEmployeeMultipleDetails(query);


                                                        }
                                                        else
                                                        {
                                                            //Balance Leave Credit Update
                                                            Staff_Work_Days = (Convert.ToDecimal(Staff_Work_Days) + Convert.ToDecimal(OLD_Leave_Credit)).ToString();
                                                            if (Convert.ToDecimal(Staff_Work_Days) > Convert.ToDecimal(Fixed_Work_Days))
                                                            {
                                                                Staff_Leave_Credit = (Convert.ToDecimal(Staff_Work_Days) - Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                                EmployeeDays_Sal_Cal = (Convert.ToDecimal(Staff_Work_Days) - Convert.ToDecimal(Staff_Leave_Credit)).ToString();

                                                                basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays_Sal_Cal))).ToString();
                                                                //EmployeeDays = EmployeeDays_Sal_Cal;

                                                                Leave_Credit_Add_Minus = (Convert.ToDecimal(OLD_Leave_Credit) - Convert.ToDecimal(Staff_Leave_Credit)).ToString();
                                                                Leave_Credit_Days_Month = "-" + Leave_Credit_Add_Minus.ToString();
                                                                Leave_Credit_Type = "Add";
                                                                //Update Leave Credit
                                                                OLD_Leave_Credit = (Convert.ToDecimal(Staff_Leave_Credit)).ToString();

                                                                //Delete Leave Credit
                                                                query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' And AttnFinYear='" + Leave_Credit_Year + "'";
                                                                Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                //Insert Leave Credit
                                                                query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                                query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                                query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                                query = query + ",'" + OLD_Leave_Credit + "','" + Leave_Credit_Year + "')";
                                                                Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                Leaver_Credit_Status = "1";
                                                            }
                                                            else if (Convert.ToDecimal(Staff_Work_Days) == Convert.ToDecimal(Fixed_Work_Days))
                                                            {
                                                                basic = (Convert.ToDecimal(basic1) * Convert.ToDecimal(Staff_Work_Days)).ToString();
                                                                //EmployeeDays = Staff_Work_Days;

                                                                Leave_Credit_Add_Minus = OLD_Leave_Credit.ToString();
                                                                Leave_Credit_Days_Month = "-" + Leave_Credit_Add_Minus.ToString();
                                                                Leave_Credit_Type = "Add";

                                                                //Zero Leaver Credit Update
                                                                //Delete Leave Credit
                                                                query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' and AttnFinYear='" + Leave_Credit_Year + "'";
                                                                Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                //Insert Leave Credit
                                                                query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                                query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                                query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                                query = query + ",'0','" + Leave_Credit_Year + "')";
                                                                Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                Leaver_Credit_Status = "1";
                                                            }
                                                            else
                                                            {
                                                                basic = (Convert.ToDecimal(basic1) * Convert.ToDecimal(Staff_Work_Days)).ToString();
                                                                //EmployeeDays = Staff_Work_Days;

                                                                //Delete Leave Credit
                                                                query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' and AttnFinYear='" + Leave_Credit_Year + "'";
                                                                Update_Leave = objdata.RptEmployeeMultipleDetails(query);

                                                                Leave_Credit_Add_Minus = OLD_Leave_Credit.ToString();
                                                                Leave_Credit_Days_Month = "-" + Leave_Credit_Add_Minus.ToString();
                                                                Leave_Credit_Type = "Add";

                                                                if (Convert.ToDecimal(OLD_Leave_Credit) != 0)
                                                                {
                                                                    //Zero Leaver Credit Update
                                                                    //Delete Leave Credit
                                                                    query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' and AttnFinYear='" + Leave_Credit_Year + "'";
                                                                    Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                    //Insert Leave Credit
                                                                    query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                                    query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                                    query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                                    query = query + ",'0','" + Leave_Credit_Year + "')";
                                                                    Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                    Leaver_Credit_Status = "1";
                                                                }
                                                                else
                                                                {
                                                                    //Zero Leaver Credit Update
                                                                    //Delete Leave Credit
                                                                    query = "Delete from [" + SessionEpay + "]..MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' and AttnFinYear='" + Leave_Credit_Year + "'";
                                                                    Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                    //Insert Leave Credit
                                                                    query = "Insert Into [" + SessionEpay + "]..MstLeaveCredit(Ccode,Lcode,EmpNo,EmpName,BiometricID,ExisistingCode,LeaveCredit,AttnFinYear) Values(";
                                                                    query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["FirstName"].ToString() + "'";
                                                                    query = query + ",'" + EmpNo + "','" + Get_Leave_EmpDet.Rows[0]["ExistingCode"].ToString() + "'";
                                                                    query = query + ",'0','" + Leave_Credit_Year + "')";
                                                                    Update_Leave = objdata.RptEmployeeMultipleDetails(query);
                                                                }

                                                                //Loss Of Pay Days And Amt Add
                                                                lop = "0.00";
                                                                lopdays = (Convert.ToDecimal(Fixed_Work_Days) - Convert.ToDecimal(Staff_Work_Days)).ToString();
                                                                lop = ((Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())) * Convert.ToDecimal(lopdays)).ToString();
                                                                lop = (Math.Round(Convert.ToDecimal(lop), 2, MidpointRounding.AwayFromZero)).ToString();

                                                                string WorkDays_Lop_Minus1 = "0";
                                                                basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work.ToString())).ToString();
                                                                WorkDays_Lop_Minus1 = (Convert.ToDecimal(Work.ToString()) - Convert.ToDecimal(lopdays.ToString())).ToString();
                                                                basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(WorkDays_Lop_Minus1))).ToString();
                                                            }
                                                        }
                                                    }
                                                }//Leave Credit Checked End
                                            }
                                            else
                                            {
                                                if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(BasicPFDays))
                                                {
                                                    basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work)).ToString();
                                                    basic = ((Convert.ToDecimal(basic1) * (Convert.ToDecimal(Work))).ToString()).ToString();
                                                }
                                                else
                                                {
                                                    basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(Work)).ToString();
                                                    basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                }

                                            }

                                            //Basic Spilit
                                            if (Convert.ToDecimal(basic) != 0)
                                            {
                                                //Basic And DA
                                                BasicAndDA = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["BasicDA"].ToString())) / 100).ToString();
                                                BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Spl_Allowance
                                                Basic_Spl_Allowance = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Spl_Allowance"].ToString())) / 100).ToString();
                                                Basic_Spl_Allowance = (Math.Round(Convert.ToDecimal(Basic_Spl_Allowance), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic HRA
                                                BasicHRA = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["HRA"].ToString())) / 100).ToString();
                                                BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Uniform
                                                Basic_Uniform = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Uniform"].ToString())) / 100).ToString();
                                                Basic_Uniform = (Math.Round(Convert.ToDecimal(Basic_Uniform), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Vehicle_Maintenance
                                                Basic_Vehicle_Maintenance = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Vehicle_Maintenance"].ToString())) / 100).ToString();
                                                Basic_Vehicle_Maintenance = (Math.Round(Convert.ToDecimal(Basic_Vehicle_Maintenance), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Communication
                                                Basic_Communication = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Communication"].ToString())) / 100).ToString();
                                                Basic_Communication = (Math.Round(Convert.ToDecimal(Basic_Communication), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Journ_Perid_Paper
                                                Basic_Journ_Perid_Paper = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Journ_Perid_Paper"].ToString())) / 100).ToString();
                                                Basic_Journ_Perid_Paper = (Math.Round(Convert.ToDecimal(Basic_Journ_Perid_Paper), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //ConvAllow
                                                ConvAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["ConvAllow"].ToString())) / 100).ToString();
                                                ConvAllow = (Math.Round(Convert.ToDecimal(ConvAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //EduAllow
                                                EduAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["EduAllow"].ToString())) / 100).ToString();
                                                EduAllow = (Math.Round(Convert.ToDecimal(EduAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //MediAllow
                                                MediAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["MediAllow"].ToString())) / 100).ToString();
                                                MediAllow = (Math.Round(Convert.ToDecimal(MediAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //BasicRAI
                                                BasicRAI = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["RAI"].ToString())) / 100).ToString();
                                                BasicRAI = (Math.Round(Convert.ToDecimal(BasicRAI), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //WashingAllow
                                                WashingAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["WashingAllow"].ToString())) / 100).ToString();
                                                WashingAllow = (Math.Round(Convert.ToDecimal(WashingAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Incentives
                                                Basic_Incentives = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Incentives"].ToString())) / 100).ToString();
                                                Basic_Incentives = (Math.Round(Convert.ToDecimal(Basic_Incentives), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Incentive_Text = "";
                                            }
                                            else
                                            {
                                                BasicAndDA = "0.00";
                                                BasicHRA = "0.00";
                                                ConvAllow = "0.00";
                                                EduAllow = "0.00";
                                                MediAllow = "0.00";
                                                BasicRAI = "0.00";
                                                WashingAllow = "0.00";

                                                Basic_Spl_Allowance = "0.00";
                                                Basic_Uniform = "0.00";
                                                Basic_Vehicle_Maintenance = "0.00";
                                                Basic_Communication = "0.00";
                                                Basic_Journ_Perid_Paper = "0.00";
                                                Basic_Incentives = "0.00";
                                                //Basic_Incentive_Text = "";
                                            }

                                            //Sub Staff Full Night Shift Incentive
                                            if (EmpType == "2")
                                            {
                                                All1 = ((Convert.ToDecimal(FullNight)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1"].ToString())).ToString();
                                                All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {
                                                All1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1"].ToString())).ToString();
                                                All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }

                                            basic = (Math.Round(Convert.ToDecimal(basic), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();

                                            //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            All2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance2"].ToString())).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            Ded1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero)).ToString();


                                            stamp = "0";
                                            HalfNight_Amt = "0.00";
                                            FullNight_Amt = "0.00";
                                            DayIncentive = "0.00";
                                            SpinningAmt = "0.00";
                                            ThreeSided_Amt = "0.00";

                                            //Sub-Staff Attn DayIncentive Calculate
                                            DataTable dtIncentive = new DataTable();
                                            DataTable dtIncentive_Month = new DataTable();
                                            Query = "";
                                            Query = Query + "Select Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "'";
                                            Query = Query + "and Lcode='" + SessionLcode + "' And MonthDays='" + Month_Total_days.ToString() + "' order by WorkerDays Asc";
                                            dtIncentive = objdata.RptEmployeeMultipleDetails(Query);


                                            Query = "";
                                            Query = Query + "Select Amt,WDays,HAllowed,OTDays,NFHDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,NFHWorkDays,CalNFHWorkDays ";
                                            Query = Query + "from [" + SessionEpay + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' order by Amt Desc";
                                            dtIncentive_Month = objdata.RptEmployeeMultipleDetails(Query);
                                            string Eligible_Day_Incnt_Tot_Days = "0";
                                            string Amt_Calculate_Day_Inc_Tot_Days = "0";
                                            if (EmpType == "2")
                                            {
                                                if (dtIncentive_Month.Rows.Count != 0)
                                                {

                                                    //Get NFH Working Days
                                                    DataTable NFH_Work_Days_DT = new DataTable();
                                                    query = "Select * from [" + SessionEpay + "]..AttenanceDetails where EmpNo='" + EmpNo + "' and " +
                                                         "Months='" + ddlMonth.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                         " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105)";
                                                    NFH_Work_Days_DT = objdata.RptEmployeeMultipleDetails(query);
                                                    string NFH_Worked_Days = "0";
                                                    if (NFH_Work_Days_DT.Rows.Count != 0) { NFH_Worked_Days = NFH_Work_Days_DT.Rows[0]["NFH_Work_Days_Incentive"].ToString(); }

                                                    //Eligible Total Days Count
                                                    if (dtIncentive_Month.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(EmployeeDays)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(cl)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(ThreeSided)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(NFh)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(NFH_Worked_Days)).ToString();
                                                    }
                                                    //Attn Total Days Count in Multiple Inct. Amount
                                                    if (dtIncentive_Month.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(EmployeeDays)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(cl)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(ThreeSided)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(NFh)).ToString();
                                                    }
                                                    if (dtIncentive_Month.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper())
                                                    {
                                                        Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(NFH_Worked_Days)).ToString();
                                                    }
                                                }
                                                if ((Convert.ToDecimal(Month_Total_days.ToString())) <= (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days.ToString())))
                                                {
                                                    if (dtIncentive_Month.Rows.Count != 0)
                                                    {
                                                        DayIncentive = dtIncentive_Month.Rows[0][0].ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }
                                                }
                                                else if ((Convert.ToDecimal(31) - Convert.ToDecimal(1)) <= (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days.ToString())))
                                                {
                                                    if (dtIncentive_Month.Rows.Count != 0)
                                                    {
                                                        DayIncentive = dtIncentive_Month.Rows[0][0].ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }
                                                }
                                                else
                                                {
                                                    if (Convert.ToDecimal(dtIncentive.Rows[0]["WorkerDays"].ToString()) <= (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days.ToString())))
                                                    {
                                                        DayIncentive = dtIncentive.Rows[0]["WorkerAmt"].ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }
                                                }
                                                if (Convert.ToDecimal(DayIncentive.ToString()) > 0)
                                                {

                                                    //Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(NFH_Worked_Days)).ToString();

                                                    DayIncentive = (Convert.ToDecimal(DayIncentive.ToString()) * (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days.ToString()))).ToString();
                                                    DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 2, MidpointRounding.ToEven)).ToString();
                                                }
                                            }
                                            if (EmpType == "2") //OT Days Amount
                                            {
                                                ThreeSided_Amt = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) * Convert.ToDecimal(ThreeSided.ToString())).ToString();
                                                ThreeSided_Amt = (Math.Round(Convert.ToDecimal(ThreeSided_Amt), 2, MidpointRounding.ToEven)).ToString();
                                            }
                                            if (EmpType == "2") //OT Hours Amount Calculate
                                            {
                                                OTHoursAmtNew_Str = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(8)).ToString();
                                                OTHoursAmtNew_Str = (Convert.ToDecimal(OTHoursAmtNew_Str.ToString()) * Convert.ToDecimal(OTHoursNew_Str.ToString())).ToString();
                                                OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 2, MidpointRounding.ToEven)).ToString();
                                            }
                                            //Sub-Staff Attn DayIncentive Calculate End
                                            Basic_For_SM = "0";
                                            Basic_For_SM = dt1.Rows[0]["BaseSalary"].ToString();
                                            //basicsalary = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) - Convert.ToDecimal(lop));
                                            if (EmpType == "1" || EmpType == "6")
                                            {
                                                basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(Basic_Spl_Allowance));
                                                //basicsalary = (Convert.ToDecimal(basicsalary) * Convert.ToDecimal(60));
                                                //basicsalary = (Convert.ToDecimal(basicsalary) / Convert.ToDecimal(100));
                                            }
                                            else
                                            {
                                                basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(Basic_Spl_Allowance));
                                                //basicsalary = (Convert.ToDecimal(basicsalary) * Convert.ToDecimal(60));
                                                //basicsalary = (Convert.ToDecimal(basicsalary) / Convert.ToDecimal(100));

                                                //if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(BasicPFDays))
                                                //{
                                                //    //basic1 = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) / Convert.ToDecimal(BasicPFDays)).ToString();
                                                //    //basicsalary = Convert.ToDecimal((Convert.ToDecimal(basic1) * (Convert.ToDecimal(BasicPFDays))));                                                    
                                                //    basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(EduAllow) + Convert.ToDecimal(MediAllow));
                                                //}
                                                //else
                                                //{
                                                //    //basicsalary = Convert.ToDecimal(basic);
                                                //    basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(EduAllow) + Convert.ToDecimal(MediAllow));
                                                //}
                                            }
                                            //basicsalary = Convert.ToDecimal(basic);

                                            if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                            {
                                                if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
                                                {

                                                    PF_salary = "1";
                                                    if (Convert.ToDecimal(basicsalary) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                                    {
                                                        PFS = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF

                                                        EmployeerPFTwo = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        //EmployeerPFTwo = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                    else
                                                    {
                                                        decimal PF_val = (Convert.ToDecimal(basicsalary));
                                                        PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF
                                                        EmployeerPFTwo = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        //EmployeerPFOne = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    PF_salary = "0";
                                                    //decimal PF_val = (Convert.ToDecimal(basicsalary));
                                                    //PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                    //PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    if (Convert.ToDecimal(basicsalary) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                                    {
                                                        PFS = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF

                                                        EmployeerPFTwo = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        //EmployeerPFTwo = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                    else
                                                    {
                                                        decimal PF_val = (Convert.ToDecimal(basicsalary));
                                                        PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF
                                                        EmployeerPFTwo = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        //EmployeerPFTwo = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()) + Convert.ToDecimal(PFS.ToString())).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }

                                                }
                                                //PFS = dt1.Rows[0]["PFS"].ToString();
                                                //decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                                //pfAmt = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                //pfAmt = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {
                                                PFS = "0";
                                                EmployeerPFOne = "0";
                                                EmployeerPFTwo = "0";
                                            }
                                            string ESI_Basic_Salary = "0.00";
                                            if (EmpType == "1" || EmpType == "6")
                                            {
                                                ESI_Basic_Salary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(Basic_Spl_Allowance) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(ConvAllow)).ToString();
                                                ESI_Basic_Salary = (Convert.ToDecimal(ESI_Basic_Salary) + Convert.ToDecimal(EduAllow) + Convert.ToDecimal(MediAllow)).ToString();
                                            }
                                            else
                                            {
                                                ESI_Basic_Salary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(Basic_Spl_Allowance) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(Basic_Vehicle_Maintenance)).ToString();
                                                ESI_Basic_Salary = (Convert.ToDecimal(ESI_Basic_Salary) + Convert.ToDecimal(Basic_Communication) + Convert.ToDecimal(Basic_Journ_Perid_Paper)).ToString();
                                            }
                                            Employee_ESI_IF = "0";
                                            Employeer_ESI_IF = "0";
                                            if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                            {
                                                decimal ESI_val = (Convert.ToDecimal(ESI_Basic_Salary));
                                                //decimal ESI_val = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA));

                                                ESI = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                                int myInt = (int)Math.Ceiling(Convert.ToDecimal(ESI));
                                                //ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                                ESI = (myInt).ToString();
                                                //string[] split_val = ESI.Split('.');
                                                //string Ist = split_val[0].ToString();
                                                //string IInd = split_val[1].ToString();

                                                //if (Convert.ToInt32(IInd) > 0)
                                                //{
                                                //    ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                //}
                                                //else
                                                //{
                                                //    ESI = Ist;
                                                //}

                                                //Employeer ESI
                                                EmployeerESI = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerESI"].ToString())) / 100).ToString();
                                                EmployeerESI = (Math.Round(Convert.ToDecimal(EmployeerESI), 2, MidpointRounding.ToEven)).ToString();
                                                //myInt = (int)Math.Ceiling(Convert.ToDecimal(EmployeerESI));
                                                //EmployeerESI = (myInt).ToString();
                                                Employee_ESI_IF = ESI;
                                                Employeer_ESI_IF = EmployeerESI;
                                            }
                                            else
                                            {
                                                ESI = "0";
                                                EmployeerESI = "0";
                                            }
                                            //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                            string Basic_Spilit_Tot = "0.00";
                                            Basic_Spilit_Tot = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(ConvAllow) + Convert.ToDecimal(EduAllow) + Convert.ToDecimal(MediAllow) + Convert.ToDecimal(BasicRAI) + Convert.ToDecimal(WashingAllow)).ToString();
                                            Basic_Spilit_Tot = (Convert.ToDecimal(Basic_Spilit_Tot) + Convert.ToDecimal(Basic_Spl_Allowance) + Convert.ToDecimal(Basic_Uniform) + Convert.ToDecimal(Basic_Vehicle_Maintenance) + Convert.ToDecimal(Basic_Communication) + Convert.ToDecimal(Basic_Journ_Perid_Paper) + Convert.ToDecimal(Basic_Incentives)).ToString();
                                            if (EmpType == "2")
                                            {
                                                TotalEarnings = (Convert.ToDecimal(Basic_Spilit_Tot) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(ThreeSided_Amt) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                            }
                                            else
                                            {
                                                TotalEarnings = (Convert.ToDecimal(Basic_Spilit_Tot) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5)).ToString();
                                            }
                                            //TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5)).ToString();

                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();

                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            Total_Deduction_IF = TotalDeductions;

                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            RoundOffNetPay = rounded1.ToString();

                                            Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";
                                        }
                                    }
                                    else //Labour Salary Calculation
                                    {

                                        AdvAmt = 0;
                                        ID = "";
                                        basicsalary = 0;
                                        Basic_For_SM = "0";
                                        Transport_Charges_Regular = "0";
                                        DataTable dt1 = new DataTable();
                                        Query = "select EmpNo,BaseSalary,'0' as FDA,'0' as VDA,'0' as HRA,'0' as Total,'0' as Unioncharges,Alllowance1,Alllowance2,Deduction1,Deduction2,PFS from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                                        dt1 = objdata.RptEmployeeMultipleDetails(Query);
                                        if (dt1.Rows.Count != 0)
                                        {
                                            if (dt1.Rows[0]["Alllowance1"].ToString() == "") { dt1.Rows[0]["Alllowance1"] = "0"; }
                                            if (dt1.Rows[0]["Alllowance2"].ToString() == "") { dt1.Rows[0]["Alllowance2"] = "0"; }
                                            if (dt1.Rows[0]["Deduction1"].ToString() == "") { dt1.Rows[0]["Deduction1"] = "0"; }
                                            if (dt1.Rows[0]["Deduction2"].ToString() == "") { dt1.Rows[0]["Deduction2"] = "0"; }
                                            if (dt1.Rows[0]["PFS"].ToString() == "") { dt1.Rows[0]["PFS"] = "0"; }
                                        }
                                        DataTable dt_con = new DataTable();
                                        DataTable dt_slab = new DataTable();
                                        ////saba Command Start
                                        //dt_con = objdata.Contract_SalaryCal(EmpNo, SessionCcode, SessionLcode);
                                        //if (dt_con.Rows.Count > 0)
                                        //{
                                        //    string BasicDays = dt_con.Rows[0]["BasicDays"].ToString();
                                        //    BasicDays = (Convert.ToDecimal(BasicDays) + Convert.ToDecimal(EmployeeDays)).ToString();

                                        //    dt_slab = objdata.Slab_cal_salary(dt_con.Rows[0]["ContractName"].ToString());
                                        //    string K_val = "0";
                                        //    for (int K = 0; K < dt_slab.Rows.Count; K++)
                                        //    {
                                        //        if ((Convert.ToDecimal(BasicDays) > Convert.ToDecimal(K_val)) && (Convert.ToDecimal(BasicDays) <= Convert.ToDecimal(dt_slab.Rows[K]["SlabName"].ToString())))
                                        //        {
                                        //            basic = (Convert.ToDecimal(dt_slab.Rows[K]["Amount"].ToString()) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                        //            break;
                                        //        }
                                        //        else
                                        //        {
                                        //            basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                        //        }
                                        //        K_val = dt_slab.Rows[K]["SlabName"].ToString();
                                        //    }

                                        //}
                                        //else
                                        //{
                                        //    basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                        //}
                                        ////saba Command
                                        // saba Change
                                        string basic1 = "0.00";
                                        Basic_For_SM = "0";
                                        Basic_For_SM = dt1.Rows[0]["BaseSalary"].ToString();
                                        basic1 = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString())).ToString();
                                        if (EmpType == "4")
                                        {
                                            //ThreeSided = "";
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh))).ToString();
                                        }
                                        else
                                        {
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh))).ToString();
                                            //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                        }

                                        //Basic Spilit
                                        BasicAndDA = "0.00";
                                        BasicHRA = "0.00";
                                        ConvAllow = "0.00";
                                        EduAllow = "0.00";
                                        MediAllow = "0.00";
                                        BasicRAI = "0.00";
                                        WashingAllow = "0.00";

                                        Basic_Uniform = "0.00";
                                        Basic_Spl_Allowance = "0.00";
                                        Basic_Vehicle_Maintenance = "0.00";
                                        Basic_Communication = "0.00";
                                        Basic_Journ_Perid_Paper = "0.00";
                                        Basic_Incentives = "0.00";

                                        DataTable dtBasicPercent = new DataTable();
                                        Query = "";
                                        Query = Query + "Select * from [" + SessionEpay + "]..MstBasicDet where Ccode='" + SessionCcode + "' and  Lcode='" + SessionLcode + "' And Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + EmpType.ToString() + "'";
                                        dtBasicPercent = objdata.RptEmployeeMultipleDetails(Query);

                                        if (EmpType == "3" || EmpType == "4")
                                        {
                                            //Basic Spilit
                                            if (Convert.ToDecimal(basic) != 0)
                                            {
                                                ////Basic And DA
                                                //BasicAndDA = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["BasicDA"].ToString())) / 100).ToString();
                                                //BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 2, MidpointRounding.AwayFromZero)).ToString();
                                                ////Basic HRA
                                                //BasicHRA = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["HRA"].ToString())) / 100).ToString();
                                                //BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 2, MidpointRounding.AwayFromZero)).ToString();
                                                ////ConvAllow
                                                //ConvAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["ConvAllow"].ToString())) / 100).ToString();
                                                //ConvAllow = (Math.Round(Convert.ToDecimal(ConvAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                ////EduAllow
                                                //EduAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["EduAllow"].ToString())) / 100).ToString();
                                                //EduAllow = (Math.Round(Convert.ToDecimal(EduAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                ////MediAllow
                                                //MediAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["MediAllow"].ToString())) / 100).ToString();
                                                //MediAllow = (Math.Round(Convert.ToDecimal(MediAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                ////BasicRAI
                                                //BasicRAI = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["RAI"].ToString())) / 100).ToString();
                                                //BasicRAI = (Math.Round(Convert.ToDecimal(BasicRAI), 2, MidpointRounding.AwayFromZero)).ToString();
                                                ////WashingAllow
                                                //WashingAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["WashingAllow"].ToString())) / 100).ToString();
                                                //WashingAllow = (Math.Round(Convert.ToDecimal(WashingAllow), 2, MidpointRounding.AwayFromZero)).ToString();



                                                //Basic And DA
                                                BasicAndDA = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["BasicDA"].ToString())) / 100).ToString();
                                                BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Spl_Allowance
                                                Basic_Spl_Allowance = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Spl_Allowance"].ToString())) / 100).ToString();
                                                Basic_Spl_Allowance = (Math.Round(Convert.ToDecimal(Basic_Spl_Allowance), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic HRA
                                                BasicHRA = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["HRA"].ToString())) / 100).ToString();
                                                BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Uniform
                                                Basic_Uniform = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Uniform"].ToString())) / 100).ToString();
                                                Basic_Uniform = (Math.Round(Convert.ToDecimal(Basic_Uniform), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Vehicle_Maintenance
                                                Basic_Vehicle_Maintenance = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Vehicle_Maintenance"].ToString())) / 100).ToString();
                                                Basic_Vehicle_Maintenance = (Math.Round(Convert.ToDecimal(Basic_Vehicle_Maintenance), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Communication
                                                Basic_Communication = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Communication"].ToString())) / 100).ToString();
                                                Basic_Communication = (Math.Round(Convert.ToDecimal(Basic_Communication), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Journ_Perid_Paper
                                                Basic_Journ_Perid_Paper = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Journ_Perid_Paper"].ToString())) / 100).ToString();
                                                Basic_Journ_Perid_Paper = (Math.Round(Convert.ToDecimal(Basic_Journ_Perid_Paper), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //ConvAllow
                                                ConvAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["ConvAllow"].ToString())) / 100).ToString();
                                                ConvAllow = (Math.Round(Convert.ToDecimal(ConvAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //EduAllow
                                                EduAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["EduAllow"].ToString())) / 100).ToString();
                                                EduAllow = (Math.Round(Convert.ToDecimal(EduAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //MediAllow
                                                MediAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["MediAllow"].ToString())) / 100).ToString();
                                                MediAllow = (Math.Round(Convert.ToDecimal(MediAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //BasicRAI
                                                BasicRAI = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["RAI"].ToString())) / 100).ToString();
                                                BasicRAI = (Math.Round(Convert.ToDecimal(BasicRAI), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //WashingAllow
                                                WashingAllow = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["WashingAllow"].ToString())) / 100).ToString();
                                                WashingAllow = (Math.Round(Convert.ToDecimal(WashingAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //Basic_Incentives
                                                Basic_Incentives = ((Convert.ToDecimal(basic.ToString()) * Convert.ToDecimal(dtBasicPercent.Rows[0]["Incentives"].ToString())) / 100).ToString();
                                                Basic_Incentives = (Math.Round(Convert.ToDecimal(Basic_Incentives), 2, MidpointRounding.AwayFromZero)).ToString();

                                            }
                                            else
                                            {
                                                BasicAndDA = "0.00";
                                                BasicHRA = "0.00";
                                                ConvAllow = "0.00";
                                                EduAllow = "0.00";
                                                MediAllow = "0.00";
                                                BasicRAI = "0.00";
                                                WashingAllow = "0.00";

                                                Basic_Uniform = "0.00";
                                                Basic_Spl_Allowance = "0.00";
                                                Basic_Vehicle_Maintenance = "0.00";
                                                Basic_Communication = "0.00";
                                                Basic_Journ_Perid_Paper = "0.00";
                                                Basic_Incentives = "0.00";
                                            }
                                        }

                                        //Hostel Basic Details Get
                                        string query = "";
                                        string basic_Check_Hostel = "0";
                                        basic_Check_Hostel = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString())).ToString();
                                        basic_Check_Hostel = (Math.Round(Convert.ToDecimal(basic_Check_Hostel), 2, MidpointRounding.AwayFromZero)).ToString();

                                        //query = "Select * from MstBasicDetHostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and TotalAmt='" + basic_Check_Hostel + "'";
                                        //DataTable dt_Hostel = new DataTable();
                                        //SqlCommand cmd = new SqlCommand(query, con);
                                        //SqlDataAdapter sda = new SqlDataAdapter(cmd);
                                        //con.Open();
                                        //sda.Fill(dt_Hostel);
                                        //con.Close();
                                        //Hostel Basic Details Spilit
                                        //if (EmpType == "4")
                                        //{
                                        //    //Basic Spilit
                                        //    if (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) != 0 && dt_Hostel.Rows.Count != 0)
                                        //    {
                                        //        //Basic And DA
                                        //        BasicAndDA = ((Convert.ToDecimal(dt_Hostel.Rows[0]["BasicAndDA"].ToString())) * (Convert.ToDecimal(EmployeeDays.ToString()) + Convert.ToDecimal(NFh.ToString()))).ToString(); //+ Convert.ToDecimal(ThreeSided.ToString()))).ToString();
                                        //        BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 2, MidpointRounding.AwayFromZero)).ToString();
                                        //        //Basic HRA
                                        //        BasicHRA = ((Convert.ToDecimal(dt_Hostel.Rows[0]["HRA"].ToString())) * (Convert.ToDecimal(EmployeeDays.ToString()) + Convert.ToDecimal(NFh.ToString()))).ToString(); //+ Convert.ToDecimal(ThreeSided.ToString()))).ToString();
                                        //        BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 2, MidpointRounding.AwayFromZero)).ToString();
                                        //        //ConvAllow
                                        //        ConvAllow = ((Convert.ToDecimal(dt_Hostel.Rows[0]["ConvAllow"].ToString())) * (Convert.ToDecimal(EmployeeDays.ToString()) + Convert.ToDecimal(NFh.ToString()))).ToString(); //+ Convert.ToDecimal(ThreeSided.ToString()))).ToString();
                                        //        ConvAllow = (Math.Round(Convert.ToDecimal(ConvAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                        //        //EduAllow
                                        //        EduAllow = "0.00";

                                        //        //MediAllow
                                        //        MediAllow = "0.0";

                                        //        //BasicRAI
                                        //        BasicRAI = "0.0";

                                        //        //WashingAllow
                                        //        WashingAllow = ((Convert.ToDecimal(dt_Hostel.Rows[0]["WashingAllow"].ToString())) * (Convert.ToDecimal(EmployeeDays.ToString()) + Convert.ToDecimal(NFh.ToString()))).ToString(); //+ Convert.ToDecimal(ThreeSided.ToString()))).ToString();
                                        //        WashingAllow = (Math.Round(Convert.ToDecimal(WashingAllow), 2, MidpointRounding.AwayFromZero)).ToString();
                                        //    }
                                        //    else
                                        //    {
                                        //        BasicAndDA = "0.00";
                                        //        BasicHRA = "0.00";
                                        //        ConvAllow = "0.00";
                                        //        EduAllow = "0.00";
                                        //        MediAllow = "0.00";
                                        //        BasicRAI = "0.00";
                                        //        WashingAllow = "0.00";
                                        //    }
                                        //}

                                        //saba End
                                        DataTable dteligible = new DataTable();
                                        DataTable dtpf = new DataTable();

                                        Query = "";
                                        Query = Query + "Select PF_per,ESI_per,StaffSalary,StampCg,VDA1,VDA2,Union_val,Spinning,DayIncentive,";
                                        Query = Query + "halfNight,FullNight,ThreeSideAmt,EmployeerPFone,EmployeerPFTwo,EmployeerESI from [" + SessionEpay + "]..MstESIPF where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        dtpf = objdata.RptEmployeeMultipleDetails(Query);

                                        Query = "";
                                        Query = Query + "Select (Eligible_ESI)as ElgibleESI,(Eligible_PF) as EligiblePF,'0' as PF_Type from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                                        dteligible = objdata.RptEmployeeMultipleDetails(Query);
                                        //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                        //if (dtpf.Rows[0]["Union_val"].ToString() == "1")
                                        //{
                                        //    txtunion.Text = dt1.Rows[0]["Unioncharges"].ToString();
                                        //}
                                        //else
                                        //{
                                        //    txtunion.Text = "0";
                                        //}

                                        if (dt1.Rows.Count > 0)
                                        {


                                            //string basic1 = "0.00";                                               

                                            basic = (Math.Round(Convert.ToDecimal(basic), 2, MidpointRounding.AwayFromZero)).ToString();

                                            if (EmpType == "3" || EmpType == "4")
                                            {
                                                DataTable dt_SPGINC = new DataTable();
                                                DataTable DT_Emp_Q = new DataTable();
                                                string SPGIN_AllAmt = "0";

                                                Query = "";
                                                Query = Query + "Select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And MachineID='" + EmpNo + "'";
                                                DT_Emp_Q = objdata.RptEmployeeMultipleDetails(Query);
                                                if (DT_Emp_Q.Rows.Count != 0)
                                                {
                                                    Query = "";
                                                    Query = Query + "Select * from Incentive_Mst where DeptName='" + DT_Emp_Q.Rows[0]["DeptName"].ToString() + "' and Designation='" + DT_Emp_Q.Rows[0]["Designation"].ToString() + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                    Query = Query + " And Wages='" + DT_Emp_Q.Rows[0]["Wages"].ToString() + "'";
                                                    dt_SPGINC = objdata.RptEmployeeMultipleDetails(Query);
                                                    if (dt_SPGINC.Rows.Count != 0)
                                                    {
                                                        if (dt_SPGINC.Rows[0]["IncAmt"].ToString() != "")
                                                        {
                                                            SPGIN_AllAmt = dt_SPGINC.Rows[0]["IncAmt"].ToString();
                                                        }
                                                        else
                                                        {
                                                            SPGIN_AllAmt = "0";
                                                        }

                                                    }
                                                    else
                                                    {
                                                        SPGIN_AllAmt = dt1.Rows[0]["Alllowance1"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    SPGIN_AllAmt = dt1.Rows[0]["Alllowance1"].ToString();
                                                }


                                                All1 = ((Convert.ToDecimal(FullNight)) * Convert.ToDecimal(SPGIN_AllAmt)).ToString();
                                                All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //All1 = ((Convert.ToDecimal(FullNight)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1"].ToString())).ToString();
                                                //All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {

                                                All1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1"].ToString())).ToString();
                                                All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }

                                            if (EmpType == "3" || EmpType == "4")
                                            {
                                                All2 = (Convert.ToDecimal(dt1.Rows[0]["Alllowance2"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh) + Convert.ToDecimal(ThreeSided) + Convert.ToDecimal(NFH_Work_Days_Allow_Days))).ToString();
                                                All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {

                                                //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                                All2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance2"].ToString())).ToString();
                                                All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                                //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            }

                                            //Canteen Deduction for Hostel Girls
                                            if (EmpType == "4")
                                            {
                                                //get canteen master amt
                                                string Canteen_Master_Amt = "0";
                                                string query_can = "";
                                                query_can = "Select * from MstCanteenAmtDet where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                                DataTable DT_Can = new DataTable();
                                                DT_Can = objdata.RptEmployeeMultipleDetails(query_can);
                                                if (DT_Can.Rows.Count != 0)
                                                {
                                                    Canteen_Master_Amt = DT_Can.Rows[0]["CanteenAmt"].ToString();
                                                }
                                                else
                                                {
                                                    Canteen_Master_Amt = "0";
                                                }

                                                string Candeen_Full_Month_Amt = "0.00"; string Candeen_Minus_Amt = "0.00";
                                                string Candeen_Final_Amt = "0.00";
                                                //Candeen_Full_Month_Amt = ((Convert.ToDecimal(Work)) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                                Candeen_Full_Month_Amt = ((Convert.ToDecimal(Work)) * Convert.ToDecimal(Canteen_Master_Amt.ToString())).ToString();
                                                //Candeen_Minus_Amt = ((Convert.ToDecimal(Home)) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                                Candeen_Minus_Amt = ((Convert.ToDecimal(Home)) * Convert.ToDecimal(Canteen_Master_Amt.ToString())).ToString();
                                                Candeen_Final_Amt = ((Convert.ToDecimal(Candeen_Full_Month_Amt)) - Convert.ToDecimal(Candeen_Minus_Amt)).ToString();
                                                Ded1 = (Convert.ToDecimal(Candeen_Final_Amt)).ToString();
                                                Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {
                                                Ded1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                                Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            }

                                            //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //lop = ((Convert.ToDecimal(basic) / 30) * Convert.ToDecimal(lopdays)).ToString();
                                            lop = "0.00";
                                            //txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //if (EmpType == "3") //Regular PF Salary Sum
                                            //{
                                            //    if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(BasicPFDays))
                                            //    {
                                            //        basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicRAI));
                                            //    }
                                            //    else
                                            //    {
                                            //        basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicRAI));
                                            //    }
                                            //}

                                            //if (EmpType == "4") //Regular PF Salary Sum
                                            //{
                                            //    if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(BasicPFDays))
                                            //    {
                                            //        basicsalary = (Convert.ToDecimal(BasicAndDA));
                                            //    }
                                            //    else
                                            //    {
                                            //        basicsalary = (Convert.ToDecimal(BasicAndDA));
                                            //    }
                                            //}

                                            //if (EmpType == "4") //Regular PF Salary Sum
                                            //{
                                            //    if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(BasicPFDays))
                                            //    {
                                            //        basicsalary = (Convert.ToDecimal(BasicAndDA));
                                            //    }
                                            //    else
                                            //    {
                                            //        basicsalary = (Convert.ToDecimal(BasicAndDA));
                                            //    }
                                            //}

                                            if (EmpType == "3" || EmpType == "4") //Regular PF Salary Sum
                                            {
                                                basicsalary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(Basic_Spl_Allowance));
                                            }

                                            if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                            {
                                                if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
                                                {
                                                    PF_salary = "1";
                                                    if (Convert.ToDecimal(basicsalary) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                                    {
                                                        PFS = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF
                                                        EmployeerPFTwo = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                    else
                                                    {
                                                        decimal PF_val = (Convert.ToDecimal(basicsalary));
                                                        PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF
                                                        EmployeerPFTwo = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    PF_salary = "0";
                                                    //decimal PF_val = (Convert.ToDecimal(basicsalary));
                                                    //PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                    //PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    if (Convert.ToDecimal(basicsalary) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                                    {
                                                        PFS = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF
                                                        EmployeerPFTwo = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                    else
                                                    {
                                                        decimal PF_val = (Convert.ToDecimal(basicsalary));
                                                        PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                        PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //Employeer PF
                                                        EmployeerPFTwo = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFTwo"].ToString())) / 100).ToString();
                                                        EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //EmployeerPFOne = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                                        EmployeerPFOne = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFTwo)).ToString();
                                                        EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                }
                                                //PF_salary = "0";
                                                //PFS = dt1.Rows[0]["PFS"].ToString();
                                                //decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                                //pfAmt = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                                //pfAmt = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {
                                                PFS = "0";
                                                EmployeerPFOne = "0";
                                                EmployeerPFTwo = "0";
                                            }

                                            HalfNight_Amt = "0.00";
                                            FullNight_Amt = "0.00";
                                            DayIncentive = "0.00";
                                            SpinningAmt = "0.00";
                                            ThreeSided_Amt = "0.00";
                                            if (EmpType == "3" || EmpType == "4") //OT Days Amount
                                            {
                                                string OT_Base = (Convert.ToDecimal(dt1.Rows[0]["Alllowance2"].ToString()) + Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString())).ToString();
                                                ThreeSided_Amt = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) * Convert.ToDecimal(ThreeSided.ToString())).ToString();
                                                //ThreeSided_Amt = (Convert.ToDecimal(OT_Base) * Convert.ToDecimal(ThreeSided.ToString())).ToString();
                                                ThreeSided_Amt = (Math.Round(Convert.ToDecimal(ThreeSided_Amt), 2, MidpointRounding.ToEven)).ToString();
                                            }

                                            //Regular Labour Full Night Incentive Calculate
                                            //if (EmpType == "3")
                                            //{
                                            //    FullNight_Amt = (Convert.ToDecimal(FullNight.ToString()) * Convert.ToDecimal("15".ToString())).ToString();
                                            //    FullNight_Amt = (Math.Round(Convert.ToDecimal(FullNight_Amt), 2, MidpointRounding.ToEven)).ToString();
                                            //}
                                            //if (EmpType == "4")
                                            //{
                                            //    FullNight_Amt = (Convert.ToDecimal(FullNight.ToString()) * Convert.ToDecimal("20".ToString())).ToString();
                                            //    FullNight_Amt = (Math.Round(Convert.ToDecimal(FullNight_Amt), 2, MidpointRounding.ToEven)).ToString();
                                            //}


                                            //Regular Labour DayIncentive Calculate
                                            DataTable dtIncentive = new DataTable();
                                            DataTable dtIncentive_Month = new DataTable();
                                            Query = "";
                                            Query = Query + "Select Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays from [" + SessionEpay + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' ";
                                            Query = Query + "and Lcode='" + SessionLcode + "' And MonthDays='" + Month_Total_days.ToString() + "' order by WorkerDays Asc";
                                            dtIncentive = objdata.RptEmployeeMultipleDetails(Query);

                                            Query = "";
                                            Query = Query + "Select Amt,WDays,HAllowed,OTDays,NFHDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,NFHWorkDays,CalNFHWorkDays ";
                                            Query = Query + "from [" + SessionEpay + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' order by Amt Desc ";
                                            dtIncentive_Month = objdata.RptEmployeeMultipleDetails(Query);

                                            string Eligible_Day_Incnt_Tot_Days = "0";
                                            string Amt_Calculate_Day_Inc_Tot_Days = "0";
                                            string WagesType_IF = "";
                                            if (SessionUserType == "2")
                                            {
                                                WagesType_IF = "2";
                                            }
                                            else
                                            {
                                                WagesType_IF = "2";
                                            }
                                            if (EmpType == "3" && WagesType_IF.ToString() == "2")
                                            {
                                                if (EmpNo == "REG10201410136")
                                                {
                                                    EmpNo = EmpNo;
                                                }
                                                //Get NFH Working Days
                                                DataTable NFH_Work_Days_DT = new DataTable();
                                                query = "Select * from [" + SessionEpay + "]..AttenanceDetails where EmpNo='" + EmpNo + "' and " +
                                                     " Months='" + ddlMonth.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                     " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105)";
                                                NFH_Work_Days_DT = objdata.RptEmployeeMultipleDetails(query);

                                                string NFH_Worked_Days = "0";

                                                if (NFH_Work_Days_DT.Rows.Count != 0) { NFH_Worked_Days = NFH_Work_Days_DT.Rows[0]["NFH_Work_Days_Incentive"].ToString(); }

                                                //Eligible Total Days Count
                                                if (dtIncentive_Month.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(EmployeeDays)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(cl)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(ThreeSided)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(NFh)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Eligible_Day_Incnt_Tot_Days = (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days) + Convert.ToDecimal(NFH_Worked_Days)).ToString();
                                                }
                                                //Attn Total Days Count in Multiple Inct. Amount
                                                if (dtIncentive_Month.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(EmployeeDays)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(cl)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(ThreeSided)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(NFh)).ToString();
                                                }
                                                if (dtIncentive_Month.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper())
                                                {
                                                    Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(NFH_Worked_Days)).ToString();
                                                }
                                                //
                                                if ((Convert.ToDecimal(Month_Total_days.ToString())) <= (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days.ToString())))
                                                {
                                                    if (dtIncentive_Month.Rows.Count != 0)
                                                    {
                                                        DayIncentive = dtIncentive_Month.Rows[0][0].ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }
                                                }
                                                else if ((Convert.ToDecimal(31) - Convert.ToDecimal(1)) <= (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days.ToString())))
                                                {
                                                    if (dtIncentive_Month.Rows.Count != 0)
                                                    {
                                                        DayIncentive = dtIncentive_Month.Rows[0][0].ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }
                                                }
                                                else
                                                {
                                                    if (Convert.ToDecimal(dtIncentive.Rows[0]["WorkerDays"].ToString()) <= (Convert.ToDecimal(Eligible_Day_Incnt_Tot_Days.ToString())))
                                                    {
                                                        DayIncentive = dtIncentive.Rows[0]["WorkerAmt"].ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }
                                                }
                                                Transport_Charges_Regular = "0";
                                                if (Convert.ToDecimal(DayIncentive.ToString()) > 0)
                                                {
                                                    //Amt_Calculate_Day_Inc_Tot_Days = (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days) + Convert.ToDecimal(NFH_Worked_Days)).ToString();

                                                    DayIncentive = (Convert.ToDecimal(DayIncentive.ToString()) * (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days.ToString()))).ToString();
                                                    DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 2, MidpointRounding.ToEven)).ToString();

                                                    //Travel Allowance Calculate
                                                    DataTable DT_Travel = new DataTable();
                                                    string Travel_Amt_Perday = "0";
                                                    query = "Select * from MstTravel_Allowance_AmtDet where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                                    DT_Travel = objdata.RptEmployeeMultipleDetails(query);
                                                    if (DT_Travel.Rows.Count != 0)
                                                    {
                                                        Travel_Amt_Perday = DT_Travel.Rows[0]["Travel_Allown_Amt"].ToString();
                                                    }
                                                    query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Vehicles_Type='OWN Bus' And ExistingCode='" + ExistNo + "' And EmpNo='" + EmpNo + "'";
                                                    DataTable DT_Tr = new DataTable();
                                                    DT_Tr = objdata.RptEmployeeMultipleDetails(query);
                                                    if (DT_Tr.Rows.Count != 0)
                                                    {
                                                        Transport_Charges_Regular = (Convert.ToDecimal(Travel_Amt_Perday.ToString()) * (Convert.ToDecimal(Amt_Calculate_Day_Inc_Tot_Days.ToString()))).ToString();
                                                        Transport_Charges_Regular = (Math.Round(Convert.ToDecimal(Transport_Charges_Regular), 2, MidpointRounding.ToEven)).ToString();
                                                    }
                                                    //
                                                }
                                            }

                                            //OT Hours Amt calculate    ....Tomorrow Continue...
                                            if (EmpType == "5")
                                            {
                                                OTHoursAmtNew_Str = (Convert.ToDecimal(dt1.Rows[0]["BaseSalary"].ToString()) / Convert.ToDecimal(8)).ToString();
                                                OTHoursAmtNew_Str = (Convert.ToDecimal(OTHoursAmtNew_Str.ToString()) * Convert.ToDecimal(OTHoursNew_Str.ToString())).ToString();
                                                OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 2, MidpointRounding.ToEven)).ToString();
                                            }

                                            //Civil 4th Week Day Attn. Incentive
                                            if (EmpType == "5")
                                            {
                                                if (ChkCivilIncentive.Checked == true)
                                                {
                                                    string DelCivil = "Delete from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                         "Month='" + ddlMonth.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                         " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105)";
                                                    con.Open();
                                                    SqlCommand cmd_verifyCivil1 = new SqlCommand(DelCivil, con);
                                                    cmd_verifyCivil1.ExecuteNonQuery();

                                                    //query = "Select sum(WorkedDays) as CWorkDays from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                    //"Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Month='" + ddlMonth.Text + "' and " +
                                                    //"FinancialYear='" + ddlFinYear.SelectedValue + "' and convert(datetime,FromDate,105) >= convert(datetime,'" + txtIstWeekDate.Text + "',105) and convert(datetime, FromDate, 105) <= convert(datetime, '" + txtLastWeekDate.Text + "', 105)";

                                                    //eAlert Civil Incentive Days
                                                    query = "Select Incent_Days as CWorkDays from [" + SessionEpay + "]..eAlert_Civil_Incent_Days_Det where MachineID='" + EmpNo + "' and " +
                                                    "Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Month='" + ddlMonth.Text + "' and " +
                                                    "FinancialYear='" + ddlFinYear.SelectedValue + "' and convert(datetime,FromDate,105) = convert(datetime,'" + txtIstWeekDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtLastWeekDate.Text + "', 105)";

                                                    DataTable dt_Civil = new DataTable();
                                                    SqlCommand cmd1 = new SqlCommand(query, con);
                                                    SqlDataAdapter sda1 = new SqlDataAdapter(cmd1);
                                                    sda1.Fill(dt_Civil);

                                                    //Get Civil Incentive Details
                                                    query = "Select * from [" + SessionEpay + "]..CivilIncentive where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                                    DataTable dt_CivilIncentive = new DataTable();
                                                    SqlCommand cmd_CivilIncen = new SqlCommand(query, con);
                                                    SqlDataAdapter sda_CivilIncen = new SqlDataAdapter(cmd_CivilIncen);
                                                    sda_CivilIncen.Fill(dt_CivilIncentive);
                                                    con.Close();
                                                    string Civil_Eligible_Days = "0";
                                                    string Civil_Incentive_Amount = "0.0";
                                                    if (dt_CivilIncentive.Rows.Count != 0)
                                                    {
                                                        Civil_Eligible_Days = dt_CivilIncentive.Rows[0]["ElbDays"].ToString();
                                                        Civil_Incentive_Amount = dt_CivilIncentive.Rows[0]["Amt"].ToString();
                                                    }

                                                    string Current_Worked_Days = "0";
                                                    //if (dt_Civil.Rows.Count != 0) { Current_Worked_Days = (Convert.ToDecimal(EmployeeDays.ToString()) + Convert.ToDecimal(dt_Civil.Rows[0]["CWorkDays"].ToString())).ToString(); }
                                                    if (dt_Civil.Rows.Count != 0) { Current_Worked_Days = (Convert.ToDecimal(0) + Convert.ToDecimal(dt_Civil.Rows[0]["CWorkDays"].ToString())).ToString(); }
                                                    if (Convert.ToDecimal(Civil_Eligible_Days) <= Convert.ToDecimal(Current_Worked_Days))
                                                    {
                                                        //string Current_Worked_Days = (Convert.ToDecimal(EmployeeDays.ToString()) + Convert.ToDecimal(dt_Civil.Rows[0]["CWorkDays"].ToString())).ToString();
                                                        DayIncentive = (Convert.ToDecimal(Civil_Incentive_Amount) * (Convert.ToDecimal(Current_Worked_Days.ToString()))).ToString();
                                                        DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 2, MidpointRounding.ToEven)).ToString();
                                                    }
                                                    else
                                                    {
                                                        DayIncentive = "0.00";
                                                    }

                                                }
                                                else
                                                {
                                                    DayIncentive = "0.00";
                                                }
                                            }

                                            string ESI_Basic_Salary = "0.00";
                                            Employee_ESI_IF = "0";
                                            Employeer_ESI_IF = "0";
                                            if (EmpType == "3" || EmpType == "4")
                                            {
                                                ESI_Basic_Salary = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(Basic_Spl_Allowance) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(Basic_Uniform)).ToString();
                                            }
                                            if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                            {
                                                decimal ESI_val = (Convert.ToDecimal(ESI_Basic_Salary));
                                                //decimal ESI_val = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA));
                                                ESI = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                                //ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                                int myInt = (int)Math.Ceiling(Convert.ToDecimal(ESI));
                                                ESI = (myInt).ToString();

                                                //string[] split_val = ESI.Split('.');
                                                //string Ist = split_val[0].ToString();
                                                //string IInd = split_val[1].ToString();

                                                //if (Convert.ToInt32(IInd) > 0)
                                                //{
                                                //    ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                //}
                                                //else
                                                //{
                                                //    ESI = Ist;
                                                //}

                                                //Employeer ESI
                                                EmployeerESI = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerESI"].ToString())) / 100).ToString();
                                                EmployeerESI = (Math.Round(Convert.ToDecimal(EmployeerESI), 2, MidpointRounding.ToEven)).ToString();
                                                //myInt = (int)Math.Ceiling(Convert.ToDecimal(EmployeerESI));
                                                //EmployeerESI = (myInt).ToString();

                                                if (EmpType == "3")
                                                {
                                                    //OT Amt Add ESI for IF Employee...
                                                    query = "Select * from Employee_Mst where MachineID='" + EmpNo + "' And CompCode='" + SessionCcode + "'";
                                                    query = query + " And LocCode='" + SessionLcode + "' And FLOOR((CAST (CONVERT(Datetime,'" + Adolescent_Date_Str + "',103) AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25) >= 18";
                                                    DataTable DT_ES_IF = new DataTable();
                                                    DT_ES_IF = objdata.RptEmployeeMultipleDetails(query);
                                                    if (DT_ES_IF.Rows.Count != 0)
                                                    {
                                                        decimal ESI_val_IF = (Convert.ToDecimal(ESI_Basic_Salary) + Convert.ToDecimal(ThreeSided_Amt));
                                                        Employee_ESI_IF = ((ESI_val_IF * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                                        int myInt_IF = (int)Math.Ceiling(Convert.ToDecimal(Employee_ESI_IF));
                                                        Employee_ESI_IF = (myInt_IF).ToString();

                                                        //Employeer ESI
                                                        Employeer_ESI_IF = ((ESI_val_IF * Convert.ToDecimal(dtpf.Rows[0]["EmployeerESI"].ToString())) / 100).ToString();
                                                        Employeer_ESI_IF = (Math.Round(Convert.ToDecimal(Employeer_ESI_IF), 2, MidpointRounding.ToEven)).ToString();
                                                    }
                                                    else
                                                    {
                                                        Employee_ESI_IF = ESI;
                                                        Employeer_ESI_IF = EmployeerESI;
                                                    }
                                                }
                                                else
                                                {
                                                    Employee_ESI_IF = ESI;
                                                    Employeer_ESI_IF = EmployeerESI;
                                                }

                                            }
                                            else
                                            {
                                                ESI = "0";
                                                EmployeerESI = "0";
                                            }
                                            stamp = "0";
                                            DataTable dt_OT = new DataTable();
                                            string dt_ot1 = "";
                                            Query = "";
                                            Query = Query + "Select netAmount from [" + SessionEpay + "]..OverTime where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Month='" + Mont + "' and ";
                                            Query = Query + "FromDate=convert(Datetime, '" + MyDate1 + "', 105) and ToDate = Convert(Datetime, '" + MyDate2 + "', 105)";
                                            dt_OT = objdata.RptEmployeeMultipleDetails(Query);
                                            if (dt_OT.Rows.Count > 0) { dt_ot1 = dt_OT.Rows[0]["netAmount"].ToString(); }
                                            if (dt_ot1.Trim() != "")
                                            {
                                                OTDays = dt_ot1.ToString();
                                            }
                                            else
                                            {
                                                OTDays = "0";
                                            }
                                            if (EmpType != "5")
                                            {
                                                string Basic_Spilit_Sum = "0.00";
                                                Basic_Spilit_Sum = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(ConvAllow) + Convert.ToDecimal(EduAllow) + Convert.ToDecimal(MediAllow) + Convert.ToDecimal(BasicRAI) + Convert.ToDecimal(WashingAllow)).ToString();
                                                Basic_Spilit_Sum = (Convert.ToDecimal(Basic_Spilit_Sum) + Convert.ToDecimal(Basic_Spl_Allowance) + Convert.ToDecimal(Basic_Uniform) + Convert.ToDecimal(Basic_Vehicle_Maintenance) + Convert.ToDecimal(Basic_Communication) + Convert.ToDecimal(Basic_Journ_Perid_Paper) + Convert.ToDecimal(Basic_Incentives)).ToString();
                                                TotalEarnings = (Convert.ToDecimal(Basic_Spilit_Sum) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(ThreeSided_Amt)).ToString();
                                            }
                                            else
                                            {
                                                TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                            }
                                            //TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(DayIncentive)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();


                                            //IF Deduction Change
                                            //OT Amt Add ESI for IF Employee...
                                            query = "Select * from Employee_Mst where MachineID='" + EmpNo + "' And CompCode='" + SessionCcode + "' And Eligible_ESI='1'";
                                            query = query + " And LocCode='" + SessionLcode + "' And FLOOR((CAST (CONVERT(Datetime,'" + Adolescent_Date_Str + "',103) AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25) >= 18";
                                            DataTable DT_Dd_IF = new DataTable();
                                            DT_Dd_IF = objdata.RptEmployeeMultipleDetails(query);
                                            bool IF_Checking = false;
                                            if (DT_Dd_IF.Rows.Count != 0)
                                            {
                                                IF_Checking = true;
                                                Total_Deduction_IF = (Convert.ToDecimal(PFS) + Convert.ToDecimal(Employee_ESI_IF) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                                Total_Deduction_IF = (Math.Round(Convert.ToDecimal(Total_Deduction_IF), 2, MidpointRounding.ToEven)).ToString();
                                            }
                                            else
                                            {
                                                IF_Checking = false;
                                                Total_Deduction_IF = TotalDeductions;
                                            }

                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            RoundOffNetPay = rounded1.ToString();

                                            if (IF_Checking == true)
                                            {
                                                string NetPay_1 = "0";
                                                NetPay_1 = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(Total_Deduction_IF)).ToString();
                                                NetPay_1 = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                                if (Convert.ToDecimal(NetPay) < 0)
                                                {
                                                    NetPay = "0";
                                                }
                                                double d12 = Convert.ToDouble(NetPay.ToString());
                                                int rounded12 = ((int)((d12 + 5) / 10)) * 10;  // Output is 20
                                                RoundOffNetPay_IF = rounded12.ToString();
                                            }
                                            else
                                            {
                                                RoundOffNetPay_IF = RoundOffNetPay;
                                            }

                                            Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";

                                        }
                                    }
                                }

                                if (!ErrVerify)
                                {
                                    string pfEligible = "";
                                    Query = "";
                                    Query = Query + "Select Eligible_PF from Employee_Mst where EmpNo='" + EmpNo + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                    dt_Others = objdata.RptEmployeeMultipleDetails(Query);
                                    if (dt_Others.Rows.Count > 0) { pfEligible = dt_Others.Rows[0]["Eligible_PF"].ToString(); }
                                    //pfEligible = objdata.PF_Eligible_Salary(EmpNo, SessionCcode, SessionLcode);
                                    DateTime MyDate = new DateTime();
                                    objSal.TotalWorkingDays = Work;
                                    objSal.NFh = NFh;
                                    objSal.Lcode = SessionLcode;
                                    objSal.Ccode = SessionCcode;
                                    objSal.ApplyLeaveDays = lopdays;
                                    objSal.GrossEarnings = TotalEarnings;
                                    objSal.TotalDeduction = TotalDeductions;
                                    objSal.NetPay = NetPay;
                                    objSal.RoundOffNetPay = RoundOffNetPay;
                                    objSal.Words = Words;
                                    objSal.AvailableDays = EmployeeDays;
                                    objSal.SalaryDate = txtTransDate.Text;
                                    objSal.work = EmployeeDays;
                                    objSal.weekoff = WeekOff;
                                    objSal.CL = cl;
                                    objSal.FixedBase = basic;
                                    objSal.FixedFDA = "0.00";
                                    objSal.FixedFRA = "0.00";
                                    objSal.HomeTown = "0.00";
                                    objSal.HalfNightAmt = HalfNight_Amt;
                                    objSal.FullNightAmt = FullNight_Amt;
                                    objSal.ThreesidedAmt = ThreeSided_Amt;
                                    objSal.SpinningAmt = SpinningAmt;
                                    objSal.DayIncentive = DayIncentive;
                                    MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd/MM/yyyy", null);
                                    //if (EmpType == "1")
                                    //{
                                    objSal.BasicAndDA = basic;
                                    objSal.HRA = "0";
                                    objSal.FDA = "0";
                                    objSal.VDA = "0";
                                    objSal.OT = "0";
                                    objSal.Allowances1 = All1;
                                    objSal.Allowances2 = All2;
                                    objSal.Allowances3 = All3;
                                    objSal.Allowances4 = Al4;
                                    objSal.Allowances5 = All5;
                                    objSal.Deduction1 = Ded1;
                                    objSal.Deduction2 = ded2;
                                    objSal.Deduction3 = ded3;
                                    objSal.Deduction4 = ded4;
                                    objSal.Deduction5 = ded5;
                                    objSal.ProvidentFund = PFS;
                                    objSal.ESI = ESI;
                                    objSal.Stamp = "0";
                                    objSal.Advance = Advance;
                                    objSal.LossofPay = lop;
                                    objSal.OT = OTDays;
                                    objSal.LOPDays = lopdays;

                                    //Basic Spilit Det
                                    objSal.BasicDA = BasicAndDA;
                                    objSal.Basic_HRA = BasicHRA;
                                    objSal.Conv_Allow = ConvAllow;
                                    objSal.Edu_Allow = EduAllow;
                                    objSal.Medi_Allow = MediAllow;
                                    objSal.Basic_RAI = BasicRAI;
                                    objSal.Washing_Allow = WashingAllow;

                                    objSal.Basic_Spl_Allowance = Basic_Spl_Allowance;
                                    objSal.Basic_Uniform = Basic_Uniform;
                                    objSal.Basic_Vehicle_Maintenance = Basic_Vehicle_Maintenance;
                                    objSal.Basic_Communication = Basic_Communication;
                                    objSal.Basic_Journ_Perid_Paper = Basic_Journ_Perid_Paper;
                                    objSal.Basic_Incentives = Basic_Incentives;
                                    objSal.Basic_Incentive_Text = Basic_Incentive_Text;

                                    if (pfEligible == "1")
                                    {
                                        objSal.PfSalary = PFS;
                                        objSal.Emp_PF = PFS;
                                    }
                                    else
                                    {
                                        objSal.PfSalary = "0.00";
                                        objSal.Emp_PF = "0.00";
                                    }

                                    //Sub-Staff OT Hours Set
                                    objSal.OTHoursAmt = OTHoursAmtNew_Str;
                                    objSal.OTHoursNew = OTHoursNew_Str;
                                    objSal.Leave_Credit_Check = Leaver_Credit_Status;

                                    objSal.Fixed_Work_Days = Fixed_Work_Days;
                                    objSal.WH_Work_Days = WH_Work_Days;

                                    //Deduction Others
                                    objSal.Ded_Others1 = DedOthers1;
                                    objSal.Ded_Others2 = DedOthers2;

                                    //Employeer PF And ESI
                                    objSal.EmployeerPFone = EmployeerPFOne;
                                    objSal.EmployeerPFTwo = EmployeerPFTwo;
                                    objSal.EmployeerESI = EmployeerESI;

                                    objSal.Leave_Credit_Days = Leave_Credit_Days_Month;
                                    objSal.Leave_Credit_Add = Leave_Credit_Add_Minus;
                                    objSal.Leave_Credit_Type = Leave_Credit_Type;

                                    //PassFlag = true;

                                    //Check Salary Conformation
                                    DataTable Sal_Confm_DT = new DataTable();
                                    string Sal_Conform_qry = "Select * from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonth.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105)" +
                                                 " and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105) And Salary_Conform='1'";
                                    Sal_Confm_DT = objdata.RptEmployeeMultipleDetails(Sal_Conform_qry);
                                    if (Sal_Confm_DT.Rows.Count == 0)
                                    {
                                        PassFlag = true;
                                    }
                                    else
                                    {
                                        PassFlag = false;
                                    }

                                }
                                if (PassFlag == true)
                                {
                                    if (Updateval == "0")
                                    {
                                        string Del2 = "Delete from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonth.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify3 = new SqlCommand(Del2, cn);
                                        cmd_verify3.ExecuteNonQuery();
                                    }
                                    else if (Updateval == "1")
                                    {
                                        string Del = "Delete from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonth.Text + "' and FinancialYear='" + ddlFinYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtToDate.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify1 = new SqlCommand(Del, cn);
                                        cmd_verify1.ExecuteNonQuery();
                                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd/MM/yyyy", null);

                                        TempDate = Convert.ToDateTime(txtTransDate.Text).AddMonths(0).ToShortDateString();
                                        TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);

                                        //Get Basic Salary for Salary Master
                                        objSal.BasicforSM = Basic_For_SM;

                                        if (EmpType == "5")
                                        {
                                            BasicAndDA = basic;
                                        }

                                        Query = "";
                                        Query = Query = "Insert into [" + SessionEpay + "]..SalaryDetails(EmpNo,MachineNo,ExisistingCode,FromBankACNo,FromBankName,FromBranch,";
                                        Query = Query + "ToBankACNo,ToBankName,ToBranch,Cash,Totalworkingdays,BasicandDA,ProvidentFund,HRA,ESI,Conveyance,ProfessionalTax,";
                                        Query = Query + "CCA,IncomeTax,GrossEarnings,SalaryAdvanceTrans,MedicalReimburse,PerIncentive,Allowances1,Allowances2,Allowances3,Allowances4,Allowances5,";
                                        Query = Query + "Deduction1,Deduction2,Deduction3,Deduction4,Deduction5,Messdeduction,Hosteldeduction,LossPay,";
                                        Query = Query + "TotalDeductions,TotalPay,NetPay,SalaryDate,Month,Year,FinancialYear,LeaveDays,LOPDays,TransDate,Ccode, Lcode,";
                                        Query = Query + "OverTime,NFh,FDA,VDA,Advance,Stamp,Unioncg,PfSalary,Words,WorkedDays,Process_Mode,Weekoff,CL,Fbasic,FFDA,FHRA,";
                                        Query = Query + "Emp_PF, FromDate, ToDate, HomeDays,HalfNightAmt,FullNightAmt,SpinningAmt,DayIncentive,ThreesidedAmt,SalaryThrough,";
                                        Query = Query + "Wagestype,Basic_SM,BasicAndDANew,BasicHRA,ConvAllow,EduAllow,MediAllow,BasicRAI,WashingAllow,RoundOffNetPay,";
                                        Query = Query + "OTHoursNew,OTHoursAmtNew,Leave_Credit_Use,Fixed_Work_Days,WH_Work_Days,DedOthers1,DedOthers2,";
                                        Query = Query + "EmployeerPFone,EmployeerPFTwo,EmployeerESI,Leave_Credit_Days,Leave_Credit_Add,Leave_Credit_Type,";
                                        Query = Query + "Basic_Spl_Allowance,Basic_Uniform,Basic_Vehicle_Maintenance,Basic_Communication,Basic_Journ_Perid_Paper,Basic_Incentives,Basic_Incentive_Text,";
                                        Query = Query + "Employeer_ESI_IF,Employee_ESI_IF,Total_Deduction_IF,RoundOffNetPay_IF,Transport_Char_Regular)";
                                        Query = Query + "Values('" + EmpNo + "','" + EmpNo + "','" + ExistNo + "','" + FromBankACno + "','" + FromBankName + "','" + FromBranch + "','" + FromBankACno + "',";
                                        Query = Query + "'" + FromBankName + "','" + FromBranch + "','" + Cash + "','" + Work + "','" + basic + "','" + PFS + "','0','" + ESI + "','" + ConvAllow + "','" + ProvisionalTax + "','" + CCA + "',";
                                        Query = Query + "'" + IncomTax + "','" + TotalEarnings + "','" + SalaryAdvanceTrans + "','" + MedicalReiburse + "','" + PerIncentive + "','" + All1 + "','" + All2 + "','" + All3 + "','" + Al4 + "','" + All5 + "',";
                                        Query = Query + "'" + Ded1 + "','" + ded2 + "','" + ded3 + "','" + ded4 + "','" + ded5 + "','" + MessDeduction + "','" + HostelDeduction + "','" + lop + "','" + TotalDeductions + "','" + Totalpay + "','" + NetPay + "',";
                                        Query = Query + "convert(datetime,'" + MyDate + "',105),'" + ddlMonth.Text + "','" + Year + "','" + ddlFinYear.SelectedValue + "','" + lopdays + "','" + lopdays + "',Convert(Datetime,'" + TransDate + "',105),'" + SessionCcode + "','" + SessionLcode + "','0','" + NFh + "',";
                                        Query = Query + "'0','0','" + Advance + "','0','" + Union + "','" + PFS + "','" + Words + "','" + EmployeeDays + "','0','" + WeekOff + "','" + cl + "','" + basic + "','0.0','0.0','" + PFS + "',";
                                        Query = Query + "Convert(Datetime,'" + MyDate1 + "',105),Convert(Datetime,'" + MyDate2 + "',105),'0.0','" + HalfNight_Amt + "','" + FullNight_Amt + "','" + SpinningAmt + "',";
                                        Query = Query + "'" + DayIncentive + "','" + ThreeSided_Amt + "','" + SalarythroughType + "','" + Get_Wagestype + "','" + Basic_For_SM + "','" + BasicAndDA + "','" + BasicHRA + "','" + ConvAllow + "',";
                                        Query = Query + "'" + EduAllow + "','" + MediAllow + "','" + BasicRAI + "','" + WashingAllow + "','" + RoundOffNetPay + "','" + OTHoursNew_Str + "','" + OTHoursAmtNew_Str + "','" + Leaver_Credit_Status + "',";
                                        Query = Query + "'" + Fixed_Work_Days + "','" + WH_Work_Days + "','" + DedOthers1 + "','" + DedOthers2 + "','" + EmployeerPFOne + "','" + EmployeerPFTwo + "',";
                                        Query = Query + "'" + EmployeerESI + "','" + Leave_Credit_Days_Month + "','" + Leave_Credit_Add_Minus + "','" + Leave_Credit_Type + "',";
                                        Query = Query + "'" + Basic_Spl_Allowance + "','" + Basic_Uniform + "','" + Basic_Vehicle_Maintenance + "','" + Basic_Communication + "',";
                                        Query = Query + "'" + Basic_Journ_Perid_Paper + "','" + Basic_Incentives + "','" + Basic_Incentive_Text + "',";
                                        Query = Query + "'" + Employeer_ESI_IF + "','" + Employee_ESI_IF + "','" + Total_Deduction_IF + "','" + RoundOffNetPay_IF + "','" + Transport_Charges_Regular + "')";

                                        objdata.RptEmployeeMultipleDetails(Query);


                                        string Balanceday = "";
                                        Query = "";
                                        Query = "Select BalanceDays from [" + SessionEpay + "]..ContractDetails where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Completed='No'";
                                        dt_Others = objdata.RptEmployeeMultipleDetails(Query);
                                        if (dt_Others.Rows.Count > 0) { Balanceday = dt_Others.Rows[0]["BalanceDays"].ToString(); }
                                        //string Balanceday = objdata.Contract_GetEmpDet(EmpNo, SessionCcode, SessionLcode);
                                        if (Balanceday.Trim() != "")
                                        {
                                            if (Convert.ToDecimal(Balanceday) > 0)
                                            {
                                                DataTable dt_det = new DataTable();
                                                Query = "";
                                                Query = Query + "Select YrDays,FixedDays,Days,ContractType,MonthDays,DecMonth from [" + SessionEpay + "]..ContractDetails where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Completed='No'";
                                                dt_det = objdata.RptEmployeeMultipleDetails(Query);
                                                if (dt_det.Rows.Count > 0)
                                                {
                                                    if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        //objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing_Month(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode, Mon);
                                                        }
                                                        else
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                                                            // objdata.Not_Contract_Reducing_Month(EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        }
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        //objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        SaveFlag = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                sucess = true;
                                cn.Close();

                            }
                            if (sucess == true)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
                            }
                            btnSalCalc.Enabled = true;
                        }
                    }
                }
            }
            btnSalCalc.Enabled = true;
        }
        catch (Exception ex)
        {
            lblError_Display.Text = ex.Message.ToString();
            btnSalCalc.Enabled = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }
    protected void ddlCategory_SelectedIndexChanged1(object sender, EventArgs e)
    {
        DataTable dtemp = new DataTable();
        Query = "Select 0 EmpTypeCd,'-----Select------' EmpType union ";
        Query = Query + "Select EmpTypeCd,EmpType from mstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        if (SessionAdmin == "2")
        {
            Query = Query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }
        dtemp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtemp;
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool SaveFlag = false;
        string BasicPFDays = "26";
        string Sweeper = "0.00";
        string Fine = "0.00";

        //Basic Spilit
        string BasicAndDA = "0.00";
        string BasicHRA = "0.00";
        string ConvAllow = "0.00";
        string EduAllow = "0.00";
        string MediAllow = "0.00";
        string BasicRAI = "0.00";
        string WashingAllow = "0.00";

        string Days24Spl_Allowance = "0.00";
        string SumWorkDays = "0";
        string SumWeekOffDays = "0";
        decimal Totaldays = 0;
        decimal PaidAmt = 0;
        string PerWp = "0";

        string Basic_Spl_Allowance = "0.00";
        string Basic_Uniform = "0.00";
        string Basic_Vehicle_Maintenance = "0.00";
        string Basic_Communication = "0.00";
        string Basic_Journ_Perid_Paper = "0.00";
        string Basic_Incentives = "0.00";
        string Basic_Incentive_Text = "Incentive";

        string Leaver_Credit_Status = "0";
        string Leave_Credit_Year = "0";
        string Leave_Credit_Days_Month = "0";
        string Leave_Credit_Add_Minus = "0";
        string Leave_Credit_Type = "";
        string Basic_Spilit_Tot = "0.00";

        DataTable Advance_Mst_DT = new DataTable();
        DataTable Advance_Pay_DT = new DataTable();
        DataTable dt_Others = new DataTable();
        string Advance_Amt_Check = "0";
        string Advance_Paid_Count = "0";
        string PF_per = "0", ESI_per = "0", StaffSalary = "0", EmployeerPFone = "0", Employer_PF = "0", Employer_ESI = "0", CanteenAmt = "0";
        string BankName = "";
        string BranchCode = "";
        string AccountNo = "";

        string Emp_Token_No_Search_Sal_Process = "";
        ///btnSalaryCalculate.Enabled = false;
        basicsalary = 0;
        try
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFromDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtToDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }


            decimal Month_Total_days = 0;
            if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonth.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(ddlFinYear.SelectedValue) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }

            TempDate = Convert.ToDateTime(txtFromDate.Text).AddMonths(0).ToShortDateString();
            string MonthName = TempDate;
            DateTime mtn;
            mtn = Convert.ToDateTime(TempDate);
            d = mtn.Month;
            int mons = mtn.Month;
            int yrs1 = mtn.Month;

            #region MonthName
            string DB_Mont = "";
            switch (d)
            {
                case 1:
                    DB_Mont = "January";
                    break;

                case 2:
                    DB_Mont = "February";
                    break;
                case 3:
                    DB_Mont = "March";
                    break;
                case 4:
                    DB_Mont = "April";
                    break;
                case 5:
                    DB_Mont = "May";
                    break;
                case 6:
                    DB_Mont = "June";
                    break;
                case 7:
                    DB_Mont = "July";
                    break;
                case 8:
                    DB_Mont = "August";
                    break;
                case 9:
                    DB_Mont = "September";
                    break;
                case 10:
                    DB_Mont = "October";
                    break;
                case 11:
                    DB_Mont = "November";
                    break;
                case 12:
                    DB_Mont = "December";
                    break;
                default:
                    break;
            }
            #endregion

            if (ChkCivilIncentive.Checked == true)
            {
                if (txtIstWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the First Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (txtLastWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Last Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));
            }
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFromDate.Text);
                DateTime dtto = Convert.ToDateTime(txtToDate.Text);
                DateTime MyDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
                DateTime MyDate2 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
                Leave_Credit_Year = MyDate1.Year.ToString();
                DataTable dtpf = new DataTable();
                //Get PF ESI Details
                Query = "select * from [" + SessionEpay + "]..MstESIPF where Ccode='" + SessionCcode.ToString() + "' and Lcode='" + SessionLcode.ToString() + "'";
                dtpf = objdata.RptEmployeeMultipleDetails(Query);
                if (dtpf.Rows.Count > 0)
                {
                    PF_per = dtpf.Rows[0]["PF_per"].ToString();
                    ESI_per = dtpf.Rows[0]["ESI_per"].ToString();
                    StaffSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    EmployeerPFone = dtpf.Rows[0]["EmployeerPFone"].ToString();
                    EmployeerPFTwo = dtpf.Rows[0]["EmployeerPFTwo"].ToString();
                    EmployeerESI = dtpf.Rows[0]["EmployeerESI"].ToString();
                }
                else
                {
                    PF_per = "0"; ESI_per = "0"; StaffSalary = "0"; EmployeerPFone = "0"; EmployeerPFTwo = "0"; EmployeerESI = "0";
                }

                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtFromDate.Text = null;
                    txtToDate.Text = null;
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    //DataTable dt = new DataTable();
                    string Staff_Labour_Cat = "";
                    if (ddlCategory.SelectedValue == "1")
                    {
                        Staff_Labour_Cat = "S";
                    }
                    else
                    {
                        Staff_Labour_Cat = "L";
                    }

                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {

                            }
                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            string EmpNo = dt.Rows[i][1].ToString();
                            All3 = dt.Rows[i][4].ToString();
                            string All4 = dt.Rows[i][5].ToString();
                            All5 = dt.Rows[i][6].ToString();
                            ded3 = dt.Rows[i][7].ToString();
                            ded4 = dt.Rows[i][8].ToString();
                            ded5 = dt.Rows[i][9].ToString();
                            Advance = dt.Rows[i][10].ToString();
                            string Others1 = dt.Rows[i][11].ToString();
                            string Others2 = dt.Rows[i][12].ToString();
                            string basic1 = "0";
                            string PF_Total_Amt = "0", BasicDA = "0", Conveyance = "0", HRA1 = "0", HRA_Str = "0", ESI_Total_Amt = "0", SplAllow1 = "0", Spl_Allow_Str = "0";

                            DataTable dt_Attend = new DataTable();
                            DataTable dt_Employee = new DataTable();
                            int j = 0;
                            if (EmpNo == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Employee Number. The Row Number is " + j + "')</script>");
                            }
                            else if (All3 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Allowance3. The Row Number is " + j + "')</script>");
                            }
                            else if (All4 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Allowance4. The Row Number is " + j + "')</script>");
                            }
                            else if (All5 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Allowance5. The Row Number is " + j + "')</script>");
                            }
                            else if (ded3 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Deduction3. The Row Number is " + j + "')</script>");
                            }
                            else if (ded4 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Deduction4. The Row Number is " + j + "')</script>");
                            }
                            else if (ded5 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Deduction5. The Row Number is " + j + "')</script>");
                            }
                            else if (Advance == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Advance. The Row Number is " + j + "')</script>");
                            }
                            else if (Others1 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Advance. The Row Number is " + j + "')</script>");
                            }
                            else if (Others2 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Advance. The Row Number is " + j + "')</script>");
                            }

                            if (!ErrFlag)
                            {
                                //Get Attendance
                                SSQL = "Select * from [" + SessionEpay + "]..AttenanceDetails where convert(datetime,FromDate,103)=convert(datetime,'" + txtFromDate.Text + "',103) and convert(datetime,ToDate,103)=convert(datetime,'" + txtToDate.Text + "',103) ";
                                SSQL = SSQL + " And EmpNo='" + EmpNo + "'";
                                SSQL = SSQL + "and Ccode='" + SessionCcode.ToString() + "' and Lcode='" + SessionLcode.ToString() + "'";
                                dt_Attend = objdata.RptEmployeeMultipleDetails(SSQL); ;
                                if (dt_Attend.Rows.Count > 0)
                                {
                                    // Get Employee Profile
                                    Query = "select ExistingCode,MachineID,Salary_Through as SalaryThro,BankName,BranchCode,AccountNo,EligibleCateen,EligibleIncentive,EligibleSnacks,Eligible_PF,";
                                    Query = Query + "Eligible_ESI,BasicSalary, Alllowance1 as Allowance1amt,Alllowance2 as Allowance2amt,Deduction1 as Deduction1amt,Deduction2 as Deduction2amt,ConvenyAmt as ConvenyAmt,SplAmt,EligibleCateen,OTEligible,";
                                    Query = Query + "LabourAmt,OT_Salary as OTSal,StdWrkHrs from Employee_Mst where EmpNo='" + EmpNo.ToString() + "'";
                                    Query = Query + " And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    dt_Employee = objdata.RptEmployeeMultipleDetails(Query); ;
                                    if (dt_Employee.Rows.Count > 0)
                                    {

                                        //Fill Attendance
                                        EmpNo = dt_Attend.Rows[0]["EmpNo"].ToString();
                                        string Days = dt_Attend.Rows[0]["Days"].ToString();
                                        string Months = dt_Attend.Rows[0]["Months"].ToString();
                                        string FinancialYear = dt_Attend.Rows[0]["FinancialYear"].ToString();
                                        NFh = dt_Attend.Rows[0]["NFh"].ToString();
                                        string NFH_Work_Days_Statutory = dt_Attend.Rows[0]["NFH_Work_Days_Statutory"].ToString();
                                        string WH_Work_Present = dt_Attend.Rows[0]["WH_Work_Present"].ToString();
                                        string weekoff = dt_Attend.Rows[0]["weekoff"].ToString();
                                        Fixed_Work_Days = dt_Attend.Rows[0]["Fixed_Work_Days"].ToString();
                                        string OTHoursNew = dt_Attend.Rows[0]["OTHoursNew"].ToString();
                                        string Total_Work = dt_Attend.Rows[0]["TotalDays"].ToString();
                                        string CanteenDays = dt_Attend.Rows[0]["ThreeSided"].ToString();
                                        string Incentive_Days = "0";
                                        string Incentive_Amt = "0";

                                        //Fill Profile
                                        string ExisistingCode = dt_Employee.Rows[0]["ExistingCode"].ToString();
                                        string MachineID = dt_Employee.Rows[0]["MachineID"].ToString();
                                        string EligibleCateen = dt_Employee.Rows[0]["EligibleCateen"].ToString();
                                        string EligibleIncentive = dt_Employee.Rows[0]["EligibleIncentive"].ToString();
                                        string EligibleSnacks = dt_Employee.Rows[0]["EligibleSnacks"].ToString();
                                        string Eligible_PF = dt_Employee.Rows[0]["Eligible_PF"].ToString();
                                        string Eligible_ESI = dt_Employee.Rows[0]["Eligible_ESI"].ToString();
                                        string BasicSalary = dt_Employee.Rows[0]["BasicSalary"].ToString();
                                        string Allowance1amt = dt_Employee.Rows[0]["Allowance1amt"].ToString();
                                        string Allowance2amt = dt_Employee.Rows[0]["Allowance2amt"].ToString();
                                        string Deduction1amt = dt_Employee.Rows[0]["Deduction1amt"].ToString();
                                        string Deduction2amt = dt_Employee.Rows[0]["Deduction2amt"].ToString();
                                        string ConvenyAmt = dt_Employee.Rows[0]["ConvenyAmt"].ToString();
                                        string SplAmt = dt_Employee.Rows[0]["SplAmt"].ToString();
                                        string SalaryThro = dt_Employee.Rows[0]["SalaryThro"].ToString();
                                        string LabourAmt = dt_Employee.Rows[0]["LabourAmt"].ToString();
                                        string OTEligible = dt_Employee.Rows[0]["OTEligible"].ToString();
                                        string OTSal = dt_Employee.Rows[0]["OTSal"].ToString(); //Edit by Narmatha
                                        string StdWork = dt_Employee.Rows[0]["StdWrkHrs"].ToString();

                                        if (EmpNo.Trim() == "305253")
                                        {
                                            string Val = "0";
                                        }
                                        if (ddlEmployeeType.SelectedValue == "1")
                                        {
                                            WagesType = "1";
                                        }
                                        else
                                        {
                                            WagesType = "2";

                                        }
                                        if (WagesType == "1")
                                        {
                                            // Staff Salary //
                                            //Basic Calc
                                            basic1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Days))).ToString();
                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                            string OTAmt = "";
                                            if (OTEligible.ToUpper() == "YES")
                                            {
                                                if (OTHoursNew != "0")
                                                {
                                                    string HrSal = (Convert.ToDecimal(basic1) / Convert.ToDecimal(8)).ToString();
                                                    //HrSal = (Math.Round(Convert.ToDecimal(HrSal), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    OTAmt = ((Convert.ToDecimal(OTHoursNew)) * (Convert.ToDecimal(HrSal))).ToString();
                                                    OTAmt = (Math.Round(Convert.ToDecimal(OTAmt), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                                else
                                                {
                                                    OTAmt = "0";
                                                }
                                            }
                                            else
                                            {
                                                OTAmt = "0";
                                            }

                                            string Basic_Per = "0";
                                            string HRA_Per = "0";
                                            string Convey_Per = "0";
                                            string Washing_Per = "0";
                                            string Other_Per = "0";

                                            DataTable DT_Sal = new DataTable();
                                            SSQL = "Select BasicDA as Basic_Percentage,HRA as HRA_Percentage,ConvAllow as Convey_Percentage,WashingAllow as Washing_Percentage,Bonus as Other_Percentage,* from [" + SessionEpay + "]..MstBasicDet where CCode='" + SessionCcode.ToString() + "' And LCode='" + SessionLcode.ToString() + "'";
                                            SSQL = SSQL + " And EmployeeType='1'";
                                            DT_Sal = objdata.RptEmployeeMultipleDetails(SSQL); ;

                                            if (DT_Sal.Rows.Count != 0)
                                            {
                                                Basic_Per = DT_Sal.Rows[0]["Basic_Percentage"].ToString();
                                                HRA_Per = DT_Sal.Rows[0]["HRA_Percentage"].ToString();
                                                Convey_Per = DT_Sal.Rows[0]["Convey_Percentage"].ToString();
                                                Washing_Per = DT_Sal.Rows[0]["Washing_Percentage"].ToString();
                                                Other_Per = DT_Sal.Rows[0]["Other_Percentage"].ToString();
                                            }
                                            else
                                            {
                                                Basic_Per = "0";
                                                HRA_Per = "0";
                                                Convey_Per = "0";
                                                Washing_Per = "0";
                                                Other_Per = "0";
                                            }



                                            PF_Total_Amt = (Convert.ToDecimal(basic) + Convert.ToDecimal(OTAmt)).ToString();
                                            BasicDA = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Basic_Per)) / 100).ToString();
                                            Conveyance = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Convey_Per)) / 100).ToString();
                                            WashingAllow = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Washing_Per)) / 100).ToString();

                                            //HRA Calculate
                                            //HRA1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                                            //HRA_Str = (Convert.ToDecimal(HRA1) * (Convert.ToDecimal(Days))).ToString();
                                            HRA1 = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(HRA_Per)) / 100).ToString();
                                            HRA_Str = HRA1;
                                            HRA_Str = (Math.Round(Convert.ToDecimal(HRA_Str), 0, MidpointRounding.AwayFromZero)).ToString();


                                            ESI_Total_Amt = (Convert.ToDecimal(BasicDA) + Convert.ToDecimal(HRA_Str)).ToString();

                                            //Spl. Allowence Calculate
                                            //SplAllow1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                                            //Spl_Allow_Str = (Convert.ToDecimal(SplAllow1) * (Convert.ToDecimal(Days))).ToString();
                                            SplAllow1 = "0";
                                            Spl_Allow_Str = "0";
                                            Spl_Allow_Str = (Math.Round(Convert.ToDecimal(Spl_Allow_Str), 0, MidpointRounding.AwayFromZero)).ToString();


                                            //Get Fixed Allowance and Deduction
                                            All1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance1amt)).ToString();
                                            All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            All2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance2amt)).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();

                                            Ded1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction1amt)).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ded2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction2amt)).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 0, MidpointRounding.AwayFromZero)).ToString();


                                            //PF Calculation
                                            if (Eligible_PF == "1")
                                            {
                                                PF_salary = "1";

                                                string PF_Check = BasicDA;

                                                //string PF_Check = ((Convert.ToDecimal(basic) * Convert.ToDecimal(50)) / 100).ToString();
                                                if (Convert.ToDecimal(PF_Check) >= Convert.ToDecimal(StaffSalary))
                                                {
                                                    PFS = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    PFS = Others1;
                                                    Employer_PF = Others1;
                                                }
                                                else
                                                {
                                                    decimal PF_val = (Convert.ToDecimal(PF_Check));
                                                    PFS = ((PF_val * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((PF_val * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    PFS = Others1;
                                                    Employer_PF = Others1;
                                                }
                                            }
                                            else
                                            {
                                                PFS = "0";
                                                Employer_PF = "0";
                                            }


                                            //ESI Calculation
                                            if (Eligible_ESI == "1")
                                            {
                                                //string ESI_Check = ((Convert.ToDecimal(basic) * Convert.ToDecimal(80)) / 100).ToString();
                                                decimal ESI_val = (Convert.ToDecimal(ESI_Total_Amt));
                                                ESI = ((ESI_val * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                                ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.ToEven)).ToString();



                                                Employer_ESI = ((ESI_val * Convert.ToDecimal(EmployeerESI)) / 100).ToString();
                                                Employer_ESI = (Math.Round(Convert.ToDecimal(Employer_ESI), 1, MidpointRounding.ToEven)).ToString();

                                                //Employee ESI Round
                                                if (ESI != "0")
                                                {
                                                    string[] split_val = ESI.Split('.');
                                                    string Ist = split_val[0].ToString();
                                                    string IInd = split_val[1].ToString();

                                                    if (Convert.ToInt32(IInd) > 0)
                                                    {
                                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                    }
                                                    else
                                                    {
                                                        ESI = Ist;
                                                    }
                                                }
                                                //Employeer ESI Round
                                                if (EmployeerESI != "0")
                                                {
                                                    string[] Emp_split_val = Employer_ESI.Split('.');
                                                    string Emp_Ist = Emp_split_val[0].ToString();
                                                    string Emp_IInd = Emp_split_val[1].ToString();

                                                    if (Convert.ToInt32(Emp_IInd) > 0)
                                                    {
                                                        Employer_ESI = (Convert.ToInt32(Emp_Ist)+1).ToString();
                                                    }
                                                    else
                                                    {
                                                        Employer_ESI = Emp_Ist;
                                                    }

                                                    ESI = Others2;
                                                    Employer_ESI = Others2;
                                                }
                                            }
                                            else
                                            {
                                                ESI = "0";
                                                Employer_ESI = "0";
                                            }



                                            CanteenAmt = "0";

                                            //if (EligibleCateen == "1")
                                            //{
                                            //    string CanDays = (Convert.ToDouble(Total_Work) - Convert.ToDouble(CanteenDays)).ToString();
                                            //    CanteenAmt = ((Convert.ToDecimal(LabourAmt)) * (Convert.ToDecimal(CanDays))).ToString();
                                            //    CanteenAmt = (Math.Round(Convert.ToDecimal(CanteenAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //    CanteenAmt = "0";
                                            //}
                                            //else
                                            //{
                                            //    CanteenAmt = "0";
                                            //}

                                            //Incentive Calc
                                            if (EligibleIncentive == "1")
                                            {
                                                DataTable dt_Incentive = new DataTable();
                                                Query = "select Days,Amount from [" + SessionEpay + "]..Incentive_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' and Employee_Type='" + WagesType + "' ";
                                                dt_Incentive = objdata.RptEmployeeMultipleDetails(SSQL); ;
                                                if (dt_Incentive.Rows.Count > 0)
                                                {
                                                    Incentive_Days = dt_Incentive.Rows[0]["Days"].ToString();
                                                    Incentive_Amt = dt_Incentive.Rows[0]["Amount"].ToString();
                                                }
                                                else
                                                {
                                                    Incentive_Days = "0";
                                                    Incentive_Amt = "0";
                                                }
                                                //string IncentiveDays = (Convert.ToDecimal(Total_Work) - Convert.ToDecimal(weekoff)).ToString();

                                                if (Convert.ToDecimal(Days) >= Convert.ToDecimal(Incentive_Days))
                                                {
                                                    //DayIncentive = (Convert.ToDecimal(10) * Convert.ToDecimal(Days)).ToString();
                                                    DayIncentive = Incentive_Amt;
                                                    DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    DayIncentive = "0";
                                                }
                                            }
                                            else
                                            {
                                                DayIncentive = "0";
                                            }



                                            if (ConvenyAmt == "")
                                            {
                                                ConvenyAmt = "0";
                                            }
                                            else
                                            {
                                                if ((Convert.ToDecimal(ConvenyAmt) == Convert.ToDecimal(0)) || (Convert.ToDecimal(Fixed_Work_Days) == Convert.ToDecimal(0)))
                                                {
                                                    ConvenyAmt = "0";
                                                }
                                                else
                                                {
                                                    ConvenyAmt = (Convert.ToDecimal(ConvenyAmt) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                    ConvenyAmt = (Convert.ToDecimal(ConvenyAmt) * (Convert.ToDecimal(Days))).ToString();
                                                }
                                            }

                                            if (SplAmt == "")
                                            {
                                                SplAmt = "0";

                                            }
                                            else
                                            {
                                                if ((Convert.ToDecimal(SplAmt) == Convert.ToDecimal(0)) || (Convert.ToDecimal(Fixed_Work_Days) == Convert.ToDecimal(0)))
                                                {
                                                    SplAmt = "0";
                                                }
                                                else
                                                {
                                                    SplAmt = (Convert.ToDecimal(SplAmt) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                    SplAmt = (Convert.ToDecimal(SplAmt) * (Convert.ToDecimal(Days))).ToString();
                                                }
                                            }

                                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTAmt)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(CanteenAmt) + Convert.ToDecimal(ConvenyAmt) + Convert.ToDecimal(SplAmt)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();

                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            string RoundOffNetPay = rounded1.ToString();

                                            string Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";

                                            string ProcessMode = "";
                                            DataTable DT_sal = new DataTable();
                                            SSQL = "Select *from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "'";
                                            SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And Ccode='" + SessionCcode.ToString() + "' And Lcode='" + SessionLcode.ToString() + "'";
                                            DT_sal = objdata.RptEmployeeMultipleDetails(SSQL); ;
                                            if (DT_sal.Rows.Count != 0)
                                            {
                                                ProcessMode = DT_sal.Rows[0]["Process_Mode"].ToString();
                                                if (ProcessMode == "1")
                                                {
                                                    SSQL = "Delete from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "'";
                                                    SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And Ccode='" + SessionCcode.ToString() + "' And Lcode='" + SessionLcode.ToString() + "'";
                                                    objdata.RptEmployeeMultipleDetails(SSQL); ;
                                                }
                                            }

                                            if (ProcessMode != "2")
                                            {
                                                SSQL = "insert into [" + SessionEpay + "]..SalaryDetails(EmpNo,MachineNo,ExisistingCode,SalaryThrough,Wagestype,";
                                                SSQL = SSQL + "FromBankACno,FromBankName,FromBranch,Totalworkingdays,PerDaySal,";
                                                SSQL = SSQL + "BasicandDA,ProvidentFund,HRA,ESI,GrossEarnings,allowances1,allowances2,allowances3,";
                                                SSQL = SSQL + "allowances4,allowances5,Deduction1,Deduction2,Deduction3,Deduction4,Deduction5,";
                                                SSQL = SSQL + "Messdeduction,Losspay,TotalDeductions,NetPay,SalaryDate,Month,Year,FinancialYear,";
                                                SSQL = SSQL + "LeaveDays,LOPDays,TransDate,OverTime,NFh,NFH_Work_Present,FDA,VDA,Advance,Stamp,PfSalary,Words,";
                                                SSQL = SSQL + "WorkedDays,Process_Mode,Weekoff,CL,Fbasic,FFDA,FHRA,Emp_PF,FromDate,ToDate,HomeDays,";
                                                SSQL = SSQL + "HalfNightAmt,FullNightAmt,SpinningAmt,DayIncentive,ThreesidedAmt,Basic_SM,";
                                                SSQL = SSQL + "BasicAndDANew,BasicHRA,EduAllow,MediAllow,BasicRAI,WashingAllow,";
                                                SSQL = SSQL + "RoundOffNetPay,OTHoursNew,OTHoursAmtNew,Leave_Credit_Use,Fixed_Work_Days,";
                                                SSQL = SSQL + "WH_Work_Days,DedOthers1,DedOthers2,EmployeerPFone,EmployeerPFTwo,EmployeerESI,";
                                                SSQL = SSQL + "Leave_Credit_Days,Leave_Credit_Add,Days_Shift,Three_Shift,";
                                                SSQL = SSQL + "Night_Shift,Atten_Inc_Week,Pref_Inc,Spn_Inc,Atten_Inc_Month,Total_Basic,ConvAllow,Conveyance,Ccode,Lcode)values";
                                                SSQL = SSQL + "('" + EmpNo + "','" + MachineID + "','" + ExisistingCode + "','" + SalaryThro.Trim() + "',";
                                                SSQL = SSQL + "'" + WagesType + "','" + AccountNo + "','" + BankName + "','" + BranchCode + "',";
                                                SSQL = SSQL + "'" + Total_Work + "','" + basic1 + "','" + BasicDA + "','" + PFS + "','" + HRA_Str + "','" + ESI + "',";
                                                SSQL = SSQL + "'" + TotalEarnings + "','" + All1 + "','" + All2 + "','" + All3 + "','" + All4 + "','" + All5 + "',";
                                                SSQL = SSQL + "'" + Ded1 + "','" + ded2 + "','" + ded3 + "','" + ded4 + "','" + ded5 + "','" + CanteenAmt + "',";
                                                SSQL = SSQL + "'" + SplAmt + "','" + TotalDeductions + "','" + NetPay + "',GetDate(),'" + ddlMonth.SelectedItem.Text + "','" + ddlFinYear.SelectedValue + "','" + ddlFinYear.SelectedValue + "',";
                                                SSQL = SSQL + "'0','0',GetDate(),'0','" + NFh + "','" + NFH_Work_Days_Statutory + "','0','0','" + Advance + "','0','" + PFS + "','" + Words + "',";
                                                SSQL = SSQL + "'" + Days + "','1','" + weekoff + "','0','" + basic + "','0','0','0','" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',";
                                                SSQL = SSQL + "'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "','0','0','0','0','" + DayIncentive + "','0','" + basic + "',";
                                                SSQL = SSQL + "'" + basic + "','0','0','0','0','" + WashingAllow + "','" + RoundOffNetPay + "','" + OTHoursNew + "','0','0','" + Fixed_Work_Days + "',";
                                                SSQL = SSQL + "'" + WH_Work_Present + "','0','0','" + PFS + "','" + Employer_PF + "','" + Employer_ESI + "','0','0','0','" + CanteenDays + "',";
                                                SSQL = SSQL + "'0','0','0','0','0','0','" + ConvenyAmt + "','" + Conveyance + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
                                                objdata.RptEmployeeMultipleDetails(SSQL);
                                            }
                                        }
                                        else if ((WagesType == "2") || (WagesType == "3"))
                                        {
                                            if (EmpNo == "908304")
                                            {
                                                string ww = "0";
                                            }

                                            //Labour Salary
                                            //Basic Calc
                                            basic1 = (Convert.ToDecimal(BasicSalary)).ToString();
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Days))).ToString();
                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                            string OTAmt = "";
                                            if (OTEligible.ToUpper() == "YES")
                                            {
                                                if (OTHoursNew != "0")
                                                {
                                                    //string HrSal = (Convert.ToDecimal(basic1) / Convert.ToDecimal(8)).ToString();
                                                    string HrSal = OTSal;


                                                    OTAmt = ((Convert.ToDecimal(OTHoursNew)) * (Convert.ToDecimal(HrSal))).ToString();
                                                    OTAmt = (Math.Round(Convert.ToDecimal(OTAmt), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                                else
                                                {
                                                    OTAmt = "0";
                                                }
                                            }
                                            else
                                            {
                                                OTAmt = "0";
                                            }

                                            string Basic_Per = "0";
                                            string HRA_Per = "0";
                                            string Convey_Per = "0";
                                            string Washing_Per = "0";
                                            string Other_Per = "0";

                                            DataTable DT_Sal = new DataTable();
                                            SSQL = "Select BasicDA as Basic_Percentage,HRA as HRA_Percentage,ConvAllow as Convey_Percentage,WashingAllow as Washing_Percentage,Bonus as Other_Percentage,* from [" + SessionEpay + "]..MstBasicDet where CCode='" + SessionCcode.ToString() + "' And LCode='" + SessionLcode.ToString() + "'";
                                            SSQL = SSQL + " And EmployeeType='2'";
                                            DT_Sal = objdata.RptEmployeeMultipleDetails(SSQL);

                                            if (DT_Sal.Rows.Count != 0)
                                            {
                                                Basic_Per = DT_Sal.Rows[0]["Basic_Percentage"].ToString();
                                                HRA_Per = DT_Sal.Rows[0]["HRA_Percentage"].ToString();
                                                Convey_Per = DT_Sal.Rows[0]["Convey_Percentage"].ToString();
                                                Washing_Per = DT_Sal.Rows[0]["Washing_Percentage"].ToString();
                                                Other_Per = DT_Sal.Rows[0]["Other_Percentage"].ToString();
                                            }
                                            else
                                            {
                                                Basic_Per = "0";
                                                HRA_Per = "0";
                                                Convey_Per = "0";
                                                Washing_Per = "0";
                                                Other_Per = "0";
                                            }


                                            PF_Total_Amt = (Convert.ToDecimal(basic) + Convert.ToDecimal(OTAmt)).ToString();
                                            BasicDA = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Basic_Per)) / 100).ToString();
                                            Conveyance = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Convey_Per)) / 100).ToString();
                                            WashingAllow = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Washing_Per)) / 100).ToString();

                                            //HRA Calculate
                                            //HRA1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                                            //HRA_Str = (Convert.ToDecimal(HRA1) * (Convert.ToDecimal(Days))).ToString();
                                            HRA1 = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(HRA_Per)) / 100).ToString();
                                            HRA_Str = HRA1;
                                            HRA_Str = (Math.Round(Convert.ToDecimal(HRA_Str), 0, MidpointRounding.AwayFromZero)).ToString();



                                            //Spl. Allowence Calculate
                                            //SplAllow1 = (Convert.ToDecimal(BasicSalary)).ToString();
                                            //Spl_Allow_Str = (Convert.ToDecimal(SplAllow1) * (Convert.ToDecimal(Days))).ToString();
                                            SplAllow1 = "0";
                                            Spl_Allow_Str = "0"; ;
                                            Spl_Allow_Str = (Math.Round(Convert.ToDecimal(Spl_Allow_Str), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ESI_Total_Amt = (Convert.ToDecimal(BasicDA) + Convert.ToDecimal(HRA_Str)).ToString();

                                            //Get Fixed Allowance and Deduction
                                            All1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance1amt)).ToString();
                                            All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            All2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance2amt)).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();

                                            Ded1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction1amt)).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ded2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction2amt)).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 0, MidpointRounding.AwayFromZero)).ToString();


                                            //PF Calculation
                                            if (Eligible_PF == "1")
                                            {
                                                PF_salary = "1";

                                                string PF_Check = BasicDA;

                                                ///string PF_Check = ((Convert.ToDecimal(BasicDA) * Convert.ToDecimal(50)) / 100).ToString();
                                                if (Convert.ToDecimal(PF_Check) >= Convert.ToDecimal(StaffSalary))
                                                {
                                                    PFS = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    PFS = Others1;
                                                    Employer_PF = Others1;
                                                }
                                                else
                                                {
                                                    decimal PF_val = (Convert.ToDecimal(PF_Check));
                                                    PFS = ((PF_val * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((PF_val * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    PFS = Others1;
                                                    Employer_PF = Others1;
                                                }
                                            }
                                            else
                                            {
                                                PFS = "0";
                                                Employer_PF = "0";
                                            }


                                            //ESI Calculation
                                            if (Eligible_ESI == "1")
                                            {
                                                //string ESI_Check = ((Convert.ToDecimal(basic) * Convert.ToDecimal(80)) / 100).ToString();
                                                decimal ESI_val = (Convert.ToDecimal(ESI_Total_Amt));
                                                ESI = ((ESI_val * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                                ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.ToEven)).ToString();

                                                Employer_ESI = ((ESI_val * Convert.ToDecimal(EmployeerESI)) / 100).ToString();
                                                Employer_ESI = (Math.Round(Convert.ToDecimal(Employer_ESI), 1, MidpointRounding.ToEven)).ToString();

                                                //Employee ESI Round
                                                if (ESI != "0")
                                                {
                                                    string[] split_val = ESI.Split('.');
                                                    string Ist = split_val[0].ToString();
                                                    string IInd = split_val[1].ToString();

                                                    if (Convert.ToInt32(IInd) > 0)
                                                    {
                                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                    }
                                                    else
                                                    {
                                                        ESI = Ist;
                                                    }
                                                }
                                                //Employeer ESI Round
                                                if (EmployeerESI != "0")
                                                {
                                                    string[] Emp_split_val = Employer_ESI.Split('.');
                                                    string Emp_Ist = Emp_split_val[0].ToString();
                                                    string Emp_IInd = Emp_split_val[1].ToString();

                                                    if (Convert.ToInt32(Emp_IInd) > 0)
                                                    {
                                                        Employer_ESI = (Convert.ToInt32(Emp_Ist) + 1).ToString();
                                                    }
                                                    else
                                                    {
                                                        Employer_ESI = Emp_Ist;
                                                    }

                                                    ESI = Others2;
                                                    Employer_ESI = Others2;
                                                }
                                            }
                                            else
                                            {
                                                ESI = "0";
                                                Employer_ESI = "0";
                                            }

                                            CanteenAmt = "0";

                                            //if (EligibleCateen == "1")
                                            //{
                                            //    string CanDays = (Convert.ToDouble(Total_Work) - Convert.ToDouble(CanteenDays)).ToString();
                                            //    CanteenAmt = ((Convert.ToDecimal(LabourAmt)) * (Convert.ToDecimal(CanDays))).ToString();
                                            //    CanteenAmt = (Math.Round(Convert.ToDecimal(CanteenAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //}
                                            //else
                                            //{
                                            //    CanteenAmt = "0";
                                            //}

                                            if (EligibleIncentive == "1")
                                            {

                                                DataTable dt_Incentive = new DataTable();
                                                Query = "select Days,Amount from [" + SessionEpay + "]..Incentive_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' and Employee_Type='" + WagesType + "' ";
                                                dt_Incentive = objdata.RptEmployeeMultipleDetails(Query); ;
                                                if (dt_Incentive.Rows.Count > 0)
                                                {
                                                    Incentive_Days = dt_Incentive.Rows[0]["Days"].ToString();
                                                    Incentive_Amt = dt_Incentive.Rows[0]["Amount"].ToString();
                                                }
                                                else
                                                {
                                                    Incentive_Days = "0";
                                                    Incentive_Amt = "0";
                                                }


                                                //string IncentiveDays = (Convert.ToDecimal(Total_Work) - Convert.ToDecimal(weekoff)).ToString();

                                                if (Convert.ToDecimal(Days) >= Convert.ToDecimal(Incentive_Days))
                                                {
                                                    //DayIncentive = (Convert.ToDecimal(10) * Convert.ToDecimal(Days)).ToString();
                                                    DayIncentive = Incentive_Amt;
                                                    DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    DayIncentive = "0";
                                                }
                                            }
                                            else
                                            {
                                                DayIncentive = "0";
                                            }



                                            if (ConvenyAmt == "")
                                            {
                                                ConvenyAmt = "0";
                                            }
                                            else
                                            {
                                                if (StdWork == "") { StdWork = "0"; }
                                                if ((Convert.ToDecimal(ConvenyAmt) == Convert.ToDecimal(0)) || (Convert.ToDecimal(StdWork) == Convert.ToDecimal(0)))
                                                {
                                                    ConvenyAmt = "0";
                                                }
                                                else
                                                {


                                                    Temp_Con_Days = (Convert.ToDecimal(ConvenyAmt) * Convert.ToDecimal(Days)).ToString();
                                                    Temp_Con_OT = (Convert.ToDecimal(ConvenyAmt) / Convert.ToDecimal(StdWork)).ToString();
                                                    Temp_Con_OT = (Convert.ToDecimal(Temp_Con_OT) * Convert.ToDecimal(OTHoursNew)).ToString();

                                                    ConvenyAmt = (Convert.ToDecimal(Temp_Con_Days) + Convert.ToDecimal(Temp_Con_OT)).ToString();
                                                    ConvenyAmt = (Math.Round(Convert.ToDecimal(ConvenyAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }

                                            }

                                            if (SplAmt == "")
                                            {
                                                SplAmt = "0";
                                            }
                                            else
                                            {
                                                //SplAmt = (Convert.ToDecimal(SplAmt) * Convert.ToDecimal(Days)).ToString();
                                                if (StdWork == "") { StdWork = "0"; }
                                                if ((Convert.ToDecimal(SplAmt) == Convert.ToDecimal(0)) || (Convert.ToDecimal(StdWork) == Convert.ToDecimal(0)))
                                                {
                                                    SplAmt = "0";
                                                }
                                                else
                                                {


                                                    Temp_Spl_Days = (Convert.ToDecimal(SplAmt) * Convert.ToDecimal(Days)).ToString();
                                                    Temp_Spl_OT = (Convert.ToDecimal(SplAmt) / Convert.ToDecimal(StdWork)).ToString();
                                                    Temp_Spl_OT = (Convert.ToDecimal(Temp_Spl_OT) * Convert.ToDecimal(OTHoursNew)).ToString();

                                                    SplAmt = (Convert.ToDecimal(Temp_Spl_Days) + Convert.ToDecimal(Temp_Spl_OT)).ToString();
                                                    SplAmt = (Math.Round(Convert.ToDecimal(SplAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }


                                            }

                                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTAmt)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(CanteenAmt) + Convert.ToDecimal(ConvenyAmt) + Convert.ToDecimal(SplAmt)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();

                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            string RoundOffNetPay = rounded1.ToString();

                                            string Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";

                                            string ProcessMode = "";
                                            DataTable DT_sal = new DataTable();
                                            SSQL = "Select * from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "'";
                                            SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And Ccode='" + SessionCcode.ToString() + "' And Lcode='" + SessionLcode.ToString() + "'";
                                            DT_sal = objdata.RptEmployeeMultipleDetails(SSQL); ;
                                            if (DT_sal.Rows.Count != 0)
                                            {
                                                ProcessMode = DT_sal.Rows[0]["Process_Mode"].ToString();
                                                if (ProcessMode == "1")
                                                {
                                                    SSQL = "Delete from [" + SessionEpay + "]..SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "'";
                                                    SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And Ccode='" + SessionCcode.ToString() + "' And Lcode='" + SessionLcode.ToString() + "'";
                                                    objdata.RptEmployeeMultipleDetails(SSQL); ;
                                                }
                                            }

                                            if (ProcessMode != "2")
                                            {
                                                SSQL = "insert into [" + SessionEpay + "]..SalaryDetails(EmpNo,MachineNo,ExisistingCode,SalaryThrough,Wagestype,";
                                                SSQL = SSQL + "FromBankACno,FromBankName,FromBranch,Totalworkingdays,PerDaySal,";
                                                SSQL = SSQL + "BasicandDA,ProvidentFund,HRA,ESI,GrossEarnings,allowances1,allowances2,allowances3,";
                                                SSQL = SSQL + "allowances4,allowances5,Deduction1,Deduction2,Deduction3,Deduction4,Deduction5,";
                                                SSQL = SSQL + "Messdeduction,Losspay,TotalDeductions,NetPay,SalaryDate,Month,Year,FinancialYear,";
                                                SSQL = SSQL + "LeaveDays,LOPDays,TransDate,OverTime,NFh,NFH_Work_Present,FDA,VDA,Advance,Stamp,PfSalary,Words,";
                                                SSQL = SSQL + "WorkedDays,Process_Mode,Weekoff,CL,Fbasic,FFDA,FHRA,Emp_PF,FromDate,ToDate,HomeDays,";
                                                SSQL = SSQL + "HalfNightAmt,FullNightAmt,SpinningAmt,DayIncentive,ThreesidedAmt,Basic_SM,";
                                                SSQL = SSQL + "BasicAndDANew,BasicHRA,EduAllow,MediAllow,BasicRAI,WashingAllow,";
                                                SSQL = SSQL + "RoundOffNetPay,OTHoursNew,OTHoursAmtNew,Leave_Credit_Use,Fixed_Work_Days,";
                                                SSQL = SSQL + "WH_Work_Days,DedOthers1,DedOthers2,EmployeerPFone,EmployeerPFTwo,EmployeerESI,";
                                                SSQL = SSQL + "Leave_Credit_Days,Leave_Credit_Add,Days_Shift,Three_Shift,";
                                                SSQL = SSQL + "Night_Shift,Atten_Inc_Week,Pref_Inc,Spn_Inc,Atten_Inc_Month,Total_Basic,ConvAllow,Conveyance,Ccode,Lcode)values";
                                                SSQL = SSQL + "('" + EmpNo + "','" + MachineID + "','" + ExisistingCode + "','" + SalaryThro.Trim() + "',";
                                                SSQL = SSQL + "'" + WagesType + "','" + AccountNo + "','" + BankName + "','" + BranchCode + "',";
                                                SSQL = SSQL + "'" + Total_Work + "','" + basic1 + "','" + BasicDA + "','" + PFS + "','" + HRA_Str + "','" + ESI + "',";
                                                SSQL = SSQL + "'" + TotalEarnings + "','" + All1 + "','" + All2 + "','" + All3 + "','" + All4 + "','" + All5 + "',";
                                                SSQL = SSQL + "'" + Ded1 + "','" + ded2 + "','" + ded3 + "','" + ded4 + "','" + ded5 + "','" + CanteenAmt + "',";
                                                SSQL = SSQL + "'" + SplAmt + "','" + TotalDeductions + "','" + NetPay + "',GetDate(),'" + ddlMonth.SelectedItem.Text + "','" + ddlFinYear.SelectedValue + "','" + ddlFinYear.SelectedValue + "',";
                                                SSQL = SSQL + "'0','0',GetDate(),'0','" + NFh + "','" + NFH_Work_Days_Statutory + "','0','0','" + Advance + "','0','" + PFS + "','" + Words + "',";
                                                SSQL = SSQL + "'" + Days + "','1','" + weekoff + "','0','" + basic + "','0','0','0','" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',";
                                                SSQL = SSQL + "'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "','0','0','0','0','" + DayIncentive + "','0','" + basic + "',";
                                                SSQL = SSQL + "'" + basic + "','0','0','0','0','" + WashingAllow + "','" + RoundOffNetPay + "','" + OTHoursNew + "','" + OTAmt + "','0','" + Fixed_Work_Days + "',";
                                                SSQL = SSQL + "'" + WH_Work_Present + "','0','0','" + PFS + "','" + Employer_PF + "','" + Employer_ESI + "','0','0','0','" + CanteenDays + "',";
                                                SSQL = SSQL + "'0','0','0','0','0','0','" + ConvenyAmt + "','" + Conveyance + "','" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "')";
                                                objdata.RptEmployeeMultipleDetails(SSQL); ;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        sSourceConnection.Close();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //btnSalCalc.Enabled = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }
    protected void btnSalDown_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();
                Query = "";
                Query = Query + "select EM.EmpNo,(EM.FirstName) as EmpName,(EM.MachineID) as MachineNo,(EM.ExistingCode) as ExisistingCode,(DM.DeptName) as DepartmentNm from Employee_Mst EM inner join Department_Mst DM on EM.DeptCode=DM.DeptCode ";
                Query = Query + "where EM.LocCode='" + SessionLcode + "' and EM.CompCode='" + SessionCcode + "' and EM.IsActive='Yes' ";
                Query = Query + "and EM.Wages='" + ddlEmployeeType.SelectedItem.Text.ToString() + "'";
                Query = Query + " Order by EM.MachineID Asc";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                gvEmp.DataSource = dt;
                gvEmp.DataBind();

                foreach (GridViewRow gvsalDed in gvEmp.Rows)
                {
                    Label lbl_EmpNo = (Label)gvsalDed.FindControl("lblEmpNo");
                    Label lbl_Advance = (Label)gvsalDed.FindControl("lblAdvance");
                    string qry_ot = "";
                    DataTable Advance_Mst_DT = new DataTable();
                    DataTable Advance_Pay_DT = new DataTable();
                    string Advance_Amt = "0.00";
                    string Balance_Amt = "0";
                    string Monthly_Paid_Amt = "0";
                    string Advance_Paid_Amt = "0";
                    if (ddlEmployeeType.SelectedItem.Text != "CIVIL")
                    {
                        qry_ot = "Select * from [" + SessionEpay + "]..AdvancePayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        qry_ot = qry_ot + " And EmpNo='" + lbl_EmpNo.Text.Trim() + "' And Completed='N'";
                        Advance_Mst_DT = objdata.RptEmployeeMultipleDetails(qry_ot);
                        if (Advance_Mst_DT.Rows.Count != 0)
                        {
                            //get Balance Amt
                            string constr = ConfigurationManager.AppSettings["ConnectionString"];
                            SqlConnection cn = new SqlConnection(constr);
                            cn.Open();
                            qry_ot = "Select isnull(sum(Amount),0) as Paid_Amt from [" + SessionEpay + "]..Advancerepayment where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                            SqlCommand cmd_wages = new SqlCommand(qry_ot, cn);
                            Advance_Paid_Amt = (cmd_wages.ExecuteScalar()).ToString();
                            Balance_Amt = (Convert.ToDecimal(Advance_Mst_DT.Rows[0]["Amount"].ToString()) - Convert.ToDecimal(Advance_Paid_Amt)).ToString();
                            Monthly_Paid_Amt = Advance_Mst_DT.Rows[0]["MonthlyDeduction"].ToString();
                            if (Convert.ToDecimal(Monthly_Paid_Amt.ToString()) >= Convert.ToDecimal(Balance_Amt.ToString()))
                            {
                                Advance_Amt = (Math.Round(Convert.ToDecimal(Balance_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                Advance_Amt = (Math.Round(Convert.ToDecimal(Monthly_Paid_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                            }
                            cn.Close();
                        }
                        else
                        {
                            Advance_Amt = "0.00";
                        }
                    }
                    else
                    {
                        Advance_Amt = "0.00";
                    }

                    lbl_Advance.Text = Advance_Amt;
                }
                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Deduction.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}
