﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstLabourAllot.aspx.cs" Inherits="MstLabourAllot" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                }
            });
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>


            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Master</a></li>
                    <li><a href="javascript:;">Labour Allotment</a></li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header"><small>Labour Allotment</small></h1>
                <!-- end page-header -->
                <!--Form Page-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    @*<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>*@
                                </div>
                                <h4 class="panel-title">Labour Allotment</h4>
                            </div>
                            <div class="panel-body panel-form">
                                <div class="form-horizontal form-bordered">
                                    <div id="LabourSub" style="display: block;">

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Unit</label>
                                                    <asp:DropDownList ID="ddlUnit" runat="server"
                                                        class="form-control select2" Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Designation</label>
                                                    <asp:DropDownList ID="ddlDesignation" runat="server"
                                                        class="form-control select2" Style="width: 100%" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Worker Code</label>
                                                    <asp:Label ID="lblWorkCode" runat="server" class="form-control" Text="0"></asp:Label>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>GENERAL</label>
                                                    <asp:TextBox ID="txtGeneral" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtGeneral_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                 <div class="col-md-2">
                                                    <label>Cost</label>
                                                    <asp:TextBox ID="txtGeneralCost" runat="server" AutoPostBack="true" Text="0" class="form-control" OnTextChanged="txtGeneralCost_TextChanged"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label>SHIFT1</label>
                                                    <asp:TextBox ID="txtShift1" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift1_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>SHIFT1COST</label>
                                                    <asp:TextBox ID="txtShift1Cost" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift1Cost_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>SHIFT2</label>
                                                    <asp:TextBox ID="txtShift2" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift2_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>SHIFT2COST</label>
                                                    <asp:TextBox ID="txtShift2Cost" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift2Cost_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>SHIFT3</label>
                                                    <asp:TextBox ID="txtShift3" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift3_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>SHIFT3COST</label>
                                                    <asp:TextBox ID="txtShift3Cost" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift3Cost_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                  <div class="col-md-1">
                                                    <label>SHIFT4</label>
                                                    <asp:TextBox ID="txtShift4" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift4_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>SHIFT4COST</label>
                                                    <asp:TextBox ID="txtShift4Cost" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift4Cost_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                  
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label>SHIFT5</label>
                                                    <asp:TextBox ID="txtShift5" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift5_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>SHIFT5COST</label>
                                                    <asp:TextBox ID="txtShift5Cost" runat="server" AutoPostBack="true" Text="0" OnTextChanged="txtShift5Cost_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>Total</label>
                                                    <asp:Label ID="txtTotal" runat="server" class="form-control" Text="0"></asp:Label>
                                                </div>
                                                 <div class="col-md-1">
                                                    <label>CostTotal</label>
                                                    <asp:Label ID="txtCostTotal" runat="server" class="form-control" Text="0"></asp:Label>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>Required</label>
                                                    <asp:TextBox ID="txtRequired" runat="server" Text="0" AutoPostBack="true" OnTextChanged="txtRequired_TextChanged" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="row">
                                               
                                                <div class="col-md-1">
                                                    <label>ESI No.</label>
                                                    <asp:TextBox ID="txtESINo" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>PF No.</label>
                                                    <asp:TextBox ID="txtPFNo" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>ESI</label>
                                                    <asp:TextBox ID="txtEsi" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>PF</label>
                                                    <asp:TextBox ID="txtPF" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Canteen</label>
                                                    <asp:TextBox ID="txtCanteen" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Tea/Snacks</label>
                                                    <asp:TextBox ID="txtTeaSnacks" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>Ldr No.</label>
                                                    <asp:TextBox ID="txtLdrno" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>Ldr Comm.</label>
                                                    <asp:TextBox ID="txtLeaderComm" runat="server" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                
                                            </div>
                                             <div class="row">
                                                  <div class="col-md-1">
                                                    <label>WEEKLYNOS</label>
                                                    <asp:TextBox ID="txtweeknos" runat="server" AutoPostBack="true" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>WEEKLYCOST</label>
                                                    <asp:TextBox ID="txtweekcost" runat="server" AutoPostBack="true" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                  <div class="col-md-1">
                                                    <label>TRAINEENOS</label>
                                                    <asp:TextBox ID="txttraineenos" runat="server" AutoPostBack="true" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>TRAINEECOST</label>
                                                    <asp:TextBox ID="txttraineecost" runat="server" AutoPostBack="true" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                 </div>
                                            <div class="row">
                                                <label class="control-label col-md-4 col-sm-4"></label>
                                                <div class="col-md-4 col-sm-4">
                                                    <asp:Button ID="btnSave1" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSave1_Click" />
                                                    <asp:Button ID="btnClear1" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClear1_Click" />
                                                </div>
                                                <div class="col-md-4 col-sm-4"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel runat="server" ScrollBars="Auto">
                                        <div id="LabourMain" style="display: block;">
                                            <div class="form-group">
                                                <br />
                                                <div class="row">
                                                    <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                        <HeaderTemplate>
                                                            <table id="example" class="display table table-bordered table-hover table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>S.No</th>
                                                                        <th>Unit</th>
                                                                        <th>Designation</th>
                                                                        <th>Worker Code</th>
                                                                        <th>GENERAL</th>
                                                                        <th>Cost</th>
                                                                        <th>SHIFT1</th>
                                                                        <th>SHIFT1COST</th>
                                                                        <th>SHIFT2</th>
                                                                        <th>SHIFT2COST</th>
                                                                        <th>SHIFT3</th>
                                                                        <th>SHIFT3COST</th>
                                                                        <th>SHIFT4</th>
                                                                        <th>SHIFT4COST</th>
                                                                        <th>SHIFT5</th>
                                                                        <th>SHIFT5COST</th>
                                                                        <th>Total</th>
                                                                        <th>Required</th>
                                                                        <th>ESI No.</th>
                                                                        <th>ESI</th>
                                                                        <th>PF No.</th>
                                                                        <th>PF</th>
                                                                        <th>Canteen</th>
                                                                        <th>Tea/Snakcs</th>
                                                                        <th>Leader Count</th>
                                                                        <th>Leader Commission</th>
                                                                        <th>Mode</th>
                                                                    </tr>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                <td><%# Eval("LocCode")%></td>
                                                                <td><%# Eval("DesignName")%></td>
                                                                <td><%# Eval("WorkerCode")%></td>
                                                                <td><%# Eval("GENERAL")%></td>
                                                                <td><%# Eval("Cost")%></td>
                                                                <td><%# Eval("SHIFT1")%></td>
                                                                 <td><%# Eval("Shift1Cost")%></td>
                                                                <td><%# Eval("SHIFT2")%></td>
                                                                 <td><%# Eval("Shift2Cost")%></td>
                                                                <td><%# Eval("SHIFT3")%></td>
                                                                 <td><%# Eval("Shift3Cost")%></td>
                                                                <td><%# Eval("SHIFT4")%></td>
                                                                 <td><%# Eval("Shift4Cost")%></td>
                                                                <td><%# Eval("SHIFT5")%></td>
                                                                 <td><%# Eval("Shift5Cost")%></td>
                                                                <td><%# Eval("Total")%></td>
                                                                <td><%# Eval("Required")%></td>
                                                                <td><%# Eval("ESINo")%></td>
                                                                <td><%# Eval("ESI")%></td>
                                                                <td><%# Eval("PFNo")%></td>
                                                                <td><%# Eval("PF")%></td>
                                                                <td><%# Eval("Canteen")%></td>
                                                                <td><%# Eval("TeaSnacks")%></td>
                                                                <td><%# Eval("LrNo")%></td>
                                                                <td><%# Eval("LrComm")%></td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEdit" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                        Text="" OnCommand="btnEdit_Command" CommandArgument="Edit" CommandName='<%# Eval("Auto_ID")%>'
                                                                        CausesValidation="true">
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                        Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>'
                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Labour Allotment details?');">
                                                                    </asp:LinkButton>
                                                                </td>

                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate></table></FooterTemplate>
                                                    </asp:Repeater>
                                                    </table>
                                                </div>
                                              
                                                 <div class="row" runat="server" visible="false">
                                                    <div class="col-md-1">
                                                    <label>WEEKLY</label>
                                                    <asp:TextBox ID="txtweek1" runat="server" AutoPostBack="true" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>WEEKLYCOST</label>
                                                    <asp:TextBox ID="txtweekCost1" runat="server" AutoPostBack="true" Text="0" class="form-control"></asp:TextBox>
                                                </div>
                                                       <div style="padding-top:25px">
                                                    <asp:Button ID="Button3" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSaveWeek_Click" />
                                                    <asp:Button ID="Button4" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClearWeek_Click" />
                                                </div>
                                                </div>
                                                  
                                              
                                           
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div runat="server" visible="false">
                <!-- begin #content -->
                <div id="content" class="content">
                    <!-- begin breadcrumb -->
                    <ol class="breadcrumb pull-right">
                        <li><a href="javascript:;">Master</a></li>
                        <li class="active">Employee Allotment</li>
                    </ol>
                    <!-- end breadcrumb -->
                    <!-- begin page-header -->
                    <h1 class="page-header">Employee Allotment </h1>
                    <!-- end page-header -->

                    <!-- begin row -->
                    <div class="row">
                        <!-- begin col-12 -->
                        <div class="col-md-12">
                            <!-- begin panel -->
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <div class="panel-heading-btn">
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                    </div>
                                    <h4 class="panel-title">Employee Allotment</h4>
                                </div>
                                <div class="panel-body">

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-2 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>ManDay Cost</label>
                                                <asp:TextBox runat="server" ID="txtDayCost" class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDayCost" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                        <!-- begin col-2 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Staff Allotment</label>
                                                <asp:TextBox runat="server" ID="txtStaffAllot" class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtStaffAllot" ValidChars="0123456789">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnCostSave" Text="Save" class="btn btn-success" OnClick="btnCostSave_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->


                                    <!-- begin row -->
                                    <div class="row">



                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Department Name</label>
                                                <asp:DropDownList runat="server" ID="ddlDepartment" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Labour Allotment</label>
                                                <asp:TextBox runat="server" ID="txtLabourAllot" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtLabourAllot" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtLabourAllot" ValidChars="0123456789">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger" OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <!-- table start -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>DeptName</th>
                                                                <th>Labour Allotment</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("DeptName")%></td>
                                                        <td><%# Eval("Emp_Count")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("DeptName")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Labour Allotment details?');">
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <!-- table End -->

                                </div>
                            </div>
                            <!-- end panel -->
                        </div>
                        <!-- end col-12 -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end #content -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

